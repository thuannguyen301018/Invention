﻿/*! 2 !*/
using System.Collections.Generic;

namespace Inv.Database
{
  public sealed class Catalog
  {
    public Catalog()
    {
      this.TableList = new Inv.DistinctList<CatalogTable>();
      this.ViewList = new Inv.DistinctList<CatalogView>();
    }

    public Inv.DistinctList<CatalogTable> TableList { get; private set; }
    public Inv.DistinctList<CatalogView> ViewList { get; private set; }

    public CatalogTable AddTable()
    {
      var Result = new CatalogTable();

      TableList.Add(Result);

      return Result;
    }
    public CatalogView AddView()
    {
      var Result = new CatalogView();

      ViewList.Add(Result);

      return Result;
    }
  }

  public sealed class CatalogTable
  {
    public CatalogTable()
    {
      this.ColumnList = new Inv.DistinctList<CatalogColumn>();
      this.IndexList = new Inv.DistinctList<CatalogIndex>();
    }

    public string Name { get; set; }
    public CatalogColumn PrimaryKeyColumn { get; set; }
    public Inv.DistinctList<CatalogColumn> ColumnList { get; private set; }
    public Inv.DistinctList<CatalogIndex> IndexList { get; private set; }

    public CatalogColumn AddColumn()
    {
      var Result = new CatalogColumn();

      ColumnList.Add(Result);

      return Result;
    }
    public CatalogIndex AddIndex()
    {
      var Result = new CatalogIndex();

      IndexList.Add(Result);

      return Result;
    }
  }

  public sealed class CatalogView
  {
    public CatalogView()
    {
      this.ColumnList = new Inv.DistinctList<CatalogColumn>();
    }

    public string Name { get; set; }
    public string Specification { get; set; }
    public Inv.DistinctList<CatalogColumn> ColumnList { get; private set; }

    public CatalogColumn AddColumn()
    {
      var Result = new CatalogColumn();

      ColumnList.Add(Result);

      return Result;
    }
  }

  public sealed class CatalogColumn
  {
    public CatalogColumn()
    {
    }

    public string Name { get; set; }
    public bool Nullable { get; set; }
    public string DataType { get; set; }
    public CatalogForeignKey ForeignKey { get; set; }
  }

  public sealed class CatalogIndex
  {
    public CatalogIndex()
    {
      this.ColumnList = new Inv.DistinctList<CatalogColumn>();
    }

    public string Name { get; set; }
    public bool IsUnique { get; set; }
    public Inv.DistinctList<CatalogColumn> ColumnList { get; private set; }
  }

  public sealed class CatalogForeignKey
  {
    public CatalogForeignKey()
    {
    }

    public string Name { get; set; }
    public CatalogTable ReferenceTable { get; set; }
    public CatalogColumn ReferenceColumn { get; set; }
  }
}