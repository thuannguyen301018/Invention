﻿/*! 30 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Database
{
  public sealed class Query<TSchematic>
    where TSchematic : Contract<Schematic>
  {
    public Query(TSchematic Schematic)
    {
      this.Schematic = Schematic;
    }

    public TSchematic Schematic { get; }

    public Insert<TTable> Insert<TTable>(Func<TSchematic, TTable> TableQuery)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      var Table = TableQuery(Schematic);
      var SourceTable = new SourceTable<TTable>(Table);

      return new Insert<TTable>(SourceTable);
    }
    public Update<TTable> Update<TTable>(Func<TSchematic, TTable> TableQuery)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      var Table = TableQuery(Schematic);
      var SourceTable = new SourceTable<TTable>(Table).As("_0");

      return new Update<TTable>(SourceTable);
    }
    public Delete<TTable> Delete<TTable>(Func<TSchematic, TTable> TableQuery)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      var Table = TableQuery(Schematic);
      var SourceTable = new SourceTable<TTable>(Table).As("_0");

      return new Delete<TTable>(SourceTable);
    }
    public SelectClause SelectClause()
    {
      return new SelectClause();
    }
    public Select Select(SelectExpression Expression)
    {
      return new Select(Expression);
    }
    public SourceTable SourceTable(Inv.Database.Table Table)
    {
      return new SourceTable(Table);
    }
    public SourceTable<TTable> SourceTable<TTable>(Func<TSchematic, TTable> TableFunction)
      where TTable : Contract<Table>
    {
      return new SourceTable<TTable>(TableFunction(Schematic));
    }
    public SourceSelect SourceSelect(SelectExpression SelectExpression)
    {
      return new SourceSelect(SelectExpression);
    }
  }

  public abstract class Expression
  {
    internal static BinaryOperatorExpression BinaryOperator(BinaryOperatorType Type, Expression Left, Expression Right)
    {
      return new BinaryOperatorExpression(Type, Left, Right);
    }
    internal static UnaryOperatorExpression UnaryOperator(UnaryOperatorType Type, Expression Expression)
    {
      return new UnaryOperatorExpression(Type, Expression);
    }
    internal static AggregateExpression Aggregate(AggregateType Type, Expression Expression)
    {
      return new AggregateExpression(Type, Expression);
    }
    internal static LogicalExpression Logical(Expression Expression)
    {
      return new LogicalExpression(Expression);
    }
    internal static VariableExpression Variable(string Name)
    {
      return new VariableExpression(Name);
    }
    internal static BooleanExpression Literal(bool Value)
    {
      return new BooleanExpression(new LiteralExpression(Value));
    }
    internal static DateExpression Literal(Inv.Date Value)
    {
      return new DateExpression(new LiteralExpression(Value));
    }
    internal static DateTimeExpression Literal(DateTime Value)
    {
      return new DateTimeExpression(new LiteralExpression(Value));
    }
    internal static DateTimeOffsetExpression Literal(DateTimeOffset Value)
    {
      return new DateTimeOffsetExpression(new LiteralExpression(Value));
    }
    internal static EnumExpression<T> Literal<T>(T Value)
    {
      return new EnumExpression<T>(new LiteralExpression(Value));
    }
    internal static Integer32Expression Literal(int Value)
    {
      return new Integer32Expression(new LiteralExpression(Value));
    }
    internal static Integer32CollectionExpression Collection(int[] ValueArray)
    {
      return new Integer32CollectionExpression(new CollectionExpression(ValueArray.Cast<object>().ToArray()));
    }
    internal static StringExpression Literal(string Value)
    {
      return new StringExpression(new LiteralExpression(Value));
    }
    internal static TimeExpression Literal(Inv.Time Value)
    {
      return new TimeExpression(new LiteralExpression(Value));
    }
    internal static TimeSpanExpression Literal(TimeSpan Value)
    {
      return new TimeSpanExpression(new LiteralExpression(Value));
    }

    public LogicalExpression IsNull()
    {
      return Expression.Logical(Expression.UnaryOperator(UnaryOperatorType.LogicalIsNull, this));
    }
    public LogicalExpression IsNotNull()
    {
      return Expression.Logical(Expression.UnaryOperator(UnaryOperatorType.LogicalIsNotNull, this));
    }
  }

  public sealed class Parameter
  {
    internal Parameter(VariableExpression Variable, int Index, Inv.Database.ColumnType ColumnType, object Value)
    {
      this.Variable = Variable;
      this.Index = Index;
      this.ColumnType = ColumnType;
      this.Value = Value;
    }

    public VariableExpression Variable { get; }
    public int Index { get; }
    public Inv.Database.ColumnType ColumnType { get; }
    public object Value { get; }
  }

  public sealed class VariableExpression : Expression
  {
    internal VariableExpression(string Name)
    {
      this.Name = "@" + Name;
    }

    public string Name { get; }
  }

  public sealed class LiteralExpression : Expression
  {
    internal LiteralExpression(object Value)
    {
      this.Value = Value;
    }

    public object Value { get; }
  }

  public sealed class CollectionExpression : Expression
  {
    internal CollectionExpression(object[] ValueArray)
    {
      this.ValueArray = ValueArray;
    }

    public object[] ValueArray { get; }
  }

  public abstract class WrapperExpression : Expression
  {
    internal WrapperExpression(Expression Expression)
    {
      Debug.Assert(Expression != null);

      this.Expression = Expression;
    }

    internal Expression Expression { get; }
  }

  public sealed class SourceColumnExpression : Expression
  {
    internal SourceColumnExpression(SourceTable Source, Column Column)
    {
      this.Source = Source;
      this.Column = Column;
    }

    public SourceTable Source { get; }
    public Column Column { get; }
  }

  internal sealed class AsDateFunctionExpression : Expression
  {
    internal AsDateFunctionExpression(Expression Base)
    {
      this.Base = Base;
    }

    public Expression Base { get; private set; }
  }

  internal sealed class DateAddFunctionExpression : Expression
  {
    internal DateAddFunctionExpression(Expression Base, DateAddFunctionInterval Interval, Expression Value)
    {
      this.Base = Base;
      this.Interval = Interval;
      this.Value = Value;
    }

    public Expression Base { get; private set; }
    public DateAddFunctionInterval Interval { get; private set; }
    public Expression Value { get; private set; }
  }

  public enum DateAddFunctionInterval
  {
    Year,
    Quarter,
    Month,
    Week,
    Day,
    Hour,
    Minute,
    Second
    // These aren't equally well supported by all platforms, so we'll leave it up to the application programmer to handle these if necessary.
    //Millisecond,
    //Microsecond,
    //Nanosecond
  }

  public sealed class LogicalExpression : WrapperExpression
  {
    internal LogicalExpression(Expression Expression)
      : base(Expression)
    {
    }

    public static bool operator true(LogicalExpression Base)
    {
      return false;
    }
    public static bool operator false(LogicalExpression Base)
    {
      return false;
    }
    public static LogicalExpression operator !(LogicalExpression Base)
    {
      return Expression.Logical(Expression.UnaryOperator(UnaryOperatorType.LogicalNot, Base));
    }
    public static LogicalExpression operator &(LogicalExpression Left, LogicalExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.LogicalAnd, Left, Right));
    }
    public static LogicalExpression operator |(LogicalExpression Left, LogicalExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.LogicalOr, Left, Right));
    }
  }

  public sealed class StringExpression : WrapperExpression
  {
    internal StringExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(StringExpression Left, StringExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(StringExpression Left, StringExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(StringExpression Left, string Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(StringExpression Left, string Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(string Left, StringExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(string Left, StringExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public sealed class BooleanExpression : WrapperExpression
  {
    internal BooleanExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static bool operator true(BooleanExpression Base)
    {
      return false;
    }
    public static bool operator false(BooleanExpression Base)
    {
      return false;
    }
    public static LogicalExpression operator !(BooleanExpression Base)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Base, Expression.Literal(0)));
    }
    public static LogicalExpression operator &(BooleanExpression Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(1))) && Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Right, Expression.Literal(1)));
    }
    public static LogicalExpression operator |(BooleanExpression Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(1))) || Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Right, Expression.Literal(1)));
    }
    public static LogicalExpression operator ==(BooleanExpression Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(BooleanExpression Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(BooleanExpression Left, bool Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(BooleanExpression Left, bool Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(bool Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(bool Left, BooleanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
    public static implicit operator LogicalExpression(BooleanExpression Value) => Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Value, Expression.Literal(1)));
  }

  public class DateExpression : WrapperExpression
  {
    internal DateExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(Inv.Date Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(Inv.Date Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator >(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonGreaterThan, Left, Right));
    }
    public static LogicalExpression operator <(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonLessThan, Left, Right));
    }
    public static LogicalExpression operator >=(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonGreaterThanOrEqual, Left, Right));
    }
    public static LogicalExpression operator <=(DateExpression Left, DateExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonLessThanOrEqual, Left, Right));
    }
    public static LogicalExpression operator >(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonGreaterThan, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator <(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonLessThan, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator >=(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonGreaterThanOrEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator <=(DateExpression Left, Inv.Date Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonLessThanOrEqual, Left, Expression.Literal(Right)));
    }

    public DateExpression AddWeeks(int Weeks)
    {
      return new DateExpression(new DateAddFunctionExpression(this, DateAddFunctionInterval.Week, Expression.Literal(Weeks)));
    }
  }

  public sealed class DateTimeExpression : WrapperExpression
  {
    internal DateTimeExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(DateTimeExpression Left, DateTimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(DateTimeExpression Left, DateTimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(DateTimeExpression Left, DateTime Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(DateTimeExpression Left, DateTime Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(DateTime Left, DateTimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(DateTime Left, DateTimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }

    public DateExpression AsDate()
    {
      return new DateExpression(new AsDateFunctionExpression(this));
    }
  }

  public sealed class DateTimeOffsetExpression : WrapperExpression
  {
    internal DateTimeOffsetExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(DateTimeOffsetExpression Left, DateTimeOffsetExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(DateTimeOffsetExpression Left, DateTimeOffsetExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(DateTimeOffsetExpression Left, DateTimeOffset Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(DateTimeOffsetExpression Left, DateTimeOffset Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(DateTimeOffset Left, DateTimeOffsetExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(DateTimeOffset Left, DateTimeOffsetExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public sealed class EnumExpression<T> : WrapperExpression
  {
    internal EnumExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(EnumExpression<T> Left, EnumExpression<T> Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(EnumExpression<T> Left, EnumExpression<T> Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(EnumExpression<T> Left, T Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(EnumExpression<T> Left, T Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(T Left, EnumExpression<T> Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(T Left, EnumExpression<T> Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public sealed class GuidExpression : WrapperExpression
  {
    internal GuidExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(GuidExpression Left, GuidExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(GuidExpression Left, GuidExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(GuidExpression Left, Guid Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(GuidExpression Left, Guid Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(Guid Left, GuidExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(Guid Left, GuidExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public class Integer32Expression : WrapperExpression
  {
    internal Integer32Expression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(Integer32Expression Left, Integer32Expression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(Integer32Expression Left, Integer32Expression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(Integer32Expression Left, int Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(Integer32Expression Left, int Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(int Left, Integer32Expression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(int Left, Integer32Expression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }

    public Integer32Expression Max()
    {
      return new Integer32Expression(Expression.Aggregate(AggregateType.Max, this));
    }

    public LogicalExpression In(ICollection<int> ValueList)
    {
      if (ValueList.Count == 1)
        return this == ValueList.ElementAt(0);
      else if (ValueList.Count == 0)
        return Expression.Literal(false) == true;
      else
        return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.LogicalIn, this, Expression.Collection(ValueList.ToArray())));
    }
  }

  public sealed class Integer32CollectionExpression : WrapperExpression
  {
    internal Integer32CollectionExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }

  public sealed class TimeExpression : WrapperExpression
  {
    internal TimeExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(TimeExpression Left, TimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(TimeExpression Left, TimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(TimeExpression Left, Inv.Time Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(TimeExpression Left, Inv.Time Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(Inv.Time Left, TimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(Inv.Time Left, TimeExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public sealed class TimeSpanExpression : WrapperExpression
  {
    internal TimeSpanExpression(Expression Expression)
      : base(Expression)
    {
    }

    public override bool Equals(object obj)
    {
      return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return base.GetHashCode();
    }

    public static LogicalExpression operator ==(TimeSpanExpression Left, TimeSpanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Right));
    }
    public static LogicalExpression operator !=(TimeSpanExpression Left, TimeSpanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Right));
    }
    public static LogicalExpression operator ==(TimeSpanExpression Left, TimeSpan Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator !=(TimeSpanExpression Left, TimeSpan Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Left, Expression.Literal(Right)));
    }
    public static LogicalExpression operator ==(TimeSpan Left, TimeSpanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonEqual, Expression.Literal(Left), Right));
    }
    public static LogicalExpression operator !=(TimeSpan Left, TimeSpanExpression Right)
    {
      return Expression.Logical(Expression.BinaryOperator(BinaryOperatorType.ComparisonNotEqual, Expression.Literal(Left), Right));
    }
  }

  public enum UnaryOperatorType
  {
    LogicalNot,
    LogicalIsNull,
    LogicalIsNotNull
  }

  public static class UnaryOperatorTypeHelper
  {
    public static bool IsPrefix(this UnaryOperatorType Type)
    {
      return Type == UnaryOperatorType.LogicalNot;
    }
  }

  public sealed class UnaryOperatorExpression : Expression
  {
    internal UnaryOperatorExpression(UnaryOperatorType Type, Expression Expression)
    {
      this.Type = Type;
      this.Expression = Expression;
    }

    internal UnaryOperatorType Type { get; }
    internal Expression Expression { get; }
  }

  public enum AggregateType
  {
    Count,
    CountDistinct,
    Min,
    Max,
    Sum,
    Average
  }

  public sealed class AggregateExpression : Expression
  {
    internal AggregateExpression(AggregateType Type, Expression Expression)
    {
      this.Type = Type;
      this.Expression = Expression;
    }

    internal AggregateType Type { get; }
    internal Expression Expression { get; }
  }

  public enum BinaryOperatorType
  {
    ComparisonEqual,
    ComparisonNotEqual,
    ComparisonGreaterThan,
    ComparisonGreaterThanOrEqual,
    ComparisonLessThan,
    ComparisonLessThanOrEqual,
    LogicalAnd,
    LogicalOr,
    LogicalIn
  }

  public sealed class BinaryOperatorExpression : Expression
  {
    internal BinaryOperatorExpression(BinaryOperatorType Type, Expression Left, Expression Right)
    {
      this.Type = Type;
      this.Left = Left;
      this.Right = Right;
    }

    internal BinaryOperatorType Type { get; }
    internal Expression Left { get; }
    internal Expression Right { get; }
  }

  public sealed class Insert<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal Insert(SourceTable<TTable> SourceTable)
    {
      this.SourceTable = SourceTable;
      this.Assignment = new Assignment<TTable>(SourceTable);
    }

    public Assignment<TTable> Assignment { get; private set; }
    public SourceTable<TTable> SourceTable { get; private set; }
  }

  // Note: SQLite does not support update from multiple tables.
  // It may be more sensible to just not support this, rather than only support it for some platforms.
  public sealed class Update<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal Update(SourceTable<TTable> SourceTable)
    {
      this.SourceTable = SourceTable;
      this.Assignment = new Assignment<TTable>(SourceTable);
    }

    public Assignment<TTable> Assignment { get; private set; }
    public SourceTable<TTable> SourceTable { get; private set; }

    public void Where(LogicalExpression Expression)
    {
      this.WhereExpression = Expression;
    }

    internal bool HasWhereExpression()
    {
      return WhereExpression != null;
    }
    internal LogicalExpression GetWhereExpression()
    {
      return WhereExpression;
    }

    private LogicalExpression WhereExpression;
  }

  public sealed class Assignment<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal Assignment(SourceTable<TTable> SourceTable)
    {
      this.SourceTable = SourceTable;

      this.SetList = new Inv.DistinctList<AssignmentSet>();
    }

    public SourceTable<TTable> SourceTable { get; private set; }

    public void Set(Func<TTable, Inv.Database.BooleanColumn> ColumnQuery, bool Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value ? 1 : 0);
    }
    public void Set(Func<TTable, Inv.Database.DateColumn> ColumnQuery, Inv.Date Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.DateColumn.Null> ColumnQuery, Inv.Date? Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.DateTimeColumn> ColumnQuery, DateTime Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.DateTimeOffsetColumn> ColumnQuery, DateTimeOffset Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set<T>(Func<TTable, Inv.Database.EnumColumn<T>> ColumnQuery, T Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set<T>(Func<TTable, Inv.Database.ForeignKeyColumn<T>> ColumnQuery, int Value)
      where T : Inv.Database.Contract<Inv.Database.Table>
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set<T>(Func<TTable, Inv.Database.ForeignKeyColumn<T>.Null> ColumnQuery, int? Value)
      where T : Inv.Database.Contract<Inv.Database.Table>
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.GuidColumn> ColumnQuery, Guid Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.ImageColumn> ColumnQuery, Inv.Image Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.ImageColumn.Null> ColumnQuery, Inv.Image Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.TimeSpanColumn> ColumnQuery, TimeSpan Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.Integer32Column> ColumnQuery, int Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.Integer64Column> ColumnQuery, long Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.StringColumn> ColumnQuery, string Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.StringColumn.Null> ColumnQuery, string Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }
    public void Set(Func<TTable, Inv.Database.TimeColumn> ColumnQuery, Inv.Time Value)
    {
      SetColumnValue(ColumnQuery(SourceTable.Table), Value);
    }

    internal IEnumerable<Inv.Database.AssignmentSet> GetSets()
    {
      return SetList;
    }
    internal IEnumerable<Inv.Database.Contract<Inv.Database.Column>> GetColumns()
    {
      return SetList.Select(S => S.Column);
    }
    internal IEnumerable<Inv.Database.Expression> GetValues()
    {
      return SetList.Select(S => S.Parameter.Variable);
    }
    internal IEnumerable<Inv.Database.Parameter> GetParameters()
    {
      return SetList.Select(S => S.Parameter);
    }

    private void SetColumnValue(Contract<Column> Column, object Value)
    {
      var Variable = Expression.Variable(Column.Base.Name);

      // Note ColumnIndex is 0-based for C# consistency.
      SetList.Add(new AssignmentSet(Column, new Parameter(Variable, SetList.Count, Column.Base.Type, Value)));
    }

    private Inv.DistinctList<AssignmentSet> SetList;
  }

  internal sealed class AssignmentSet
  {
    internal AssignmentSet(Inv.Database.Contract<Inv.Database.Column> Column, Inv.Database.Parameter Parameter)
    {
      this.Column = Column;
      this.Parameter = Parameter;
    }

    public Inv.Database.Contract<Inv.Database.Column> Column { get; private set; }
    public Inv.Database.Parameter Parameter { get; private set; }
  }

  public sealed class Delete<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal Delete(SourceTable<TTable> SourceTable)
    {
      this.SourceTable = SourceTable;
    }

    public SourceTable<TTable> SourceTable { get; private set; }

    public void Where(LogicalExpression Expression)
    {
      this.WhereExpression = Expression;
    }

    internal bool HasWhereExpression()
    {
      return WhereExpression != null;
    }
    internal LogicalExpression GetWhereExpression()
    {
      return WhereExpression;
    }

    private LogicalExpression WhereExpression;
  }

  public sealed class Select
  {
    internal Select(SelectExpression Expression)
    {
      this.Expression = Expression;
      this.OrderList = new DistinctList<Order>();
    }

    internal SelectExpression Expression { get; }

    public Order OrderBy(Expression Expression)
    {
      var Result = new Order(Expression);

      OrderList.Add(Result);

      return Result;
    }

    private Inv.DistinctList<Order> OrderList;
  }

  public enum OrderDirection
  {
    Ascending,
    Descending
  }

  public sealed class Order
  {
    internal Order(Expression Expression)
    {
      this.Expression = Expression;
    }

    internal Expression Expression { get; }
    internal OrderDirection Direction { get; private set; }

    public Order Asc()
    {
      this.Direction = OrderDirection.Ascending;
      return this;
    }
    public Order Desc()
    {
      this.Direction = OrderDirection.Descending;
      return this;
    }
  }

  public abstract class SelectExpression : Expression
  {
    internal SelectExpression()
    {
    }

    public SelectJoin Except(SelectExpression Right)
    {
      return new SelectJoin(SelectJoinType.Except, this, Right);
    }
    public SelectJoin Intersect(SelectExpression Right)
    {
      return new SelectJoin(SelectJoinType.Intersect, this, Right);
    }
    public SelectJoin Union(SelectExpression Right)
    {
      return new SelectJoin(SelectJoinType.Union, this, Right);
    }
  }

  public enum SelectJoinType
  {
    Union,
    Except,
    Intersect
  }

  public sealed class SelectJoin : SelectExpression
  {
    internal SelectJoin(SelectJoinType Type, SelectExpression Left, SelectExpression Right)
    {
      this.Type = Type;
      this.Left = Left;
      this.Right = Right;
    }

    internal SelectJoinType Type { get; }
    internal SelectExpression Left { get; }
    internal SelectExpression Right { get; }
  }

  public sealed class SelectClause : SelectExpression
  {
    internal SelectClause()
    {
      this.TargetList = new Inv.DistinctList<Target>();
      this.SourceList = new Inv.DistinctList<Source>();
      this.OrderByList = new Inv.DistinctList<OrderBy>();
      this.GroupByList = new Inv.DistinctList<Expression>();
    }

    public void ReturnAll()
    {
      this.IsDistinct = false;
    }
    public void ReturnDistinct()
    {
      this.IsDistinct = true;
    }
    public void Top(int TopLimit)
    {
      this.TopLimit = TopLimit;
    }
    public Target<TExpression> Select<TExpression>(TExpression Expression)
      where TExpression : Expression
    {
      var Result = new Target<TExpression>(TargetList.Count, Expression);

      TargetList.Add(Result);

      return Result;
    }
    public void From(Source Source)
    {
      SourceList.Add(Source);
    }
    public void Where(LogicalExpression Expression)
    {
      this.WhereExpression = Expression;
    }
    public void OrderByAscending(params Expression[] OrderByExpressionArray)
    {
      if (OrderByExpressionArray == null || OrderByExpressionArray.Length == 0)
        return;

      OrderByList.AddRange(OrderByExpressionArray.Select(E => new OrderBy() { Expression = E, IsAscending = true }));
    }
    public void OrderByDescending(params Expression[] OrderByExpressionArray)
    {
      if (OrderByExpressionArray == null || OrderByExpressionArray.Length == 0)
        return;

      OrderByList.AddRange(OrderByExpressionArray.Select(E => new OrderBy() { Expression = E, IsAscending = false }));
    }
    public void GroupBy(Expression Expression)
    {
      GroupByList.Add(Expression);
    }
    public void Having(Expression Expression)
    {
      // having list . add
    }

    internal bool IsDistinct { get; private set; }
    internal int? TopLimit { get; private set; }

    internal bool HasTargets()
    {
      return TargetList.Count > 0;
    }
    internal IEnumerable<Target> GetTargets()
    {
      return TargetList;
    }
    internal bool HasSources()
    {
      return SourceList.Count > 0;
    }
    internal IEnumerable<Source> GetSources()
    {
      return SourceList;
    }
    internal bool HasWhereExpression()
    {
      return WhereExpression != null;
    }
    internal LogicalExpression GetWhereExpression()
    {
      return WhereExpression;
    }
    internal bool HasOrderBys()
    {
      return OrderByList.Any();
    }
    internal IEnumerable<OrderBy> GetOrderBys()
    {
      return OrderByList;
    }
    internal bool HasGroupBys()
    {
      return GroupByList.Any();
    }
    internal IEnumerable<Expression> GetGroupBys()
    {
      return GroupByList;
    }
    
    private Inv.DistinctList<Target> TargetList;
    private Inv.DistinctList<Source> SourceList;
    private LogicalExpression WhereExpression;
    private Inv.DistinctList<OrderBy> OrderByList;
    private Inv.DistinctList<Expression> GroupByList;
  }

  internal sealed class OrderBy
  {
    public Expression Expression { get; set; }
    public bool IsAscending { get; set; }
  }

  public abstract class Target
  {
    internal Target(int Index, Expression Expression)
    {
      this.Index = Index;
      this.Expression = Expression;
    }

    internal int Index { get; }
    internal Expression Expression { get; }
    internal string Alias { get; private set; }

    internal Target As(string Alias)
    {
      this.Alias = Alias;
      return this;
    }
  }

  public sealed class Target<TExpression> : Target
    where TExpression : Expression
  {
    internal Target(int Index, TExpression Expression)
      : base(Index, Expression)
    {
      this.Expression = Expression;
    }

    internal new TExpression Expression { get; }

    public new Target<TExpression> As(string Alias)
    {
      base.As(Alias);
      return this;
    }

    public LogicalExpression IsNull()
    {
      return Expression.IsNull();
    }
    public LogicalExpression IsNotNull()
    {
      return Expression.IsNotNull();
    }
  }

  public abstract class Source
  {
    public SourceJoin<Source, TRight> InnerJoin<TRight>(TRight Right)
      where TRight : Source
    {
      return new SourceJoin<Source, TRight>(SourceJoinType.InnerJoin, this, Right);
    }
    public SourceJoin<Source, TRight> CrossJoin<TRight>(TRight Right)
      where TRight : Source
    {
      return new SourceJoin<Source, TRight>(SourceJoinType.CrossJoin, this, Right);
    }
    public SourceJoin<Source, TRight> LeftOuterJoin<TRight>(TRight Right)
      where TRight : Source
    {
      return new SourceJoin<Source, TRight>(SourceJoinType.LeftOuterJoin, this, Right);
    }
  }

  public enum SourceJoinType
  {
    CrossJoin, 
    InnerJoin,
    LeftOuterJoin
  }

  public abstract class SourceJoin : Source
  {
    internal SourceJoin(SourceJoinType Type, Source Left, Source Right)
    {
      this.Type = Type;
      this.Left = Left;
      this.Right = Right;
    }

    internal SourceJoinType Type { get; }
    internal Source Left { get; }
    internal Source Right { get; }
    internal LogicalExpression Expression { get; private set; }

    public SourceJoin On(LogicalExpression Expression)
    {
      this.Expression = Expression;
      return this;
    }
  }

  public sealed class SourceJoin<TLeft, TRight> : SourceJoin
    where TLeft : Source
    where TRight : Source
  {
    internal SourceJoin(SourceJoinType Type, TLeft Left, TRight Right)
      : base(Type, Left, Right)
    {
      this.Left = Left;
      this.Right = Right;
    }

    internal new TLeft Left { get; }
    internal new TRight Right { get; }
  }

  public class SourceTable : Source
  {
    internal SourceTable(Table Table)
    {
      this.Table = Table;
    }

    internal string Alias { get; private set; }
    internal Table Table { get; }

    public SourceTable As(string Alias)
    {
      this.Alias = Alias;
      return this;
    }
    public SourceColumnExpression this[Column Column]
    {
      get { return new SourceColumnExpression(this, Column); }
    }
  }

  public sealed class SourceTable<TTable> : SourceTable
    where TTable : Contract<Table>
  {
    internal SourceTable(TTable Table)
      : base(Table.Base)
    {
      this.Table = Table;
    }

    public new TTable Table { get; }

    public new SourceTable<TTable> As(string Alias)
    {
      base.As(Alias);
      return this;
    }
    public BooleanExpression this[Func<TTable, BooleanColumn> Function]
    {
      get { return new BooleanExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public DateExpression this[Func<TTable, DateColumn> Function]
    {
      get { return new DateExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public DateExpression this[Func<TTable, DateColumn.Null> Function]
    {
      get { return new DateExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public DateTimeExpression this[Func<TTable, DateTimeColumn> Function]
    {
      get { return new DateTimeExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public DateTimeOffsetExpression this[Func<TTable, DateTimeOffsetColumn> Function]
    {
      get { return new DateTimeOffsetExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public GuidExpression this[Func<TTable, GuidColumn> Function]
    {
      get { return new GuidExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public Integer32Expression this[Func<TTable, Integer32Column> Function]
    {
      get { return new Integer32Expression(new SourceColumnExpression(this, Function(Table))); }
    }
    public Integer32Expression this[Func<TTable, Integer32Column.Null> Function]
    {
      get { return new Integer32Expression(new SourceColumnExpression(this, Function(Table))); }
    }
    public Integer32Expression this[Func<TTable, PrimaryKeyColumn> Function]
    {
      get { return new Integer32Expression(new SourceColumnExpression(this, Function(Table))); }
    }
    public Integer32Expression this[Func<TTable, ForeignKeyColumn> Function]
    {
      get { return new Integer32Expression(new SourceColumnExpression(this, Function(Table))); }
    }
    public Integer32Expression this[Func<TTable, ForeignKeyColumn.Null> Function]
    {
      get { return new Integer32Expression(new SourceColumnExpression(this, Function(Table))); }
    }
    public StringExpression this[Func<TTable, StringColumn> Function]
    {
      get { return new StringExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public TimeExpression this[Func<TTable, TimeColumn> Function]
    {
      get { return new TimeExpression(new SourceColumnExpression(this, Function(Table))); }
    }
    public TimeSpanExpression this[Func<TTable, TimeSpanColumn> Function]
    {
      get { return new TimeSpanExpression(new SourceColumnExpression(this, Function(Table))); }
    }

    public EnumExpression<T> GetEnum<T>(Func<TTable, EnumColumn<T>> Function)
    {
      return new EnumExpression<T>(new SourceColumnExpression(this, Function(Table)));
    }
  }

  public sealed class SourceSelect : Source
  {
    internal SourceSelect(SelectExpression SelectExpression)
    {
      this.SelectExpression = SelectExpression;
    }

    public DateExpression this[Target<DateExpression> Target]
    {
      get { return new DateExpression(new SourceSelectTargetExpression(this, Target)); }
    }
    public Integer32Expression this[Target<Integer32Expression> Target]
    {
      get { return new Integer32Expression(new SourceSelectTargetExpression(this, Target)); }
    }
    public DateExpression this[SourceSelectDateField Field]
    {
      get { return new DateExpression(Field); }
    }
    public Integer32Expression this[SourceSelectInteger32Field Field]
    {
      get { return new Integer32Expression(Field); }
    }

    public SourceSelectDateField AddDateField()
    {
      return new SourceSelectDateField(this);
    }
    public SourceSelectInteger32Field AddInteger32Field()
    {
      return new SourceSelectInteger32Field(this);
    }

    internal SelectExpression SelectExpression { get; }
    internal string Alias { get; private set; }

    public SourceSelect As(string Alias)
    {
      this.Alias = Alias;
      return this;
    }
  }

  public class SourceSelectField : Expression
  {
    internal SourceSelectField(SourceSelect SourceSelect)
    {
      this.SourceSelect = SourceSelect;
    }

    internal SourceSelect SourceSelect { get; private set; }
    internal string Alias { get; private set; }

    public SourceSelectField As(string Alias)
    {
      this.Alias = Alias;
      return this;
    }
  }

  public sealed class SourceSelectDateField : SourceSelectField
  {
    internal SourceSelectDateField(SourceSelect SourceSelect)
      : base(SourceSelect)
    {
    }

    public DateExpression Max()
    {
      return new DateExpression(Expression.Aggregate(AggregateType.Max, this));
    }
    public new SourceSelectDateField As(string Alias)
    {
      base.As(Alias);
      return this;
    }
  }

  public sealed class SourceSelectInteger32Field : SourceSelectField
  {
    internal SourceSelectInteger32Field(SourceSelect SourceSelect)
      : base(SourceSelect)
    {
    }

    public new SourceSelectInteger32Field As(string Alias)
    {
      base.As(Alias);
      return this;
    }
  }

  internal sealed class SourceSelectTargetExpression : Expression
  {
    internal SourceSelectTargetExpression(SourceSelect SourceSelect, Target Target)
    {
      this.SourceSelect = SourceSelect;
      this.Target = Target;
    }

    internal string SourceAlias => SourceSelect.Alias;
    internal string TargetAlias => Target.Alias;

    private SourceSelect SourceSelect;
    private Target Target;
  }
}