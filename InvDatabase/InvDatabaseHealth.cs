﻿/*! 2 !*/
using System.Collections.Generic;

namespace Inv.Database
{
  public sealed class HealthCheck
  {
    internal HealthCheck()
    {
      this.GroupList = new Inv.DistinctList<HealthCheckGroup>();
    }

    public HealthCheckGroup AddGroup(string Message)
    {
      var Result = new HealthCheckGroup(Message);

      GroupList.Add(Result);

      return Result;
    }
    public void RemoveGroup(HealthCheckGroup Group)
    {
      GroupList.Remove(Group);
    }
    public IEnumerable<HealthCheckGroup> GetGroups()
    {
      return GroupList;
    }

    private Inv.DistinctList<HealthCheckGroup> GroupList;
  }

  public sealed class HealthCheckGroup
  {
    internal HealthCheckGroup(string Title)
    {
      this.Title = Title;
      this.ResultList = new Inv.DistinctList<HealthCheckResult>();
    }

    public string Title { get; private set; }

    public HealthCheckResult AddResult(string Message)
    {
      var Result = new HealthCheckResult(Message);

      ResultList.Add(Result);

      return Result;
    }
    public IEnumerable<HealthCheckResult> GetResults()
    {
      return ResultList;
    }

    private Inv.DistinctList<HealthCheckResult> ResultList;
  }

  public sealed class HealthCheckResult
  {
    internal HealthCheckResult(string Message)
    {
      this.Message = Message;
    }

    public string Message { get; private set; }
  }
}