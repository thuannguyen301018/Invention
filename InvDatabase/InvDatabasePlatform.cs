﻿/*! 21 !*/
using System;

namespace Inv.Database
{
  public interface Platform
  {
    bool Exists();
    void Create();
    void Drop();

    Connection NewConnection();

    bool UseManualDataTypeCheck { get; }
    bool UseUpdateAliases { get; }
    bool UseUpdateFromSyntax { get; }
    bool UseDeleteFromSyntax { get; }
    bool UseLimitSyntax { get; }
    bool UseForeignKeys { get; }
    bool UseAlterTableAddUniqueConstraint { get; }

    string GetEscapeOpen();
    string GetEscapeClose();
    string GetColumnDefinition(Inv.Database.Contract<Inv.Database.Column> Column);
    string GetPrimaryKeyName(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn);
    string GetPrimaryKeyConstraintDefinition(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn);
    string GetForeignKeyConstraintDefinition(Inv.Database.ForeignKeyColumn ForeignKeyColumn);
    string GetUniqueConstraintDefinition(Inv.Database.Index Index);
    string GetAsDateFunctionCall(string BaseExpression);
    string GetDateAddFunctionCall(string BaseExpression, Inv.Database.DateAddFunctionInterval Type, string ValueExpression);
    string GetLastIdentityCommandText();
    Inv.Database.Catalog GetCatalog(Inv.Database.Connection Connection);
    string GetCatalogColumnDataType(Inv.Database.Contract<Inv.Database.Column> Column);
  }

  public interface Connection : IDisposable
  {
    Platform Platform { get; }
    void Open();
    void HealthCheck(HealthCheck Check);
    void DataTypeCheck(HealthCheckGroup CheckGroup, Inv.Database.Table Table, int PrimaryKeyValue, Inv.Database.Column Column, string RawValue);
    void RetrySleep(int Milliseconds);
    Transaction BeginTransaction();
    Command NewCommand();
  }

  public interface Transaction : IDisposable
  {
    void Rollback();
    void Commit();
  }

  public interface Command : IDisposable
  {
    void SetTimeout(int Seconds);
    void SetText(string Text);
    void SetParameter(string Name, int Index, Inv.Database.ColumnType ColumnType, object Value);
    int ExecuteNonQuery();
    Reader ExecuteReader();
  }

  public interface Reader : IDisposable
  {
    bool Read();
    bool IsNull(int ColumnIndex);
    bool GetBoolean(int ColumnIndex);
    byte GetByte(int ColumnIndex);
    Inv.Date GetDate(int ColumnIndex);
    DateTime GetDateTime(int ColumnIndex);
    DateTimeOffset GetDateTimeOffset(int ColumnIndex);
    decimal GetDecimal(int ColumnIndex);
    double GetDouble(int ColumnIndex);
    float GetFloat(int ColumnIndex);
    Guid GetGuid(int ColumnIndex);
    Inv.Image GetImage(int ColumnIndex);
    int GetInteger32(int ColumnIndex);
    long GetInteger64(int ColumnIndex);
    Inv.Money GetMoney(int ColumnIndex);
    Inv.Time GetTime(int ColumnIndex);
    TimeSpan GetTimeSpan(int ColumnIndex);
    string GetString(int ColumnIndex);
  }
}