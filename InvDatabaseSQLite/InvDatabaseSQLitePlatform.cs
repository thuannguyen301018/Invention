﻿/*! 30 !*/
using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using Inv.Support;

namespace Inv.Database.SQLite
{
  public sealed class Platform : Inv.Database.Platform
  {
    public Platform(Inv.File File)
    {
      SQLitePCL.Batteries_V2.Init();

      if (SQLitePCL.raw.sqlite3_threadsafe() == 0)
        throw new Exception("SQLite has not been compiled in threadsafe mode.");

      this.File = File;

      this.DateAddFunctionIntervalDictionary = new Dictionary<DateAddFunctionInterval, string>()
      {
        { DateAddFunctionInterval.Year, "YEAR" },
        { DateAddFunctionInterval.Quarter, "QUARTER" },
        { DateAddFunctionInterval.Month, "MONTH" },
        { DateAddFunctionInterval.Week, "WEEK" },
        { DateAddFunctionInterval.Day, "DAY" },
        { DateAddFunctionInterval.Hour, "HOUR" },
        { DateAddFunctionInterval.Minute, "MINUTE" },
        { DateAddFunctionInterval.Second, "SECOND" }
      };
    }

    bool Inv.Database.Platform.Exists()
    {
      return File.Exists();
    }
    void Inv.Database.Platform.Create()
    {
      Platform.CheckResult(null, "Open", SQLitePCL.raw.sqlite3_open_v2(GetFilePath(), out var Handle, SQLitePCL.raw.SQLITE_OPEN_READWRITE | SQLitePCL.raw.SQLITE_OPEN_CREATE, null));

      if (Handle != null)
      {
        Platform.CheckResult(null, "Close", SQLitePCL.raw.sqlite3_close_v2(Handle));
        Handle.Dispose();
      }
    }
    void Inv.Database.Platform.Drop()
    {
      File.Delete();
    }
    Inv.Database.Connection Inv.Database.Platform.NewConnection()
    {
      return new Connection(this, GetFilePath());
    }
    bool Inv.Database.Platform.UseManualDataTypeCheck => true;
    bool Inv.Database.Platform.UseUpdateAliases => false;
    bool Inv.Database.Platform.UseUpdateFromSyntax => false;
    bool Inv.Database.Platform.UseDeleteFromSyntax => false;
    bool Inv.Database.Platform.UseLimitSyntax => true;
    bool Inv.Database.Platform.UseForeignKeys => false;
    bool Inv.Database.Platform.UseAlterTableAddUniqueConstraint => false;
    string Inv.Database.Platform.GetEscapeOpen()
    {
      return "[";
    }
    string Inv.Database.Platform.GetEscapeClose()
    {
      return "]";
    }
    string Inv.Database.Platform.GetColumnDefinition(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      var BaseColumn = Column.Base;

      var Result = "";

      switch (BaseColumn.Type)
      {
        case Inv.Database.ColumnType.Binary:
        case Inv.Database.ColumnType.Image:
          Result = "BLOB";
          break;

        case Inv.Database.ColumnType.Boolean:
        case Inv.Database.ColumnType.Enum:
        case Inv.Database.ColumnType.ForeignKey:
        case Inv.Database.ColumnType.Integer32:
        case Inv.Database.ColumnType.Integer64:
        case Inv.Database.ColumnType.PrimaryKey:
        case Inv.Database.ColumnType.TimeSpan:
          Result = "INTEGER";
          break;

        case Inv.Database.ColumnType.Date:
        case Inv.Database.ColumnType.DateTime:
        case Inv.Database.ColumnType.DateTimeOffset:
        case Inv.Database.ColumnType.Decimal:
        case Inv.Database.ColumnType.Guid:
        case Inv.Database.ColumnType.Money:
        case Inv.Database.ColumnType.String:
        case Inv.Database.ColumnType.Time:
          Result = "TEXT";
          break;

        case Inv.Database.ColumnType.Double:
          Result = "REAL";
          break;

        default:
          throw new Exception("Unexpected ColumnType '" + BaseColumn.Type.ToString() + "'");
      }

      // Must be declared here, since we can't specify autoincrement without the primary key declaration, and it can't be declared in the isolated primary key declaration.
      var PrimaryKeyColumn = Column as Inv.Database.PrimaryKeyColumn;
      if (PrimaryKeyColumn != null)
        Result += $" CONSTRAINT { Syntax.PrimaryKeyName(PrimaryKeyColumn) } PRIMARY KEY ON CONFLICT ROLLBACK AUTOINCREMENT";

      if (!BaseColumn.Nullable) // Note: this is implied in the PRIMARY KEY constraint, but some earlier versions of SQLite allow nulls in primary keys.
        Result += " NOT NULL ON CONFLICT ROLLBACK";

      return Result;
    }
    string Inv.Database.Platform.GetPrimaryKeyName(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      return Syntax.PrimaryKeyName(PrimaryKeyColumn);
    }
    string Inv.Database.Platform.GetPrimaryKeyConstraintDefinition(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      return null;
    }
    string Inv.Database.Platform.GetForeignKeyConstraintDefinition(Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      throw new Exception("SQLite does not support alter table add constraint; GetForeignKeyDefinition() should never be accessed.");
    }
    string Inv.Database.Platform.GetUniqueConstraintDefinition(Inv.Database.Index Index)
    {
      throw new Exception("SQLite does not support alter table add constraint; GetUniqueConstraintDefinition() should never be accessed.");
    }
    string Inv.Database.Platform.GetAsDateFunctionCall(string BaseExpression)
    {
      return $"date({ BaseExpression })";
    }
    string Inv.Database.Platform.GetDateAddFunctionCall(string BaseExpression, Inv.Database.DateAddFunctionInterval Type, string ValueExpression)
    {
      var IntervalType = DateAddFunctionIntervalDictionary.GetValueOrDefault(Type);

      if (string.IsNullOrEmpty(IntervalType))
        throw new Exception("Unexpected DateAddFunctionInterval '" + Type.ToString() + "'");

      return $"date({ BaseExpression }, '{ ValueExpression } { IntervalType }')";
    }
    string Inv.Database.Platform.GetLastIdentityCommandText()
    {
      return "select last_insert_rowid()";
    }
    Inv.Database.Catalog Inv.Database.Platform.GetCatalog(Inv.Database.Connection Connection)
    {
      var Result = new Inv.Database.Catalog();

      var TableNameDictionary = new Dictionary<string, CatalogTable>();
      var TableNameSQLDictionary = new Dictionary<string, string>();

      var ViewNameDictionary = new Dictionary<string, CatalogView>();

      using (var TableCommand = Connection.NewCommand())
      {
        TableCommand.SetText($"select [name], [sql] from sqlite_master where type = 'table'");

        using (var TableReader = TableCommand.ExecuteReader())
        {
          while (TableReader.Read())
          {
            var TableName = TableReader.GetString(0);
            var SQL = TableReader.GetString(1);

            // Ignore internal system tables.
            if (TableName.Equals("sqlite_sequence", StringComparison.CurrentCultureIgnoreCase))
              continue;

            var Table = Result.AddTable();
            Table.Name = TableName;

            TableNameDictionary.Add(TableName, Table);
            TableNameSQLDictionary.Add(TableName, SQL);
          }
        }
      }

      using (var ViewCommand = Connection.NewCommand())
      {
        ViewCommand.SetText($"select [name] from sqlite_master where type = 'view'");

        using (var ViewReader = ViewCommand.ExecuteReader())
        {
          while (ViewReader.Read())
          {
            var ViewName = ViewReader.GetString(0);

            var View = Result.AddView();
            View.Name = ViewName;

            ViewNameDictionary.Add(ViewName, View);
          }
        }
      }

      foreach (var Table in Result.TableList)
      {
        using (var TableInfoCommand = Connection.NewCommand())
        {
          TableInfoCommand.SetText($"pragma table_info('{ Table.Name }')");

          using (var TableInfoReader = TableInfoCommand.ExecuteReader())
          {
            while (TableInfoReader.Read())
            {
              var ColumnId = TableInfoReader.GetInteger32(0);
              var ColumnName = TableInfoReader.GetString(1);
              var DataType = TableInfoReader.GetString(2);
              var NotNullable = TableInfoReader.GetBoolean(3);
              var DefaultValue = TableInfoReader.GetString(4);
              var IsPrimaryKey = TableInfoReader.GetInteger32(5);

              var Column = Table.AddColumn();
              Column.Name = ColumnName;
              Column.DataType = DataType;
              Column.Nullable = !NotNullable;

              if (IsPrimaryKey > 0)
              {
                if (Table.PrimaryKeyColumn != null)
                  throw new Exception($"Table '{ Table.Name }' has multiple primary key columns.");

                Table.PrimaryKeyColumn = Column;
              }
            }
          }
        }

        using (var IndexListCommand = Connection.NewCommand())
        {
          IndexListCommand.SetText($"pragma index_list('{ Table.Name }')");

          using (var IndexListReader = IndexListCommand.ExecuteReader())
          {
            while (IndexListReader.Read())
            {
              var IndexID = IndexListReader.GetInteger32(0);
              var IndexName = IndexListReader.GetString(1);
              var IsUnique = IndexListReader.GetBoolean(2);
              var IndexType = IndexListReader.GetString(3);

              var Index = Table.AddIndex();
              Index.Name = IndexName;
              Index.IsUnique = IsUnique;
            }
          }
        }
      }

      foreach (var Table in Result.TableList)
      {
        foreach (var Index in Table.IndexList)
        {
          using (var IndexInfoCommand = Connection.NewCommand())
          {
            IndexInfoCommand.SetText($"pragma index_info('{ Index.Name }')");

            var SequenceColumnDictionary = new Dictionary<int, CatalogColumn>();

            using (var IndexInfoReader = IndexInfoCommand.ExecuteReader())
            {
              while (IndexInfoReader.Read())
              {
                var IndexSequence = IndexInfoReader.GetInteger32(0);
                var TableSequence = IndexInfoReader.GetInteger32(1);
                var ColumnName = IndexInfoReader.GetString(2);

                var Column = Table.ColumnList.Find(C => C.Name.Equals(ColumnName, StringComparison.CurrentCultureIgnoreCase));
                if (Column == null)
                  throw new Exception($"Column '{ ColumnName }' could not be found for index '{ Index.Name }'");

                SequenceColumnDictionary.Add(IndexSequence, Column);
              }
            }

            foreach (var Column in SequenceColumnDictionary.OrderBy(P => P.Key).Select(P => P.Value))
              Index.ColumnList.Add(Column);
          }
        }

        /*using (var ForeignKeyCommand = Connection.NewCommand())
        {
          ForeignKeyCommand.SetText($"pragma foreign_key_list('{ Table.Name }')");

          using (var ForeignKeyReader = ForeignKeyCommand.ExecuteReader())
          {
            while (ForeignKeyReader.Read())
            {
              var ForeignKeyID = ForeignKeyReader.GetInteger32(0);
              var ForeignKeySequence = ForeignKeyReader.GetInteger32(1);
              var ForeignKeyReferenceTableName = ForeignKeyReader.GetString(2);
              var ForeignKeyColumnName = ForeignKeyReader.GetString(3);
              var ForeignKeyReferenceColumnName = ForeignKeyReader.GetString(4);
              var ForeignKeyUpdateRule = ForeignKeyReader.GetString(5);
              var ForeignKeyDeleteRule = ForeignKeyReader.GetString(6);

              var ForeignKeyName = "FK_" + Table.Name + "_" + ForeignKeyColumnName;

              var Column = Table.ColumnList.Find(C => C.Name.Equals(ForeignKeyColumnName, StringComparison.CurrentCultureIgnoreCase));
              if (Column == null)
                throw new Exception($"Could not find column '{ ForeignKeyColumnName }' on table '{ Table.Name }' for foreign key '{ ForeignKeyName }'");

              var ReferenceTable = TableNameDictionary.GetValueOrDefault(ForeignKeyReferenceTableName);
              if (ReferenceTable == null)
                throw new Exception($"Could not find table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

              var ReferenceColumn = ReferenceTable.ColumnList.Find(C => C.Name.Equals(ForeignKeyReferenceColumnName, StringComparison.CurrentCultureIgnoreCase));
              if (ReferenceColumn == null)
                throw new Exception($"Could not find column '{ ForeignKeyReferenceColumnName }' on table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

              if (!ForeignKeyUpdateRule.Equals("NO ACTION", StringComparison.CurrentCultureIgnoreCase))
                throw new Exception("NO ACTION update rule required for all foreign keys");

              if (!ForeignKeyDeleteRule.Equals("NO ACTION", StringComparison.CurrentCultureIgnoreCase))
                throw new Exception("NO ACTION update rule required for all foreign keys");

              var ForeignKey = new CatalogForeignKey();
              Column.ForeignKey = ForeignKey;
              ForeignKey.Name = ForeignKeyName;
              ForeignKey.ReferenceTable = ReferenceTable;
              ForeignKey.ReferenceColumn = ReferenceColumn;
            }
          }
        }*/

        var TableSQL = TableNameSQLDictionary.GetValueOrDefault(Table.Name);

        if (string.IsNullOrEmpty(TableSQL))
          throw new Exception($"Unable to find create table specification for table { Table.Name }");

        var OpenParenthesisIndex = TableSQL.IndexOf('(');
        var CloseParenthesisIndex = TableSQL.LastIndexOf(')');
        if (OpenParenthesisIndex < 0 || CloseParenthesisIndex < 0)
          throw new Exception($"Unable to parse create table specification for table { Table.Name }");

        var ColumnList = TableSQL.Substring(OpenParenthesisIndex + 1, CloseParenthesisIndex - OpenParenthesisIndex - 1).Trim();
        if (string.IsNullOrWhiteSpace(ColumnList) || ColumnList.Length == 0)
          throw new Exception($"Create table specification for table { Table.Name } contains no columns");

        var ColumnDefinitionArray = ColumnList.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(D => D.Trim()).ToArray();
        foreach (var ColumnDefinition in ColumnDefinitionArray.Where(C => C.Contains("CONSTRAINT", StringComparison.CurrentCultureIgnoreCase)))
        {
          var ColumnNameIndex = ColumnDefinition.IndexOf(' ');
          if (ColumnNameIndex < 0)
            throw new Exception($"Unable to parse column specification for table { Table.Name } ({ ColumnDefinition })");

          var ColumnName = ColumnDefinition.Substring(0, ColumnNameIndex + 1).Trim();
          var Column = Table.ColumnList.Find(C => AreColumnNamesEqual(C.Name, ColumnName));
          if (Column == null)
            throw new Exception($"Unable to find column { ColumnName } for table { Table.Name }");

          if (ColumnDefinition.Contains("PRIMARY KEY", StringComparison.CurrentCultureIgnoreCase))
          {
            if (Table.PrimaryKeyColumn != Column)
              throw new Exception($"pragma table_info() considers { Table.PrimaryKeyColumn.Name } primary key for table { Table.Name }, but sqlite_master's SQL specification has { Column.Name }");

            var PrimaryKeySplitIndex = ColumnDefinition.IndexOf("PRIMARY KEY", StringComparison.CurrentCultureIgnoreCase);
            if (PrimaryKeySplitIndex < 0)
              throw new Exception($"Primary key specification for table { Table.Name } expected format '[COLUMN NAME] INTEGER CONSTRAINT [PRIMARY KEY NAME] PRIMARY KEY [UNIQUE CONFLICT] AUTOINCREMENT [NOT NULL CONFLICT]'");

            var ColumnNameAndType = ColumnDefinition.Substring(0, PrimaryKeySplitIndex).Trim();
            var PrimaryKeyNameLeadingSpaceIndex = ColumnNameAndType.LastIndexOf(' ');
            if (PrimaryKeyNameLeadingSpaceIndex < 0)
              throw new Exception($"Unable to parse primary key specification for table { Table.Name } ({ ColumnDefinition })");

            var PrimaryKeyName = ColumnNameAndType.Substring(PrimaryKeyNameLeadingSpaceIndex + 1).Trim();

            var PrimaryKeyIndex = Table.AddIndex();
            PrimaryKeyIndex.Name = PrimaryKeyName;
            PrimaryKeyIndex.IsUnique = true;
            PrimaryKeyIndex.ColumnList.Add(Table.PrimaryKeyColumn);

            var ConflictSpecification = ColumnDefinition.Substring(PrimaryKeySplitIndex + "PRIMARY KEY".Length).Trim();
            if (!ConflictSpecification.Equals("ON CONFLICT ROLLBACK AUTOINCREMENT NOT NULL ON CONFLICT ROLLBACK", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception($"Unexpected primary key conflict clause constraint for table { Table.Name } ({ ConflictSpecification })");

            continue;
          }

          /*if (ColumnDefinition.Contains("REFERENCES", StringComparison.CurrentCultureIgnoreCase))
          {
            if (Column.ForeignKey == null)
              throw new Exception($"pragma foreign_key_list() considers { Column.Name } to not be a foreign key for table { Table.Name }, but sqlite_master's SQL specification does");

            var ForeignKeySplitIndex = ColumnDefinition.IndexOf("REFERENCES", StringComparison.CurrentCultureIgnoreCase);
            if (ForeignKeySplitIndex < 0)
              throw new Exception($"Foreign key specification for table { Table.Name } expected format '[COLUMN NAME] [COLUMN TYPE] CONSTRAINT [FOREIGN KEY NAME] REFERENCES [REFERENCE TABLE] ([REFERENCE COLUMNS]) [UPDATE CONFLICT] [DELETE CONFLICT] [NOT NULL CONFLICT]'");

            var ColumnNameAndType = ColumnDefinition.Substring(0, ForeignKeySplitIndex).Trim();
            var ForeignKeyNameLeadingSpaceIndex = ColumnNameAndType.LastIndexOf(' ');
            if (ForeignKeyNameLeadingSpaceIndex < 0)
              throw new Exception($"Unable to parse foreign key specification for table { Table.Name } ({ ColumnDefinition })");

            var ForeignKeyName = ColumnNameAndType.Substring(ForeignKeyNameLeadingSpaceIndex + 1).Trim();

            var ReferenceSpecification = ColumnDefinition.Substring(ForeignKeySplitIndex + "REFERENCES".Length).Trim();
            var ConflictStartIndex = ReferenceSpecification.IndexOf("ON", StringComparison.CurrentCultureIgnoreCase);
            if (ConflictStartIndex < 0)
              throw new Exception($"Unable to parse foreign key reference specification for table { Table.Name } ({ ColumnDefinition })");

            var ConflictSpecification = ReferenceSpecification.Substring(ConflictStartIndex).Trim();
            if (!ConflictSpecification.StartsWith("ON UPDATE NO ACTION ON DELETE NO ACTION", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception($"Unexpected foreign key update/delete conflict clause constraint for table { Table.Name } ({ ConflictSpecification })");

            var NullConflictSpecification = Column.Nullable ? "DEFAULT NULL" : "NOT NULL ON CONFLICT ROLLBACK";
            if (!ConflictSpecification.EndsWith(NullConflictSpecification, StringComparison.CurrentCultureIgnoreCase))
              throw new Exception($"Unexpected foreign key nullability specification for table { Table.Name } ({ ConflictSpecification })");

            continue;
          }*/

          throw new Exception($"Unexpected column constraint for table { Table.Name }");
        }
      }

      return Result;
    }
    string Inv.Database.Platform.GetCatalogColumnDataType(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      switch (Column.Base.Type)
      {
        case Inv.Database.ColumnType.Binary:
        case Inv.Database.ColumnType.Image:
          return "BLOB";

        case Inv.Database.ColumnType.Boolean:
        case Inv.Database.ColumnType.Enum:
        case Inv.Database.ColumnType.ForeignKey:
        case Inv.Database.ColumnType.Integer32:
        case Inv.Database.ColumnType.Integer64:
        case Inv.Database.ColumnType.PrimaryKey:
          return "INTEGER";

        case Inv.Database.ColumnType.Date:
        case Inv.Database.ColumnType.DateTime:
        case Inv.Database.ColumnType.DateTimeOffset:
        case Inv.Database.ColumnType.Decimal:
        case Inv.Database.ColumnType.Guid:
        case Inv.Database.ColumnType.Money:
        case Inv.Database.ColumnType.String:
        case Inv.Database.ColumnType.Time:
        case Inv.Database.ColumnType.TimeSpan:
          return "TEXT";

        case Inv.Database.ColumnType.Double:
          return "REAL";

        default:
          throw new Exception("Unexpected ColumnType '" + Column.Base.Type.ToString() + "'");
      }
    }

    internal static void CheckResult(SQLitePCL.sqlite3 Handle, string Method, int ResultCode, int? ExpectedCode = null)
    {
      var CheckCode = ExpectedCode ?? 0;

      if (ResultCode != CheckCode)
      {
        var ErrorMessage = Handle != null ? SQLitePCL.raw.sqlite3_errmsg(Handle) : "<null>";
        var ExtendedResultCode = Handle != null ? SQLitePCL.raw.sqlite3_extended_errcode(Handle) : (int?)null;

        throw new Exception($"Error on '{ Method }': '{ ErrorMessage }' (ResultCode = [{ ResultCode }], ExtendedResultCode = [{ ExtendedResultCode }])");
      }
    }

    private string GetFilePath()
    {
      return File.GetPlatformPath();
    }
    private bool AreColumnNamesEqual(string SchematicName, string SQLName)
    {
      if (SchematicName.Equals(SQLName, StringComparison.CurrentCultureIgnoreCase))
        return true;

      if (SQLName.Equals("[" + SchematicName + "]", StringComparison.CurrentCultureIgnoreCase))
        return true;

      return false;
    }

    private Inv.File File;
    private Dictionary<Inv.Database.DateAddFunctionInterval, string> DateAddFunctionIntervalDictionary;
  }

  internal sealed class Connection : Inv.Database.Connection
  {
    internal Connection(Platform Platform, string FilePath)
    {
      this.Platform = Platform;
      this.FilePath = FilePath;
    }

    Inv.Database.Platform Inv.Database.Connection.Platform => Platform;
    void IDisposable.Dispose()
    {
      if (Base != null)
      {
        Platform.CheckResult(Base, "Close", SQLitePCL.raw.sqlite3_close_v2(Base));

        Base.Dispose();
        Base = null;
      }
    }
    void Inv.Database.Connection.Open()
    {
      Platform.CheckResult(Base, "Open", SQLitePCL.raw.sqlite3_open_v2(FilePath, out Base, SQLitePCL.raw.SQLITE_OPEN_READWRITE | SQLitePCL.raw.SQLITE_OPEN_CREATE | SQLitePCL.raw.SQLITE_OPEN_FULLMUTEX, null));

      // Turn on foreign key checking.
      Platform.CheckResult(Base, "Pragma - foreign keys", SQLitePCL.raw.sqlite3_exec(Base, "pragma foreign_keys = on;"));

      // Turn on WAL.
      using (var Command = NewCommand())
      {
        Command.SetText("pragma journal_mode = WAL;");

        using (var Reader = Command.ExecuteReader())
        {
          if (!Reader.Read())
            throw new Exception("Could not validate WAL has been enabled for this connection");

          if (!Reader.GetString(0).Equals("wal", StringComparison.CurrentCultureIgnoreCase))
            throw new Exception("WAL is not enabled for this connection");
        }
      }

      // Check that foreign keys are enabled.
      using (var Command = NewCommand())
      {
        Command.SetText("pragma foreign_keys");

        using (var Reader = Command.ExecuteReader())
        {
          if (!Reader.Read())
            throw new Exception("Could not validate foreign keys are enabled for this connection");

          if (!Reader.GetBoolean(0))
            throw new Exception("Foreign keys are not enabled for this connection");
        }
      }
    }
    void Inv.Database.Connection.HealthCheck(Inv.Database.HealthCheck HealthCheck)
    {
      using (var Command = NewCommand())
      {
        Command.SetText("pragma integrity_check(1000)");

        using (var Reader = Command.ExecuteReader())
        {
          var IsOK = false;
          var IntegrityGroup = (Inv.Database.HealthCheckGroup)null;

          while (Reader.Read())
          {
            var Message = Reader.GetString(0);

            if (Message.Equals("ok", StringComparison.CurrentCultureIgnoreCase))
            {
              // Ignore.
              Debug.Assert(!IsOK, "Multiple 'ok' results returned.");

              IsOK = true;
            }
            else
            {
              Debug.Assert(!IsOK, "'ok' result was returned, but integrity issues were found.");

              if (IntegrityGroup == null)
                IntegrityGroup = HealthCheck.AddGroup("Structure");

              IntegrityGroup.AddResult(Message);
            }
          }
        }
      }
    }
    void Inv.Database.Connection.DataTypeCheck(HealthCheckGroup CheckGroup, Inv.Database.Table Table, int PrimaryKeyValue, Inv.Database.Column Column, string RawValue)
    {
      var IgnoreColumnTypeSet = new[] { Inv.Database.ColumnType.Binary, Inv.Database.ColumnType.ForeignKey, Inv.Database.ColumnType.Image, Inv.Database.ColumnType.String }.ToHashSetX();

      if (IgnoreColumnTypeSet.Contains(Column.Type))
        return;

      void AddResult(string Format)
      {
        CheckGroup.AddResult($"{ Table.Name }.{ Column.Name }: Expected { Format } but found '{ RawValue }'");
      }

      switch (Column.Type)
      {
        case Inv.Database.ColumnType.Boolean:
          if (!int.TryParse(RawValue, out var BooleanResult) || BooleanResult > 1 || BooleanResult < 0)
            AddResult("boolean (0/1)");
          break;

        case Inv.Database.ColumnType.Date:
          if (!Inv.Date.TryParseExact(RawValue, Protocol.DateFormat, out var DateResult))
            AddResult($"date ({ Protocol.DateFormat })");
          break;

        case Inv.Database.ColumnType.DateTime:
          if (!DateTime.TryParseExact(RawValue, Protocol.DateTimeFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out var DateTimeResult))
            AddResult($"datetime ({ Protocol.DateTimeFormat })");
          break;

        case Inv.Database.ColumnType.DateTimeOffset:
          if (!DateTimeOffset.TryParseExact(RawValue, Protocol.DateTimeOffsetFormat, null, System.Globalization.DateTimeStyles.AssumeLocal, out var DateTimeOffsetResult))
            AddResult($"datetimeoffset ({ Protocol.DateTimeOffsetFormat })");
          break;

        case Inv.Database.ColumnType.Decimal:
        case Inv.Database.ColumnType.Money:
          if (!decimal.TryParse(RawValue, out var DecimalResult))
            AddResult(Column.Type.ToString().ToLower());
          break;

        case Inv.Database.ColumnType.Double:
          if (!double.TryParse(RawValue, out var DoubleResult))
            AddResult("double");
          break;

        case Inv.Database.ColumnType.Enum:
          if (!int.TryParse(RawValue, out var EnumResult) || EnumResult < 0)
            AddResult("enum (0-n)");
          break;

        case Inv.Database.ColumnType.Guid:
          if (!Guid.TryParse(RawValue, out var GuidResult))
            AddResult("guid");
          break;

        case Inv.Database.ColumnType.Integer32:
          if (!int.TryParse(RawValue, out var IntegerResult))
            AddResult("32-bit integer");
          break;

        case Inv.Database.ColumnType.Integer64:
        case Inv.Database.ColumnType.TimeSpan:
          if (!long.TryParse(RawValue, out var LongResult))
            AddResult(Column.Type == Inv.Database.ColumnType.Integer64 ? "64-bit integer" : "timespan");
          break;

        case Inv.Database.ColumnType.Time:
          if (!Inv.Time.TryParseExact(RawValue, Protocol.TimeFormat, out var TimeResult))
            AddResult($"time ({ Protocol.TimeFormat })");
          break;
      }
    }
    void Inv.Database.Connection.RetrySleep(int Milliseconds)
    {
      //System.Threading.Thread.Sleep(Milliseconds);
    }
    Inv.Database.Transaction Inv.Database.Connection.BeginTransaction()
    {
      var Result = new Transaction(this);

      Result.ExecuteBegin();

      return Result;
    }
    Inv.Database.Command Inv.Database.Connection.NewCommand()
    {
      return new Command(this);
    }

    internal SQLitePCL.sqlite3 GetHandle() => Base;

    internal Inv.Database.Command NewCommand()
    {
      return new Command(this);
    }

    private Platform Platform;
    private string FilePath;
    private SQLitePCL.sqlite3 Base;

    private sealed class ForeignKey
    {
      public ForeignKey()
      {
        this.ViolateIDSet = new HashSet<int>();
      }

      public int ID { get; set; }
      public string TableName { get; set; }
      public string ColumnName { get; set; }
      public string ReferenceTableName { get; set; }

      public HashSet<int> ViolateIDSet { get; private set; }
    }
  }

  internal sealed class Transaction : Inv.Database.Transaction
  {
    internal Transaction(Connection Connection)
    {
      this.Connection = Connection;
    }

    void IDisposable.Dispose()
    {
      if (!IsRollback && !IsCommit)
        throw new Exception("Transaction has not been committed or rolled back.");
    }
    void Inv.Database.Transaction.Commit()
    {
      if (!IsBegin)
        throw new Exception("Transaction has not begun.");

      using (var Command = Connection.NewCommand())
      {
        Command.SetText("commit transaction");
        Command.ExecuteNonQuery();
      }

      this.IsCommit = true;
    }
    void Inv.Database.Transaction.Rollback()
    {
      if (!IsBegin)
        throw new Exception("Transaction has not begun.");

      using (var Command = Connection.NewCommand())
      {
        Command.SetText("rollback transaction");
        Command.ExecuteNonQuery();
      }

      this.IsRollback = true;
    }

    internal void ExecuteBegin()
    {
      if (IsBegin)
        throw new Exception("Transaction has already begun.");

      using (var Command = Connection.NewCommand())
      {
        Command.SetText("begin transaction");
        Command.ExecuteNonQuery();
      }

      this.IsBegin = true;
    }

    private Connection Connection;
    private bool IsBegin;
    private bool IsCommit;
    private bool IsRollback;
  }

  internal sealed class Command : Inv.Database.Command
  {
    internal Command(Connection Connection)
    {
      this.Connection = Connection;
      this.SetParameterActionList = new Inv.DistinctList<Action>();
    }

    void IDisposable.Dispose()
    {
      if (Base != null)
        Finalise();
    }
    Inv.Database.Reader Inv.Database.Command.ExecuteReader()
    {
      Prepare();

      return new Reader(Base);
    }
    int Inv.Database.Command.ExecuteNonQuery()
    {
      Prepare();

      var Result = SQLitePCL.raw.sqlite3_step(Base);

      Platform.CheckResult(Connection.GetHandle(), "ExecuteNonQuery", Result, SQLitePCL.raw.SQLITE_DONE);

      return SQLitePCL.raw.sqlite3_changes(Connection.GetHandle());
    }
    void Inv.Database.Command.SetParameter(string Name, int Index, Inv.Database.ColumnType ColumnType, object Value)
    {
      SetParameterActionList.Add(() =>
      {
        // Note: SQLite parameters are 1-based.
        var ActualIndex = Index + 1;

        if (Value == null)
          Platform.CheckResult(Connection.GetHandle(), "SetParameter(null)", SQLitePCL.raw.sqlite3_bind_null(Base, ActualIndex));
        else if (Value is bool)
          Platform.CheckResult(Connection.GetHandle(), "SetParameter(int)", SQLitePCL.raw.sqlite3_bind_int(Base, ActualIndex, (bool)Value ? 1 : 0));
        else if (Value is byte || Value is int || Value is Enum)
          Platform.CheckResult(Connection.GetHandle(), "SetParameter(int)", SQLitePCL.raw.sqlite3_bind_int(Base, ActualIndex, (int)Value));
        else if (Value is long)
          BindInteger64(ActualIndex, (long)Value);
        else if (Value is Inv.Date)
          BindText(ActualIndex, ((Inv.Date)Value).ToString(Protocol.DateFormat, System.Globalization.CultureInfo.InvariantCulture));
        else if (Value is DateTimeOffset)
          BindText(ActualIndex, ((DateTimeOffset)Value).ToString(Protocol.DateTimeOffsetFormat, System.Globalization.CultureInfo.InvariantCulture));
        else if (Value is DateTime)
          BindText(ActualIndex, ((DateTime)Value).ToString(Protocol.DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture));
        else if (Value is decimal)
          BindDecimal(ActualIndex, (decimal)Value);
        else if (Value is double || Value is float)
          BindDouble(ActualIndex, (double)Value);
        else if (Value is Guid)
          BindText(ActualIndex, ((Guid)Value).ToString());
        else if (Value is Inv.Image)
          BindBlob(ActualIndex, ((Inv.Image)Value).GetBuffer());
        else if (Value is Inv.Money)
          BindDecimal(ActualIndex, ((Inv.Money)Value).GetAmount());
        else if (Value is string)
          BindText(ActualIndex, (string)Value);
        else if (Value is Inv.Time)
          BindText(ActualIndex, ((Inv.Time)Value).ToString(Protocol.TimeFormat));
        else if (Value is TimeSpan)
          BindTimeSpan(ActualIndex, (TimeSpan)Value);
      });
    }
    void Inv.Database.Command.SetText(string Text)
    {
      this.CommandText = Text;
    }
    void Inv.Database.Command.SetTimeout(int Seconds)
    {
      // Not supported.
    }

    private void Prepare()
    {
      Debug.Assert(Base == null, "Prepare should not be called when Base != null");

      if (Base != null)
        Finalise();

      Platform.CheckResult(Connection.GetHandle(), "Prepare", SQLitePCL.raw.sqlite3_prepare_v2(Connection.GetHandle(), CommandText, out Base));

      foreach (var SetParameterAction in SetParameterActionList)
        SetParameterAction();
    }
    private void Finalise()
    {
      Debug.Assert(Base != null, "Finalise should not be called when Base = null");

      if (Base != null)
      {
        Platform.CheckResult(Connection.GetHandle(), "Finalise", SQLitePCL.raw.sqlite3_finalize(Base));

        Base.Dispose();
        Base = null;
      }
    }
    private void BindBlob(int Index, byte[] Value)
    {
      Platform.CheckResult(Connection.GetHandle(), "BindBlob", SQLitePCL.raw.sqlite3_bind_blob(Base, Index, Value));
    }
    private void BindDecimal(int Index, decimal Value)
    {
      BindText(Index, Value.ToString());
    }
    private void BindDouble(int Index, double Value)
    {
      Platform.CheckResult(Connection.GetHandle(), "BindDouble", SQLitePCL.raw.sqlite3_bind_double(Base, Index, Value));
    }
    private void BindInteger64(int Index, long Value)
    {
      Platform.CheckResult(Connection.GetHandle(), "BindInteger64", SQLitePCL.raw.sqlite3_bind_int64(Base, Index, Value));
    }
    private void BindText(int Index, string Value)
    {
      Platform.CheckResult(Connection.GetHandle(), "BindText", SQLitePCL.raw.sqlite3_bind_text(Base, Index, Value));
    }
    private void BindTimeSpan(int Index, TimeSpan Value)
    {
      BindInteger64(Index, Value.Ticks);
    }

    private Connection Connection;
    private string CommandText;
    private SQLitePCL.sqlite3_stmt Base;
    private Inv.DistinctList<Action> SetParameterActionList;
  }

  internal sealed class Reader : Inv.Database.Reader
  {
    internal Reader(SQLitePCL.sqlite3_stmt Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      // TODO: Do nothing. Base will be disposed by the command object.
    }
    bool Database.Reader.Read()
    {
      if (LastResult != null && LastResult == SQLitePCL.raw.SQLITE_DONE)
        return false;

      this.LastResult = SQLitePCL.raw.sqlite3_step(Base);

      return LastResult == SQLitePCL.raw.SQLITE_ROW;
    }
    bool Database.Reader.IsNull(int ColumnIndex)
    {
      return SQLitePCL.raw.sqlite3_column_type(Base, ColumnIndex) == SQLitePCL.raw.SQLITE_NULL;
    }
    bool Database.Reader.GetBoolean(int ColumnIndex)
    {
      return ReadInteger32(ColumnIndex) > 0;
    }
    byte Database.Reader.GetByte(int ColumnIndex)
    {
      return (byte)ReadInteger32(ColumnIndex);
    }
    Inv.Date Database.Reader.GetDate(int ColumnIndex)
    {
      return Inv.Date.ParseExact(ReadText(ColumnIndex), Protocol.DateFormat);
    }
    DateTime Database.Reader.GetDateTime(int ColumnIndex)
    {
      return DateTime.ParseExact(ReadText(ColumnIndex), Protocol.DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
    }
    DateTimeOffset Database.Reader.GetDateTimeOffset(int ColumnIndex)
    {
      return DateTimeOffset.ParseExact(ReadText(ColumnIndex), Protocol.DateTimeOffsetFormat, System.Globalization.CultureInfo.InvariantCulture);
    }
    decimal Database.Reader.GetDecimal(int ColumnIndex)
    {
      return ReadDecimal(ColumnIndex);
    }
    double Database.Reader.GetDouble(int ColumnIndex)
    {
      return SQLitePCL.raw.sqlite3_column_double(Base, ColumnIndex);
    }
    float Database.Reader.GetFloat(int ColumnIndex)
    {
      return float.Parse(ReadText(ColumnIndex));
    }
    Guid Database.Reader.GetGuid(int ColumnIndex)
    {
      return Guid.Parse(ReadText(ColumnIndex));
    }
    Inv.Image Database.Reader.GetImage(int ColumnIndex)
    {
      return new Inv.Image(SQLitePCL.raw.sqlite3_column_blob(Base, ColumnIndex), ".png");
    }
    int Inv.Database.Reader.GetInteger32(int ColumnIndex)
    {
      return ReadInteger32(ColumnIndex);
    }
    long Inv.Database.Reader.GetInteger64(int ColumnIndex)
    {
      return ReadInteger64(ColumnIndex);
    }
    Inv.Money Database.Reader.GetMoney(int ColumnIndex)
    {
      return new Inv.Money(ReadDecimal(ColumnIndex));
    }
    string Inv.Database.Reader.GetString(int ColumnIndex)
    {
      return ReadText(ColumnIndex);
    }
    Inv.Time Database.Reader.GetTime(int ColumnIndex)
    {
      return Inv.Time.ParseExact(ReadText(ColumnIndex), Protocol.TimeFormat);
    }
    TimeSpan Database.Reader.GetTimeSpan(int ColumnIndex)
    {
      return ReadTimeSpan(ColumnIndex);
    }

    private decimal ReadDecimal(int ColumnIndex)
    {
      return decimal.Parse(ReadText(ColumnIndex));
    }
    private int ReadInteger32(int ColumnIndex)
    {
      return SQLitePCL.raw.sqlite3_column_int(Base, ColumnIndex);
    }
    private long ReadInteger64(int ColumnIndex)
    {
      return SQLitePCL.raw.sqlite3_column_int64(Base, ColumnIndex);
    }
    private string ReadText(int ColumnIndex)
    {
      return SQLitePCL.raw.sqlite3_column_text(Base, ColumnIndex);
    }
    private TimeSpan ReadTimeSpan(int ColumnIndex)
    {
      return new TimeSpan(ReadInteger64(ColumnIndex));
    }

    private int? LastResult;
    private SQLitePCL.sqlite3_stmt Base;
  }

  internal static class Protocol
  {
    public static readonly string TimeFormat = "HH:mm:ss.fff";
    public static readonly string DateFormat = "yyyy-MM-dd";
    public static readonly string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
    public static readonly string DateTimeOffsetFormat = "yyyy-MM-dd HH:mm:ss.fffzzz";
  }
}
 