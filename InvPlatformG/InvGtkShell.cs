﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class GtkShell
  {
    static GtkShell()
    {
      DefaultWindowWidth = 800;
      DefaultWindowHeight = 600;

      MainFolderPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
    }

    public static int DefaultWindowWidth { get; set; }
    public static int DefaultWindowHeight { get; set; }
    public static string MainFolderPath { get; set; }

    public static void Run(Inv.Application InvApplication)
    {
      var GtkEngine = new GtkEngine(InvApplication);
      GtkEngine.Run();
    }
  }

  internal sealed class GtkPlatform : Inv.Platform
  {
    public GtkPlatform(GtkEngine Engine)
    {
      this.Engine = Engine;
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
    }

    int Platform.ThreadAffinity()
    {
      return Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      // TODO: Gtk date/time picker.
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      // TODO: mail.

      return false;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return System.IO.File.Exists(SelectAssetPath(Asset));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(SelectAssetPath(Asset), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.LocationIsSupported
    {
#if DEBUG
      get { return true; }
#else
      get { return false; }
#endif
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      // TODO: implement using web services?
      LocationLookup.SetPlacemarks(new Placemark[] { });
    }
    void Platform.AudioPlaySound(Inv.Sound Sound, float Volume, float Rate)
    {
      // TODO: play sound.
    }
    void Platform.WindowBrowse(Inv.File File)
    {
    }
    void Platform.WindowPost(Action Action)
    {
      Engine.Asynchronise(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      CurrentProcess.Refresh();
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      Engine.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }

    private string SelectAssetPath(Asset Asset)
    {
      return Path.Combine(Inv.GtkShell.MainFolderPath, "Assets", Asset.Name);
    }
    private string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(GtkShell.MainFolderPath, Folder.Name);
      else
        Result = GtkShell.MainFolderPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private GtkEngine Engine;
    private System.Diagnostics.Process CurrentProcess;


    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      throw new NotImplementedException();
    }

    void Platform.WindowCall(Action Action)
    {
      throw new NotImplementedException();
    }

    void Platform.WebServerConnect(WebServer Server)
    {
      throw new NotImplementedException();
    }

    void Platform.WebServerDisconnect(WebServer Server)
    {
      throw new NotImplementedException();
    }
  }

  internal sealed class GtkEngine
  {
    public GtkEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Gdk.Key, Inv.Key>()
      {
        { Gdk.Key.Key_0, Inv.Key.n0 },
        { Gdk.Key.Key_1, Inv.Key.n1 },
        { Gdk.Key.Key_2, Inv.Key.n2 },
        { Gdk.Key.Key_3, Inv.Key.n3 },
        { Gdk.Key.Key_4, Inv.Key.n4 },
        { Gdk.Key.Key_5, Inv.Key.n5 },
        { Gdk.Key.Key_6, Inv.Key.n6 },
        { Gdk.Key.Key_7, Inv.Key.n7 },
        { Gdk.Key.Key_8, Inv.Key.n8 },
        { Gdk.Key.Key_9, Inv.Key.n9 },
        { Gdk.Key.A, Inv.Key.A },
        { Gdk.Key.B, Inv.Key.B },
        { Gdk.Key.C, Inv.Key.C },
        { Gdk.Key.D, Inv.Key.D },
        { Gdk.Key.E, Inv.Key.E },
        { Gdk.Key.F, Inv.Key.F },
        { Gdk.Key.G, Inv.Key.G },
        { Gdk.Key.H, Inv.Key.H },
        { Gdk.Key.I, Inv.Key.I },
        { Gdk.Key.J, Inv.Key.J },
        { Gdk.Key.K, Inv.Key.K },
        { Gdk.Key.L, Inv.Key.L },
        { Gdk.Key.M, Inv.Key.M },
        { Gdk.Key.N, Inv.Key.N },
        { Gdk.Key.O, Inv.Key.O },
        { Gdk.Key.P, Inv.Key.P },
        { Gdk.Key.Q, Inv.Key.Q },
        { Gdk.Key.R, Inv.Key.R },
        { Gdk.Key.S, Inv.Key.S },
        { Gdk.Key.T, Inv.Key.T },
        { Gdk.Key.U, Inv.Key.U },
        { Gdk.Key.V, Inv.Key.V },
        { Gdk.Key.W, Inv.Key.W },
        { Gdk.Key.X, Inv.Key.X },
        { Gdk.Key.Y, Inv.Key.Y },
        { Gdk.Key.Z, Inv.Key.Z },
        { Gdk.Key.a, Inv.Key.A },
        { Gdk.Key.b, Inv.Key.B },
        { Gdk.Key.c, Inv.Key.C },
        { Gdk.Key.d, Inv.Key.D },
        { Gdk.Key.e, Inv.Key.E },
        { Gdk.Key.f, Inv.Key.F },
        { Gdk.Key.g, Inv.Key.G },
        { Gdk.Key.h, Inv.Key.H },
        { Gdk.Key.i, Inv.Key.I },
        { Gdk.Key.j, Inv.Key.J },
        { Gdk.Key.k, Inv.Key.K },
        { Gdk.Key.l, Inv.Key.L },
        { Gdk.Key.m, Inv.Key.M },
        { Gdk.Key.n, Inv.Key.N },
        { Gdk.Key.o, Inv.Key.O },
        { Gdk.Key.p, Inv.Key.P },
        { Gdk.Key.q, Inv.Key.Q },
        { Gdk.Key.r, Inv.Key.R },
        { Gdk.Key.s, Inv.Key.S },
        { Gdk.Key.t, Inv.Key.T },
        { Gdk.Key.u, Inv.Key.U },
        { Gdk.Key.v, Inv.Key.V },
        { Gdk.Key.w, Inv.Key.W },
        { Gdk.Key.x, Inv.Key.X },
        { Gdk.Key.y, Inv.Key.Y },
        { Gdk.Key.z, Inv.Key.Z },
        { Gdk.Key.F1, Inv.Key.F1 },
        { Gdk.Key.F2, Inv.Key.F2 },
        { Gdk.Key.F3, Inv.Key.F3 },
        { Gdk.Key.F4, Inv.Key.F4 },
        { Gdk.Key.F5, Inv.Key.F5 },
        { Gdk.Key.F6, Inv.Key.F6 },
        { Gdk.Key.F7, Inv.Key.F7 },
        { Gdk.Key.F8, Inv.Key.F8 },
        { Gdk.Key.F9, Inv.Key.F9 },
        { Gdk.Key.F10, Inv.Key.F10 },
        { Gdk.Key.F11, Inv.Key.F11 },
        { Gdk.Key.F12, Inv.Key.F12 },
        { Gdk.Key.Escape, Inv.Key.Escape },
        { Gdk.Key.ISO_Enter, Inv.Key.Enter },
        { Gdk.Key.Tab, Inv.Key.Tab },
        { Gdk.Key.space, Inv.Key.Space },
        { Gdk.Key.period, Inv.Key.Period },
        { Gdk.Key.comma, Inv.Key.Comma },
        { Gdk.Key.dead_tilde, Inv.Key.Tilde },
        //{ Gdk.Key.OemTilde, Inv.Key.BackQuote }, // TODO: plus shift.
        { Gdk.Key.plus, Inv.Key.Plus },
        { Gdk.Key.minus, Inv.Key.Minus },
        { Gdk.Key.Up, Inv.Key.Up },
        { Gdk.Key.Down, Inv.Key.Down },
        { Gdk.Key.Left, Inv.Key.Left },
        { Gdk.Key.Right, Inv.Key.Right },
        { Gdk.Key.Home, Inv.Key.Home },
        { Gdk.Key.End, Inv.Key.End },
        { Gdk.Key.Page_Up, Inv.Key.PageUp },
        { Gdk.Key.Page_Down, Inv.Key.PageDown },
        { Gdk.Key.Clear, Inv.Key.Clear },
        { Gdk.Key.Insert, Inv.Key.Insert },
        { Gdk.Key.Delete, Inv.Key.Delete },
        { Gdk.Key.slash, Inv.Key.Slash },
        { Gdk.Key.backslash, Inv.Key.Backslash },
        { Gdk.Key.division, Inv.Key.Slash },
        { Gdk.Key.multiply, Inv.Key.Asterisk },
      };
      #endregion

      this.RouteDictionary = new Dictionary<Type, Func<Panel, GtkControl>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Board), TranslateBoard },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        //{ typeof(Inv.Table), TranslateTable },
      };
    }

    public void Run()
    {
      using (var SingletonMutex = InvApplication.Title != null ? new GtkMutex(InvApplication.Title) : null)
      {
        var SingletonBound = SingletonMutex == null;

        if (!SingletonBound)
        {
          var SingletonAttempt = 0;
          while (SingletonAttempt < 10)
          {
            SingletonBound = SingletonMutex.Lock(TimeSpan.FromMilliseconds(500));

            if (SingletonBound)
            {
              break; // We control the mutex, can continue as the singleton process.
            }
            else
            {
              SingletonAttempt++;
            }
          }
        }

        if (SingletonBound)
        {
          Gtk.Application.Init();

          this.GtkWindow = new GtkWindow();
          GtkWindow.WidthRequest = GtkShell.DefaultWindowWidth;
          GtkWindow.HeightRequest = GtkShell.DefaultWindowHeight;
          GtkWindow.ResizeChecked += (Sender, Event) =>
          {
            ProcessResize();
          };
          GtkWindow.KeyPressEvent += (Sender, Event) =>
          {
            var InvSurface = InvApplication.Window.ActiveSurface;

            if (InvSurface != null)
            {
              var InvKey = TranslateKey(Event.Event.Key);
              if (InvKey != null)
              {
                var Keystroke = new Keystroke()
                {
                  Key = InvKey.Value,
                  Modifier = GetModifier()
                };
                InvSurface.KeystrokeInvoke(Keystroke);
              }
            }
          };
          GtkWindow.Shown += (Sender, Event) =>
          {
          };
          GtkWindow.Hidden += (Sender, Event) =>
          {
            Gtk.Application.Quit();
          };
          GtkWindow.Show();

          InvApplication.SetPlatform(new GtkPlatform(this));

          InvApplication.StartInvoke();
          try
          {
            GtkWindow.Title = InvApplication.Title;

            ProcessResize();

            Process();

            // TODO: hook up rendering event.

            Gtk.Application.Run();
          }
          finally
          {
            InvApplication.StopInvoke();
          }
        }
      }
    }

    internal KeyModifier GetModifier()
    {
      // TODO: keyboard modifier.

      return new KeyModifier();
    }
    internal void Reclamation()
    {
      // TODO: low memory cleanup.
    }
    internal void Asynchronise(Action Action)
    {
      Gtk.Application.Invoke((Sender, Event) => Action()); // TODO: is this async?

      //GtkContext.Post((State) => Action(), null);
    }

    private void Process()
    {
      if (InvApplication.IsExit)
      {
        GtkWindow.Hide();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet)
          {
            var GtkTimer = AccessTimer(InvTimer, S => new GtkTimer(S.IntervalInvoke));

            if (GtkTimer.IntervalTime != InvTimer.IntervalTime || InvTimer.IsEnabled != GtkTimer.IsEnabled)
              GtkTimer.Set(InvTimer.IsEnabled, InvTimer.IntervalTime);
          }

          InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
        }

        var InvSurfaceActive = InvWindow.ActiveSurface;

        if (InvSurfaceActive != null)
        {
          var GtkSurface = AccessSurface(InvSurfaceActive, S =>
          {
            var Result = new GtkSurface();
            return Result;
          });

          if (GtkWindow.Child != (Gtk.Widget)GtkSurface)
            InvSurfaceActive.ArrangeInvoke();

          ProcessTransition(GtkSurface);

          InvSurfaceActive.ComposeInvoke();

          if (InvSurfaceActive.Render())
          {
            TranslateBackground(InvSurfaceActive.Background, GtkSurface);
            GtkSurface.Child = TranslatePanel(InvSurfaceActive.Content);
          }

          InvSurfaceActive.ProcessChanges(P => TranslatePanel(P));

          if (InvSurfaceActive.Focus != null)
          {
            // TODO: focus.
          }

          // TODO: animation.
          //if (InvSurfaceActive != null)
          //  ProcessAnimation(InvSurfaceActive);

          if (InvWindow.Render())
            TranslateBackground(InvWindow.Background, GtkWindow);

          InvWindow.DisplayRate.Calculate();
        }
      }
    }
    private void ProcessTransition(GtkSurface GtkSurface)
    {
      GtkWindow.Child = GtkSurface;
    }
    private void ProcessResize()
    {
      InvApplication.Window.Width = GtkWindow.WidthRequest;
      InvApplication.Window.Height = GtkWindow.HeightRequest;
    }

    private GtkControl TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel);
    }
    private GtkControl TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var GtkButton = AccessPanel(InvButton, P =>
      {
        var Result = new GtkButton();
        Result.Button.Clicked += (Sender, Event) =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
            P.SingleTapInvoke();

          // TODO: right click.
          //if (InvApplication.Window.IsActiveSurface(P.Surface))
          //  P.ContextTapInvoke();
        };
        return Result;
      });

      RenderPanel(InvButton, GtkButton, () =>
      {
        TranslateCornerRadius(InvButton.CornerRadius, GtkButton);
        TranslateBackground(InvButton.Background, GtkButton);
        TranslateVisibility(InvButton.Visibility, GtkButton);
        TranslateBorder(InvButton.Border, GtkButton);
        TranslateMargin(InvButton.Margin, GtkButton);
        TranslatePadding(InvButton.Padding, GtkButton);
        TranslateOpacity(InvButton.Opacity, GtkButton);
        TranslateAlignment(InvButton.Alignment, GtkButton);
        TranslateSize(InvButton.Size, GtkButton);
        TranslateElevation(InvButton.Elevation, GtkButton);

        //GtkButton.IsEnabled = InvButton.IsEnabled;
        //GtkButton.Focusable = InvButton.IsFocusable;

        if (InvButton.ContentSingleton.Render())
        {
          GtkButton.Button.Child = null; // detach previous content in case it has moved down the visual tree.
          GtkButton.Button.Child = TranslatePanel(InvButton.ContentSingleton.Data);
        }
      });

      return GtkButton;
    }
    private GtkControl TranslateBoard(Inv.Panel InvPanel)
    {
      var InvBoard = (Inv.Board)InvPanel;

      var GtkBoard = AccessPanel(InvBoard, P =>
      {
        return new GtkBoard();
      });

      RenderPanel(InvBoard, GtkBoard, () =>
      {
        TranslateBackground(InvBoard.Background, GtkBoard);
        TranslateCornerRadius(InvBoard.CornerRadius, GtkBoard);
        TranslateMargin(InvBoard.Margin, GtkBoard);
        TranslatePadding(InvBoard.Padding, GtkBoard);
        TranslateBorder(InvBoard.Border, GtkBoard);
        TranslateOpacity(InvBoard.Opacity, GtkBoard);
        TranslateAlignment(InvBoard.Alignment, GtkBoard);
        TranslateVisibility(InvBoard.Visibility, GtkBoard);
        TranslateSize(InvBoard.Size, GtkBoard);
        TranslateElevation(InvBoard.Elevation, GtkBoard);

        if (InvBoard.PinCollection.Render())
        {
          foreach (var Child in GtkBoard.Fixed.Children)
            GtkBoard.Fixed.Remove(Child);

          foreach (var InvElement in InvBoard.PinCollection)
          {
            var GtkElement = new Gtk.EventBox();
            GtkElement.SetSizeRequest(InvElement.Rect.Width, InvElement.Rect.Height);
            GtkElement.Show();

            var GtkPanel = TranslatePanel(InvElement.Panel);
            GtkElement.Child = GtkPanel;

            GtkBoard.Fixed.Put(GtkElement, InvElement.Rect.Left, InvElement.Rect.Top);
          }
        }
      });

      return GtkBoard;
    }
    private GtkControl TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var GtkDock = AccessPanel(InvDock, P =>
      {
        var Result = new GtkDock(P.Orientation == DockOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvDock, GtkDock, () =>
      {
        TranslateBackground(InvDock.Background, GtkDock);
        TranslateCornerRadius(InvDock.CornerRadius, GtkDock);
        TranslateBorder(InvDock.Border, GtkDock);
        TranslateMargin(InvDock.Margin, GtkDock);
        TranslatePadding(InvDock.Padding, GtkDock);
        TranslateOpacity(InvDock.Opacity, GtkDock);
        TranslateAlignment(InvDock.Alignment, GtkDock);
        TranslateVisibility(InvDock.Visibility, GtkDock);
        TranslateSize(InvDock.Size, GtkDock);
        TranslateElevation(InvDock.Elevation, GtkDock);

        if (InvDock.CollectionRender())
          GtkDock.Compose(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(H => TranslatePanel(H)), InvDock.FooterCollection.Select(H => TranslatePanel(H)).Reverse());
      });

      return GtkDock;
    }
    private GtkControl TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var GtkEdit = AccessPanel(InvEdit, P =>
      {
        var Result = new GtkEdit();
        Result.Entry.Changed += (Sender, Event) => P.ChangeText(Result.Entry.Text);
        return Result;
      });

      RenderPanel(InvEdit, GtkEdit, () =>
      {
        if (InvEdit.Background.Render())
        {
          ExecuteBackground(InvEdit.Background, GtkEdit);
          GtkEdit.Entry.OverrideBackgroundColor(Gtk.StateFlags.Normal, TranslateColour(InvEdit.Background.Colour));
        }

        TranslateCornerRadius(InvEdit.CornerRadius, GtkEdit);
        TranslateBorder(InvEdit.Border, GtkEdit);
        TranslateMargin(InvEdit.Margin, GtkEdit);
        TranslatePadding(InvEdit.Padding, GtkEdit);
        TranslateOpacity(InvEdit.Opacity, GtkEdit);
        TranslateAlignment(InvEdit.Alignment, GtkEdit);
        TranslateVisibility(InvEdit.Visibility, GtkEdit);
        TranslateSize(InvEdit.Size, GtkEdit);
        TranslateElevation(InvEdit.Elevation, GtkEdit);
        TranslateFont(InvEdit.Font, GtkEdit.Entry);

        GtkEdit.Entry.IsEditable = !InvEdit.IsReadOnly;
        GtkEdit.Entry.Text = InvEdit.Text;
      });

      return GtkEdit;
    }
    private GtkControl TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var GtkFrame = AccessPanel(InvFrame, P =>
      {
        var Result = new GtkFrame();
        return Result;
      });

      RenderPanel(InvFrame, GtkFrame, () =>
      {
        TranslateBackground(InvFrame.Background, GtkFrame);
        TranslateCornerRadius(InvFrame.CornerRadius, GtkFrame);
        TranslateBorder(InvFrame.Border, GtkFrame);
        TranslateMargin(InvFrame.Margin, GtkFrame);
        TranslatePadding(InvFrame.Padding, GtkFrame);
        TranslateOpacity(InvFrame.Opacity, GtkFrame);
        TranslateAlignment(InvFrame.Alignment, GtkFrame);
        TranslateVisibility(InvFrame.Visibility, GtkFrame);
        TranslateSize(InvFrame.Size, GtkFrame);
        TranslateElevation(InvFrame.Elevation, GtkFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          GtkFrame.Child = null; // detach previous content in case it has moved.
          GtkFrame.Child = TranslatePanel(InvFrame.ContentSingleton.Data);
        }
      });

      return GtkFrame;
    }
    private GtkControl TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var GtkGraphic = AccessPanel(InvGraphic, P =>
      {
        var Result = new GtkGraphic();
        return Result;
      });

      RenderPanel(InvGraphic, GtkGraphic, () =>
      {
        TranslateCornerRadius(InvGraphic.CornerRadius, GtkGraphic);
        TranslateBackground(InvGraphic.Background, GtkGraphic);
        TranslateBorder(InvGraphic.Border, GtkGraphic);
        TranslateMargin(InvGraphic.Margin, GtkGraphic);
        TranslatePadding(InvGraphic.Padding, GtkGraphic);
        TranslateOpacity(InvGraphic.Opacity, GtkGraphic);
        TranslateAlignment(InvGraphic.Alignment, GtkGraphic);
        TranslateVisibility(InvGraphic.Visibility, GtkGraphic);
        TranslateSize(InvGraphic.Size, GtkGraphic);

        TranslateElevation(InvGraphic.Elevation, GtkGraphic);

        if (InvGraphic.ImageSingleton.Render())
        {
          if (InvGraphic.ImageSingleton.Data == null)
          {
            GtkGraphic.Image.Pixbuf = null;
          }
          else
          {
            using (var MS = new MemoryStream(InvGraphic.ImageSingleton.Data.GetBuffer()))
              GtkGraphic.Image.Pixbuf = new Gdk.Pixbuf(MS);

            GtkGraphic.Image.Pixbuf = GtkGraphic.Image.Pixbuf.ScaleSimple(InvGraphic.Size.Width.Value, InvGraphic.Size.Height.Value, Gdk.InterpType.Hyper);
          }
        }
      });

      return GtkGraphic;
    }
    private GtkControl TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var GtkLabel = AccessPanel(InvLabel, P =>
      {
        var Result = new GtkLabel();
        return Result;
      });

      RenderPanel(InvLabel, GtkLabel, () =>
      {
        TranslateBackground(InvLabel.Background, GtkLabel);
        TranslateCornerRadius(InvLabel.CornerRadius, GtkLabel);
        TranslateBorder(InvLabel.Border, GtkLabel);
        TranslateMargin(InvLabel.Margin, GtkLabel);
        TranslatePadding(InvLabel.Padding, GtkLabel);
        TranslateOpacity(InvLabel.Opacity, GtkLabel);
        TranslateAlignment(InvLabel.Alignment, GtkLabel);
        TranslateVisibility(InvLabel.Visibility, GtkLabel);
        TranslateSize(InvLabel.Size, GtkLabel);
        TranslateElevation(InvLabel.Elevation, GtkLabel);
        TranslateFont(InvLabel.Font, GtkLabel.Label);

        //GtkLabel.TextWrapping = InvLabel.LineWrapping ? System.Windows.TextWrapping.Wrap : System.Windows.TextWrapping.NoWrap;
        //GtkLabel.TextAlignment = InvLabel.Justification == null || InvLabel.Justification == Justification.Left ? System.Windows.TextAlignment.Left : InvLabel.Justification == Justification.Right ? System.Windows.TextAlignment.Right : System.Windows.TextAlignment.Center;
        GtkLabel.Label.Text = InvLabel.Text;
      });

      return GtkLabel;
    }
    private GtkControl TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var GtkMemo = AccessPanel(InvMemo, P =>
      {
        var Result = new GtkMemo();
        Result.TextView.Buffer.Changed += (Sender, Event) => P.ChangeText(Result.TextView.Buffer.Text);
        return Result;
      });

      RenderPanel(InvMemo, GtkMemo, () =>
      {
        if (InvMemo.Background.Render())
        {
          ExecuteBackground(InvMemo.Background, GtkMemo);

          GtkMemo.TextView.OverrideBackgroundColor(Gtk.StateFlags.Normal, TranslateColour(InvMemo.Background.Colour));
        }

        TranslateCornerRadius(InvMemo.CornerRadius, GtkMemo);
        TranslateBorder(InvMemo.Border, GtkMemo);
        TranslateMargin(InvMemo.Margin, GtkMemo);
        TranslatePadding(InvMemo.Padding, GtkMemo);
        TranslateOpacity(InvMemo.Opacity, GtkMemo);
        TranslateAlignment(InvMemo.Alignment, GtkMemo);
        TranslateVisibility(InvMemo.Visibility, GtkMemo);
        TranslateSize(InvMemo.Size, GtkMemo);
        TranslateElevation(InvMemo.Elevation, GtkMemo);
        TranslateFont(InvMemo.Font, GtkMemo.TextView);

        GtkMemo.TextView.Editable = !InvMemo.IsReadOnly;
        GtkMemo.TextView.Buffer.Text = InvMemo.Text;
      });

      return GtkMemo;
    }
    private GtkControl TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var GtkOverlay = AccessPanel(InvOverlay, P =>
      {
        var Result = new GtkOverlay();
        return Result;
      });

      RenderPanel(InvOverlay, GtkOverlay, () =>
      {
        TranslateBackground(InvOverlay.Background, GtkOverlay);
        TranslateCornerRadius(InvOverlay.CornerRadius, GtkOverlay);
        TranslateBorder(InvOverlay.Border, GtkOverlay);
        TranslateMargin(InvOverlay.Margin, GtkOverlay);
        TranslatePadding(InvOverlay.Padding, GtkOverlay);
        TranslateOpacity(InvOverlay.Opacity, GtkOverlay);
        TranslateAlignment(InvOverlay.Alignment, GtkOverlay);
        TranslateVisibility(InvOverlay.Visibility, GtkOverlay);
        TranslateSize(InvOverlay.Size, GtkOverlay);
        TranslateElevation(InvOverlay.Elevation, GtkOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          foreach (var Child in GtkOverlay.Fixed.Children)
            GtkOverlay.Fixed.Remove(Child);

          foreach (var InvElement in InvOverlay.GetPanels())
          {
            var GtkElement = TranslatePanel(InvElement);
            GtkOverlay.Fixed.Add(GtkElement);
          }
        }
      });

      return GtkOverlay;
    }
    private GtkControl TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var GtkCanvas = AccessPanel(InvCanvas, P =>
      {
        var Result = new GtkCanvas();

        return Result;
      });

      RenderPanel(InvCanvas, GtkCanvas, () =>
      {
        TranslateBackground(InvCanvas.Background, GtkCanvas);
        TranslateCornerRadius(InvCanvas.CornerRadius, GtkCanvas);
        TranslateMargin(InvCanvas.Margin, GtkCanvas);
        TranslatePadding(InvCanvas.Padding, GtkCanvas);
        TranslateBorder(InvCanvas.Border, GtkCanvas);
        TranslateOpacity(InvCanvas.Opacity, GtkCanvas);
        TranslateAlignment(InvCanvas.Alignment, GtkCanvas);
        TranslateVisibility(InvCanvas.Visibility, GtkCanvas);
        TranslateSize(InvCanvas.Size, GtkCanvas);
        TranslateElevation(InvCanvas.Elevation, GtkCanvas);

        if (InvCanvas.Redrawing)
        {
          InvCanvas.DrawInvoke(null);
        }
      });

      return GtkCanvas;
    }
    private GtkControl TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var GtkScroll = AccessPanel(InvScroll, P =>
      {
        var Result = new GtkScroll();
        Result.ScrolledWindow.HScrollbar.Visible = P.Orientation == ScrollOrientation.Horizontal;
        Result.ScrolledWindow.VScrollbar.Visible = P.Orientation == ScrollOrientation.Vertical;
        return Result;
      });

      RenderPanel(InvScroll, GtkScroll, () =>
      {
        TranslateBackground(InvScroll.Background, GtkScroll);
        TranslateCornerRadius(InvScroll.CornerRadius, GtkScroll);
        TranslateBorder(InvScroll.Border, GtkScroll);
        TranslateMargin(InvScroll.Margin, GtkScroll);
        TranslatePadding(InvScroll.Padding, GtkScroll);
        TranslateOpacity(InvScroll.Opacity, GtkScroll);
        TranslateAlignment(InvScroll.Alignment, GtkScroll);
        TranslateVisibility(InvScroll.Visibility, GtkScroll);
        TranslateSize(InvScroll.Size, GtkScroll);
        TranslateElevation(InvScroll.Elevation, GtkScroll);

        if (InvScroll.ContentSingleton.Render())
          GtkScroll.ScrolledWindow.Child = TranslatePanel(InvScroll.ContentSingleton.Data);
      });

      return GtkScroll;
    }
    private GtkControl TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var GtkStack = AccessPanel(InvStack, P =>
      {
        var Result = new GtkStack(P.Orientation == StackOrientation.Horizontal);
        return Result;
      });

      RenderPanel(InvStack, GtkStack, () =>
      {
        TranslateBackground(InvStack.Background, GtkStack);
        TranslateCornerRadius(InvStack.CornerRadius, GtkStack);
        TranslateBorder(InvStack.Border, GtkStack);
        TranslateMargin(InvStack.Margin, GtkStack);
        TranslatePadding(InvStack.Padding, GtkStack);
        TranslateOpacity(InvStack.Opacity, GtkStack);
        TranslateAlignment(InvStack.Alignment, GtkStack);
        TranslateVisibility(InvStack.Visibility, GtkStack);
        TranslateSize(InvStack.Size, GtkStack);
        TranslateElevation(InvStack.Elevation, GtkStack);

        if (InvStack.PanelCollection.Render())
          GtkStack.Compose(InvStack.GetPanels().Select(P => TranslatePanel(P)));
      });

      return GtkStack;
    }

    // NOTE: Gdk.RGBA requires Gtk# 3.0 installed.
    private Gdk.RGBA TranslateColour(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return Gdk.RGBA.Zero;

      var Record = InvColour.GetARGBRecord();

      return new Gdk.RGBA()
      {
        Alpha = Record.A / 255.0,
        Red = Record.R / 255.0,
        Green = Record.G / 255.0,
        Blue = Record.B / 255.0
      };
    }
    private GtkTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, GtkTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (GtkTimer)InvTimer.Node;
      }
    }
    private GtkSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, GtkSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (GtkSurface)InvSurface.Node;
      }
    }
    private void TranslateBackground(Inv.Background InvBackground, GtkControl GtkControl)
    {
      if (InvBackground.Render())
        ExecuteBackground(InvBackground, GtkControl);
    }
    private void ExecuteBackground(Inv.Background InvBackground, GtkControl GtkControl)
    {
      GtkControl.Background.OverrideBackgroundColor(Gtk.StateFlags.Normal, TranslateColour(InvBackground.Colour));
    }
    private void TranslateBackground(Inv.Background InvBackground, Gtk.Widget GtkWidget)
    {
      if (InvBackground.Render())
        GtkWidget.OverrideBackgroundColor(Gtk.StateFlags.Normal, TranslateColour(InvBackground.Colour));
    }
    private void TranslateVisibility(Inv.Visibility InvVisibility, Gtk.Widget GtkWidget)
    {
      if (InvVisibility.Render())
        GtkWidget.Visible = InvVisibility.Get();
    }
    private void TranslateAlignment(Alignment InvAlignment, Gtk.Alignment GtkAlignment)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Inv.Placement.Stretch:
            GtkAlignment.Set(0.0F, 0.0F, 1.0F, 1.0F);
            break;

          case Inv.Placement.StretchLeft:
            GtkAlignment.Set(0.0F, 0.0F, 0.0F, 1.0F);
            break;

          case Inv.Placement.StretchCenter:
            GtkAlignment.Set(0.5F, 0.0F, 0.0F, 1.0F);
            break;

          case Inv.Placement.StretchRight:
            GtkAlignment.Set(1.0F, 0.0F, 0.0F, 1.0F);
            break;

          case Inv.Placement.TopStretch:
            GtkAlignment.Set(0.0F, 0.0F, 1.0F, 0.0F);
            break;

          case Inv.Placement.TopLeft:
            GtkAlignment.Set(0.0F, 0.0F, 0.0F, 0.0F);
            break;

          case Inv.Placement.TopCenter:
            GtkAlignment.Set(0.5F, 0.0F, 0.0F, 0.0F);
            break;

          case Inv.Placement.TopRight:
            GtkAlignment.Set(1.0F, 0.0F, 0.0F, 0.0F);
            break;

          case Inv.Placement.CenterStretch:
            GtkAlignment.Set(0.0F, 0.5F, 1.0F, 0.0F);
            break;

          case Inv.Placement.CenterLeft:
            GtkAlignment.Set(0.0F, 0.5F, 0.0F, 0.0F);
            break;

          case Inv.Placement.Center:
            GtkAlignment.Set(0.5F, 0.5F, 0.0F, 0.0F);
            break;

          case Inv.Placement.CenterRight:
            GtkAlignment.Set(1.0F, 0.5F, 0.0F, 0.0F);
            break;

          case Inv.Placement.BottomStretch:
            GtkAlignment.Set(0.0F, 1.0F, 1.0F, 0.0F);
            break;

          case Inv.Placement.BottomLeft:
            GtkAlignment.Set(0.0F, 1.0F, 0.0F, 0.0F);
            break;

          case Inv.Placement.BottomCenter:
            GtkAlignment.Set(0.5F, 1.0F, 0.0F, 0.0F);
            break;

          case Inv.Placement.BottomRight:
            GtkAlignment.Set(1.0F, 1.0F, 0.0F, 0.0F);
            break;

         default:
            throw new ApplicationException("Inv.Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateBorder(Inv.Border InvBorder, GtkControl GtkControl)
    {
      if (InvBorder.Render())
      {
        GtkControl.Border.Alignment.SetPadding((uint)InvBorder.Top, (uint)InvBorder.Bottom, (uint)InvBorder.Left, (uint)InvBorder.Right);

        GtkControl.Border.OverrideBackgroundColor(Gtk.StateFlags.Normal, TranslateColour(InvBorder.Colour));
      }
    }
    private void TranslateCornerRadius(Inv.CornerRadius InvCornerRadius, GtkControl GtkWidget)
    {
      if (InvCornerRadius.Render())
      {
        // TODO: corner radius.
      }
    }
    private void TranslateMargin(Inv.Edge InvEdge, GtkControl GtkControl)
    {
      if (InvEdge.Render())
        GtkControl.SetPadding((uint)InvEdge.Top, (uint)InvEdge.Bottom, (uint)InvEdge.Left, (uint)InvEdge.Right);
    }
    private void TranslatePadding(Inv.Edge InvEdge, GtkControl GtkControl)
    {
      if (InvEdge.Render())
      {
        GtkControl.Background.MarginTop = InvEdge.Top;
        GtkControl.Background.MarginBottom = InvEdge.Bottom;
        GtkControl.Background.MarginLeft = InvEdge.Left;
        GtkControl.Background.MarginRight = InvEdge.Right;
      }
    }
    private void TranslateElevation(Inv.Elevation InvElevation, Gtk.Widget GtkWidget)
    {
      if (InvElevation.Render())
      {
        if (InvElevation.Get() == 0)
        {
          // clear.
        }
        else
        {
          // shadows.
        }
      }
    }
    private void TranslateFont(Inv.Font InvFont, Gtk.Widget GtkWidget)
    {
      if (InvFont.Render())
      {
        var FontDesc = Pango.FontDescription.FromString(InvFont.Name ?? "Segoe UI");
        if (InvFont.Size != null)
          FontDesc.Size = (int)(InvFont.Size * Pango.Scale.PangoScale);

        FontDesc.Weight = TranslateFontWeight(InvFont.Weight);

        GtkWidget.OverrideFont(FontDesc);
        GtkWidget.OverrideColor(Gtk.StateFlags.Normal, TranslateColour(InvFont.Colour));
      }
    }
    private Pango.Weight TranslateFontWeight(FontWeight InvFontWeight)
    {
      switch (InvFontWeight)
      {
        case Inv.FontWeight.Thin:
          return Pango.Weight.Ultralight;

        case Inv.FontWeight.Light:
          return Pango.Weight.Light;

        case Inv.FontWeight.Regular:
          return Pango.Weight.Normal;

        case Inv.FontWeight.Medium:
          return Pango.Weight.Semibold;

        case Inv.FontWeight.Bold:
          return Pango.Weight.Bold;

        case Inv.FontWeight.Heavy:
          return Pango.Weight.Ultrabold;

        default:
          throw new ApplicationException("FontWeight not handled: " + InvFontWeight);
      }
    }
    private void TranslateOpacity(Opacity InvOpacity, Gtk.Widget GtkWidget)
    {
      if (InvOpacity.Render())
      {
        // TODO: opacity.
      }
    }
    private void TranslateSize(Inv.Size InvSize, Gtk.Widget GtkWidget)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null && InvSize.Height != null)
          GtkWidget.SetSizeRequest(InvSize.Width.Value, InvSize.Height.Value);

        //if (InvSize.Width != null)
        //  GtkWidget.Width = HorizontalPtToPx(InvSize.Width.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.WidthProperty);
        //
        //if (InvSize.Height != null)
        //  GtkWidget.Height = VerticalPtToPx(InvSize.Height.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.HeightProperty);
        //
        //if (InvSize.MinimumWidth != null)
        //  GtkWidget.MinWidth = HorizontalPtToPx(InvSize.MinimumWidth.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.MinWidthProperty);
        //
        //if (InvSize.MinimumHeight != null)
        //  GtkWidget.MinHeight = VerticalPtToPx(InvSize.MinimumHeight.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.MinHeightProperty);
        //
        //if (InvSize.MaximumWidth != null)
        //  GtkWidget.MaxWidth = HorizontalPtToPx(InvSize.MaximumWidth.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.MaxWidthProperty);
        //
        //if (InvSize.MaximumHeight != null)
        //  GtkWidget.MaxHeight = VerticalPtToPx(InvSize.MaximumHeight.Value);
        //else
        //  GtkWidget.ClearValue(System.Windows.FrameworkElement.MaxHeightProperty);
      }
    }
    private Inv.Key? TranslateKey(Gdk.Key GdkKey)
    {
      Inv.Key Result;
      if (KeyDictionary.TryGetValue(GdkKey, out Result))
        return Result;
      else
        return null;
    }

    private void RenderPanel(Inv.Panel InvPanel, Gtk.Widget GtkElement, Action Action)
    {
      if (InvPanel.Render())
        Action();
    }
    private TElement AccessPanel<TPanel, TElement>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TPanel : Inv.Panel
      where TElement : Gtk.Widget
    {
      if (InvPanel.Node == null)
      {
        var Result = BuildFunction(InvPanel);
        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (TElement)InvPanel.Node;
      }
    }

    private Inv.Application InvApplication;
    private GtkWindow GtkWindow;
    private Dictionary<Gdk.Key, Inv.Key> KeyDictionary;
    private Dictionary<Type, Func<Inv.Panel, GtkControl>> RouteDictionary;
  }
}
