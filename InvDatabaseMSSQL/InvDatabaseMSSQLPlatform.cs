﻿/*! 24 !*/
using System;
using System.Collections.Generic;
using Inv.Support;

namespace Inv.Database.MSSQL
{
  public sealed class Platform : Inv.Database.Platform
  {
    public Platform(string Server, string Database)
    {
      this.Server = Server;
      this.Database = Database;

      this.DateAddFunctionIntervalDictionary = new Dictionary<DateAddFunctionInterval, string>()
      {
        { DateAddFunctionInterval.Year, "YEAR" },
        { DateAddFunctionInterval.Quarter, "QUARTER" },
        { DateAddFunctionInterval.Month, "MONTH" },
        { DateAddFunctionInterval.Week, "WEEK" },
        { DateAddFunctionInterval.Day, "DAY" },
        { DateAddFunctionInterval.Hour, "HOUR" },
        { DateAddFunctionInterval.Minute, "MINUTE" },
        { DateAddFunctionInterval.Second, "SECOND" }
      };
    }

    bool Inv.Database.Platform.Exists()
    {
      using (var Connection = new System.Data.SqlClient.SqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"SELECT [Name] FROM master.dbo.sysdatabases where [Name] = '{ Database }'";

        using (var Reader = Command.ExecuteReader())
          return Reader.Read();
      }
    }
    void Inv.Database.Platform.Create()
    {
      using (var Connection = new System.Data.SqlClient.SqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"CREATE DATABASE { Database }";
        Command.ExecuteNonQuery();
      }
    }
    void Inv.Database.Platform.Drop()
    {
      using (var Connection = new System.Data.SqlClient.SqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"DROP DATABASE { Database }";
        Command.ExecuteNonQuery();
      }
    }
    bool Inv.Database.Platform.UseManualDataTypeCheck => false;
    bool Inv.Database.Platform.UseUpdateAliases => true;
    bool Inv.Database.Platform.UseUpdateFromSyntax => true;
    bool Inv.Database.Platform.UseDeleteFromSyntax => true;
    bool Inv.Database.Platform.UseLimitSyntax => false;
    bool Inv.Database.Platform.UseForeignKeys => true;
    bool Inv.Database.Platform.UseAlterTableAddUniqueConstraint => true;
    Inv.Database.Connection Inv.Database.Platform.NewConnection()
    {
      return new Connection(this, new System.Data.SqlClient.SqlConnection(GetConnectionString(true)));
    }
    string Inv.Database.Platform.GetEscapeOpen()
    {
      return "[";
    }
    string Inv.Database.Platform.GetEscapeClose()
    {
      return "]";
    }
    string Inv.Database.Platform.GetColumnDefinition(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      var BaseColumn = Column.Base;

      var Result = "";

      switch (BaseColumn.Type)
      {
        case ColumnType.Binary:
        case ColumnType.Image:
          Result += "varbinary(max)";
          break;

        case ColumnType.Boolean:
          Result += "bit";
          break;

        case ColumnType.Date:
        case ColumnType.DateTime:
          Result += "datetime2";
          break;

        case ColumnType.DateTimeOffset:
          Result += "datetimeoffset";
          break;

        case ColumnType.Decimal:
        case ColumnType.Money:
          Result += "decimal";
          break;

        case ColumnType.Double:
          Result += "float(53)";
          break;

        case ColumnType.Enum:
        case ColumnType.ForeignKey:
        case ColumnType.Integer32:
          Result += "int";
          break;

        case ColumnType.PrimaryKey:
          Result += "int identity(1,1)";
          break;

        case ColumnType.Guid:
          Result += "uniqueidentifier";
          break;

        case ColumnType.Integer64:
          Result += "bigint";
          break;

        case ColumnType.String:
          Result += "nvarchar(4000)";
          break;

        case ColumnType.Time:
          Result += "time";
          break;

        case ColumnType.TimeSpan:
          Result += "bigint";
          break;

        default:
          throw new Exception($"Unexpected ColumnType '{ BaseColumn.Type.ToString() }'");
      }

      return Result + (BaseColumn.Nullable ? "" : " not") + " null";
    }
    string Inv.Database.Platform.GetPrimaryKeyName(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      return Syntax.PrimaryKeyName(PrimaryKeyColumn);
    }
    string Inv.Database.Platform.GetPrimaryKeyConstraintDefinition(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      return $"CONSTRAINT { Syntax.PrimaryKeyName(PrimaryKeyColumn) } PRIMARY KEY CLUSTERED ([{ PrimaryKeyColumn.Name }])";
    }
    string Inv.Database.Platform.GetForeignKeyConstraintDefinition(Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      return $"{ Syntax.ForeignKeyName(ForeignKeyColumn) } FOREIGN KEY ([{ ForeignKeyColumn.Name }]) REFERENCES [{ ForeignKeyColumn.ReferenceTable.Name }] ([{ ForeignKeyColumn.ReferenceTable.PrimaryKeyColumn.Name }]) ON DELETE NO ACTION ON UPDATE NO ACTION";
    }
    string Inv.Database.Platform.GetUniqueConstraintDefinition(Inv.Database.Index Index)
    {
      return $"{ Syntax.IndexName(Index) } unique";
    }
    string Inv.Database.Platform.GetLastIdentityCommandText()
    {
      return "select cast(@@IDENTITY as int)";
    }
    string Inv.Database.Platform.GetAsDateFunctionCall(string BaseExpression)
    {
      return $"cast({ BaseExpression } as date)";
    }
    string Inv.Database.Platform.GetDateAddFunctionCall(string BaseExpression, Inv.Database.DateAddFunctionInterval Type, string ValueExpression)
    {
      var IntervalType = DateAddFunctionIntervalDictionary.GetValueOrDefault(Type);

      if (string.IsNullOrEmpty(IntervalType))
        throw new Exception("Unexpected DateAddFunctionInterval '" + Type.ToString() + "'");

      return $"DATEADD({ IntervalType }, { ValueExpression }, { BaseExpression })";
    }
    Inv.Database.Catalog Inv.Database.Platform.GetCatalog(Inv.Database.Connection Connection)
    {
      var Result = new Inv.Database.Catalog();

      var TableNameDictionary = new Dictionary<string, CatalogTable>();
      var ViewNameDictionary = new Dictionary<string, CatalogView>();

      using (var TableCommand = Connection.NewCommand())
      {
        TableCommand.SetText($"select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='{ Database }'");

        using (var TableReader = TableCommand.ExecuteReader())
        {
          while (TableReader.Read())
          {
            var TableName = TableReader.GetString(0);

            var Table = Result.AddTable();
            Table.Name = TableName;

            TableNameDictionary.Add(TableName, Table);
          }
        }
      }

      using (var ViewCommand = Connection.NewCommand())
      {
        ViewCommand.SetText($"select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'VIEW' AND TABLE_CATALOG='{ Database }'");

        using (var ViewReader = ViewCommand.ExecuteReader())
        {
          while (ViewReader.Read())
          {
            var ViewName = ViewReader.GetString(0);

            var View = Result.AddView();
            View.Name = ViewName;

            ViewNameDictionary.Add(ViewName, View);
          }
        }
      }

      using (var ViewSpecificationCommand = Connection.NewCommand())
      {
        ViewSpecificationCommand.SetText($"select TABLE_NAME, VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS where TABLE_CATALOG='{ Database }'");

        using (var ViewSpecificationReader = ViewSpecificationCommand.ExecuteReader())
        {
          while (ViewSpecificationReader.Read())
          {
            var ViewName = ViewSpecificationReader.GetString(0);
            var ViewSpecification = ViewSpecificationReader.GetString(1);

            var View = ViewNameDictionary.GetValueOrDefault(ViewName);

            if (View == null)
              throw new Exception($"Could not find view named '{ ViewName }'");

            View.Specification = ViewSpecification;
          }
        }
      }

      using (var ColumnCommand = Connection.NewCommand())
      {
        ColumnCommand.SetText($"SELECT TABLE_NAME, COLUMN_NAME, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION FROM INFORMATION_SCHEMA.COLUMNS where TABLE_CATALOG='{ Database }'");

        using (var ColumnReader = ColumnCommand.ExecuteReader())
        {
          while (ColumnReader.Read())
          {
            var TableName = ColumnReader.GetString(0);
            var ColumnName = ColumnReader.GetString(1);
            var Nullable = ColumnReader.GetString(2) == "YES";
            var DataType = ColumnReader.GetString(3);
            var CharacterMaximumLength = ColumnReader.IsNull(4) ? (long?)null : ColumnReader.GetInteger32(4);
            var NumericPrecision = ColumnReader.IsNull(5) ? (byte?)null : ColumnReader.GetByte(5);

            CatalogColumn Column;

            var Table = TableNameDictionary.GetValueOrDefault(TableName);
            if (Table == null)
            {
              var View = ViewNameDictionary.GetValueOrDefault(TableName);

              if (View == null)
                throw new Exception($"Could not find table or view named '{ TableName }'");

              Column = View.AddColumn();
            }
            else
            {
              Column = Table.AddColumn();
            }

            Column.Name = ColumnName;
            Column.Nullable = Nullable;
            Column.DataType = DataType;

            if (CharacterMaximumLength != null)
              Column.DataType += "(" + (CharacterMaximumLength == -1 ? "max" : CharacterMaximumLength.ToString()) + ")";
            else if (NumericPrecision != null && Column.DataType == "float")
              Column.DataType += "(" + NumericPrecision.Value.ToString() + ")";
          }
        }
      }

      using (var TableConstraintCommand = Connection.NewCommand())
      {
        TableConstraintCommand.SetText($"select C.TABLE_NAME, C.CONSTRAINT_NAME, C.CONSTRAINT_TYPE, K.ORDINAL_POSITION, K.COLUMN_NAME from INFORMATION_SCHEMA.TABLE_CONSTRAINTS C inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE K on C.CONSTRAINT_NAME = K.CONSTRAINT_NAME order by C.CONSTRAINT_NAME, K.ORDINAL_POSITION");

        using (var TableConstraintReader = TableConstraintCommand.ExecuteReader())
        {
          while (TableConstraintReader.Read())
          {
            var TableName = TableConstraintReader.GetString(0);
            var ConstraintName = TableConstraintReader.GetString(1);
            var ConstraintType = TableConstraintReader.GetString(2);
            var OrdinalPosition = TableConstraintReader.GetInteger32(3);
            var ColumnName = TableConstraintReader.GetString(4);

            var Table = TableNameDictionary.GetValueOrDefault(TableName);
            if (Table == null)
              throw new Exception($"Could not find table '{ TableName }' for constraint '{ ConstraintName }'");

            var Column = Table.ColumnList.Find(C => C.Name.Equals(ColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (Column == null)
              throw new Exception($"Could not find column '{ ColumnName }' on table '{ TableName }' for constraint '{ ConstraintName }'");

            switch (ConstraintType)
            {
              case "PRIMARY KEY":
                if (Table.PrimaryKeyColumn != null)
                  throw new Exception("Multiple column primary keys are not supported.");

                if (Column.DataType != "int")
                  throw new Exception("Invalid data type for primary key column.");

                if (Column.Nullable)
                  throw new Exception("Primary key columns cannot be nullable.");

                // Update the data type of the column and set it as the primary key.
                Column.DataType = "primary";
                Table.PrimaryKeyColumn = Column;

                var PrimaryIndex = new CatalogIndex();
                Table.IndexList.Add(PrimaryIndex);
                PrimaryIndex.IsUnique = true;
                PrimaryIndex.Name = ConstraintName;
                PrimaryIndex.ColumnList.Add(Column);
                break;

              case "FOREIGN KEY":
                break;

              case "UNIQUE":
                var UniqueIndex = Table.IndexList.Find(I => I.Name.Equals(ConstraintName, StringComparison.CurrentCultureIgnoreCase));
                if (UniqueIndex == null)
                {
                  UniqueIndex = new CatalogIndex();
                  Table.IndexList.Add(UniqueIndex);
                  UniqueIndex.Name = ConstraintName;
                  UniqueIndex.IsUnique = true;
                }
                UniqueIndex.ColumnList.Add(Column);
                break;

              default:
                throw new Exception("Unexpected constraint type '" + ConstraintType + "'");
            }
          }
        }
      }

      using (var IndexCommand = Connection.NewCommand())
      {
        IndexCommand.SetText($"select t.name, i.name, ic.key_ordinal, c.name from sys.tables t inner join sys.indexes i on t.object_id = i.object_id inner join sys.index_columns ic on ic.object_id = t.object_id and ic.index_id = i.index_id inner join sys.columns c on c.object_id = t.object_id and c.column_id = ic.column_id where i.type = 2 and i.is_unique = 0 order by t.name, i.name, ic.key_ordinal");

        using (var IndexReader = IndexCommand.ExecuteReader())
        {
          while (IndexReader.Read())
          {
            var TableName = IndexReader.GetString(0);
            var IndexName = IndexReader.GetString(1);
            var Sequence = IndexReader.GetByte(2);
            var ColumnName = IndexReader.GetString(3);

            var Table = TableNameDictionary.GetValueOrDefault(TableName);
            if (Table == null)
              throw new Exception($"Could not find table '{ TableName }' for index '{ IndexName }'");

            var Column = Table.ColumnList.Find(C => C.Name.Equals(ColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (Column == null)
              throw new Exception($"Could not find column '{ ColumnName }' on table '{ TableName }' for index '{ IndexName }'");

            var Index = Table.IndexList.Find(I => I.Name.Equals(IndexName, StringComparison.CurrentCultureIgnoreCase));
            if (Index == null)
            {
              Index = new CatalogIndex();
              Table.IndexList.Add(Index);
              Index.Name = IndexName;
              Index.IsUnique = false;
            }
            Index.ColumnList.Add(Column);
          }
        }
      }

      using (var ForeignKeyCommand = Connection.NewCommand())
      {
        ForeignKeyCommand.SetText(@"
          select
            fk.name, ft.name, cc.name, rt.name, rc.name, fk.update_referential_action_desc, fk.delete_referential_action_desc
          from
            sys.foreign_keys fk
            inner join sys.tables ft on fk.parent_object_id = ft.object_id
            inner join sys.foreign_key_columns fkc on fkc.constraint_object_id = fk.object_id
            inner join sys.columns cc on cc.object_id = fkc.parent_object_id and cc.column_id = fkc.parent_column_id
            inner join sys.tables rt on rt.object_id = fkc.referenced_object_id
            inner join sys.columns rc on rc.object_id = fkc.referenced_object_id and rc.column_id = fkc.referenced_column_id
        ");

        using (var ForeignKeyReader = ForeignKeyCommand.ExecuteReader())
        {
          while (ForeignKeyReader.Read())
          {
            var ForeignKeyName = ForeignKeyReader.GetString(0);
            var ForeignKeyTableName = ForeignKeyReader.GetString(1);
            var ForeignKeyColumnName = ForeignKeyReader.GetString(2);
            var ForeignKeyReferenceTableName = ForeignKeyReader.GetString(3);
            var ForeignKeyReferenceColumnName = ForeignKeyReader.GetString(4);
            var ForeignKeyUpdateRule = ForeignKeyReader.GetString(5);
            var ForeignKeyDeleteRule = ForeignKeyReader.GetString(6);

            var Table = TableNameDictionary.GetValueOrDefault(ForeignKeyTableName);
            if (Table == null)
              throw new Exception($"Could not find table '{ ForeignKeyTableName }' for foreign key '{ ForeignKeyName }'");

            var Column = Table.ColumnList.Find(C => C.Name.Equals(ForeignKeyColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (Column == null)
              throw new Exception($"Could not find column '{ ForeignKeyColumnName }' on table '{ ForeignKeyTableName }' for foreign key '{ ForeignKeyName }'");

            var ReferenceTable = TableNameDictionary.GetValueOrDefault(ForeignKeyReferenceTableName);
            if (ReferenceTable == null)
              throw new Exception($"Could not find table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

            var ReferenceColumn = ReferenceTable.ColumnList.Find(C => C.Name.Equals(ForeignKeyReferenceColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (ReferenceColumn == null)
              throw new Exception($"Could not find column '{ ForeignKeyReferenceColumnName }' on table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

            if (!ForeignKeyUpdateRule.Equals("NO_ACTION", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception("NO_ACTION update rule required for all foreign keys");

            if (!ForeignKeyDeleteRule.Equals("NO_ACTION", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception("NO_ACTION update rule required for all foreign keys");

            var ForeignKey = new CatalogForeignKey();
            Column.ForeignKey = ForeignKey;
            ForeignKey.Name = ForeignKeyName;
            ForeignKey.ReferenceTable = ReferenceTable;
            ForeignKey.ReferenceColumn = ReferenceColumn;
          }
        }
      }

      return Result;
    }
    string Inv.Database.Platform.GetCatalogColumnDataType(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      var Base = Column.Base;
      var ColumnType = Base.Type;

      switch (ColumnType)
      {
        case Inv.Database.ColumnType.PrimaryKey:
          return "primary";

        case Inv.Database.ColumnType.String:
          return "nvarchar(4000)";

        case ColumnType.Binary:
        case ColumnType.Image:
          return "varbinary(max)";

        case ColumnType.Boolean:
          return "bit";

        case ColumnType.Date:
        case ColumnType.DateTime:
          return "datetime2";

        case ColumnType.DateTimeOffset:
          return "datetimeoffset";

        case ColumnType.Enum:
        case ColumnType.ForeignKey:
        case ColumnType.Integer32:
          return "int";

        case ColumnType.Integer64:
          return "bigint";

        case ColumnType.Decimal:
        case ColumnType.Money:
          return "decimal";

        case ColumnType.Time:
          return "time";

        case ColumnType.TimeSpan:
          return "bigint";

        case ColumnType.Guid:
          return "uniqueidentifier";

        case ColumnType.Double:
          return "float(53)";

        default:
          throw new Exception($"Unexpected '{ ColumnType.ToString() }'");
      }
    }

    private string GetConnectionString(bool IncludeDatabase)
    {
      var Result = $"Server={ Server };Trusted_Connection=Yes;";

      if (IncludeDatabase)
        Result += $"Database={ Database };";

      return Result;
    }

    private string Server;
    private string Database;
    private Dictionary<Inv.Database.DateAddFunctionInterval, string> DateAddFunctionIntervalDictionary;
  }

  internal sealed class Connection : Inv.Database.Connection
  {
    internal Connection(Platform Platform, System.Data.SqlClient.SqlConnection Base)
    {
      this.Platform = Platform;
      this.Base = Base;
    }

    Inv.Database.Platform Inv.Database.Connection.Platform => Platform;
    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    void Inv.Database.Connection.Open()
    {
      Base.Open();
    }
    void Inv.Database.Connection.HealthCheck(Inv.Database.HealthCheck HealthCheck)
    {
    }
    void Inv.Database.Connection.DataTypeCheck(HealthCheckGroup CheckGroup, Inv.Database.Table Table, int PrimaryKeyValue, Inv.Database.Column Column, string RawValue)
    {
    }
    void Inv.Database.Connection.RetrySleep(int Milliseconds)
    {
      System.Threading.Thread.Sleep(Milliseconds);
    }
    Inv.Database.Transaction Inv.Database.Connection.BeginTransaction()
    {
      var Result = new Transaction(Base.BeginTransaction());
      Result.ShutdownEvent += () =>
      {
        this.Transaction = null;
      };

      this.Transaction = Result;

      return Result;
    }
    Inv.Database.Command Inv.Database.Connection.NewCommand()
    {
      var SqlCommand = Base.CreateCommand();

      if (Transaction != null)
        SqlCommand.Transaction = Transaction.Base;

      return new Command(SqlCommand);
    }

    private Platform Platform;
    private System.Data.SqlClient.SqlConnection Base;
    private Inv.Database.MSSQL.Transaction Transaction;
  }

  internal sealed class Transaction : Inv.Database.Transaction
  {
    internal Transaction(System.Data.SqlClient.SqlTransaction Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    void Inv.Database.Transaction.Commit()
    {
      Base.Commit();

      Shutdown();
    }
    void Inv.Database.Transaction.Rollback()
    {
      Base.Rollback();

      Shutdown();
    }

    internal System.Data.SqlClient.SqlTransaction Base { get; }
    internal event Action ShutdownEvent;

    private void Shutdown()
    {
      if (ShutdownEvent != null)
        ShutdownEvent();
    }
  }

  internal sealed class Command : Inv.Database.Command
  {
    internal Command(System.Data.SqlClient.SqlCommand Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    Inv.Database.Reader Inv.Database.Command.ExecuteReader()
    {
      return new Reader(Base.ExecuteReader());
    }
    int Inv.Database.Command.ExecuteNonQuery()
    {
      return Base.ExecuteNonQuery();
    }
    void Inv.Database.Command.SetParameter(string Name, int Index, Inv.Database.ColumnType ColumnType, object Value)
    {
      if ((ColumnType == ColumnType.Image || ColumnType == ColumnType.Binary) && Value == null)
      {
        Base.Parameters.Add(new System.Data.SqlClient.SqlParameter(Name, System.Data.SqlDbType.VarBinary, -1)).Value = DBNull.Value;
      }
      else if (ColumnType == ColumnType.Date && Value != null)
      {
        var Date = (Inv.Date)Value;
        Base.Parameters.Add(new System.Data.SqlClient.SqlParameter(Name, System.Data.SqlDbType.Date)).Value = Date.ToDateTime();
      }
      else if (ColumnType == ColumnType.Time && Value != null)
      {
        var Time = (Inv.Time)Value;
        Base.Parameters.Add(new System.Data.SqlClient.SqlParameter(Name, System.Data.SqlDbType.Time)).Value = Time.ToTimeSpan();
      }
      else
      {
        Base.Parameters.Add(new System.Data.SqlClient.SqlParameter(Name, Value ?? DBNull.Value));
      }
    }
    void Inv.Database.Command.SetText(string Text)
    {
      Base.CommandText = Text;
    }
    void Inv.Database.Command.SetTimeout(int Seconds)
    {
      Base.CommandTimeout = Seconds;
    }

    private System.Data.SqlClient.SqlCommand Base;
  }

  internal sealed class Reader : Inv.Database.Reader
  {
    internal Reader(System.Data.SqlClient.SqlDataReader Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    bool Database.Reader.Read()
    {
      return Base.Read();
    }
    bool Database.Reader.IsNull(int ColumnIndex)
    {
      return Base.IsDBNull(ColumnIndex);
    }
    bool Database.Reader.GetBoolean(int ColumnIndex)
    {
      return Base.GetBoolean(ColumnIndex);
    }
    byte Database.Reader.GetByte(int ColumnIndex)
    {
      return Base.GetByte(ColumnIndex);
    }
    Inv.Date Database.Reader.GetDate(int ColumnIndex)
    {
      return new Inv.Date(Base.GetDateTime(ColumnIndex));
    }
    DateTime Database.Reader.GetDateTime(int ColumnIndex)
    {
      return Base.GetDateTime(ColumnIndex);
    }
    DateTimeOffset Database.Reader.GetDateTimeOffset(int ColumnIndex)
    {
      return Base.GetDateTimeOffset(ColumnIndex);
    }
    decimal Database.Reader.GetDecimal(int ColumnIndex)
    {
      return Base.GetDecimal(ColumnIndex);
    }
    double Database.Reader.GetDouble(int ColumnIndex)
    {
      return Base.GetDouble(ColumnIndex);
    }
    float Database.Reader.GetFloat(int ColumnIndex)
    {
      return Base.GetFloat(ColumnIndex);
    }
    Guid Database.Reader.GetGuid(int ColumnIndex)
    {
      return Base.GetGuid(ColumnIndex);
    }
    Inv.Image Database.Reader.GetImage(int ColumnIndex)
    {
      return new Inv.Image(Base.GetSqlBytes(ColumnIndex).Buffer, ".png");
    }
    int Inv.Database.Reader.GetInteger32(int ColumnIndex)
    {
      return Base.GetInt32(ColumnIndex);
    }
    long Inv.Database.Reader.GetInteger64(int ColumnIndex)
    {
      return Base.GetInt64(ColumnIndex);
    }
    Inv.Money Database.Reader.GetMoney(int ColumnIndex)
    {
      return new Inv.Money(Base.GetDecimal(ColumnIndex));
    }
    string Inv.Database.Reader.GetString(int ColumnIndex)
    {
      return Base.GetString(ColumnIndex);
    }
    Inv.Time Database.Reader.GetTime(int ColumnIndex)
    {
      return new Inv.Time(Base.GetTimeSpan(ColumnIndex));
    }
    TimeSpan Database.Reader.GetTimeSpan(int ColumnIndex)
    {
      return Base.GetTimeSpan(ColumnIndex);
    }

    private System.Data.SqlClient.SqlDataReader Base;
  }
}