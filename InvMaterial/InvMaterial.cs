﻿/*! 120 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Material
{
  public static class Theme
  {
    static Theme()
    {
      FontName = "Roboto";
      Set(Inv.Colour.DimGray, false);
    }

    public static bool Dark { get; private set; }
    public static Inv.Colour PrimaryColour { get; private set; } // Used on buttons and app bars.
    public static Inv.Colour PrimaryTextColour { get; private set; } // Text colour used on backgrounds of PrimaryColour.
    public static Inv.Colour TextColour { get; private set; }
    public static Inv.Colour SubtleColour { get; private set; }
    public static Inv.Colour EdgeColour { get; private set; }
    public static Inv.Colour SoftColour { get; private set; }
    public static Inv.Colour BlankColour { get; private set; }
    // "On"X colours are used for text and images that appear "on" areas coloured using X.
    /*
    public static Inv.Colour BackgroundColour { get; private set; } // Behind scrollable content.
    public static Inv.Colour ErrorColour { get; private set; }
    public static Inv.Colour SurfaceColour { get; private set; } // Cards, sheets, and menus.
    public static Inv.Colour PrimaryVariantColour { get; private set; }
    public static Inv.Colour SecondaryColour { get; private set; } // Used on FABs and selection controls.
    public static Inv.Colour SecondaryVariantColour { get; private set; }
    public static Inv.Colour OnPrimaryColour { get; private set; }
    public static Inv.Colour OnSecondaryColour { get; private set; }
    public static Inv.Colour OnBackgroundColour { get; private set; }
    public static Inv.Colour OnSurfaceColour { get; private set; }
    public static Inv.Colour OnErrorColour { get; private set; }
    */
    public static string FontName { get; private set; }

    // TODO: Shape and typography sharing.

    public static Inv.Colour SmokeColour { get; private set; }

    public static void Set(Inv.Colour PrimaryValue, bool DarkValue)
    {
      PrimaryColour = PrimaryValue;
      PrimaryTextColour = PrimaryValue.BackgroundToBlackWhiteForeground();
      Dark = DarkValue;

      SmokeColour = Dark ? Inv.Colour.GraySmoke : Inv.Colour.WhiteSmoke;
      TextColour = Dark ? Inv.Colour.White : Inv.Colour.Black;
      SubtleColour = Dark ? Inv.Colour.LightGray : Inv.Colour.DimGray;
      EdgeColour = Dark ? Inv.Colour.DimGray : Inv.Colour.LightGray;
      SoftColour = Dark ? Inv.Colour.DeepGray : Inv.Colour.DarkGray;
      BlankColour = Dark ? Inv.Colour.BlackSmoke : Inv.Colour.White;
    }
    public static void SetPrimaryColour(Inv.Colour Colour)
    {
      Set(Colour, Dark);
    }
    public static void SetDark()
    {
      Set(PrimaryColour, true);
    }
    public static void SetLight()
    {
      Set(PrimaryColour, false);
    }
    public static void SetFontName(string Name)
    {
      FontName = Name;
    }
  }

  public sealed class Space : Inv.Panel<Inv.Overlay>
  {
    public Space()
    {
      this.PlaneList = new Inv.DistinctList<Inv.Material.Plane>();

      this.Base = Inv.Overlay.New();
    }

    public Inv.Material.Sheet NewSheet()
    {
      return new Inv.Material.Sheet(this);
    }
    public AlertDialog NewAlertDialog()
    {
      var Result = new AlertDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public SimpleDialog NewSimpleDialog()
    {
      var Result = new SimpleDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public ConfirmationDialog NewConfirmationDialog()
    {
      var Result = new ConfirmationDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public FullScreenDialog NewFullScreenDialog()
    {
      var Result = new FullScreenDialog(this);
      Result.ShowEvent += () => AddPlane(Result);
      Result.HideEvent += () => RemovePlane(Result);
      return Result;
    }
    public PropertySheet NewPropertySheet()
    {
      return new PropertySheet(this);
    }
    public SelectDialog<T> NewSelectDialog<T>()
    {
      return new SelectDialog<T>(this);
    }
    public bool HasActivePlane()
    {
      return PlaneList.Any();
    }
    public void HideActivePlane()
    {
      if (PlaneList.Any())
      {
        // If the entry successfully hides, then it will be removed from ModalEntryList.
        var Plane = PlaneList.LastOrDefault();

        if (Plane != null)
          Plane.Hide();
      }
    }
    public bool HasPanel(Inv.Panel Panel)
    {
      return Base.HasPanel(Panel);
    }
    public void ShowPanel(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }
    public void HidePanel(Inv.Panel Panel)
    {
      Base.RemovePanel(Panel);
    }

    internal void AddPlane(Inv.Material.Plane Plane)
    {
      PlaneList.Add(Plane);
    }
    internal void RemovePlane(Inv.Material.Plane Plane)
    {
      var LastPlane = PlaneList.LastOrDefault();

      Inv.Assert.Check(LastPlane != null, "Attempted to remove plane but no plane is active.");
      Inv.Assert.Check(LastPlane != null && Plane == LastPlane, "Attempted to remove plane but a different plane was active");

      if (LastPlane != null)
        PlaneList.Remove(LastPlane);
    }

    private Inv.DistinctList<Plane> PlaneList;
  }

  public sealed class Plane
  {
    internal void Hide()
    {
      if (HideEvent != null)
        HideEvent();
    }

    internal event Action HideEvent;
  }

  public sealed class Sheet
  {
    internal Sheet(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Frame = Inv.Frame.New();
      Frame.Alignment.Stretch();
    }

    public Inv.Panel Content
    {
      get => Frame.Content;
      set => Frame.Content = value;
    }
    public Inv.Background Background => Frame.Background;

    public void Show()
    {
      Space.ShowPanel(Frame);
    }
    public void Hide()
    {
      Space.HidePanel(Frame);
    }

    private Inv.Material.Space Space;
    private Inv.Frame Frame;
  }

  public sealed class Scaffold : Inv.Panel<Inv.Overlay>
  {
    public Scaffold()
    {
      this.Base = Inv.Overlay.New();

      this.Layout = new Inv.Material.AppLayout();
      Base.AddPanel(Layout);

      this.Drawer = new Drawer(this);
      Base.AddPanel(Drawer);
      Drawer.ShowEvent += () => Layout.FloatingActionContainer.Visibility.Collapse();
      Drawer.HideEvent += () => Layout.FloatingActionContainer.Visibility.Show();
    }

    public AppBar AppBar => Layout.AppBar;
    public Drawer Drawer { get; private set; }
    public Inv.Material.FloatingActionContainer FloatingActionContainer => Layout.FloatingActionContainer;
    public Inv.Panel Content
    {
      set { Layout.Content = value; }
    }

    public void Clear()
    {
      Layout.Clear();
    }

    private Inv.Material.AppLayout Layout;
  }

  internal sealed class Scrim : Inv.Panel<Inv.Button>
  {
    internal Scrim()
    {
      this.Base = Inv.Button.NewStark();
      Base.Background.Colour = Inv.Colour.Black.AdjustAlpha(56);
      Base.Visibility.Collapse();
      Base.SingleTapEvent += HandleSingleTap;
    }

    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    public void Enable()
    {
      if (!Base.HasSingleTap())
        Base.SingleTapEvent += HandleSingleTap;
    }
    public void Disable()
    {
      if (Base.HasSingleTap())
        Base.SingleTapEvent -= HandleSingleTap;
    }

    private void HandleSingleTap()
    {
      if (SingleTapEvent != null)
        SingleTapEvent();
    }
  }

  public sealed class Drawer : Inv.Panel<Inv.Overlay>
  {
    internal Drawer(Scaffold Scaffold)
    {
      this.Scaffold = Scaffold;

      this.Base = Inv.Overlay.New();

      this.Scrim = new Inv.Material.Scrim();
      Base.AddPanel(Scrim);
      Scrim.SingleTapEvent += () => Hide(true);

      this.BackgroundFrame = Inv.Frame.New();
      Base.AddPanel(BackgroundFrame);
      BackgroundFrame.Background.Colour = null;
      BackgroundFrame.Alignment.Stretch();
      BackgroundFrame.Margin.Set(0, 0, 56, 0); // Mobile only.

      this.Scroll = Inv.Scroll.NewVertical();
      Scroll.Elevation.Set(4);

      this.Stack = Inv.Stack.NewVertical();
      Scroll.Content = Stack;
      Stack.Padding.Set(0, 0, 0, 8);
      Stack.Background.Colour = Inv.Material.Theme.BlankColour;
    }

    public void Show()
    {
      Scrim.Visibility.Show();
      BackgroundFrame.Transition(Scroll).CarouselPrevious();

      if (ShowEvent != null)
        ShowEvent();
    }
    public void Hide()
    {
      Hide(true);
    }
    public bool IsShown()
    {
      return Scrim.Visibility.Get();
    }
    public DrawerHeader AddHeader()
    {
      var Result = new DrawerHeader();

      Stack.AddPanel(Result);

      return Result;
    }
    public DrawerItem AddItem()
    {
      var Result = new DrawerItem(this);

      Stack.AddPanel(Result);

      return Result;
    }
    public Inv.Material.Divider AddDivider()
    {
      var Result = new Inv.Material.Divider();
      Result.Margin.Set(0, 8, 0, 0);

      Stack.AddPanel(Result);

      return Result;
    }

    internal event Action ShowEvent;
    internal event Action HideEvent;

    internal void Hide(bool IsAnimated)
    {
      var Transition = BackgroundFrame.Transition(null);
      if (IsAnimated)
        Transition.CarouselNext();

      Scrim.Visibility.Collapse();

      if (HideEvent != null)
        HideEvent();
    }

    private readonly Scaffold Scaffold;
    private readonly Inv.Material.Scrim Scrim;
    private readonly Inv.Frame BackgroundFrame;
    private readonly Inv.Scroll Scroll;
    private readonly Inv.Stack Stack;
  }
  
  public sealed class DrawerHeader : Inv.Panel<Inv.Stack>
  {
    internal DrawerHeader()
    {
      this.Base = Inv.Stack.NewVertical();
      Base.Margin.Set(16, 18, 16, 8);

      this.TitleLabel = Inv.Label.New();
      Base.AddPanel(TitleLabel);
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Medium();
      TitleLabel.Font.Size = 24;
      TitleLabel.Font.Colour = Theme.TextColour;

      this.SubtextLabel = Inv.Label.New();
      Base.AddPanel(SubtextLabel);
      SubtextLabel.Margin.Set(0, 8, 0, 0);
      SubtextLabel.Font.Name = Theme.FontName;
      SubtextLabel.Font.Medium();
      SubtextLabel.Font.Size = 15;
      SubtextLabel.Font.Colour = Inv.Material.Theme.SubtleColour;
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public string Subtext
    {
      get { return SubtextLabel.Text; }
      set
      {
        SubtextLabel.Text = value;

        Refresh();
      }
    }

    private void Refresh()
    {
      SubtextLabel.Visibility.Set(!string.IsNullOrWhiteSpace(SubtextLabel.Text));
    }

    private Inv.Label TitleLabel;
    private Inv.Label SubtextLabel;
  }

  public sealed class DrawerItem : Inv.Panel<Inv.Frame>
  {
    internal DrawerItem(Drawer Drawer)
    {
      this.IsAnimatedClose = true;

      this.Base = Inv.Frame.New();
      Base.Size.SetHeight(48);
      Base.Margin.Set(8, 8, 8, 0);

      this.Button = Inv.Button.NewFlat();
      Base.Content = Button;
      Button.Corner.Set(4);
      Button.SingleTapEvent += () =>
      {
        if (IsAutoClose)
          Drawer.Hide(IsAnimatedClose);

        if (SingleTapEvent != null)
          SingleTapEvent();
      };

      var Dock = Inv.Dock.NewHorizontal();
      Button.Content = Dock;

      this.Graphic = Inv.Graphic.New();
      Dock.AddHeader(Graphic);
      Graphic.Margin.Set(8, 0, 0, 0);
      Graphic.Size.Set(24);
      Graphic.Alignment.CenterStretch();

      this.CaptionLabel = Inv.Label.New();
      Dock.AddClient(CaptionLabel);
      CaptionLabel.Margin.Set(32, 0, 0, 0);
      CaptionLabel.Alignment.CenterStretch();
      CaptionLabel.Font.Size = 14;
      CaptionLabel.Font.Name = Theme.FontName;
      CaptionLabel.Font.Medium();
      CaptionLabel.Font.Colour = Theme.TextColour;
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public Inv.Image Image
    {
      get { return ImageField; }
      set
      {
        ImageField = value;

        Refresh();
      }
    }
    public bool IsActive
    {
      get { return IsActiveField; }
      set
      {
        IsActiveField = value;

        Refresh();
      }
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    public void AutoClose()
    {
      this.IsAutoClose = true;
    }
    public void DisableAnimation()
    {
      this.IsAnimatedClose = false;
    }

    private void Refresh()
    {
      var ImageColour = IsActive ? Theme.PrimaryColour : Inv.Colour.DimGray;

      if (ImageField != null)
        Graphic.Image = Inv.Application.Access().Graphics.Tint(ImageField, ImageColour);

      Button.Background.Colour = IsActive ? Theme.PrimaryColour.AdjustAlpha(56) : Inv.Material.Theme.BlankColour;
      CaptionLabel.Font.Colour = IsActive ? Theme.PrimaryColour : Inv.Material.Theme.SubtleColour;
    }

    private Inv.Button Button;
    private Inv.Graphic Graphic;
    private Inv.Label CaptionLabel;
    private Inv.Image ImageField;
    private bool IsActiveField;
    private bool IsAutoClose;
    private bool IsAnimatedClose;
  }

  public sealed class AppLayout : Inv.Panel<Inv.Overlay>
  {
    public AppLayout()
    {
      this.Base = Inv.Overlay.New();

      this.ContentFrame = Inv.Frame.New();
      Base.AddPanel(ContentFrame);

      var AppBarFrame = Inv.Frame.New();
      Base.AddPanel(AppBarFrame);
      AppBarFrame.Alignment.TopStretch();

      this.AppBar = new AppBar();
      AppBarFrame.Content = AppBar;
      AppBar.AdjustEvent += () => SetContentHeight();

      this.FloatingActionContainer = new FloatingActionContainer(this);
      Base.AddPanel(FloatingActionContainer);

      SetContentHeight();
    }

    public AppBar AppBar { get; private set; }
    public Inv.Material.FloatingActionContainer FloatingActionContainer { get; private set; }
    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }

    internal void Clear()
    {
      AppBar.Clear();

      FloatingActionContainer.Clear();
    }
    internal void AddPanel(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }
    internal void RemovePanel(Inv.Panel Panel)
    {
      Base.RemovePanel(Panel);
    }

    private void SetContentHeight()
    {
      var Dimension = AppBar.GetDimension();

      ContentFrame.Margin.Set(0, Dimension.Height != 0 ? Dimension.Height : 56, 0, 0);
    }

    private Inv.Frame ContentFrame;
  }

  internal sealed class AppBarLayout : Inv.Panel<Inv.Dock>
  {
    public AppBarLayout()
    {
      this.Base = Inv.Dock.NewHorizontal();

      this.LeadingButton = new IconButton();
      Base.AddHeader(LeadingButton);
      LeadingButton.Padding.Set(16);
      LeadingButton.SetSize(56);
      LeadingButton.Visibility.Collapse();
      LeadingButton.Background.Colour = Inv.Material.Theme.PrimaryColour;
      LeadingButton.ForegroundColour = Inv.Material.Theme.PrimaryTextColour;
      LeadingButton.Alignment.StretchLeft();
      LeadingButton.SingleTapEvent += () =>
      {
        if (SingleTapEvent != null)
          SingleTapEvent();
      };

      this.ContentFrame = Inv.Frame.New();
      Base.AddClient(ContentFrame);
      ContentFrame.Margin.Set(16, 0, 0, 0); // TODO: Should be 16 from baseline.
    }

    public void Clear()
    {
      LeadingButton.Image = null;
      SingleTapEvent = null;

      ContentFrame.Content = null;

      Base.RemoveFooters();
    }
    public void SetLeading(Inv.Image Image, Action SingleTapEvent)
    {
      LeadingButton.Image = Image;
      LeadingButton.Visibility.Set(Image != null);

      this.SingleTapEvent = SingleTapEvent;
    }
    public IconButton AddTrailingIconButton()
    {
      var Result = new IconButton();
      Result.Background.Colour = Inv.Material.Theme.PrimaryColour;
      Result.ForegroundColour = Inv.Material.Theme.PrimaryTextColour;
      Result.Alignment.StretchRight();
      Result.Padding.Set(16);
      Result.SetSize(56);

      Base.AddFooter(Result);

      return Result;
    }

    internal Inv.Panel CustomContent
    {
      get => ContentFrame.Content;
      set => ContentFrame.Content = value;
    }

    private IconButton LeadingButton;
    private Inv.Frame ContentFrame;
    private Action SingleTapEvent;
  }

  public sealed class AppBar : Inv.Panel<Inv.Stack>
  {
    public AppBar()
    {
      this.Base = Inv.Stack.NewVertical();
      Base.Alignment.TopStretch();
      Base.Background.Colour = Inv.Material.Theme.PrimaryColour;
      Base.Elevation.Set(2);

      this.LayoutFrame = Inv.Frame.New();
      Base.AddPanel(LayoutFrame);
      LayoutFrame.Size.SetHeight(56);

      this.PrimaryLayout = new AppBarLayout();
      LayoutFrame.Content = PrimaryLayout;

      this.FlexibleFrame = Inv.Frame.New();
      Base.AddPanel(FlexibleFrame);
    }

    public Inv.Panel FlexibleContent
    {
      set { FlexibleFrame.Content = value; }
    }

    public void Clear()
    {
      PrimaryLayout.Clear();

      FlexibleContent = null;
    }
    public void ContentAsTitle(string Text)
    {
      var TitleLabel = Inv.Label.New();
      TitleLabel.Text = Text;
      TitleLabel.Alignment.CenterLeft();
      TitleLabel.LineWrapping = false;
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Medium();
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Colour = Inv.Material.Theme.PrimaryTextColour;

      PrimaryLayout.CustomContent = TitleLabel;
    }
    public void ContentAsCustom(Inv.Panel Panel)
    {
      PrimaryLayout.CustomContent = Panel;
    }
    public void SetLeading(Inv.Image Image, Action SingleTapEvent)
    {
      PrimaryLayout.SetLeading(Image, SingleTapEvent);
    }
    public IconButton AddTrailingIconButton()
    {
      return PrimaryLayout.AddTrailingIconButton();
    }
    public AppSearch EnableSearch()
    {
      if (AppSearch == null)
        this.AppSearch = new AppSearch(this);

      return AppSearch;
    }

    internal Inv.Dimension GetDimension() => Base.GetDimension();
    internal event Action AdjustEvent
    {
      add { Base.AdjustEvent += value; }
      remove { Base.AdjustEvent -= value; }
    }

    internal void ShowSearch(AppSearch Search)
    {
      LayoutFrame.Transition(Search.Layout);//.Fade();
    }
    internal void HideSearch(AppSearch Search)
    {
      LayoutFrame.Transition(PrimaryLayout);//.Fade();
    }

    private Inv.Frame LayoutFrame;
    private AppBarLayout PrimaryLayout;
    private Inv.Frame FlexibleFrame;
    private AppSearch AppSearch;
  }

  public sealed class AppSearch
  {
    internal AppSearch(Inv.Material.AppBar AppBar)
    {
      this.AppBar = AppBar;

      this.Timer = Inv.Application.Access().Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromMilliseconds(500);
      Timer.IntervalEvent += () =>
      {
        Timer.Stop();

        var NewText = Edit.Text.EmptyOrWhitespaceAsNull();

        if (!string.Equals(Text, NewText, StringComparison.CurrentCultureIgnoreCase))
        {
          this.Text = NewText;

          if (ChangeEvent != null)
            ChangeEvent();
        }
      };

      this.Layout = new AppBarLayout();

      this.Edit = Inv.Edit.NewText();
      Layout.CustomContent = Edit;
      Edit.Font.Size = 18;
      Edit.Font.Name = Theme.FontName;
      Edit.Font.Colour = Inv.Material.Theme.PrimaryTextColour;
      Edit.Alignment.CenterStretch();
      Edit.ChangeEvent += () =>
      {
        ClearButton.Visibility.Set(!string.IsNullOrWhiteSpace(Edit.Text));

        Timer.Restart();
      };

      this.ClearButton = Layout.AddTrailingIconButton();
      ClearButton.Image = Inv.Material.Resources.Content.Clear;
      ClearButton.Visibility.Collapse();
      ClearButton.SingleTapEvent += () => Clear();

      Layout.SetLeading(Inv.Material.Resources.Navigation.ArrowBack, () =>
      {
        Clear();
        Refresh(false);
      });

      // NOTE: this is added to the AppBar.
      this.SearchButton = AppBar.AddTrailingIconButton();
      SearchButton.Image = Inv.Material.Resources.Action.Search;
      SearchButton.SingleTapEvent += () =>
      {
        Refresh(true);

        Edit.Focus.Set();
      };
    }

    public string Text { get; private set; }
    public event Action ChangeEvent;
    public event Action EnterEvent;
    public event Action ExitEvent;

    internal Inv.Material.AppBarLayout Layout { get; }

    private void Refresh(bool IsSearching)
    {
      if (IsSearching)
      {
        SearchButton.Visibility.Collapse();

        AppBar.ShowSearch(this);

        if (EnterEvent != null)
          EnterEvent();
      }
      else
      {
        SearchButton.Visibility.Show();

        AppBar.HideSearch(this);

        if (ExitEvent != null)
          ExitEvent();
      }
    }
    private void Clear()
    {
      Edit.Text = "";
      ClearButton.Visibility.Set(false);
    }

    private Inv.Material.AppBar AppBar;
    private Inv.WindowTimer Timer;
    private Inv.Edit Edit;
    private Inv.Material.IconButton ClearButton;
    private Inv.Material.IconButton SearchButton;
  }

  public sealed class Tab : Inv.Panel<Inv.Button>
  {
    internal Tab()
    {
      this.Base = Inv.Button.NewFlat();

      this.Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.IndicatorFrame = Inv.Frame.New();
      Overlay.AddPanel(IndicatorFrame);
      IndicatorFrame.Margin.Set(0, 0, 0, 1);
      IndicatorFrame.Size.SetHeight(1);
      IndicatorFrame.Alignment.BottomStretch();

      this.DividerFrame = Inv.Frame.New();
      Overlay.AddPanel(DividerFrame);
      DividerFrame.Size.SetHeight(1);
      DividerFrame.Alignment.BottomStretch();

      this.Label = Inv.Label.New();
      Overlay.AddPanel(Label);
      Label.Padding.Set(12, 0);
      Label.Alignment.Center();
      Label.Font.Name = Theme.FontName;
      Label.Font.Medium();
      Label.Font.Size = 14;
      Label.Font.Colour = Theme.TextColour;

      Refresh();
    }

    public string Title
    {
      get { return Label.Text; }
      set { Label.Text = value.ToUpper(); }
    }
    public bool IsActive
    {
      get { return IsActiveField; }
      set
      {
        if (value != IsActiveField)
        {
          IsActiveField = value;

          Refresh();
        }
      }
    }
    public Inv.Visibility Visibility => Base.Visibility;

    internal event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    internal void Lighten()
    {
      if (!IsLightened)
      {
        this.IsLightened = true;
        Refresh();
      }
    }
    internal void Darken()
    {
      if (IsLightened)
      {
        this.IsLightened = false;
        Refresh();
      }
    }

    private void Refresh()
    {
      var BackgroundColour = IsLightened ? Inv.Material.Theme.BlankColour : Inv.Material.Theme.PrimaryColour;
      var ForegroundTextColour = IsLightened ? Inv.Material.Theme.PrimaryColour : Inv.Material.Theme.PrimaryTextColour;
      var ActiveForegroundColour = IsActiveField ? IsLightened ? ForegroundTextColour : BackgroundColour.BackgroundToForeground() : Inv.Colour.Transparent;

      Base.Background.Colour = BackgroundColour;
      Label.Font.Colour = ForegroundTextColour;
      IndicatorFrame.Background.Colour = ActiveForegroundColour;
      DividerFrame.Background.Colour = IsActiveField ? ActiveForegroundColour : IsLightened ? Inv.Material.Theme.EdgeColour : Inv.Colour.Transparent;
    }

    private Inv.Overlay Overlay;
    private Inv.Frame IndicatorFrame;
    private Inv.Frame DividerFrame;
    private Inv.Label Label;
    private bool IsActiveField;
    private bool IsLightened;
  }

  public sealed class TabBar : Inv.Panel<Inv.Dock>
  {
    public TabBar()
    {
      this.TabList = new Inv.DistinctList<Tab>();

      this.Base = Inv.Dock.NewHorizontal();
      Base.Size.SetHeight(48);
    }

    public Tab ActiveTab { get; private set; }
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public Tab AddTab(string Title)
    {
      var Result = new Tab();
      Result.Title = Title;
      Result.SingleTapEvent += () =>
      {
        Activate(Result);
      };

      RefreshTab(Result);

      Base.AddClient(Result);

      TabList.Add(Result);

      return Result;
    }
    public void RemoveTabs()
    {
      Base.RemoveClients();
      TabList.Clear();
    }
    public void Activate(Tab Tab)
    {
      InternalActivate(Tab);

      if (ChangeEvent != null)
        ChangeEvent();
    }
    public void Lighten()
    {
      if (!IsLightened)
      {
        this.IsLightened = true;
        RefreshTheme();
      }
    }
    public void Darken()
    {
      if (IsLightened)
      {
        this.IsLightened = false;
        RefreshTheme();
      }
    }

    internal void InternalActivate(Tab Tab)
    {
      this.ActiveTab = Tab;

      // Don't fire again if already active.
      if (Tab.IsActive)
        return;

      foreach (var EachTab in TabList)
        EachTab.IsActive = EachTab == Tab;
    }

    private void RefreshTab(Tab Tab)
    {
      if (IsLightened)
        Tab.Lighten();
      else
        Tab.Darken();
    }
    private void RefreshTheme()
    {
      foreach (var Tab in TabList)
        RefreshTab(Tab);
    }

    private bool IsLightened;
    private Inv.DistinctList<Tab> TabList;
  }

  public sealed class TabBarView
  {
    public TabBarView()
    {
      this.TabFrameDictionary = new Dictionary<Inv.Material.Tab, FrameControl>();

      this.TabBar = new TabBar();
      TabBar.ChangeEvent += () =>
      {
        TransitionToActive();
      };

      this.ContentFrame = Inv.Frame.New();
    }

    public Inv.Material.TabBar TabBar { get; private set; }
    public Inv.Frame Content => ContentFrame;

    public Tab AddTab(string Title, Func<Inv.Panel> BuildContentQuery)
    {
      var Result = TabBar.AddTab(Title);

      var TabFrame = new FrameControl(TabFrameDictionary.Count, Result, BuildContentQuery);

      TabFrameDictionary.Add(Result, TabFrame);

      return Result;
    }
    public Inv.Transition Activate(Tab Tab)
    {
      TabBar.InternalActivate(Tab);

      return TransitionToActive();
    }
    public void RemoveTabs()
    {
      TabBar.RemoveTabs();
      TabFrameDictionary.Clear();
      ContentFrame.Content = null;
      this.ActiveFrame = null;
    }
    public void RefreshActive()
    {
      var Content = ActiveFrame?.BuildContent();

      ContentFrame.Transition(Content).Fade();
    }

    private Inv.Transition TransitionToActive()
    {
      var ActiveTab = TabBar.ActiveTab;
      var TabFrame = TabFrameDictionary.GetValueOrDefault(ActiveTab);
      var TabFrameContent = TabFrame?.BuildContent();

      var Transition = ContentFrame.Transition(TabFrameContent);

      if (ActiveFrame != null)
      {
        if (ActiveFrame.Index < TabFrame.Index)
          Transition.CarouselNext();
        else
          Transition.CarouselPrevious();
      }
      else
      {
        // Don't animate on first load.
        if (ContentFrame.Content == null)
          Transition.None();
        else
          Transition.Fade();
      }

      this.ActiveFrame = TabFrame;

      return Transition;
    }

    private Inv.Frame ContentFrame;
    private FrameControl ActiveFrame;
    private Dictionary<Inv.Material.Tab, FrameControl> TabFrameDictionary;

    private sealed class FrameControl
    {
      public FrameControl(int Index, Tab Tab, Func<Inv.Panel> BuildContentQuery)
      {
        this.Index = Index;
        this.Tab = Tab;
        this.BuildContentQuery = BuildContentQuery;
      }

      public Tab Tab { get; private set; }
      public int Index { get; private set; }

      public Inv.Panel BuildContent()
      {
        return BuildContentQuery();
      }

      private readonly Func<Inv.Panel> BuildContentQuery;
    }
  }

  // https://material.io/guidelines/components/buttons.html#buttons-style
  public sealed class Button : Inv.Panel<Inv.Button>
  {
    internal Button()
    {
      this.Base = Inv.Button.NewFlat();
      Base.Corner.Set(2);
      Base.Size.SetHeight(36);
      Base.Size.SetMinimumWidth(64);

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Alignment.Center();
      Label.Margin.Set(16, 8);
      Label.Font.Size = 14;
      Label.Font.Name = Theme.FontName;
      Label.Font.Medium();
      Label.Font.Colour = Theme.TextColour;

      // 2dp corner radius
      // 8dp left/right margin
      // 8dp left/right padding

      // if touch is present
      //   36dp content height
      //   48dp touch height
      //   16dp left/right padding
      //   14pt Roboto medium
      //   64dp min content width
      //   88dp min width
      // else
      //   32dp content height
      //   40dp touch height
      //   16dp left/right padding
      //   13pt Roboto medium

      // Light
      //  Disabled text 26% #000000
      // Dark
      //  Disabled text 30% #FFFFFF

      // Focused 12% opacity #000000
    }

    public string Caption
    {
      set { Label.Text = value.ToUpper(); }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Font Font => Label.Font;
    public Inv.Background Background => Base.Background;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    internal Inv.Elevation Elevation => Base.Elevation;

    private Inv.Label Label;
  }

  // https://material.io/guidelines/components/buttons.html#buttons-flat-buttons
  public sealed class FlatButton : Inv.Panel<Inv.Material.Button>
  {
    public FlatButton()
    {
      this.Base = new Button();

      // Light
      //  Pressed 40% #999999
      // Dark
      //  Pressed 25% #CCCCCC
    }

    public string Caption
    {
      set { Base.Caption = value; }
    }
    public Inv.Colour CaptionColour
    {
      get { return Base.Font.Colour; }
      set { Base.Font.Colour = value; }
    }
    public Inv.Background Background => Base.Background;
    public Inv.Margin Margin => Base.Margin;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
  }

  // https://material.io/guidelines/components/buttons.html#buttons-raised-buttons
  public sealed class RaisedButton : Inv.Panel<Inv.Material.Button>
  {
    public RaisedButton()
    {
      this.Base = new Button();
      Base.Elevation.Set(2);
      Base.Background.Colour = Inv.Material.Theme.PrimaryColour;

      // 2dp elevation
      // Desktop - elevate on hover

      // Light
      //  Disabled button 12% #000000
      // Dark
      //  Normal colour 500
      //  Pressed colour 700
      //  Disabled button 12% #FFFFFF
    }

    public string Caption
    {
      set { Base.Caption = value; }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Font Font => Base.Font;
    public Inv.Background Background => Base.Background;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
  }

  public sealed class FloatingActionContainer : Inv.Panel<Inv.Frame>
  {
    internal FloatingActionContainer(AppLayout Layout)
    {
      this.Layout = Layout;

      this.Base = Inv.Frame.New();
    }

    public Inv.Visibility Visibility => Base.Visibility;

    public FloatingActionButton AsButton()
    {
      if (Button == null)
      {
        Button = new FloatingActionButton();
      }
      else
      {
        Button.Clear();
        Button.RemoveSingleTap();
      }

      InitialiseButton();

      Base.Content = Button;

      return Button;
    }
    public FloatingActionSpeedDial AsSpeedDial()
    {
      if (SpeedDial == null)
        SpeedDial = new FloatingActionSpeedDial(Layout);
      else
        SpeedDial.Clear();

      InitialiseSpeedDial();

      Base.Content = SpeedDial;

      return SpeedDial;
    }

    internal void Clear()
    {
      Base.Content = null;

      if (Button != null)
      {
        Button.Clear();

        InitialiseButton();
      }

      if (SpeedDial != null)
      {
        SpeedDial.Clear();

        InitialiseSpeedDial();
      }
    }

    private void InitialiseButton()
    {
      Button.Margin.Set(0, 0, 16, 16);
      Button.Alignment.BottomRight();
      Button.Visibility.Show();
    }
    private void InitialiseSpeedDial()
    {
      SpeedDial.Margin.Set(0, 0, 16, 16);
      SpeedDial.Alignment.BottomRight();
      SpeedDial.Visibility.Show();
    }

    private readonly AppLayout Layout;
    private FloatingActionButton Button;
    private FloatingActionSpeedDial SpeedDial;
  }

  public sealed class FloatingActionButton : Inv.Panel<Inv.Button>
  {
    public FloatingActionButton()
    {
      this.Base = Inv.Button.NewFlat();
      Base.Elevation.Set(2);
      Base.Background.Colour = Theme.PrimaryColour;
      Base.SingleTapEvent += () =>
      {
        if (SingleTapEvent != null)
          SingleTapEvent();
      };

      var Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;
      Stack.Alignment.Stretch();

      this.Icon = new Inv.Material.Icon();
      Stack.AddPanel(Icon);
      Icon.Colour = Theme.PrimaryTextColour;
      Icon.Alignment.CenterStretch();
      Icon.Size.Set(24);

      this.Label = Inv.Label.New();
      Stack.AddPanel(Label);
      Label.Margin.Set(12, 0, 8, 0);
      Label.Font.Name = Theme.FontName;
      Label.Font.Medium();
      Label.Font.Size = 13;
      Label.Font.Colour = Icon.Colour;

      Refresh();
    }

    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get { return Icon.Image; }
      set { Icon.Image = value; }
    }
    public Inv.Colour BackgroundColour
    {
      set
      {
        Base.Background.Colour = value;
        Icon.Colour = value.BackgroundToBlackWhiteForeground();
        Label.Font.Colour = Icon.Colour;
      }
    }
    public string Text
    {
      get { return Label.Text; }
      set
      {
        Label.Text = value?.ToUpper();
        Refresh();
      }
    }
    public event Action SingleTapEvent;

    internal void Clear()
    {
      Text = null;
      Image = null;
    }
    internal void RemoveSingleTap()
    {
      SingleTapEvent = null;
    }

    private void Refresh()
    {
      if (!string.IsNullOrWhiteSpace(Text))
      {
        Base.Size.Set(null, 48);
        Base.Corner.Set(24);
        Base.Padding.Set(16, 0, 16, 0);

        Label.Visibility.Show();
      }
      else
      {
        Base.Size.Set(56);
        Base.Corner.Set(28);
        Base.Padding.Set(16);

        Label.Visibility.Collapse();
      }
    }

    private Inv.Material.Icon Icon;
    private Inv.Label Label;
  }

  public sealed class FloatingActionSpeedDial : Inv.Panel<Inv.Material.FloatingActionButton>
  {
    internal FloatingActionSpeedDial(AppLayout Layout)
    {
      this.Layout = Layout;
      this.ButtonList = new Inv.DistinctList<FloatingActionSpeedDialButton>();

      this.Base = new FloatingActionButton();
      Base.SingleTapEvent += () =>
      {
        ButtonDock.Alignment.Set(Base.Alignment.Get());
        ButtonDock.Margin.Set(Base.Margin);

        CancelButton.Alignment.Set(Base.Alignment.Get());

        foreach (var Button in ButtonList)
        {
          switch (Base.Alignment.Get())
          {
            case Placement.TopLeft:
            case Placement.CenterLeft:
            case Placement.StretchLeft:
            case Placement.BottomLeft:
              Button.Alignment.CenterLeft();
              break;

            case Placement.TopCenter:
            case Placement.Center:
            case Placement.StretchCenter:
            case Placement.BottomCenter:
              Button.Alignment.Center();
              break;

            case Placement.TopRight:
            case Placement.CenterRight:
            case Placement.StretchRight:
            case Placement.BottomRight:
              Button.Alignment.CenterRight();
              break;

            case Placement.TopStretch:
            case Placement.CenterStretch:
            case Placement.Stretch:
            case Placement.BottomStretch:
              Button.Alignment.Stretch();
              break;

            default:
              break;
          }
        }

        Layout.AddPanel(Scrim);
        Layout.AddPanel(ButtonDock);

        Base.Visibility.Collapse();
      };

      this.Scrim = new Scrim();
      Scrim.SingleTapEvent += () => HideSpeedDial();
      Scrim.Visibility.Show();

      this.ButtonDock = Inv.Dock.NewVertical();

      this.CancelButton = new FloatingActionButton();
      ButtonDock.AddFooter(CancelButton);
      CancelButton.Image = Resources.Content.Clear;
      CancelButton.SingleTapEvent += () => HideSpeedDial();
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get => Base.Image;
      set => Base.Image = value;
    }
    public string Text
    {
      get => Base.Text;
      set => Base.Text = value;
    }

    public FloatingActionSpeedDialButton AddButton()
    {
      var Result = new FloatingActionSpeedDialButton(() => HideSpeedDial());

      ButtonDock.AddHeader(Result);

      ButtonList.Add(Result);

      return Result;
    }

    internal void Clear()
    {
      Base.Clear();

      ButtonDock.RemovePanels();
      ButtonList.Clear();
    }

    private void HideSpeedDial()
    {
      Layout.RemovePanel(Scrim);
      Layout.RemovePanel(ButtonDock);

      Base.Visibility.Show();
    }

    private Inv.Material.AppLayout Layout;
    private Inv.Material.Scrim Scrim;
    private Inv.Dock ButtonDock;
    private Inv.Material.FloatingActionButton CancelButton;
    private Inv.DistinctList<FloatingActionSpeedDialButton> ButtonList;
  }

  public sealed class FloatingActionSpeedDialButton : Inv.Panel<Inv.Material.FloatingActionButton>
  {
    internal FloatingActionSpeedDialButton(Action HideSpeedDialAction)
    {
      this.Base = new FloatingActionButton();
      Base.Alignment.Center();
      Base.Margin.Set(0, 0, 0, 8);
      Base.BackgroundColour = Inv.Material.Theme.BlankColour;
      Base.SingleTapEvent += () =>
      {
        if (HideSpeedDialAction != null)
          HideSpeedDialAction();

        if (SingleTapEvent != null)
          SingleTapEvent();
      };
    }

    public Inv.Image Image
    {
      get { return Base.Image; }
      set { Base.Image = value; }
    }
    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent;

    internal Inv.Alignment Alignment => Base.Alignment;
  }

  public sealed class Icon : Inv.Panel<Inv.Graphic>
  {
    public Icon()
    {
      this.Base = Inv.Graphic.New();
      //Base.Size.Set(24);

      this.ColourField = Inv.Material.Theme.PrimaryColour;
    }

    public Inv.Size Size => Base.Size;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Padding Padding => Base.Padding;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Image Image
    {
      get { return ImageField; }
      set
      {
        if (ImageField != value)
        {
          ImageField = value;

          Refresh();
        }
      }
    }
    public Inv.Colour Colour
    {
      get { return ColourField; }
      set
      {
        if (ColourField != value)
        {
          ColourField = value;

          Refresh();
        }
      }
    }

    private void Refresh()
    {
      Base.Image = Inv.Material.Icon.Tint(ImageField, ColourField);
    }

    private Inv.Colour ColourField;
    private Inv.Image ImageField;

    static Icon()
    {
      TintCacheDictionary = new Dictionary<Inv.Image, TintCache>();
    }

    private static Inv.Image Tint(Inv.Image Image, Inv.Colour Colour)
    {
      if (Image == null)
        return null;

      var TintCache = TintCacheDictionary.GetValueOrDefault(Image);
      if (TintCache == null)
      {
        TintCache = new TintCache();
        TintCacheDictionary.Add(Image, TintCache);
      }

      var Result = TintCache.ColourImageDictionary.GetValueOrDefault(Colour);

      if (Result == null)
      {
        Result = Inv.Application.Access().Graphics.Tint(Image, Colour);
        TintCache.ColourImageDictionary.Add(Colour, Result);
      }

      return Result;
    }

    private static Dictionary<Inv.Image, TintCache> TintCacheDictionary;

    private sealed class TintCache
    {
      public TintCache()
      {
        this.ColourImageDictionary = new Dictionary<Inv.Colour, Inv.Image>();
      }

      public Dictionary<Inv.Colour, Inv.Image> ColourImageDictionary { get; private set; }
    }
  }

  public sealed class IconButton : Inv.Panel<Inv.Button>
  {
    public IconButton()
    {
      this.Base = Inv.Button.NewFlat();

      this.Icon = new Inv.Material.Icon();
      Base.Content = Icon;
    }

    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public Inv.Padding Padding => Base.Padding;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Image Image
    {
      set { Icon.Image = value; }
    }
    public Inv.Colour ForegroundColour
    {
      get { return Icon.Colour; }
      set { Icon.Colour = value; }
    }
    public Inv.Background Background => Base.Background;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public bool HasSingleTap()
    {
      return Base.HasSingleTap();
    }
    public void SetSize(int Size)
    {
      Base.Size.Set(Size);

      Base.Corner.Set(Size / 2);
    }

    private Inv.Material.Icon Icon;
  }

  public sealed class PropertyField : Inv.Panel<Inv.Stack>
  {
    internal PropertyField(PropertyValidator Validator)
    {
      this.Validator = Validator;
      this.InherentReportList = new Inv.DistinctList<PropertyRuleReport>();
      this.TransientReportList = new Inv.DistinctList<PropertyRuleReport>();

      this.IsEmpty = true;
      this.IsFocused = false;
      this.CanActivate = true;

      this.Base = Inv.Stack.NewVertical();

      this.Button = Inv.Button.NewStark();
      Base.AddPanel(Button);
      Button.Size.SetMinimumHeight(56);
      Button.Corner.Set(4, 4, 0, 0);
      Button.Background.Colour = Inv.Material.Theme.SmokeColour;
      Button.Border.Set(0, 0, 0, 1);
      Button.Border.Colour = Inv.Material.Theme.SoftColour;
      Button.Padding.Set(12, 0, 0, 0);
      Button.SingleTapEvent += ActivateHandler;

      var ContentOverlay = Inv.Overlay.New();
      Button.Content = ContentOverlay;

      this.GhostEdit = Inv.Edit.NewText();
      ContentOverlay.AddPanel(GhostEdit);
      GhostEdit.Size.Set(1);
      GhostEdit.Focus.GotEvent += () => Activate();

      this.GhostLabel = Inv.Label.New();
      ContentOverlay.AddPanel(GhostLabel);
      GhostLabel.Alignment.CenterStretch();
      GhostLabel.Margin.Set(0, 0, 0, 1);
      GhostLabel.Font.Name = Theme.FontName;
      GhostLabel.Font.Size = 16;
      GhostLabel.Font.Colour = Inv.Material.Theme.SubtleColour.Lighten(0.05F);

      this.ClientDock = Inv.Dock.NewVertical();
      ContentOverlay.AddPanel(ClientDock);
      ClientDock.Margin.Set(0, 0, 0, 1);
      ClientDock.Visibility.Collapse();

      this.CaptionLabel = Inv.Label.New();
      ClientDock.AddHeader(CaptionLabel);
      CaptionLabel.Margin.Set(0, 8, 0, 0);
      CaptionLabel.Font.Name = Theme.FontName;
      CaptionLabel.Font.Size = 12;
      CaptionLabel.Font.Colour = Theme.TextColour;

      var ContentDock = Inv.Dock.NewHorizontal();
      ClientDock.AddClient(ContentDock);
      ContentDock.Padding.Set(0, 0, 8, 0);

      this.ContentFrame = Inv.Frame.New();
      ContentDock.AddClient(ContentFrame);
      ContentFrame.Padding.Set(0, 4, 0, 4);
      ContentFrame.Border.Set(0, 0, 0, 2);

      this.ClearButton = new Inv.Material.IconButton();
      ContentDock.AddFooter(ClearButton);
      ClearButton.Margin.Set(8, 0, 0, 4);
      ClearButton.SetSize(24);
      ClearButton.Image = Inv.Material.Resources.Content.Clear;
      ClearButton.Alignment.Center();
      ClearButton.Visibility.Collapse();
      ClearButton.SingleTapEvent += () =>
      {
        if (ClearEvent != null)
          ClearEvent();

        Refresh();
      };

      this.ErrorIcon = new Inv.Material.Icon();
      ContentDock.AddFooter(ErrorIcon);
      ErrorIcon.Size.Set(24);
      ErrorIcon.Colour = Inv.Colour.Red;
      ErrorIcon.Image = Inv.Material.Resources.Alert.Error;
      ErrorIcon.Margin.Set(8, 0, 0, 4);
      ErrorIcon.Alignment.Center();

      this.HelperLabel = Inv.Label.New();
      Base.AddPanel(HelperLabel);
      HelperLabel.Margin.Set(12, 6, 0, 0);
      HelperLabel.Size.SetHeight(16);
      HelperLabel.Alignment.BottomLeft();
      HelperLabel.Font.Name = Theme.FontName;
      HelperLabel.Font.Size = 11;
      HelperLabel.Font.Colour = Theme.TextColour;

      this.MandatoryRule = Validator.AddRule(this);
      MandatoryRule.CheckEvent += (Report) =>
      {
        if (IsMandatoryField && IsEmpty)
          return Report.Fail(CaptionText + " " + (IsMandatoryPlural ? "are" : "is") + " required");

        return Report.Pass();
      };

      Refresh();
    }

    public bool IsMandatory
    {
      get { return IsMandatoryField; }
      set
      {
        this.IsMandatoryField = value;
        Refresh();
      }
    }
    public bool IsEnabled
    {
      get => Button.IsEnabled;
      set => Button.IsEnabled = value;
    }
    public string CaptionText
    {
      get { return CaptionLabel.Text; }
      set
      {
        CaptionLabel.Text = value;
        GhostLabel.Text = value;
      }
    }
    public string ErrorText
    {
      get { return ErrorTextField; }
      set
      {
        this.ErrorTextField = value;

        Refresh();
      }
    }
    public string HelperText
    {
      get { return HelperTextField; }
      set
      {
        this.HelperTextField = value;
        Refresh();
      }
    }
    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public Inv.Focus Focus { get; set; }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public PropertyValidator Validator { get; private set; }
    public event Action ChangeEvent;

    public void Activate(Inv.Focus OverrideFocus = null)
    {
      if (!CanActivate)
        return;

      if (!IsActivated)
      {
        this.IsActivated = true;

        RefreshActivated();

        var Focus = OverrideFocus ?? this.Focus;
        if (Focus != null)
          Focus.Set();

        Button.SingleTapEvent -= ActivateHandler;
      }
    }
    public void AsButton(Action SingleTapEvent, Action ClearEvent = null)
    {
      this.CanActivate = false;
      this.Focus = Button.Focus;
      this.SingleTapEvent = SingleTapEvent;
      this.ClearEvent = ClearEvent;
      Button.IsFocusable = true;
    }

    internal bool IsEmpty { get; private set; }
    internal bool IsMandatoryPlural { get; set; }

    internal void Deactivate()
    {
      if (IsActivated)
      {
        this.IsActivated = false;

        RefreshActivated();

        Button.SingleTapEvent += ActivateHandler;
      }
    }
    internal void GotFocus()
    {
      if (!IsFocused)
      {
        this.IsFocused = true;

        Refresh();
      }
    }
    internal void LostFocus()
    {
      if (IsFocused)
      {
        this.IsFocused = false;

        Deactivate();

        Refresh();
      }
    }
    internal void SetEmpty(bool IsEmpty)
    {
      this.IsEmpty = IsEmpty;

      Refresh();
    }
    internal void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
    internal PropertyRule AddRule()
    {
      return Validator.AddRule(this);
    }
    internal void AddInherentReport(PropertyRuleReport Report)
    {
      InherentReportList.Add(Report);
    }
    internal void AddTransientReport(PropertyRuleReport Report)
    {
      TransientReportList.Add(Report);
    }
    internal void RemoveTransientReports()
    {
      TransientReportList.Clear();
    }
    internal void ReportSet(string Message)
    {
      ErrorText = Message;
    }
    internal void ReportRefresh()
    {
      foreach (var Report in InherentReportList.Union(TransientReportList))
      {
        if (!string.IsNullOrWhiteSpace(Report.Message))
        {
          ReportSet(Report.Message);
          return;
        }
      }

      ReportSet(null);
    }

    private void Refresh()
    {
      GhostLabel.Visibility.Set(IsEmpty && !IsActivated && !IsFocused);
      GhostEdit.Visibility.Set(GhostLabel.Visibility.Get());
      ClearButton.Visibility.Set(!IsMandatoryField && (!CanActivate || IsActivated && IsFocused) && !IsEmpty && ClearEvent != null);

      ClientDock.Visibility.Set(!IsEmpty || IsActivated || IsFocused);
      CaptionLabel.Font.Colour = !string.IsNullOrWhiteSpace(ErrorText) ? Inv.Colour.Red : IsFocused ? Inv.Material.Theme.PrimaryColour : Inv.Material.Theme.SubtleColour;

      var SetHelperText = IsMandatoryField && IsEmpty ? "Required" : HelperTextField;
      HelperLabel.Text = string.IsNullOrWhiteSpace(ErrorTextField) ? SetHelperText : ErrorTextField;
      HelperLabel.Font.Colour = string.IsNullOrWhiteSpace(ErrorTextField) ? Inv.Material.Theme.SubtleColour : Inv.Colour.Red;
      ErrorIcon.Visibility.Set(IsFocused && !string.IsNullOrWhiteSpace(ErrorTextField));

      if (!string.IsNullOrWhiteSpace(ErrorTextField))
        Button.Border.Colour = Inv.Colour.Red;
      else
        Button.Border.Colour = IsFocused ? Inv.Material.Theme.PrimaryColour : Inv.Material.Theme.SoftColour;

      Button.Border.Set(0, 0, 0, IsFocused ? 2 : 1);
      GhostLabel.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
      ClientDock.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
    }
    private void RefreshActivated()
    {
      GhostLabel.Visibility.Set(!IsActivated);
      GhostEdit.Visibility.Set(GhostLabel.Visibility.Get());
      ClientDock.Visibility.Set(IsActivated);
    }
    private void ActivateHandler()
    {
      if (SingleTapEvent != null)
        SingleTapEvent();

      if (CanActivate)
        Activate();
    }

    private Inv.Button Button;
    private Inv.Edit GhostEdit;
    private Inv.Label GhostLabel;
    private Inv.Dock ClientDock;
    private Inv.Label CaptionLabel;
    private Inv.Frame ContentFrame;
    private Inv.Material.IconButton ClearButton;
    private Inv.Material.Icon ErrorIcon;
    private Inv.Label HelperLabel;
    private bool IsActivated;
    private bool IsFocused;
    private bool IsMandatoryField;
    private bool CanActivate;
    private string ErrorTextField;
    private string HelperTextField;
    private PropertyRule MandatoryRule;
    private Inv.DistinctList<PropertyRuleReport> InherentReportList;
    private Inv.DistinctList<PropertyRuleReport> TransientReportList;
    private event Action SingleTapEvent;
    private event Action ClearEvent;
  }

  public sealed class Integer32Field : Inv.Panel<PropertyField>
  {
    public Integer32Field(PropertyField Base)
    {
      this.Base = Base;

      this.Edit = Inv.Edit.NewInteger();
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Name = Theme.FontName;
      Edit.Font.Size = 16;
      Edit.Font.Colour = Theme.TextColour;
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        if (ChangeEvent != null)
          ChangeEvent();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();

      var ValidRule = Base.AddRule();
      ValidRule.CheckEvent += (Report) =>
      {
        if (!Base.IsEmpty && Content == null)
          return Report.Fail("Must be a valid number");

        return Report.Pass();
      };

      this.MinimumValueRule = Base.AddRule();
      MinimumValueRule.CheckEvent += (Report) =>
      {
        if (MinimumValueField != null && Content != null && Content < MinimumValueField)
          return Report.Fail("Must be greater than " + MinimumValueField.Value.ToString());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public int? Content
    {
      get
      {
        var TrimValue = Edit.Text.NullAsEmpty().TrimStart('0');

        if (int.TryParse(TrimValue, out var Result))
          return Result;

        return null;
      }
      set => Edit.Text = value?.ToString();
    }
    public int? MinimumValue
    {
      get => MinimumValueField;
      set
      {
        MinimumValueField = value;
        MinimumValueRule.IsEnabled = value != null;
      }
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add => Edit.ReturnEvent += value;
      remove => Edit.ReturnEvent -= value;
    }
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);

    private Inv.Edit Edit;
    private int? MinimumValueField;
    private PropertyRule MinimumValueRule;
  }

  public sealed class DecimalField : Inv.Panel<PropertyField>
  {
    public DecimalField(PropertyField Base)
    {
      this.Base = Base;

      this.Edit = Inv.Edit.NewDecimal();
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Name = Theme.FontName;
      Edit.Font.Size = 16;
      Edit.Font.Colour = Theme.TextColour;
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        if (ChangeEvent != null)
          ChangeEvent();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public decimal? Content
    {
      get
      {
        var TrimValue = Edit.Text.NullAsEmpty();

        if (decimal.TryParse(TrimValue, out var Result))
          return Result;

        return null;
      }
      set { Edit.Text = value?.ToString(); }
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add { Edit.ReturnEvent += value; }
      remove { Edit.ReturnEvent -= value; }
    }
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);

    private Inv.Edit Edit;
  }

  public sealed class MoneyField : Inv.Panel<PropertyField>
  {
    public MoneyField(PropertyField Base)
    {
      this.Base = Base;
      this.DecimalField = new Inv.Material.DecimalField(Base);
    }

    public bool IsMandatory
    {
      get => DecimalField.IsMandatory;
      set => DecimalField.IsMandatory = value;
    }
    public string CaptionText
    {
      get => DecimalField.CaptionText;
      set => DecimalField.CaptionText = value;
    }
    public Inv.Money? Content
    {
      get
      {
        var DecimalValue = DecimalField.Content;

        return DecimalValue != null ? new Inv.Money(DecimalValue.Value) : (Inv.Money?)null;
      }
      set { DecimalField.Content = value?.GetAmount(); }
    }
    public string ErrorText
    {
      get => DecimalField.ErrorText;
      set => DecimalField.ErrorText = value;
    }
    public string HelperText
    {
      get => DecimalField.HelperText;
      set => DecimalField.HelperText = value;
    }
    public Inv.Margin Margin => DecimalField.Margin;
    public Inv.Alignment Alignment => DecimalField.Alignment;
    public Inv.Visibility Visibility => DecimalField.Visibility;
    public event Action ReturnEvent
    {
      add { DecimalField.ReturnEvent += value; }
      remove { DecimalField.ReturnEvent -= value; }
    }
    public event Action ChangeEvent
    {
      add => DecimalField.ChangeEvent += value;
      remove => DecimalField.ChangeEvent -= value;
    }

    public Inv.Focus GetFocus() => DecimalField.GetFocus();
    public bool IsEmpty() => DecimalField.IsEmpty();

    private Inv.Material.DecimalField DecimalField;
  }

  public sealed class EditField : Inv.Panel<PropertyField>
  {
    public EditField(PropertyField Base, Inv.EditInput EditInput)
    {
      this.Base = Base;

      this.Edit = Inv.Edit.New(EditInput);
      Base.Focus = Edit.Focus;
      Base.Content = Edit;
      Edit.Font.Name = Theme.FontName;
      Edit.Font.Size = 16;
      Edit.Font.Colour = Theme.TextColour;
      Edit.ChangeEvent += () =>
      {
        Base.SetEmpty(IsEmpty());

        Base.ChangeInvoke();

        if (ChangeEvent != null)
          ChangeEvent();
      };
      Edit.Focus.GotEvent += () => Base.GotFocus();
      Edit.Focus.LostEvent += () => Base.LostFocus();
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public bool IsReadOnly
    {
      get => Edit.IsReadOnly;
      set => Edit.IsReadOnly = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string Content
    {
      get => Edit.Text;
      set => Edit.Text = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ReturnEvent
    {
      add { Edit.ReturnEvent += value; }
      remove { Edit.ReturnEvent -= value; }
    }
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Edit.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);
    public void Activate() => Base.Activate();

    public static implicit operator Inv.Material.PropertyField(EditField Self) => Self?.Base;

    private Inv.Edit Edit;
  }

  public sealed class MemoField : Inv.Panel<PropertyField>
  {
    public MemoField(PropertyField Base)
    {
      this.Base = Base;

      this.Memo = Inv.Memo.New();
      Base.Focus = Memo.Focus;
      Base.Content = Memo;
      Memo.Font.Name = Theme.FontName;
      Memo.Font.Size = 16;
      Memo.Font.Colour = Theme.TextColour;
      Memo.ChangeEvent += () =>
      {
        Base.SetEmpty(string.IsNullOrWhiteSpace(Memo.Text));

        Base.ChangeInvoke();

        if (ChangeEvent != null)
          ChangeEvent();
      };
      Memo.Focus.GotEvent += () => Base.GotFocus();
      Memo.Focus.LostEvent += () => Base.LostFocus();
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string Content
    {
      get { return Memo.Text; }
      set { Memo.Text = value; }
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Memo.Focus;
    public bool IsEmpty() => string.IsNullOrWhiteSpace(Memo.Text);

    private Inv.Memo Memo;
  }

  public sealed class DateOfBirthField : Inv.Panel<PropertyField>
  {
    public DateOfBirthField(PropertyField Base)
    {
      this.Base = Base;

      var Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.Dock = Inv.Dock.NewHorizontal();
      Overlay.AddPanel(Dock);

      this.DayEdit = Inv.Edit.NewInteger();
      Dock.AddHeader(DayEdit);
      DayEdit.Font.Name = Theme.FontName;
      DayEdit.Font.Size = 16;
      DayEdit.Font.Colour = Theme.TextColour;
      DayEdit.Size.SetMinimumWidth(35);
      DayEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();

        if (DayEdit.Text.Length >= 2)
          MonthEdit.Focus.Set();
      };
      DayEdit.Focus.GotEvent += () => Refresh();
      DayEdit.Focus.LostEvent += () => Refresh();

      this.DayMonthLabel = Inv.Label.New();
      Dock.AddHeader(DayMonthLabel);
      DayMonthLabel.Visibility.Collapse();
      DayMonthLabel.Margin.Set(4, 0);
      DayMonthLabel.Alignment.Center();
      DayMonthLabel.Font.Name = Theme.FontName;
      DayMonthLabel.Font.Size = 16;
      DayMonthLabel.Font.Colour = Theme.TextColour;
      DayMonthLabel.Text = "/";

      this.MonthEdit = Inv.Edit.NewInteger();
      Dock.AddHeader(MonthEdit);
      MonthEdit.Font.Name = Theme.FontName;
      MonthEdit.Font.Size = 16;
      MonthEdit.Font.Colour = Theme.TextColour;
      MonthEdit.Size.SetMinimumWidth(35);
      MonthEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();

        if (MonthEdit.Text.Length >= 2)
          YearEdit.Focus.Set();
      };
      MonthEdit.Focus.GotEvent += () => Refresh();
      MonthEdit.Focus.LostEvent += () => Refresh();

      this.MonthYearLabel = Inv.Label.New();
      Dock.AddHeader(MonthYearLabel);
      MonthYearLabel.Visibility.Collapse();
      MonthYearLabel.Margin.Set(4, 0);
      MonthYearLabel.Alignment.Center();
      MonthYearLabel.Font.Name = Theme.FontName;
      MonthYearLabel.Font.Size = 16;
      MonthYearLabel.Font.Colour = Theme.TextColour;
      MonthYearLabel.Text = "/";

      this.YearEdit = Inv.Edit.NewInteger();
      Dock.AddClient(YearEdit);
      YearEdit.Font.Name = Theme.FontName;
      YearEdit.Font.Size = 16;
      YearEdit.Font.Colour = Theme.TextColour;
      YearEdit.Size.SetMinimumWidth(35);
      YearEdit.ChangeEvent += () =>
      {
        Refresh();

        ChangeInvoke();
      };
      YearEdit.Focus.GotEvent += () => Refresh();
      YearEdit.Focus.LostEvent += () => Refresh();

      this.FocusEdit = Inv.Edit.NewText();
      Base.Focus = FocusEdit.Focus;
      Overlay.AddPanel(FocusEdit);
      FocusEdit.Focus.GotEvent += () =>
      {
        DayMonthLabel.Visibility.Show();
        MonthYearLabel.Visibility.Show();

        DayEdit.Focus.Set();
      };

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        var LeapYear = 2016;

        var DayValue = Day;
        var MonthValue = Month;
        var YearValue = Year;

        if (DayValue == null && MonthValue == null && YearValue == null)
          return Report.Pass();

        if (!IsValidInput())
          return Report.Fail("Invalid date", GetDayFocus());

        if (DayValue == null)
          return Report.Fail("Day is required", GetDayFocus());

        if (DayValue < 1)
          return Report.Fail($"'{ DayValue }' is not a valid day", GetDayFocus());

        if (MonthValue == null)
          return Report.Fail("Month is required", GetMonthFocus());

        if (MonthValue < 1 || MonthValue > 12)
          return Report.Fail($"'{ MonthValue }' is not a valid month", GetMonthFocus());

        if (YearValue == null)
        {
          if (DayValue > DateTime.DaysInMonth(LeapYear, MonthValue.Value))
          {
            var Date = new Inv.Date(LeapYear, MonthValue.Value, 1);

            return Report.Fail($"{ Date.ToString("MMMM") } does not have { DayValue } days", GetDayFocus());
          }
        }
        else if (YearValue <= 0 || (YearValue > 99 && YearValue < 1800))
        {
          return Report.Fail($"Unrecognisable year", GetYearFocus());
        }
        else if (DayValue > DateTime.DaysInMonth(YearValue.Value, MonthValue.Value))
        {
          var Date = new Inv.Date(YearValue.Value, MonthValue.Value, 1);

          return Report.Fail($"{ Date.ToString("MMMM yyyy") } does not have { DayValue } days", GetDayFocus());
        }

        return Report.Pass();
      };
    }

    public int? Day
    {
      get { return int.TryParse(DayEdit.Text, out var Result) ? Result : (int?)null; }
      set { DayEdit.Text = value != null ? value.ToString().PadLeft(2, '0') : ""; }
    }
    public int? Month
    {
      get { return int.TryParse(MonthEdit.Text, out var Result) ? Result : (int?)null; }
      set { MonthEdit.Text = value != null ? value.ToString().PadLeft(2, '0') : ""; }
    }
    public int? Year
    {
      get { return int.TryParse(YearEdit.Text, out var Result) ? Result : (int?)null; }
      set { YearEdit.Text = value != null ? (value % 100).ToString() : ""; }
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return (string.IsNullOrWhiteSpace(DayEdit.Text) || DayEdit.Text.IsNumeric()) && (string.IsNullOrWhiteSpace(MonthEdit.Text) || MonthEdit.Text.IsNumeric()) && (string.IsNullOrWhiteSpace(YearEdit.Text) || YearEdit.Text.IsNumeric());
    }
    public Inv.Focus GetFocus() => DayEdit.Focus;
    public Inv.Focus GetDayFocus() => DayEdit.Focus;
    public Inv.Focus GetMonthFocus() => MonthEdit.Focus;
    public Inv.Focus GetYearFocus() => YearEdit.Focus;

    private void Refresh()
    {
      var IsFocused = false;

      if (DayEdit.Focus.Has() || MonthEdit.Focus.Has() || YearEdit.Focus.Has())
      {
        IsFocused = true;

        FocusEdit.Visibility.Collapse();

        Base.GotFocus();
      }
      else
      {
        Base.LostFocus();

        FocusEdit.Visibility.Show();
      }

      DayMonthLabel.Visibility.Set(IsFocused || !IsEmpty());
      MonthYearLabel.Visibility.Set(DayMonthLabel.Visibility.Get());
    }
    private void ChangeInvoke()
    {
      Base.SetEmpty(IsEmpty());

      Base.ChangeInvoke();

      if (ChangeEvent != null)
        ChangeEvent();
    }
    private bool IsEmpty()
    {
      return Day == null && Month == null && Year == null;
    }

    private Inv.Edit FocusEdit;
    private Inv.Dock Dock;
    private Inv.Edit DayEdit;
    private Inv.Label DayMonthLabel;
    private Inv.Edit MonthEdit;
    private Inv.Label MonthYearLabel;
    private Inv.Edit YearEdit;
  }

  public sealed class DateField : Inv.Panel<PropertyField>
  {
    public DateField(PropertyField Base)
    {
      this.Base = Base;
      Base.AsButton(() =>
      {
        var DatePicker = Inv.CalendarPicker.NewDate();
        DatePicker.Value = (ContentField ?? Inv.Date.Now).ToDateTime();
        DatePicker.SelectEvent += () => Content = DatePicker.Value.AsDate();
        DatePicker.Show();
      }, () =>
      {
        this.Content = null;
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Name = Theme.FontName;
      Label.Font.Size = 16;
      Label.Font.Colour = Theme.TextColour;

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (!IsEmpty() && !IsValidInput())
          return Report.Fail("Invalid date", GetFocus());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public Inv.Date? Content
    {
      get
      {
        return ContentField;
      }
      set
      {
        if (ContentField != value)
        {
          ContentField = value;

          Change();
        }
      }
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return ContentField != null;
    }
    public Inv.Focus GetFocus() => Base.Focus;

    public static implicit operator Inv.Material.PropertyField(DateField Self) => Self?.Base;

    private void Change()
    {
      if (ContentField != null)
        Label.Text = ContentField.Value.ToString("dd / MM / yyyy");
      else
        Label.Text = "   /    /     ";

      Base.SetEmpty(ContentField == null);

      Base.ChangeInvoke();

      if (ChangeEvent != null)
        ChangeEvent();
    }
    private bool IsEmpty()
    {
      return ContentField == null;
    }

    private Inv.Label Label;
    private Inv.Date? ContentField;
  }

  public sealed class TimeField : Inv.Panel<PropertyField>
  {
    public TimeField(PropertyField Base)
    {
      this.Base = Base;
      Base.AsButton(() =>
      {
        var TimePicker = Inv.CalendarPicker.NewTime();
        TimePicker.Value = (ContentField ?? DateTime.Now.AsTime()).ToDateTime();
        TimePicker.SelectEvent += () => Content = TimePicker.Value.AsTime();
        TimePicker.Show();
      }, () =>
      {
        this.Content = null;
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Name = Theme.FontName;
      Label.Font.Size = 16;
      Label.Font.Colour = Theme.TextColour;

      var Rule = Base.AddRule();
      Rule.CheckEvent += (Report) =>
      {
        if (!IsEmpty() && !IsValidInput())
          return Report.Fail("Invalid time", GetFocus());

        return Report.Pass();
      };
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public Inv.Time? Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          ContentField = value;

          Change();

        }
      }
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public bool IsValidInput()
    {
      return ContentField != null;
    }
    public Inv.Focus GetFocus() => Base.Focus;

    private void Change()
    {
      if (ContentField != null)
        Label.Text = ContentField.Value.ToString("HH : mm");
      else
        Label.Text = "   :   ";

      Base.SetEmpty(ContentField == null);

      Base.ChangeInvoke();

      if (ChangeEvent != null)
        ChangeEvent();
    }
    private bool IsEmpty()
    {
      return ContentField == null;
    }

    public static implicit operator Inv.Material.PropertyField(TimeField Self) => Self?.Base;

    private Inv.Label Label;
    private Inv.Time? ContentField;
  }

  public sealed class SelectDialog<T>
  {
    internal SelectDialog(Inv.Material.Space Space)
    {
      this.Space = Space;
    }

    public string Title { get; set; }
    public string NullTitle { get; set; }
    public T Content { get; set; }
    public IEqualityComparer<T> EqualityComparer;
    public event Func<T, string> TitleQuery;
    public event Func<T, string> DescriptionQuery;
    public event Action AcceptEvent;
    public event Action CancelEvent;
    public event Action HideEvent;

    public void OrderByTitle()
    {
      this.OrderByTitleField = true;
    }
    public void OrderByComposition()
    {
      this.OrderByTitleField = false;
    }
    public void GroupBy<G>(Func<T, G> KeyQuery, Func<G, string> HeaderQuery)
    {
      this.GroupByCollation = new SelectRowCollation<T, G>(KeyQuery, HeaderQuery);
    }
    public void Show(IEnumerable<T> Values)
    {
      var ConfirmationDialog = Space.NewConfirmationDialog();
      ConfirmationDialog.Title = Title;
      ConfirmationDialog.Alignment.Stretch();
      ConfirmationDialog.HideEvent += () => HideEvent?.Invoke();

      var SelectedContext = this.Content;

      var ListFlow = new Inv.Material.ListFlow<SelectRow<T>>();
      ConfirmationDialog.Content = ListFlow;
      ListFlow.ComposeEvent += (Item) =>
      {
        var Row = Item.Context;

        if (Row.IsNull)
        {
          var Tile = Item.AsTile();
          Tile.Title = NullTitle;
          Tile.LeadIcon(SelectedContext == null ? Resources.Toggle.RadioButtonChecked : Resources.Toggle.RadioButtonUnchecked);
          Tile.SingleTapEvent += () =>
          {
            SelectedContext = default(T);

            ListFlow.Reload();
          };
        }
        else if (Row.Context == null)
        {
          Item.AsSubheader().Text = Row.Header;
        }
        else
        {
          var Context = Row.Context;

          var ListTile = Item.AsTile();
          ListTile.Title = GetTitle(Context, false);
          ListTile.Description = GetDescription(Context);
          ListTile.LeadIcon(IsEqualTo(Context, SelectedContext) ? Resources.Toggle.RadioButtonChecked : Resources.Toggle.RadioButtonUnchecked);
          ListTile.SingleTapEvent += () =>
          {
            SelectedContext = Context;

            ListFlow.Reload();
          };

          if (ListTile.Description != null)
            ListTile.Layout.FormatTwoLine();
          else
            ListTile.Layout.FormatOneLine();
        }
      };

      Inv.DistinctList<SelectRow<T>> RowList;

      if (Values == null)
        RowList = new Inv.DistinctList<SelectRow<T>>();
      else if (GroupByCollation != null)
        RowList = GroupByCollation.GetRows(Values);
      else
        RowList = Values.Select(R => new SelectRow<T>() { Context = R }).ToDistinctList();

      RowList.Sort((A, B) =>
      {
        var Result = GroupByCollation != null ? A.Header.CompareTo(B.Header) : 0;

        if (Result == 0)
          Result = (A.Context != null).CompareTo(B.Context != null);

        if (Result == 0 && A.Context != null && OrderByTitleField)
          Result = GetTitle(A.Context, false).CompareTo(GetTitle(B.Context, false));

        return Result;
      });

      if (!string.IsNullOrWhiteSpace(NullTitle))
        RowList.Add(new SelectRow<T>() { IsNull = true });

      ListFlow.Load(RowList);

      var SearchDock = Inv.Dock.NewHorizontal();
      ConfirmationDialog.AddTitleFooter(SearchDock);
      SearchDock.Padding.Set(8);
      SearchDock.Margin.Set(12, 0, 12, 0);
      SearchDock.Alignment.Center();
      SearchDock.Corner.Set(3);
      SearchDock.Background.Colour = Inv.Material.Theme.SmokeColour;
      //SearchDock.Border.Set(0, 0, 0, 1);
      //SearchDock.Border.Colour = Inv.Material.Theme.EdgeColour;

      var SearchImage = new Inv.Material.Icon();
      SearchDock.AddHeader(SearchImage);
      SearchImage.Margin.Set(0, 0, 8, 0);
      SearchImage.Image = Inv.Material.Resources.Action.Search;
      SearchImage.Size.Set(24, 24);

      var SearchText = "";
      var SearchEdit = Inv.Edit.NewSearch();
      SearchDock.AddClient(SearchEdit);
      SearchEdit.Alignment.CenterStretch();
      SearchEdit.Font.Name = Theme.FontName;
      SearchEdit.Font.Size = 16;
      SearchEdit.Font.Light();
      SearchEdit.Font.Colour = Theme.TextColour;
      SearchEdit.Size.SetWidth(150);

      var Timer = Inv.Application.Access().Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromMilliseconds(500);
      Timer.IntervalEvent += () =>
      {
        Timer.Stop();

        var NewText = SearchEdit.Text.EmptyOrWhitespaceAsNull();

        if (!string.Equals(SearchText, NewText, StringComparison.CurrentCultureIgnoreCase))
        {
          SearchText = NewText;

          var ComposeList = RowList.ToDistinctList();
          if (!string.IsNullOrWhiteSpace(SearchText))
            ComposeList = ComposeList.Where(C => C.Header != null ? C.Header.Contains(SearchText, StringComparison.CurrentCultureIgnoreCase) : GetTitle(C.Context, true).Contains(SearchText, StringComparison.CurrentCultureIgnoreCase)).ToDistinctList();

          ListFlow.Load(ComposeList);
        }
      };

      SearchEdit.ChangeEvent += () =>
      {
        Timer.Restart();
      };

      ConfirmationDialog.SetFocus(SearchEdit.Focus);
      ConfirmationDialog.AddCancelAction(() =>
      {
        CancelEvent?.Invoke();
      });
      ConfirmationDialog.AddAction("OK", () =>
      {
        this.Content = SelectedContext;

        ConfirmationDialog.Hide();

        AcceptEvent?.Invoke();
      });
      ConfirmationDialog.Show();
    }

    internal string GetTitle(T Value, bool IncludeHeader)
    {
      if (Value == null)
        return NullTitle;

      var Result = "";

      if (IncludeHeader && GroupByCollation != null)
        Result = GroupByCollation.GetHeader(Value) + " | ";

      if (TitleQuery != null)
        Result += TitleQuery(Value);
      else
        Result += Value.ToString();

      return Result;
    }
    internal bool IsEqualTo(T A, T B)
    {
      if (EqualityComparer != null)
        return EqualityComparer.Equals(A, B);

      return object.Equals(A, B);
    }

    private string GetDescription(T Value)
    {
      if (DescriptionQuery != null)
      {
        if (Value == null)
          return "";

        return DescriptionQuery(Value);
      }

      return null;
    }

    private Inv.Material.Space Space;
    private bool OrderByTitleField;
    private SelectRowCollation<T> GroupByCollation;
  }

  public sealed class SelectField<T> : Inv.Panel<PropertyField>
  {
    public SelectField(PropertyField Base, Space Space)
    {
      var IsShown = false;

      this.Base = Base;
      Base.AsButton(() =>
      {
        if (IsShown)
          return;

        IsShown = true;

        if (ValueList == null || ValueList.Count == 0)
        {
          var AlertDialog = Space.NewAlertDialog();
          AlertDialog.Title = CaptionText;
          AlertDialog.Message = $"No { CaptionText.Plural().ToLower() } are available.";
          AlertDialog.AddAction("CLOSE", () => AlertDialog.Hide());
          AlertDialog.HideEvent += () => IsShown = false;
          AlertDialog.Show();

          return;
        }

        SelectDialog.Title = Base.CaptionText;
        SelectDialog.Show(ValueList);
      });

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Name = Theme.FontName;
      Label.Font.Size = 16;
      Label.Font.Colour = Theme.TextColour;

      this.SelectDialog = new SelectDialog<T>(Space);
      SelectDialog.HideEvent += () => IsShown = false;
      SelectDialog.AcceptEvent += () => this.Content = SelectDialog.Content;
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public T Content
    {
      get { return ContentField; }
      set
      {
        if (!SelectDialog.IsEqualTo(value, ContentField))
        {
          this.ContentField = value;

          Refresh();

          Change();
        }
      }
    }
    public bool HasContent
    {
      get { return ContentField != null; }
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public string NullTitle
    {
      get => SelectDialog.NullTitle;
      set
      {
        SelectDialog.NullTitle = value;

        Refresh();
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public Inv.Font Font => Label.Font;
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Base.Focus;
    public void Compose(IEnumerable<T> ValueList)
    {
      this.ValueList = ValueList.ToDistinctList();
    }
    public void SetTitleQuery(Func<T, string> TitleQuery)
    {
      SelectDialog.TitleQuery += TitleQuery;
    }
    public void SetDescriptionQuery(Func<T, string> DescriptionQuery)
    {
      SelectDialog.DescriptionQuery += DescriptionQuery;
    }
    public void SetEqualityComparer(IEqualityComparer<T> EqualityComparer)
    {
      SelectDialog.EqualityComparer = EqualityComparer;
    }
    public void OrderByTitle()
    {
      SelectDialog.OrderByTitle();
    }
    public void OrderByComposition()
    {
      SelectDialog.OrderByComposition();
    }
    public void GroupBy<G>(Func<T, G> KeyQuery, Func<G, string> HeaderQuery)
    {
      SelectDialog.GroupBy(KeyQuery, HeaderQuery);
    }

    public static implicit operator Inv.Material.PropertyField(SelectField<T> Self) => Self?.Base;

    private void Refresh()
    {
      Label.Text = SelectDialog.GetTitle(ContentField, true);

      Base.SetEmpty(ContentField == null && string.IsNullOrWhiteSpace(NullTitle));
    }
    private void Change()
    {
      Base.ChangeInvoke();

      if (ChangeEvent != null)
        ChangeEvent();
    }

    private SelectDialog<T> SelectDialog;
    private Inv.Label Label;
    private Inv.DistinctList<T> ValueList;
    private T ContentField;
  }

  public sealed class CollectField<T> : Inv.Panel<PropertyField>
  {
    public CollectField(PropertyField Base, Space Space)
    {
      var IsShown = false;

      this.ContentList = new Inv.DistinctList<T>();
      this.AvailableList = new Inv.DistinctList<T>();

      this.Base = Base;
      Base.IsMandatoryPlural = true;
      Base.AsButton(() =>
      {
        if (IsShown)
          return;

        IsShown = true;

        var CachedSet = EqualityComparer != null ? ContentList.ToHashSetX(EqualityComparer) : ContentList.ToHashSetX();

        var ConfirmationDialog = Space.NewConfirmationDialog();
        ConfirmationDialog.Title = Base.CaptionText;
        ConfirmationDialog.HideEvent += () =>
        {
          IsShown = false;
        };

        var ListFlow = new Inv.Material.ListFlow<SelectRow<T>>();
        ConfirmationDialog.Content = ListFlow;
        ListFlow.ComposeEvent += (Item) =>
        {
          var Row = Item.Context;

          if (Row.Context == null)
          {
            Item.AsSubheader().Text = Row.Header;
          }
          else
          {
            var Context = Row.Context;

            var ListCheck = Item.AsCheck();
            ListCheck.Title = GetTitle(Context);
            ListCheck.IsChecked = CachedSet.Contains(Context);
            ListCheck.ChangeEvent += () =>
            {
              CachedSet.Toggle(Context);
            };
          }
        };

        Inv.DistinctList<SelectRow<T>> RowList;

        if (Collation != null)
          RowList = Collation.GetRows(AvailableList);
        else
          RowList = AvailableList.Select(R => new SelectRow<T>() { Context = R }).ToDistinctList();

        RowList.Sort((A, B) =>
        {
          var Result = Collation != null ? A.Header.CompareTo(B.Header) : 0;

          if (Result == 0)
            Result = (A.Context != null).CompareTo(B.Context != null);

          if (Result == 0 && A.Context != null)
            Result = GetTitle(A.Context).CompareTo(GetTitle(B.Context));

          return Result;
        });

        ListFlow.Load(RowList);

        ConfirmationDialog.AddCancelAction();
        ConfirmationDialog.AddAction("OK", () =>
        {
          this.ContentList = CachedSet.ToDistinctList();

          Change();

          ConfirmationDialog.Hide();
        });

        ConfirmationDialog.Show();
      });

      var Overlay = Inv.Overlay.New();
      Base.Content = Overlay;

      this.Label = Inv.Label.New();
      Overlay.AddPanel(Label);
      Label.Font.Name = Theme.FontName;
      Label.Font.Size = 16;
      Label.Font.Colour = Theme.TextColour;

      this.Wrap = Inv.Wrap.NewHorizontal();
      Overlay.AddPanel(Wrap);
    }

    public bool IsMandatory
    {
      get => Base.IsMandatory;
      set => Base.IsMandatory = value;
    }
    public string CaptionText
    {
      get => Base.CaptionText;
      set => Base.CaptionText = value;
    }
    public string HelperText
    {
      get => Base.HelperText;
      set => Base.HelperText = value;
    }
    public string ErrorText
    {
      get => Base.ErrorText;
      set => Base.ErrorText = value;
    }
    public bool IsEnabled
    {
      get => Base.IsEnabled;
      set => Base.IsEnabled = value;
    }
    public Inv.DistinctList<T> Content
    {
      get { return ContentList.ToDistinctList(); }
      set
      {
        if (value == null)
          return;

        this.ContentList = value.ToDistinctList();

        Change();
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Visibility Visibility => Base.Visibility;
    public event Action ChangeEvent;

    public Inv.Focus GetFocus() => Base.Focus;
    public void Compose(IEnumerable<T> AvailableList)
    {
      this.AvailableList = AvailableList.ToDistinctList();
    }
    public void SetTitleQuery(Func<T, string> TitleQuery)
    {
      this.TitleQuery = TitleQuery;
    }
    public void SetEqualityComparer(IEqualityComparer<T> EqualityComparer)
    {
      this.EqualityComparer = EqualityComparer;
    }
    public void SetCollation<G>(Func<T, G> KeyQuery, Func<G, string> HeaderQuery)
    {
      this.Collation = new SelectRowCollation<T, G>(KeyQuery, HeaderQuery);
    }

    public static implicit operator Inv.Material.PropertyField(CollectField<T> Self) => Self?.Base;

    private void Change()
    {
      Wrap.RemovePanels();

      foreach (var Content in ContentList.OrderBy(C => GetTitle(C)))
      {
        var Chip = new Inv.Material.Chip();
        Wrap.AddPanel(Chip);
        Chip.Text = GetTitle(Content);
        Chip.Margin.Set(0, 4, 8, 4);
      }

      Base.SetEmpty(ContentList.Count == 0);

      Base.ChangeInvoke();

      if (ChangeEvent != null)
        ChangeEvent();
    }
    private void ChangeInvoke()
    {
      Base.SetEmpty(ContentList.Count == 0);

      if (ChangeEvent != null)
        ChangeEvent();
    }
    private bool IsEqualTo(T A, T B)
    {
      if (EqualityComparer != null)
        return EqualityComparer.Equals(A, B);

      return A.Equals(B);
    }
    private string GetTitle(T Value)
    {
      if (TitleQuery != null)
        return TitleQuery(Value);

      return Value.ToString();
    }

    private Inv.Label Label;
    private Inv.Wrap Wrap;
    private Inv.DistinctList<T> ContentList;
    private Inv.DistinctList<T> AvailableList;
    private Func<T, string> TitleQuery;
    private IEqualityComparer<T> EqualityComparer;
    private SelectRowCollation<T> Collation;
  }

  internal sealed class SelectRow<T>
  {
    public bool IsNull { get; set; }
    public string Header { get; set; }
    public T Context { get; set; }
  }

  internal sealed class SelectRowCollation<T>
  {
    public event Func<T, object> KeyQuery;
    public event Func<object, string> HeaderQuery;

    public string GetHeader(T Context)
    {
      return HeaderQuery(KeyQuery(Context));
    }
    public Inv.DistinctList<SelectRow<T>> GetRows(IEnumerable<T> ContextList)
    {
      var Result = new Inv.DistinctList<SelectRow<T>>();

      foreach (var ContextGroup in ContextList.GroupBy(R => KeyQuery(R)))
      {
        var HeaderRow = new SelectRow<T>() { Header = HeaderQuery(ContextGroup.Key) };

        Result.Add(HeaderRow);

        foreach (var Context in ContextGroup)
          Result.Add(new SelectRow<T>() { Header = HeaderRow.Header, Context = Context });
      }

      return Result;
    }
  }

  internal sealed class SelectRowCollation<T, G>
  {
    internal SelectRowCollation(Func<T, G> KeyQuery, Func<G, string> HeaderQuery)
    {
      this.Base = new SelectRowCollation<T>();
      Base.KeyQuery += (Context) =>
      {
        if (KeyQuery != null)
          return KeyQuery(Context);

        return null;
      };
      Base.HeaderQuery += (Group) =>
      {
        if (HeaderQuery != null)
          return HeaderQuery((G)Group);

        return "";
      };
    }

    public static implicit operator SelectRowCollation<T>(SelectRowCollation<T, G> Collation) => Collation?.Base;

    private SelectRowCollation<T> Base;
  }

  public sealed class Switch : Inv.Panel<Inv.Switch>
  {
    public Switch()
    {
      this.Base = Inv.Switch.New();
      Base.PrimaryColour = Inv.Material.Theme.PrimaryColour;
      Base.SecondaryColour = Inv.Material.Theme.PrimaryColour.Lighten(0.50F);
    }

    public bool IsOn
    {
      get { return Base.IsOn; }
      set { Base.IsOn = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ChangeEvent
    {
      add { Base.ChangeEvent += value; }
      remove { Base.ChangeEvent -= value; }
    }
  }

  public sealed class FullScreenDialog
  {
    internal FullScreenDialog(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Plane = new Inv.Material.Plane();
      Plane.HideEvent += () => Hide();

      this.TransitionFrame = Inv.Frame.New();
      TransitionFrame.Alignment.Stretch();
      TransitionFrame.Background.Colour = Inv.Material.Theme.BlankColour;

      this.ContentFrame = Inv.Frame.New();
    }

    public Inv.Background Background => ContentFrame.Background;
    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public event Action ShowEvent;
    public event Action HideEvent;

    public void SetFocus(Inv.Focus Focus)
    {
      if (Focus != null && IsShown)
        Focus.Set();
      else
        this.Focus = Focus;
    }
    public void Show()
    {
      Space.ShowPanel(TransitionFrame);

      TransitionFrame.Transition(ContentFrame).Fade();

      if (ShowEvent != null)
        ShowEvent();

      if (Focus != null)
      {
        Focus.Set();
        this.Focus = null;
      }

      this.IsShown = true;
    }
    public void Hide()
    {
      this.IsShown = false;

      Space.HidePanel(TransitionFrame);

      if (HideEvent != null)
        HideEvent();
    }

    public static implicit operator Inv.Material.Plane(FullScreenDialog Self) => Self?.Plane;
    
    private Inv.Material.Space Space;
    private Inv.Material.Plane Plane;
    private Inv.Frame TransitionFrame;
    private Inv.Frame ContentFrame;
    private Inv.Focus Focus;
    private bool IsShown;
  }

  internal sealed class BaseDialog
  {
    internal BaseDialog(Inv.Material.Space Space)
    {
      this.Space = Space;

      this.Plane = new Plane();
      Plane.HideEvent += () => Hide();

      this.Overlay = Inv.Overlay.New();

      this.Scrim = new Inv.Material.Scrim();
      Overlay.AddPanel(Scrim);
      Scrim.Visibility.Show();
      Scrim.SingleTapEvent += () => Hide();

      this.ContentFrame = Inv.Frame.New();
      Overlay.AddPanel(ContentFrame);
      ContentFrame.Alignment.CenterStretch();
      ContentFrame.Background.Colour = Inv.Material.Theme.BlankColour;
      ContentFrame.Elevation.Set(4);
      ContentFrame.Corner.Set(2);
    }

    public Inv.Panel Content
    {
      set { ContentFrame.Content = value; }
    }
    public Inv.Alignment Alignment => ContentFrame.Alignment;
    public event Action ShowEvent;
    public event Action HideEvent;

    public void Show()
    {
      Space.ShowPanel(Overlay);

      if (ShowEvent != null)
        ShowEvent();

      if (Focus != null)
      {
        Focus.Set();
        this.Focus = null;
      }

      this.IsShown = true;
    }
    public void Hide()
    {
      this.IsShown = false;

      Space.HidePanel(Overlay);

      if (HideEvent != null)
        HideEvent();
    }
    public void SetFocus(Inv.Focus Focus)
    {
      if (Focus != null && IsShown)
        Focus.Set();
      else
        this.Focus = Focus;
    }
    public void DismissCasual()
    {
      Scrim.Enable();
      ContentFrame.Margin.Set(44);
    }
    public void DismissExplicit()
    {
      Scrim.Disable();
      ContentFrame.Margin.Set(8);
    }

    public static implicit operator Inv.Material.Plane(BaseDialog Self) => Self?.Plane;

    private Inv.Material.Plane Plane;
    private Inv.Material.Space Space;
    private Inv.Overlay Overlay;
    private Inv.Material.Scrim Scrim;
    private Inv.Frame ContentFrame;
    private Inv.Focus Focus;
    private bool IsShown;
  }

  public sealed class ConfirmationDialog
  {
    internal ConfirmationDialog(Inv.Material.Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.DismissExplicit();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;
      Dock.Corner.Set(0, 0, 2, 2);

      this.TitleDock = Inv.Dock.NewHorizontal();
      Dock.AddHeader(TitleDock);

      this.TitleLabel = Inv.Label.New();
      TitleDock.AddClient(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Margin.Set(24, 24, 24, 19);
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Medium();
      TitleLabel.Font.Colour = Theme.TextColour;

      this.TitleDivider = new Inv.Material.Divider();
      Dock.AddHeader(TitleDivider);

      this.ActionDivider = new Inv.Material.Divider();
      Dock.AddFooter(ActionDivider);
      ActionDivider.Visibility.Collapse();

      this.ActionStack = Inv.Stack.NewHorizontal();
      Dock.AddFooter(ActionStack);
      ActionStack.Corner.Set(2);
      ActionStack.Alignment.CenterRight();
      ActionStack.Margin.Set(24, 8, 8, 8);
      ActionStack.Visibility.Collapse();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));

        if (string.IsNullOrWhiteSpace(value))
          TitleDivider.Visibility.Collapse();
      }
    }
    public Inv.Material.Divider TitleDivider { get; private set; }
    public Inv.Panel Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          this.ContentField = value;

          Dock.RemoveClients();
          if (ContentField != null)
            Dock.AddClient(ContentField);
        }
      }
    }
    public Inv.Alignment Alignment => Base.Alignment;
    public event Action ShowEvent
    {
      add { Base.ShowEvent += value; }
      remove { Base.ShowEvent -= value; }
    }
    public event Action HideEvent
    {
      add { Base.HideEvent += value; }
      remove { Base.HideEvent -= value; }
    }

    public void Show()
    {
      Base.Show();
    }
    public void Hide()
    {
      Base.Hide();
    }
    public void SetFocus(Inv.Focus Focus)
    {
      Base.SetFocus(Focus);
    }
    public void AddAction(string Caption, Action Action)
    {
      ActionDivider.Visibility.Show();
      ActionStack.Visibility.Show();

      var Button = new FlatButton();
      ActionStack.AddPanel(Button);
      Button.Background.Colour = Inv.Material.Theme.BlankColour;
      Button.CaptionColour = Theme.PrimaryColour;
      Button.Caption = Caption;
      Button.Margin.Set(8, 0, 0, 0);
      Button.SingleTapEvent += () =>
      {
        if (Action != null)
          Action();
      };

      ActionStack.Visibility.Show();
    }
    public void AddTitleFooter(Inv.Panel Panel)
    {
      TitleDock.AddFooter(Panel);
    }
    public void AddCancelAction(Action Action = null)
    {
      AddAction("CANCEL", () =>
      {
        Action?.Invoke();

        Hide();
      });
    }

    public static implicit operator Inv.Material.Plane(ConfirmationDialog Self) => Self?.Base;

    private Inv.Material.BaseDialog Base;
    private Inv.Dock Dock;
    private Inv.Dock TitleDock;
    private Inv.Label TitleLabel;
    private Inv.Panel ContentField;
    private Inv.Material.Divider ActionDivider;
    private Inv.Stack ActionStack;
  }

  public sealed class SimpleDialog
  {
    internal SimpleDialog(Inv.Material.Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.DismissCasual();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;
      Dock.Padding.Set(0, 0, 0, 8);
      Dock.Corner.Set(0, 0, 2, 2);

      this.TitleLabel = Inv.Label.New();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Margin.Set(24, 24, 24, 19);
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Medium();
      TitleLabel.Font.Colour = Theme.TextColour;
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));

        if (string.IsNullOrWhiteSpace(value))
          TitleDivider.Visibility.Collapse();
      }
    }
    public Inv.Material.Divider TitleDivider { get; private set; }
    public Inv.Panel Content
    {
      get => ContentField;
      set
      {
        if (ContentField != value)
        {
          this.ContentField = value;

          Dock.RemoveClients();
          if (ContentField != null)
            Dock.AddClient(ContentField);
        }
      }
    }
    public event Action ShowEvent
    {
      add { Base.ShowEvent += value; }
      remove { Base.ShowEvent -= value; }
    }
    public event Action HideEvent
    {
      add { Base.HideEvent += value; }
      remove { Base.HideEvent -= value; }
    }

    public void Show()
    {
      Base.Show();
    }
    public void Hide()
    {
      Base.Hide();
    }

    public static implicit operator Inv.Material.Plane(SimpleDialog Self) => Self?.Base;

    private Inv.Material.BaseDialog Base;
    private Inv.Dock Dock;
    private Inv.Label TitleLabel;
    private Inv.Panel ContentField;
  }

  public sealed class AlertDialog
  {
    internal AlertDialog(Space Space)
    {
      this.Base = new BaseDialog(Space);
      Base.DismissExplicit();

      var Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;

      var ContentStack = Inv.Stack.NewVertical();
      Dock.AddClient(ContentStack);
      ContentStack.Corner.Set(2);
      ContentStack.Margin.Set(24);

      this.TitleLabel = Inv.Label.New();
      ContentStack.AddPanel(TitleLabel);
      TitleLabel.Visibility.Collapse();
      TitleLabel.Alignment.CenterLeft();
      TitleLabel.Margin.Set(0, 0, 0, 20);
      TitleLabel.LineWrapping = true;
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Bold();
      TitleLabel.Font.Colour = Theme.TextColour;

      this.ContentLabel = Inv.Label.New();
      ContentStack.AddPanel(ContentLabel);
      ContentLabel.Alignment.CenterLeft();
      ContentLabel.LineWrapping = true;
      ContentLabel.Font.Size = 16;
      ContentLabel.Font.Colour = Inv.Material.Theme.SubtleColour;
      ContentLabel.Font.Name = Theme.FontName;

      this.ActionStack = Inv.Stack.NewHorizontal();
      Dock.AddFooter(ActionStack);
      ActionStack.Corner.Set(2);
      ActionStack.Alignment.CenterRight();
      ActionStack.Margin.Set(24, 8, 8, 8);
      ActionStack.Visibility.Collapse();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(!string.IsNullOrWhiteSpace(value));
      }
    }
    public string Message
    {
      set { ContentLabel.Text = value; }
    }
    public event Action ShowEvent
    {
      add { Base.ShowEvent += value; }
      remove { Base.ShowEvent -= value; }
    }
    public event Action HideEvent
    {
      add { Base.HideEvent += value; }
      remove { Base.HideEvent -= value; }
    }

    public void Show()
    {
      Base.Show();
    }
    public void Hide()
    {
      Base.Hide();
    }
    public void AddAction(string Caption, Action Action)
    {
      var Button = new FlatButton();
      ActionStack.AddPanel(Button);
      Button.Background.Colour = Inv.Material.Theme.BlankColour;
      Button.Caption = Caption;
      Button.Margin.Set(8, 0, 0, 0);
      Button.SingleTapEvent += () =>
      {
        if (Action != null)
          Action();
      };

      ActionStack.Visibility.Show();
    }
    public void AddCancelAction()
    {
      AddAction("CANCEL", () => Hide());
    }

    public static implicit operator Inv.Material.Plane(AlertDialog Self) => Self?.Base;

    private Inv.Material.BaseDialog Base;
    private Inv.Label TitleLabel;
    private Inv.Label ContentLabel;
    private Inv.Stack ActionStack;
  }

  public sealed class Chip : Inv.Panel<Inv.Frame>
  {
    public Chip()
    {
      this.Base = Inv.Frame.New();
      Base.Corner.Set(18);
      Base.Background.Colour = Inv.Material.Theme.EdgeColour;
      Base.Size.SetHeight(32);

      this.Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;
      Dock.Alignment.Stretch();

      this.Icon = new Inv.Material.Icon();
      Dock.AddHeader(Icon);
      Icon.Size.Set(24);
      Icon.Margin.Set(6, 0, 8, 0);
      Icon.Alignment.CenterStretch();
      Icon.Visibility.Collapse();

      this.Label = Inv.Label.New();
      Dock.AddHeader(Label);
      Label.Alignment.CenterStretch();
      Label.Font.Name = Theme.FontName;
      Label.Font.Medium();
      Label.Font.Size = 14;
      Label.Font.Colour = Theme.TextColour;

      this.ActionButton = Inv.Button.NewFlat();
      Dock.AddHeader(ActionButton);
      ActionButton.Alignment.CenterStretch();
      ActionButton.Margin.Set(8, 0, 8, 0);

      this.ActionIcon = new Inv.Material.Icon();
      ActionButton.Content = ActionIcon;
      ActionIcon.Colour = Inv.Colour.DimGray;
      ActionIcon.Size.Set(20);

      Refresh();
    }

    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Image Image
    {
      set
      {
        Icon.Image = value;
        Refresh();
      }
    }
    public Inv.Image ActionImage
    {
      set
      {
        ActionIcon.Image = value;
        Refresh();
      }
    }
    public event Action ActionTapEvent
    {
      add { ActionButton.SingleTapEvent += value; }
      remove { ActionButton.SingleTapEvent -= value; }
    }

    private void Refresh()
    {
      int LabelLeftMargin;
      int LabelRightMargin;

      if (Icon.Image != null)
      {
        Icon.Visibility.Show();
        LabelLeftMargin = 0;
      }
      else
      {
        Icon.Visibility.Collapse();
        LabelLeftMargin = 12;
      }

      if (ActionIcon.Image != null)
      {
        ActionButton.Visibility.Show();
        LabelRightMargin = 0;
      }
      else
      {
        ActionButton.Visibility.Collapse();
        LabelRightMargin = 12;
      }

      Label.Margin.Set(LabelLeftMargin, 0, LabelRightMargin, 0);
    }

    private Inv.Dock Dock;
    private Inv.Material.Icon Icon;
    private Inv.Label Label;
    private Inv.Button ActionButton;
    private Inv.Material.Icon ActionIcon;
  }

  // https://material.io/guidelines/components/cards.html
  public sealed class Card : Inv.Panel<Inv.Frame>
  {
    public Card()
    {
      // Constant width, variable height.

      // Max height is the maximum available height of the platform (i.e. screen height)

      // Cards don't scroll internally (they expand) on mobile, but can scroll on desktop.

      // https://material.io/guidelines/components/cards.html#cards-actions
      // Content
      // - Optional header
      // - Rich media
      // - Supporting text 
      // - Supplemental actions (action area 4)

      this.Base = Inv.Frame.New();
      Base.Background.Colour = Inv.Material.Theme.BlankColour;
      Base.Corner.Set(2);
      Base.Elevation.Set(1);
    }

    public Inv.Size Size => Base.Size;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;
    public Inv.Panel Content
    {
      set { Base.Content = value; }
    }
  }

  public sealed class ListFlow<T> : Inv.Panel<Inv.Flow>
  {
    public ListFlow()
    {
      this.Base = Inv.Flow.New();

      this.CachedSection = Base.AddCachedSection<T>();
      CachedSection.Template(() => new ListItem<T>(), (I, R) =>
      {
        I.Context = R;

        if (ComposeEvent != null)
          ComposeEvent(I);
      });
    }

    public event Action<ListItem<T>> ComposeEvent;

    public void Clear()
    {
      CachedSection.Clear();
    }
    public void Load(Inv.DistinctList<T> ContextList)
    {
      CachedSection.Load(ContextList);
    }
    public void Reload()
    {
      CachedSection.Reload();
    }

    private Inv.CachedSection<T> CachedSection;
  }

  public sealed class ListItem<T> : Inv.Panel<Inv.Frame>
  {
    public ListItem()
    {
      this.Base = Inv.Frame.New();
      this.Tile = new ListTile();
      this.Content = new ListContent();
      this.Subheader = new ListSubheader();
      this.Check = new ListCheck();
    }

    public T Context { get; internal set; }

    public ListTile AsTile()
    {
      return SetContent(Tile);
    }
    public ListContent AsContent()
    {
      return SetContent(Content);
    }
    public ListSubheader AsSubheader()
    {
      return SetContent(Subheader);
    }
    public ListCheck AsCheck()
    {
      return SetContent(Check);
    }

    internal void Clear()
    {
      Tile.Clear();
      Content.Clear();
      Subheader.Clear();
    }
    internal TPanel SetContent<TPanel>(TPanel Panel)
      where TPanel : Inv.Panel
    {
      Clear();

      Base.Content = Panel;

      return Panel;
    }

    private readonly ListTile Tile;
    private readonly ListContent Content;
    private readonly ListSubheader Subheader;
    private readonly ListCheck Check;
  }

  public sealed class ListTile : Inv.Panel<ListContent>
  {
    public ListTile()
    {
      this.Base = new ListContent();
    }

    public string Title
    {
      get { return Base.Title; }
      set { Base.Title = value; }
    }
    public string Description
    {
      get { return Base.Description; }
      set { Base.Description = value; }
    }
    public Inv.Background Background => Base.Background;
    public ListContentLayout Layout => Base.Layout;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void LeadNone()
    {
      Base.LeadNone();
    }
    public void LeadIcon(Inv.Image Image = null, Inv.Colour Colour = null)
    {
      Base.LeadIcon(Image, Colour);
    }
    public void LeadAvatar(Inv.Image Image)
    {
      Base.LeadAvatar(Image);
    }
    public void TrailNone()
    {
      Base.TrailNone();
    }
    public void TrailAction(Inv.Image Image, Action SingleTapAction)
    {
      Base.TrailAction(Image, SingleTapAction);
    }
    public void TrailMetadata(string Metadata, Inv.Colour Colour = null)
    {
      Base.TrailMetadata(Metadata, Colour);
    }

    internal void Clear()
    {
      Base.Clear();
    }
  }

  public sealed class ListContent : Inv.Panel<Inv.Dock>
  {
    public ListContent()
    {
      this.Layout = new ListContentLayout();
      Layout.ChangeEvent += () => Refresh();

      this.Base = Inv.Dock.NewHorizontal();

      this.PrimaryButton = Inv.Button.NewFlat();
      Base.AddClient(PrimaryButton);

      this.PrimaryDock = Inv.Dock.NewHorizontal();
      PrimaryButton.Content = PrimaryDock;

      this.LeadingFrame = Inv.Frame.New();
      PrimaryDock.AddHeader(LeadingFrame);
      LeadingFrame.Size.SetWidth(72);
      LeadingFrame.Alignment.StretchLeft();
      LeadingFrame.Visibility.Collapse();

      this.TextStack = Inv.Stack.NewVertical();
      PrimaryDock.AddClient(TextStack);
      TextStack.Alignment.CenterStretch();
      TextStack.Margin.Set(0, 0, 16, 0);

      this.TitleLabel = Inv.Label.New();
      TextStack.AddPanel(TitleLabel);
      TitleLabel.LineWrapping = false;
      TitleLabel.Font.Name = Theme.FontName;
      TitleLabel.Font.Light();
      TitleLabel.Font.Colour = Theme.TextColour;

      this.DescriptionLabel = Inv.Label.New();
      TextStack.AddPanel(DescriptionLabel);
      DescriptionLabel.LineWrapping = true;
      DescriptionLabel.Margin.Set(0, 4, 0, 0);
      DescriptionLabel.Font.Colour = Inv.Material.Theme.SubtleColour;
      DescriptionLabel.Font.Name = Theme.FontName;
      DescriptionLabel.Font.Light();
      DescriptionLabel.Font.Colour = Theme.TextColour;

      this.TrailingFrame = Inv.Frame.New();
      Base.AddFooter(TrailingFrame);
      TrailingFrame.Margin.Set(0, 0, 0, 0);
      TrailingFrame.Visibility.Collapse();

      Clear();

      Refresh();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set { DescriptionLabel.Text = value; }
    }
    public Inv.Background Background => Base.Background;
    public ListContentLayout Layout { get; private set; }
    public bool IsEnabled { get; set; }

    public void LeadNone()
    {
      this.LeadingContentType = LeadingType.None;

      LeadingFrame.Content = null;
      LeadingFrame.Visibility.Collapse();

      this.LeadingIcon = null;
      this.LeadingAvatarGraphic = null;

      Refresh();
    }
    public void LeadIcon(Inv.Image Image = null, Inv.Colour Colour = null)
    {
      this.LeadingContentType = LeadingType.Icon;

      if (LeadingIcon == null)
      {
        this.LeadingIcon = new Inv.Material.Icon();
        LeadingIcon.Margin.Set(16, 0, 16, 0);
        LeadingIcon.Size.Set(24);
        LeadingIcon.Alignment.Center();
      }

      if (Image != null)
        LeadingIcon.Image = Image;

      LeadingIcon.Colour = Colour ?? Theme.PrimaryColour;

      LeadingFrame.Content = LeadingIcon;
      LeadingFrame.Visibility.Show();

      this.LeadingAvatarGraphic = null;

      Refresh();
    }
    public void LeadAvatar(Inv.Image Image)
    {
      this.LeadingContentType = LeadingType.Avatar;

      if (LeadingAvatarGraphic == null)
      {
        this.LeadingAvatarGraphic = Inv.Graphic.New();
        LeadingAvatarGraphic.Margin.Set(16, 0, 16, 0);
      }

      LeadingAvatarGraphic.Image = Image;

      LeadingFrame.Content = LeadingAvatarGraphic;
      LeadingFrame.Visibility.Show();

      this.LeadingIcon = null;

      Refresh();
    }
    public void TrailNone()
    {
      TrailingFrame.Content = null;
      TrailingFrame.Visibility.Collapse();
    }
    public void TrailAction(Inv.Image Image, Action SingleTapAction)
    {
      var TrailIconButton = new Inv.Material.IconButton();
      TrailIconButton.Background.Colour = Inv.Colour.Transparent;
      TrailIconButton.Alignment.Center();
      TrailIconButton.Padding.Set(12);
      TrailIconButton.SetSize(48);
      TrailIconButton.Image = Image;
      TrailIconButton.SingleTapEvent += () =>
      {
        if (SingleTapAction != null)
          SingleTapAction();
      };

      TrailingFrame.Content = TrailIconButton;
      TrailingFrame.Visibility.Show();
    }
    public void TrailCustom(Inv.Panel Content)
    {
      TrailingFrame.Content = Content;
      TrailingFrame.Visibility.Set(Content != null);
    }
    public void TrailMetadata(string Metadata, Inv.Colour Colour = null)
    {
      var MetadataLabel = Inv.Label.New();
      MetadataLabel.Margin.Set(8, 16, 8, 0);
      MetadataLabel.Alignment.TopRight();
      MetadataLabel.Font.Name = Theme.FontName;
      MetadataLabel.Font.Size = 13;
      MetadataLabel.Font.Colour = Colour ?? Inv.Material.Theme.SubtleColour;
      MetadataLabel.Text = Metadata;

      TrailingFrame.Content = MetadataLabel;
      TrailingFrame.Visibility.Show();
    }

    internal event Action SingleTapEvent
    {
      add
      {
        PrimaryButton.SingleTapEvent += value;
        RefreshEvents();
      }
      remove
      {
        PrimaryButton.SingleTapEvent -= value;
        RefreshEvents();
      }
    }
    internal event Action ContextTapEvent
    {
      add
      {
        PrimaryButton.ContextTapEvent += value;
        RefreshEvents();
      }
      remove
      {
        PrimaryButton.ContextTapEvent -= value;
        RefreshEvents();
      }
    }

    internal void Clear()
    {
      Title = null;
      Description = null;
      IsEnabled = true;

      LeadNone();
      TrailNone();
      PrimaryButton.RemoveSingleTap();
      PrimaryButton.RemoveContextTap();

      Background.Colour = Inv.Material.Theme.BlankColour;

      Layout.Clear();

      Refresh();
      RefreshEvents();
    }

    private void Refresh()
    {
      LeadingFrame.Visibility.Set(LeadingFrame.Content != null);

      TitleLabel.Font.Size = Layout.IsDense ? 13 : 16;

      DescriptionLabel.Visibility.Set(Layout.Format != ListContentLayoutFormat.SingleLine);
      DescriptionLabel.LineWrapping = Layout.Format == ListContentLayoutFormat.ThreeLine;
      DescriptionLabel.Font.Size = Layout.IsDense ? 13 : 14;

      if (LeadingIcon != null)
      {
        LeadingIcon.Size.Set(Layout.IsDense ? (Layout.HasAvatars ? 36 : 20) : (Layout.HasAvatars ? 40 : 24));

        //if (List.Layout == ListLayout.ThreeLine)
        //  LeadingIcon.Alignment.TopCenter();
        //else
        //  LeadingIcon.Alignment.Center();

        PrimaryDock.Padding.Set(0);
      }
      else if (LeadingAvatarGraphic != null)
      {
        LeadingAvatarGraphic.Size.Set(Layout.IsDense ? 36 : 40);

        if (Layout.Format == ListContentLayoutFormat.ThreeLine)
          LeadingAvatarGraphic.Alignment.TopCenter();
        else
          LeadingAvatarGraphic.Alignment.Center();

        PrimaryDock.Padding.Set(0);
      }
      else
      {
        PrimaryDock.Padding.Set(16, 0, 0, 0);
      }

      var DividerHeight = Layout.ShowDivider ? 1 : 0;
      Base.Border.Set(0, 0, 0, DividerHeight);
      Base.Border.Colour = Inv.Colour.Black.AdjustAlpha((int)(0.12 * 255));

      int ContentHeight;

      if (Layout.ContentHeight != null)
      {
        ContentHeight = Layout.ContentHeight.Value;
      }
      else if (Layout.Format == ListContentLayoutFormat.SingleLine)
      {
        if (LeadingContentType == LeadingType.Avatar)
          ContentHeight = Layout.IsDense ? 48 : 56;
        else
          ContentHeight = Layout.IsDense ? 40 : 48;
      }
      else if (Layout.Format == ListContentLayoutFormat.TwoLine)
      {
        ContentHeight = Layout.IsDense ? 60 : 72;
      }
      else
      {
        ContentHeight = Layout.IsDense ? 76 : 88;
      }

      Base.Size.SetHeight(ContentHeight + DividerHeight);
    }
    private void RefreshEvents()
    {
      PrimaryButton.Background.Colour = PrimaryButton.HasSingleTap() || PrimaryButton.HasContextTap() ? Inv.Colour.Transparent : null;
    }

    private Inv.Button PrimaryButton;
    private Inv.Dock PrimaryDock;
    private Inv.Stack TextStack;
    private Inv.Label TitleLabel;
    private Inv.Label DescriptionLabel;
    private Inv.Frame LeadingFrame;
    private Inv.Material.Icon LeadingIcon;
    private Inv.Graphic LeadingAvatarGraphic;
    private LeadingType LeadingContentType;
    private Inv.Frame TrailingFrame;

    private enum LeadingType
    {
      None, Icon, Avatar
    }
  }

  public sealed class ListContentLayout
  {
    internal ListContentLayout()
    {
    }

    public bool IsDense
    {
      get { return IsDenseField; }
      set
      {
        if (value != IsDenseField)
        {
          IsDenseField = value;
          ChangeInvoke();
        }
      }
    }
    public bool HasAvatars
    {
      get { return HasAvatarsField; }
      set
      {
        if (value != HasAvatarsField)
        {
          HasAvatarsField = value;
          ChangeInvoke();
        }
      }
    }
    public int? ContentHeight
    {
      get { return ContentHeightField; }
      set
      {
        if (value != ContentHeightField)
        {
          ContentHeightField = value;
          ChangeInvoke();
        }
      }
    }
    public bool ShowDivider
    {
      get { return ShowDividerField; }
      set
      {
        if (value != ShowDividerField)
        {
          ShowDividerField = value;
          ChangeInvoke();
        }
      }
    }

    public void FormatOneLine()
    {
      this.Format = ListContentLayoutFormat.SingleLine;

      ChangeInvoke();
    }
    public void FormatTwoLine()
    {
      this.Format = ListContentLayoutFormat.TwoLine;

      ChangeInvoke();
    }
    public void FormatThreeLine()
    {
      this.Format = ListContentLayoutFormat.ThreeLine;

      ChangeInvoke();
    }

    internal ListContentLayoutFormat Format { get; private set; }
    internal event Action ChangeEvent;

    internal void Clear()
    {
      this.Format = ListContentLayoutFormat.SingleLine;
      this.IsDenseField = false;
      this.HasAvatarsField = false;
      this.ContentHeightField = null;
      this.ShowDividerField = false;

      ChangeInvoke();
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private bool IsDenseField;
    private bool HasAvatarsField;
    private int? ContentHeightField;
    private bool ShowDividerField;
  }

  internal enum ListContentLayoutFormat
  {
    SingleLine, TwoLine, ThreeLine
  }

  public sealed class ListSubheader : Inv.Panel<Inv.Label>
  {
    public ListSubheader()
    {
      this.Base = Inv.Label.New();
      Base.Padding.Set(16);
      Base.Size.SetHeight(48);
      Base.Font.Name = Theme.FontName;
      Base.Font.Size = 13;
      Base.Font.Colour = Theme.PrimaryColour;
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    internal void Clear()
    {
      Text = null;
    }
  }

  public sealed class ListStack : Inv.Panel<Inv.Stack>
  {
    public ListStack()
    {
      // 8 top and bottom padding, except when subheaders are present

      this.Base = Inv.Stack.NewVertical();
    }

    public Inv.Background Background => Base.Background;

    public ListContent AddContent()
    {
      return AddPanel<ListContent>();
    }
    public ListTile AddTile()
    {
      return AddPanel<ListTile>();
    }
    public ListSubheader AddSubheader()
    {
      return AddPanel<ListSubheader>();
    }
    public Divider AddDivider()
    {
      return AddPanel<Divider>();
    }
    public ListCheck AddCheck()
    {
      return AddPanel<ListCheck>();
    }
    public void AddPanel(Inv.Panel Panel)
    {
      Base.AddPanel(Panel);
    }

    private T AddPanel<T>()
      where T : Inv.Panel, new()
    {
      var Result = new T();

      AddPanel(Result);

      return Result;
    }
  }

  public enum ListLayout
  {
    SingleLine, TwoLine, ThreeLine
  }

  public sealed class ListCheck : Inv.Panel<Inv.Button>
  {
    public ListCheck()
    {
      this.Base = Inv.Button.NewFlat();
      Base.Size.SetHeight(48);
      Base.Background.Colour = Inv.Colour.Transparent;
      Base.SingleTapEvent += () =>
      {
        IsChecked = !IsChecked;

        if (ChangeEvent != null)
          ChangeEvent();
      };

      var Dock = Inv.Dock.NewHorizontal();
      Base.Content = Dock;

      this.Icon = new Inv.Material.Icon();
      Dock.AddHeader(Icon);
      Icon.Size.Set(24);
      Icon.Margin.Set(24, 0, 0, 0);
      Icon.Alignment.Center();

      this.Label = Inv.Label.New();
      Dock.AddClient(Label);
      Label.Alignment.CenterLeft();
      Label.Margin.Set(32, 0, 0, 0);
      Label.Font.Name = Theme.FontName;
      Label.Font.Size = 14;
      Label.Font.Colour = Theme.TextColour;

      Refresh();
    }

    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        if (value != IsCheckedField)
        {
          IsCheckedField = value;

          Refresh();
        }
      }
    }
    public string Title
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public event Action ChangeEvent;

    private void Refresh()
    {
      Icon.Image = IsCheckedField ? Resources.Toggle.CheckBox : Resources.Toggle.CheckBoxOutlineBlank;
    }

    private Inv.Material.Icon Icon;
    private Inv.Label Label;
    private bool IsCheckedField;
  }

  public sealed class PropertyValidator
  {
    public PropertyValidator()
    {
      this.IsEnabled = true;
      this.RuleList = new Inv.DistinctList<PropertyRule>();
      this.ValidatorList = new Inv.DistinctList<PropertyValidator>();
    }

    public bool IsEnabled { get; set; }

    public PropertyRuleReport Run()
    {
      var Result = (PropertyRuleReport)null;

      foreach (var Rule in RuleList)
      {
        Rule.Check();

        if (Result == null && Rule.Report.IsFailure)
          Result = Rule.Report;
      }

      foreach (var Validator in ValidatorList.Where(V => V.IsEnabled))
      {
        var ValidatorReport = Validator.Run();

        if (Result == null && ValidatorReport != null && ValidatorReport.IsFailure)
          Result = ValidatorReport;
      }

      return Result;
    }
    public PropertyRule AddRule(PropertyField TargetField, params PropertyField[] FieldArray)
    {
      var Result = new PropertyRule(TargetField, FieldArray);

      RuleList.Add(Result);

      return Result;
    }
    public PropertyValidator AddValidator()
    {
      var Result = new PropertyValidator();

      ValidatorList.Add(Result);

      return Result;
    }
    public void RemoveValidator(PropertyValidator Validator)
    {
      ValidatorList.Remove(Validator);
    }

    private Inv.DistinctList<PropertyRule> RuleList;
    private Inv.DistinctList<PropertyValidator> ValidatorList;
  }

  public sealed class PropertyRule
  {
    internal PropertyRule(PropertyField TargetField, params PropertyField[] FieldArray)
    {
      this.IsEnabledField = true;

      this.Report = new PropertyRuleReport();
      Report.DefaultField = TargetField;
      TargetField.AddInherentReport(Report);

      Observe(TargetField);
      foreach (var Field in FieldArray)
        Observe(Field);
    }

    public bool IsEnabled
    {
      get { return IsEnabledField; }
      set
      {
        if (IsEnabledField != value)
        {
          IsEnabledField = value;

          Check();
        }
      }
    }
    public PropertyRuleReport Report { get; private set; }
    // Only a Func for syntax convenience.
    public event Func<PropertyRuleReport, bool> CheckEvent;

    internal void Check()
    {
      if (!IsEnabled)
        return;

      if (CheckEvent != null)
        CheckEvent(Report);
    }
    internal void Observe(PropertyField Field)
    {
      Field.ChangeEvent += () => Check();
    }

    private bool IsEnabledField;
  }

  public sealed class PropertyRuleReport
  {
    public PropertyRuleReport()
    {
    }

    public bool IsFailure { get; private set; }
    public string Message { get; private set; }
    public Inv.Material.PropertyField ActivateField { get; private set; }
    public Inv.Focus Focus { get; private set; }

    public bool Pass()
    {
      this.IsFailure = false;
      this.Message = null;
      this.ActivateField = null;
      this.Focus = null;

      // Clear the last message.
      if (DefaultField != null)
      {
        DefaultField.RemoveTransientReports();
        DefaultField.ReportRefresh();
      }

      return true;
    }
    public bool Fail(string Message, Inv.Material.PropertyField PropertyField = null, Inv.Focus SetFocus = null)
    {
      this.IsFailure = true;
      this.Message = Message;
      this.ActivateField = PropertyField ?? DefaultField;
      this.Focus = SetFocus ?? PropertyField?.Focus ?? DefaultField.Focus;

      Debug.Assert(Focus != null, "Could not determine appropriate Focus");

      if (DefaultField == null && PropertyField != null)
      {
        PropertyField.RemoveTransientReports();
        PropertyField.AddTransientReport(this);
      }

      var RefreshField = DefaultField ?? PropertyField;
      if (RefreshField != null)
        RefreshField.ReportRefresh();

      return false;
    }
    public bool Fail(string Message, Inv.Focus SetFocus)
    {
      return Fail(Message, null, SetFocus);
    }
    public void Highlight()
    {
      if (ActivateField != null)
        ActivateField.Activate();

      if (Focus != null)
        Focus.Set();
    }

    internal PropertyField DefaultField { get; set; }
  }
    
  public sealed class PropertySheet : Inv.Panel<Inv.Scroll>
  {
    public PropertySheet(Inv.Material.Space Space)
    {
      this.Space = Space;
      this.Validator = new PropertyValidator();

      this.Base = Inv.Scroll.NewVertical();

      this.Dock = Inv.Dock.NewVertical();
      Base.Content = Dock;

      this.Stack = Inv.Stack.NewVertical();
      Dock.AddClient(Stack);
      Stack.Background.Colour = Inv.Material.Theme.BlankColour;
      Stack.Padding.Set(0, 12, 0, 0);
    }

    public Inv.Margin Margin => Dock.Margin;
    public PropertyValidator Validator { get; private set; }

    public void AddHeader(Inv.Panel Panel)
    {
      Dock.AddHeader(Panel);
    }
    public PropertyBand AddBand(Inv.Image Image)
    {
      var Result = new PropertyBand();
      Result.SetIcon(Image);

      Stack.AddPanel(Result);

      return Result;
    }
    public PropertyGroup AddGroup(Inv.Image Image)
    {
      var Result = new PropertyGroup(Space, Validator);
      Result.SetIcon(Image);

      Stack.AddPanel(Result);

      return Result;
    }
    public PropertySet<T> AddSet<T>(Inv.Image Image, string Caption)
      where T : new()
    {
      return new PropertySet<T>(this, Space, AddBand(Image), Caption);
    }
    public PropertySwitchGroup AddSwitchGroup(string Caption)
    {
      var Result = new PropertySwitchGroup(Caption);

      Stack.AddPanel(Result);

      return Result;
    }
    public void AddDivider()
    {
      Stack.AddPanel(new Inv.Material.Divider());
    }

    private readonly Inv.Material.Space Space;
    private readonly Inv.Dock Dock;
    private readonly Inv.Stack Stack;
  }

  public sealed class PropertyBand : Inv.Panel<Inv.Dock>
  {
    public PropertyBand()
    {
      this.Base = Inv.Dock.NewHorizontal();
      Base.Margin.Set(0, 0, 0, 4);

      this.Icon = new Inv.Material.Icon();
      Base.AddHeader(Icon);
      Icon.Margin.Set(16, 12, 16, 12);
      Icon.Alignment.TopLeft();
      Icon.Size.Set(24);

      this.ContentFrame = Inv.Frame.New();
      Base.AddClient(ContentFrame);
      ContentFrame.Margin.Set(16, 0, 0, 0);
    }

    public Inv.Panel Content
    {
      get { return ContentFrame.Content; }
      set { ContentFrame.Content = value; }
    }

    public void SetIcon(Inv.Image Image)
    {
      Icon.Image = Image;
    }

    private Inv.Material.Icon Icon;
    private Inv.Frame ContentFrame;
  }

  public sealed class PropertyGroup : Inv.Panel<PropertyBand>
  {
    public PropertyGroup(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;

      this.Base = new PropertyBand();

      this.Stack = new Inv.Material.PropertyStack(Space, Validator);
      Base.Content = Stack;
      Stack.Margin.Set(0, 0, 72, 0);
    }

    public PropertyStack Stack { get; private set; }

    public void SetIcon(Inv.Image Image)
    {
      Base.SetIcon(Image);
    }

    private readonly Inv.Material.Space Space;
  }

  public sealed class PropertyStack : Inv.Panel<Inv.Stack>
  {
    public PropertyStack(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;
      this.Validator = Validator;

      this.Span = new PropertySpan(Space, Validator);
      Span.AddPanelEvent += (Panel) =>
      {
        Panel.Control.Margin.Set(0, 0, 0, 8);
        Base.AddPanel(Panel);
      };
      Span.ChangeEvent += () => ChangeInvoke();

      this.Base = Inv.Stack.NewVertical();
    }

    public PropertyValidator Validator { get; private set; }
    public PropertySpan Span { get; private set; }
    public event Action ChangeEvent;

    internal Inv.Margin Margin => Base.Margin;

    public Inv.Material.MemoField AddMemoField(string Caption)
    {
      var Result = new Inv.Material.MemoField(Span.AddField());
      Result.Margin.Set(0, 0, 0, 8);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.PropertyRow AddRow()
    {
      var Result = new Inv.Material.PropertyRow(Space, Validator);
      Base.AddPanel(Result);
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }

    internal void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private readonly Inv.Material.Space Space;
  }

  public sealed class PropertyRow : Inv.Panel<Inv.Dock>
  {
    internal PropertyRow(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Span = new PropertySpan(Space, Validator);
      Span.AddPanelEvent += (Panel) =>
      {
        if (Base.PanelCount > 0)
          Panel.Control.Margin.Set(8, 0, 0, 0);

        Base.AddClient(Panel);
      };

      this.Base = Inv.Dock.NewHorizontal();
      Base.Margin.Set(0, 0, 0, 8);
    }

    public PropertySpan Span { get; private set; }

    internal event Action ChangeEvent
    {
      add { Span.ChangeEvent += value; }
      remove { Span.ChangeEvent -= value; }
    }
  }

  public sealed class PropertySpan
  {
    internal PropertySpan(Inv.Material.Space Space, PropertyValidator Validator)
    {
      this.Space = Space;
      this.Validator = Validator;
    }

    public Inv.Material.Integer32Field AddInteger32Field(string Caption)
    {
      var Result = new Inv.Material.Integer32Field(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DecimalField AddDecimalField(string Caption)
    {
      var Result = new Inv.Material.DecimalField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.MoneyField AddMoneyField(string Caption)
    {
      var Result = new Inv.Material.MoneyField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.EditField AddEditField(string Caption, Inv.EditInput EditInput)
    {
      var Result = new Inv.Material.EditField(AddField(), EditInput);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.EditField AddEmailField(string Caption)
    {
      return AddEditField(Caption, EditInput.Email);
    }
    public Inv.Material.EditField AddNameField(string Caption)
    {
      return AddEditField(Caption, EditInput.Name);
    }
    public Inv.Material.EditField AddPasswordField(string Caption)
    {
      return AddEditField(Caption, EditInput.Password);
    }
    public Inv.Material.EditField AddPhoneField(string Caption)
    {
      return AddEditField(Caption, EditInput.Phone);
    }
    public Inv.Material.EditField AddTextField(string Caption)
    {
      return AddEditField(Caption, EditInput.Text);
    }
    public Inv.Material.EditField AddUsernameField(string Caption)
    {
      return AddEditField(Caption, EditInput.Username);
    }
    public Inv.Material.SelectField<T> AddSelectField<T>(string Caption)
    {
      var Result = new Inv.Material.SelectField<T>(AddField(), Space);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.CollectField<T> AddCollectField<T>(string Caption)
    {
      var Result = new Inv.Material.CollectField<T>(AddField(), Space);
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DateOfBirthField AddDateOfBirthField(string Caption)
    {
      var Result = new Inv.Material.DateOfBirthField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.DateField AddDateField(string Caption)
    {
      var Result = new Inv.Material.DateField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }
    public Inv.Material.TimeField AddTimeField(string Caption)
    {
      var Result = new Inv.Material.TimeField(AddField());
      Result.CaptionText = Caption;
      Result.ChangeEvent += () => ChangeInvoke();

      return Result;
    }

    internal event Action ChangeEvent;
    internal event Action<Inv.Panel> AddPanelEvent;

    internal PropertyField AddField()
    {
      var Result = new PropertyField(Validator);

      if (AddPanelEvent != null)
        AddPanelEvent(Result);

      return Result;
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private readonly Inv.Material.Space Space;
    private readonly PropertyValidator Validator;
  }

  internal sealed class PropertySet
  {
    internal PropertySet(PropertySheet Sheet, Inv.Material.Space Space, Inv.Material.PropertyBand Band, string Caption)
    {
      this.Sheet = Sheet;
      this.Space = Space;
      this.Band = Band;

      var Dock = Inv.Dock.NewVertical();
      Band.Content = Dock;

      this.Stack = Inv.Stack.NewVertical();
      Dock.AddClient(Stack);

      this.NewButton = Inv.Button.NewFlat();
      Dock.AddFooter(NewButton);
      NewButton.Background.Colour = Inv.Material.Theme.BlankColour;
      NewButton.Corner.Set(4);
      NewButton.Padding.Set(12);
      NewButton.Margin.Set(0, 8, 0, 8);
      NewButton.Alignment.StretchLeft();
      NewButton.SingleTapEvent += () =>
      {
        AddItem();

        RefreshItems();
      };

      var NewLabel = Inv.Label.New();
      NewButton.Content = NewLabel;
      NewLabel.Text = "ADD " + Caption.ToUpper();
      NewLabel.Font.Name = Theme.FontName;
      NewLabel.Font.Medium();
      NewLabel.Font.Size = 12;
      NewLabel.Font.Colour = Inv.Material.Theme.SubtleColour;

      this.AllowRemoveLastField = true;
      this.ItemList = new Inv.DistinctList<PropertyItem>();
    }

    public bool AllowRemoveLast
    {
      get => AllowRemoveLastField;
      set
      {
        this.AllowRemoveLastField = value;

        RefreshItems();
      }
    }
    public event Func<object> NewQuery;
    public event Action<PropertyItem> BuildItemEvent;

    public void SetIcon(Inv.Image Image = null)
    {
      Band.SetIcon(Image);
    }
    public Inv.DistinctList<object> Get()
    {
      foreach (var Item in ItemList)
        Item.SaveInvoke();

      return ItemList.Select(I => I.Context).ToDistinctList();
    }
    public void Set(Inv.DistinctList<object> ContextList)
    {
      // TODO: Recycle items rather than completely flushing.
      RemoveItems(ItemList.ToArray());

      foreach (var Context in ContextList)
      {
        var EditItem = AddItem();
        EditItem.Context = Context;
        EditItem.LoadInvoke();
      }

      RefreshItems();
    }
    public PropertyItem AddItem()
    {
      var Result = new PropertyItem(Sheet, Space);
      Result.Context = NewQuery();
      Result.ClearEvent += () => RemoveItems(Result);

      if (BuildItemEvent != null)
        BuildItemEvent(Result);

      ItemList.Add(Result);

      if (AddItemEvent != null)
        AddItemEvent(Result);

      Stack.AddPanel(Result);

      if (Result.Primary != null)
        Result.Primary.Activate();

      return Result;
    }
    public void RemoveItems(params PropertyItem[] ItemArray)
    {
      foreach (var Item in ItemArray)
      {
        ItemList.Remove(Item);

        Sheet.Validator.RemoveValidator(Item.Stack.Validator);

        if (RemoveItemEvent != null)
          RemoveItemEvent(Item);

        Stack.RemovePanel(Item);
      }

      RefreshItems();
    }

    internal event Action<PropertyItem> AddItemEvent;
    internal event Action<PropertyItem> RemoveItemEvent;

    private void RefreshItems()
    {
      if (!ItemList.Any())
        return;

      foreach (var Item in ItemList.Take(ItemList.Count - 1))
      {
        Item.IsSeparator = true;
        Item.ClearButtonVisibility.Show();
      }

      var LastItem = ItemList.LastOrDefault();
      LastItem.IsSeparator = false;
      LastItem.ClearButtonVisibility.Show();

      if (ItemList.Count == 1 && !AllowRemoveLastField)
        ItemList.FirstOrDefault().ClearButtonVisibility.Collapse();
    }

    private readonly PropertySheet Sheet;
    private readonly Inv.Material.Space Space;
    private readonly Inv.Material.PropertyBand Band;
    private readonly Inv.Stack Stack;
    private readonly Inv.DistinctList<PropertyItem> ItemList;
    private readonly Inv.Button NewButton;
    private bool AllowRemoveLastField;
  }

  public sealed class PropertyItem : Inv.Panel<Inv.Dock>
  {
    internal PropertyItem(PropertySheet Sheet, Inv.Material.Space Space)
    {
      this.Base = Inv.Dock.NewHorizontal();

      var StackDock = Inv.Dock.NewVertical();
      Base.AddClient(StackDock);

      this.Stack = new Inv.Material.PropertyStack(Space, Sheet.Validator.AddValidator());
      StackDock.AddClient(Stack);

      this.Divider = new Inv.Material.Divider();
      StackDock.AddFooter(Divider);
      Divider.Margin.Set(0, 0, 0, 8);
      Divider.Visibility.Collapse();

      this.Frame = Inv.Frame.New();
      Base.AddFooter(Frame);
      Frame.Margin.Set(24, 0, 24, 32);
      Frame.Size.Set(24);

      this.ClearButton = new Inv.Material.IconButton();
      Frame.Content = ClearButton;
      ClearButton.Alignment.Center();
      ClearButton.SetSize(32);
      ClearButton.Image = Inv.Material.Resources.Content.Clear;
    }

    public bool IsNew { get; private set; }
    public PropertyField Primary { get; set; }
    public PropertyStack Stack { get; private set; }
    public event Action<object> LoadEvent;
    public event Action<object> SaveEvent;

    internal object Context { get; set; }
    internal bool IsSeparator
    {
      set => Divider.Visibility.Set(value);
    }
    internal Inv.Material.PropertyValidator Validator { get; private set; }
    internal Inv.Visibility ClearButtonVisibility => ClearButton.Visibility;
    internal event Action ClearEvent
    {
      add { ClearButton.SingleTapEvent += value; }
      remove { ClearButton.SingleTapEvent -= value; }
    }

    internal void NewInvoke()
    {
      this.IsNew = true;

      if (LoadEvent != null)
        LoadEvent(Context);
    }
    internal void LoadInvoke()
    {
      this.IsNew = false;

      if (LoadEvent != null)
        LoadEvent(Context);
    }
    internal void SaveInvoke()
    {
      if (SaveEvent != null)
        SaveEvent(Context);
    }

    private Inv.Frame Frame;
    private Inv.Material.IconButton ClearButton;
    private Inv.Material.Divider Divider;
  }

  public sealed class PropertySet<T>
    where T : new()
  {
    internal PropertySet(PropertySet Base)
    {
      this.Base = Base;
      Base.NewQuery += () => new T();
      Base.BuildItemEvent += (Item) =>
      {
        if (BuildItemEvent != null)
          BuildItemEvent(new PropertyItem<T>(Item));
      };
    }
    internal PropertySet(PropertySheet Sheet, Inv.Material.Space Space, Inv.Material.PropertyBand Band, string Caption)
      : this(new PropertySet(Sheet, Space, Band, Caption))
    {
    }

    public event Action<PropertyItem<T>> BuildItemEvent;

    public void SetIcon(Inv.Image Image = null)
    {
      Base.SetIcon(Image);
    }
    public Inv.DistinctList<T> Get()
    {
      return Base.Get().Convert<T>().ToDistinctList();
    }
    public void Set(Inv.DistinctList<T> ContextList)
    {
      Base.Set(ContextList.Convert<object>().ToDistinctList());
    }
    public PropertyItem<T> AddItem()
    {
      return new PropertyItem<T>(Base.AddItem());
    }

    private PropertySet Base;
  }

  public sealed class PropertyItem<T> : Inv.Panel<PropertyItem>
    where T : new()
  {
    internal PropertyItem(PropertyItem Base)
    {
      this.Base = Base;
      Base.LoadEvent += O =>
      {
        if (LoadEvent != null)
          LoadEvent((T)O);
      };
      Base.SaveEvent += O =>
      {
        if (SaveEvent != null)
          SaveEvent((T)O);
      };
    }

    public bool IsNew => Base.IsNew;
    public PropertyField Primary
    {
      get => Base.Primary;
      set => Base.Primary = value;
    }
    public PropertyStack Stack => Base.Stack;
    public event Action<T> LoadEvent;
    public event Action<T> SaveEvent;
  }

  public sealed class PropertySwitchGroup : Inv.Panel<Inv.Stack>
  {
    internal PropertySwitchGroup(string Caption)
    {
      this.Base = Inv.Stack.NewVertical();

      var Subheader = new Inv.Material.ListSubheader();
      Base.AddPanel(Subheader);
      Subheader.Text = Caption;
    }

    public Inv.Material.Switch Add(string Caption)
    {
      var Dock = Inv.Dock.NewHorizontal();
      Base.AddPanel(Dock);
      Dock.Padding.Set(16, 12, 16, 16);

      var Label = Inv.Label.New();
      Dock.AddClient(Label);
      Label.Text = Caption;
      Label.Font.Size = 16;
      Label.Font.Name = Theme.FontName;
      Label.Font.Colour = Theme.TextColour;

      var Switch = new Inv.Material.Switch();
      Dock.AddFooter(Switch);
      Switch.Alignment.CenterRight();

      return Switch;
    }
  }

  public sealed class Divider : Inv.Panel<Inv.Frame>
  {
    public Divider()
    {
      this.Base = Inv.Frame.New();
      Base.Size.SetHeight(1);
      Base.Background.Colour = Inv.Colour.Black.AdjustAlpha((int)(0.12 * 255));
    }

    public Inv.Margin Margin => Base.Margin;
    public Inv.Visibility Visibility => Base.Visibility;

    public void Inset()
    {
      Base.Margin.Set(56, 0, 0, 0);
    }
  }
}