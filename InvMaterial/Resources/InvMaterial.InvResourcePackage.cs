namespace Inv.Material
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvMaterial.InvResourcePackage.rs");
    }

    public static readonly ResourcesAction Action;
    public static readonly ResourcesAlert Alert;
    public static readonly ResourcesAv Av;
    public static readonly ResourcesCommunication Communication;
    public static readonly ResourcesContent Content;
    public static readonly ResourcesDevice Device;
    public static readonly ResourcesEditor Editor;
    public static readonly ResourcesFile File;
    public static readonly ResourcesHardware Hardware;
    public static readonly ResourcesImage Image;
    public static readonly ResourcesMaps Maps;
    public static readonly ResourcesNavigation Navigation;
    public static readonly ResourcesNotification Notification;
    public static readonly ResourcesPlaces Places;
    public static readonly ResourcesSocial Social;
    public static readonly ResourcesToggle Toggle;
  }

  public sealed class ResourcesAction
  {
    public ResourcesAction() { }

    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference _3dRotation;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Accessibility;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Accessible;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccountBalance;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccountBalanceWallet;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccountBox;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccountCircle;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddShoppingCart;
    ///<Summary>(.png) 192 x 192 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Alarm;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AlarmAdd;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AlarmOff;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AlarmOn;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AllOut;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Android;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Announcement;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AspectRatio;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Assessment;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Assignment;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssignmentInd;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssignmentLate;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssignmentReturn;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssignmentReturned;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssignmentTurnedIn;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Autorenew;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Backup;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Book;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Bookmark;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BookmarkBorder;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BugReport;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Build;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Cached;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CameraEnhance;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CardGiftcard;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CardMembership;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CardTravel;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChangeHistory;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CheckCircle;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChromeReaderMode;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Class;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Code;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CompareArrows;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Copyright;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CreditCard;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dashboard;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DateRange;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Delete;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DeleteForever;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Description;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dns;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Done;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DoneAll;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DonutLarge;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DonutSmall;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Eject;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EuroSymbol;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Event;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EventSeat;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExitToApp;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Explore;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Extension;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Face;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Favorite;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FavoriteBorder;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Feedback;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FindInPage;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FindReplace;
    ///<Summary>(.png) 192 x 192 (5.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Fingerprint;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlightLand;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlightTakeoff;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlipToBack;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlipToFront;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gavel;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GetApp;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gif;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Grade;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GroupWork;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GTranslate;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Help;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HelpOutline;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HighlightOff;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference History;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Home;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HourglassEmpty;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HourglassFull;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Http;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Https;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ImportantDevices;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Info;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InfoOutline;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Input;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InvertColors;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Label;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LabelOutline;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Language;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Launch;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LightbulbOutline;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LineStyle;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LineWeight;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference List;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Lock;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LockOpen;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LockOutline;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Loyalty;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MarkunreadMailbox;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Motorcycle;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NoteAdd;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference OfflinePin;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Opacity;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference OpenInBrowser;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference OpenInNew;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference OpenWith;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pageview;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PanTool;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Payment;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermCameraMic;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermContactCalendar;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermDataSetting;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermDeviceInformation;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermIdentity;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermMedia;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermPhoneMsg;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PermScanWifi;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pets;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PictureInPicture;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PictureInPictureAlt;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlayForWork;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Polymer;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PowerSettingsNew;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PregnantWoman;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Print;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference QueryBuilder;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference QuestionAnswer;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Receipt;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RecordVoiceOver;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Redeem;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveShoppingCart;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Reorder;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ReportProblem;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Restore;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RestorePage;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Room;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RoundedCorner;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Rowing;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Schedule;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Search;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Settings;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsApplications;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsBackupRestore;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsBluetooth;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsBrightness;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsCell;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsEthernet;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsInputAntenna;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsInputComponent;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsInputComposite;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsInputHdmi;
    ///<Summary>(.png) 192 x 192 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsInputSvideo;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsOverscan;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsPhone;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsPower;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsRemote;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsVoice;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Shop;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ShoppingBasket;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ShoppingCart;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ShopTwo;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SpeakerNotes;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SpeakerNotesOff;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Spellcheck;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Stars;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Store;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Subject;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SupervisorAccount;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwapHoriz;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwapVert;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwapVerticalCircle;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SystemUpdateAlt;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tab;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TabUnselected;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Theaters;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ThumbDown;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ThumbsUpDown;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ThumbUp;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Timeline;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Toc;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Today;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Toll;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TouchApp;
    ///<Summary>(.png) 192 x 192 (3.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TrackChanges;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Translate;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TrendingDown;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TrendingFlat;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TrendingUp;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TurnedIn;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TurnedInNot;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Update;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VerifiedUser;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewAgenda;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewArray;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewCarousel;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewColumn;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewDay;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewHeadline;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewList;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewModule;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewQuilt;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewStream;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewWeek;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Visibility;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VisibilityOff;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WatchLater;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Work;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference YoutubeSearchedFor;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ZoomIn;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ZoomOut;
  }

  public sealed class ResourcesAlert
  {
    public ResourcesAlert() { }

    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddAlert;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Error;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ErrorOutline;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Warning;
  }

  public sealed class ResourcesAv
  {
    public ResourcesAv() { }

    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddToQueue;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Airplay;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Album;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArtTrack;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AvTimer;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrandingWatermark;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallToAction;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ClosedCaption;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Equalizer;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Explicit;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FastForward;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FastRewind;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FeaturedPlayList;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FeaturedVideo;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FiberDvr;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FiberManualRecord;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FiberNew;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FiberPin;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FiberSmartRecord;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Forward10;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Forward30;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Forward5;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Games;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Hd;
    ///<Summary>(.png) 192 x 192 (2.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Hearing;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HighQuality;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LibraryAdd;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LibraryBooks;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LibraryMusic;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Loop;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Mic;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MicNone;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MicOff;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Movie;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MusicVideo;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NewReleases;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Note;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotInterested;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pause;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PauseCircleFilled;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PauseCircleOutline;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlayArrow;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlayCircleFilled;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlayCircleFilledWhite;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlayCircleOutline;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlaylistAdd;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlaylistAddCheck;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlaylistPlay;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Queue;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference QueueMusic;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference QueuePlayNext;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Radio;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RecentActors;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveFromQueue;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Repeat;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RepeatOne;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Replay;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Replay10;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Replay30;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Replay5;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Shuffle;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SkipNext;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SkipPrevious;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SlowMotionVideo;
    ///<Summary>(.png) 192 x 192 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Snooze;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SortByAlpha;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Stop;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Subscriptions;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Subtitles;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SurroundSound;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VideoCall;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Videocam;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VideocamOff;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VideoLabel;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VideoLibrary;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VolumeDown;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VolumeMute;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VolumeOff;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VolumeUp;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Web;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WebAsset;
  }

  public sealed class ResourcesCommunication
  {
    public ResourcesCommunication() { }

    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Business;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Call;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallEnd;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallMade;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallMerge;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallMissed;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallMissedOutgoing;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallReceived;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CallSplit;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Chat;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChatBubble;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChatBubbleOutline;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ClearAll;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Comment;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ContactMail;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ContactPhone;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Contacts;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DialerSip;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dialpad;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Email;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Forum;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ImportContacts;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ImportExport;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InvertColorsOff;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LiveHelp;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocationOff;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocationOn;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MailOutline;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Message;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NoSim;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Phone;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonelinkErase;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonelinkLock;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonelinkRing;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonelinkSetup;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PortableWifiOff;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PresentToAll;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RingVolume;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RssFeed;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ScreenShare;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SpeakerPhone;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StayCurrentLandscape;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StayCurrentPortrait;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StayPrimaryLandscape;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StayPrimaryPortrait;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StopScreenShare;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwapCalls;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Textsms;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Voicemail;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VpnKey;
  }

  public sealed class ResourcesContent
  {
    public ResourcesContent() { }

    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Add;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddBox;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddCircle;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddCircleOutline;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Archive;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Backspace;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Block;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Clear;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ContentCopy;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ContentCut;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ContentPaste;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Create;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DeleteSweep;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Drafts;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterList;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Flag;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FontDownload;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Forward;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gesture;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Inbox;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Link;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LowPriority;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Mail;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Markunread;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MoveToInbox;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NextWeek;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Redo;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Remove;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveCircle;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveCircleOutline;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Reply;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ReplyAll;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Report;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Save;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SelectAll;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Send;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Sort;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TextFormat;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Unarchive;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Undo;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Weekend;
  }

  public sealed class ResourcesDevice
  {
    public ResourcesDevice() { }

    ///<Summary>(.png) 192 x 192 (3.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccessAlarm;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccessAlarms;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccessTime;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddAlarm;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirplanemodeActive;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirplanemodeInactive;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery20;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery30;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery50;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery60;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery80;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Battery90;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryAlert;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging20;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging30;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging50;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging60;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging80;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryCharging90;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryChargingFull;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryFull;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryStd;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BatteryUnknown;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Bluetooth;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BluetoothConnected;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BluetoothDisabled;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BluetoothSearching;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrightnessAuto;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrightnessHigh;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrightnessLow;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrightnessMedium;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DataUsage;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DeveloperMode;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Devices;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dvr;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GpsFixed;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GpsNotFixed;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GpsOff;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GraphicEq;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocationDisabled;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocationSearching;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NetworkCell;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NetworkWifi;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Nfc;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ScreenLockLandscape;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ScreenLockPortrait;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ScreenLockRotation;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ScreenRotation;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SdStorage;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsSystemDaydream;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellular0bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellular1bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellular2bar;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellular3bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellular4bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularConnectedNoInternet0bar;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularConnectedNoInternet1bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularConnectedNoInternet2bar;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularConnectedNoInternet3bar;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularConnectedNoInternet4bar;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularNoSim;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularNull;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalCellularOff;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi0bar;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi1bar;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi1barLock;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi2bar;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi2barLock;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi3bar;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi3barLock;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi4bar;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifi4barLock;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SignalWifiOff;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Storage;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Usb;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wallpaper;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Widgets;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WifiLock;
    ///<Summary>(.png) 192 x 192 (3.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WifiTethering;
  }

  public sealed class ResourcesEditor
  {
    public ResourcesEditor() { }

    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AttachFile;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AttachMoney;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderAll;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderBottom;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderClear;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderColor;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderHorizontal;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderInner;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderLeft;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderOuter;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderRight;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderStyle;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderTop;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BorderVertical;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BubbleChart;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DragHandle;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatAlignCenter;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatAlignJustify;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatAlignLeft;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatAlignRight;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatBold;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatClear;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatColorFill;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatColorReset;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatColorText;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatIndentDecrease;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatIndentIncrease;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatItalic;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatLineSpacing;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatListBulleted;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatListNumbered;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatPaint;
    ///<Summary>(.png) 192 x 192 (0.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatQuote;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatShapes;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatSize;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatStrikethrough;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatTextdirectionLToR;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatTextdirectionRToL;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FormatUnderlined;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Functions;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Highlight;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertChart;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertComment;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertDriveFile;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertEmoticon;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertInvitation;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertLink;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference InsertPhoto;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LinearScale;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MergeType;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ModeComment;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ModeEdit;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MonetizationOn;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MoneyOff;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MultilineChart;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PieChart;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PieChartOutlined;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Publish;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ShortText;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ShowChart;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SpaceBar;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StrikethroughS;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TextFields;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Title;
    ///<Summary>(.png) 192 x 192 (0.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VerticalAlignBottom;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VerticalAlignCenter;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VerticalAlignTop;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WrapText;
  }

  public sealed class ResourcesFile
  {
    public ResourcesFile() { }

    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Attachment;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Cloud;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudCircle;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudDone;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudDownload;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudOff;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudQueue;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CloudUpload;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CreateNewFolder;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FileDownload;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FileUpload;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Folder;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FolderOpen;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FolderShared;
  }

  public sealed class ResourcesHardware
  {
    public ResourcesHardware() { }

    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Cast;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CastConnected;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Computer;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DesktopMac;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DesktopWindows;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DeveloperBoard;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DeviceHub;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DevicesOther;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dock;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gamepad;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Headset;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HeadsetMic;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Keyboard;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardArrowDown;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardArrowLeft;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardArrowRight;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardArrowUp;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardBackspace;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardCapslock;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardHide;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardReturn;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardTab;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference KeyboardVoice;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Laptop;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LaptopChromebook;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LaptopMac;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LaptopWindows;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Memory;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Mouse;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneAndroid;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneIphone;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Phonelink;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonelinkOff;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PowerInput;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Router;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Scanner;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Security;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SimCard;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Smartphone;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Speaker;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SpeakerGroup;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tablet;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TabletAndroid;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TabletMac;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Toys;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tv;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VideogameAsset;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Watch;
  }

  public sealed class ResourcesImage
  {
    public ResourcesImage() { }

    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddAPhoto;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddToPhotos;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Adjust;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Assistant;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AssistantPhoto;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Audiotrack;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BlurCircular;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BlurLinear;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BlurOff;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BlurOn;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness1;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness2;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness3;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness4;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness5;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness6;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brightness7;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BrokenImage;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Brush;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BurstMode;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Camera;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CameraAlt;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CameraFront;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CameraRear;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CameraRoll;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CenterFocusStrong;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CenterFocusWeak;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Collections;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CollectionsBookmark;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Colorize;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ColorLens;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Compare;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ControlPoint;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ControlPointDuplicate;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Crop;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Crop169;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Crop32;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Crop54;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Crop75;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropDin;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropFree;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropLandscape;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropOriginal;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropPortrait;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropRotate;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CropSquare;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Dehaze;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Details;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Edit;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Exposure;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExposureNeg1;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExposureNeg2;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExposurePlus1;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExposurePlus2;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExposureZero;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter1;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter2;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter3;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter4;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter5;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter6;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter7;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter8;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter9;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Filter9plus;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterBAndW;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterCenterFocus;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterDrama;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterFrames;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterHdr;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterNone;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterTiltShift;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FilterVintage;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Flare;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlashAuto;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlashOff;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FlashOn;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Flip;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Gradient;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Grain;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GridOff;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GridOn;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HdrOff;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HdrOn;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HdrStrong;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HdrWeak;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Healing;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Image;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ImageAspectRatio;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Iso;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Landscape;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LeakAdd;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LeakRemove;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Lens;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LinkedCamera;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Looks;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Looks3;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Looks4;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Looks5;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Looks6;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LooksOne;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LooksTwo;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Loupe;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MonochromePhotos;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MovieCreation;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MovieFilter;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MusicNote;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Nature;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NaturePeople;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NavigateBefore;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NavigateNext;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Palette;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Panorama;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PanoramaFishEye;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PanoramaHorizontal;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PanoramaVertical;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PanoramaWideAngle;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Photo;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoAlbum;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoCamera;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoFilter;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoLibrary;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoSizeSelectActual;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoSizeSelectLarge;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhotoSizeSelectSmall;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PictureAsPdf;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Portrait;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveRedEye;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Rotate90degreesCcw;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RotateLeft;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RotateRight;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Slideshow;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Straighten;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Style;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwitchCamera;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SwitchVideo;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TagFaces;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Texture;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Timelapse;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Timer;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Timer10;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Timer3;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TimerOff;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tonality;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Transform;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tune;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewComfy;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewCompact;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Vignette;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WbAuto;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WbCloudy;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WbIncandescent;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WbIridescent;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WbSunny;
  }

  public sealed class ResourcesMaps
  {
    public ResourcesMaps() { }

    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AddLocation;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Beenhere;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Directions;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsBike;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsBoat;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsBus;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsCar;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsRailway;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsRun;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsSubway;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsTransit;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DirectionsWalk;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EditLocation;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EvStation;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Flight;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Hotel;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Layers;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LayersClear;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalActivity;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalAirport;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalAtm;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalBar;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalCafe;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalCarWash;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalConvenienceStore;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalDining;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalDrink;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalFlorist;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalGasStation;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalGroceryStore;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalHospital;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalHotel;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalLaundryService;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalLibrary;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalMall;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalMovies;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalOffer;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalParking;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPharmacy;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPhone;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPizza;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPlay;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPostOffice;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalPrintshop;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalSee;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalShipping;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocalTaxi;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Map;
    ///<Summary>(.png) 192 x 192 (2.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MyLocation;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Navigation;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NearMe;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PersonPin;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PersonPinCircle;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PinDrop;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Place;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RateReview;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Restaurant;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RestaurantMenu;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Satellite;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StoreMallDirectory;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Streetview;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Subway;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Terrain;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Traffic;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Train;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Tram;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TransferWithinAStation;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ZoomOutMap;
  }

  public sealed class ResourcesNavigation
  {
    public ResourcesNavigation() { }

    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Apps;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowBack;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowDownward;
    ///<Summary>(.png) 192 x 192 (0.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowDropDown;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowDropDownCircle;
    ///<Summary>(.png) 192 x 192 (0.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowDropUp;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowForward;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ArrowUpward;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Cancel;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Check;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChevronLeft;
    ///<Summary>(.png) 192 x 192 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChevronRight;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Close;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExpandLess;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ExpandMore;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FirstPage;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Fullscreen;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FullscreenExit;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LastPage;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Menu;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MoreHoriz;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MoreVert;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Refresh;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SubdirectoryArrowLeft;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SubdirectoryArrowRight;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference UnfoldLess;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference UnfoldMore;
  }

  public sealed class ResourcesNotification
  {
    public ResourcesNotification() { }

    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Adb;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatFlat;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatFlatAngled;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatIndividualSuite;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatLegroomExtra;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatLegroomNormal;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatLegroomReduced;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatReclineExtra;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirlineSeatReclineNormal;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BluetoothAudio;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ConfirmationNumber;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DiscFull;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DoNotDisturb;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DoNotDisturbAlt;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DoNotDisturbOff;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DoNotDisturbOn;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference DriveEta;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EnhancedEncryption;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EventAvailable;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EventBusy;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference EventNote;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FolderSpecial;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LiveTv;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Mms;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference More;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NetworkCheck;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NetworkLocked;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NoEncryption;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference OndemandVideo;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PersonalVideo;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneBluetoothSpeaker;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneForwarded;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneInTalk;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneLocked;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoneMissed;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhonePaused;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Power;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PriorityHigh;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RvHookup;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SdCard;
    ///<Summary>(.png) 192 x 192 (0.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SimCardAlert;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Sms;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SmsFailed;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Sync;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SyncDisabled;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SyncProblem;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SystemUpdate;
    ///<Summary>(.png) 192 x 192 (1.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TapAndPlay;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference TimeToLeave;
    ///<Summary>(.png) 192 x 192 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Vibration;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VoiceChat;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference VpnLock;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wc;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Wifi;
  }

  public sealed class ResourcesPlaces
  {
    public ResourcesPlaces() { }

    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AcUnit;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AirportShuttle;
    ///<Summary>(.png) 192 x 192 (2.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AllInclusive;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BeachAccess;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BusinessCenter;
    ///<Summary>(.png) 192 x 192 (1.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Casino;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChildCare;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ChildFriendly;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FitnessCenter;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference FreeBreakfast;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GolfCourse;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HotTub;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Kitchen;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pool;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RoomService;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RvHookup;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SmokeFree;
    ///<Summary>(.png) 192 x 192 (1.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SmokingRooms;
    ///<Summary>(.png) 192 x 192 (2.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Spa;
  }

  public sealed class ResourcesSocial
  {
    public ResourcesSocial() { }

    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Cake;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Domain;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Group;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference GroupAdd;
    ///<Summary>(.png) 192 x 192 (0.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LocationCity;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Mood;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference MoodBad;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Notifications;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotificationsActive;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotificationsNone;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotificationsOff;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference NotificationsPaused;
    ///<Summary>(.png) 192 x 192 (0.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Pages;
    ///<Summary>(.png) 192 x 192 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PartyMode;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference People;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PeopleOutline;
    ///<Summary>(.png) 192 x 192 (1.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Person;
    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PersonAdd;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PersonOutline;
    ///<Summary>(.png) 192 x 192 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PlusOne;
    ///<Summary>(.png) 192 x 192 (0.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Poll;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Public;
    ///<Summary>(.png) 192 x 192 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference School;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SentimentDissatisfied;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SentimentNeutral;
    ///<Summary>(.png) 192 x 192 (2.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SentimentSatisfied;
    ///<Summary>(.png) 192 x 192 (3.0KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SentimentVeryDissatisfied;
    ///<Summary>(.png) 192 x 192 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SentimentVerySatisfied;
    ///<Summary>(.png) 192 x 192 (1.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Share;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Whatshot;
  }

  public sealed class ResourcesToggle
  {
    public ResourcesToggle() { }

    ///<Summary>(.png) 96 x 96 (0.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CheckBox;
    ///<Summary>(.png) 96 x 96 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference CheckBoxOutlineBlank;
    ///<Summary>(.png) 96 x 96 (0.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference IndeterminateCheckBox;
    ///<Summary>(.png) 96 x 96 (1.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RadioButtonChecked;
    ///<Summary>(.png) 96 x 96 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RadioButtonUnchecked;
    ///<Summary>(.png) 192 x 192 (1.6KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Star;
    ///<Summary>(.png) 192 x 192 (2.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StarBorder;
    ///<Summary>(.png) 192 x 192 (2.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StarHalf;
  }
}