﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inv
{
  public static class BuildShell
  {
    public static void Run(Inv.BuildScript Script)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 800;
        Inv.WpfShell.Options.DefaultWindowHeight = 1000;
        Inv.WpfShell.Run(A => new BuildApplication(A, Script));
      });
    }
  }
}
