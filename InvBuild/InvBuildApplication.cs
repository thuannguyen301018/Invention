﻿/*! 11 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  internal sealed class BuildApplication : Inv.Mimic<Inv.Application>
  {
    public BuildApplication(Inv.Application Base, BuildScript Script)
    {
      this.Base = Base;

      var ApplicationFilePath = System.Reflection.Assembly.GetEntryAssembly().Location;
      var ApplicationFolderPath = System.IO.Path.GetDirectoryName(ApplicationFilePath);

      this.LogFolder = new Inv.LogDirectory(System.IO.Path.Combine(ApplicationFolderPath, "Logs")).ToFolder();
      this.BuildLogSeries = LogFolder.AddSeries("Build Logs", Script.Title, "build.log");

      Script.StepSet.Clear();
      Script.TaskSet.Clear();
      Script.StepSet.Clear();

      Base.Title = Script.Title;

      var Surface = Base.Window.NewSurface();
      Base.Window.Transition(Surface);
      Surface.Background.Colour = Inv.Colour.WhiteSmoke;

      var MainDock = Surface.NewVerticalDock();
      Surface.Content = MainDock;

      var MainScroll = Surface.NewVerticalScroll();
      MainDock.AddClient(MainScroll);

      var ActionBar = new BuildActionBar(Surface);
      MainDock.AddFooter(ActionBar);

      // TODO: scripts/tasks/steps selected label?

      var BuildButton = ActionBar.AddFooterButton();
      BuildButton.Colour = Inv.Colour.ForestGreen;
      BuildButton.Image = Resources.Images.ReplayWhite;
      BuildButton.Text = "BUILD";
      BuildButton.ExecuteEvent += () => Execute(Script);

      void BuildRefresh() => BuildButton.IsEnabled = Script.ProcedureSet.Count > 0 && Script.TaskSet.Count > 0 && Script.StepSet.Count > 0;

      var ProcedureStack = Surface.NewVerticalStack();
      MainScroll.Content = ProcedureStack;

      foreach (var Procedure in Script.GetProcedures())
      {
        var ProcedureButton = new BuildToggleButton(Surface, 0);
        ProcedureStack.AddPanel(ProcedureButton);
        ProcedureButton.Header = Procedure.Name;
        ProcedureButton.Colour = ProcedureColour;
        ProcedureButton.IsChecked = Script.ProcedureSet.Contains(Procedure);

        var TaskStack = Surface.NewVerticalStack();
        ProcedureStack.AddPanel(TaskStack);
        TaskStack.Visibility.Set(ProcedureButton.IsChecked);

        var TaskButtonList = new Inv.DistinctList<BuildToggleButton>();

        ProcedureButton.CheckEvent += () =>
        {
          ProcedureButton.IsChecked = Script.ProcedureSet.Toggle(Procedure);
          TaskStack.Visibility.Set(ProcedureButton.IsChecked);

          foreach (var TaskButton in TaskButtonList)
            TaskButton.Check(ProcedureButton.IsChecked);
        };

        foreach (var Task in Procedure.GetTasks())
        {
          var TaskButton = new BuildToggleButton(Surface, 1);
          TaskStack.AddPanel(TaskButton);
          TaskButton.Header = Task.Name;
          TaskButton.Colour = TaskColour;
          TaskButton.IsChecked = Script.TaskSet.Contains(Task);

          var StepStack = Surface.NewVerticalStack();
          TaskStack.AddPanel(StepStack);
          StepStack.Visibility.Set(TaskButton.IsChecked);

          var StepButtonList = new Inv.DistinctList<BuildToggleButton>();

          TaskButton.CheckEvent += () =>
          {
            TaskButton.IsChecked = Script.TaskSet.Toggle(Task);
            StepStack.Visibility.Set(TaskButton.IsChecked);

            foreach (var StepButton in StepButtonList)
              StepButton.Check(TaskButton.IsChecked);
          };

          foreach (var Step in Task.GetSteps())
          {
            var StepButton = new BuildToggleButton(Surface, 2);
            StepStack.AddPanel(StepButton);
            StepButton.Header = Step.Name;
            StepButton.Footer = Step.Description;
            StepButton.Colour = StepColour;
            StepButton.IsChecked = Script.StepSet.Contains(Step);
            StepButton.CheckEvent += () =>
            {
              StepButton.IsChecked = Script.StepSet.Toggle(Step);

              BuildRefresh();
            };

            StepButtonList.Add(StepButton);
          }

          TaskButtonList.Add(TaskButton);
        }
      }

      BuildRefresh();
    }

    private void Execute(BuildScript Script)
    {
      var MainContext = new BuildContext();

      var PreviousSurface = Base.Window.ActiveSurface;

      var Surface = Base.Window.NewSurface();
      Surface.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.Window.Transition(Surface).CarouselNext();

      var MainDock = Surface.NewVerticalDock();
      Surface.Content = MainDock;

      var MainScroll = Surface.NewVerticalScroll();
      MainDock.AddClient(MainScroll);

      var ActionBar = new BuildActionBar(Surface);
      MainDock.AddFooter(ActionBar);

      var CloseButton = ActionBar.AddHeaderButton();
      CloseButton.IsEnabled = false;
      CloseButton.Colour = Inv.Colour.DarkGray;
      CloseButton.Image = Resources.Images.NavigateBackWhite;
      CloseButton.Text = "CLOSE";

      var StopButton = ActionBar.AddFooterButton();
      StopButton.IsEnabled = false;
      StopButton.Colour = Inv.Colour.DarkRed;
      StopButton.Image = Resources.Images.StopWhite;
      StopButton.Text = "STOP";

      var MainStack = Surface.NewVerticalStack();
      MainScroll.Content = MainStack;
      MainStack.Alignment.CenterStretch();

      var ProgressLabel = Surface.NewLabel();
      MainStack.AddPanel(ProgressLabel);
      ProgressLabel.Margin.Set(20, 20, 20, 0);
      ProgressLabel.Font.Size = 60;
      ProgressLabel.Font.Monospaced();
      ProgressLabel.Justify.Center();
      ProgressLabel.Text = "0%";

      var ElapsedLabel = Surface.NewLabel();
      MainStack.AddPanel(ElapsedLabel);
      ElapsedLabel.Margin.Set(20, 0, 20, 20);
      ElapsedLabel.Font.Size = 20;
      ElapsedLabel.Font.Colour = Inv.Colour.DimGray;
      ElapsedLabel.Font.Monospaced();
      ElapsedLabel.Justify.Center();
      ElapsedLabel.Text = "...";

      var ScriptTile = new BuildProgressTile(Surface, 0);
      MainStack.AddPanel(ScriptTile);
      ScriptTile.Colour = ProcedureColour;

      var TaskTile = new BuildProgressTile(Surface, 1);
      MainStack.AddPanel(TaskTile);
      TaskTile.Colour = TaskColour;

      var StepTile = new BuildProgressTile(Surface, 2);
      MainStack.AddPanel(StepTile);
      StepTile.Colour = StepColour;

      var LogMemo = Surface.NewMemo();
      LogMemo.Margin.Set(0, 20, 0, 20);
      LogMemo.Font.Monospaced();
      LogMemo.Font.Size = 12;
      LogMemo.Font.Colour = Inv.Colour.Black;
      LogMemo.IsReadOnly = true;

      var LogButton = Surface.NewFlatButton();
      MainStack.AddPanel(LogButton);
      LogButton.Margin.Set(5);
      LogButton.Padding.Set(5);
      LogButton.Background.Colour = Inv.Colour.WhiteSmoke;
      LogButton.IsEnabled = false;
      LogButton.SingleTapEvent += () =>
      {
        LogButton.IsEnabled = false;
        MainStack.InsertPanelAfter(LogButton, LogMemo);
        MainStack.RemovePanel(LogButton);
        LogMemo.Text = MainContext.GetLog();
      };

      var InterruptLabel = Surface.NewLabel();
      MainStack.AddPanel(InterruptLabel);
      InterruptLabel.Font.Monospaced();
      InterruptLabel.Font.Size = 30;
      InterruptLabel.Justify.Center();

      var LogLabel = Surface.NewLabel();
      LogButton.Content = LogLabel;
      LogLabel.Font.Size = 20;
      LogLabel.Font.Colour = Inv.Colour.Black;
      LogLabel.Font.Monospaced();
      LogLabel.LineWrapping = false;
      LogLabel.Justify.Center();

      var StartTime = DateTime.Now;

      var ElapsedTimer = Base.Window.NewTimer();
      ElapsedTimer.IntervalTime = TimeSpan.FromSeconds(1);
      ElapsedTimer.IntervalEvent += () =>
      {
        ElapsedLabel.Text = (DateTime.Now - StartTime).TruncateMilliseconds().ToString();
      };
      ElapsedTimer.Start();

      var MainTask = Base.Window.RunTask(Thread =>
      {
        var LogThrottle = Environment.TickCount;

        Thread.Post(() => StopButton.IsEnabled = true);

        LogFolder.Open();
        try
        {
          using (var BuildWriter = BuildLogSeries.AsWriter())
          {
            MainContext.LogEvent += (Line) =>
            {
              BuildWriter.WriteLine(Line);

              var LogTick = Environment.TickCount;

              if (LogTick < LogThrottle || LogTick > LogThrottle + 1000)
              {
                LogThrottle = LogTick;

                Thread.Post(() =>
                {
                  LogLabel.Text = Line;
                });
              }
            };

            var ProgressIndex = 0;
            var ProgressCount = Script.StepSet.Count;

            var ProcedureArray = Script.GetProcedures().Intersect(Script.ProcedureSet).ToArray();
            var ProcedureIndex = 1;

            foreach (var Procedure in ProcedureArray)
            {
              if (MainContext.IsCancelled)
                break;

              var ScriptHeader = string.Format("({0} of {1}) {2}", ProcedureIndex, ProcedureArray.Length, Procedure.Name);

              Thread.Post(() =>
              {
                ScriptTile.Header = ScriptHeader;
                TaskTile.Header = "";
                StepTile.Header = "";
                LogLabel.Text = "";
              });

              var TaskArray = Procedure.GetTasks().Intersect(Script.TaskSet).ToArray();
              var TaskIndex = 1;

              foreach (var Task in TaskArray)
              {
                if (MainContext.IsCancelled)
                  break;

                var TaskHeader = string.Format("({0} of {1}) {2}", TaskIndex, TaskArray.Length, Task.Name);

                Thread.Post(() =>
                {
                  TaskTile.Header = TaskHeader;
                  StepTile.Header = "";
                  LogLabel.Text = "";
                });

                var StepIndex = 1;
                var StepArray = Task.GetSteps().Intersect(Script.StepSet).ToArray();
                foreach (var Step in StepArray)
                {
                  if (MainContext.IsCancelled)
                    break;

                  var StepHeader = string.Format("({0} of {1}) {2}", StepIndex, StepArray.Length, Step.Name);
                  var StepFooter = Step.Description;

                  Thread.Post(() =>
                  {
                    StepTile.Header = StepHeader;
                    StepTile.Footer = StepFooter;
                    LogLabel.Text = "";
                  });

                  MainContext.Clear();

                  try
                  {
                    Step.Action(MainContext);
                  }
                  catch (Exception Exception)
                  {
                    MainContext.Caught(Exception);
                  }

                  StepIndex++;
                  ProgressIndex++;

                  var ProgressText = ((double)ProgressIndex / (double)ProgressCount).ToString("P0");
                  Thread.Post(() =>
                  {
                    ProgressLabel.Text = ProgressText;
                  });
                }

                TaskIndex++;
              }

              ProcedureIndex++;
            }

            Thread.Post(() =>
            {
              StopButton.IsEnabled = false;

              ElapsedTimer.Stop();
              ElapsedLabel.Text = (DateTime.Now - StartTime).TruncateMilliseconds().ToString();

              LogButton.IsEnabled = MainContext.IsCancelled && MainContext.HasLog();
              if (LogButton.IsEnabled)
                LogLabel.Text += Environment.NewLine + "(click for the full log)";
              else
                LogLabel.Text = "";

              if (MainContext.IsInterrupted)
              {
                InterruptLabel.Font.Colour = Inv.Colour.DarkOrange;
                InterruptLabel.Text = "USER INTERRUPTED";

                Base.Audio.Play(Resources.Sounds.Failed);
              }
              else if (MainContext.IsFailed)
              {
                InterruptLabel.Font.Colour = Inv.Colour.DarkRed;
                InterruptLabel.Text = "TOOL FAILED";

                Base.Audio.Play(Resources.Sounds.Failed);
              }
              else if (MainContext.Exception != null)
              {
                InterruptLabel.Font.Colour = Inv.Colour.DarkRed;
                InterruptLabel.Text = "LOGIC EXCEPTION";

                var ExceptionMemo = Surface.NewMemo();
                MainStack.AddPanel(ExceptionMemo);
                ExceptionMemo.Font.Monospaced();
                ExceptionMemo.Font.Size = 12;
                ExceptionMemo.Font.Colour = Inv.Colour.DarkRed;
                ExceptionMemo.IsReadOnly = true;
                ExceptionMemo.Text = MainContext.Exception.AsReport();

                Base.Audio.Play(Resources.Sounds.Failed);
              }
              else
              {
                InterruptLabel.Font.Colour = Inv.Colour.DarkGreen;
                InterruptLabel.Text = "COMPLETED";

                Base.Audio.Play(Resources.Sounds.Completed);
              }

              CloseButton.IsEnabled = true;
            });
          }
        }
        finally
        {
          LogFolder.Close();
        }
      });

      CloseButton.ExecuteEvent += () =>
      {
        MainTask.Wait();
        Base.Window.Transition(PreviousSurface).CarouselPrevious();
      };
      StopButton.ExecuteEvent += () =>
      {
        StopButton.IsEnabled = false;

        MainContext.Interupt();
      };
    }

    private Inv.LogFolder LogFolder;
    private Inv.LogSeries BuildLogSeries;
    private readonly Inv.Colour ProcedureColour = Inv.Colour.SteelBlue;
    private readonly Inv.Colour TaskColour = Inv.Colour.HotPink.Darken(0.50F);
    private readonly Inv.Colour StepColour = Inv.Colour.LightGray;
  }

  internal sealed class BuildActionBar : Inv.Panel<Inv.Dock>
  {
    public BuildActionBar(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewHorizontalDock();
      Base.Background.Colour = Inv.Colour.DimGray;
      Base.Border.Colour = Inv.Colour.DimGray.Darken(0.50F);
      Base.Border.Set(0, 4, 0, 0);
    }

    public BuildActionButton AddHeaderButton()
    {
      var Result = new BuildActionButton(Surface);
      Base.AddHeader(Result);
      return Result;
    }
    public BuildActionButton AddFooterButton()
    {
      var Result = new BuildActionButton(Surface);
      Base.AddFooter(Result);
      return Result;
    }

    private readonly Inv.Surface Surface;
  }

  internal sealed class BuildActionButton : Inv.Panel<Inv.Button>
  {
    public BuildActionButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Margin.Set(5);
      Base.Padding.Set(5);
      Base.Size.Set(96, 96);
      Base.Elevation.Set(2);

      var Dock = Surface.NewVerticalDock();
      Base.Content = Dock;

      this.Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Alignment.Center();
      Graphic.Margin.Set(0, 0, 4, 0);
      Graphic.Size.Set(48, 48);

      this.Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Font.Size = 20;
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Monospaced();
      Label.Justify.Center();
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public Inv.Image Image
    {
      get { return Graphic.Image; }
      set { Graphic.Image = value; }
    }
    public event Action ExecuteEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    private Label Label;
    private Graphic Graphic;
  }

  internal sealed class BuildToggleButton : Inv.Panel<Inv.Button>
  {
    public BuildToggleButton(Inv.Surface Surface, int Level)
    {
      this.Base = Surface.NewFlatButton();
      Base.Margin.Set(5 + (10 * Level), 5 - Level, 5, 0);
      Base.Padding.Set(5 - Level);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Size.Set(32 - (4 * Level), 32 - (4 * Level));
      Graphic.Margin.Set(0, 0, 4, 0);

      this.HeaderLabel = Surface.NewLabel();
      Dock.AddClient(HeaderLabel);
      HeaderLabel.Font.Size = 20 - (Level * 2);
      HeaderLabel.Font.Monospaced();

      this.FooterLabel = Surface.NewLabel();
      Dock.AddFooter(FooterLabel);
      FooterLabel.Font.Size = 20 - (Level * 2);
      FooterLabel.Font.Monospaced();
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public string Header
    {
      get { return HeaderLabel.Text; }
      set { HeaderLabel.Text = value; }
    }
    public string Footer
    {
      get { return FooterLabel.Text; }
      set { FooterLabel.Text = value; }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        var IsBlack = Base.Background.Colour.BackgroundToForeground() == Inv.Colour.Black;

        HeaderLabel.Font.Colour = value ?
          (IsBlack ? Inv.Colour.Black : Inv.Colour.White) :
          (IsBlack ? Inv.Colour.DimGray : Inv.Colour.LightGray);

        FooterLabel.Font.Colour = HeaderLabel.Font.Colour;

        Graphic.Image = value ?
          (IsBlack ? Resources.Images.CheckCircleBlack : Resources.Images.CheckCircleWhite) :
          (IsBlack ? Resources.Images.NotInterestedBlack : Resources.Images.NotInterestedWhite);
      }
    }
    public event Action CheckEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public void Check(bool IsChecked)
    {
      if (IsCheckedField != IsChecked)
        Base.SingleTap();
    }

    private Label HeaderLabel;
    private Label FooterLabel;
    private Graphic Graphic;
    private bool IsCheckedField;
  }

  internal sealed class BuildProgressTile : Inv.Panel<Inv.Dock>
  {
    public BuildProgressTile(Inv.Surface Surface, int Level)
    {
      this.Base = Surface.NewHorizontalDock();
      Base.Margin.Set(5);
      Base.Padding.Set(5);

      this.HeaderLabel = Surface.NewLabel();
      Base.AddClient(HeaderLabel);
      HeaderLabel.Font.Size = 20;
      HeaderLabel.Font.Monospaced();
      HeaderLabel.LineWrapping = false;

      this.FooterLabel = Surface.NewLabel();
      Base.AddFooter(FooterLabel);
      FooterLabel.Font.Size = 20;
      FooterLabel.Font.Monospaced();
    }

    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set
      {
        Base.Background.Colour = value;

        var IsBlack = Base.Background.Colour.BackgroundToForeground() == Inv.Colour.Black;

        HeaderLabel.Font.Colour = IsBlack ? Inv.Colour.Black : Inv.Colour.White;

        FooterLabel.Font.Colour = HeaderLabel.Font.Colour;
      }
    }
    public string Header
    {
      get { return HeaderLabel.Text; }
      set { HeaderLabel.Text = value; }
    }
    public string Footer
    {
      get { return FooterLabel.Text; }
      set { FooterLabel.Text = value; }
    }

    private Label HeaderLabel;
    private Label FooterLabel;
  }
}
