﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Database.Test
{
  public static class Shell
  {
    public static void Install(Inv.Application Application, Inv.DistinctList<Inv.Database.Platform> PlatformList)
    {
      Application.Title = "Inv.Database.Test";
      Application.StartEvent += () =>
      {
        var Surface = Application.Window.NewSurface();

        var Schematic = new Schematic();

        var EngineList = new Inv.DistinctList<Inv.Database.Engine>();
        foreach (var Platform in PlatformList)
          EngineList.Add(new Inv.Database.Engine(Platform));

        foreach (var Engine in EngineList)
        {
          // Test existence.
          if (Engine.Exists())
            Engine.Destroy();

          // Test creation and destruction.
          Engine.Create();
          if (!Engine.Exists())
            throw new Exception("Database should exist.");
          Engine.Destroy();
          if (Engine.Exists())
            throw new Exception("Database should not exist.");

          Engine.Create();

          using (var Connection = Engine.Connect(Schematic))
          {
            Connection.CreateSchematic();

            var DetectedCatalog = Connection.GetDetectedCatalog();
            var ExpectedCatalog = Connection.GetExpectedCatalog();

            Inv.Database.IntegrityEngine.Execute
            (
              ExpectedCatalog: ExpectedCatalog,
              DetectedCatalog: DetectedCatalog,
              IssueList: out var IssueList
            );

            if (IssueList.Any())
              throw new Exception("Integrity check failed: " + Environment.NewLine + IssueList.AsSeparatedText(Environment.NewLine));
          }

          // TODO: Test record insert.

          // TODO: Test record update.

          // TODO: Test record delete.
        }
      };
    }

    public sealed class Schematic : Inv.Database.Contract<Inv.Database.Schematic>
    {
      public Schematic()
      {
        this.Base = new Inv.Database.Schematic();
        Base.Compile(this);
      }

      public readonly DataTypeTable DataType;
      public readonly ReferenceTable Reference;

      Inv.Database.Schematic Inv.Database.Contract<Inv.Database.Schematic>.Base => Base;

      private Inv.Database.Schematic Base;
    }

    public sealed class DataTypeTable : Inv.Mimic<Inv.Database.Table>, Inv.Database.Contract<Inv.Database.Table>
    {
      public DataTypeTable(Inv.Database.Table Base)
      {
        this.Base = Base;
      }

      public readonly Inv.Database.PrimaryKeyColumn PrimaryKey;
      public readonly Inv.Database.BinaryColumn Binary;
      public readonly Inv.Database.BooleanColumn Boolean;
      public readonly Inv.Database.DateColumn Date;
      public readonly Inv.Database.DateTimeColumn DateTime;
      public readonly Inv.Database.DateTimeOffsetColumn DateTimeOffset;
      public readonly Inv.Database.DecimalColumn Decimal;
      public readonly Inv.Database.DoubleColumn Double;
      public readonly Inv.Database.EnumColumn<TestEnum> Enum;
      public readonly Inv.Database.GuidColumn Guid;
      public readonly Inv.Database.ImageColumn Image;
      public readonly Inv.Database.Integer32Column Integer32;
      public readonly Inv.Database.Integer64Column Integer64;
      public readonly Inv.Database.MoneyColumn Money;
      public readonly Inv.Database.StringColumn String;
      public readonly Inv.Database.TimeColumn Time;
      public readonly Inv.Database.TimeSpanColumn TimeSpan;
      public readonly Inv.Database.ForeignKeyColumn<ReferenceTable> ForeignKey;
      public readonly Inv.Database.BinaryColumn.Null BinaryNull;
      public readonly Inv.Database.BooleanColumn.Null BooleanNull;
      public readonly Inv.Database.DateColumn.Null DateNull;
      public readonly Inv.Database.DateTimeColumn.Null DateTimeNull;
      public readonly Inv.Database.DateTimeOffsetColumn.Null DateTimeOffsetNull;
      public readonly Inv.Database.DecimalColumn.Null DecimalNull;
      public readonly Inv.Database.DoubleColumn.Null DoubleNull;
      public readonly Inv.Database.EnumColumn<TestEnum>.Null EnumNull;
      public readonly Inv.Database.GuidColumn.Null GuidNull;
      public readonly Inv.Database.ImageColumn.Null ImageNull;
      public readonly Inv.Database.Integer32Column.Null Integer32Null;
      public readonly Inv.Database.Integer64Column.Null Integer64Null;
      public readonly Inv.Database.MoneyColumn.Null MoneyNull;
      public readonly Inv.Database.StringColumn.Null StringNull;
      public readonly Inv.Database.TimeColumn.Null TimeNull;
      public readonly Inv.Database.TimeSpanColumn.Null TimeSpanNull;
      public readonly Inv.Database.ForeignKeyColumn<ReferenceTable>.Null ForeignKeyNull;

      Inv.Database.Table Inv.Database.Contract<Inv.Database.Table>.Base => Base;

      private Inv.Database.DuplicateIndex DK_Date => Inv.Database.Index.Duplicate(Date);
      private Inv.Database.DuplicateIndex DK_Date_String => Inv.Database.Index.Duplicate(Date, String);
      private Inv.Database.UniqueIndex UK_String => Inv.Database.Index.Unique(String);
      private Inv.Database.UniqueIndex UK_Integer64_String => Inv.Database.Index.Unique(Integer64, String);
    }

    public sealed class ReferenceTable : Inv.Mimic<Inv.Database.Table>, Inv.Database.Contract<Inv.Database.Table>
    {
      public ReferenceTable(Inv.Database.Table Base)
      {
        this.Base = Base;
      }

      public readonly Inv.Database.PrimaryKeyColumn PrimaryKey;

      Inv.Database.Table Inv.Database.Contract<Inv.Database.Table>.Base => Base;
    }

    public enum TestEnum
    {
      A, B, C
    }
  }
}