﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvTestClient
{
  class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      var ScreenWidth = Inv.WpfShell.PrimaryScreenWidth;
      var ScreenHeight = Inv.WpfShell.PrimaryScreenHeight;

      var ClientWidth = Math.Min(1024, ScreenWidth / 2);
      var ClientHeight = Math.Min(1600, ScreenHeight - 40);

      Inv.ServerShell.SocketHost = "10.250.22.43";

      Inv.WpfShell.FullScreenMode = false;
      Inv.WpfShell.DefaultWindowX = 0;
      Inv.WpfShell.DefaultWindowY = 0;
      Inv.WpfShell.DefaultWindowWidth = ClientWidth;
      Inv.WpfShell.DefaultWindowHeight = ClientHeight;
      Inv.WpfShell.SingletonIdentity = "TEST-01";

      Inv.WpfShell.Run(A => new Inv.ServerApplication(A, Guid.NewGuid()));
    }
  }
}
