﻿/*! 12 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inv
{
  public class Worker : MarshalByRefObject
  {
    public void SetGlobal(string Text)
    {
      Global = Text;
    }
    public void PrintDomain()
    {
      Debug.WriteLine(string.Format("Object is executing in AppDomain \"{0}\" + {1}", AppDomain.CurrentDomain.FriendlyName, Global));
    }

    public static string Global;
  }

  public static class TestServerProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      //var localWorker = new Worker();
      //localWorker.SetGlobal("BOOM");
      //
      //var ad = AppDomain.CreateDomain("New domain");
      //var remoteWorker = (Worker)ad.CreateInstanceAndUnwrap(typeof(Worker).Assembly.FullName, typeof(Worker).FullName);
      //remoteWorker.SetGlobal("DIFF");
      //
      //localWorker.PrintDomain();
      //remoteWorker.PrintDomain();

      //var AppDomainType = typeof(string).GetTypeInfo().Assembly.GetType("System.AppDomain").GetTypeInfo();
      //var AppDomainCreateDomain = AppDomainType.DeclaredMethods.Find(M => M.Name == "CreateDomain" && M.GetParameters().Length == 1);
      //var NewDomain = AppDomainCreateDomain.Invoke(null, new object[] { "MyDomain" });

      //var ServerDomain = AppDomain.CreateDomain("Server");
      //var ServerInstance = ServerDomain.CreateInstanceAndUnwrap("InvTestServer", "Inv.TestServerInstance");
      //var ServerStartMethod = ServerInstance.GetType().GetMethod("Start");
      //ServerStartMethod.Invoke(ServerInstance, new object[] { });

      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 400;
        Inv.WpfShell.Options.DefaultWindowHeight = 400;
        Inv.WpfShell.Run(A =>
        {
          A.Title = "InvServer";

          Action RefreshTenants = null;

          var Engine = Inv.ServerShell.NewEngine(A, (Identity, Application) => InvTest.Shell.Install(Application));
          Engine.AcceptEvent += (Tenant) =>
          {
            RefreshTenants();
          };
          Engine.RejectEvent += (Tenant) =>
          {
            RefreshTenants();
          };
          A.StartEvent += () =>
          {
            A.Window.Background.Colour = Inv.Colour.WhiteSmoke;

            var HostSurface = A.Window.NewSurface();
            A.Window.Transition(HostSurface);

            var TenantStack = HostSurface.NewVerticalStack();
            HostSurface.Content = TenantStack;

            RefreshTenants = () =>
            {
              var TenantArray = Engine.GetTenants().Select(S => new
              {
                Title = S.InvApplication.Title,
              }).ToArray();

              A.Window.Post(() =>
              {
                TenantStack.RemovePanels();

                foreach (var Tenant in TenantArray)
                {
                  var HostButton = HostSurface.NewFlatButton();
                  TenantStack.AddPanel(HostButton);
                  HostButton.Background.Colour = Inv.Colour.DimGray;
                  HostButton.Padding.Set(5);
                  HostButton.Margin.Set(0, 0, 0, 2);

                  var HostLabel = HostSurface.NewLabel();
                  HostButton.Content = HostLabel;
                  HostLabel.Font.Size = 20;
                  HostLabel.Font.Colour = Inv.Colour.White;
                  HostLabel.Text = Tenant.Title;
                }
              });
            };

            Engine.Start();

            StartClient(Guid.NewGuid(), true);
            StartClient(Guid.NewGuid(), false);
          };
          A.StopEvent += () =>
          {
            Engine.Stop();
          };
        });
      });
    }

    private static void StartClient(Guid Identity, bool First)
    {
      var Thread = new Thread(() =>
      {
        var ScreenWidth = Inv.WpfShell.PrimaryScreenWidth;
        var ScreenHeight = Inv.WpfShell.PrimaryScreenHeight; 

        var ClientWidth = Math.Min(1024, ScreenWidth / 2);
        var ClientHeight = Math.Min(1600, ScreenHeight - 40);

        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowX = First ? 0 : ScreenWidth - ClientWidth;
        Inv.WpfShell.Options.DefaultWindowY = 0;
        Inv.WpfShell.Options.DefaultWindowWidth = ClientWidth;
        Inv.WpfShell.Options.DefaultWindowHeight = ClientHeight;
        Inv.WpfShell.Options.SingletonIdentity = Identity.ToString();

        Inv.WpfShell.Run(A => new Inv.ServerApplication(A, Identity));
      });
      Thread.IsBackground = true;
      Thread.SetApartmentState(ApartmentState.STA);
      Thread.Start();
    }
  }
}
