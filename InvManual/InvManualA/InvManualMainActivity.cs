﻿/*! 9 !*/
namespace Inv.Manual
{
  [Android.App.Activity(Label = "Invention Manual", Theme = "@android:style/Theme.NoTitleBar", MainLauncher = true, NoHistory = true)]
  public sealed class SplashActivity : Inv.AndroidSplashActivity
  {
    protected override void Install()
    {
      this.ImageResourceId = Inv.Manual.Resource.Drawable.splash;
      this.LaunchActivity = typeof(MainActivity);
      this.HockeyAppID = "c76282f92a244ca191acd5d81659e0e0";
    }
  }
  [Android.App.Activity
    (
    Label = "Invention Manual", 
    MainLauncher = false,
    Icon = "@drawable/icon", 
    ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize
    )
  ]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    protected override void Install(Inv.Application Application)
    {
      Inv.Manual.Shell.Install(Application);
    }
  }
}