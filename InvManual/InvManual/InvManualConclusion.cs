﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class ConclusionSurface : Inv.Mimic<Inv.Surface>
  {
    public ConclusionSurface(Application Application)
    {
      this.Base = Application.NewSurface();
      Base.Background.Colour = Theme.SheetColour;

      var OuterDock = Base.NewVerticalDock();
      Base.Content = OuterDock;

      var NavigatePanel = new NavigatePanel(Base);
      OuterDock.AddFooter(NavigatePanel);
      NavigatePanel.BackEvent += () => Application.BackTopic(Application.GetBooks().Last().GetTopics().Last());
      NavigatePanel.NextEvent += () => Application.NextIntroduction();
      NavigatePanel.NextText = "START AGAIN >";
      NavigatePanel.TitleIsEnabled = false;
      NavigatePanel.TitleText = "";

      var Scroll = Base.NewVerticalScroll();
      OuterDock.AddClient(Scroll);

      var Dock = Base.NewVerticalDock();
      Scroll.Content = Dock;

      var SubjectPanel = new SubjectPanel(Base);
      Dock.AddHeader(SubjectPanel);
      SubjectPanel.Text = "The End";

      this.Document = new DocumentPanel(Base);
      Dock.AddHeader(Document);

      Base.GestureBackwardEvent += () => NavigatePanel.Back();
      Base.GestureForwardEvent += () => NavigatePanel.Next();
      Base.ArrangeEvent += () =>
      {
        var LastTopic = Application.GetBooks().Last().GetTopics().Last();

        NavigatePanel.BackText = "< " + LastTopic.Subject.ToUpper();
      };
    }

    public DocumentPanel Document { get; private set; }
  }
}
