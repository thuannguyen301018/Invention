﻿/*! 10 !*/
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class AboutSurface : Inv.Mimic<Inv.Surface>
  {
    public AboutSurface(Application Application)
    {
      this.Base = Application.NewSurface();

      Base.Background.Colour = Theme.SheetColour;
      
      var Dialog = new DialogPanel(Base);
      Base.Content = Dialog;

      var OuterDock = Base.NewVerticalDock();
      Dialog.AddLayer(OuterDock);

      var NavigatePanel = new NavigatePanel(Base);
      OuterDock.AddFooter(NavigatePanel);
      NavigatePanel.BackIsEnabled = Application.PlatformTarget == DeviceTarget.IOS;
      NavigatePanel.BackText = Resources.Texts.Version ?? "?VERSION?"; // NOTE: doesn't work in Play.

      var SettingsCount = 0;
      NavigatePanel.BackEvent += () =>
      {
        SettingsCount++;
        if (SettingsCount >= 6)
        {
          NavigatePanel.BackIsEnabled = false;
          Application.ToggleSettings();
        }
      };
      NavigatePanel.NextIsEnabled = true;
      NavigatePanel.NextText = "MANUAL >";
      NavigatePanel.TitleIsEnabled = false;
#if DEBUG
      NavigatePanel.TitleText = Base.Window.Width + " x " + Base.Window.Height;
#endif
      NavigatePanel.NextEvent += () => Application.NextIntroduction();
      
      var Scroll = Base.NewVerticalScroll();
      OuterDock.AddClient(Scroll);

      var Dock = Base.NewVerticalDock();
      Scroll.Content = Dock;

      var SubjectPanel = new SubjectPanel(Base);
      Dock.AddHeader(SubjectPanel);
      SubjectPanel.Text = "About";

      this.Document = new DocumentPanel(Base);
      Dock.AddHeader(Document);

      var ControlPanel = new ControlPanel(Base, "");
      Dock.AddFooter(ControlPanel);
      ControlPanel.Margin.Set(Theme.DocumentGap, 0, Theme.DocumentGap, Theme.DocumentGap);
      
      var UpdatesButton = ControlPanel.AddButton("UPDATES");
      UpdatesButton.SelectEvent += () =>
      {
        var Flyout = Dialog.NewFlyoutPanel();

        var Frame = Base.NewFrame();
        Flyout.Content = Frame;
        Frame.Border.Set(4);
        Frame.Padding.Set(4);
        Frame.Border.Colour = Inv.Colour.DimGray;
        Frame.Background.Colour = Theme.SheetColour;
        Frame.Margin.Set(44);
        Frame.Alignment.Center();

        var Memo = Base.NewMemo();
        Frame.Content = Memo;
        Memo.IsReadOnly = true;
        Memo.Font.Name = Theme.CodeFontName;
        Memo.Font.Size = Theme.CodeFontSize;
        Memo.Font.Colour = Theme.CodeFontColour;
        Memo.Text = Resources.Texts.Updates ?? "?UPDATES?";

        Flyout.Show();
      };

      var LicenseButton = ControlPanel.AddButton("LICENSE");
      LicenseButton.SelectEvent += () =>
      {
        var Flyout = Dialog.NewFlyoutPanel();

        var Frame = Base.NewFrame();
        Flyout.Content = Frame;
        Frame.Border.Set(4);
        Frame.Border.Colour = Inv.Colour.DimGray;
        Frame.Background.Colour = Theme.SheetColour;
        Frame.Padding.Set(4);
        Frame.Margin.Set(44);
        Frame.Alignment.Center();

        var Memo = Base.NewMemo();
        Frame.Content = Memo;
        Memo.IsReadOnly = true;
        Memo.Font.Name = Theme.CodeFontName;
        Memo.Font.Size = Theme.CodeFontSize;
        Memo.Font.Colour = Theme.CodeFontColour;

        using (var LicenseStream = typeof(AboutSurface).GetReflectionInfo().Assembly.GetManifestResourceStream("Inv.Manual.LICENSE"))
        {
          if (LicenseStream == null)
          {
            Memo.Text = "https://gitlab.com/hodgskin-callan/Invention/blob/master/LICENSE";
          }
          else
          {
            using (var LicenseReader = new System.IO.StreamReader(LicenseStream))
              Memo.Text = LicenseReader.ReadToEnd();
          }
        }

        Flyout.Show();
      };

      var RateButton = ControlPanel.AddButton("RATE & REVIEW");
      RateButton.SelectEvent += () => Application.BrowseMarket();

      Base.GestureBackwardEvent += () => Base.Window.Application.Audio.Play(Resources.Sounds.ErrorTap);
      Base.GestureForwardEvent += () => NavigatePanel.Next();
      Base.ArrangeEvent += () => ControlPanel.Arrange(Base.Window.Width - (Theme.DocumentGap * 2));
    }

    public DocumentPanel Document { get; private set; }
  }
}
