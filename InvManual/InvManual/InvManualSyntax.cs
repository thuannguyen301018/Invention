﻿/*! 4 !*/
using Inv.Support;

namespace Inv.Manual
{
  internal static class SyntaxGrammar
  {
    public const string MarkKeyword = "mark";
    public const string ReadKeyword = "read";

    public static readonly Inv.Syntax.Grammar Reference = new Inv.Syntax.Grammar()
    {
      IgnoreComment = true,
      IgnoreWhitespace = true,
      KeywordCaseSensitive = true,
      AllowMultiLineString = false,
      SingleLineCommentPrefix = "//",
      MultiLineCommentStart = "/*",
      MultiLineCommentFinish = "*/",
      StringAroundQuote = '"',
      StringEscape = '\\',
      StringVerbatim = '\0',
      IdentifierOpenQuote = '[',
      IdentifierCloseQuote = ']',
      DateTimePrefix = "dt ",
      TimeSpanPrefix = "ts ",
      UseHexadecimalPrefix = true,
      UseBase64Prefix = true,
      AlwaysQuoteIdentifier = true
    };

    static SyntaxGrammar()
    {
      // Load the keywords from the reflected constant fields.
      var SyntaxGrammarType = typeof(SyntaxGrammar);

      foreach (var FieldInfo in SyntaxGrammarType.GetReflectionFields())
      {
        if (FieldInfo.IsStatic && FieldInfo.IsPublic && FieldInfo.Name.EndsWith("Keyword") && FieldInfo.FieldType == typeof(string))
          Reference.Keyword.Add((string)FieldInfo.GetValue(SyntaxGrammarType));
      }

      foreach (var Location in Inv.Support.EnumHelper.GetEnumerable<Location>())
        Reference.Keyword.Add(Location.ToString().ToLower());
    }
  }
}
