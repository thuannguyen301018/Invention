﻿/*! 26 !*/
using System;
using System.Linq;
using System.Collections.Generic;
using Inv.Support;

namespace Inv.Database.MySQL
{
  public sealed class Platform : Inv.Database.Platform
  {
    public Platform(PlatformType Type, string Server, string Port, string User, string Password, string Database)
    {
      this.Type = Type;
      this.Server = Server;
      this.Port = Port;
      this.User = User;
      this.Password = Password;
      this.Database = Database;

      this.DateAddFunctionIntervalDictionary = new Dictionary<DateAddFunctionInterval, string>()
      {
        { DateAddFunctionInterval.Year, "YEAR" },
        { DateAddFunctionInterval.Quarter, "QUARTER" },
        { DateAddFunctionInterval.Month, "MONTH" },
        { DateAddFunctionInterval.Week, "WEEK" },
        { DateAddFunctionInterval.Day, "DAY" },
        { DateAddFunctionInterval.Hour, "HOUR" },
        { DateAddFunctionInterval.Minute, "MINUTE" },
        { DateAddFunctionInterval.Second, "SECOND" }
      };
    }

    bool Inv.Database.Platform.Exists()
    {
      using (var Connection = new MySql.Data.MySqlClient.MySqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{ Database }'";

        using (var Reader = Command.ExecuteReader())
          return Reader.Read();
      }
    }
    void Inv.Database.Platform.Create()
    {
      using (var Connection = new MySql.Data.MySqlClient.MySqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"CREATE DATABASE { Database };";
        Command.ExecuteNonQuery();
      }
    }
    void Inv.Database.Platform.Drop()
    {
      using (var Connection = new MySql.Data.MySqlClient.MySqlConnection(GetConnectionString(false)))
      using (var Command = Connection.CreateCommand())
      {
        Connection.Open();

        Command.CommandText = $"DROP DATABASE { Database };";
        Command.ExecuteNonQuery();
      }
    }
    bool Inv.Database.Platform.UseManualDataTypeCheck => false;
    bool Inv.Database.Platform.UseUpdateAliases => true;
    bool Inv.Database.Platform.UseUpdateFromSyntax => false;
    bool Inv.Database.Platform.UseDeleteFromSyntax => false;
    bool Inv.Database.Platform.UseLimitSyntax => true;
    bool Inv.Database.Platform.UseForeignKeys => true;
    bool Inv.Database.Platform.UseAlterTableAddUniqueConstraint => true;
    Inv.Database.Connection Inv.Database.Platform.NewConnection()
    {
      return new Connection(this, new MySql.Data.MySqlClient.MySqlConnection(GetConnectionString(true)));
    }
    string Inv.Database.Platform.GetEscapeOpen()
    {
      return "`";
    }
    string Inv.Database.Platform.GetEscapeClose()
    {
      return "`";
    }
    string Inv.Database.Platform.GetColumnDefinition(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      var BaseColumn = Column.Base;

      var Result = "";

      switch (BaseColumn.Type)
      {
        case ColumnType.Binary:
        case ColumnType.Image:
          Result = "LONGBLOB";
          break;

        case ColumnType.Boolean:
          Result = "BIT";
          break;

        case ColumnType.Date:
          Result = "DATE";
          break;

        case ColumnType.DateTime:
          Result = "DATETIME";
          break;

        case ColumnType.Enum:
        case ColumnType.ForeignKey:
        case ColumnType.Integer32:
          Result = "INTEGER";
          break;

        case ColumnType.PrimaryKey:
          Result = "INTEGER NOT NULL AUTO_INCREMENT";
          break;

        case ColumnType.Integer64:
          Result = "BIGINT";
          break;

        case ColumnType.Decimal:
        case ColumnType.Money:
          Result = "DECIMAL";
          break;

        case ColumnType.Time:
        case ColumnType.TimeSpan:
          Result = "TIME";
          break;

        case ColumnType.DateTimeOffset:
          Result = $"VARCHAR({ Protocol.DateTimeOffsetLength })";
          break;

        case ColumnType.String:
          Result = "VARCHAR(500)"; // MySQL rows cannot exceed 65536 bytes - this is a heuristic best guess. We could use the TEXT data type, but UKs on that data type are problematic.
          break;

        case ColumnType.Guid:
          Result = "VARCHAR(36)";
          break;

        case ColumnType.Double:
          Result = "DOUBLE";
          break;

        default:
          throw new Exception("Unexpected ColumnType '" + BaseColumn.Type.ToString() + "'");
      }

      if (BaseColumn.Type != ColumnType.PrimaryKey)
        Result += (BaseColumn.Nullable ? "" : " NOT") + " NULL";

      return Result;
    }
    string Inv.Database.Platform.GetPrimaryKeyName(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      // Primary keys in MySQL are always called 'PRIMARY'.

      return "PRIMARY";
    }
    string Inv.Database.Platform.GetPrimaryKeyConstraintDefinition(Inv.Database.PrimaryKeyColumn PrimaryKeyColumn)
    {
      return $"CONSTRAINT PRIMARY KEY (`{ PrimaryKeyColumn.Name }`)";
    }
    string Inv.Database.Platform.GetForeignKeyConstraintDefinition(Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      return $"{ Syntax.ForeignKeyName(ForeignKeyColumn) } FOREIGN KEY (`{ ForeignKeyColumn.Name }`) REFERENCES `{ ForeignKeyColumn.ReferenceTable.Name }` (`{ ForeignKeyColumn.ReferenceTable.PrimaryKeyColumn.Name }`) ON DELETE NO ACTION ON UPDATE NO ACTION";
    }
    string Inv.Database.Platform.GetUniqueConstraintDefinition(Index Index)
    {
      return $"unique key { Syntax.IndexName(Index) }";
    }
    string Inv.Database.Platform.GetAsDateFunctionCall(string BaseExpression)
    {
      return $"date({ BaseExpression })";
    }
    string Inv.Database.Platform.GetDateAddFunctionCall(string BaseExpression, Inv.Database.DateAddFunctionInterval Type, string ValueExpression)
    {
      var IntervalType = DateAddFunctionIntervalDictionary.GetValueOrDefault(Type);

      if (string.IsNullOrEmpty(IntervalType))
        throw new Exception("Unexpected DateAddFunctionInterval '" + Type.ToString() + "'");

      return $"date_add({ BaseExpression }, INTERVAL { ValueExpression } { IntervalType })";
    }
    string Inv.Database.Platform.GetLastIdentityCommandText()
    {
      return "select LAST_INSERT_ID()";
    }
    Inv.Database.Catalog Inv.Database.Platform.GetCatalog(Inv.Database.Connection Connection)
    {
      var Result = new Inv.Database.Catalog();

      var TableNameDictionary = new Dictionary<string, CatalogTable>();
      var ViewNameDictionary = new Dictionary<string, CatalogView>();

      using (var TableCommand = Connection.NewCommand())
      {
        TableCommand.SetText($"select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE' AND TABLE_SCHEMA='{ Database }'");

        using (var TableReader = TableCommand.ExecuteReader())
        {
          while (TableReader.Read())
          {
            var TableName = TableReader.GetString(0);

            var Table = Result.AddTable();
            Table.Name = TableName;

            TableNameDictionary.Add(TableName, Table);
          }
        }
      }

      using (var ViewCommand = Connection.NewCommand())
      {
        ViewCommand.SetText($"select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'VIEW' AND TABLE_SCHEMA='{ Database }'");

        using (var ViewReader = ViewCommand.ExecuteReader())
        {
          while (ViewReader.Read())
          {
            var ViewName = ViewReader.GetString(0);

            var View = Result.AddView();
            View.Name = ViewName;

            ViewNameDictionary.Add(ViewName, View);
          }
        }
      }

      using (var ViewSpecificationCommand = Connection.NewCommand())
      {
        ViewSpecificationCommand.SetText($"select TABLE_NAME, VIEW_DEFINITION from INFORMATION_SCHEMA.VIEWS where TABLE_SCHEMA='{ Database }'");

        using (var ViewSpecificationReader = ViewSpecificationCommand.ExecuteReader())
        {
          while (ViewSpecificationReader.Read())
          {
            var ViewName = ViewSpecificationReader.GetString(0);
            var ViewSpecification = ViewSpecificationReader.GetString(1);

            var View = ViewNameDictionary.GetValueOrDefault(ViewName);

            if (View == null)
              throw new Exception($"Could not find view named '{ ViewName }'");

            View.Specification = ViewSpecification;
          }
        }
      }

      using (var ColumnCommand = Connection.NewCommand())
      {
        ColumnCommand.SetText($"SELECT TABLE_NAME, COLUMN_NAME, IS_NULLABLE, DATA_TYPE, COLUMN_KEY, EXTRA, CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA='{ Database }'");

        using (var ColumnReader = ColumnCommand.ExecuteReader())
        {
          while (ColumnReader.Read())
          {
            var TableName = ColumnReader.GetString(0);
            var ColumnName = ColumnReader.GetString(1);
            var Nullable = ColumnReader.GetString(2) == "YES";
            var DataType = ColumnReader.GetString(3);
            var ColumnKey = ColumnReader.GetString(4);
            var Extra = ColumnReader.GetString(5);
            var CharacterMaximumLength = ColumnReader.IsNull(6) ? (long?)null : ColumnReader.GetInteger64(6);

            CatalogColumn Column;

            var Table = TableNameDictionary.GetValueOrDefault(TableName);
            if (Table == null)
            {
              var View = ViewNameDictionary.GetValueOrDefault(TableName);

              if (View == null)
                throw new Exception($"Could not find table or view named '{ TableName }'");

              Column = View.AddColumn();
            }
            else
            {
              Column = Table.AddColumn();
            }

            Column.Name = ColumnName;
            Column.Nullable = Nullable;

            if (DataType.Equals("int") && ColumnKey.Equals("PRI") && Extra.Equals("auto_increment"))
              Column.DataType = "primary";
            else
              Column.DataType = DataType;

            if (CharacterMaximumLength != null && Column.DataType.Equals("varchar", StringComparison.CurrentCultureIgnoreCase))
              Column.DataType += "(" + CharacterMaximumLength.ToString() + ")";
          }
        }
      }

      using (var IndexCommand = Connection.NewCommand())
      {
        IndexCommand.SetText($"SELECT TABLE_NAME, INDEX_NAME, COLUMN_NAME, SEQ_IN_INDEX, NON_UNIQUE FROM INFORMATION_SCHEMA.STATISTICS where TABLE_SCHEMA = '{ Database }' order by TABLE_NAME, INDEX_NAME, SEQ_IN_INDEX");

        using (var IndexReader = IndexCommand.ExecuteReader())
        {
          while (IndexReader.Read())
          {
            var TableName = IndexReader.GetString(0);
            var IndexName = IndexReader.GetString(1);
            var ColumnName = IndexReader.GetString(2);
            var Sequence = IndexReader.GetInteger32(3);
            var IsDuplicate = IndexReader.GetBoolean(4);

            var Table = TableNameDictionary.GetValueOrDefault(TableName);
            if (Table == null)
              throw new Exception($"Could not find table '{ TableName }' for index '{ IndexName }'");

            var Column = Table.ColumnList.Find(C => C.Name.Equals(ColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (Column == null)
              throw new Exception($"Could not find column '{ ColumnName }' on table '{ TableName }' for index '{ IndexName }'");

            var Index = Table.IndexList.Find(I => I.Name.Equals(IndexName, StringComparison.CurrentCultureIgnoreCase));
            if (Index == null)
            {
              Index = new CatalogIndex();
              Table.IndexList.Add(Index);
              Index.Name = IndexName;
              Index.IsUnique = !IsDuplicate;
            }

            // No need to do sorting or indexing here, as the query is ordered by SEQ_IN_INDEX already.
            Index.ColumnList.Add(Column);
          }
        }
      }

      using (var ForeignKeyCommand = Connection.NewCommand())
      {
        ForeignKeyCommand.SetText($"SELECT RC.CONSTRAINT_NAME, RC.TABLE_NAME, KC.COLUMN_NAME, RC.REFERENCED_TABLE_NAME, KC.REFERENCED_COLUMN_NAME, RC.UPDATE_RULE, RC.DELETE_RULE from INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS `RC` inner join information_schema.KEY_COLUMN_USAGE `KC` on `KC`.CONSTRAINT_NAME = `RC`.CONSTRAINT_NAME where `RC`.CONSTRAINT_SCHEMA='{ Database }'");

        using (var ForeignKeyReader = ForeignKeyCommand.ExecuteReader())
        {
          while (ForeignKeyReader.Read())
          {
            var ForeignKeyName = ForeignKeyReader.GetString(0);
            var ForeignKeyTableName = ForeignKeyReader.GetString(1);
            var ForeignKeyColumnName = ForeignKeyReader.GetString(2);
            var ForeignKeyReferenceTableName = ForeignKeyReader.GetString(3);
            var ForeignKeyReferenceColumnName = ForeignKeyReader.GetString(4);
            var ForeignKeyUpdateRule = ForeignKeyReader.GetString(5);
            var ForeignKeyDeleteRule = ForeignKeyReader.GetString(6);

            var Table = TableNameDictionary.GetValueOrDefault(ForeignKeyTableName);
            if (Table == null)
              throw new Exception($"Could not find table '{ ForeignKeyTableName }' for foreign key '{ ForeignKeyName }'");

            var Column = Table.ColumnList.Find(C => C.Name.Equals(ForeignKeyColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (Column == null)
              throw new Exception($"Could not find column '{ ForeignKeyColumnName }' on table '{ ForeignKeyTableName }' for foreign key '{ ForeignKeyName }'");

            var ReferenceTable = TableNameDictionary.GetValueOrDefault(ForeignKeyReferenceTableName);
            if (ReferenceTable == null)
              throw new Exception($"Could not find table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

            var ReferenceColumn = ReferenceTable.ColumnList.Find(C => C.Name.Equals(ForeignKeyReferenceColumnName, StringComparison.CurrentCultureIgnoreCase));
            if (ReferenceColumn == null)
              throw new Exception($"Could not find column '{ ForeignKeyReferenceColumnName }' on table '{ ForeignKeyReferenceTableName }' for foreign key '{ ForeignKeyName }'");

            // TODO: Shouldn't be an exception.
            if (!ForeignKeyUpdateRule.Equals("NO ACTION", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception("NO ACTION update rule required for all foreign keys");

            // TODO: Shouldn't be an exception.
            if (!ForeignKeyDeleteRule.Equals("NO ACTION", StringComparison.CurrentCultureIgnoreCase))
              throw new Exception("NO ACTION update rule required for all foreign keys");

            var ForeignKey = new CatalogForeignKey();
            Column.ForeignKey = ForeignKey;
            ForeignKey.Name = ForeignKeyName;
            ForeignKey.ReferenceTable = ReferenceTable;
            ForeignKey.ReferenceColumn = ReferenceColumn;
          }
        }
      }

      return Result;
    }
    string Inv.Database.Platform.GetCatalogColumnDataType(Inv.Database.Contract<Inv.Database.Column> Column)
    {
      var Base = Column.Base;
      var ColumnType = Base.Type;

      switch (ColumnType)
      {
        case Inv.Database.ColumnType.PrimaryKey:
          return "primary";

        case Inv.Database.ColumnType.String:
          return "varchar(500)";

        case ColumnType.Binary:
        case ColumnType.Image:
          return "longblob";

        case ColumnType.Boolean:
          return "bit";

        case ColumnType.Date:
          return "date";

        case ColumnType.DateTime:
          return "datetime";

        case ColumnType.DateTimeOffset:
          return $"varchar({ Protocol.DateTimeOffsetLength })";

        case ColumnType.Enum:
        case ColumnType.ForeignKey:
        case ColumnType.Integer32:
          return "int";

        case ColumnType.Integer64:
          return "bigint";

        case ColumnType.Decimal:
        case ColumnType.Money:
          return "decimal";

        case ColumnType.Time:
        case ColumnType.TimeSpan:
          return "time";

        case ColumnType.Guid:
          return "varchar(36)";

        case ColumnType.Double:
          return "double";

        default:
          throw new Exception($"Unexpected '{ ColumnType.ToString() }'");
      }
    }

    private string GetConnectionString(bool IncludeDatabase)
    {
      var Result = $"Server={ Server };";

      if (!string.IsNullOrWhiteSpace(Port))
        Result += $"port={ Port };";

      Result += $"user id={ User };password={ Password };";

      switch (Type)
      {
        case PlatformType.Version57:
          Result += "sslmode=none;";
          break;

        case PlatformType.Version80:
          Result += "sslmode=required;";
          break;

        default:
          throw new Exception($"Unsupported MySql version '{ Type.ToString() }'");
      }

      if (IncludeDatabase)
        Result += $"Database={ Database };";

      return Result;
    }

    private string Server;
    private string Port;
    private string User;
    private string Password;
    private string Database;
    private PlatformType Type;
    private Dictionary<Inv.Database.DateAddFunctionInterval, string> DateAddFunctionIntervalDictionary;
  }

  public enum PlatformType
  {
    Version57,
    Version80
  }

  internal sealed class Connection : Inv.Database.Connection
  {
    internal Connection(Platform Platform, MySql.Data.MySqlClient.MySqlConnection Base)
    {
      this.Platform = Platform;
      this.Base = Base;
    }

    Inv.Database.Platform Inv.Database.Connection.Platform => Platform;
    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    void Inv.Database.Connection.Open()
    {
      Base.Open();

      var RequiredSqlModeList = new Inv.DistinctList<string>();
      RequiredSqlModeList.Add("STRICT_TRANS_TABLES");
      RequiredSqlModeList.Add("STRICT_ALL_TABLES");
      RequiredSqlModeList.Add("NO_ZERO_IN_DATE");
      RequiredSqlModeList.Add("NO_ZERO_DATE");
      RequiredSqlModeList.Add("ERROR_FOR_DIVISION_BY_ZERO");
      RequiredSqlModeList.Add("NO_ENGINE_SUBSTITUTION");

      using (var SetCommand = Base.CreateCommand())
      {
        SetCommand.CommandText = $"SET SESSION sql_mode = 'TRADITIONAL'";
        SetCommand.ExecuteNonQuery();
      }

      using (var ValidateCommand = Base.CreateCommand())
      {
        ValidateCommand.CommandText = "SELECT @@SESSION.sql_mode";

        using (var ValidateReader = ValidateCommand.ExecuteReader())
        {
          if (!ValidateReader.Read())
            throw new Exception("Unable to ensure sql_mode.");

          var SqlMode = ValidateReader.GetString(0);

          var MissingSqlModeList = RequiredSqlModeList.Where(M => !SqlMode.Contains(M));
          if (MissingSqlModeList.Any())
            throw new Exception($"sql_mode is missing the following required flags: { MissingSqlModeList.AsSeparatedText(", ", " and ") }.");
        }
      }
    }
    void Inv.Database.Connection.HealthCheck(Inv.Database.HealthCheck HealthCheck)
    {
    }
    void Inv.Database.Connection.DataTypeCheck(HealthCheckGroup CheckGroup, Inv.Database.Table Table, int PrimaryKeyValue, Inv.Database.Column Column, string RawValue)
    {
    }
    void Inv.Database.Connection.RetrySleep(int Milliseconds)
    {
      System.Threading.Thread.Sleep(Milliseconds);
    }
    Inv.Database.Transaction Inv.Database.Connection.BeginTransaction()
    {
      var Result = new Transaction(Base.BeginTransaction());
      Result.ShutdownEvent += () =>
      {
        this.Transaction = null;
      };

      this.Transaction = Result;

      return Result;
    }
    Inv.Database.Command Inv.Database.Connection.NewCommand()
    {
      var SqlCommand = Base.CreateCommand();

      if (Transaction != null)
        SqlCommand.Transaction = Transaction.Base;

      return new Command(SqlCommand);
    }

    private Platform Platform;
    private MySql.Data.MySqlClient.MySqlConnection Base;
    private Inv.Database.MySQL.Transaction Transaction;
  }

  internal sealed class Transaction : Inv.Database.Transaction
  {
    internal Transaction(MySql.Data.MySqlClient.MySqlTransaction Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    void Inv.Database.Transaction.Commit()
    {
      Base.Commit();

      Shutdown();
    }
    void Inv.Database.Transaction.Rollback()
    {
      Base.Rollback();

      Shutdown();
    }

    internal MySql.Data.MySqlClient.MySqlTransaction Base { get; }
    internal event Action ShutdownEvent;

    private void Shutdown()
    {
      if (ShutdownEvent != null)
        ShutdownEvent();
    }
  }

  internal sealed class Command : Inv.Database.Command
  {
    internal Command(MySql.Data.MySqlClient.MySqlCommand Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    Inv.Database.Reader Inv.Database.Command.ExecuteReader()
    {
      return new Reader(Base.ExecuteReader());
    }
    int Inv.Database.Command.ExecuteNonQuery()
    {
      return Base.ExecuteNonQuery();
    }
    void Inv.Database.Command.SetParameter(string Name, int Index, Inv.Database.ColumnType ColumnType, object Value)
    {
      if (ColumnType == ColumnType.Image && Value == null)
      {
        var Parameter = new MySql.Data.MySqlClient.MySqlParameter(Name, MySql.Data.MySqlClient.MySqlDbType.VarBinary, -1);
        Parameter.Value = DBNull.Value;

        Base.Parameters.Add(Parameter);
      }
      else if (ColumnType == ColumnType.DateTimeOffset && Value != null)
      {
        var DateTimeOffsetValue = (DateTimeOffset)Value;
        Base.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter(Name, DateTimeOffsetValue.ToString(Protocol.DateTimeOffsetFormat)));
      }
      else if (ColumnType == ColumnType.Date && Value != null)
      {
        var DateValue = (Inv.Date)Value;
        Base.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter(Name, DateValue.ToString(Protocol.DateFormat)));
      }
      else if (ColumnType == ColumnType.Time && Value != null)
      {
        var TimeValue = (Inv.Time)Value;
        Base.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter(Name, TimeValue.ToTimeSpan()));
      }
      else
      {
        Base.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter(Name, Value ?? DBNull.Value));
      }
    }
    void Inv.Database.Command.SetText(string Text)
    {
      Base.CommandText = Text;
    }
    void Inv.Database.Command.SetTimeout(int Seconds)
    {
      Base.CommandTimeout = Seconds;
    }

    private MySql.Data.MySqlClient.MySqlCommand Base;
  }

  internal sealed class Reader : Inv.Database.Reader
  {
    internal Reader(MySql.Data.MySqlClient.MySqlDataReader Base)
    {
      this.Base = Base;
    }

    void IDisposable.Dispose()
    {
      Base.Dispose();
    }
    bool Database.Reader.Read()
    {
      return Base.Read();
    }
    bool Database.Reader.IsNull(int ColumnIndex)
    {
      return Base.IsDBNull(ColumnIndex);
    }
    bool Database.Reader.GetBoolean(int ColumnIndex)
    {
      return Base.GetBoolean(ColumnIndex);
    }
    byte Database.Reader.GetByte(int ColumnIndex)
    {
      return Base.GetByte(ColumnIndex);
    }
    Inv.Date Database.Reader.GetDate(int ColumnIndex)
    {
      return new Inv.Date(Base.GetDateTime(ColumnIndex));
    }
    DateTime Database.Reader.GetDateTime(int ColumnIndex)
    {
      return Base.GetDateTime(ColumnIndex);
    }
    DateTimeOffset Database.Reader.GetDateTimeOffset(int ColumnIndex)
    {
      // MySqlDataReader doesn't support DateTimeOffset natively.

      return DateTimeOffset.ParseExact(Base.GetString(ColumnIndex), Protocol.DateTimeOffsetFormat, System.Globalization.CultureInfo.InvariantCulture);
    }
    decimal Database.Reader.GetDecimal(int ColumnIndex)
    {
      return Base.GetDecimal(ColumnIndex);
    }
    double Database.Reader.GetDouble(int ColumnIndex)
    {
      return Base.GetDouble(ColumnIndex);
    }
    float Database.Reader.GetFloat(int ColumnIndex)
    {
      return Base.GetFloat(ColumnIndex);
    }
    Guid Database.Reader.GetGuid(int ColumnIndex)
    {
      return Base.GetGuid(ColumnIndex);
    }
    Inv.Image Database.Reader.GetImage(int ColumnIndex)
    {
      var BufferLength = (int)Base.GetBytes(ColumnIndex, 0, null, 0, 0);
      var Buffer = new byte[BufferLength];
      int BufferIndex = 0;

      while (BufferIndex < BufferLength)
        BufferIndex += (int)Base.GetBytes(ColumnIndex, BufferIndex, Buffer, BufferIndex, BufferLength - BufferIndex);

      return new Inv.Image(Buffer, ".png");
    }
    int Inv.Database.Reader.GetInteger32(int ColumnIndex)
    {
      return Base.GetInt32(ColumnIndex);
    }
    long Inv.Database.Reader.GetInteger64(int ColumnIndex)
    {
      return Base.GetInt64(ColumnIndex);
    }
    Inv.Money Database.Reader.GetMoney(int ColumnIndex)
    {
      return new Inv.Money(Base.GetDecimal(ColumnIndex));
    }
    string Inv.Database.Reader.GetString(int ColumnIndex)
    {
      return Base.GetString(ColumnIndex);
    }
    Inv.Time Database.Reader.GetTime(int ColumnIndex)
    {
      return new Inv.Time(Base.GetTimeSpan(ColumnIndex));
    }
    TimeSpan Database.Reader.GetTimeSpan(int ColumnIndex)
    {
      return Base.GetTimeSpan(ColumnIndex);
    }

    private MySql.Data.MySqlClient.MySqlDataReader Base;
  }

  internal static class Protocol
  {
    public static readonly int DateTimeOffsetLength = 27;
    public static readonly string DateTimeOffsetFormat = "yyyyMMddHHmmssfffffffzzz";
    public static readonly string DateFormat = "yyyyMMdd";
  }
}
 