/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Text;
using System.Diagnostics;
using Android.Content;
using Android.Runtime;
using Java.Security;
using Javax.Crypto;

namespace Inv
{
  internal class AndroidVault
  {
    public AndroidVault(Context Context)
    {
      this.Context = Context;
      this.ServiceId = Context.PackageName ?? "Invention";
      this.PasswordProtection = new KeyStore.PasswordProtection(FilePassword);
    }

    public IEnumerable<Secret> LoadSecrets(Inv.Vault Vault)
    {
      lock (FileLock)
      {
        Load();

        var Result = new Inv.DistinctList<Secret>();

        var Postfix = "-" + ServiceId;

        var KeyAliasEnumerator = KeyStore.Aliases();
        while (KeyAliasEnumerator.HasMoreElements)
        {
          var KeyAlias = KeyAliasEnumerator.NextElement().ToString();
          if (KeyAlias.EndsWith(Postfix))
          {
            var KeyEntry = KeyStore.GetEntry(KeyAlias, PasswordProtection) as KeyStore.SecretKeyEntry;
            if (KeyEntry != null)
            {
              var KeyName = KeyAlias.Split('-')[0];

              var Bytes = KeyEntry.SecretKey.GetEncoded();
              var Serialized = System.Text.Encoding.UTF8.GetString(Bytes);
              var Secret = new Secret(Vault, KeyName);
              Secret.Deserialize(Serialized);
              Result.Add(Secret);
            }
          }
        }

        Result.Sort((a, b) => a.Name.CompareTo(b.Name));

        return Result;
      }
    }
    public void Load(Secret Secret)
    {
      lock (FileLock)
      {
        Load();

        var KeyEntry = KeyStore.GetEntry(MakeAlias(Secret), PasswordProtection) as KeyStore.SecretKeyEntry;

        if (KeyEntry != null)
        {
          var Bytes = KeyEntry.SecretKey.GetEncoded();
          var Serialized = System.Text.Encoding.UTF8.GetString(Bytes);
          Secret.Deserialize(Serialized);
        }
        else
        {
          Secret.Properties.Clear();
        }
      }
    }
    public void Save(Secret Secret)
    {
      lock (FileLock)
      {
        Load();

        var SecretKey = new SecretKey(Secret);
        var Entry = new KeyStore.SecretKeyEntry(SecretKey);
        KeyStore.SetEntry(MakeAlias(Secret), Entry, PasswordProtection);

        Save();
      }
    }
    public void Delete(Secret Secret)
    {
      lock (FileLock)
      {
        Load();
        
        KeyStore.DeleteEntry(MakeAlias(Secret));

        Save();
      }
    }

    [DebuggerNonUserCode]
    private void Load()
    {
      Debug.Assert(System.Threading.Monitor.IsEntered(FileLock));
      
      if (KeyStore == null)
      {
        this.KeyStore = KeyStore.GetInstance(KeyStore.DefaultType);
        try
        {
          using (var s = Context.OpenFileInput(FileName))
            KeyStore.Load(s, FilePassword);
        }
        catch (Java.IO.FileNotFoundException)
        {
          LoadEmptyKeyStore(FilePassword);
        }
      }
    }
    private void Save()
    {
      Debug.Assert(System.Threading.Monitor.IsEntered(FileLock));

      using (var Stream = Context.OpenFileOutput(FileName, FileCreationMode.Private))
        KeyStore.Store(Stream, FilePassword);
    }
    private string MakeAlias(Secret Secret)
    {
      return Secret.Name + "-" + ServiceId;
    }
    private void LoadEmptyKeyStore(char[] password)
    {
      if (id_load_Ljava_io_InputStream_arrayC == IntPtr.Zero)
        id_load_Ljava_io_InputStream_arrayC = JNIEnv.GetMethodID(KeyStore.Class.Handle, "load", "(Ljava/io/InputStream;[C)V");

      var intPtr = IntPtr.Zero;
      var intPtr2 = JNIEnv.NewArray(password);
      JNIEnv.CallVoidMethod(KeyStore.Handle, id_load_Ljava_io_InputStream_arrayC, new JValue[]
			{
				new JValue (intPtr),
				new JValue (intPtr2)
			});
      JNIEnv.DeleteLocalRef(intPtr);
      if (password != null)
      {
        JNIEnv.CopyArray(intPtr2, password);
        JNIEnv.DeleteLocalRef(intPtr2);
      }
    }

    private Context Context;
    private string ServiceId;
    private KeyStore KeyStore;
    private KeyStore.PasswordProtection PasswordProtection;

    private const string FileName = "Inv.Vault";
    private static readonly object FileLock = new object();
    private static readonly char[] FilePassword = "3295043EA18CA264B2C40E0B72051DEF2D07AD2B4593F43DDDE1515A7EC32617".ToCharArray();
    private static IntPtr id_load_Ljava_io_InputStream_arrayC;

    private sealed class SecretKey : Java.Lang.Object, ISecretKey
    {
      public SecretKey(Secret Secret)
      {
        this.bytes = System.Text.Encoding.UTF8.GetBytes(Secret.Serialize());
      }
      public byte[] GetEncoded()
      {
        return bytes;
      }
      public string Algorithm
      {
        get { return "RAW"; }
      }
      public string Format
      {
        get { return "RAW"; }
      }

      private byte[] bytes;
    }
  }
}
