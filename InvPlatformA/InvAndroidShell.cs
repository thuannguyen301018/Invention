/*! 164 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Android.Runtime;
using Android.Views;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Subclass this activity to run the Android implementation of your portable application.
  /// </summary>
  public abstract class AndroidActivity : Android.App.Activity, Android.Locations.ILocationListener, Android.Views.View.IOnSystemUiVisibilityChangeListener//, Android.Views.TextService.SpellCheckerSession.ISpellCheckerSessionListener
  {
    /// <summary>
    /// Implement this method with your portable application code.
    /// </summary>
    /// <param name="Application"></param>
    protected abstract void Install(Inv.Application Application);

    /// <summary>
    /// Always show the bottom navigation bar.
    /// When the bar is hidden it can still be accessed by swiping from the bottom of the device.
    /// </summary>
    public bool ShowNavigationBar { get; set; }
    /// <summary>
    /// Always show the top status bar.
    /// When the bar is hidden it can still be accessed by swiping from the top of the device.
    /// </summary>
    public bool ShowStatusBar { get; set; }

    /// <summary>
    /// This is called when the overall system is running low on memory.
    /// </summary>
    public override void OnLowMemory()
    {
#if DEBUG
      Bridge.Engine.Reclamation();
#endif

      base.OnLowMemory();
    }
    /// <summary>
    /// Called when a key was pressed down and not handled by any of the views.
    /// </summary>
    /// <param name="Keycode"></param>
    /// <param name="Event"></param>
    /// <returns></returns>
    public override bool OnKeyDown(Android.Views.Keycode Keycode, Android.Views.KeyEvent Event)
    {
      switch (Keycode)
      {
        case Android.Views.Keycode.VolumeUp:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Raise, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        case Android.Views.Keycode.VolumeDown:
          AudioManager.AdjustStreamVolume(Android.Media.Stream.Music, Android.Media.Adjust.Lower, Android.Media.VolumeNotificationFlags.ShowUi);
          return true;

        default:
          break;
      }

      if (!Bridge.Engine.IsInputBlocked)
      {
        Bridge.Engine.Guard(() =>
        {
          LastKeyModifier.IsLeftAlt = Event.IsAltPressed;
          LastKeyModifier.IsRightAlt = Event.IsAltPressed;
          LastKeyModifier.IsLeftCtrl = Event.IsCtrlPressed;
          LastKeyModifier.IsRightCtrl = Event.IsCtrlPressed;
          LastKeyModifier.IsLeftShift = Event.IsShiftPressed;
          LastKeyModifier.IsRightShift = Event.IsShiftPressed;

          var InvSurface = Bridge.Application.Window.ActiveSurface;
          if (InvSurface != null)
          {
            var InvKey = Bridge.Engine.TranslateKey(Keycode);
            if (InvKey != null)
              InvSurface.KeystrokeInvoke(new Inv.Keystroke(InvKey.Value, LastKeyModifier.Clone()));
          }
        });
      }

      return base.OnKeyDown(Keycode, Event);
    }
    /// <summary>
    /// Called when a key was released up and not handled by any of the views.
    /// </summary>
    /// <param name="Keycode"></param>
    /// <param name="Event"></param>
    /// <returns></returns>
    public override bool OnKeyUp([GeneratedEnum] Keycode keyCode, KeyEvent e)
    {
      Bridge.Application.Window.CheckModifier(LastKeyModifier);

      return base.OnKeyUp(keyCode, e);
    }
    /// <summary>
    /// Called when the activity has detected the user's press of the back key.
    /// </summary>
    public override void OnBackPressed()
    {
      Bridge.Engine.Guard(() =>
      {
        if (!Bridge.Engine.IsInputBlocked)
        {
          var InvSurface = Bridge.Application.Window.ActiveSurface;

          if (InvSurface != null && InvSurface.HasGestureBackward)
          {
            InvSurface.GestureBackwardInvoke();
          }
          else if (Bridge.Application.ExitQueryInvoke())
          {
            // TODO: this returns to the splash activity which effectively ends this activity.
            base.OnBackPressed();
          }
        }
      });
    }
    /// <summary>
    /// Called by the system when the device configuration changes while the activity is running.
    /// </summary>
    /// <param name="newConfig"></param>
    public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
    {
      base.OnConfigurationChanged(newConfig);

      Bridge.Engine.Guard(() =>
      {
        Bridge.Engine.Resize();

        var InvSurface = Bridge.Application.Window.ActiveSurface;
        if (InvSurface != null)
          InvSurface.ArrangeInvoke();
      });
    }
    /// <summary>
    /// Called to process touch screen events.
    /// </summary>
    /// <param name="ev"></param>
    /// <returns></returns>
    public override bool DispatchTouchEvent(Android.Views.MotionEvent ev)
    {
      var InvSurface = Bridge.Application.Window.ActiveSurface;
      if (InvSurface == null)
        return base.DispatchTouchEvent(ev);

      if (Bridge.Engine.IsInputBlocked)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return true; // ignore interaction while transitioning.
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX <= Bridge.Engine.EdgeSwipeSize && InvSurface.HasGestureBackward)
      {
        LeftEdgeSwipe = true;
        RightEdgeSwipe = false;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Down && ev.RawX >= (Bridge.Engine.DisplayMetrics.WidthPixels - Bridge.Engine.EdgeSwipeSize) && InvSurface.HasGestureForward)
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = true;

        return true;
      }
      else if (ev.Action == Android.Views.MotionEventActions.Move && (LeftEdgeSwipe || RightEdgeSwipe))
      {
        if (LeftEdgeSwipe && ev.RawX >= 0)
        {
          if (ev.RawX >= (Bridge.Engine.EdgeSwipeSize * 2))
          {
            Bridge.Engine.Guard(() => InvSurface.GestureBackwardInvoke());
            LeftEdgeSwipe = false;

            return true;
          }
        }
        else if (RightEdgeSwipe && ev.RawX <= Bridge.Engine.DisplayMetrics.WidthPixels)
        {
          if (ev.RawX <= Bridge.Engine.DisplayMetrics.WidthPixels - (Bridge.Engine.EdgeSwipeSize * 2))
          {
            Bridge.Engine.Guard(() => InvSurface.GestureForwardInvoke());
            RightEdgeSwipe = false;

            return true;
          }
        }
        else
        {
          LeftEdgeSwipe = false;
          RightEdgeSwipe = false;
        }

        return base.DispatchTouchEvent(ev);
      }
      else
      {
        LeftEdgeSwipe = false;
        RightEdgeSwipe = false;

        return base.DispatchTouchEvent(ev);
      }
    }

    /// <summary>
    /// Access the native control bridge.
    /// </summary>
    protected Inv.AndroidBridge Bridge { get; private set; }

    /// <summary>
    /// Called when the activity is starting.
    /// </summary>
    /// <param name="bundle"></param>
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      // check the required attribute parameters are set.

      if (Inv.Assert.IsEnabled)
      {
        var AndroidActivityAttribute = GetType().CustomAttributes.Find(A => A.AttributeType == typeof(Android.App.ActivityAttribute));

        var LabelParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Label");
        Inv.Assert.Check(
          LabelParameter != null && 
          LabelParameter.TypedValue.Value != null, 
          "[Activity] attribute must include the Label parameter.");

        var MainLauncherParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "MainLauncher");
        Inv.Assert.Check(
          MainLauncherParameter != null && 
          MainLauncherParameter.TypedValue.Value != null, 
          "[Activity] attribute must include the MainLauncher parameter.");

        // TODO: this is inconsistent because default themes are not always available (something to do with Android Support nuget packages?).
        //var ThemeParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Theme");
        //Inv.Assert.Check(ThemeParameter != null && ThemeParameter.TypedValue.Value != null, "[Activity] attribute must include the Theme parameter.");

        var IconParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "Icon");
        Inv.Assert.Check(
          IconParameter != null && 
          IconParameter.TypedValue.Value != null, 
          "[Activity] attribute must include the Icon parameter.");

        var ConfigurationChangesParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "ConfigurationChanges");

        var RequiredConfig = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize;

        Inv.Assert.Check(
          ConfigurationChangesParameter != null && 
          ConfigurationChangesParameter.TypedValue.Value != null && 
          ((Android.Content.PM.ConfigChanges)(ConfigurationChangesParameter.TypedValue.Value) & RequiredConfig) == RequiredConfig, 
            "[Activity] attribute must specify parameter ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize" + Environment.NewLine + "Otherwise, the activity will restart when the device orientation or screen size is changed (such as rotating the device).");
      }

      // NOTE: any exceptions in the constructor will terminate the application without any trace.

      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);

      base.OnCreate(bundle);

      this.ContentViewList = new Inv.DistinctList<Android.Views.View>();
      this.LastKeyModifier = new Inv.KeyModifier();
      this.Bridge = new Inv.AndroidBridge(this);

      AppDomain.CurrentDomain.UnhandledException += UnhandledException;
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnobservedTaskException;
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += UnhandledExceptionRaiser;

      System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

      Install(Bridge.Application);

      Bridge.Engine.Start();

      var WindowFlags = Android.Views.WindowManagerFlags.HardwareAccelerated;

      if (!ShowStatusBar)
        WindowFlags |= Android.Views.WindowManagerFlags.Fullscreen;

      Window.AddFlags(WindowFlags);

      Window.SetSoftInputMode(Android.Views.SoftInput.StateHidden);

      Window.DecorView.SetOnSystemUiVisibilityChangeListener(this);
      Window.DecorView.SetBackgroundColor(Android.Graphics.Color.Black);

      this.AudioManager = (Android.Media.AudioManager)GetSystemService(Android.Content.Context.AudioService);
      this.ClipboardManager = (Android.Content.ClipboardManager)GetSystemService(Android.Content.Context.ClipboardService);
/*
      this.TextServicesManager = (Android.Views.TextService.TextServicesManager)GetSystemService(Android.Content.Context.TextServicesManagerService);

      var SpellCheckerSession = TextServicesManager.NewSpellCheckerSession(bundle, null, this, true);
      Debug.WriteLine("w");
      SpellCheckerSession.GetSentenceSuggestions(new[] { new Android.Views.TextService.TextInfo("yacht") }, 1);
      Debug.WriteLine("x");*/
    }
    /// <summary>
    /// Perform any final cleanup before an activity is destroyed.
    /// </summary>
    protected override void OnDestroy()
    {
      // NOTE: destroy is not guaranteed to run.
      Bridge.Engine.Stop();

      // TODO: perhaps we shouldn't unhook this as tasks may be finalised when the process is being torn down.
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser -= UnhandledExceptionRaiser;
      System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnobservedTaskException;
      AppDomain.CurrentDomain.UnhandledException -= UnhandledException;

      base.OnDestroy();
    }
    /// <summary>
    /// Called when you are no longer visible to the user.
    /// </summary>
    protected override void OnStop()
    {
      // NOTE: stop seems to be called all the time...

      base.OnStop();
    }
    /// <summary>
    /// Called as part of the activity lifecycle when an activity is going into the background, but has not (yet) been killed.
    /// </summary>
    protected override void OnPause()
    {
      base.OnPause();

      if (Bridge != null && Bridge.Engine != null)
        Bridge.Engine.Pause();
    }
    /// <summary>
    /// Called after your activity is ready to start interacting with the user.
    /// </summary>
    protected override void OnResume()
    {
      base.OnResume();

      if (Bridge != null && Bridge.Engine != null)
        Bridge.Engine.Resume();
    }
    /// <summary>
    ///  Called when an activity you launched exits, giving you the requestCode started it with, the resultCode it returned, and any additional data from it.
    /// </summary>
    /// <param name="requestCode"></param>
    /// <param name="resultCode"></param>
    /// <param name="data"></param>
    protected override void OnActivityResult(int requestCode, Android.App.Result resultCode, Android.Content.Intent data)
    {
      try
      {
        if (requestCode == PickFileId && data != null)
        {
          if (ActiveFilePicker != null)
          {
            if (resultCode == Android.App.Result.Ok)
            {
              var FileName = GetRealPathFromURI(data.Data);

              var Extension = System.IO.Path.GetExtension(FileName);

              if (string.IsNullOrEmpty(Extension))
              {
                var MimeType = ContentResolver.GetType(data.Data) ?? "";

                var MimeIndex = MimeType.IndexOf('/');

                if (MimeIndex >= 0)
                  FileName += "." + MimeType.Substring(MimeIndex + 1);
              }

              Bridge.Engine.Guard(() => ActiveFilePicker.SelectInvoke(new Pick(FileName, () => ContentResolver.OpenInputStream(data.Data))));
            }
            else if (resultCode == Android.App.Result.Canceled)
            {
              Bridge.Engine.Guard(() => ActiveFilePicker.CancelInvoke());
            }
            else
            {
              // what is Android.App.Result.FirstUser?
              return;
            }

            this.ActiveFilePicker = null;
          }
        }
      }
      catch (Exception Exception)
      {
        Bridge.Engine.Guard(() =>
        {
          Bridge.Application.HandleExceptionInvoke(Exception);

          if (ActiveFilePicker != null)
            ActiveFilePicker.CancelInvoke();
        });
      }
    }

    void Android.Locations.ILocationListener.OnLocationChanged(Android.Locations.Location location)
    {
      Bridge.Engine.Guard(() => Bridge.Application.Location.ChangeInvoke(new Inv.Coordinate(location.Latitude, location.Longitude, location.Altitude)));
    }
    void Android.Locations.ILocationListener.OnProviderDisabled(string provider)
    {
    }
    void Android.Locations.ILocationListener.OnProviderEnabled(string provider)
    {
    }
    void Android.Locations.ILocationListener.OnStatusChanged(string provider, Android.Locations.Availability status, Android.OS.Bundle extras)
    {
    }
    void Android.Views.View.IOnSystemUiVisibilityChangeListener.OnSystemUiVisibilityChange(Android.Views.StatusBarVisibility Visibility)
    {
      if (((Android.Views.SystemUiFlags)Visibility & Android.Views.SystemUiFlags.Fullscreen) == 0)
        Bridge.Engine.Immersive();
    }
    //void Android.Views.TextService.SpellCheckerSession.ISpellCheckerSessionListener.OnGetSentenceSuggestions(Android.Views.TextService.SentenceSuggestionsInfo[] results)
    //{
    //  Debug.WriteLine("y");
    //}
    //void Android.Views.TextService.SpellCheckerSession.ISpellCheckerSessionListener.OnGetSuggestions(Android.Views.TextService.SuggestionsInfo[] results)
    //{
    //  Debug.WriteLine("z");
    //}

    internal const int PickFileId = 1000;
    internal bool IsTransitioning
    {
      get { return IsTransitioningField > 0; }
      set
      {
        if (value)
          this.IsTransitioningField++;
        else
          this.IsTransitioningField--;

        if (IsTransitioningField == 1)
          Window.SetFlags(Android.Views.WindowManagerFlags.NotTouchable, Android.Views.WindowManagerFlags.NotTouchable);
        else if (IsTransitioningField == 0)
          Window.ClearFlags(Android.Views.WindowManagerFlags.NotTouchable);
        else if (IsTransitioningField < 0)
          this.IsTransitioningField = 0; // just in case.
      }
    }

    internal DirectoryFilePicker ActiveFilePicker { get; set; }
    internal Android.Content.ClipboardManager ClipboardManager { get; private set; }
    //internal Android.Views.TextService.TextServicesManager TextServicesManager { get; private set; }

    /// <summary>
    /// Tracks the content views directly on this activity.
    /// </summary>
    /// <param name="view"></param>
    /// <param name="params"></param>
    public override void AddContentView(Android.Views.View view, Android.Views.ViewGroup.LayoutParams @params)
    {
      base.AddContentView(view, @params);

      ContentViewList.Add(view);
    }
    internal void RemoveContentView(Android.Views.View view)
    {
      if (view != null)
      {
        var ParentViewGroup = view.Parent as Android.Views.ViewGroup;
        if (ParentViewGroup != null)
          ParentViewGroup.RemoveView(view);

        ContentViewList.Remove(view);
      }
    }
    internal bool HasContentView(Android.Views.View view)
    {
      return ContentViewList.Contains(view);
    }
    internal Android.Views.View GetContentView()
    {
      return ContentViewList.Count > 0 ? ContentViewList[0] : null;
    }

    private void UnhandledException(object Sender, System.UnhandledExceptionEventArgs Event)
    {
      var Application = Bridge.Application;
      var Exception = Event.ExceptionObject as Exception;
      if (Application != null && Exception != null)
        Application.HandleExceptionInvoke(Exception);
    }
    private void UnobservedTaskException(object Sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs Event)
    {
      var Application = Bridge.Application;
      var Exception = Event.Exception;
      if (Application != null && Exception != null)
      {
        // A Task's exception(s) were not observed either by Waiting on the Task or accessing its Exception property. As a result, the unobserved exception was rethrown by the finalizer thread.
        // System.Net.WebException
        // The request was aborted: The request was canceled.
        //   at System.Net.HttpWebRequest+<MyGetResponseAsync>d__243.MoveNext () [0x003d5] in <38a52d63ed804d168e02e1c7dc43c55c>:0 

        bool ShouldHandleException;

        if (Exception.InnerExceptions != null && Exception.InnerExceptions.Count == 0)
        {
          ShouldHandleException = true;
        }
        else
        {
          ShouldHandleException = false;

          foreach (var IndividualException in Exception.InnerExceptions)
          {
            var WebException = IndividualException as System.Net.WebException;

            if (WebException == null || WebException.Status != System.Net.WebExceptionStatus.RequestCanceled)
              ShouldHandleException = true;
          }
        }

        if (ShouldHandleException)
          Application.HandleExceptionInvoke(Exception);
      }

      Event.SetObserved();
    }
    private void UnhandledExceptionRaiser(object Sender, Android.Runtime.RaiseThrowableEventArgs Event)
    {
      var Application = Bridge.Application;
      var Exception = Event.Exception;
      if (Application != null && Exception != null)
        Application.HandleExceptionInvoke(Exception);

      Event.Handled = true;
    }

    private string GetRealPathFromURI(Android.Net.Uri contentURI)
    {
      var returnCursor = ContentResolver.Query(contentURI, null, null, null, null);

      string Result = null;

      if (returnCursor != null)
      {
        try
        {
          var nameIndex = returnCursor.GetColumnIndex(Android.Provider.OpenableColumns.DisplayName);

          if (returnCursor.MoveToFirst())
            Result = returnCursor.GetString(nameIndex);
        }
        finally
        {
          returnCursor.Close();
        }
      }
      
      if (Result == null)
        Result = contentURI.LastPathSegment;

      return Result;
    }
    private Inv.DistinctList<Android.Views.View> ContentViewList;
    private Android.Media.AudioManager AudioManager;
    private bool LeftEdgeSwipe;
    private bool RightEdgeSwipe;
    private int IsTransitioningField;
    private Inv.KeyModifier LastKeyModifier;
  }

  /// <summary>
  /// The bridge can be used to break the abstraction and reach into the implementation controls.
  /// This is for workarounds where Invention does not adequately handle a native scenario.
  /// </summary>
  public sealed class AndroidBridge
  {
    internal AndroidBridge(AndroidActivity Activity)
    {
      this.Application = new Inv.Application();
      this.Engine = new AndroidEngine(Application, Activity);
    }

    /// <summary>
    /// Retrieve the <see cref="AndroidSurface"/>
    /// </summary>
    /// <param name="InvSurface"></param>
    /// <returns></returns>
    public AndroidSurface GetSurface(Inv.Surface InvSurface)
    {
      return Engine.TranslateSurface(InvSurface);
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidBoard"/>
    /// </summary>
    /// <param name="InvBoard"></param>
    /// <returns></returns>
    public AndroidBoard GetBoard(Inv.Board InvBoard)
    {
      return Engine.TranslateBoard(InvBoard).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidBrowser"/>
    /// </summary>
    /// <param name="InvBrowser"></param>
    /// <returns></returns>
    public AndroidBrowser GetBrowser(Inv.Browser InvBrowser)
    {
      return Engine.TranslateBrowser(InvBrowser).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidButton"/>
    /// </summary>
    /// <param name="InvButton"></param>
    /// <returns></returns>
    public AndroidButton GetButton(Inv.Button InvButton)
    {
      return Engine.TranslateButton(InvButton).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidCanvas"/>
    /// </summary>
    /// <param name="InvCanvas"></param>
    /// <returns></returns>
    public AndroidCanvas GetCanvas(Inv.Canvas InvCanvas)
    {
      return Engine.TranslateCanvas(InvCanvas).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidDock"/>
    /// </summary>
    /// <param name="InvDock"></param>
    /// <returns></returns>
    public AndroidDock GetDock(Inv.Dock InvDock)
    {
      return Engine.TranslateDock(InvDock).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidEdit"/>
    /// </summary>
    /// <param name="InvEdit"></param>
    /// <returns></returns>
    public Android.Views.View GetEdit(Inv.Edit InvEdit)
    {
      return Engine.TranslateEdit(InvEdit).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidFlow"/>
    /// </summary>
    /// <param name="InvFlow"></param>
    /// <returns></returns>
    public AndroidFlow GetFlow(Inv.Flow InvFlow)
    {
      return Engine.TranslateFlow(InvFlow).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidFrame"/>
    /// </summary>
    /// <param name="InvFrame"></param>
    /// <returns></returns>
    public AndroidFrame GetFrame(Inv.Frame InvFrame)
    {
      return Engine.TranslateFrame(InvFrame).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidGraphic"/>
    /// </summary>
    /// <param name="InvGraphic"></param>
    /// <returns></returns>
    public AndroidGraphic GetGraphic(Inv.Graphic InvGraphic)
    {
      return Engine.TranslateGraphic(InvGraphic).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidLabel"/>
    /// </summary>
    /// <param name="InvLabel"></param>
    /// <returns></returns>
    public AndroidLabel GetLabel(Inv.Label InvLabel)
    {
      return Engine.TranslateLabel(InvLabel).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidMemo"/>
    /// </summary>
    /// <param name="InvMemo"></param>
    /// <returns></returns>
    public AndroidMemo GetMemo(Inv.Memo InvMemo)
    {
      return Engine.TranslateMemo(InvMemo).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidNative"/>
    /// </summary>
    /// <param name="InvNative"></param>
    /// <returns></returns>
    public AndroidNative GetNative(Inv.Native InvNative)
    {
      return Engine.TranslateNative(InvNative).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidOverlay"/>
    /// </summary>
    /// <param name="InvOverlay"></param>
    /// <returns></returns>
    public AndroidOverlay GetOverlay(Inv.Overlay InvOverlay)
    {
      return Engine.TranslateOverlay(InvOverlay).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidScroll"/>
    /// </summary>
    /// <param name="InvScroll"></param>
    /// <returns></returns>
    public AndroidScroll GetScroll(Inv.Scroll InvScroll)
    {
      return Engine.TranslateScroll(InvScroll).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidStack"/>
    /// </summary>
    /// <param name="InvStack"></param>
    /// <returns></returns>
    public AndroidStack GetStack(Inv.Stack InvStack)
    {
      return Engine.TranslateStack(InvStack).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidTable"/>
    /// </summary>
    /// <param name="InvTable"></param>
    /// <returns></returns>
    public AndroidTable GetTable(Inv.Table InvTable)
    {
      return Engine.TranslateTable(InvTable).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidVideo"/>
    /// </summary>
    /// <param name="InvVideo"></param>
    /// <returns></returns>
    public AndroidVideo GetVideo(Inv.Video InvVideo)
    {
      return Engine.TranslateVideo(InvVideo).Element;
    }
    /// <summary>
    /// Retrieve the <see cref="AndroidWrap"/>
    /// </summary>
    /// <param name="InvWrap"></param>
    /// <returns></returns>
    public AndroidWrap GetWrap(Inv.Wrap InvWrap)
    {
      return Engine.TranslateWrap(InvWrap).Element;
    }

    internal Inv.Application Application { get; private set; }
    internal AndroidEngine Engine { get; private set; }
  }

  internal sealed class AndroidPlatform : Inv.Platform
  {
    public AndroidPlatform(AndroidEngine Engine)
    {
      this.Engine = Engine;
      this.DownloadsPath = Engine.AndroidActivity.ApplicationContext.ExternalCacheDir?.AbsolutePath;
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
      this.Vault = new AndroidVault(Engine.AndroidActivity);
    }

    int Platform.ThreadAffinity()
    {
      return System.Threading.Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      if (CalendarPicker.SetDate && CalendarPicker.SetTime)
      {
        var Fragment = new AndroidDateTimePickerFragment();
        Fragment.SelectedDate = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedDate;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.FaultEvent += (Exception) => Engine.InvApplication.HandleExceptionInvoke(Exception);
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidDateTimePickerFragment.TAG);
      }
      else if (CalendarPicker.SetDate)
      {
        var Fragment = new AndroidDatePickerFragment();
        Fragment.SelectedDate = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedDate;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.FaultEvent += (Exception) => Engine.InvApplication.HandleExceptionInvoke(Exception);
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidDatePickerFragment.TAG);
      }
      else if (CalendarPicker.SetTime)
      {
        var Fragment = new AndroidTimePickerFragment();
        Fragment.SelectedTime = CalendarPicker.Value;
        Fragment.SelectEvent += () =>
        {
          Engine.Guard(() =>
          {
            CalendarPicker.Value = Fragment.SelectedTime;
            CalendarPicker.SelectInvoke();
          });
        };
        Fragment.CancelEvent += () =>
        {
          Engine.Guard(() => CalendarPicker.CancelInvoke());
        };
        Fragment.FaultEvent += (Exception) => Engine.InvApplication.HandleExceptionInvoke(Exception);
        Fragment.Show(Engine.AndroidActivity.FragmentManager, AndroidTimePickerFragment.TAG);
      }
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      ExternalSecurityPolicyOverride();

      var EmailIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendMultiple);
      //EmailIntent.SetType("text/plain");
      EmailIntent.SetType("message/rfc822");
      EmailIntent.PutExtra(Android.Content.Intent.ExtraEmail, EmailMessage.GetTos().Select(T => T.Address).ToArray());
      EmailIntent.PutExtra(Android.Content.Intent.ExtraSubject, EmailMessage.Subject);
      EmailIntent.PutExtra(Android.Content.Intent.ExtraText, EmailMessage.Body);

      var AttachmentUris = new List<Android.OS.IParcelable>();

      // NOTE: we have to copy all attachment files from the internal storage to external storage so the email app can actually access the file.
      foreach (var Attachment in EmailMessage.GetAttachments())
      {
        var AttachmentSourcePath = Engine.SelectFilePath(Attachment.File);
        var AttachmentTargetPath = System.IO.Path.Combine(DownloadsPath, Attachment.Name);

        System.IO.File.Copy(AttachmentSourcePath, AttachmentTargetPath, true);

        var JavaFile = new Java.IO.File(AttachmentTargetPath);
        JavaFile.SetReadable(true);

        AttachmentUris.Add(Android.Net.Uri.FromFile(JavaFile));
      }

      if (AttachmentUris.Count > 0)
        EmailIntent.PutParcelableArrayListExtra(Android.Content.Intent.ExtraStream, AttachmentUris);

      if (!IsIntentSupported(EmailIntent))
        return false;

      Engine.AndroidActivity.StartActivity(EmailIntent);
      return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return IsIntentSupported(new Android.Content.Intent(Android.Content.Intent.ActionDial)); }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
      var DialIntent = new Android.Content.Intent(Android.Content.Intent.ActionDial, Android.Net.Uri.Parse("tel:" + PhoneNumber));
      if (!IsIntentSupported(DialIntent))
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "Unable to dial on this device", Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(DialIntent);
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
      var SmsIntent = new Android.Content.Intent(Android.Content.Intent.ActionSendto, Android.Net.Uri.Parse("smsto:" + PhoneNumber));
      if (!IsIntentSupported(SmsIntent))
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "Unable to SMS on this device", Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(SmsIntent);
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(Engine.SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(Engine.SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(Engine.SelectFilePath(File), Timestamp);
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536);
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536);
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(Engine.SelectFilePath(File), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(Engine.SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(Engine.SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(Engine.SelectFilePath(SourceFile), Engine.SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(Engine.SelectFilePath(SourceFile), Engine.SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new System.IO.DirectoryInfo(Engine.SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.BufferedStream(Engine.AndroidActivity.Assets.Open(Asset.Name));
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return Engine.AndroidActivity.Assets.List("").Contains(Asset.Name);
    }
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      var PickTitle = FilePicker.Title ?? "Select " + FilePicker.PickType.ToString().ToUpper() + " File";

      var PickIntent = new Android.Content.Intent(Android.Content.Intent.ActionGetContent);

      switch (FilePicker.PickType)
      {
        case PickType.Any:
          PickIntent.SetType("*/*");
          break;

        case PickType.Image:
          PickIntent.SetType("image/*");
          break;

        case PickType.Sound:
          PickIntent.SetType("audio/*");
          break;

        default:
          throw new Exception("FileType not handled: " + FilePicker.PickType);
      }

      Engine.AndroidActivity.ActiveFilePicker = FilePicker;
      Engine.AndroidActivity.StartActivityForResult(Android.Content.Intent.CreateChooser(PickIntent, PickTitle), AndroidActivity.PickFileId);
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return Engine.SelectFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return Engine.SelectFilePath(File);
    }
    bool Platform.LocationIsSupported
    {
      get { return Engine.AndroidLocationProvider != null; }
    }
    void Platform.LocationShowMap(string Location)
    {
      var ViewIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("geo:0,0?q=" + Location.Replace(new[] { ' ', ',' }, '+')));
      if (!IsIntentSupported(ViewIntent))
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "Unable to view maps on this device", Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(ViewIntent);
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
      var AndroidGeocoder = new Android.Locations.Geocoder(Engine.AndroidActivity);
      AndroidGeocoder.GetFromLocationAsync(LocationLookup.Coordinate.Latitude, LocationLookup.Coordinate.Longitude, 10).ContinueWith(Task =>
      {
        Debug.WriteLine(Task.Result.ToArray().Select(R => R.ToString()).AsSeparatedText(Environment.NewLine));

        LocationLookup.SetPlacemarks(Task.Result.Select(P => new Inv.LocationPlacemark
        (
          Name: P.Premises ?? P.GetAddressLine(0),
          Locality: P.Locality,
          SubLocality: P.SubLocality,
          PostalCode: P.PostalCode,
          AdministrativeArea: P.AdminArea,
          SubAdministrativeArea: P.SubAdminArea,
          CountryName: P.CountryName,
          CountryCode: P.CountryCode,
          Latitude: P.HasLatitude ? P.Latitude : LocationLookup.Coordinate.Latitude,
          Longitude: P.HasLongitude ? P.Longitude : LocationLookup.Coordinate.Longitude
        )));
      });
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume, float Rate, float Pan)
    {
      Engine.PlaySound(Sound, Volume, Rate, Pan, false);
    }
    TimeSpan Platform.AudioGetSoundLength(Inv.Sound Sound)
    {
      return Engine.GetSoundLength(Sound);
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      Clip.Node = Engine.PlaySound(Clip.Sound, Clip.Volume, Clip.Rate, Clip.Pan, Clip.Loop);
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      var AndroidStreamId = Clip.Node as int?;
      if (AndroidStreamId != null)
      {
        Clip.Node = null;
        Engine.SoundPool.Stop(AndroidStreamId.Value);
      }
    }
    void Platform.WindowBrowse(Inv.File File)
    {
      // NOTE: can only browse files that are in ExternalStorage.
      ExternalSecurityPolicyOverride();

      var FilePath = Engine.SelectFilePath(File);

      var BrowseFilePath = System.IO.Path.Combine(DownloadsPath, File.Name);
      System.IO.File.Copy(Engine.SelectFilePath(File), BrowseFilePath, true);

      var JavaFile = new Java.IO.File(BrowseFilePath);
      JavaFile.SetReadable(true);

      var BrowseIntent = new Android.Content.Intent(Android.Content.Intent.ActionView);
      BrowseIntent.SetDataAndType(Android.Net.Uri.FromFile(JavaFile), "application/" + File.Extension.Trim('.'));

      var AppList = Engine.AndroidActivity.PackageManager.QueryIntentActivities(BrowseIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      if (AppList.Count == 0)
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "There is no installed application to view " + File.Name, Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(BrowseIntent);
    }
    void Platform.WindowShare(Inv.File File)
    {
      // NOTE: can only browse files that are in ExternalStorage.
      ExternalSecurityPolicyOverride();

      var FilePath = Engine.SelectFilePath(File);

      var ShareFilePath = System.IO.Path.Combine(DownloadsPath, File.Name);
      System.IO.File.Copy(Engine.SelectFilePath(File), ShareFilePath, true);

      var JavaFile = new Java.IO.File(ShareFilePath);
      JavaFile.SetReadable(true);

      var ShareIntent = new Android.Content.Intent(Android.Content.Intent.ActionSend);
      ShareIntent.SetType("application/*");
      ShareIntent.PutExtra(Android.Content.Intent.ExtraStream, Android.Net.Uri.Parse($"file://{ShareFilePath}"));
      ShareIntent.AddFlags(Android.Content.ActivityFlags.GrantReadUriPermission);

      var AppList = Engine.AndroidActivity.PackageManager.QueryIntentActivities(ShareIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      if (AppList.Count == 0)
        Android.Widget.Toast.MakeText(Engine.AndroidActivity, "There is no installed application to share " + File.Name, Android.Widget.ToastLength.Short).Show();
      else
        Engine.AndroidActivity.StartActivity(ShareIntent);
    }
    void Platform.WindowPost(Action Action)
    {
      // NOTE: don't use RunOnUiThread because we always want to post to the end of the message queue.
      Engine.Post(Action);
    }
    void Platform.WindowCall(Action Action)
    {
      // TODO: Android.OS.Looper.MainLooper.IsCurrentThread is API Level 23.
      var Runnable = new Java.Lang.Runnable(Action);
      Engine.AndroidActivity.RunOnUiThread(Runnable); // NOTE: this will run immediately if we already on the UIThread (as desired).
      Runnable.Wait();
    }
    Inv.Dimension Platform.WindowGetDimension(Inv.Panel Panel)
    {
      var Result = Engine.GetPanel(Panel);

      return Result == null || Result.ContentElement == null ? Inv.Dimension.Zero : new Dimension(Engine.PxToPt(Result.ContentElement.Width), Engine.PxToPt(Result.ContentElement.Height));
    }
    void Platform.WindowStartAnimation(Inv.Animation Animation)
    {
      Engine.StartAnimation(Animation);
    }
    void Platform.WindowStopAnimation(Inv.Animation Animation)
    {
      Engine.StopAnimation(Animation);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      // NOTE: CurrentProcess.Refresh() doesn't seem to be required here on Android.
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      Engine.Reclamation();
    }
    string Platform.ProcessAncillaryInformation()
    {
      return "grefc=" + Java.Interop.JniRuntime.CurrentRuntime.GlobalReferenceCount + ";gwrefc=" + Java.Interop.JniRuntime.CurrentRuntime.WeakGlobalReferenceCount;
    }
    void Platform.WebBroadcastConnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = new Inv.Udp.Broadcast();
      
      WebBroadcast.Node = UdpBroadcast;
      WebBroadcast.SetBroadcast(UdpBroadcast);
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast WebBroadcast)
    {
      var UdpBroadcast = (Inv.Udp.Broadcast)WebBroadcast.Node;
      if (UdpBroadcast != null)
      {
        UdpBroadcast.Close();

        WebBroadcast.Node = null;
        WebBroadcast.SetBroadcast(null);        
      }
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      var BrowserIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse(Uri.AbsoluteUri));
      Engine.AndroidActivity.StartActivity(BrowserIntent);
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      var ProgressExecuting = true;

#pragma warning disable CS0618 // Type or member is obsolete (there's no replacement for this dialog, so will continue to use it until it's completely removed from Android).
      var ProgressDialog = new Android.App.ProgressDialog(Engine.AndroidActivity);
#pragma warning restore CS0618 // Type or member is obsolete
      ProgressDialog.SetTitle(Engine.InvApplication.Title);
      ProgressDialog.SetMessage("Downloading installation package...");
      ProgressDialog.SetProgressStyle(Android.App.ProgressDialogStyle.Horizontal);
      ProgressDialog.SetCancelable(false);
      ProgressDialog.SetCanceledOnTouchOutside(false);
      ProgressDialog.SetProgressNumberFormat(null);
      ProgressDialog.SetButton("CANCEL", (Sender, Event) => 
      {
        ProgressExecuting = false;
      });
      ProgressDialog.Show();

      System.Threading.Tasks.Task.Run(() =>
      {
        try
        {
#if DEBUG
          System.Net.ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) => true;

          //throw new Exception("Fake exception that breaks the download of the installation package.");
#endif
          var WebBroker = Engine.InvApplication.Web.NewBroker();

          using (var WebRequest = WebBroker.GET(Uri.AbsoluteUri))
          using (var WebResponse = WebRequest.Send())
          using (var WebStream = WebResponse.AsStream())
          {
            if (ProgressExecuting)
            {
              var AppName = Engine.AndroidActivity.ApplicationInfo.LoadLabel(Engine.AndroidActivity.PackageManager).Replace(" ", "");
              var TempFile = Java.IO.File.CreateTempFile(AppName + Guid.NewGuid(), ".apk", Engine.AndroidActivity.ExternalCacheDir);
              var TempUri = Android.Net.Uri.FromFile(TempFile);

              using (var FileStream = new System.IO.FileStream(TempUri.Path, System.IO.FileMode.Create))
              {
                const int BufferSize = 512 * 1024; // 512KB
                var Buffer = new byte[BufferSize];

                var WriteLength = 0L;
                var ReadLength = WebStream.Length;

                Engine.AndroidActivity.RunOnUiThread(() =>
                {
                  if (ReadLength > 0)
                  {
                    ProgressDialog.Max = (int)ReadLength;
                    ProgressDialog.Progress = 0;
                  }
                  else
                  {
                    ProgressDialog.Indeterminate = true;
                  }
                });

                int BufferLength;
                while (ProgressExecuting && (BufferLength = WebStream.Read(Buffer, 0, BufferSize)) > 0)
                {
                  if (Engine.AndroidActivity.IsFinishing)
                    break;

                  FileStream.Write(Buffer, 0, BufferLength);

                  WriteLength += BufferLength;

                  if (ReadLength > 0)
                  {
                    var DownloadProgress = (int)WriteLength;

                    Engine.AndroidActivity.RunOnUiThread(() =>
                    {
                      ProgressDialog.Progress = DownloadProgress;
                    });
                  }
                }
              }

              if (ProgressExecuting)
              {
                System.Threading.Thread.Sleep(1000);

                ExternalSecurityPolicyOverride();

                var UpgradeIntent = new Android.Content.Intent(Android.Content.Intent.ActionView);
                UpgradeIntent.SetDataAndType(TempUri, "application/vnd.android.package-archive");
                UpgradeIntent.AddFlags(Android.Content.ActivityFlags.NewTask);
                Engine.AndroidActivity.StartActivity(UpgradeIntent);
              }
            }
          }

          // user manually cancelled or we completed successfully.
          Engine.AndroidActivity.RunOnUiThread(() =>
          {
            ProgressDialog.Dismiss();
          });
        }
        catch (Exception Exception)
        {
          Engine.AndroidActivity.RunOnUiThread(() =>
          {
            ProgressDialog.SetMessage(Exception.Message);

            // change the CANCEL button into a CLOSE button.
            var ProgressButton = ProgressDialog.GetButton((int)Android.Content.DialogButtonTypeObsoleted.Button1);
            ProgressButton.Text = "CLOSE";
            ProgressButton.Click += (Sender, Event) =>
            {
              ProgressDialog.Dismiss();
            };
          });
        }
      });
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      var MarketIntent = new Android.Content.Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + GooglePlayID));
      MarketIntent.AddFlags(Android.Content.ActivityFlags.NoHistory | Android.Content.ActivityFlags.ClearWhenTaskReset | Android.Content.ActivityFlags.MultipleTask | Android.Content.ActivityFlags.NewTask);
      Engine.AndroidActivity.StartActivity(MarketIntent);
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      Vault.Load(Secret);
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      Vault.Save(Secret);
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      Vault.Delete(Secret);
    }
    string Platform.ClipboardGet()
    {
      return Engine.AndroidActivity.ClipboardManager.Text;
    }
    void Platform.ClipboardSet(string Text)
    {
      Engine.AndroidActivity.ClipboardManager.Text = Text;
    }
    Inv.Dimension Platform.GraphicsGetDimension(Inv.Image Image)
    {
      var Result = Engine.TranslateGraphicsBitmap(Image);

      return Result == null ? Inv.Dimension.Zero : new Inv.Dimension(Engine.PxToPt(Result.Width), Engine.PxToPt(Result.Height));
    }
    Inv.Image Platform.GraphicsGrayscale(Inv.Image Image)
    {
      var AndroidBitmap = Engine.TranslateGraphicsBitmap(Image);
      var AndroidResult = Android.Graphics.Bitmap.CreateBitmap(AndroidBitmap.Width, AndroidBitmap.Height, AndroidBitmap.GetConfig());

      using (var AndroidCanvas = new Android.Graphics.Canvas(AndroidResult))
      using (var AndroidPaint = new Android.Graphics.Paint())
      {
        var AndroidColorMatrix = new Android.Graphics.ColorMatrix();
        AndroidColorMatrix.SetSaturation(0);
        var AndroidColorFilter = new Android.Graphics.ColorMatrixColorFilter(AndroidColorMatrix);
        AndroidPaint.SetColorFilter(AndroidColorFilter);
        AndroidCanvas.DrawBitmap(AndroidBitmap, 0, 0, AndroidPaint);
      }

      return ConvertToPngInvImage(AndroidResult);
    }
    Inv.Image Platform.GraphicsTint(Inv.Image Image, Inv.Colour Colour)
    {
      var AndroidBitmap = Engine.TranslateGraphicsBitmap(Image);
      var AndroidResult = Android.Graphics.Bitmap.CreateBitmap(AndroidBitmap.Width, AndroidBitmap.Height, AndroidBitmap.GetConfig());

      var AndroidCanvas = new Android.Graphics.Canvas(AndroidResult);
      AndroidCanvas.DrawBitmap(AndroidBitmap, 0, 0, Engine.TranslateGraphicsImagePaint(1.0F, Colour));

      return ConvertToPngInvImage(AndroidResult);
    }
    Inv.Image Platform.GraphicsResize(Inv.Image Image, Inv.Dimension Dimension)
    {
      var AndroidBitmap = Engine.TranslateGraphicsBitmap(Image);

      var TargetWidth = Engine.PtToPx(Dimension.Width);
      var TargetHeight = Engine.PtToPx(Dimension.Height);

      var ScaleX = TargetWidth / (float)AndroidBitmap.Width;
      var ScaleY = TargetHeight / (float)AndroidBitmap.Height;
      var ScaleMatrix = new Android.Graphics.Matrix();
      ScaleMatrix.SetScale(ScaleX, ScaleY);

      var AndroidResult = Android.Graphics.Bitmap.CreateBitmap(TargetWidth, TargetHeight, AndroidBitmap.GetConfig() ?? Android.Graphics.Bitmap.Config.Argb8888);
      var AndroidCanvas = new Android.Graphics.Canvas(AndroidResult);
      AndroidCanvas.DrawBitmap(AndroidBitmap, null, new Android.Graphics.Rect(0, 0, TargetWidth, TargetHeight), Engine.TranslateGraphicsImagePaint(1.0F, null));

      // NOTE: this simple method creates bad quality scaled bitmaps.
      //var AndroidResult = Android.Graphics.Bitmap.CreateScaledBitmap(AndroidBitmap, Dimension.Width, Dimension.Height, true);
      
      //var AndroidResult = Android.Graphics.Bitmap.CreateBitmap(AndroidBitmap, 0, 0, AndroidBitmap.Width, AndroidBitmap.Height, ScaleMatrix, true);

      return ConvertToPngInvImage(AndroidResult);
    }

    private Inv.Image ConvertToPngInvImage(Android.Graphics.Bitmap AndroidBitmap)
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        AndroidBitmap.Compress(Android.Graphics.Bitmap.CompressFormat.Png, 100, MemoryStream);
        MemoryStream.Flush();

        var InvResult = new Inv.Image(MemoryStream.ToArray(), ".png");
        Engine.AssignImage(InvResult, AndroidBitmap);
        return InvResult;
      }
    }
    private bool IsIntentSupported(Android.Content.Intent AndroidIntent)
    {
      var AvailableActivities = Engine.AndroidActivity.PackageManager.QueryIntentActivities(AndroidIntent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);

      return AvailableActivities != null && AvailableActivities.Count > 0;
    }
    private void ExternalSecurityPolicyOverride()
    {
      Android.OS.StrictMode.SetVmPolicy(new Android.OS.StrictMode.VmPolicy.Builder().Build());
    }

    private AndroidEngine Engine;
    private AndroidVault Vault;
    private readonly string DownloadsPath;
    private System.Diagnostics.Process CurrentProcess;
  }

  internal sealed class AndroidFrameCallback : Android.Views.View, Android.Views.Choreographer.IFrameCallback
  {
    public AndroidFrameCallback(Android.Content.Context Context, Action Action)
      : base(Context)
    {
      this.Action = Action;
    }

    void Android.Views.Choreographer.IFrameCallback.DoFrame(long frameTimeNanos)
    {
      Action();
    }

    private readonly Action Action;
  }

  internal sealed class AndroidEngine
  {
    public AndroidEngine(Inv.Application InvApplication, AndroidActivity AndroidActivity)
    {
      this.InvApplication = InvApplication;
      this.AndroidActivity = AndroidActivity;
      this.ImageList = new Inv.DistinctList<Inv.Image>(16384); // 64KB memory.

      this.FillPaint = new Android.Graphics.Paint()
      {
        AntiAlias = false
      };
      FillPaint.SetStyle(Android.Graphics.Paint.Style.Fill);

      this.FillAndStrokePaint = new Android.Graphics.Paint()
      {
        AntiAlias = false
      };
      FillAndStrokePaint.SetStyle(Android.Graphics.Paint.Style.FillAndStroke);

      this.StrokePaint = new Android.Graphics.Paint()
      {
        AntiAlias = false,
        StrokeCap = Android.Graphics.Paint.Cap.Round
      };
      StrokePaint.SetStyle(Android.Graphics.Paint.Style.Stroke);

      this.ImagePaint = new Android.Graphics.Paint()
      {
        AntiAlias = true
      };

      this.TextPaint = new Android.Graphics.Paint()
      {
        AntiAlias = true
      };

      if (AndroidFoundation.SdkVersion >= 21)
        this.SoundPool = new Android.Media.SoundPool.Builder().SetMaxStreams(20).Build();
      else
#pragma warning disable CS0618 // Type or member is obsolete
        this.SoundPool = new Android.Media.SoundPool(20, Android.Media.Stream.Music, 0);
#pragma warning restore CS0618 // Type or member is obsolete

      this.SoundList = new Inv.DistinctList<Inv.Sound>(1024);

      this.RouteArray = new Inv.EnumArray<Inv.ControlType, Func<Inv.Control, AndroidNode>>()
      {
        { Inv.ControlType.Browser, P => TranslateBrowser((Inv.Browser)P) },
        { Inv.ControlType.Button, P => TranslateButton((Inv.Button)P) },
        { Inv.ControlType.Board, P => TranslateBoard((Inv.Board)P) },
        { Inv.ControlType.Dock, P => TranslateDock((Inv.Dock)P) },
        { Inv.ControlType.Edit, P => TranslateEdit((Inv.Edit)P) },
        { Inv.ControlType.Flow, P => TranslateFlow((Inv.Flow)P) },
        { Inv.ControlType.Frame, P => TranslateFrame((Inv.Frame)P) },
        { Inv.ControlType.Graphic, P => TranslateGraphic((Inv.Graphic)P) },
        { Inv.ControlType.Label, P => TranslateLabel((Inv.Label)P) },
        { Inv.ControlType.Memo, P => TranslateMemo((Inv.Memo)P) },
        { Inv.ControlType.Native, P => TranslateNative((Inv.Native)P) },
        { Inv.ControlType.Overlay, P => TranslateOverlay((Inv.Overlay)P) },
        { Inv.ControlType.Canvas, P => TranslateCanvas((Inv.Canvas)P) },
        { Inv.ControlType.Scroll, P => TranslateScroll((Inv.Scroll)P) },
        { Inv.ControlType.Stack, P => TranslateStack((Inv.Stack)P) },
        { Inv.ControlType.Switch, P => TranslateSwitch((Inv.Switch)P) },
        { Inv.ControlType.Table, P => TranslateTable((Inv.Table)P) },
        { Inv.ControlType.Video, P => TranslateVideo((Inv.Video)P) },
        { Inv.ControlType.Wrap, P => TranslateWrap((Inv.Wrap)P) },
      };

      #region Key mapping.
      this.KeyDictionary = new Dictionary<Android.Views.Keycode, Inv.Key>()
      {
        { Android.Views.Keycode.Num0, Key.n0 },
        { Android.Views.Keycode.Num1, Key.n1 },
        { Android.Views.Keycode.Num2, Key.n2 },
        { Android.Views.Keycode.Num3, Key.n3 },
        { Android.Views.Keycode.Num4, Key.n4 },
        { Android.Views.Keycode.Num5, Key.n5 },
        { Android.Views.Keycode.Num6, Key.n6 },
        { Android.Views.Keycode.Num7, Key.n7 },
        { Android.Views.Keycode.Num8, Key.n8 },
        { Android.Views.Keycode.Num9, Key.n9 },

        { Android.Views.Keycode.A, Key.A },
        { Android.Views.Keycode.B, Key.B },
        { Android.Views.Keycode.C, Key.C },
        { Android.Views.Keycode.D, Key.D },
        { Android.Views.Keycode.E, Key.E },
        { Android.Views.Keycode.F, Key.F },
        { Android.Views.Keycode.G, Key.G },
        { Android.Views.Keycode.H, Key.H },
        { Android.Views.Keycode.I, Key.I },
        { Android.Views.Keycode.J, Key.J },
        { Android.Views.Keycode.K, Key.K },
        { Android.Views.Keycode.L, Key.L },
        { Android.Views.Keycode.M, Key.M },
        { Android.Views.Keycode.N, Key.N },
        { Android.Views.Keycode.O, Key.O },
        { Android.Views.Keycode.P, Key.P },
        { Android.Views.Keycode.Q, Key.Q },
        { Android.Views.Keycode.R, Key.R },
        { Android.Views.Keycode.S, Key.S },
        { Android.Views.Keycode.T, Key.T },
        { Android.Views.Keycode.U, Key.U },
        { Android.Views.Keycode.V, Key.V },
        { Android.Views.Keycode.W, Key.W },
        { Android.Views.Keycode.X, Key.X },
        { Android.Views.Keycode.Y, Key.Y },
        { Android.Views.Keycode.Z, Key.Z },
        { Android.Views.Keycode.F1, Key.F1 },
        { Android.Views.Keycode.F2, Key.F2 },
        { Android.Views.Keycode.F3, Key.F3 },
        { Android.Views.Keycode.F4, Key.F4 },
        { Android.Views.Keycode.F5, Key.F5 },
        { Android.Views.Keycode.F6, Key.F6 },
        { Android.Views.Keycode.F7, Key.F7 },
        { Android.Views.Keycode.F8, Key.F8 },
        { Android.Views.Keycode.F9, Key.F9 },
        { Android.Views.Keycode.F10, Key.F10 },
        { Android.Views.Keycode.F11, Key.F11 },
        { Android.Views.Keycode.F12, Key.F12 },
        { Android.Views.Keycode.Escape, Key.Escape },
        { Android.Views.Keycode.Enter, Key.Enter },
        { Android.Views.Keycode.Tab, Key.Tab },
        { Android.Views.Keycode.Space, Key.Space },
        { Android.Views.Keycode.Period, Key.Period },
        { Android.Views.Keycode.Comma, Key.Comma },
        { Android.Views.Keycode.Grave, Key.BackQuote },
        { Android.Views.Keycode.Apostrophe, Key.SingleQuote },
        { Android.Views.Keycode.DpadUp, Key.Up },
        { Android.Views.Keycode.DpadDown, Key.Down },
        { Android.Views.Keycode.DpadLeft, Key.Left },
        { Android.Views.Keycode.DpadRight, Key.Right },
        { Android.Views.Keycode.MoveHome, Key.Home },
        { Android.Views.Keycode.MoveEnd, Key.End },
        { Android.Views.Keycode.PageUp, Key.PageUp },
        { Android.Views.Keycode.PageDown, Key.PageDown },
        { Android.Views.Keycode.Clear, Key.Clear },
        { Android.Views.Keycode.Insert, Key.Insert },
        { Android.Views.Keycode.Del, Key.Backspace }, // del is 'backwards delete'
        { Android.Views.Keycode.ForwardDel, Key.Delete }, // 'forwards delete'
        { Android.Views.Keycode.Backslash, Key.Backslash },
        { Android.Views.Keycode.Slash, Key.Slash },
        { Android.Views.Keycode.ShiftLeft, Key.LeftShift },
        { Android.Views.Keycode.ShiftRight, Key.RightShift },
        { Android.Views.Keycode.CtrlLeft, Key.LeftCtrl },
        { Android.Views.Keycode.CtrlRight, Key.RightCtrl },
        { Android.Views.Keycode.AltLeft, Key.LeftAlt },
        { Android.Views.Keycode.AltRight, Key.RightAlt },
      };
      #endregion

      this.PersonalPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

      this.Handler = new Android.OS.Handler(Android.OS.Looper.MainLooper);

      InvApplication.Directory.Installation = AndroidActivity.ApplicationContext.PackageName;
      InvApplication.SetPlatform(new AndroidPlatform(this));
      InvApplication.Device.Target = DeviceTarget.Android;
      InvApplication.Device.Name = Environment.MachineName;
      InvApplication.Device.Manufacturer = CapitalizeFirstLetter(Android.OS.Build.Manufacturer);
      InvApplication.Device.Model = CapitalizeFirstLetter(Android.OS.Build.Model);
      if (InvApplication.Device.Model.StartsWith(InvApplication.Device.Manufacturer, StringComparison.OrdinalIgnoreCase))
        InvApplication.Device.Model = CapitalizeFirstLetter(InvApplication.Device.Model.Substring(InvApplication.Device.Manufacturer.Length).Trim());

      InvApplication.Device.System = "Android " + Android.OS.Build.VERSION.Release + " (" + Android.OS.Build.VERSION.Sdk + ")";
      InvApplication.Device.Keyboard = IsHardwareKeyboardAvailable();
      InvApplication.Device.Mouse = false; // TODO: is this a thing?  you can use mouse in the emulator.
      InvApplication.Device.Touch = true;
      InvApplication.Device.ProportionalFontName = "sans-serif";
      InvApplication.Device.MonospacedFontName = "monospace";

      InvApplication.Process.Id = Android.OS.Process.MyPid();

      InvApplication.Window.NativePanelType = typeof(Android.Views.View);

      this.Choreographer = Android.Views.Choreographer.Instance;
      this.FrameCallback = new AndroidFrameCallback(AndroidActivity, Process);

      this.AndroidSurfaceLayoutParams = new Android.Widget.FrameLayout.LayoutParams(Android.Widget.FrameLayout.LayoutParams.MatchParent, Android.Widget.FrameLayout.LayoutParams.MatchParent);

      this.FontFamilyArray = new Inv.EnumArray<FontWeight, string>()
      {
        { FontWeight.Thin, "-thin" },
        { FontWeight.Light, "-light" },
        { FontWeight.Regular, "" },
        { FontWeight.Medium, "-medium" },
        { FontWeight.Bold, "" }, // regular + bold style.
        { FontWeight.Heavy, "-black" },
      };

      this.AndroidNormalFontArray = new Inv.EnumArray<FontWeight, Android.Graphics.Typeface>();
      AndroidNormalFontArray.Fill(W => TranslateGraphicsTypeface("sans-serif", W, false));

      this.AndroidItalicFontArray = new Inv.EnumArray<FontWeight, Android.Graphics.Typeface>();
      AndroidItalicFontArray.Fill(W => TranslateGraphicsTypeface("sans-serif", W, true));

      this.InputManager = (Android.Views.InputMethods.InputMethodManager)AndroidActivity.GetSystemService(Android.Content.Context.InputMethodService);

      Resize();
    }

    public Inv.Application InvApplication { get; private set; }

    public void Start()
    {
      try
      {
        InvApplication.StartInvoke();

        AndroidActivity.Title = InvApplication.Title;
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception);

        // NOTE: can't let the application start if the StartEvent failed (it will hang on a black screen) so have the application crash instead.
        throw Exception;
        // TODO: instead show a native Android error screen like we have in Uwa?
      }

      Process();
    }
    public void Stop()
    {
      Guard(() =>
      {
        InvApplication.StopInvoke();
      });
    }
    public void Resume()
    {
      Immersive();

      if (IsProcessPaused)
      {
        this.IsProcessPaused = false;

        Guard(() =>
        {
          if (InvApplication.Location.IsRequired)
          {
            this.AndroidLocationManager = AndroidActivity.GetSystemService(Android.Content.Context.LocationService) as Android.Locations.LocationManager;

            var LocationCriteria = new Android.Locations.Criteria()
            {
              Accuracy = Android.Locations.Accuracy.Fine,
              PowerRequirement = Android.Locations.Power.NoRequirement
            };

            AndroidLocationProvider = AndroidLocationManager?.GetBestProvider(LocationCriteria, true);

            if (AndroidLocationProvider != null)
            {
              // 2000L = the minimum time between updates (in seconds), 
              // 1.0F = the minimum distance the user needs to move to generate an update (in meters),

              AndroidLocationManager.RequestLocationUpdates(AndroidLocationProvider, 2000L, 1.0F, AndroidActivity);
            }
          }
        });

        Guard(() =>
        {
          InvApplication.ResumeInvoke();
        });

        if (IsProcessSuspended)
        {
          // if the last callback wasn't actually posted, lets start now.
          IsProcessSuspended = false;
          Process();
        }
      }
    }
    public void Pause()
    {
      if (!IsProcessPaused)
      {
        this.IsProcessPaused = true;

        Guard(() =>
        {
          InvApplication.SuspendInvoke();
        });

        Guard(() =>
        {
          if (AndroidLocationManager != null)
          {
            AndroidLocationManager.RemoveUpdates(AndroidActivity);
            AndroidLocationManager = null;
          }
        });
      }
    }

    internal AndroidActivity AndroidActivity { get; private set; }
    internal string AndroidLocationProvider { get; set; }
    internal Android.Util.DisplayMetrics DisplayMetrics { get; private set; }
    internal Android.Media.SoundPool SoundPool { get; private set; }
    internal int EdgeSwipeSize { get; private set; }
    internal bool IsInputBlocked
    {
      get { return AndroidActivity.IsTransitioning || (InvApplication?.Window.InputPrevented ?? false); }
    }

    internal Inv.Key? TranslateKey(Android.Views.Keycode AndroidKey)
    {
      if (KeyDictionary.TryGetValue(AndroidKey, out Inv.Key Result))
        return Result;
      else
        return null;
    }
    internal int PlaySound(Inv.Sound InvSound, float Volume, float Rate, float Pan, bool Loop)
    {
      if (InvSound == null)
        return 0;

      var AndroidSoundNode = GetSound(InvSound);

      // NOTE: android will inconsistently play a sound with a volume greater than 1.0F.
      //       this requirement is asserted in the Inv API, but this is code is to ensure sound is played in RELEASE, despite a mistake with the volume.
      if (Volume > 1.0F)
        Volume = 1.0F;
      else if (Volume < 0.0F)
        Volume = 0.0F;

      // NOTE: Android docs say that 0.5 to 2.0 is the support range for playback rate.
      if (Rate > 2.0F)
        Rate = 2.0F;
      else if (Rate < 0.5F)
        Rate = 0.5F;

      // translate pan to left/right volumes.
      var LeftVolume = Volume;
      var RightVolume = Volume;
      if (Pan < 0)
        RightVolume *= (1 + Pan);
      else if (Pan > 0)
        LeftVolume *= (1 - Pan);

      // NOTE: this is required because the first time a sound is loaded, it doesn't work straight away.
      int StreamID;
      var Retry = 0;
      do
      {
        StreamID = SoundPool.Play(AndroidSoundNode.SoundId, LeftVolume, RightVolume, 1, Loop ? -1 : 0, Rate);
        if (StreamID == 0)
          System.Threading.Thread.Sleep(10);
      }
      while (StreamID == 0 && Retry++ < 100);

      if (StreamID == 0)
        Debug.WriteLine("Play sound failed");

      return StreamID;
    }
    internal TimeSpan GetSoundLength(Inv.Sound InvSound)
    {
      if (InvSound == null)
        return TimeSpan.Zero;

      var AndroidSoundNode = GetSound(InvSound);

      using (var FileInputStream = new Java.IO.FileInputStream(AndroidSoundNode.FilePath))
      using (var AndroidMediaPlayer = new Android.Media.MediaPlayer())
      {
        AndroidMediaPlayer.SetDataSource(FileInputStream.FD);
        AndroidMediaPlayer.Prepare();

        return TimeSpan.FromMilliseconds(AndroidMediaPlayer.Duration);
      }
    }
    internal void Resize()
    {
      var Display = AndroidActivity.WindowManager.DefaultDisplay;

      this.DisplayMetrics = new Android.Util.DisplayMetrics();
      Display.GetRealMetrics(DisplayMetrics);

      InvApplication.Device.PixelDensity = DisplayMetrics.Density;
      // 0.75 - ldpi
      // 1.00 - mdpi
      // 1.50 - hdpi
      // 2.00 - xhdpi
      // 3.00 - xxhdpi
      // 4.00 - xxxhdpi
      this.ConvertPtToPxMultiplier = (int)(InvApplication.Device.PixelDensity * ConvertPtToPxFactor);

      // NOTE: should never happen, but lets not have downstream div by zeros.
      if (ConvertPtToPxMultiplier <= 0)
        this.ConvertPtToPxMultiplier = 1;

      this.EdgeSwipeSize = PtToPx(10);

      InvApplication.Window.Width = PxToPt(DisplayMetrics.WidthPixels);
      InvApplication.Window.Height = PxToPt(DisplayMetrics.HeightPixels);
    }
    internal void Immersive()
    {
      // NOTE: the typecast a deliberate workaround, we need to use a different enum and cast it to get the desired behaviour.
      // NOTE: needs to happen 'OnResume' so it re-applies when your return from another activity.
      var UiFlags = Android.Views.SystemUiFlags.ImmersiveSticky;

      if (!AndroidActivity.ShowNavigationBar)
        UiFlags |= Android.Views.SystemUiFlags.HideNavigation;

      if (!AndroidActivity.ShowStatusBar)
        UiFlags |= Android.Views.SystemUiFlags.Fullscreen;

      AndroidActivity.Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;
    }
    internal void Reclamation()
    {
      // dispose bitmaps.
      foreach (var Image in ImageList)
      {
        var Bitmap = Image.Node as Android.Graphics.Bitmap;

        if (Bitmap != null)
          Bitmap.Dispose();

        Image.Node = null;
      }
      ImageList.Clear();

      // unload sounds.
      /*
      foreach (var Sound in SoundList)
      {
        var SoundId = (int?)Sound.Node;

        if (SoundId != null)
          SoundPool.Unload(SoundId.Value);

        Sound.Node = null;
      }
      SoundList.Clear();
      */
    }
    internal void Guard(Action Action)
    {
      try
      {
        Action();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception.Preserve());
      }
    }
    internal T Guard<T>(Func<T> Func, T Default = default(T))
    {
      try
      {
        return Func();
      }
      catch (Exception Exception)
      {
        InvApplication.HandleExceptionInvoke(Exception.Preserve());

        return Default;
      }
    }
    internal Android.Graphics.Bitmap TranslateGraphicsBitmap(Inv.Image Image)
    {
      if (Image == null)
      {
        return null;
      }
      else
      {
        var Result = Image.Node as Android.Graphics.Bitmap;

        if (Result == null)
        {
          var ImageBuffer = Image.GetBuffer();

          Result = Android.Graphics.BitmapFactory.DecodeByteArray(ImageBuffer, 0, ImageBuffer.Length/*, new Android.Graphics.BitmapFactory.Options() { InScaled = false }*/);

          AssignImage(Image, Result);
        }

        return Result;
      }
    }
    internal Android.Graphics.Paint TranslateGraphicsFillAndStrokePaint(Colour InvColour, int StrokePixels)
    {
      if (InvColour == null)
        return null;

      FillAndStrokePaint.Color = TranslateGraphicsColor(InvColour);
      FillAndStrokePaint.StrokeWidth = StrokePixels;

      return FillAndStrokePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsFillPaint(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;

      FillPaint.Color = TranslateGraphicsColor(InvColour);

      return FillPaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsStrokePaint(Inv.Colour InvColour, int StrokePixels)
    {
      if (InvColour == null || StrokePixels <= 0)
        return null;

      StrokePaint.Color = TranslateGraphicsColor(InvColour);
      StrokePaint.StrokeWidth = StrokePixels;

      return StrokePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsImagePaint(float Opacity, Inv.Colour Tint)
    {
      if (Opacity == 1.0F && Tint == null)
        return null;

      if (Opacity == 1.0F)
        ImagePaint.Alpha = 255;
      else
        ImagePaint.Alpha = (int)(Opacity * 255);

      var PreviousFilter = Tint == null ? ImagePaint.SetColorFilter(null) : ImagePaint.SetColorFilter(TranslateGraphicsColorFilter(Tint));
      if (PreviousFilter != null)
        PreviousFilter.Dispose();

      return ImagePaint;
    }
    internal Android.Graphics.Paint TranslateGraphicsTextPaint(string FontName, int FontSizePixels, Inv.FontWeight FontWeight, Inv.Colour FontColour, bool FontItalics)
    {
      // NOTE: a null textpaint causes the downstream code to fail.
      //if (FontColour == null || FontSize == 0)
      //  return null;

      TextPaint.Color = TranslateGraphicsColor(FontColour);
      TextPaint.TextSize = FontSizePixels;
      TextPaint.SetTypeface(TranslateGraphicsTypeface(FontName, FontWeight, FontItalics));

      return TextPaint;
    }
    internal void AssignImage(Inv.Image Image, Android.Graphics.Bitmap Bitmap)
    {
      if (Image.Node == null || !ImageList.Contains(Image))
        ImageList.Add(Image);

      Image.Node = Bitmap;
    }
    internal string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    internal string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(PersonalPath, Folder.Name);
      else
        Result = PersonalPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }
    internal int PtToPx(int Points)
    {
      return Points * ConvertPtToPxMultiplier / ConvertPtToPxFactor;
    }
    internal int PxToPt(int Pixels)
    {
      return Pixels * ConvertPtToPxFactor / ConvertPtToPxMultiplier;
    }
    internal AndroidContainer GetPanel(Inv.Panel InvPanel)
    {
      return TranslatePanel(InvPanel);
    }
    internal void Post(Action Action)
    {
      Handler.Post(() => Guard(Action));
    }
    internal AndroidSurface TranslateSurface(Inv.Surface InvSurface)
    {
      if (InvSurface.Node == null)
      {
        var Result = new AndroidSurface(AndroidActivity);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidSurface)InvSurface.Node;
      }
    }
    internal AndroidNode<AndroidBoard> TranslateBoard(Inv.Board InvBoard)
    {
      var AndroidNode = AccessPanel(InvBoard, P =>
      {
        var AndroidBoard = new AndroidBoard(AndroidActivity);
        return AndroidBoard;
      });

      RenderPanel(InvBoard, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidBoard = AndroidNode.Element;

        TranslateLayout(InvBoard, AndroidContainer, AndroidBoard);
        TranslateBackground(InvBoard, AndroidBoard.Background);

        if (InvBoard.PinCollection.Render())
          AndroidBoard.ComposePins(InvBoard.PinCollection.Select(P => new AndroidPin(TranslatePanel(P.Panel), PtToPx(P.Rect.Left), PtToPx(P.Rect.Top), PtToPx(P.Rect.Right), PtToPx(P.Rect.Bottom))));
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidBrowser> TranslateBrowser(Inv.Browser InvBrowser)
    {
      var AndroidNode = AccessPanel(InvBrowser, P =>
      {
        var AndroidBrowser = new AndroidBrowser(AndroidActivity);
        AndroidBrowser.BlockQuery += (Url) =>
        {
          return Guard(() =>
          {
            var Fetch = new Inv.BrowserFetch(new Uri(Url));
            P.FetchInvoke(Fetch);
            return Fetch.IsCancelled;
          });
        };
        AndroidBrowser.ReadyEvent += (Url) =>
        {
          Guard(() =>
          {
            // NOTE: Url starting with data: means it's an inline document (which could be longer than can be stored in a Uri).
            if (!string.IsNullOrWhiteSpace(Url) && !Url.StartsWith("data:"))
              P.ReadyInvoke(new BrowserReady(new Uri(Url)));
          });
        };
        return AndroidBrowser;
      });

      RenderPanel(InvBrowser, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidBrowser = AndroidNode.Element;

        TranslateLayout(InvBrowser, AndroidContainer, AndroidBrowser);
        TranslateBackground(InvBrowser, AndroidBrowser.Background);

        var Navigate = InvBrowser.UriSingleton.Render();
        if (InvBrowser.HtmlSingleton.Render())
          Navigate = true;

        if (Navigate)
          AndroidBrowser.Navigate(InvBrowser.UriSingleton.Data, InvBrowser.HtmlSingleton.Data);
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidButton> TranslateButton(Inv.Button InvButton)
    {
      var IsFlat = InvButton.Style == ButtonStyle.Flat;

      var AndroidNode = AccessPanel(InvButton, P =>
      {
        var AndroidButton = new AndroidButton(AndroidActivity);

        void ReleaseExclusive()
        {
          this.InvExclusiveButton = null;
          this.AndroidExclusiveButton = null;

          if (IsFlat)
            TranslateBackground(AndroidButton.Hovered ? TranslateHoverColour(P.Background.Colour) : P.Background.Colour, AndroidButton.Background);

          //Debug.WriteLine("Release");
          P.ReleaseInvoke();
        }

        AndroidButton.ClickEvent += () =>
        {
          Guard(() =>
          {
            // Down -> Up -> Click.
            //Debug.WriteLine("Click");

            if (!IsInputBlocked && AndroidExclusiveButton == AndroidButton/*&& InvButton.Surface == InvApplication.Window.ActiveSurface*/)
              P.SingleTapInvoke();
          });
        };
        AndroidButton.LongClickEvent += () =>
        {
          Guard(() =>
          {
            // Down -> LongClick -> Up
            //Debug.WriteLine("LongClick");

            if (!IsInputBlocked && AndroidExclusiveButton == AndroidButton)
              P.ContextTapInvoke();
          });
        };

        AndroidButton.TouchEvent += (Action) =>
        {
          Guard(() =>
          {
            switch (Action)
            {
              case Android.Views.MotionEventActions.Down:
              case Android.Views.MotionEventActions.Pointer1Down: // first touch.
                //Debug.WriteLine("Down");
                if (AndroidExclusiveButton == null || AndroidExclusiveButton == AndroidButton)
                {
                  this.InvExclusiveButton = P;
                  this.AndroidExclusiveButton = AndroidButton;

                  if (IsFlat)
                    TranslateBackground(TranslatePressColour(P.Background.Colour), AndroidButton.Background);

                  //Debug.WriteLine("Press");
                  P.PressInvoke();
                }
                break;

              case Android.Views.MotionEventActions.Up:
              case Android.Views.MotionEventActions.Pointer1Up: // first touch.
                //Debug.WriteLine("Up");

                if (AndroidExclusiveButton == AndroidButton)
                  ReleaseExclusive();
                break;

              case Android.Views.MotionEventActions.Cancel:
                //Debug.WriteLine("Cancel");

                if (AndroidExclusiveButton == AndroidButton)
                  ReleaseExclusive();
                break;

              case Android.Views.MotionEventActions.Move:
                // Move is not used for buttons.
                break;

              case Android.Views.MotionEventActions.Pointer2Down: // second touch.
              case Android.Views.MotionEventActions.Pointer3Down: // third touch.
              case Android.Views.MotionEventActions.Pointer2Up:
              case Android.Views.MotionEventActions.Pointer3Up:
                // Ignore subsequent touches on buttons.
                break;

              default:
                //Debug.WriteLine("UNHANDLED BUTTON TOUCH ACTION: " + Action.ToString());
                break;
            }
          });
        };
        AndroidButton.HoverEvent += (Action) =>
        {
          Guard(() =>
          {
            switch (Action)
            {
              case Android.Views.MotionEventActions.HoverEnter:
                InvButton.OverInvoke();

                if (IsFlat)
                  TranslateBackground(TranslateHoverColour(P.Background.Colour), AndroidButton.Background);
                break;

              case Android.Views.MotionEventActions.HoverExit:
                InvButton.AwayInvoke();

                if (IsFlat)
                  TranslateBackground(P.Background.Colour, AndroidButton.Background);
                break;

              default:
                break;
            }
          });
        };
        return AndroidButton;
      });

      RenderPanel(InvButton, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidButton = AndroidNode.Element;

        if (IsFlat)
          AndroidContainer.Alpha = InvButton.IsEnabled ? 1.0F : 0.5F;

        AndroidButton.Enabled = InvButton.IsEnabled;
        AndroidButton.Focusable = InvButton.IsFocusable;
        TranslateLayout(InvButton, AndroidContainer, AndroidButton);
        TranslateBackground(InvButton, AndroidButton.Background);
        TranslateFocus(InvButton.Focus, AndroidButton);
        TranslateTooltip(InvButton.Tooltip, AndroidButton);

#if DEBUG
        // settooltiptext was introduced in API 26.
        if (AndroidFoundation.SdkVersion >= 26)
          TranslateHint(InvButton.Hint, AndroidButton);
#endif

        if (InvButton.ContentSingleton.Render())
          AndroidButton.SetContentElement(TranslatePanel(InvButton.Content));
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidCanvas> TranslateCanvas(Inv.Canvas InvCanvas)
    {
      var AndroidNode = AccessPanel(InvCanvas, P =>
      {
        var Result = new AndroidCanvas(AndroidActivity, this);
        Result.PressEvent += (TouchX, TouchY) => Guard(() => P.PressInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.MoveEvent += (TouchX, TouchY) => Guard(() => P.MoveInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ReleaseEvent += (TouchX, TouchY) => Guard(() => P.ReleaseInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.SingleTapEvent += (TouchX, TouchY) => Guard(() => P.SingleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.DoubleTapEvent += (TouchX, TouchY) => Guard(() => P.DoubleTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.LongPressEvent += (TouchX, TouchY) => Guard(() => P.ContextTapInvoke(new Point(PxToPt(TouchX), PxToPt(TouchY))));
        Result.ZoomEvent += (TouchX, TouchY, Delta) => Guard(() => P.ZoomInvoke(new Inv.Zoom(new Point(PxToPt(TouchX), PxToPt(TouchY)), Delta)));
        Result.GraphicsDrawAction = () => Guard(() => P.DrawInvoke(Result));
        return Result;
      });

      RenderPanel(InvCanvas, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidCanvas = AndroidNode.Element;

        TranslateLayout(InvCanvas, AndroidContainer, AndroidCanvas);
        TranslateBackground(InvCanvas, AndroidCanvas.Background);

        if (InvCanvas.Redrawing)
          AndroidCanvas.Invalidate();
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidDock> TranslateDock(Inv.Dock InvDock)
    {
      var AndroidNode = AccessPanel(InvDock, P =>
      {
        var AndroidDock = new AndroidDock(AndroidActivity);
        AndroidDock.Orientation = InvDock.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidDock;
      });

      RenderPanel(InvDock, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidDock = AndroidNode.Element;

        TranslateLayout(InvDock, AndroidContainer, AndroidDock);
        TranslateBackground(InvDock, AndroidDock.Background);

        AndroidDock.Orientation = InvDock.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;

        if (InvDock.CollectionRender())
          AndroidDock.ComposeElements(InvDock.HeaderCollection.Select(H => TranslatePanel(H)), InvDock.ClientCollection.Select(C => TranslatePanel(C)), InvDock.FooterCollection.Reverse().Select(F => TranslatePanel(F)));
      });

      return AndroidNode;
    }
    internal AndroidNode<Android.Views.View> TranslateEdit(Inv.Edit InvEdit)
    {
      var IsSearch = InvEdit.Input == EditInput.Search;

      var AndroidNode = AccessPanel(InvEdit, P =>
      {
        var RecursionSafety = false;

        if (IsSearch)
        {
          var AndroidSearch = new AndroidSearchFrame(AndroidActivity, this);

          AndroidSearch.ChangeEvent += () => Guard(() =>
          {
            if (IsInputBlocked)
            {
              if (!RecursionSafety)
              {
                RecursionSafety = true;
                AndroidSearch.Query = P.Text;
                RecursionSafety = false;
              }
            }
            else
            {
              P.ChangeText(AndroidSearch.Query);
            }
          });
          AndroidSearch.ReturnEvent += () => Guard(() => P.Return());
          AndroidSearch.GotFocusEvent += () => Guard(() => P.Focus.GotInvoke());
          AndroidSearch.LostFocusEvent += () => Guard(() => P.Focus.LostInvoke());

          return (Android.Views.View)AndroidSearch;
        }
        else
        {
          var AndroidEdit = new AndroidEdit(AndroidActivity);

          switch (P.Input)
          {
            case EditInput.Number:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber;
              break;

            case EditInput.Integer:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned;
              break;

            case EditInput.Decimal:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassNumber | Android.Text.InputTypes.NumberFlagSigned | Android.Text.InputTypes.NumberFlagDecimal;
              break;

            case EditInput.Password:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPassword;
              break;

            case EditInput.Name:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationPersonName | Android.Text.InputTypes.TextFlagCapWords;
              break;

            case EditInput.Email:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationEmailAddress;
              break;

            case EditInput.Phone:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassPhone;
              break;

            case EditInput.Search:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText;
              break;

            case EditInput.Text:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextFlagAutoCorrect | Android.Text.InputTypes.TextFlagAutoComplete;
              break;

            case EditInput.Uri:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationUri;
              break;

            case EditInput.Username:
              AndroidEdit.InputType = Android.Text.InputTypes.ClassText | Android.Text.InputTypes.TextVariationVisiblePassword | Android.Text.InputTypes.TextFlagNoSuggestions;
              break;

            default:
              throw new Exception("EditInput not handled: " + P.Input);
          }

          AndroidEdit.ChangeEvent += () => Guard(() =>
          {
            if (IsInputBlocked)
            {
              if (!RecursionSafety)
              {
                RecursionSafety = true;
                AndroidEdit.Text = P.Text;
                RecursionSafety = false;
              }
            }
            else
            {
              P.ChangeText(AndroidEdit.Text);
            }
          });
          AndroidEdit.ReturnEvent += () => Guard(() => P.Return());
          AndroidEdit.GotFocusEvent += () => Guard(() => P.Focus.GotInvoke());
          AndroidEdit.LostFocusEvent += () => Guard(() => P.Focus.LostInvoke());

          return (Android.Views.View)AndroidEdit;
        }
      });

      RenderPanel(InvEdit, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;

        if (IsSearch)
        {
          var AndroidSearch = (AndroidSearchFrame)AndroidNode.Element;
          TranslateLayout(InvEdit, AndroidContainer, AndroidSearch);

          // NOTE: defending against this exception, assuming that it belongs to the search edit:
          // System.NullReferenceException
          // Object reference not set to an instance of an object
          //   at Inv.AndroidEngine.TranslateBackground (Inv.Panel InvPanel, Inv.AndroidBackground AndroidBackground) [0x0001f] in <4926134f8cec4297bedc8d76e65c7815>:0 
          //   at Inv.AndroidEngine+<>c__DisplayClass51_0.<TranslateEdit>b__1 () [0x00069] in <4926134f8cec4297bedc8d76e65c7815>:0 
          //   at Inv.AndroidEngine.RenderPanel[T] (Inv.Panel InvPanel, Inv.AndroidNode`1[T] AndroidNode, System.Action Action) [0x00008] in <4926134f8cec4297bedc8d76e65c7815>:0 
          if (AndroidSearch.Background != null) 
            TranslateBackground(InvEdit, AndroidSearch.Background);

          TranslateFont(InvEdit.Font, (FontTypeface, FontColor, FontSize) =>
          {
            if (AndroidFoundation.SdkVersion >= 21)
              TranslateFontDecorations(AndroidSearch.GetTextView(), InvEdit.Font);

            AndroidSearch.SetFont(FontTypeface, FontColor, FontSize);
          });

          TranslateFocus(InvEdit.Focus, AndroidSearch);

          AndroidSearch.Enabled = !InvEdit.IsReadOnly;

          if (InvEdit.Text != AndroidSearch.Query)
          {
            // NOTE: this works, but doesn't seem to display when it is first run.
            AndroidSearch.Query = InvEdit.Text;
          }
        }
        else
        {
          var AndroidEdit = (AndroidEdit)AndroidNode.Element;

          TranslateLayout(InvEdit, AndroidContainer, AndroidEdit);
          TranslateBackground(InvEdit, AndroidEdit.Background);

          TranslateFont(InvEdit.Font, (FontTypeface, FontColor, FontSize) =>
          {
            if (AndroidFoundation.SdkVersion >= 21)
              TranslateFontDecorations(AndroidEdit, InvEdit.Font);

            AndroidEdit.SetTypeface(FontTypeface, TranslateGraphicsTypefaceStyle(InvEdit.Font.Weight == FontWeight.Bold, InvEdit.Font.Italics));
            AndroidEdit.SetTextColor(FontColor);
            AndroidEdit.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
          });

          TranslateFocus(InvEdit.Focus, AndroidEdit);

          if (AndroidEdit.Enabled == InvEdit.IsReadOnly)
            AndroidEdit.Enabled = !InvEdit.IsReadOnly;

          if (AndroidEdit.Text != InvEdit.Text)
            AndroidEdit.Text = InvEdit.Text;
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidFlow> TranslateFlow(Inv.Flow InvFlow)
    {
      AndroidFlowItem TranslateFlowItem(Inv.Panel InvContent)
      {
        // make sure all changes are processed before returning the panel.
        ProcessChanges();

        return new AndroidFlowItem()
        {
          Panel = InvContent,
          Container = TranslatePanel(InvContent)
        };
      }

      var AndroidNode = AccessPanel(InvFlow, P =>
      {
        var AndroidFlow = new AndroidFlow(AndroidActivity, new AndroidFlowConfiguration
        {
          SectionCountQuery = () => P.SectionCount,
          ItemCountQuery = (Section) => Guard(() => P.GetSection(Section)?.ItemCount ?? 0),
          ItemContentQuery = (Section, Item) => Guard(() => TranslateFlowItem(P.GetSection(Section)?.ItemInvoke(Item))),
          RecycleEvent = (Section, Item, Recycle) => Guard(() => P.GetSection(Section)?.RecycleInvoke(Item, Recycle.Panel)),
          HeaderContentQuery = (Section) => Guard(() => TranslateFlowItem(P.GetSection(Section)?.Header)),
          FooterContentQuery = (Section) => Guard(() => TranslateFlowItem(P.GetSection(Section)?.Footer))
        });
        AndroidFlow.RefreshEvent += () => Guard(() => P.RefreshInvoke(new FlowRefresh(P, AndroidFlow.CompleteRefresh)));
        return AndroidFlow;
      });

      RenderPanel(InvFlow, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidFlow = AndroidNode.Element;

        TranslateLayout(InvFlow, AndroidContainer, AndroidFlow);
        TranslateBackground(InvFlow, AndroidFlow.Background);

        AndroidFlow.IsRefreshable = InvFlow.IsRefreshable;

        if (InvFlow.IsRefresh)
        {
          InvFlow.IsRefresh = false;

          AndroidFlow.NewRefresh();
        }

        if (InvFlow.IsReload || InvFlow.ReloadSectionList.Count > 0 || InvFlow.ReloadItemList.Count > 0)
        {
          InvFlow.IsReload = false;
          InvFlow.ReloadSectionList.Clear(); // not supported.
          InvFlow.ReloadItemList.Clear(); // not supported.

          AndroidFlow.Reload();
        }

        if (InvFlow.ScrollSection != null || InvFlow.ScrollIndex != null)
        {
          if (InvFlow.ScrollSection != null && InvFlow.ScrollIndex != null)
          {
            var Section = InvFlow.ScrollSection.Value;
            var Item = InvFlow.ScrollIndex.Value;

            // we need to Post because the listview may not yet be created.
            Post(() => AndroidFlow.ScrollToItem(Section, Item));
          }

          InvFlow.ScrollSection = null;
          InvFlow.ScrollIndex = null;
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidFrame> TranslateFrame(Inv.Frame InvFrame)
    {
      var AndroidNode = AccessPanel(InvFrame, P =>
      {
        var Result = new AndroidFrame(AndroidActivity);
        return Result;
      });

      RenderPanel(InvFrame, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidFrame = AndroidNode.Element;

        TranslateLayout(InvFrame, AndroidContainer, AndroidFrame);
        TranslateBackground(InvFrame, AndroidFrame.Background);

        if (InvFrame.ContentSingleton.Render())
        {
          var AndroidFromContent = AndroidFrame.ContentElement;
          var AndroidToContent = TranslatePanel(InvFrame.ContentSingleton.Data);

          var InvTransition = InvFrame.ActiveTransition;

          if (InvTransition == null)
          {
            if (AndroidFromContent != AndroidToContent)
            {
              if (AndroidFromContent != null)
                AndroidFrame.SafeRemoveView(AndroidFromContent);

              if (AndroidToContent != null)
                AndroidFrame.SafeAddView(AndroidToContent);

              AndroidFrame.SetContentElement(AndroidToContent);
            }
          }
          else if (AndroidFrame.IsTransitioning)
          {
            InvFrame.ContentSingleton.Change(); // try again next cycle.
          }
          else
          {
            // NOTE: give the previous panel a chance to process before it is animated away.
            if (InvFrame.FromPanel != null)
            {
              TranslatePanel(InvFrame.FromPanel);
              InvFrame.FromPanel = null;
            }

            if (AndroidFromContent != AndroidToContent)
            {
              AndroidFrame.IsTransitioning = true;
              ExecuteTransition(InvTransition, AndroidFrame, AndroidFromContent, AndroidToContent, V => AndroidFrame.SafeAddView(V), V => AndroidFrame.SafeRemoveView(V), () => AndroidFrame.IsTransitioning = false);

              AndroidFrame.SetContentElement(AndroidToContent);
            }

            InvFrame.ActiveTransition = null;
          }
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidGraphic> TranslateGraphic(Inv.Graphic InvGraphic)
    {
      var AndroidNode = AccessPanel(InvGraphic, P =>
      {
        var AndroidGraphic = new AndroidGraphic(AndroidActivity);
        return AndroidGraphic;
      });

      RenderPanel(InvGraphic, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidGraphic = AndroidNode.Element;

        TranslateLayout(InvGraphic, AndroidContainer, AndroidGraphic);
        TranslateBackground(InvGraphic, AndroidGraphic.Background);

        if (InvGraphic.ImageSingleton.Render())
          AndroidGraphic.SetImageBitmap(TranslateGraphicsBitmap(InvGraphic.Image));
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidLabel> TranslateLabel(Inv.Label InvLabel)
    {
      var AndroidNode = AccessPanel(InvLabel, P =>
      {
        var AndroidLabel = new AndroidLabel(AndroidActivity);
        return AndroidLabel;
      });

      RenderPanel(InvLabel, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidLabel = AndroidNode.Element;

        TranslateLayout(InvLabel, AndroidContainer, AndroidLabel);
        TranslateBackground(InvLabel, AndroidLabel.Background);
        TranslateFont(InvLabel.Font, (FontTypeface, FontColor, FontSize) =>
        {
          if (AndroidFoundation.SdkVersion >= 21)
            TranslateFontDecorations(AndroidLabel, InvLabel.Font);

          AndroidLabel.SetTypeface(FontTypeface, TranslateGraphicsTypefaceStyle(InvLabel.Font.Weight == FontWeight.Bold, InvLabel.Font.Italics));
          AndroidLabel.SetTextColor(FontColor);
          AndroidLabel.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        AndroidLabel.Gravity = TranslateGravityFlag(InvLabel.Justify) | Android.Views.GravityFlags.CenterVertical;
        AndroidLabel.SetSingleLine(!InvLabel.LineWrapping);
        AndroidLabel.Text = InvLabel.Text;
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidMemo> TranslateMemo(Inv.Memo InvMemo)
    {
      void SetText(Inv.Memo P, AndroidMemo AndroidMemo)
      {
        if (P.MarkupCollection.Count == 0)
        {
          AndroidMemo.Text = P.Text;
        }
        else
        {
          var SpannableString = new Android.Text.SpannableString(P.Text);

          foreach (var Markup in P.MarkupCollection)
          {
            if (Markup.RangeList.Count > 0)
            {
              var MarkupColour = Markup.Font.Colour != null ? TranslateGraphicsColor(Markup.Font.Colour) : (Android.Graphics.Color?)null;
              var MarkupName = Markup.Font.Name;
              var MarkupSize = Markup.Font.Size;
              var MarkupWeight = Markup.Font.Weight;
              var MarkupItalics = Markup.Font.Italics;

              // NOTE: the Android*Span instances can't be reused by each range (that's why that are created inside the loop instead of outside).
              foreach (var Range in Markup.RangeList)
              {
                if (MarkupName != null || MarkupWeight != null || MarkupSize != null || MarkupItalics)
                  SpannableString.SetSpan(new CustomTypefaceSpan(MarkupName ?? P.Font.Name, TranslateGraphicsTypeface(MarkupName, MarkupWeight ?? P.Font.Weight ?? Inv.FontWeight.Regular, MarkupItalics), PtToPx(MarkupSize ?? InvMemo.Font.Size ?? 14)), Range.Index, Range.Index + Range.Count, 0);

                if (MarkupColour != null)
                  SpannableString.SetSpan(new Android.Text.Style.ForegroundColorSpan(MarkupColour.Value), Range.Index, Range.Index + Range.Count, 0);
              }
            }
          }

          AndroidMemo.SetSpannableString(SpannableString);
        }
      }

      var AndroidNode = AccessPanel(InvMemo, P =>
      {
        var RecursionSafety = false;

        var AndroidMemo = new AndroidMemo(AndroidActivity);
        AndroidMemo.ChangeEvent += () => Guard(() =>
        {
          if (IsInputBlocked)
          {
            if (!RecursionSafety)
            {
              RecursionSafety = true;
              SetText(P, AndroidMemo);
              RecursionSafety = false;
            }
          }
          else
          {
            P.ChangeText(AndroidMemo.Text);
          }
        });
        AndroidMemo.GotFocusEvent += () => Guard(() => P.Focus.GotInvoke());
        AndroidMemo.LostFocusEvent += () => Guard(() => P.Focus.LostInvoke());
        return AndroidMemo;
      });

      RenderPanel(InvMemo, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidMemo = AndroidNode.Element;

        TranslateLayout(InvMemo, AndroidContainer, AndroidMemo);
        TranslateBackground(InvMemo, AndroidMemo.Background);

        TranslateFont(InvMemo.Font, (FontTypeface, FontColor, FontSize) =>
        {
          if (AndroidFoundation.SdkVersion >= 21)
            TranslateFontDecorations(AndroidMemo.GetTextView(), InvMemo.Font);

          AndroidMemo.SetTypeface(FontTypeface, TranslateGraphicsTypefaceStyle(InvMemo.Font.Weight == FontWeight.Bold, InvMemo.Font.Italics));
          AndroidMemo.SetTextColor(FontColor);
          AndroidMemo.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);
        });

        TranslateFocus(InvMemo.Focus, AndroidMemo);

        AndroidMemo.IsReadOnly = InvMemo.IsReadOnly;

        var RenderText = AndroidMemo.Text != InvMemo.Text;
        if (InvMemo.MarkupCollection.Render())
          RenderText = true;

        if (RenderText)
          SetText(InvMemo, AndroidMemo);
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidNative> TranslateNative(Inv.Native InvNative)
    {
      var AndroidNode = AccessPanel(InvNative, P =>
      {
        var Result = new AndroidNative(AndroidActivity);
        return Result;
      });

      RenderPanel(InvNative, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidNative = AndroidNode.Element;

        TranslateLayout(InvNative, AndroidContainer, AndroidNative);
        TranslateBackground(InvNative, AndroidNative.Background);

        if (InvNative.ContentSingleton.Render())
          AndroidNative.SetContentElement((Android.Views.View)InvNative.ContentSingleton.Data);
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidOverlay> TranslateOverlay(Inv.Overlay InvOverlay)
    {
      var AndroidNode = AccessPanel(InvOverlay, P =>
      {
        var AndroidOverlay = new AndroidOverlay(AndroidActivity);
        return AndroidOverlay;
      });

      RenderPanel(InvOverlay, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidOverlay = AndroidNode.Element;

        TranslateLayout(InvOverlay, AndroidContainer, AndroidOverlay);
        TranslateBackground(InvOverlay, AndroidOverlay.Background);

        if (InvOverlay.PanelCollection.Render())
          AndroidOverlay.ComposeElements(InvOverlay.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidScroll> TranslateScroll(Inv.Scroll InvScroll)
    {
      var AndroidNode = AccessPanel(InvScroll, P =>
      {
        var Result = new AndroidScroll(AndroidActivity);
        Result.SetOrientation(InvScroll.IsHorizontal);
        return Result;
      });

      RenderPanel(InvScroll, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidScroll = AndroidNode.Element;

        TranslateLayout(InvScroll, AndroidContainer, AndroidScroll);
        TranslateBackground(InvScroll, AndroidScroll.Background);

        AndroidScroll.SetOrientation(InvScroll.IsHorizontal);

        if (InvScroll.ContentSingleton.Render())
        {
          var AndroidContent = TranslatePanel(InvScroll.Content);
          AndroidScroll.SetContentElement(AndroidContent);
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidStack> TranslateStack(Inv.Stack InvStack)
    {
      var AndroidNode = AccessPanel(InvStack, P =>
      {
        var AndroidStack = new AndroidStack(AndroidActivity);
        AndroidStack.Orientation = InvStack.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidStack;
      });

      RenderPanel(InvStack, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidStack = AndroidNode.Element;

        TranslateLayout(InvStack, AndroidContainer, AndroidStack);
        TranslateBackground(InvStack, AndroidStack.Background);

        AndroidStack.Orientation = InvStack.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;

        if (InvStack.PanelCollection.Render())
          AndroidStack.ComposeElements(InvStack.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidSwitch> TranslateSwitch(Inv.Switch InvSwitch)
    {
      void RefreshDrawable(AndroidSwitch AndroidSwitch)
      {
        var AndroidColor = TranslateGraphicsColor(InvSwitch.IsOn ? InvSwitch.PrimaryColour : InvSwitch.SecondaryColour);

        AndroidSwitch.TrackDrawable.SetColorFilter(AndroidColor, Android.Graphics.PorterDuff.Mode.Multiply);
        AndroidSwitch.ThumbDrawable.SetColorFilter(AndroidColor, Android.Graphics.PorterDuff.Mode.Multiply);
      }

      var AndroidNode = AccessPanel(InvSwitch, P =>
      {
        var AndroidSwitch = new AndroidSwitch(AndroidActivity);
        //AndroidSwitch.TextOn = "";
        //AndroidSwitch.TextOff = "";
        AndroidSwitch.ShowText = false;
        AndroidSwitch.Focusable = true;
        AndroidSwitch.SetMinimumWidth(PtToPx(71)); // match the iOS default size.
        AndroidSwitch.SetMinimumHeight(PtToPx(51));
        AndroidSwitch.ChangeEvent += () =>
        {
          Guard(() =>
          {
            P.ChangeChecked(AndroidSwitch.Checked);

            RefreshDrawable(AndroidSwitch);
          });
        };
        return AndroidSwitch;
      });

      RenderPanel(InvSwitch, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidSwitch = AndroidNode.Element;

        TranslateLayout(InvSwitch, AndroidContainer, AndroidSwitch);
        TranslateBackground(InvSwitch, AndroidSwitch.Background);

        //AndroidContainer.Alpha = InvSwitch.IsEnabled ? 1.0F : 0.5F;
        AndroidSwitch.Checked = InvSwitch.IsOn;
        AndroidSwitch.Enabled = InvSwitch.IsEnabled;

        RefreshDrawable(AndroidSwitch);
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidTable> TranslateTable(Inv.Table InvTable)
    {
      var AndroidNode = AccessPanel(InvTable, P =>
      {
        var AndroidTable = new AndroidTable(AndroidActivity);
        return AndroidTable;
      });

      RenderPanel(InvTable, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidTable = AndroidNode.Element;

        TranslateLayout(InvTable, AndroidContainer, AndroidTable);
        TranslateBackground(InvTable, AndroidTable.Background);

        if (InvTable.CollectionRender())
        {
          var AndroidRowList = new Inv.DistinctList<AndroidTableRow>(InvTable.RowCollection.Count);

          foreach (var InvRow in InvTable.RowCollection)
          {
            var AndroidRow = new AndroidTableRow(AndroidTable, TranslatePanel(InvRow.Content), InvRow.LengthType == TableAxisLength.Star, InvRow.LengthType == TableAxisLength.Fixed ? PtToPx(InvRow.LengthValue) : InvRow.LengthType == TableAxisLength.Star ? InvRow.LengthValue : (int?)null);
            AndroidRowList.Add(AndroidRow);
          }

          var AndroidColumnList = new Inv.DistinctList<AndroidTableColumn>(InvTable.ColumnCollection.Count);

          foreach (var InvColumn in InvTable.ColumnCollection)
          {
            var AndroidColumn = new AndroidTableColumn(AndroidTable, TranslatePanel(InvColumn.Content), InvColumn.LengthType == TableAxisLength.Star, InvColumn.LengthType == TableAxisLength.Fixed ? PtToPx(InvColumn.LengthValue) : InvColumn.LengthType == TableAxisLength.Star ? InvColumn.LengthValue : (int?)null);
            AndroidColumnList.Add(AndroidColumn);
          }

          var AndroidCellList = new Inv.DistinctList<AndroidTableCell>(InvTable.CellCollection.Count);

          foreach (var InvCell in InvTable.CellCollection)
          {
            var AndroidCell = new AndroidTableCell(AndroidColumnList[InvCell.Column.Index], AndroidRowList[InvCell.Row.Index], TranslatePanel(InvCell.Content));
            AndroidCellList.Add(AndroidCell);
          }

          AndroidTable.ComposeElements(AndroidRowList, AndroidColumnList, AndroidCellList);
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidVideo> TranslateVideo(Inv.Video InvVideo)
    {
      var AndroidNode = AccessPanel(InvVideo, P =>
      {
        var AndroidVideo = new AndroidVideo(AndroidActivity);
        return AndroidVideo;
      });

      RenderPanel(InvVideo, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidVideo = AndroidNode.Element;

        TranslateLayout(InvVideo, AndroidContainer, AndroidVideo);
        TranslateBackground(InvVideo, AndroidVideo.Background);

        if (InvVideo.SourceSingleton.Render())
        {
          var InvSource = InvVideo.SourceSingleton.Data;

          if (InvSource?.Asset != null)
            AndroidVideo.LoadAsset(AndroidActivity.Assets.OpenFd(InvSource.Asset.Name));
          else if (InvSource?.File != null)
            AndroidVideo.LoadUri(new Uri(SelectFilePath(InvSource.File)));
          else if (InvSource?.Uri != null)
            AndroidVideo.LoadUri(InvSource.Uri);
          else
            AndroidVideo.Unload();
        }

        if (InvVideo.StateSingleton.Render())
        {
          switch (InvVideo.StateSingleton.Data)
          {
            case VideoState.Stop:
              AndroidVideo.Stop();
              break;

            case VideoState.Pause:
              AndroidVideo.Pause();
              break;

            case VideoState.Play:
              AndroidVideo.Start();
              break;

            case VideoState.Restart:
              AndroidVideo.Stop();
              AndroidVideo.Start();
              break;

            default:
              throw new Exception("VideoState not handled: " + InvVideo.StateSingleton.Data);
          }
        }
      });

      return AndroidNode;
    }
    internal AndroidNode<AndroidWrap> TranslateWrap(Inv.Wrap InvWrap)
    {
      var AndroidNode = AccessPanel(InvWrap, P =>
      {
        var AndroidWrap = new AndroidWrap(AndroidActivity);
        AndroidWrap.Orientation = InvWrap.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;
        return AndroidWrap;
      });

      RenderPanel(InvWrap, AndroidNode, () =>
      {
        var AndroidContainer = AndroidNode.Container;
        var AndroidWrap = AndroidNode.Element;

        TranslateLayout(InvWrap, AndroidContainer, AndroidWrap);
        TranslateBackground(InvWrap, AndroidWrap.Background);

        AndroidWrap.Orientation = InvWrap.IsHorizontal ? Android.Widget.Orientation.Horizontal : Android.Widget.Orientation.Vertical;

        if (InvWrap.PanelCollection.Render())
          AndroidWrap.ComposeElements(InvWrap.PanelCollection.Select(E => TranslatePanel(E)));
      });

      return AndroidNode;
    }
    internal void StartAnimation(Inv.Animation InvAnimation)
    {
      var AndroidAnimationGroupList = new Inv.DistinctList<AnimationGroup>();
      InvAnimation.Node = AndroidAnimationGroupList;

      var InvLastTarget = InvAnimation.GetTargets().LastOrDefault();

      foreach (var InvTarget in InvAnimation.GetTargets())
      {
        var InvPanel = InvTarget.Panel;
        var AndroidContainer = TranslatePanel(InvPanel);
        var AndroidPanel = AndroidContainer.ContentElement ?? AndroidContainer;

        // TODO: switch to ValueAnimator?

        var AnimationCount = 0;

        void AnimationComplete()
        {
          if (--AnimationCount == 0)
            InvAnimation.Complete();
        }

        var AndroidAnimationGroup = new AnimationGroup(InvPanel, AndroidPanel);
        AndroidAnimationGroupList.Add(AndroidAnimationGroup);

        foreach (var InvTransform in InvTarget.GetTransforms())
          TranslateAnimationTransform(InvAnimation, InvPanel, AndroidPanel, InvTransform, AndroidAnimationGroup, AnimationComplete);

        AnimationCount = AndroidAnimationGroup.AnimationSet.Animations.Count;

        AndroidPanel.StartAnimation(AndroidAnimationGroup.AnimationSet);
      }
    }
    internal void StopAnimation(Inv.Animation InvAnimation)
    {
      if (InvAnimation.Node != null)
      {
        var AndroidAnimationGroupList = (Inv.DistinctList<AnimationGroup>)InvAnimation.Node;
        InvAnimation.Node = null;

        foreach (var AndroidAnimationGroup in AndroidAnimationGroupList)
          AndroidAnimationGroup.Stop();
      }
    }
    internal static Android.Graphics.Color TranslateGraphicsColor(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return Android.Graphics.Color.Transparent;
      }
      else if (InvColour.Node == null)
      {
        var Result = new Android.Graphics.Color(InvColour.RawValue);

        InvColour.Node = new AndroidColourNode()
        {
          Color = Result,
          //Filter = null
        };

        return Result;
      }
      else
      {
        return ((AndroidColourNode)InvColour.Node).Color;
      }
    }

    private void Process()
    {
      if (InvApplication.IsExit)
      {
        AndroidActivity.Finish();
        InvApplication.Dismiss();
        // NOTE: this won't actually end the application, that's the job of the Android OS (apparently).

        // TODO: there doesn't appear to be a way to actually terminate the process in code.
        //Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        //System.Environment.Exit(0);
      }
      else
      {
        Guard(() =>
        {
          var InvWindow = InvApplication.Window;

          // per-frame process event for the window.
          InvWindow.ProcessInvoke();

          // timer management.
          if (InvWindow.ActiveTimerSet.Count > 0)
          {
            foreach (var InvTimer in InvWindow.ActiveTimerSet)
            {
              var AndroidTimer = AccessTimer(InvTimer, S =>
              {
                var Result = new System.Timers.Timer();
                Result.Elapsed += (Sender, Event) =>
                {
                  if (InvTimer.IsEnabled)
                  {
                    Post(() =>
                    {
                      if (InvTimer.IsEnabled)
                        InvTimer.IntervalInvoke();
                    });
                  }
                };
                return Result;
              });

              if (InvTimer.IsRestarting)
              {
                InvTimer.IsRestarting = false;
                AndroidTimer.Stop();
              }

              if (AndroidTimer.Interval != InvTimer.IntervalTime.TotalMilliseconds)
                AndroidTimer.Interval = InvTimer.IntervalTime.TotalMilliseconds;

              if (InvTimer.IsEnabled && !AndroidTimer.Enabled)
                AndroidTimer.Start();
              else if (!InvTimer.IsEnabled && AndroidTimer.Enabled)
                AndroidTimer.Stop();
            }

            InvWindow.ActiveTimerSet.RemoveWhere(T => !T.IsEnabled);
          }

          // compose the active surface.
          var InvSurface = InvWindow.ActiveSurface;

          if (InvSurface != null)
          {
            var AndroidSurface = TranslateSurface(InvSurface);

            if (!AndroidActivity.HasContentView(AndroidSurface) && InvWindow.ActiveTransition != null)
              InvSurface.ArrangeInvoke();

            ProcessTransition(AndroidSurface);

            InvSurface.ComposeInvoke();

            UpdateSurface(InvSurface, AndroidSurface);
          }

          // process all panel changes.
          ProcessChanges();

          // render window background.
          if (InvWindow.Render())
          {
            // TODO: this collides with the IsTransitioning input prevention code.
            //if (InvWindow.InputPrevented == InvWindow)
            //{
            //  this.AndroidInputPrevented = !InvWindow.InputPrevented;
            //
            //  if (AndroidInputPrevented)
            //    AndroidActivity.Window.SetFlags(Android.Views.WindowManagerFlags.NotTouchable, Android.Views.WindowManagerFlags.NotTouchable);
            //  else
            //    AndroidActivity.Window.ClearFlags(Android.Views.WindowManagerFlags.NotTouchable);
            //}

            if (InvWindow.Background.Render())
              AndroidActivity.Window.DecorView.SetBackgroundColor(TranslateGraphicsColor(InvWindow.Background.Colour ?? Inv.Colour.Black));
          }

          // process focus.
          if (InvWindow.Focus != null)
          {
            var AndroidFocus = InvWindow.Focus.Node as AndroidNode;
            InvWindow.Focus = null;
            if (AndroidFocus != null)
            {
              var AndroidElement = AndroidFocus.Element;
              var FocusContract = AndroidElement as AndroidFocusContract;
              if (FocusContract != null)
                AndroidElement = FocusContract.View;

              if (AndroidElement.RequestFocus())
                AndroidElement.PostDelayed(() => InputManager.ShowSoftInput(AndroidElement, Android.Views.InputMethods.ShowFlags.Implicit), 0);
            }
          }

          // NOTE: this is required because 'Touch Up/Cancel' events are not fired when a button is removed from the visual tree, in the single/context tap events.
          if (AndroidExclusiveButton != null)
          {
            var AutomaticReleaseButton = !AndroidExclusiveButton.IsAttachedToWindow;

            if (!AutomaticReleaseButton)
            {
              var ParentContainer = AndroidExclusiveButton.Parent as AndroidContainer;
              if (ParentContainer != null)
                AutomaticReleaseButton = !ParentContainer.ContentVisible;
            }

            if (AutomaticReleaseButton)
            {
              // previous exclusive button must have been removed from the visual tree, better reset the background colour.
              if (InvExclusiveButton != null)
              {
                TranslateBackground(InvExclusiveButton.Background.Colour, AndroidExclusiveButton.Background);

                InvExclusiveButton.ReleaseInvoke();
              }

              this.InvExclusiveButton = null;
              this.AndroidExclusiveButton = null;
            }
          }

          InvWindow.DisplayRate.Calculate();
        });

        if (IsProcessPaused)
          IsProcessSuspended = true;
        else
          Choreographer.PostFrameCallback(FrameCallback);
      }
    }

    private void UpdateSurface(Surface InvSurface, AndroidSurface AndroidSurface)
    {
      if (InvSurface.Render())
      {
        AndroidSurface.SetBackgroundColor(TranslateGraphicsColor(InvSurface.Background.Colour));

        var AndroidPanel = TranslatePanel(InvSurface.Content);
        AndroidSurface.SetContentElement(AndroidPanel);
      }
    }
    private void ProcessChanges()
    {
      InvApplication.Window.ProcessChanges(P => TranslatePanel(P));
    }
    private void ProcessTransition(AndroidSurface AndroidSurface)
    {
      var InvWindow = InvApplication.Window;
      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
      }
      else if (!AndroidActivity.IsTransitioning)
      {
        var AndroidContent = AndroidActivity.GetContentView();

        if (AndroidSurface == AndroidContent)
        {
          //Debug.WriteLine("Transition to self");
        }
        else
        {
          // give the previous surface a chance to process before it is animated away.
          if (InvWindow.FromSurface != null)
          {
            if (TranslateSurface(InvWindow.FromSurface) == AndroidContent)
              UpdateSurface(InvWindow.FromSurface, (AndroidSurface)AndroidContent);

            InvWindow.FromSurface = null;
          }

          ExecuteTransition(InvTransition, null, AndroidContent as AndroidSurface, AndroidSurface, V => AndroidActivity.AddContentView(V, AndroidSurfaceLayoutParams), V => AndroidActivity.RemoveContentView(V), () => { });
        }

        InvWindow.ActiveTransition = null;
      }
    }
    private void ExecuteTransition(Inv.Transition InvTransition, Android.Views.View ParentView, Android.Views.View AndroidFromView, Android.Views.View AndroidToView, Action<Android.Views.View> AddViewAction, Action<Android.Views.View> RemoveViewAction, Action CompletedAction)
    {
      var AnimateDuration = (long)InvTransition.Duration.TotalMilliseconds;
      var AnimateOut = AndroidFromView != null;
      var AnimateIn = AndroidToView != null;

      var IsCompleted = false;
      void CompleteAnimation()
      {
        if (!IsCompleted)
        {
          IsCompleted = true;

          CompletedAction();
        }
      }

      switch (InvTransition.Type)
      {
        case TransitionType.None:
          if (AnimateOut)
            RemoveViewAction(AndroidFromView);

          if (AnimateIn)
            AddViewAction(AndroidToView);

          CompletedAction();
          break;

        case TransitionType.Fade:
          if (!AnimateOut && !AnimateIn)
          {
            CompletedAction();
          }
          else
          {
            var AndroidFadeDuration = AnimateOut && AnimateIn ? AnimateDuration / 2 : AnimateDuration;
            var AndroidFadeOutAnimation = AnimateOut ? new AndroidAlphaAnimation(1, 0) : null;
            var AndroidFadeInAnimation = AnimateIn ? new AndroidAlphaAnimation(0, 1) : null;

            if (AnimateOut)
            {
              var IsEnded = false;
              void EndAnimation()
              {
                if (!IsEnded)
                {
                  IsEnded = true;

                  RemoveViewAction(AndroidFromView);
                  AndroidFromView.Alpha = 1.0F;

                  if (AnimateIn)
                  {
                    AndroidToView.Visibility = Android.Views.ViewStates.Visible;
                    AndroidToView.StartAnimation(AndroidFadeInAnimation);
                  }
                  else
                  {
                    AndroidActivity.IsTransitioning = false;

                    CompleteAnimation();
                  }
                }
              }

              AndroidFadeOutAnimation.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
              AndroidFadeOutAnimation.Duration = AndroidFadeDuration;
              AndroidFadeOutAnimation.EndEvent += () => EndAnimation();

              Post(() =>
              {
                AndroidActivity.IsTransitioning = true;

                AndroidFromView.StartAnimation(AndroidFadeOutAnimation);

                Handler.PostDelayed(EndAnimation, AndroidFadeDuration);
              });
            }

            if (AnimateIn)
            {
              var IsEnded = false;
              void EndAnimation()
              {
                if (!IsEnded)
                {
                  IsEnded = true;

                  AndroidActivity.IsTransitioning = false;

                  CompleteAnimation();
                }
              }

              AddViewAction(AndroidToView);
              AndroidToView.Visibility = Android.Views.ViewStates.Invisible;

              AndroidFadeInAnimation.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
              AndroidFadeInAnimation.Duration = AnimateDuration;
              AndroidFadeInAnimation.EndEvent += () => EndAnimation();

              if (!AnimateOut)
              {
                Post(() =>
                {
                  AndroidActivity.IsTransitioning = true;

                  AndroidToView.Visibility = Android.Views.ViewStates.Visible;
                  AndroidToView.StartAnimation(AndroidFadeInAnimation);

                  Handler.PostDelayed(EndAnimation, AndroidFadeDuration);
                });
              }
            }
          }
          break;

        case TransitionType.CarouselNext:
        case TransitionType.CarouselPrevious:
        case TransitionType.CarouselAscend:
        case TransitionType.CarouselDescend:
          if (!AnimateOut && !AnimateIn)
          {
            CompletedAction();
          }
          else
          {
            var CarouselForward = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselAscend;
            var CarouselHorizontal = InvTransition.Type == TransitionType.CarouselNext || InvTransition.Type == TransitionType.CarouselPrevious;

            var AndroidActualWidth = CarouselHorizontal ? ParentView != null ? ParentView.Width : DisplayMetrics.WidthPixels : 0.0F;
            var AndroidActualHeight = !CarouselHorizontal ? ParentView != null ? ParentView.Height : DisplayMetrics.HeightPixels : 0.0F;

            if (AnimateOut)
            {
              var IsEnded = false;

              void EndAnimation()
              {
                if (!IsEnded)
                {
                  IsEnded = true;

                  RemoveViewAction(AndroidFromView);

                  AndroidActivity.IsTransitioning = false;

                  CompleteAnimation();
                }
              }

              var NextSlideOut = new AndroidTranslateAnimation(0.0F, CarouselForward ? -AndroidActualWidth : AndroidActualWidth, 0.0F, CarouselForward ? -AndroidActualHeight : AndroidActualHeight);
              NextSlideOut.Duration = AnimateDuration;
              NextSlideOut.EndEvent += () => EndAnimation();

              Post(() =>
              {
                AndroidActivity.IsTransitioning = true;

                AndroidFromView.StartAnimation(NextSlideOut);

                Handler.PostDelayed(EndAnimation, AnimateDuration);
              });
            }

            if (AnimateIn)
            {
              var IsEnded = false;

              void EndAnimation()
              {
                if (!IsEnded)
                {
                  IsEnded = true;

                  AndroidActivity.IsTransitioning = false;

                  CompleteAnimation();
                }
              }

              AddViewAction(AndroidToView);
              AndroidToView.Visibility = Android.Views.ViewStates.Invisible;

              var NextSlideIn = new AndroidTranslateAnimation(CarouselForward ? AndroidActualWidth : -AndroidActualWidth, 0.00F, CarouselForward ? AndroidActualHeight : -AndroidActualHeight, 0.0F);
              NextSlideIn.Duration = AnimateDuration;
              NextSlideIn.EndEvent += () => EndAnimation();

              Post(() =>
              {
                AndroidActivity.IsTransitioning = true;

                AndroidToView.Visibility = Android.Views.ViewStates.Visible;
                AndroidToView.StartAnimation(NextSlideIn);

                Handler.PostDelayed(EndAnimation, AnimateDuration);
              });
            }
          }
          break;


        default:
          throw new Exception("TransitionType not handled: " + InvTransition.Type);
      }
    }
    private void TranslateAnimationTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, Android.Views.View AndroidPanel, AnimationTransform InvTransform, AnimationGroup AnimationGroup, Action CompleteAction)
    {
      switch (InvTransform.Type)
      {
        case AnimationType.Fade:
          TranslateAnimationFadeTransform(InvAnimation, InvPanel, AndroidPanel, (AnimationFadeTransform)InvTransform, AnimationGroup, CompleteAction);
          break;

        case AnimationType.Rotate:
          TranslateAnimationRotateTransform(InvAnimation, InvPanel, AndroidPanel, (AnimationRotateTransform)InvTransform, AnimationGroup, CompleteAction);
          break;

        case AnimationType.Scale:
          TranslateAnimationScaleTransform(InvAnimation, InvPanel, AndroidPanel, (AnimationScaleTransform)InvTransform, AnimationGroup, CompleteAction);
          break;

        case AnimationType.Translate:
          TranslateAnimationTranslateTransform(InvAnimation, InvPanel, AndroidPanel, (AnimationTranslateTransform)InvTransform, AnimationGroup, CompleteAction);
          break;

        default:
          throw new Exception("Animation Transform not handled: " + InvTransform.Type);
      }
    }
    private void TranslateAnimationFadeTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, Android.Views.View AndroidPanel, AnimationFadeTransform InvTransform, AnimationGroup AnimationGroup, Action CompleteAction)
    {
      var InvControl = InvPanel.Control;
      var FromAlpha = float.IsNaN(InvTransform.FromOpacity) ? AndroidPanel.Alpha : InvTransform.FromOpacity;
      var ToAlpha = InvTransform.ToOpacity;

      var Result = new AndroidAlphaAnimation(FromAlpha, ToAlpha);
      Result.FillAfter = true;

      if (InvTransform.Offset != null && InvTransform.Offset.Value > TimeSpan.Zero)
        Result.StartOffset = (long)InvTransform.Offset.Value.TotalMilliseconds;

      var IsEnded = false;

      void EndAnimation()
      {
        if (!IsEnded)
        {
          IsEnded = true;

          Guard(() =>
          {
            if (!AnimationGroup.IsCancelled)
            {
              AndroidPanel.Alpha = ToAlpha;
              InvControl.Opacity.BypassSet(ToAlpha);

              CompleteAction();
            }
          });
        }
      }

      Result.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
      Result.Duration = (long)InvTransform.Duration.TotalMilliseconds;
      Result.StartEvent += () =>
      {
        Guard(() =>
        {
          if (!AnimationGroup.IsCancelled)
          {
            AndroidPanel.Alpha = FromAlpha;
            InvControl.Opacity.BypassSet(FromAlpha);

            // Android doesn't guarantee the end event will fire, so this is extra insurance.
            Handler.PostDelayed(EndAnimation, Result.StartOffset + Result.Duration);
          }
        });
      };
      Result.EndEvent += () => EndAnimation();

      AnimationGroup.AnimationSet.AddAnimation(Result);
      AnimationGroup.StopEvent += () =>
      {
        // set the alpha at the point the animation was stopped.
        var Transformation = new Android.Views.Animations.Transformation();
        Result.GetTransformation(AndroidPanel.DrawingTime, Transformation);

        AndroidPanel.Alpha = Transformation.Alpha;
        InvControl.Opacity.BypassSet(AndroidPanel.Alpha);
      };
    }
    private void TranslateAnimationRotateTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, Android.Views.View AndroidPanel, AnimationRotateTransform InvTransform, AnimationGroup AnimationGroup, Action CompleteAction)
    {
      var FromAngle = float.IsNaN(InvTransform.FromAngle) ? AndroidPanel.Rotation : InvTransform.FromAngle;
      var ToAngle = InvTransform.ToAngle;

      var Result = new AndroidRotateAnimation(FromAngle, ToAngle);
      Result.FillAfter = true;

      if (InvTransform.Offset != null)
        Result.StartOffset = (long)InvTransform.Offset.Value.TotalMilliseconds;

      var IsEnded = false;

      void EndAnimation()
      {
        if (!IsEnded)
        {
          IsEnded = true;

          Guard(() =>
          {
            if (!AnimationGroup.IsCancelled)
            {
              AndroidPanel.Rotation = ToAngle;

              CompleteAction();
            }
          });
        }
      }

      Result.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
      Result.Duration = (long)InvTransform.Duration.TotalMilliseconds;
      Result.StartEvent += () =>
      {
        Guard(() =>
        {
          if (!AnimationGroup.IsCancelled)
          {
            AndroidPanel.Rotation = FromAngle;

            // Android doesn't guarantee the end event will fire, so this is extra insurance.
            Handler.PostDelayed(EndAnimation, Result.StartOffset + Result.Duration);
          }
        });
      };
      Result.EndEvent += () => EndAnimation();

      AnimationGroup.AnimationSet.AddAnimation(Result);
      AnimationGroup.StopEvent += () =>
      {
        // set the rotation at the point the animation was stopped.
        var Transformation = new Android.Views.Animations.Transformation();
        Result.GetTransformation(AndroidPanel.DrawingTime, Transformation);
        var ValueArray = new float[9];
        Transformation.Matrix.GetValues(ValueArray);

        // calculate the degree of rotation
        AndroidPanel.Rotation = -(float)Math.Round(Math.Atan2(ValueArray[Android.Graphics.Matrix.MskewX], ValueArray[Android.Graphics.Matrix.MscaleX]) * (180 / Math.PI));
      };
    }
    private void TranslateAnimationScaleTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, Android.Views.View AndroidPanel, AnimationScaleTransform InvTransform, AnimationGroup AnimationGroup, Action CompleteAction)
    {
      var FromX = float.IsNaN(InvTransform.FromWidth) ? AndroidPanel.ScaleX : InvTransform.FromWidth;
      var ToX = float.IsNaN(InvTransform.ToWidth) ? AndroidPanel.ScaleX : InvTransform.ToWidth;
      var FromY = float.IsNaN(InvTransform.FromHeight) ? AndroidPanel.ScaleY : InvTransform.FromHeight;
      var ToY = float.IsNaN(InvTransform.ToHeight) ? AndroidPanel.ScaleY : InvTransform.ToHeight;

      var Result = new AndroidScaleAnimation(FromX, ToX, FromY, ToY);
      Result.FillAfter = true;

      if (InvTransform.Offset != null)
        Result.StartOffset = (long)InvTransform.Offset.Value.TotalMilliseconds;

      var IsEnded = false;

      void EndAnimation()
      {
        if (!IsEnded)
        {
          IsEnded = true;

          Guard(() =>
          {
            if (!AnimationGroup.IsCancelled)
            {
              // the following is required to make the scalex/scaley 'stick' after the animation actually ends.
              // otherwise the panel will be scaled twice, briefly, which causes a visual stutter.
              Result.Cancel();
              Handler.PostDelayed(() =>
              {
                AndroidPanel.ScaleX = ToX;
                AndroidPanel.ScaleY = ToY;
              }, 0);

              CompleteAction();
            }
          });
        }
      }

      Result.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
      Result.Duration = (long)InvTransform.Duration.TotalMilliseconds;
      Result.StartEvent += () =>
      {
        Guard(() =>
        {
          if (!AnimationGroup.IsCancelled)
          {
            AndroidPanel.ScaleX = FromX;
            AndroidPanel.ScaleY = FromY;

            // Android doesn't guarantee the end event will fire, so this is extra insurance.
            Handler.PostDelayed(EndAnimation, Result.StartOffset + Result.Duration);
          }
        });
      };
      Result.EndEvent += () => EndAnimation();

      AnimationGroup.AnimationSet.AddAnimation(Result);
      AnimationGroup.StopEvent += () =>
      {
        // set the scale at the point the animation was stopped.
        var Transformation = new Android.Views.Animations.Transformation();
        Result.GetTransformation(AndroidPanel.DrawingTime, Transformation);
        var ValueArray = new float[9];
        Transformation.Matrix.GetValues(ValueArray);
        AndroidPanel.ScaleX = ValueArray[Android.Graphics.Matrix.MscaleX];
        AndroidPanel.ScaleY = ValueArray[Android.Graphics.Matrix.MscaleY];
      };
    }
    private void TranslateAnimationTranslateTransform(Inv.Animation InvAnimation, Inv.Panel InvPanel, Android.Views.View AndroidPanel, AnimationTranslateTransform InvTransform, AnimationGroup AnimationGroup, Action CompleteAction)
    {
      var FromX = InvTransform.FromX == null ? AndroidPanel.TranslationX : PtToPx(InvTransform.FromX.Value);
      var ToX = InvTransform.ToX == null ? AndroidPanel.TranslationX : PtToPx(InvTransform.ToX.Value);
      var FromY = InvTransform.FromY == null ? AndroidPanel.TranslationY : PtToPx(InvTransform.FromY.Value);
      var ToY = InvTransform.ToY == null ? AndroidPanel.TranslationY : PtToPx(InvTransform.ToY.Value);

      var Result = new AndroidTranslateAnimation(FromX, ToX, FromY, ToY);
      Result.FillAfter = true;

      if (InvTransform.Offset != null)
        Result.StartOffset = (long)InvTransform.Offset.Value.TotalMilliseconds;

      var IsEnded = false;

      void EndAnimation()
      {
        if (!IsEnded)
        {
          IsEnded = true;

          Guard(() =>
          {
            if (!AnimationGroup.IsCancelled)
            {
              Result.Cancel();
              Handler.PostDelayed(() =>
              {
                AndroidPanel.TranslationX = ToX;
                AndroidPanel.TranslationY = ToY;
              }, 0);

              CompleteAction();
            }
          });
        }
      }

      Result.Interpolator = new Android.Views.Animations.AccelerateDecelerateInterpolator();
      Result.Duration = (long)InvTransform.Duration.TotalMilliseconds;
      Result.StartEvent += () =>
      {
        Guard(() =>
        {
          if (!AnimationGroup.IsCancelled)
          {
            AndroidPanel.TranslationX = FromX;
            AndroidPanel.TranslationY = FromY;

            // Android doesn't guarantee the end event will fire, so this is extra insurance.
            Handler.PostDelayed(EndAnimation, Result.StartOffset + Result.Duration);
          }
        });
      };
      Result.EndEvent += () => EndAnimation();

      AnimationGroup.AnimationSet.AddAnimation(Result);
      AnimationGroup.StopEvent += () =>
      {
        // set the scale at the point the animation was stopped.
        var Transformation = new Android.Views.Animations.Transformation();
        Result.GetTransformation(AndroidPanel.DrawingTime, Transformation);
        var ValueArray = new float[9];
        Transformation.Matrix.GetValues(ValueArray);
        AndroidPanel.TranslationX = ValueArray[Android.Graphics.Matrix.MtransX];
        AndroidPanel.TranslationY = ValueArray[Android.Graphics.Matrix.MtransY];
      };
    }
    private AndroidContainer TranslatePanel(Inv.Panel InvPanel)
    {
      var InvControl = InvPanel?.Control;

      if (InvControl == null)
        return null;
      else
        return RouteArray[InvControl.ControlType](InvControl).Container;
    }
    private System.Timers.Timer AccessTimer(Inv.WindowTimer InvTimer, Func<Inv.WindowTimer, System.Timers.Timer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (System.Timers.Timer)InvTimer.Node;
      }
    }
    private AndroidNode<TView> AccessPanel<TControl, TView>(TControl InvControl, Func<TControl, TView> BuildFunction)
      where TControl : Inv.Control
      where TView : Android.Views.View
    {
      if (InvControl.Node == null)
      {
        var Result = new AndroidNode<TView>();

        Result.Container = new AndroidContainer(AndroidActivity, InvControl);
        Result.Element = BuildFunction(InvControl);

        Result.Container.SetContentElement(Result.Element);

        InvControl.Node = Result;

        return Result;
      }
      else
      {
        return (AndroidNode<TView>)InvControl.Node;
      }
    }
    private void RenderPanel<T>(Inv.Control InvControl, AndroidNode<T> AndroidNode, Action Action)
      where T : Android.Views.View
    {
      if (InvControl.Render())
      {
        // trap any exceptions as it will cascade to disrupt other panels that may not have any problems.
        try
        {
          Action();
        }
        catch (Exception Exception)
        {
          InvApplication.HandleExceptionInvoke(Exception);
        }
      }
    }
    private void TranslateBackground(Inv.Control InvControl, AndroidBackground AndroidBackground)
    {
      if (InvControl.Background.Render())
        AndroidBackground.SetColor(TranslateGraphicsColor(InvControl.Background.Colour));

      if (InvControl.Corner.Render())
        AndroidBackground.SetCornerRadius(PtToPx(InvControl.Corner.TopLeft), PtToPx(InvControl.Corner.TopRight), PtToPx(InvControl.Corner.BottomRight), PtToPx(InvControl.Corner.BottomLeft));

      if (InvControl.Border.Render())
        AndroidBackground.SetBorderStroke(TranslateGraphicsColor(InvControl.Border.Colour), PtToPx(InvControl.Border.Left), PtToPx(InvControl.Border.Top), PtToPx(InvControl.Border.Right), PtToPx(InvControl.Border.Bottom));
    }
    private void TranslateBackground(Inv.Colour InvColour, AndroidBackground AndroidBackground)
    {
      AndroidBackground.SetColor(TranslateGraphicsColor(InvColour));
    }
    private void TranslateHint(string Hint, Android.Views.View AndroidElement)
    {
      if (AndroidElement.TooltipText != Hint)
        AndroidElement.TooltipText = Hint;
    }
    private void TranslateLayout(Inv.Control InvControl, AndroidContainer AndroidContainer, Android.Views.View AndroidElement)
    {
      if (InvControl.Opacity.Render())
        AndroidElement.Alpha = InvControl.Opacity.Get();

      var InvVisibility = InvControl.Visibility;
      if (InvVisibility.Render())
        AndroidContainer.SetContentVisiblity(InvVisibility.Get());

      var InvPadding = InvControl.Padding;
      var InvBorder = InvControl.Border;
      if (InvPadding.Render() || InvBorder.IsChanged)
        AndroidElement.SetPadding(PtToPx(InvPadding.Left + InvBorder.Left), PtToPx(InvPadding.Top + InvBorder.Top), PtToPx(InvPadding.Right + InvBorder.Right), PtToPx(InvPadding.Bottom + InvBorder.Bottom));

      TranslateAlignment(InvControl.Alignment, AndroidContainer);
      TranslateMargin(InvControl.Margin, AndroidContainer);
      TranslateSize(InvControl.Size, AndroidContainer);

      // ClipToOutline, OutlineProvider, Elevation introduced in API 21 (TranslateElevation needs to be a separate method to avoid breaking the runtime).
      if (AndroidFoundation.SdkVersion >= 21)
        TranslateElevation(AndroidContainer, AndroidElement, InvControl);

      // only connect SizedChanged for tracked panels.
      AndroidContainer.SizeChanged -= PanelSizedChanged;
      if (InvControl.HasAdjust)
        AndroidContainer.SizeChanged += PanelSizedChanged;

    }
    private void TranslateElevation(AndroidContainer AndroidContainer, Android.Views.View AndroidPanel, Inv.Control InvControl)
    {
      var InvElevation = InvControl.Elevation;
      if (InvElevation.Render())
      {
        // TODO: custom shadows so we can select the elevation colour?

        var Depth = InvElevation.Get();

        if (Depth > 0)
        {
          //Debug.Assert(AndroidPanel.EnumerateParents().All(G => !G.ClipChildren), "All ViewGroup parents of an elevated panel must have .SetClipChildren(false) called on them to ensure elevation shadows won't be clipped.");

          AndroidContainer.SetClipToPadding(false);

          AndroidPanel.OutlineProvider = new AndroidPanelOutlineProvider(this, InvControl);
          AndroidPanel.Elevation = PtToPx(Depth);
        }
        else
        {
          AndroidContainer.SetClipToPadding(true);

          AndroidPanel.OutlineProvider = null;
          AndroidPanel.Elevation = 0;
        }
      }
    }
    private void TranslateMargin(Inv.Edge InvMargin, AndroidContainer AndroidContainer)
    {
      if (InvMargin.Render())
      {
        if (InvMargin.IsSet)
          AndroidContainer.SetContentMargins(PtToPx(InvMargin.Left), PtToPx(InvMargin.Top), PtToPx(InvMargin.Right), PtToPx(InvMargin.Bottom));
        else
          AndroidContainer.SetContentMargins(0, 0, 0, 0);
      }
    }
    private void TranslateAlignment(Inv.Alignment InvAlignment, AndroidContainer AndroidContainer)
    {
      if (InvAlignment.Render())
      {
        switch (InvAlignment.Get())
        {
          case Placement.BottomStretch:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.BottomLeft:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.BottomCenter:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.BottomRight:
            AndroidContainer.SetContentVerticalBottom();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.TopStretch:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.TopLeft:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.TopCenter:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.TopRight:
            AndroidContainer.SetContentVerticalTop();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.CenterStretch:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.CenterLeft:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.Center:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.CenterRight:
            AndroidContainer.SetContentVerticalCenter();
            AndroidContainer.SetContentHorizontalRight();
            break;

          case Placement.Stretch:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalStretch();
            break;

          case Placement.StretchLeft:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalLeft();
            break;

          case Placement.StretchCenter:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalCenter();
            break;

          case Placement.StretchRight:
            AndroidContainer.SetContentVerticalStretch();
            AndroidContainer.SetContentHorizontalRight();
            break;

          default:
            throw new Exception("Placement not handled: " + InvAlignment.Get());
        }
      }
    }
    private void TranslateSize(Inv.Size InvSize, AndroidContainer AndroidContainer)
    {
      if (InvSize.Render())
      {
        if (InvSize.Width != null)
          AndroidContainer.SetContentWidth(PtToPx(InvSize.Width.Value));
        else
          AndroidContainer.ClearContentWidth();

        if (InvSize.Height != null)
          AndroidContainer.SetContentHeight(PtToPx(InvSize.Height.Value));
        else
          AndroidContainer.ClearContentHeight();

        if (InvSize.MinimumWidth != null)
          AndroidContainer.SetContentMinimumWidth(PtToPx(InvSize.MinimumWidth.Value));
        else
          AndroidContainer.ClearContentMinimumWidth();

        if (InvSize.MinimumHeight != null)
          AndroidContainer.SetContentMinimumHeight(PtToPx(InvSize.MinimumHeight.Value));
        else
          AndroidContainer.ClearContentMinimumHeight();

        if (InvSize.MaximumWidth != null)
          AndroidContainer.SetContentMaximumWidth(PtToPx(InvSize.MaximumWidth.Value));
        else
          AndroidContainer.ClearContentMaximumWidth();

        if (InvSize.MaximumHeight != null)
          AndroidContainer.SetContentMaximumHeight(PtToPx(InvSize.MaximumHeight.Value));
        else
          AndroidContainer.ClearContentMaximumHeight();
      }
    }
    private void TranslateFont(Inv.Font InvFont, Action<Android.Graphics.Typeface, Android.Graphics.Color, int> Action)
    {
      if (InvFont.Render())
      {
        var FontTypeface = TranslateGraphicsTypeface(InvFont.Name, InvFont.Weight ?? Inv.FontWeight.Regular, InvFont.Italics);
        var FontColor = TranslateGraphicsColor(InvFont.Colour ?? Inv.Colour.Black);
        var FontSize = InvFont.Size ?? 14; // TODO: what is the Inv default font size?

        Action(FontTypeface, FontColor, FontSize);
      }
    }
    private void TranslateFocus(Inv.Focus InvFocus, Android.Views.View AndroidPanel)
    {
      if (InvFocus.Render())
      {
        // TODO: (dis)connect the GotFocus / LostFocus events from the control?
      }
    }
    private void TranslateTooltip(Inv.Tooltip InvTooltip, Android.Views.View AndroidPanel)
    {
      if (InvTooltip.Render())
      {
        // TODO: does Android support tooltips?
      }
    }
    private void TranslateFontDecorations(Android.Widget.TextView AndroidView, Inv.Font Font)
    {
      // NOTE: FontFeatureSettings was introduced in API Level 21.
      AndroidView.FontFeatureSettings = Font.SmallCaps ? "smcp" : null;

      if (Font.Strikethrough)
        AndroidView.PaintFlags |= Android.Graphics.PaintFlags.StrikeThruText;
      else
        AndroidView.PaintFlags &= ~Android.Graphics.PaintFlags.StrikeThruText;

      if (Font.Underline)
        AndroidView.PaintFlags |= Android.Graphics.PaintFlags.UnderlineText;
      else
        AndroidView.PaintFlags &= ~Android.Graphics.PaintFlags.UnderlineText;
    }

    private Android.Views.GravityFlags TranslateGravityFlag(Inv.Justify Justify)
    {
      var Justification = Justify.Get();

      return Justification == Justification.Center ? Android.Views.GravityFlags.CenterHorizontal : Justification == Justification.Left ? Android.Views.GravityFlags.Left : Android.Views.GravityFlags.Right;
    }
    private Android.Graphics.Typeface TranslateGraphicsTypeface(string FontName, Inv.FontWeight InvFontWeight, bool Italic)
    {
      if (FontName != null)
        return Android.Graphics.Typeface.Create(
          FontName + FontFamilyArray[InvFontWeight], TranslateGraphicsTypefaceStyle(InvFontWeight == FontWeight.Bold, Italic));
      else
        return Italic ? AndroidItalicFontArray[InvFontWeight] : AndroidNormalFontArray[InvFontWeight];
    }
    private Android.Graphics.TypefaceStyle TranslateGraphicsTypefaceStyle(bool Bold, bool Italic)
    {
      return Bold ? (Italic ? Android.Graphics.TypefaceStyle.BoldItalic : Android.Graphics.TypefaceStyle.Bold) : (Italic ? Android.Graphics.TypefaceStyle.Italic : Android.Graphics.TypefaceStyle.Normal);
    }
    private Android.Graphics.ColorFilter TranslateGraphicsColorFilter(Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else if (InvColour.Node == null)
      {
        var Color = new Android.Graphics.Color(InvColour.RawValue);
        var Result = new Android.Graphics.PorterDuffColorFilter(Color, Android.Graphics.PorterDuff.Mode.SrcAtop);

        InvColour.Node = new AndroidColourNode()
        {
          Color = Color,
          //Filter = Result
        };

        return Result;
      }
      else
      {
        var Node = (AndroidColourNode)InvColour.Node;

        return new Android.Graphics.PorterDuffColorFilter(Node.Color, Android.Graphics.PorterDuff.Mode.SrcAtop);

        //if (Node.Filter == null)
        //  Node.Filter = new Android.Graphics.PorterDuffColorFilter(Node.Color, Android.Graphics.PorterDuff.Mode.SrcAtop);
        //
        //return Node.Filter;
      }
    }
    private Inv.Colour TranslateHoverColour(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Lighten(0.25F);
    }
    private Inv.Colour TranslatePressColour(Inv.Colour InvColour)
    {
      if (InvColour == null)
        return null;
      else
        return InvColour.Darken(0.25F);
    }
    private AndroidSoundNode GetSound(Inv.Sound InvSound)
    {
      var AndroidSoundNode = InvSound.Node as AndroidSoundNode?;

      if (AndroidSoundNode == null)
      {
        var Buffer = InvSound.GetBuffer();

        var FilePath = System.IO.Path.Combine(PersonalPath, SoundList.Count + InvSound.GetFormat());

        using (var FileStream = new System.IO.FileStream(FilePath, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 65536))
          FileStream.Write(Buffer, 0, Buffer.Length);

        AndroidSoundNode = new AndroidSoundNode()
        {
          SoundId = SoundPool.Load(FilePath, 1),
          FilePath = FilePath
        };

        // just in case it is already in the list with a non-android sound id.
        if (InvSound.Node == null || !SoundList.Contains(InvSound))
          SoundList.Add(InvSound);

        InvSound.Node = AndroidSoundNode;
      }

      return AndroidSoundNode.Value;
    }

    private int CalculateInSampleSize(Android.Graphics.BitmapFactory.Options options, int reqWidth, int reqHeight)
    {
      // Raw height and width of image
      var height = options.OutHeight;
      var width = options.OutWidth;
      var inSampleSize = 1D;

      if (height > reqHeight || width > reqWidth)
      {
        var halfHeight = (int)(height / 2);
        var halfWidth = (int)(width / 2);

        // Calculate a inSampleSize that is a power of 2 - the decoder will use a value that is a power of two anyway.
        while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth)
          inSampleSize *= 2;
      }

      return (int)inSampleSize;
    }
    private string CapitalizeFirstLetter(string Source)
    {
      if (Source == null || Source.Length == 0)
        return "";

      var FirstLetter = Source[0];
      if (char.IsUpper(FirstLetter))
        return Source;
      else
        return char.ToUpper(FirstLetter) + Source.Substring(1);
    }
    private bool IsHardwareKeyboardAvailable()
    {
      return AndroidActivity.Resources.Configuration.Keyboard != Android.Content.Res.KeyboardType.Nokeys;
    }
    private void PanelSizedChanged(Inv.Control TagControl)
    {
      if (TagControl != null)
        TagControl.AdjustInvoke();
    }

    private Android.OS.Handler Handler;
    private const int ConvertPtToPxFactor = 1000;
    private int ConvertPtToPxMultiplier;
    private bool IsProcessPaused;
    private readonly string PersonalPath;
    private readonly AndroidFrameCallback FrameCallback;
    private Android.Graphics.Paint FillPaint;
    private Android.Graphics.Paint FillAndStrokePaint;
    private Android.Graphics.Paint StrokePaint;
    private Android.Graphics.Paint ImagePaint;
    private Android.Graphics.Paint TextPaint;
    private Android.Views.Choreographer Choreographer;
    private Android.Locations.LocationManager AndroidLocationManager;
    private readonly Android.Views.ViewGroup.LayoutParams AndroidSurfaceLayoutParams;
    private Android.Views.InputMethods.InputMethodManager InputManager;
    private readonly Inv.EnumArray<Inv.ControlType, Func<Inv.Control, AndroidNode>> RouteArray;
    private Dictionary<Android.Views.Keycode, Inv.Key> KeyDictionary;
    private Inv.EnumArray<Inv.FontWeight, Android.Graphics.Typeface> AndroidNormalFontArray;
    private Inv.EnumArray<Inv.FontWeight, Android.Graphics.Typeface> AndroidItalicFontArray;
    private Inv.DistinctList<Inv.Image> ImageList;
    private Inv.DistinctList<Inv.Sound> SoundList;
    private bool IsProcessSuspended;
    private Inv.Button InvExclusiveButton;
    private AndroidButton AndroidExclusiveButton;
    private readonly EnumArray<FontWeight, string> FontFamilyArray;
    //private bool AndroidInputPrevented;

    private struct AndroidSoundNode
    {
      public int SoundId;
      public string FilePath;
    }

    private struct AndroidColourNode
    {
      public Android.Graphics.Color Color;
      //public Android.Graphics.ColorFilter Filter;
    }

    private sealed class AndroidPanelOutlineProvider : Android.Views.ViewOutlineProvider
    {
      public AndroidPanelOutlineProvider(AndroidEngine Engine, Inv.Control Control)
      {
        this.Engine = Engine;
        this.Control = Control;
      }

      public override void GetOutline(Android.Views.View View, Android.Graphics.Outline Outline)
      {
        if (Control.Corner.IsUniform)
        {
          if (Control.Corner.TopLeft == 0)
            Outline.SetRect(0, 0, View.Width, View.Height);
          else
            Outline.SetRoundRect(0, 0, View.Width, View.Height, Engine.PtToPx(Control.Corner.TopLeft));
        }
        else
        {
          PathSize.Width = View.Width;
          PathSize.Height = View.Height;

          PathCornerRadius.TopLeft = Engine.PtToPx(Control.Corner.TopLeft);
          PathCornerRadius.TopRight = Engine.PtToPx(Control.Corner.TopRight);
          PathCornerRadius.BottomRight = Engine.PtToPx(Control.Corner.BottomRight);
          PathCornerRadius.BottomLeft = Engine.PtToPx(Control.Corner.BottomLeft);

          OutlinePath.Reset();
          PathBuilder.AddRoundedRect(PathSize, PathCornerRadius, PathBorderThickness);

          Outline.SetConvexPath(PathBuilder.Render());
        }
      }

      private Inv.Control Control;
      private AndroidEngine Engine;
    }

    private static readonly Android.Graphics.Path OutlinePath = new Android.Graphics.Path();
    private static readonly AndroidBezierPathBuilder PathBuilder = new AndroidBezierPathBuilder(OutlinePath);
    private static Inv.BezierSize PathSize = new Inv.BezierSize();
    private static Inv.BezierCornerRadius PathCornerRadius = new Inv.BezierCornerRadius();
    private static Inv.BezierBorderThickness PathBorderThickness = new Inv.BezierBorderThickness();

    private sealed class AnimationGroup
    {
      public AnimationGroup(Inv.Panel InvPanel, Android.Views.View AndroidPanel)
      {
        this.InvPanel = InvPanel;
        this.AndroidPanel = AndroidPanel;

        // NOTE: AnimationSet does not support animations on different views.
        this.AnimationSet = new Android.Views.Animations.AnimationSet(false);
      }

      public bool IsCancelled { get; private set; }
      public readonly Inv.Panel InvPanel;
      public readonly Android.Views.View AndroidPanel;
      public readonly Android.Views.Animations.AnimationSet AnimationSet;
      public event Action StopEvent;

      internal void Stop()
      {
        this.IsCancelled = true;

        if (StopEvent != null)
          StopEvent();

        AnimationSet.Cancel();
        AndroidPanel.ClearAnimation();
      }
    }
  }

  internal abstract class AndroidNode
  {
    public AndroidContainer Container { get; set; }
    public Android.Views.View Element { get; set; }
  }

  internal sealed class AndroidNode<T> : AndroidNode
    where T : Android.Views.View
  {
    public new T Element
    {
      get { return (T)base.Element; }
      set { base.Element = value; }
    }
  }

  internal sealed class CustomTypefaceSpan : Android.Text.Style.TypefaceSpan
  {
    public CustomTypefaceSpan(string family, Android.Graphics.Typeface type, int FontSize)
      : base(family)
    {
      this.Typeface = type;
      this.FontSize = FontSize;
    }

    public override void UpdateDrawState(Android.Text.TextPaint ds)
    {
      ApplyCustomTypeFace(ds);
    }
    public override void UpdateMeasureState(Android.Text.TextPaint paint)
    {
      ApplyCustomTypeFace(paint);
    }

    private void ApplyCustomTypeFace(Android.Graphics.Paint paint)
    {
      Android.Graphics.TypefaceStyle oldStyle;
      var old = paint.Typeface;
      if (old == null)
      {
        oldStyle = 0;
      }
      else
      {
        oldStyle = old.Style;
      }

      var fake = oldStyle & ~Typeface.Style;
      if ((fake & Android.Graphics.TypefaceStyle.Bold) != 0)
      {
        paint.FakeBoldText = true;
      }

      if ((fake & Android.Graphics.TypefaceStyle.Italic) != 0)
      {
        paint.TextSkewX = -0.25f;
      }

      paint.TextSize = FontSize;

      paint.SetTypeface(Typeface);
    }

    private Android.Graphics.Typeface Typeface;
    private readonly int FontSize;
  }
}