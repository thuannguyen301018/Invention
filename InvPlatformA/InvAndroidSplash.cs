/*! 11 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Views.Animations;
using Android.Graphics;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{

  /// <summary>
  /// Subclass to include a simple splash screen of an image in your app.
  /// </summary>
  public abstract class AndroidSplashActivity : Android.App.Activity
  {
    public Inv.Colour BackgroundColour
    {
      set => Window.DecorView.SetBackgroundColor(AndroidEngine.TranslateGraphicsColor(value ?? Inv.Colour.Black));
    }
    /// <summary>
    /// Set this to an Id in your resources.
    /// </summary>
    public int ImageResourceId { get; protected set; }
    /// <summary>
    /// Set this to the type of the main app activity.
    /// </summary>
    public Type LaunchActivity { get; protected set; }
    /// <summary>
    /// The HockeyApp ID that uniquely identifies this app for crash reporting.
    /// </summary>
    public string HockeyAppID { get; protected set; }

    /// <summary>
    /// Implement this method to set <see cref="ImageResourceId"/> for the splash image, <see cref="LaunchActivity"/> for the main activity and optionally <see cref="HockeyAppID"/> for HockeyApp integration.
    /// </summary>
    protected abstract void Install();

    /// <summary>
    /// Called when the activity is starting.
    /// </summary>
    /// <param name="bundle"></param>
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      if (Inv.Assert.IsEnabled)
      {
        var AndroidActivityAttribute = GetType().CustomAttributes.Find(A => A.AttributeType == typeof(Android.App.ActivityAttribute));
        var MainLauncherParameter = AndroidActivityAttribute.NamedArguments.Find(A => A.MemberName == "MainLauncher");

        Inv.Assert.Check(MainLauncherParameter != null && MainLauncherParameter.TypedValue.Value != null, "[Activity] attribute must include MainLauncher parameter.");
      }

      RequestWindowFeature(WindowFeatures.NoTitle);
      base.OnCreate(bundle);

      Install();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(ImageResourceId != 0, "ImageResourceId must be specified in the Install method.");
        Inv.Assert.Check(LaunchActivity != null, "LaunchActivity must be specified in the Install method.");
      }

      var RelativeLayout = new RelativeLayout(this);
      SetContentView(RelativeLayout);
      RelativeLayout.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent)
      {
        Gravity = GravityFlags.Fill
      };
      var ImageView = new ImageView(this);
      RelativeLayout.AddView(ImageView);
      ImageView.SetImageResource(ImageResourceId);
      var ImageLP = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
      ImageView.LayoutParameters = ImageLP;
      ImageLP.AddRule(LayoutRules.CenterInParent);

      var FadeIn = new AlphaAnimation(0, 1);
      FadeIn.Interpolator = new DecelerateInterpolator();
      FadeIn.Duration = 1000;

      var FadeOut = new AlphaAnimation(1, 0);
      FadeOut.Interpolator = new AccelerateInterpolator();
      FadeOut.StartOffset = 1000;
      FadeOut.Duration = 1000;

      var Animation = new AnimationSet(false);
      Animation.AddAnimation(FadeIn);
      Animation.AddAnimation(FadeOut);
      Animation.AnimationEnd += (Sender, Event) => RelativeLayout.Visibility = ViewStates.Invisible;

      RelativeLayout.Animation = Animation;

      Task.Run(() =>
      {
        // give time to the splash animation.
        System.Threading.Thread.Sleep(1000);

        // check for registered HockeyApp crashes.
        if (!HockeyApp.Android.CrashManager.DidCrashInLastSession())
          Launch();
      });
    }
    /// <summary>
    /// Called after your activity is ready to start interacting with the user.
    /// </summary>
    protected override void OnResume()
    {
      base.OnResume();

      if (HockeyAppID != null)
        HockeyApp.Android.CrashManager.Register(this, HockeyAppID, new AndroidCrashManagerListener(this));
    }

    private void Launch()
    {
      StartActivity(LaunchActivity);
      Finish(); // terminate the splash activity.
    }

    private sealed class AndroidCrashManagerListener : HockeyApp.Android.CrashManagerListener
    {
      public AndroidCrashManagerListener(AndroidSplashActivity Splash)
      {
        this.Splash = Splash;
      }

      public override void OnUserDeniedCrashes()
      {
        base.OnUserDeniedCrashes();

        Splash.Launch();
      }
      public override void OnCrashesNotSent()
      {
        base.OnCrashesNotSent();

        Splash.Launch();
      }
      public override void OnCrashesSent()
      {
        base.OnCrashesSent();

        Splash.Launch();
      }

      private AndroidSplashActivity Splash;
    }
  }
}