/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Opengl;
using Android.Graphics;
using Java.Lang;
using Java.Nio;
using Javax.Microedition.Khronos.Opengles;
using Inv.Support;

#if DEBUG
namespace Inv
{
  public abstract class AndroidDebugOpenGLActivity : Activity
  {
    protected override void OnCreate(Bundle bundle)
    {
      base.OnCreate(bundle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);
      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);

      if (hasGLES20())
      {
        mGLView = new GLSurfaceView(this);
        mGLView.SetEGLContextClientVersion(2);
        mGLView.PreserveEGLContextOnPause = true;
        mGLView.SetRenderer(new GLES20Renderer());
      }
      else
      {
        // Time to get a new phone, OpenGL ES 2.0 not supported.
      }

      SetContentView(mGLView);
    }

    private bool hasGLES20()
    {
      var am = (ActivityManager)GetSystemService(Context.ActivityService);
      var info = am.DeviceConfigurationInfo;
      return info.ReqGlEsVersion >= 0x20000;
    }

    protected override void OnResume()
    {
      base.OnResume();

      // The activity must call the GL surface view's onResume() on activity onResume().
      if (mGLView != null)
      {
        mGLView.OnResume();
      }
    }

    protected override void OnPause()
    {
      base.OnPause();

      // The activity must call the GL surface view's onPause() on activity onPause().
      if (mGLView != null)
      {
        mGLView.OnPause();
      }
    }

    private GLSurfaceView mGLView;
  }

  public class GLES20Renderer : Java.Lang.Object, GLSurfaceView.IRenderer
  {
    public GLES20Renderer()
    {
      SurfaceWidth = -1;
      SurfaceHeight = -1;
    }

    public void OnSurfaceCreated(IGL10 notUsed, Javax.Microedition.Khronos.Egl.EGLConfig config)
    {
      /*
      // Some one-time OpenGL initialization can be made here probably based on features of this particular context
      GLES10.GlHint(GL10.GlPerspectiveCorrectionHint, GL10.GlFastest);

      GLES10.GlClearColor(0.5f, 0.5f, 0.5f, 1);
      GLES10.GlShadeModel(GL10.GlFlat);
      GLES10.GlDisable(GL10.GlDepthTest);
      GLES10.GlEnable(GL10.GlTexture2d);
      // By default, OpenGL enables features that improve quality but reduce performance. One might want to tweak that especially on software renderer.
      GLES10.GlDisable(GL10.GlDither);
      GLES10.GlDisable(GL10.GlLighting);
      */

      SurfaceWidth = -1;
      SurfaceHeight = -1;

      this.Sprite = new GLSprite(null);//InvTest.Resources.Images.PhoenixLogo960x540);
    }
    public void OnSurfaceChanged(IGL10 notUsed, int width, int height)
    {
      SurfaceWidth = width;
      SurfaceHeight = height;

      GLES10.GlViewport(0, 0, width, height);

      // Set our projection matrix. This doesn't have to be done each time we draw, but usually a new projection needs to be set when the viewport  is resized.
      GLES10.GlMatrixMode(GL10.GlProjection);
      GLES10.GlLoadIdentity();
      GLES10.GlOrthof(0.0f, width, 0.0f, height, 0.0f, 1.0f);

      GLES10.GlShadeModel(GL10.GlFlat);
      GLES10.GlEnable(GL10.GlBlend);
      GLES10.GlBlendFunc(GL10.GlSrcAlpha, GL10.GlOneMinusSrcAlpha);
      GLES10.GlColor4x(0x10000, 0x10000, 0x10000, 0x10000);
      GLES10.GlEnable(GL10.GlTexture2d);
    }
    public void OnDrawFrame(IGL10 notUsed)
    {
      GLES10.GlMatrixMode(GL10.GlModelview);

      //if (mUseVerts)
      //  Grid.BeginDrawing(true, false);

      //GLES20.GlClearColor(0.5f, 1.0f, 0.3f, 1f);
      //GLES20.GlClear(GLES20.GlColorBufferBit | GLES20.GlDepthBufferBit);

      DrawSprite(Sprite, 100, 100, Sprite.Width, Sprite.Height);

      //if (mUseVerts)
      //  Grid.EndDrawing();
    }

    private void DrawSprite(GLSprite Sprite, int x, int y, int width, int height)
    {
      Sprite.draw(x, SurfaceHeight - (y + height), 0, width, height);
    }

    public void setVertMode(bool useVerts, bool useHardwareBuffers)
    {
      mUseVerts = useVerts;
      mUseHardwareBuffers = useVerts ? useHardwareBuffers : false;
    }

    private int SurfaceWidth;
    private int SurfaceHeight;
    private GLSprite Sprite;
    private bool mUseVerts;
    private bool mUseHardwareBuffers;
  }

  public sealed class GLSprite
  {
    private int TextureId;
    private Grid mGrid = null;

    public int Width { get; private set; }
    public int Height { get; private set; }

    public GLSprite(Inv.Image Image)
    {
      var ImageBuffer = Image.GetBuffer();

      var BitmapOptions = new BitmapFactory.Options();
      BitmapOptions.InPreferredConfig = Bitmap.Config.Rgb565; // Set our bitmaps to 16-bit, 565 format.

      using (var ImageBitmap = Android.Graphics.BitmapFactory.DecodeByteArray(ImageBuffer, 0, ImageBuffer.Length, BitmapOptions))
      using (var TextureBitmap = GetBitmapForTexture(ImageBitmap))
      {
        this.Width = ImageBitmap.Width;
        this.Height = ImageBitmap.Height;

        var mTextureNameWorkspace = new int[1];
        GLES10.GlGenTextures(1, mTextureNameWorkspace, 0);

        this.TextureId = mTextureNameWorkspace[0];
        GLES10.GlBindTexture(GL10.GlTexture2d, TextureId);

        GLES10.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureMinFilter, GL10.GlNearest);
        GLES10.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureMagFilter, GL10.GlLinear);

        GLES10.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureWrapS, GL10.GlClampToEdge);
        GLES10.GlTexParameterf(GL10.GlTexture2d, GL10.GlTextureWrapT, GL10.GlClampToEdge);

        GLES10.GlTexEnvf(GL10.GlTextureEnv, GL10.GlTextureEnvMode, GL10.GlReplace);

        GLUtils.TexImage2D(GL10.GlTexture2d, 0, TextureBitmap, 0);

        var mCropWorkspace = new int[4];
        mCropWorkspace[0] = 0;
        mCropWorkspace[1] = TextureBitmap.Height;
        mCropWorkspace[2] = TextureBitmap.Width;
        mCropWorkspace[3] = -TextureBitmap.Height;

        GLES11.GlTexParameteriv(GL10.GlTexture2d, GL11Ext.GlTextureCropRectOes, mCropWorkspace, 0);

        var error = GLES10.GlGetError();
        if (error != GL10.GlNoError)
          throw new System.Exception("Texture Load GLError: " + error);
      }
    }

    public void draw(int x, int y, int z, int width, int height)
    {
      GLES10.GlBindTexture(GLES10.GlTexture2d, TextureId);

      if (mGrid == null)
      {
        GLES11Ext.GlDrawTexfOES(x, y, z, width, height);
      }
      else
      {
        // Draw using verts or VBO verts.
        GLES10.GlPushMatrix();
        GLES10.GlLoadIdentity();
        GLES10.GlTranslatef(x, y, z);

        mGrid.draw(true, false);

        GLES10.GlPopMatrix();
      }
    }

    private static Bitmap GetBitmapForTexture(Bitmap Bitmap)
    {
      var RequiredWidth = Bitmap.Width.RoundUpToPowerOf2();
      var RequiredHeight = Bitmap.Height.RoundUpToPowerOf2();

      var Overlay = Android.Graphics.Bitmap.CreateBitmap(RequiredWidth, RequiredHeight, Bitmap.GetConfig());
      var Canvas = new Android.Graphics.Canvas(Overlay);
      var Matrix = new Android.Graphics.Matrix();
      Matrix.PostTranslate(0, RequiredHeight - Bitmap.Height);

      Canvas.DrawBitmap(Bitmap, Matrix, null);
      return Overlay;
    }
  }

  class Grid
  {
    private FloatBuffer mFloatVertexBuffer;
    private FloatBuffer mFloatTexCoordBuffer;
    private FloatBuffer mFloatColorBuffer;
    private IntBuffer mFixedVertexBuffer;
    private IntBuffer mFixedTexCoordBuffer;
    private IntBuffer mFixedColorBuffer;

    private CharBuffer mIndexBuffer;

    private Java.Nio.Buffer mVertexBuffer;
    private Java.Nio.Buffer mTexCoordBuffer;
    private Java.Nio.Buffer mColorBuffer;
    private int mCoordinateSize;
    private int mCoordinateType;

    private int mW;
    private int mH;
    private int mIndexCount;
    private bool mUseHardwareBuffers;
    private int mVertBufferIndex;
    private int mIndexBufferIndex;
    private int mTextureCoordBufferIndex;
    private int mColorBufferIndex;

    public Grid(int vertsAcross, int vertsDown, bool useFixedPoint)
    {
      if (vertsAcross < 0 || vertsAcross >= 65536)
      {
        throw new IllegalArgumentException("vertsAcross");
      }
      if (vertsDown < 0 || vertsDown >= 65536)
      {
        throw new IllegalArgumentException("vertsDown");
      }
      if (vertsAcross * vertsDown >= 65536)
      {
        throw new IllegalArgumentException("vertsAcross * vertsDown >= 65536");
      }

      mUseHardwareBuffers = false;

      mW = vertsAcross;
      mH = vertsDown;
      var size = vertsAcross * vertsDown;
      const int FLOAT_SIZE = 4;
      const int FIXED_SIZE = 4;
      const int CHAR_SIZE = 2;

      if (useFixedPoint)
      {
        mFixedVertexBuffer = ByteBuffer.AllocateDirect(FIXED_SIZE * size * 3).Order(ByteOrder.NativeOrder()).AsIntBuffer();
        mFixedTexCoordBuffer = ByteBuffer.AllocateDirect(FIXED_SIZE * size * 2).Order(ByteOrder.NativeOrder()).AsIntBuffer();
        mFixedColorBuffer = ByteBuffer.AllocateDirect(FIXED_SIZE * size * 4).Order(ByteOrder.NativeOrder()).AsIntBuffer();

        mVertexBuffer = mFixedVertexBuffer;
        mTexCoordBuffer = mFixedTexCoordBuffer;
        mColorBuffer = mFixedColorBuffer;
        mCoordinateSize = FIXED_SIZE;
        mCoordinateType = GL10.GlFixed;

      }
      else
      {
        mFloatVertexBuffer = ByteBuffer.AllocateDirect(FLOAT_SIZE * size * 3).Order(ByteOrder.NativeOrder()).AsFloatBuffer();
        mFloatTexCoordBuffer = ByteBuffer.AllocateDirect(FLOAT_SIZE * size * 2).Order(ByteOrder.NativeOrder()).AsFloatBuffer();
        mFloatColorBuffer = ByteBuffer.AllocateDirect(FLOAT_SIZE * size * 4).Order(ByteOrder.NativeOrder()).AsFloatBuffer();

        mVertexBuffer = mFloatVertexBuffer;
        mTexCoordBuffer = mFloatTexCoordBuffer;
        mColorBuffer = mFloatColorBuffer;
        mCoordinateSize = FLOAT_SIZE;
        mCoordinateType = GL10.GlFloat;
      }

      var quadW = mW - 1;
      var quadH = mH - 1;
      var quadCount = quadW * quadH;
      var indexCount = quadCount * 6;
      mIndexCount = indexCount;
      mIndexBuffer = ByteBuffer.AllocateDirect(CHAR_SIZE * indexCount).Order(ByteOrder.NativeOrder()).AsCharBuffer();

      /*
       * Initialize triangle list mesh.
       *
       *     [0]-----[  1] ...
       *      |    /   |
       *      |   /    |
       *      |  /     |
       *     [w]-----[w+1] ...
       *      |       |
       *
       */

      {
        int i = 0;
        for (int y = 0; y < quadH; y++)
        {
          for (int x = 0; x < quadW; x++)
          {
            var a = (char)(y * mW + x);
            var b = (char)(y * mW + x + 1);
            var c = (char)((y + 1) * mW + x);
            var d = (char)((y + 1) * mW + x + 1);

            mIndexBuffer.Put(i++, a);
            mIndexBuffer.Put(i++, b);
            mIndexBuffer.Put(i++, c);

            mIndexBuffer.Put(i++, b);
            mIndexBuffer.Put(i++, c);
            mIndexBuffer.Put(i++, d);
          }
        }
      }

      mVertBufferIndex = 0;
    }

    void set(int i, int j, float x, float y, float z, float u, float v, float[] color)
    {
      if (i < 0 || i >= mW)
      {
        throw new IllegalArgumentException("i");
      }
      if (j < 0 || j >= mH)
      {
        throw new IllegalArgumentException("j");
      }

      var index = mW * j + i;

      var posIndex = index * 3;
      var texIndex = index * 2;
      var colorIndex = index * 4;

      if (mCoordinateType == GL10.GlFloat)
      {
        mFloatVertexBuffer.Put(posIndex, x);
        mFloatVertexBuffer.Put(posIndex + 1, y);
        mFloatVertexBuffer.Put(posIndex + 2, z);

        mFloatTexCoordBuffer.Put(texIndex, u);
        mFloatTexCoordBuffer.Put(texIndex + 1, v);

        if (color != null)
        {
          mFloatColorBuffer.Put(colorIndex, color[0]);
          mFloatColorBuffer.Put(colorIndex + 1, color[1]);
          mFloatColorBuffer.Put(colorIndex + 2, color[2]);
          mFloatColorBuffer.Put(colorIndex + 3, color[3]);
        }
      }
      else
      {
        mFixedVertexBuffer.Put(posIndex, (int)(x * (1 << 16)));
        mFixedVertexBuffer.Put(posIndex + 1, (int)(y * (1 << 16)));
        mFixedVertexBuffer.Put(posIndex + 2, (int)(z * (1 << 16)));

        mFixedTexCoordBuffer.Put(texIndex, (int)(u * (1 << 16)));
        mFixedTexCoordBuffer.Put(texIndex + 1, (int)(v * (1 << 16)));

        if (color != null)
        {
          mFixedColorBuffer.Put(colorIndex, (int)(color[0] * (1 << 16)));
          mFixedColorBuffer.Put(colorIndex + 1, (int)(color[1] * (1 << 16)));
          mFixedColorBuffer.Put(colorIndex + 2, (int)(color[2] * (1 << 16)));
          mFixedColorBuffer.Put(colorIndex + 3, (int)(color[3] * (1 << 16)));
        }
      }
    }

    public static void BeginDrawing(bool useTexture, bool useColor)
    {
      GLES10.GlEnableClientState(GL10.GlVertexArray);

      if (useTexture)
      {
        GLES10.GlEnableClientState(GL10.GlTextureCoordArray);
        GLES10.GlEnable(GL10.GlTexture2d);
      }
      else
      {
        GLES10.GlDisableClientState(GL10.GlTextureCoordArray);
        GLES10.GlDisable(GL10.GlTexture2d);
      }

      if (useColor)
      {
        GLES10.GlEnableClientState(GL10.GlColorArray);
      }
      else
      {
        GLES10.GlDisableClientState(GL10.GlColorArray);
      }
    }

    public void draw(bool useTexture, bool useColor)
    {
      if (!mUseHardwareBuffers)
      {
        GLES10.GlVertexPointer(3, mCoordinateType, 0, mVertexBuffer);

        if (useTexture)
          GLES10.GlTexCoordPointer(2, mCoordinateType, 0, mTexCoordBuffer);

        if (useColor)
          GLES10.GlColorPointer(4, mCoordinateType, 0, mColorBuffer);

        GLES10.GlDrawElements(GL10.GlTriangles, mIndexCount, GL10.GlUnsignedShort, mIndexBuffer);
      }
      else
      {
        // draw using hardware buffers
        GLES11.GlBindBuffer(GL11.GlArrayBuffer, mVertBufferIndex);
        GLES11.GlVertexPointer(3, mCoordinateType, 0, 0);

        if (useTexture)
        {
          GLES11.GlBindBuffer(GL11.GlArrayBuffer, mTextureCoordBufferIndex);
          GLES11.GlTexCoordPointer(2, mCoordinateType, 0, 0);
        }

        if (useColor)
        {
          GLES11.GlBindBuffer(GL11.GlArrayBuffer, mColorBufferIndex);
          GLES11.GlColorPointer(4, mCoordinateType, 0, 0);
        }

        GLES11.GlBindBuffer(GL11.GlElementArrayBuffer, mIndexBufferIndex);
        GLES11.GlDrawElements(GL11.GlTriangles, mIndexCount, GL11.GlUnsignedShort, 0);

        GLES11.GlBindBuffer(GL11.GlArrayBuffer, 0);
        GLES11.GlBindBuffer(GL11.GlElementArrayBuffer, 0);
      }
    }

    public static void EndDrawing()
    {
      GLES10.GlDisableClientState(GL10.GlVertexArray);
    }

    public bool usingHardwareBuffers()
    {
      return mUseHardwareBuffers;
    }
    public void invalidateHardwareBuffers()
    {
      //When the OpenGL ES device is lost, GL handles become invalidated. In that case, we just want to "forget" the old handles (without explicitly deleting them) and make new ones.
      mVertBufferIndex = 0;
      mIndexBufferIndex = 0;
      mTextureCoordBufferIndex = 0;
      mColorBufferIndex = 0;
      mUseHardwareBuffers = false;
    }
    public void releaseHardwareBuffers()
    {
      // Deletes the hardware buffers allocated by this object (if any).

      if (mUseHardwareBuffers)
      {
        var buffer = new int[1];
        buffer[0] = mVertBufferIndex;
        GLES11.GlDeleteBuffers(1, buffer, 0);

        buffer[0] = mTextureCoordBufferIndex;
        GLES11.GlDeleteBuffers(1, buffer, 0);

        buffer[0] = mColorBufferIndex;
        GLES11.GlDeleteBuffers(1, buffer, 0);

        buffer[0] = mIndexBufferIndex;
        GLES11.GlDeleteBuffers(1, buffer, 0);

        invalidateHardwareBuffers();
      }
    }
    public void generateHardwareBuffers()
    {
      // Allocates hardware buffers on the graphics card and fills them with
      // data if a buffer has not already been previously allocated.  Note that
      // this function uses the GL_OES_vertex_buffer_object extension, which is
      // not guaranteed to be supported on every device.
      // @param gl A pointer to the OpenGL ES context.

      if (!mUseHardwareBuffers)
      {
        var buffer = new int[1];

        // Allocate and fill the vertex buffer.
        GLES11.GlGenBuffers(1, buffer, 0);
        mVertBufferIndex = buffer[0];
        GLES11.GlBindBuffer(GL11.GlArrayBuffer, mVertBufferIndex);
        var vertexSize = mVertexBuffer.Capacity() * mCoordinateSize;
        GLES11.GlBufferData(GL11.GlArrayBuffer, vertexSize, mVertexBuffer, GL11.GlStaticDraw);

        // Allocate and fill the texture coordinate buffer.
        GLES11.GlGenBuffers(1, buffer, 0);
        mTextureCoordBufferIndex = buffer[0];
        GLES11.GlBindBuffer(GL11.GlArrayBuffer, mTextureCoordBufferIndex);
        var texCoordSize = mTexCoordBuffer.Capacity() * mCoordinateSize;
        GLES11.GlBufferData(GL11.GlArrayBuffer, texCoordSize, mTexCoordBuffer, GL11.GlStaticDraw);

        // Allocate and fill the color buffer.
        GLES11.GlGenBuffers(1, buffer, 0);
        mColorBufferIndex = buffer[0];
        GLES11.GlBindBuffer(GL11.GlArrayBuffer, mColorBufferIndex);
        var colorSize = mColorBuffer.Capacity() * mCoordinateSize;
        GLES11.GlBufferData(GL11.GlArrayBuffer, colorSize, mColorBuffer, GL11.GlStaticDraw);

        // Unbind the array buffer.
        GLES11.GlBindBuffer(GL11.GlArrayBuffer, 0);

        // Allocate and fill the index buffer.
        GLES11.GlGenBuffers(1, buffer, 0);
        mIndexBufferIndex = buffer[0];
        GLES11.GlBindBuffer(GL11.GlElementArrayBuffer, mIndexBufferIndex);
        // A char is 2 bytes.
        var indexSize = mIndexBuffer.Capacity() * 2;
        GLES11.GlBufferData(GL11.GlElementArrayBuffer, indexSize, mIndexBuffer, GL11.GlStaticDraw);

        // Unbind the element array buffer.
        GLES11.GlBindBuffer(GL11.GlElementArrayBuffer, 0);

        mUseHardwareBuffers = true;

        System.Diagnostics.Debug.Assert(mVertBufferIndex != 0);
        System.Diagnostics.Debug.Assert(mTextureCoordBufferIndex != 0);
        System.Diagnostics.Debug.Assert(mIndexBufferIndex != 0);
        System.Diagnostics.Debug.Assert(GLES11.GlGetError() == 0);
      }
    }

    // These functions exposed to patch Grid info into native code.
    public int getVertexBuffer()
    {
      return mVertBufferIndex;
    }
    public int getTextureBuffer()
    {
      return mTextureCoordBufferIndex;
    }
    public int getIndexBuffer()
    {
      return mIndexBufferIndex;
    }
    public int getColorBuffer()
    {
      return mColorBufferIndex;
    }
    public int getIndexCount()
    {
      return mIndexCount;
    }
    public bool getFixedPoint()
    {
      return (mCoordinateType == GL10.GlFixed);
    }
  }
}
#endif