**Invention**

Develop native apps for iOS, Android and Windows with 100% code sharing using Visual Studio and C#.NET. Download the Invention Manual app for your device:

* [Invention Manual on Apple iTunes](https://itunes.apple.com/us/app/invention-manual/id1195335633?mt=8)
* [Invention Manual on Google Play](https://play.google.com/store/apps/details?id=com.x10host.invention.manual)
* [Invention Manual on Windows Store](https://www.microsoft.com/en-us/store/p/invention-manual/9nj614c9m60v)

Short video on how to install the Invention Visual Studio extension and Nuget packages.

https://www.youtube.com/watch?v=Q5N3zTUGoJc&t=124s

Follow the devblog for Invention:

https://medium.com/@hodgskin.callan/

Supported platforms

	iOS		iTunes App Store
	ANDROID		Google Play* (or .apk file)
	UWP		Windows Store
	WPF		Windows Desktop
	SERVER		Remote client-server over TCP sockets

Write your app code in portable class libraries (PCLs) using the Invention API. Your app code runs consistently on each platform without additional effort. Build your app once and deliver to the three major app stores.
User interface can be thought as an arrangement of rectangular panels. There are panels for layout such as stacks, docks and overlays. There are panels for function such as buttons, labels, graphics, vidoes, edits and memos. There is even a panel for custom rendering at 60 fps. All panels have background, borders, corners, margins, padding, size, alignment, opacity and visibility.

Invention maps these panels to the appropriate native control on each platform. Default styling is stripped from each platform and you have full artistic control of your app. Invention also consolidates common services across the platforms.

	AUDIO		mp3 playback and sound effects
	CALENDAR	access the platforms date and time pickers
	CLIPBOARD	read and write text from the clipboard
	DEVICE		query device name, model, operating system and capabilities
	DIRECTORY	local storage of folders and files
	EMAIL		start email messages and attach files
	GRAPHICS	tinting, grayscale and resing images
	LOCATION	global positioning and reverse geocoding
	MARKET		launch the app listing in the app store
	PHONE		dial phone numbers and start sending text messages
	PROCESS		get process memory usage
	VAULT		save secrets to the keychain
	WEB		download from the web, use REST APIs and Json
	WINDOW		represents the device screen for layout management

Rapidly develop your app using the Windows Desktop platform. This platform is based on the Windows Presentation Foundation (WPF) and is fast to compile and run in Visual Studio. There are modes to emulate devices so you can easily refine the experience across all devices. Android and iOS applications require Xamarin (commercial license). Windows Store applications are built on the Windows Runtime (WinRT).

**Pathos** is the first game built on Invention. Pathos is a turn-based dungeon adventure game based on the rules of Nethack. Visit the [official website](https://pathos.azurewebsites.net) for the links to Apple iTunes, Google Play and the Windows Store.

**Phoenix Mobile** is the companion app for the Phoenix system. The app is for swim school instructors to take attendance and mark assessment when in the pool with students. The device needs a waterproof casing of course! You can evaluate the app by connecting to the KESTRAL DEMO SERVER and signing in with pin 9394.

* [Phoenix Mobile for iOS](https://itunes.apple.com/us/app/phoenix-mobile/id1266190537?ls=1&mt=8)
* [Phoenix Mobile for Android](https://play.google.com/store/apps/details?id=au.com.kestral.phoenix.mobile)

**Spellunk** is a fun and challenging word puzzle to demonstrate the capabilities of the Invention platform. This project is completely open source and welcomes community contributions.

* [Spellunk for iOS](mailto:hodgskin.callan@gmail.com) (Apple iTunes via TestFlight, email me to join)
* [Spellunk for Android](https://play.google.com/store/apps/details?id=hodgskin.callan.spellunk) (Google Play Open Beta)
* [Spellunk for Windows](https://www.microsoft.com/en-au/store/p/spellunk/9mvpfh3q9v9z) (Windows Store)


The following is the necessary code for a portable 'Hello World' application that runs on WPF, iOS, Android and UWP.

```csharp
// Portable Application
namespace My
{
  public static class App
  {
    public static void Install(Inv.Application Application)
    {
      var Surface = Application.Window.NewSurface();
      Surface.Background.Colour = Inv.Colour.WhiteSmoke;

      var Label = Surface.NewLabel();
      Surface.Content = Label;
      Label.Alignment.Center();
      Label.Font.Size = 20;
      Label.Text = "Hello World";

      Application.Window.Transition(Surface);
    }
  }
}

// Windows Desktop Mainline (WPF)
namespace My
{
  public class Program
  {
    [System.STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.Run(My.App.Install);
    }
  }
}

// iOS Mainline
namespace My
{
  public class Application
  {
    static void Main(string[] args)
    {
      Inv.iOSShell.Run(My.App.Install);
    }
  }
}

// Android Mainline
namespace My
{
  [Android.App.Activity(Label = "My App", MainLauncher = true, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    protected override void Install(Inv.Application Application)
    {
      My.App.Install(Application);
    }
  }
}

// Universal Windows Mainline
namespace My
{
  public sealed partial class MainPage : Windows.UI.Xaml.Controls.Page
  {
    public MainPage()
    {
      this.InitializeComponent();

      Inv.UwaShell.Attach(this, My.App.Install);
    }
  }
}
```

**Invention is open source under the MIT license.**
