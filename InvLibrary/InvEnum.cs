﻿/*! 2 !*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Inv.Support;

namespace Inv
{
  public interface IEnumArray
  {
    IEnumerable<EnumTuple<int, object>> GetTuples();
    void SetValue(int Index, object Value);
    int Length { get; }
  }

  [DataContract]
  public sealed class EnumArray<TKey, TValue> : IEnumArray, IEnumerable<TValue>
    where TKey : struct
  {
    public EnumArray()
    {
      this.Spec = EnumFoundation.LoadSpec<TKey>();
      this.ValueArray = new TValue[Spec.Count];
    }

    public int Length
    {
      get { return Spec.Count; }
    }
    public TKey? IndexOf(TValue Value)
    {
      foreach (var Result in Spec.KeyArray)
      {
        if (object.Equals(this[Result], Value))
          return Result;
      }

      return null;
    }
    public TValue this[TKey Index]
    {
      get { return ValueArray[Spec.AsIndex(Index)]; }
      set { ValueArray[Spec.AsIndex(Index)] = value; }
    }
    public void Add(TKey Key, TValue Value)
    {
      // NOTE: only for the collection initializer syntax.
      this[Key] = Value;
    }
    public void Fill(TValue Value)
    {
      foreach (var Key in Spec.KeyArray)
        ValueArray[Spec.AsIndex(Key)] = Value;
    }
    public void Fill(Func<TKey, TValue> Function)
    {
      foreach (var Key in Spec.KeyArray)
        ValueArray[Spec.AsIndex(Key)] = Function(Key);
    }
    public TKey Locate(TValue Value)
    {
      for (var Index = 0; Index < ValueArray.Length; Index++)
      {
        if (object.Equals(ValueArray[Index], Value))
          return Spec.KeyArray[Index];
      }

      return default(TKey);
    }
    public IEnumerator<TValue> GetEnumerator()
    {
      return ValueArray.AsEnumerable().GetEnumerator();
    }
    public IEnumerable<EnumTuple<TKey, TValue>> GetTuples()
    {
      foreach (var Key in Spec.KeyArray)
        yield return new EnumTuple<TKey, TValue>(Key, this[Key]);
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext c)
    {
      this.Spec = EnumFoundation.LoadSpec<TKey>();
    }

    IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
    {
      return GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    IEnumerable<EnumTuple<int, object>> IEnumArray.GetTuples()
    {
      return GetTuples().Select(T => new EnumTuple<int, object>((int)(object)(T.Key), T.Value));
    }
    void IEnumArray.SetValue(int Index, object Value)
    {
      this[(TKey)(object)Index] = (TValue)Value;
    }

    //[NonSerialized]
    private Inv.EnumSpec<TKey> Spec;
    [DataMember]
    private TValue[] ValueArray;
  }

  [DataContract]
  public sealed class EnumTuple<TKey, TValue>
    where TKey : struct
  {
    internal EnumTuple(TKey Key, TValue Value)
    {
      this.Key = Key;
      this.Value = Value;
    }

    [DataMember]
    public TKey Key { get; private set; }
    [DataMember]
    public TValue Value { get; private set; }
  }

  [DataContract]
  public struct EnumSet<TKey> : IEnumerable<TKey>
    where TKey : struct
  {
    static EnumSet()
    {
      Empty = new Inv.EnumSet<TKey>(EnumFoundation.LoadSpec<TKey>());
      Universal = new EnumSet<TKey>(EnumFoundation.LoadSpec<TKey>());
      Universal.BitArray.SetAll(true);
    }

    public static EnumSet<TKey> Empty { get; private set; }
    public static EnumSet<TKey> Universal { get; private set; }

    private EnumSet(EnumSpec<TKey> Spec, System.Collections.BitArray BitArray)
    {
      this.Spec = Spec;
      this.BitArray = BitArray;
    }
    private EnumSet(EnumSpec<TKey> Spec)
      : this(Spec, new System.Collections.BitArray(Spec.Count))
    {
    }

    public int Cardinality
    {
      get
      {
        var CopyArray = new Int32[(BitArray.Length >> 5) + 1];

        BitArray.CopyTo(CopyArray, 0);

        // fix for not truncated bits in last integer that may have been set to true with SetAll()
        CopyArray[CopyArray.Length - 1] &= ~(-1 << (BitArray.Length % 32));

        var Count = 0;
        for (var Index = 0; Index < CopyArray.Length; Index++)
        {
          var Value = CopyArray[Index];

          // magic (http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel)
          unchecked
          {
            Value = Value - ((Value >> 1) & 0x55555555);
            Value = (Value & 0x33333333) + ((Value >> 2) & 0x33333333);
            Value = ((Value + (Value >> 4) & 0xF0F0F0F) * 0x1010101) >> 24;
          }

          Count += Value;

        }

        return Count;
      }
    }
    public bool IsEmpty
    {
      get { return Cardinality == 0; }
    }
    public bool this[TKey Index]
    {
      get { return BitArray[Spec.AsIndex(Index)]; }
    }

    public bool Contains(TKey Member)
    {
      return BitArray[Spec.AsIndex(Member)];
    }
    public IEnumerator<TKey> GetEnumerator()
    {
      var ValueArray = Spec.KeyArray;

      for (var Index = 0; Index < BitArray.Length; Index++)
      {
        if (BitArray[Index])
          yield return ValueArray[Index];
      }
    }
    public EnumSet<TKey> Intersect(EnumSet<TKey> OtherSet)
    {
      var Result = new EnumSet<TKey>(Spec);

      Result.BitArray.Or(OtherSet.BitArray);
      Result.BitArray.And(BitArray);

      return Result;
    }
    public EnumSet<TKey> Intersect(TKey Member)
    {
      var Result = new EnumSet<TKey>(Spec);

      Result.BitArray.Set(Spec.AsIndex(Member), true);
      Result.BitArray.And(BitArray);

      return Result;
    }
    public EnumSet<TKey> Union(EnumSet<TKey> OtherSet)
    {
      var Result = new EnumSet<TKey>(Spec);

      Result.BitArray.Or(BitArray);
      Result.BitArray.Or(OtherSet.BitArray);

      return Result;
    }
    public EnumSet<TKey> Union(TKey Member)
    {
      var Result = new EnumSet<TKey>(Spec);

      Result.BitArray.Set(Spec.AsIndex(Member), true);
      Result.BitArray.Or(BitArray);

      return Result;
    }
    public EnumSet<TKey> Except(EnumSet<TKey> OtherSet)
    {
      var Result = new EnumSet<TKey>(Spec);

      for (var Index = 0; Index < BitArray.Length; Index++)
      {
        if (OtherSet.BitArray[Index])
          Result.BitArray[Index] = false;
      }

      return Result;
    }

    public override bool Equals(object obj)
    {
      return this == (EnumSet<TKey>)obj; // uses the == operator overload.
    }
    public override int GetHashCode()
    {
      return BitArray.GetHashCode();
    }
    public override string ToString()
    {
      return "[" + string.Join(", ", Enum.GetNames(typeof(TKey))) + "]";
    }

    public static EnumSet<TKey> operator &(EnumSet<TKey> Left, TKey Right)
    {
      return Left.Intersect(Right);
    }
    public static EnumSet<TKey> operator &(EnumSet<TKey> Left, EnumSet<TKey> Right)
    {
      return Left.Intersect(Right);
    }
    public static EnumSet<TKey> operator |(EnumSet<TKey> Left, EnumSet<TKey> Right)
    {
      return Left.Union(Right);
    }
    public static EnumSet<TKey> operator |(EnumSet<TKey> Left, TKey Right)
    {
      return Left.Union(Right);
    }
    public static bool operator ==(EnumSet<TKey> Left, EnumSet<TKey> Right)
    {
      if (object.ReferenceEquals(Left, Right))
        return true;

      //if (Right == null)
      //  return false;

      for (var Index = 0; Index < Left.BitArray.Length; Index++)
      {
        if (Left.BitArray[Index] != Right.BitArray[Index])
          return false;
      }

      return true;
    }
    public static bool operator !=(EnumSet<TKey> Left, EnumSet<TKey> Right)
    {
      return !(Left == Right); // uses the == operator overload.
    }

    [OnDeserialized]
    private void OnDeserialized(StreamingContext c)
    {
      this.Spec = EnumFoundation.LoadSpec<TKey>();
    }

    IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
    {
      return GetEnumerator();
    }
    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    //[NonSerialized]
    private Inv.EnumSpec<TKey> Spec;
    [DataMember]
    private BitArray BitArray;
  }

  internal sealed class EnumSpec<TKey>
    where TKey : struct
  {
    internal EnumSpec()
    {
      this.KeyArray = (TKey[])Enum.GetValues(typeof(TKey));

      if (KeyArray.Length == 0 || ((int)(object)KeyArray[0] == 0 && KeyArray.Select(V => (int)(object)V).ToList().IsContiguous()))
      {
        // Key starts from zero and is contiguous, we don't need a dictionary.
        this.IndexDictionary = null;
        this.Count = KeyArray.Length;
      }
      else
      {
        this.IndexDictionary = new Dictionary<TKey, int>();

        var Index = 0;
        foreach (var Member in Inv.Support.EnumHelper.GetEnumerable<TKey>())
          IndexDictionary.Add(Member, Index++);

        this.Count = IndexDictionary.Count;
      }
    }

    public int Count { get; private set; }
    public TKey[] KeyArray { get; private set; }

    public int AsIndex(TKey Member)
    {
      return IndexDictionary != null ? IndexDictionary[Member] : (int)(object)Member;
    }

    private Dictionary<TKey, int> IndexDictionary;
  }

  internal sealed class EnumFoundation
  {
    static EnumFoundation()
    {
      SpecDictionary = new Dictionary<Type, object>();
    }

    internal static Inv.EnumSpec<TKey> LoadSpec<TKey>()
      where TKey : struct
    {
      var EnumType = typeof(TKey);

      lock (SpecDictionary)
        return (EnumSpec<TKey>)SpecDictionary.GetOrAdd(EnumType, Key => new EnumSpec<TKey>());
    }

    private static Dictionary<Type, object> SpecDictionary;
  }
}