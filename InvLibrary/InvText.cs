﻿/*! 2 !*/
using System;
using System.Text;
using System.IO;
using System.Globalization;

namespace Inv
{
  public sealed class TextParser : IDisposable
  {
    public TextParser(TextReader TextReader)
    {
      this.Cache = "";
      this.StringBuilder = new StringBuilder();
      this.TextReader = TextReader;

      // Initialise extractor to determine if it is already finished.
      NextCharacter();
    }
    public void Dispose()
    {
      TextReader.Dispose();
    }

    public TextReader TextReader { get; private set; }

    public bool More()
    {
      return !Finished;
    }
    public char NextCharacter()
    {
      char Result;

      if (Cache.Length == 0)
      {
        if (Finished)
        {
          Result = '\0';
        }
        else
        {
          var NextRead = TextReader.Read();

          if (NextRead == -1)
          {
            Finished = true;
            Result = '\0';
          }
          else
          {
            Result = (char)NextRead;

            Cache += Result;
          }
        }
      }
      else
      {
        Result = Cache[0];
      }

      return Result;
    }
    public bool NextIsString(string Value)
    {
      var ValueLength = Value.Length;
      var CacheLength = Cache.Length;
      var ReadLength = ValueLength - CacheLength;

      if (ReadLength > 0 && !Finished)
      {
        var StreamCache = new char[ReadLength];

        var ResultLength = TextReader.Read(StreamCache, 0, ReadLength);

        if (ResultLength <= 0)
          Finished = Cache.Length == 0;
        else if (ResultLength == ReadLength)
          Cache += new string(StreamCache);
        else
          Cache += new string(StreamCache, 0, ResultLength);
      }

      return Cache.StartsWith(Value);
    }
    public char ReadCharacter()
    {
      var Result = NextCharacter();

      if (Cache.Length > 0)
        Cache = Cache.Substring(1, Cache.Length - 1);

      return Result;
    }
    public string ReadString(string Value)
    {
      if (!NextIsString(Value))
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected token '{0}' but found token '{1}'.", Value, Cache.Substring(1, Value.Length)));

      Cache = Cache.Substring(Value.Length, Cache.Length - Value.Length);

      return Value;
    }
    public string ReadUntilString(string Value)
    {
      StringBuilder.Length = 0;

      while (More() && !NextIsString(Value))
        StringBuilder.Append(ReadCharacter());

      return StringBuilder.ToString();
    }
    public string ReadWhileCharacterSet(CharacterSet CharacterSet)
    {
      StringBuilder.Length = 0;

      while (More() && CharacterSet.ContainsValue(NextCharacter()))
        StringBuilder.Append(ReadCharacter());

      return StringBuilder.ToString();
    }
    public string ReadUntilCharacterSet(CharacterSet CharacterSet)
    {
      StringBuilder.Length = 0;

      while (More() && !CharacterSet.ContainsValue(NextCharacter()))
        StringBuilder.Append(ReadCharacter());

      return StringBuilder.ToString();
    }
    public string ReadUntilCharacter(char Character)
    {
      StringBuilder.Length = 0;

      while (More() && Character != NextCharacter())
        StringBuilder.Append(ReadCharacter());

      return StringBuilder.ToString();
    }
    public char ReadCharacter(char Value)
    {
      if (NextCharacter() != Value)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected character {0} but found character {1}.", Value, NextCharacter()));

      return ReadCharacter();
    }

    private string Cache;
    private StringBuilder StringBuilder;
    private bool Finished;
  }
}
