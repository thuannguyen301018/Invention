﻿/*! 13 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using Inv.Support;

namespace Inv
{
  // TODO: efficient float/double require unsafe pointers?

  internal static class CompactFoundation
  {
    internal const byte Null = 0x00;
    internal const byte NonNull = 0xFF;
  }

  public sealed class CompactProtocol
  {
    public CompactProtocol()
    {
      this.InterceptDictionary = new Dictionary<Type, CompactIntercept>();
    }

    internal CompactIntercept GetIntercept<T>()
    {
      return InterceptDictionary.GetValueOrDefault(typeof(T));
    }
    public void AddIntercept<T>(Action<Inv.CompactWriter, T> WriterAction, Func<Inv.CompactReader, T> ReaderFunc)
    {
      InterceptDictionary.Add(typeof(T), new CompactIntercept(typeof(T), (Writer, Context) => WriterAction(Writer, (T)Context), (Reader) => ReaderFunc(Reader)));
    }

    private Dictionary<Type, CompactIntercept> InterceptDictionary;
  }

  internal sealed class CompactIntercept
  {
    internal CompactIntercept(Type Type, Action<Inv.CompactWriter, object> WriterAction, Func<Inv.CompactReader, object> ReaderFunc)
    {
      this.Type = Type;
      this.WriterAction = WriterAction;
      this.ReaderFunc = ReaderFunc;
    }

    public Type Type { get; private set; }
    public Action<Inv.CompactWriter, object> WriterAction { get; private set; }
    public Func<Inv.CompactReader, object> ReaderFunc { get; private set; }
  }

  public sealed class CompactWriter : IDisposable
  {
    public CompactWriter(Stream Stream, bool DisposeStream)
    {
      this.Stream = Stream;
      this.DisposeStream = DisposeStream;
    }
    public void Dispose()
    {
      if (DisposeStream)
        Stream.Dispose();
    }

    public void SetProtocol(CompactProtocol Protocol)
    {
      this.Protocol = Protocol;
    }
    public void WriteDataObject<T>(T Value)
    {
      var Intercept = Protocol?.GetIntercept<T>();

      if (Intercept != null)
      {
        if(!WriteNull(Value))
          Intercept.WriterAction(this, Value);
      }
      else
      {
        Debug.WriteLine("*** NO INTERCEPT " + typeof(T).ToString());

        // TODO: inline data contract serializers not working?
        //using (var XmlBinaryWriter = XmlDictionaryWriter.CreateBinaryWriter(Stream, null, null, false))
        //  new DataContractSerializer(typeof(T)).WriteObject(XmlBinaryWriter, Value);

        using (var MemoryStream = new MemoryStream())
        {
          using (var XmlBinaryWriter = System.Xml.XmlDictionaryWriter.CreateBinaryWriter(MemoryStream))
            new System.Runtime.Serialization.DataContractSerializer(typeof(T)).WriteObject(XmlBinaryWriter, Value);

          MemoryStream.Flush();

          WriteByteArray(MemoryStream.ToArray());
        }
      }
    }
    public void WriteDataObjectArray<T>(T[] ValueArray)
    {
      var Intercept = Protocol?.GetIntercept<T>();

      if (Intercept != null)
      {
        WriteInt32(ValueArray.Length);

        foreach (var Value in ValueArray)
        {
          if(!WriteNull(Value))
            Intercept.WriterAction(this, Value);
        }
      }
      else
      {
        Debug.WriteLine("*** NO INTERCEPT " + ValueArray.GetType().ToString());

        using (var MemoryStream = new MemoryStream())
        {
          using (var XmlBinaryWriter = System.Xml.XmlDictionaryWriter.CreateBinaryWriter(MemoryStream))
            new System.Runtime.Serialization.DataContractSerializer(typeof(T[])).WriteObject(XmlBinaryWriter, ValueArray);

          MemoryStream.Flush();

          WriteByteArray(MemoryStream.ToArray());
        }
      }
    }
    public void WriteStream(Action<Stream> Action)
    {
      Action(Stream);
    }
    public void WriteBoolean(bool Value)
    {
      Stream.WriteByte(Value ? (byte)1 : (byte)0);
    }
    public void WriteBooleanNullable(bool? Value)
    {
      Stream.WriteByte(Value == null ? (byte)255 : (Value.Value ? (byte)1 : (byte)0));
    }
    public void WriteCharacter(char Value)
    {
      var Buffer = BitConverter.GetBytes(Value);
      WriteByteArray(Buffer);
    }
    public void WriteDateTime(DateTime Value)
    {
      WriteInt64(Value.ToBinary());
    }
    public void WriteDateTimeNullable(DateTime? Value)
    {
      if (!WriteNull(Value))
        WriteDateTime(Value.Value);
    }
    public void WriteDateTimeOffset(DateTimeOffset Value)
    {
      WriteInt64(Value.DateTime.ToBinary());
      WriteInt64(Value.Offset.Ticks);
    }
    public void WriteDateTimeOffsetNullable(DateTimeOffset? Value)
    {
      if (!WriteNull(Value))
        WriteDateTimeOffset(Value.Value);
    }
    public void WriteDate(Inv.Date Value)
    {
      WriteInt64(Value.ToDateTimeBinary());
    }
    public void WriteGuid(Guid Value)
    {
      var GuidArray = Value.ToByteArray();

      Stream.Write(GuidArray, 0, GuidArray.Length);
    }
    public void WriteGuidNullable(Guid? Value)
    {
      if (!WriteNull(Value))
        WriteGuid(Value.Value);
    }
    public void WriteTimeSpan(TimeSpan Value)
    {
      WriteInt64(Value.Ticks);
    }
    public void WriteTimeSpanNullable(TimeSpan? Value)
    {
      if (!WriteNull(Value))
        WriteTimeSpan(Value.Value);
    }
    public void WriteByte(byte Value)
    {
      Stream.WriteByte(Value);
    }
    public void WriteSInt8(sbyte Value)
    {
      Stream.WriteByte((byte)Value);
    }
    public void WriteUInt8(byte Value)
    {
      Stream.WriteByte(Value);
    }
    public void WriteUInt16(ushort Value)
    {
      this.InternalBuffer[0] = (byte)Value;
      this.InternalBuffer[1] = (byte)(Value >> 8);
      Stream.Write(this.InternalBuffer, 0, 2);
    }
    public void WriteUInt32(uint Value)
    {
      this.InternalBuffer[0] = (byte)Value;
      this.InternalBuffer[1] = (byte)(Value >> 8);
      this.InternalBuffer[2] = (byte)(Value >> 16);
      this.InternalBuffer[3] = (byte)(Value >> 24);
      Stream.Write(this.InternalBuffer, 0, 4);
    }
    public void WriteInt16(short Value)
    {
      this.InternalBuffer[0] = (byte)Value;
      this.InternalBuffer[1] = (byte)(Value >> 8);
      Stream.Write(this.InternalBuffer, 0, 2);
    }
    public void WriteInt32(int Value)
    {
      this.InternalBuffer[0] = (byte)Value;
      this.InternalBuffer[1] = (byte)(Value >> 8);
      this.InternalBuffer[2] = (byte)(Value >> 16);
      this.InternalBuffer[3] = (byte)(Value >> 24);
      Stream.Write(this.InternalBuffer, 0, 4);
    }
    public void WriteInt32Nullable(int? Value)
    {
      if (!WriteNull(Value))
        WriteInt32(Value.Value);
    }
    public void WriteInt64(long Value)
    {
      this.InternalBuffer[0] = (byte)Value;
      this.InternalBuffer[1] = (byte)(Value >> 8);
      this.InternalBuffer[2] = (byte)(Value >> 16);
      this.InternalBuffer[3] = (byte)(Value >> 24);
      this.InternalBuffer[4] = (byte)(Value >> 32);
      this.InternalBuffer[5] = (byte)(Value >> 40);
      this.InternalBuffer[6] = (byte)(Value >> 48);
      this.InternalBuffer[7] = (byte)(Value >> 56);
      Stream.Write(InternalBuffer, 0, 8);
    }
    public void WriteInt64Nullable(long? Value)
    {
      if (!WriteNull(Value))
        WriteInt64(Value.Value);
    }
    public void WriteDecimal(decimal Value)
    {
      var Bits = Decimal.GetBits(Value);
      var lo = Bits[0];
      var mid = Bits[1];
      var hi = Bits[2];
      var flags = Bits[3];

      InternalBuffer[0] = (byte)lo;
      InternalBuffer[1] = (byte)(lo >> 8);
      InternalBuffer[2] = (byte)(lo >> 16);
      InternalBuffer[3] = (byte)(lo >> 24);

      InternalBuffer[4] = (byte)mid;
      InternalBuffer[5] = (byte)(mid >> 8);
      InternalBuffer[6] = (byte)(mid >> 16);
      InternalBuffer[7] = (byte)(mid >> 24);

      InternalBuffer[8] = (byte)hi;
      InternalBuffer[9] = (byte)(hi >> 8);
      InternalBuffer[10] = (byte)(hi >> 16);
      InternalBuffer[11] = (byte)(hi >> 24);

      InternalBuffer[12] = (byte)flags;
      InternalBuffer[13] = (byte)(flags >> 8);
      InternalBuffer[14] = (byte)(flags >> 16);
      InternalBuffer[15] = (byte)(flags >> 24);

      Stream.Write(InternalBuffer, 0, 16);
    }
    public void WriteDecimalNullable(decimal? Value)
    {
      if (!WriteNull(Value))
        WriteDecimal(Value.Value);
    }
    public void WriteFloat(float Value)
    {
      var FloatArray = BitConverter.GetBytes(Value);
      Stream.Write(FloatArray, 0, FloatArray.Length);
    }
    public void WriteFloatNullable(float? Value)
    {
      if (!WriteNull(Value))
        WriteFloat(Value.Value);
    }
    public void WriteDouble(double Value)
    {
      var DoubleArray = BitConverter.GetBytes(Value);
      Stream.Write(DoubleArray, 0, DoubleArray.Length);
    }
    public void WriteDoubleNullable(double? Value)
    {
      if (!WriteNull(Value))
        WriteDouble(Value.Value);
    }
    public void WriteString(string Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Buffer = Encoding.UTF8.GetBytes(Value);
        var Length = Buffer.Length;

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(CompactFoundation.NonNull);
        else
          Stream.Write(Buffer, 0, Length);
      }
    }
    public void WriteBinary(Inv.Binary Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.GetLength();

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(CompactFoundation.NonNull);

        WriteString(Value.GetFormat());
        WriteByteArray(Value.GetBuffer());
      }
    }
    public void WriteImage(Inv.Image Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.GetLength();

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(CompactFoundation.NonNull);

        WriteString(Value.GetFormat());
        WriteByteArray(Value.GetBuffer());
      }
    }
    public void WriteSound(Inv.Sound Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.GetLength();

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(CompactFoundation.NonNull);

        WriteString(Value.GetFormat());
        WriteByteArray(Value.GetBuffer());
      }
    }
    public void WriteByteArray(byte[] Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.Length;

        WriteInt32(Length);

        if (Length == 0)
          WriteByte(CompactFoundation.NonNull);
        else
          Stream.Write(Value, 0, Length);
      }
    }
    public void WriteInt32Array(int[] Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.Length;

        WriteInt32(Length);

        if (Length == 0)
        {
          WriteByte(CompactFoundation.NonNull);
        }
        else
        {
          for (var Index = 0; Index < Length; Index++)
            WriteInt32(Value[Index]);
        }
      }
    }
    public void WriteInt64Array(long[] Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.Length;

        WriteInt32(Length);

        if (Length == 0)
        {
          WriteByte(CompactFoundation.NonNull);
        }
        else
        {
          for (var Index = 0; Index < Value.Length; Index++)
            WriteInt64(Value[Index]);
        }
      }
    }
    public void WriteInt64NullableArray(long?[] Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.Length;

        WriteInt32(Length);

        if (Length == 0)
        {
          WriteByte(CompactFoundation.NonNull);
        }
        else
        {
          for (var Index = 0; Index < Value.Length; Index++)
            WriteInt64Nullable(Value[Index]);
        }
      }
    }
    public void WriteStringArray(string[] Value)
    {
      if (Value == null)
      {
        WriteInt32(0); // length
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        var Length = Value.Length;

        WriteInt32(Length);

        if (Length == 0)
        {
          WriteByte(CompactFoundation.NonNull);
        }
        else
        {
          for (var Index = 0; Index < Value.Length; Index++)
            WriteString(Value[Index]);
        }
      }
    }
    public void WriteColour(Inv.Colour Value)
    {
      if (Value == null)
      {
        WriteInt32(0);
        WriteByte(CompactFoundation.Null);
      }
      else
      {
        WriteInt32(Value.RawValue);

        if (Value.RawValue == 0)
          WriteByte(CompactFoundation.NonNull);
      }
    }
    public void WriteUri(Uri Value)
    {
      WriteString(Value?.OriginalString);
    }

    private bool WriteNull<T>(T? Value)
      where T : struct
    {
      var Result = Value == null;

      WriteByte(Result ? CompactFoundation.Null : CompactFoundation.NonNull);

      return Result;
    }
    private bool WriteNull<T>(T Value)
    {
      var Result = Value == null;

      WriteByte(Result ? CompactFoundation.Null : CompactFoundation.NonNull);

      return Result;
    }

    private Stream Stream;
    private bool DisposeStream;
    private CompactProtocol Protocol;
    private byte[] InternalBuffer = new byte[16]; // always 16 bytes, for optimised writes of data types.
  }

  public sealed class CompactReader : IDisposable
  {
    public CompactReader(Stream Stream, bool DisposeStream)
    {
      this.Stream = Stream;

      var num = Encoding.UTF8.GetMaxByteCount(1);
      if (num < 16)
        num = 16;
      this.InternalBuffer = new byte[num];
      this.DisposeStream = DisposeStream;
    }
    public void Dispose()
    {
      if (DisposeStream)
        Stream.Dispose();
    }

    public void SetProtocol(CompactProtocol Protocol)
    {
      this.Protocol = Protocol;
    }
    public T ReadDataObject<T>()
    {
      var Intercept = Protocol?.GetIntercept<T>();

      if (Intercept != null)
      {
        if (ReadNull())
          return default(T);
        else
          return (T)Intercept.ReaderFunc(this);
      }
      else
      {
        // TODO: inline data contract serializers not working?
        //using (var BinaryReader = XmlDictionaryReader.CreateBinaryReader(Stream, XmlDictionaryReaderQuotas.Max))
        //  return (T)new DataContractSerializer(typeof(T)).ReadObject(BinaryReader);

        var Result = default(T);

        var ReadBuffer = ReadByteArray();

        using (var MemoryStream = new MemoryStream(ReadBuffer))
        using (var BinaryReader = System.Xml.XmlDictionaryReader.CreateBinaryReader(MemoryStream, System.Xml.XmlDictionaryReaderQuotas.Max))
          Result = (T)new System.Runtime.Serialization.DataContractSerializer(typeof(T)).ReadObject(BinaryReader);

        return Result;
      }
    }
    public T[] ReadDataObjectArray<T>()
    {
      var Intercept = Protocol?.GetIntercept<T>();

      if (Intercept != null)
      {
        var ArrayLength = ReadInt32();
        var Result = new T[ArrayLength];

        for (var Index = 0; Index < ArrayLength; Index++)
        {
          if (ReadNull())
            Result[Index] = default(T);
          else
            Result[Index] = (T)Intercept.ReaderFunc(this);
        }

        return Result;
      }
      else
      {
        var Result = default(T[]);

        var ReadBuffer = ReadByteArray();

        using (var MemoryStream = new MemoryStream(ReadBuffer))
        using (var BinaryReader = System.Xml.XmlDictionaryReader.CreateBinaryReader(MemoryStream, System.Xml.XmlDictionaryReaderQuotas.Max))
          Result = (T[])new System.Runtime.Serialization.DataContractSerializer(typeof(T[])).ReadObject(BinaryReader);

        return Result;
      }
    }

    public void ReadStream(Action<Stream> Action)
    {
      Action(Stream);
    }
    public Inv.Binary ReadBinary()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Format = ReadString();
      var Buffer = ReadByteArray();

      return new Inv.Binary(Buffer, Length, Format);
    }
    public bool ReadBoolean()
    {
      FillInternalBuffer(1);
      return InternalBuffer[0] != 0;
    }
    public bool? ReadBooleanNullable()
    {
      FillInternalBuffer(1);
      return InternalBuffer[0] == 255 ? (bool?)null : (InternalBuffer[0] != 0);
    }
    public byte ReadByte()
    {
      var Result = Stream.ReadByte();

      if (Result == -1)
        throw new Exception("End of stream");

      return (byte)Result;
    }
    public byte[] ReadByteArray()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      return FillExternalBuffer(Length);
    }
    public char ReadCharacter()
    {
      var Buffer = ReadByteArray();

      return BitConverter.ToChar(Buffer, 0);
    }
    public Inv.Colour ReadColour()
    {
      var RawValue = ReadInt32();

      if (RawValue == 0)
      {
        if (ReadNull())
          return null;
      }

      return Inv.Colour.FromArgb(RawValue);
    }
    public Inv.Date ReadDate()
    {
      var DateTimeBinary = ReadInt64();

      return new Inv.Date(DateTime.FromBinary(DateTimeBinary));
    }
    public DateTime ReadDateTime()
    {
      var DateTimeBinary = ReadInt64();

      return DateTime.FromBinary(DateTimeBinary);
    }
    public DateTime? ReadDateTimeNullable()
    {
      if (ReadNull())
        return null;

      return ReadDateTime();
    }
    public DateTimeOffset ReadDateTimeOffset()
    {
      var DateTimeBinary = ReadInt64();
      var OffsetTicks = ReadInt64();

      return new DateTimeOffset(DateTime.FromBinary(DateTimeBinary), TimeSpan.FromTicks(OffsetTicks));
    }
    public DateTimeOffset? ReadDateTimeOffsetNullable()
    {
      if (ReadNull())
        return null;

      return ReadDateTimeOffset();
    }
    public decimal ReadDecimal()
    {
      FillInternalBuffer(16);

      var lo = ((int)InternalBuffer[0]) | ((int)InternalBuffer[1] << 8) | ((int)InternalBuffer[2] << 16) | ((int)InternalBuffer[3] << 24);
      var mid = ((int)InternalBuffer[4]) | ((int)InternalBuffer[5] << 8) | ((int)InternalBuffer[6] << 16) | ((int)InternalBuffer[7] << 24);
      var hi = ((int)InternalBuffer[8]) | ((int)InternalBuffer[9] << 8) | ((int)InternalBuffer[10] << 16) | ((int)InternalBuffer[11] << 24);
      var flags = ((int)InternalBuffer[12]) | ((int)InternalBuffer[13] << 8) | ((int)InternalBuffer[14] << 16) | ((int)InternalBuffer[15] << 24);

      return new Decimal(new int[] { lo, mid, hi, flags });
    }
    public decimal? ReadDecimalNullable()
    {
      if (ReadNull())
        return null;

      return ReadDecimal();
    }
    public double ReadDouble()
    {
      FillInternalBuffer(8);
      return BitConverter.ToDouble(InternalBuffer, 0);
    }
    public double? ReadDoubleNullable()
    {
      if (ReadNull())
        return null;

      return ReadDouble();
    }
    public float ReadFloat()
    {
      FillInternalBuffer(4);
      return BitConverter.ToSingle(InternalBuffer, 0);
    }
    public float? ReadFloatNullable()
    {
      if (ReadNull())
        return null;

      return ReadFloat();
    }
    public Guid ReadGuid()
    {
      FillInternalBuffer(16);
      return new Guid(InternalBuffer); // TODO: copy this buffer?
    }
    public Guid? ReadGuidNullable()
    {
      if (ReadNull())
        return null;

      return ReadGuid();
    }
    public Inv.Image ReadImage()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Format = ReadString();
      var Buffer = ReadByteArray();

      return new Inv.Image(Buffer, Length, Format);
    }
    public short ReadInt16()
    {
      FillInternalBuffer(2);
      return (short)((int)InternalBuffer[0] | (int)InternalBuffer[1] << 8);
    }
    public int ReadInt32()
    {
      FillInternalBuffer(4);
      return BitConverter.ToInt32(InternalBuffer, 0);
    }
    public int? ReadInt32Nullable()
    {
      if (ReadNull())
        return null;

      return ReadInt32();
    }
    public int[] ReadInt32Array()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Result = new int[Length];

      for (var Index = 0; Index < Length; Index++)
        Result[Index] = ReadInt32();

      return Result;
    }
    public long ReadInt64()
    {
      FillInternalBuffer(8);
      var num = (uint)((int)InternalBuffer[0] | (int)InternalBuffer[1] << 8 | (int)InternalBuffer[2] << 16 | (int)InternalBuffer[3] << 24);
      var num2 = (uint)((int)InternalBuffer[4] | (int)InternalBuffer[5] << 8 | (int)InternalBuffer[6] << 16 | (int)InternalBuffer[7] << 24);
      return (long)((ulong)num2 << 32 | (ulong)num);
    }
    public long? ReadInt64Nullable()
    {
      if (ReadNull())
        return null;

      return ReadInt64();
    }
    public long[] ReadInt64Array()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Result = new long[Length];

      for (var Index = 0; Index < Length; Index++)
        Result[Index] = ReadInt64();

      return Result;
    }
    public long?[] ReadInt64NullableArray()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Result = new long?[Length];

      for (var Index = 0; Index < Length; Index++)
        Result[Index] = ReadInt64Nullable();

      return Result;
    }
    public sbyte ReadSInt8()
    {
      FillInternalBuffer(1);
      return (sbyte)this.InternalBuffer[0];
    }
    public Inv.Sound ReadSound()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Format = ReadString();
      var Buffer = ReadByteArray();

      return new Inv.Sound(Buffer, Length, Format);
    }
    public string ReadString()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Buffer = FillExternalBuffer(Length);

      return Encoding.UTF8.GetString(Buffer, 0, Length);
    }
    public TimeSpan ReadTimeSpan()
    {
      return new TimeSpan(ReadInt64());
    }
    public TimeSpan? ReadTimeSpanNullable()
    {
      if (ReadNull())
        return null;

      return ReadTimeSpan();
    }
    public byte ReadUInt8()
    {
      return ReadByte();
    }
    public ushort ReadUInt16()
    {
      FillInternalBuffer(2);
      return (ushort)((int)InternalBuffer[0] | (int)InternalBuffer[1] << 8);
    }
    public uint ReadUInt32()
    {
      FillInternalBuffer(4);
      return (uint)((int)InternalBuffer[0] | (int)InternalBuffer[1] << 8 | (int)InternalBuffer[2] << 16 | (int)InternalBuffer[3] << 24);
    }
    public Uri ReadUri()
    {
      var Result = ReadString();

      return Result != null ? new Uri(Result) : null;
    }
    public string[] ReadStringArray()
    {
      var Length = ReadInt32();

      if (Length == 0)
      {
        if (ReadNull())
          return null;
      }

      var Result = new string[Length];

      for (var Index = 0; Index < Length; Index++)
        Result[Index] = ReadString();

      return Result;
    }

    private byte[] FillExternalBuffer(int Length)
    { 
      var Result = new byte[Length];

      FillBuffer(ref Result, Length);

      return Result;
    }
    private void FillInternalBuffer(int Length)
    {
      FillBuffer(ref InternalBuffer, Length);
    }
    private void FillBuffer(ref byte[] Buffer, int Length)
    {
      if (Length == 1)
      {
        var Result = Stream.ReadByte();

        if (Result == -1)
          throw new Exception("End of stream");

        Buffer[0] = (byte)Result;
      }
      else if (Length > 0)
      {
        var Count = 0;
        do
        {
          var Bytes = Stream.Read(Buffer, Count, Length - Count);

          if (Bytes == 0)
            throw new Exception("End of stream");

          Count += Bytes;
        }
        while (Count < Length);
      }
    }
    private bool ReadNull()
    {
      return ReadByte() == 0x00;
    }

    private Stream Stream;
    private bool DisposeStream;
    private byte[] InternalBuffer; // max 16 bytes, for optimised reads of data types.
    private CompactProtocol Protocol;
  }
}