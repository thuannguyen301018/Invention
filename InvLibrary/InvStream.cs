﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a Unix-style pipe of data between a reader and a writer thread.
  /// </summary>
  /// <remarks>
  /// <para><see cref="PipeStream"/> can be used to pipe data between threads. After creating the <see cref="PipeStream"/> with the desired maximum buffer size, the
  /// reader thread should attempt to <see cref="Read"/> data from the <see cref="PipeStream"/>; this call will block until data is available.</para>
  /// <para>The writer thread can <see cref="Write"/> data into the <see cref="PipeStream"/> at any point; as enough data becomes available for the reader
  /// thread to read, <see cref="Read"/> will return.</para>
  /// </remarks>
  public sealed class PipeStream : Stream
  {
    /// <summary>
    /// Initializes a new <see cref="PipeStream"/> with the specified buffer size.
    /// </summary>
    /// <param name="BufferSize">The maximum size of the buffer to use for this <see cref="PipeStream"/>.</param>
    public PipeStream(int BufferSize)
    {
      this.BufferQueue = new Queue<byte>();
      this.BufferSize = BufferSize;
    }

    /// <summary>
    /// The total number of bytes written to this <see cref="PipeStream"/>.
    /// </summary>
    public long WriteSize { get; private set; }
    /// <summary>
    /// True; <see cref="PipeStream"/> supports reading.
    /// </summary>
    public override bool CanRead
    {
      get { return true; }
    }
    /// <summary>
    /// False; <see cref="PipeStream"/> does not support seeking.
    /// </summary>
    public override bool CanSeek
    {
      get { return false; }
    }
    /// <summary>
    /// True; <see cref="PipeStream"/> supports writing.
    /// </summary>
    public override bool CanWrite
    {
      get { return true; }
    }
    /// <summary>
    /// Get the length in bytes of the stream.
    /// </summary>
    public override long Length
    {
      get { return BufferQueue.Count; }
    }
    /// <summary>
    /// Gets or sets the position within the current stream. Always 0 for <see cref="PipeStream"/>, cannot be set.
    /// </summary>
    public override long Position
    {
      get { return 0; }
      set { throw new NotImplementedException(); }
    }

    /// <summary>
    /// Flush the <see cref="PipeStream"/>, signaling any blocked readers that there is available data.
    /// </summary>
    public override void Flush()
    {
      IsFlushed = true;

      lock (BufferQueue)
      {
        Monitor.Pulse(BufferQueue);
      }
    }
    /// <summary>
    /// Write data to the <see cref="PipeStream"/>.
    /// </summary>
    /// <remarks>
    /// <see cref="Write"/> will block if the <see cref="PipeStream"/> buffer fills up until a reader successfully reads some bytes from the <see cref="PipeStream"/>.
    /// </remarks>
    /// <param name="buffer">The buffer containing the data to write.</param>
    /// <param name="offset">The first byte within the buffer to write.</param>
    /// <param name="count">The number of bytes to write to the <see cref="PipeStream"/>.</param>
    public override void Write(byte[] buffer, int offset, int count)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(buffer, "buffer");
        Inv.Assert.Check(offset >= 0, "Offset must be zero or more.");
        Inv.Assert.Check(count >= 0, "Count must be zero or more.");
        Inv.Assert.Check(offset + count <= buffer.Length, "Write length must be within buffer length.");
      }

      if (count > 0)
      {
        var Total = count;
        var Index = offset;

        lock (BufferQueue)
        {
          do
          {
            while (BufferQueue.Count >= BufferSize)
              Monitor.Wait(BufferQueue);

            IsFlushed = false;

            var Limit = BufferSize - BufferQueue.Count;

            if (Limit < Total)
            {
              Total -= Limit;
              Limit += Index;
            }
            else
            {
              Limit = Index + Total;
              Total = 0;
            }

            while (Index < Limit)
              BufferQueue.Enqueue(buffer[Index++]);

            Monitor.Pulse(BufferQueue);
          }
          while (Total > 0);
        }

        WriteSize += count;
      }
    }
    /// <summary>
    /// Attempt to read data from the <see cref="PipeStream"/>.
    /// </summary>
    /// <remarks>
    /// <see cref="Read"/> will block until the buffer has been flushed, and until either the buffer is full or there are at least <paramref name="count"/> bytes available to read.
    /// </remarks>
    /// <param name="buffer">The buffer to store read data into.</param>
    /// <param name="offset">The offset into the buffer to start storing read data to.</param>
    /// <param name="count">The number of bytes to read.</param>
    /// <returns>The number of bytes successfully read.</returns>
    public override int Read(byte[] buffer, int offset, int count)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(buffer != null, "Buffer must not be null.");
        Inv.Assert.Check(offset >= 0, "Offset must be zero or more.");
        Inv.Assert.Check(count >= 0, "Count must be zero or more.");
        Inv.Assert.Check(offset + count <= buffer.Length, "Read length must be within buffer length.");
      }

      if (count == 0)
      {
        return 0;
      }
      else
      {
        var Total = count;
        var Index = offset;

        lock (BufferQueue)
        {
          do
          {
            var Range = Math.Min(BufferSize, Total);

            while (BufferQueue.Count < Range && !IsFlushed)
              Monitor.Wait(BufferQueue);

            var Limit = BufferQueue.Count;

            if (Limit == 0)
              break;

            if (Limit < Total)
            {
              Total -= Limit;
              Limit += Index;
            }
            else
            {
              Limit = Index + Total;
              Total = 0;
            }

            while (Index < Limit)
              buffer[Index++] = BufferQueue.Dequeue();

            Monitor.Pulse(BufferQueue);
          }
          while (Total > 0);
        }

        return count - Total;
      }
    }
    /// <summary>
    /// Set the position within the current stream. Not implemented by <see cref="PipeStream"/>.
    /// </summary>
    /// <exception cref="NotImplementedException">Always thrown; <see cref="PipeStream"/> does not support seeking.</exception>
    /// <param name="offset">A byte offset relative to the <paramref name="origin"/> parameter.</param>
    /// <param name="origin">A value of type <see cref="System.IO.SeekOrigin"/> indicating the reference point used to obtain the new position.</param>
    /// <returns>The new position within the current stream.</returns>
    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }
    /// <summary>
    /// Set the length of the current stream. Not implemented by <see cref="PipeStream"/>.
    /// </summary>
    /// <exception cref="NotImplementedException">Always thrown.</exception>
    /// <param name="value">The desired length of the current stream in bytes.</param>
    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }

    private int BufferSize;
    private readonly Queue<byte> BufferQueue;
    private bool IsFlushed;
  }

  public sealed class ProgressStream : Stream
  {
    public ProgressStream(Stream Stream)
    {
      this.Stream = Stream;
      this.StreamLength = Stream.Length;
      this.StreamPosition = Stream.Position;
    }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
        Stream.Dispose();
    }

    public event Action ProgressEvent;

    public override bool CanRead
    {
      get { return true; }
    }
    public override bool CanWrite
    {
      get { return false; }
    }
    public override bool CanSeek
    {
      get { return false; }
    }
    public override long Length
    {
      get { return StreamLength; }
    }
    public override long Position
    {
      get { return StreamPosition; }
      set { throw new NotImplementedException(); }
    }

    public override void Flush()
    {
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      var Result = Stream.Read(buffer, offset, count);
      StreamPosition += Result;

      if (ProgressEvent != null)
        ProgressEvent();

      return Result;
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }

    private readonly Stream Stream;
    private readonly long StreamLength;
    private long StreamPosition;
  }

  public sealed class BufferedReadStream : Stream
  {
    public BufferedReadStream(Stream InputStream, Func<Stream, Stream> OutputFunction)
    {
      this.InputStream = InputStream;
      this.MemoryStream = new MemoryStream();
      this.OutputStream = OutputFunction(MemoryStream);
    }

    public override bool CanWrite
    {
      get { throw new NotImplementedException(); }
    }
    public override bool CanRead
    {
      get { return true; }
    }
    public override bool CanSeek
    {
      get { throw new NotImplementedException(); }
    }
    public override void Flush()
    {
      throw new NotImplementedException();
    }
    public override long Length
    {
      get { throw new NotImplementedException(); }
    }
    public override long Position
    {
      get { throw new NotImplementedException(); }
      set { throw new NotImplementedException(); }
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      while (true)
      {
        var ReadLength = MemoryStream.Read(buffer, offset, count);
        if (ReadLength > 0)
          return ReadLength;

        if (InputStream == null)
        {
          OutputStream.Dispose();
          return 0;
        }

        MemoryStream.Position = 0;

        ReadLength = InputStream.Read(FileBuffer, 0, FileBuffer.Length);
        if (ReadLength == 0)
        {
          InputStream.Dispose();
          InputStream = null;

          OutputStream.Flush();
        }
        else
        {
          OutputStream.Write(FileBuffer, 0, ReadLength);
        }

        MemoryStream.SetLength(MemoryStream.Position);
        MemoryStream.Position = 0;
      }
    }

    private Stream InputStream;
    private readonly Stream OutputStream;
    private readonly MemoryStream MemoryStream;
    private readonly byte[] FileBuffer = new byte[64 * 1024];
  }

  public sealed class ComparisonStream : System.IO.Stream
  {
    public ComparisonStream(System.IO.Stream SourceStream)
    {
      this.SourceStream = SourceStream;
    }

    public override bool CanRead
    {
      get { return false; }
    }
    public override bool CanSeek
    {
      get { return false; }
    }
    public override bool CanWrite
    {
      get { return true; }
    }
    public override void Flush()
    {
    }
    public override long Length
    {
      get { throw new NotImplementedException(); }
    }
    public override long Position
    {
      get
      {
        throw new NotImplementedException();
      }
      set
      {
        throw new NotImplementedException();
      }
    }

    public override int Read(byte[] buffer, int offset, int count)
    {
      throw new NotImplementedException();
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      throw new NotImplementedException();
    }
    public override void SetLength(long value)
    {
      throw new NotImplementedException();
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      var Input = new byte[count];
      SourceStream.Read(Input, 0, count);

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(offset == 0, "Write offset must be zero.");

      if (!buffer.Slice(count).EqualTo(Input))
      {
        if (System.Diagnostics.Debugger.IsAttached)
          Debugger.Break();
        else
          Debug.Assert(false, "Buffer does not match input array.");
      }
    }

    private Stream SourceStream;
  }
}