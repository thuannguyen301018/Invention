﻿/*! 7 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class WeakReferenceGovernor
  {
    static WeakReferenceGovernor()
    {
      CompactionIsActive = true;

      ManagedCompaction = new Inv.WeakReferenceCollection<Inv.IWeakReferenceCompaction>();

      CompactionInterval = TimeSpan.FromMinutes(1);
      CompactSignal = new AutoResetSignal(false, "WeakReferenceGovernor-Compact");

      CompactionCriticalSection = new ExclusiveCriticalSection("WeakReferenceGovernor-Compaction");

      CompactionTask = Inv.TaskGovernor.RunActivity("Inv.WeakReferenceGovernor-Compaction", CompactionMethod);
    }

    public static TimeSpan CompactionInterval { get; set; }
    public static Inv.WeakReferenceCollection<T> NewCollection<T>()
    {
      var Result = new Inv.WeakReferenceCollection<T>();

      AddCompaction(Result);

      return Result;
    }
    public static Inv.WeakReferenceAugmentation<TKey, TValue> NewAugmentation<TKey, TValue>()
    {
      var Result = new Inv.WeakReferenceAugmentation<TKey, TValue>();

      AddCompaction(Result);

      return Result;
    }
    public static void AddCompaction(IWeakReferenceCompaction Compaction)
    {
      ManagedCompaction.Add(Compaction);
    }
    public static void RequestCompact()
    {
      CompactSignal.Set();
    }
    public static void ShutdownCompact()
    {
      CompactionIsActive = false;
      CompactSignal.Set();
    }
    public static void Compact()
    {
      // calls to compact are serialised.
      using (CompactionCriticalSection.Lock())
      {
        ManagedCompaction.Compact();

        foreach (var CompactCollection in ManagedCompaction)
          CompactCollection.Compact();
      }
    }

    private static void CompactionMethod()
    {
      try
      {
        do
        {
          CompactSignal.WaitOne(CompactionInterval);

          if (CompactionIsActive)
          {
            try
            {
              Compact();
            }
            catch (Exception Exception)
            {
              // NOTE: this is not expected to fail.
              if (Debugger.IsAttached)
                Debugger.Break();
              else
                Debug.Assert(false, Exception.Describe());
            }
          }
        }
        while (CompactionIsActive);
      }
      catch (Exception Exception)
      {
        // TODO: a failed compaction thread means the collections will accumulate 'dead' entries and may impact on performance.
        if (Debugger.IsAttached)
          Debugger.Break();
        else
          Debug.Assert(false, Exception.Describe());
      }
    }

    private static bool CompactionIsActive;
    private static Inv.WeakReferenceCollection<IWeakReferenceCompaction> ManagedCompaction;
    private static Inv.ExclusiveCriticalSection CompactionCriticalSection;
    private static Inv.TaskActivity CompactionTask;
    private static AutoResetSignal CompactSignal;
  }

  public interface IWeakReferenceCompaction
  {
    void Compact();
  }

  public sealed class WeakReferenceCollection<TItem> : IWeakReferenceCompaction, IEnumerable<TItem>
  {
    internal WeakReferenceCollection()
    {
      this.CriticalSection = new Inv.ExclusiveCriticalSection("WeakReferenceCollection");
      this.ReferenceList = new Inv.DistinctList<WeakReference>();
    }

    public void Clear()
    {
      using (CriticalSection.Lock())
      {
        ReferenceList.Clear();
      }
    }
    public void Add(TItem Item)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Item, "Item");

      var WeakReference = new WeakReference(Item);

      using (CriticalSection.Lock())
      {
        ReferenceList.Add(WeakReference);
      }
    }
    public void Remove(TItem Item)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Item, "Item");

      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => Item.Equals(Index.Target));
      }
    }
    public void RemoveRange(IEnumerable<TItem> Items)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Items, "Items");

      var ItemSet = Items.ToHashSetX();

      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => ItemSet.Contains((TItem)Index.Target));
      }
    }
    public TItem RemoveFirstOrDefault()
    {
      using (CriticalSection.Lock())
      {
        for (var WeakReferenceIndex = 0; WeakReferenceIndex < ReferenceList.Count; WeakReferenceIndex++)
        {
          var WeakReference = ReferenceList[WeakReferenceIndex];
          var WeakReferenceTarget = WeakReference.Target;

          if (WeakReferenceTarget != null)
          {
            ReferenceList.RemoveRange(0, WeakReferenceIndex + 1);

            return (TItem)WeakReferenceTarget;
          }
        }

        return default(TItem);
      }
    }
    public int Count
    {
      get
      {
        using (CriticalSection.Lock())
        {
          return ReferenceList.Count(Index => Index.Target != null);
        }
      }
    }

    internal void Compact()
    {
      using (CriticalSection.Lock())
      {
        ReferenceList.RemoveAll(Index => !Index.IsAlive);
      }
    }

    private Inv.DistinctList<TItem> Retrieve()
    {
      var Result = new Inv.DistinctList<TItem>();

      using (CriticalSection.Lock())
      {
        foreach (var WeakReference in ReferenceList)
        {
          var Item = WeakReference.Target;

          if (Item != null)
            Result.Add((TItem)Item);
        }
      }

      return Result;
    }

    void IWeakReferenceCompaction.Compact()
    {
      Compact();
    }
    IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return Retrieve().GetEnumerator();
    }

    private Inv.ExclusiveCriticalSection CriticalSection;
    private Inv.DistinctList<WeakReference> ReferenceList;
  }

  public sealed class WeakReferenceAugmentation<TKey, TValue> : IWeakReferenceCompaction, IEnumerable<KeyValuePair<TKey, TValue>>
  {
    internal WeakReferenceAugmentation()
    {
      this.CriticalSection = new Inv.ExclusiveCriticalSection("WeakReferenceAugmentation");
      this.Dictionary = new Dictionary<WeakKey, TValue>(new WeakKeyEqualityComparer());
    }

    public int Count
    {
      get
      {
        using (CriticalSection.Lock())
        {
          return Dictionary.Count(Index => Index.Key.Reference.IsAlive);
        }
      }
    }

    public void Add(TKey Key, TValue Value)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        Dictionary.Add(WeakKey, Value);
      }
    }
    public bool Remove(TKey Key)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.Remove(WeakKey);
      }
    }
    public void Update(TKey Key, Func<TValue, TValue> ModifyFunction)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        var CurrentValue = Dictionary.GetValueOrDefault(WeakKey);

        var ModifyValue = ModifyFunction(CurrentValue);

        if (!object.Equals(ModifyValue, CurrentValue))
          Dictionary[WeakKey] = CurrentValue;
      }
    }
    public bool HasValue(TKey Key)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.ContainsKey(WeakKey);
      }
    }
    public bool TryGetValue(TKey Key, out TValue Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Key != null, "Key must not be null.");

      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        return Dictionary.TryGetValue(WeakKey, out Value);
      }
    }
    public bool TrySetValue(TKey Key, TValue Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Key != null, "Key must not be null.");

      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        if (Dictionary.ContainsKey(WeakKey))
        {
          Dictionary[WeakKey] = Value;

          return true;
        }
        else
        {
          return false;
        }
      }
    }
    public TValue GetValueOrDefault(TKey Key, TValue DefaultValue = default(TValue))
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        if (Dictionary.TryGetValue(WeakKey, out var Value))
          return Value;
        else
          return DefaultValue;
      }
    }
    public TValue GetValueOrAdd(TKey Key, Func<TKey, TValue> AddFunction)
    {
      var WeakKey = new WeakKey(Key);

      using (CriticalSection.Lock())
      {
        if (!Dictionary.TryGetValue(WeakKey, out var Value))
        {
          Value = AddFunction(Key);

          Dictionary.Add(WeakKey, Value);
        }

        return Value;
      }
    }
    public TValue this[TKey Key]
    {
      get
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Key != null, "Key must not be null.");

        var WeakKey = new WeakKey(Key);

        using (CriticalSection.Lock())
        {
          return Dictionary[WeakKey];
        }
      }
      set
      {
        var WeakKey = new WeakKey(Key);

        using (CriticalSection.Lock())
        {
          Dictionary[WeakKey] = value;
        }
      }
    }

    internal void Compact()
    {
      using (CriticalSection.Lock())
        Dictionary.RemoveAll(Index => !Index.Key.Reference.IsAlive);
    }
    internal void Compact(Func<TValue, bool> RemoveWhereFunc)
    {
      using (CriticalSection.Lock())
        Dictionary.RemoveAll(Index => !Index.Key.Reference.IsAlive || RemoveWhereFunc(Index.Value));
    }

    private Inv.DistinctList<KeyValuePair<TKey, TValue>> ToDistinctList()
    {
      using (CriticalSection.Lock())
      {
        var Result = new Inv.DistinctList<KeyValuePair<TKey, TValue>>(Dictionary.Count);

        foreach (var WeakReference in Dictionary)
        {
          var ReferenceTarget = WeakReference.Key.Reference.Target;

          if (ReferenceTarget != null)
            Result.Add(new KeyValuePair<TKey, TValue>((TKey)ReferenceTarget, WeakReference.Value));
        }

        return Result;
      }
    }

    void IWeakReferenceCompaction.Compact()
    {
      Compact();
    }
    IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
    {
      return ToDistinctList().GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return ToDistinctList().GetEnumerator();
    }

    private ExclusiveCriticalSection CriticalSection;
    private Dictionary<WeakKey, TValue> Dictionary;

    private sealed class WeakKey
    {
      public WeakKey(TKey Key)
      {
        this.Reference = new WeakReference(Key);
        this.HashCode = Key.GetHashCode();
      }

      public WeakReference Reference { get; private set; }
      public int HashCode { get; private set; }
    }

    private sealed class WeakKeyEqualityComparer : IEqualityComparer<WeakKey>
    {
      bool IEqualityComparer<WeakKey>.Equals(WeakKey x, WeakKey y)
      {
        if (x == y)
        {
          return true;
        }
        else
        {
          var xTarget = x.Reference.Target;
          var yTarget = y.Reference.Target;

          return xTarget != null && yTarget != null && xTarget.Equals(yTarget);
        }
      }
      int IEqualityComparer<WeakKey>.GetHashCode(WeakKey obj)
      {
        return obj.HashCode;
      }
    }
  }
}