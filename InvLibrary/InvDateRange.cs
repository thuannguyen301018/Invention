﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class DateRange
  {
    public DateRange(Inv.Date? FromValue, Inv.Date? UntilValue)
    {
      this.Base = new Inv.Range<Inv.Date>(FromValue, UntilValue);
    }
    internal DateRange(Inv.Range<Inv.Date> Base)
    {
      this.Base = Base;
    }

    public Inv.Date? From
    {
      get { return Base.From.Index; }
    }
    public Inv.Date? Until
    {
      get { return Base.Until.Index; }
    }

    public override bool Equals(object obj)
    {
      var Source = obj as Inv.DateRange;

      if (Source != null)
        return Base.Equals(Source.Base);
      else
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return Base.GetHashCode();
    }
    public override string ToString()
    {
      return Base.ToString();
    }

    internal Inv.Range<Inv.Date> Base { get; private set; }
  }

  public sealed class DateRangeSet : IEnumerable<Inv.DateRange>
  {
    public DateRangeSet()
    {
      this.Base = new RangeSet<Inv.Date>();
      Base.IntersectOnBoundary = true;
    }
    public DateRangeSet(Inv.DateRange DateRange)
    {
      this.Base = new RangeSet<Inv.Date>(DateRange.Base);
      Base.IntersectOnBoundary = true;
    }
    public DateRangeSet(Inv.Date? From, Inv.Date? Until)
      : this(new Inv.DateRange(From, Until))
    {
    }
    public DateRangeSet(Inv.Date Date)
      : this(Date, Date)
    {
    }

    public int Count
    {
      get { return Base.Count; }
    }
    public Inv.DateRange FirstRange
    {
      get { return AsDateRange(Base.FirstRange); }
    }
    public Inv.DateRange LastRange
    {
      get { return AsDateRange(Base.LastRange); }
    }

    public override bool Equals(object obj)
    {
      var Source = obj as Inv.DateRangeSet;

      if (Source != null)
        return Base.Equals(Source.Base);
      else
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return Base.GetHashCode();
    }
    public override string ToString()
    {
      return Base.ToString();
    }

    public bool IsEmpty()
    {
      return Base.IsEmpty();
    }
    public bool IsUniversal()
    {
      return Base.IsUniversal();
    }
    public bool IsEqualTo(Inv.DateRangeSet CompareSet)
    {
      return Base.IsEqualTo(CompareSet.Base);
    }
    public bool Intersects(Inv.DateRange Range)
    {
      return Base.Intersects(Range.Base);
    }
    public bool Intersects(Inv.Date? From, Inv.Date? Until)
    {
      return Base.Intersects(From, Until);
    }
    public Inv.DateRangeSet Invert()
    {
      return new Inv.DateRangeSet(Base.Invert());
    }
    public Inv.DateRangeSet Union(Inv.Date? From, Inv.Date? Until)
    {
      return new Inv.DateRangeSet(Base.Union(From, Until));
    }
    public Inv.DateRangeSet Union(Inv.DateRange Range)
    {
      return new Inv.DateRangeSet(Base.Union(Range.Base));
    }
    public Inv.DateRangeSet Intersect(Inv.Date? From, Inv.Date? Until)
    {
      return new Inv.DateRangeSet(Base.Intersect(From, Until));
    }
    public Inv.DateRangeSet Intersect(Inv.DateRangeSet IntersectSet)
    {
      return new Inv.DateRangeSet(Base.Intersect(IntersectSet.Base));
    }
    public Inv.DateRangeSet Subtract(Inv.DateRangeSet SubtractSet)
    {
      return new Inv.DateRangeSet(Base.Subtract(SubtractSet.Base));
    }
    public Inv.DateRangeSet Subtract(Inv.Date? From, Inv.Date? Until)
    {
      return new Inv.DateRangeSet(Base.Subtract(From, Until));
    }
    public Inv.DateRangeSet Union(Inv.DateRangeSet UnionSet)
    {
      return new Inv.DateRangeSet(Base.Union(UnionSet.Base));
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      throw new NotImplementedException();
    }
    System.Collections.Generic.IEnumerator<Inv.DateRange> System.Collections.Generic.IEnumerable<Inv.DateRange>.GetEnumerator()
    {
      return EnumerateRanges().GetEnumerator();
    }

    private Inv.DateRange AsDateRange(Inv.Range<Inv.Date> Range)
    {
      return Range != null ? new Inv.DateRange(Range) : null;
    }
    private IEnumerable<Inv.DateRange> EnumerateRanges()
    {
      return Base.Select(R => new DateRange(R));
    }

    public static DateRangeSet FromDateRanges(IEnumerable<Inv.DateRange> DateRanges)
    {
      var Result = Empty;

      foreach (var DateRange in DateRanges)
        Result = Result.Union(DateRange);

      return Result;
    }
    public static DateRangeSet FromDateRangeArray(params Inv.DateRange[] DateRangeArray)
    {
      var Result = Empty;

      foreach (var DateRange in DateRangeArray)
        Result = Result.Union(DateRange);

      return Result;
    }

    public static readonly DateRangeSet Empty = new DateRangeSet();
    public static readonly DateRangeSet Universal = new DateRangeSet(null, null);

    private DateRangeSet(Inv.RangeSet<Inv.Date> Base)
    {
      this.Base = Base;
      Base.IntersectOnBoundary = true;
    }

    private Inv.RangeSet<Inv.Date> Base;
  }
}