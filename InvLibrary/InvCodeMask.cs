﻿/*! 3 !*/
using System;
using System.Collections;
using Inv.Support;
using System.Text;
using System.Diagnostics;

//
// Mask Format:
//
//  C = Any character
//  9 = Numbers (0..9);
//  A = Alpha numeric (0..9, a-z)
//  L = Alphabet letter (a-z)
//  N = Non-alphanumeric
//
//  Use MinLength to indicate the required and optional parts of the mask.
//  Characters beyond the mask and before MaxLength are considered to have the format
//  of C = any character.
//
//  All other characters are considered literal.  Backslash '\' can be used to
//  escape the mask characters.  It is good practice to backslash your literal
//  characters to allow future expandability.
//
//  eg #1: Post Code = 9999
//     #2: 8.3 filename = AAAAAAAA\.AAA
//     #3: XML Date Time Format = 9999\/99\/99\T99\:99\:99
//

namespace Inv
{
  public sealed class CodeMask
  {
    public CodeMask(string Format)
    {
      this.Format = Format;
      this.ElementList = new Inv.DistinctList<CodeMaskElement>();

      var Escape = false;

      foreach (var Symbol in Format)
      {
        if (Escape)
        {
          ElementList.Add(new CodeMaskElement(CodeMaskType.Specific, Symbol));

          Escape = false;
        }
        else
        {
          switch (Symbol)
          {
            case '\\':
              Escape = true;
              break;

            case 'C':
              ElementList.Add(new CodeMaskElement(CodeMaskType.Any, null));
              break;

            case '9':
              ElementList.Add(new CodeMaskElement(CodeMaskType.Number, null));
              break;

            case 'A':
              ElementList.Add(new CodeMaskElement(CodeMaskType.Alphanumeric, null));
              break;

            case 'L':
              ElementList.Add(new CodeMaskElement(CodeMaskType.Letter, null));
              break;

            case 'N':
              ElementList.Add(new CodeMaskElement(CodeMaskType.NonAlphanumeric, null));
              break;

            default:
              ElementList.Add(new CodeMaskElement(CodeMaskType.Specific, Symbol));
              break;
          }
        }
      }

      if (Escape)
        throw new Exception("Incomplete escape symbol in code mask: " + Format);
    }

    public int MinimumLength { get; set; }
    public int MaximumLength { get; set; }

    public string AsFormatted(string Text)
    {
      var Index = 0;
      var Length = Text.Length;

      var Builder = new StringBuilder();

      foreach (var Element in ElementList)
      {
        if (Index < Length)
        {
          if (Element.Type == CodeMaskType.Specific)
            Builder.Append(Element.Specific);
          else
            Builder.Append(Text[Index++]);
        }
      }

      if (Index < Length)
        Builder.Append(Text.Substring(Index, Length - Index));

      return Builder.ToString().ToUpper();
    }
    public string AsUnformatted(string Text)
    {
      var Index = 0;
      var Length = Text.Length;

      var Builder = new StringBuilder();

      foreach (var Element in ElementList)
      {
        if (Index < Length)
        {
          if (Element.Type != CodeMaskType.Specific)
            Builder.Append(Text[Index]);

          Index++;
        }
      }

      if (Index < Length)
        Builder.Append(Text.Substring(Index, Length - Index));

      return Builder.ToString();
    }

    public bool Fixed(int Index)
    {
      if (!ElementList.ExistsAt(Index))
        return false;

      return ElementList[Index].Type == CodeMaskType.Specific;
    }
    public int LeadingFixedLength()
    {
      var Result = 0;
      while ((Result < Format.Length) && Fixed(Result))
        Result++;

      return Result;
    }
    public bool Conforms(string Value)
    {
      var Length = Value.Length;

      if (MinimumLength > 0 && Length < MinimumLength)
        return false;

      if (MaximumLength > 0 && Length > MaximumLength)
        return false;

      var Index = 0;
      while (Index < Length)
      {
        if (!Allowed(Index).Allowed(Value[Index]))
          return false;
        Index++;
      }
      return true;
    }

    private CodeMaskElement Allowed(int CharacterIndex)
    {
      if (CharacterIndex >= 0 && CharacterIndex < ElementList.Count)
        return ElementList[CharacterIndex];
      else
        return new CodeMaskElement(CodeMaskType.Any, null);
    }
    
    private string Format;
    private Inv.DistinctList<CodeMaskElement> ElementList;
  }

  internal sealed class CodeMaskElement
  {
    internal CodeMaskElement(CodeMaskType Type, char? Specific)
    {
      this.Type = Type;
      this.Specific = Specific;
    }

    public bool Allowed(char Symbol)
    {
      switch (Type)
      {
        case CodeMaskType.Any:
          return true;

        case CodeMaskType.Number:
          return Symbol.IsNumeric();

        case CodeMaskType.Alphanumeric:
          return char.IsLetterOrDigit(Symbol);

        case CodeMaskType.Letter:
          return char.IsLetter(Symbol);

        case CodeMaskType.NonAlphanumeric:
          return !char.IsLetterOrDigit(Symbol);

        case CodeMaskType.Specific:
          return Specific.HasValue ? Char.ToUpper(Symbol).CompareTo(Char.ToUpper(Specific.Value)) == 0 : false;

        default:
          throw new Exception(string.Format("Unhandled CodeMaskType {0}", Type));
      }
    }

    public CodeMaskType Type { get; set; }
    public char? Specific { get; set; }
  }

  internal enum CodeMaskType
  {
    Any, Letter, Number, Alphanumeric, NonAlphanumeric, Specific
  }
}

