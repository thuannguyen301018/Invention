﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Diagnostics;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class AutoResetSignal : Signal
  {
    public AutoResetSignal(bool initialState, string name)
      : base (initialState, System.Threading.EventResetMode.AutoReset, name)
    {
    }
  }

  public sealed class ManualResetSignal : Signal
  {
    public ManualResetSignal(bool initialState, string name)
      : base(initialState, System.Threading.EventResetMode.ManualReset, name)
    {
    }

    public void Reset()
    {
      Handle.Reset();
    }
  }

  public abstract class Signal : IDisposable
  {
    internal Signal(bool initialState, System.Threading.EventResetMode mode, string name)
    {
      Debug.Assert(!string.IsNullOrEmpty(name), "Name is mandatory for Inv.Signals");
      // Name length is actually 260, but headway for the system prefix and the guid suffix means i've guessed a max of 200.
      Debug.Assert(name.Length < 200, "Name must be shorter than 200 characters");

      this.Handle = new EventWaitHandle(initialState, mode, $"{name}-{Guid.NewGuid()}");
    }

    public void Dispose()
    {
      Handle.Dispose();
    }

    public void Set()
    {
      Handle.Set();
    }

    public bool WaitOne()
    {
      return Handle.WaitOne();
    }
    public bool WaitOne(int millisecondsTimeout)
    {
      return Handle.WaitOne(millisecondsTimeout);
    }
    public bool WaitOne(TimeSpan timeout)
    {
      return Handle.WaitOne(timeout);
    }

    internal readonly EventWaitHandle Handle;
  }
}
