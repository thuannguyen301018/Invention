﻿/*! 9 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public abstract class BitmixItem<TItem>
    where TItem : BitmixItem<TItem>
  {
    protected BitmixItem(int Index, string Identifier)
    {
      this.Index = Index;
      this.Flag = 1L << Index;
      this.Identifier = Identifier;
    }

    public readonly int Index;
    public readonly long Flag;
    public readonly string Identifier;

    public override string ToString()
    {
      return Identifier;
    }

    public static readonly Inv.DistinctList<TItem> List;

    protected static void Touch()
    {
      // force the static constructor.
    }

    static BitmixItem()
    {
      List = new Inv.DistinctList<TItem>();

      var Index = 0;

      foreach (var Field in typeof(TItem).GetReflectionInfo().GetReflectionFields().Where(F => F.IsPublic && F.IsStatic))
      {
        if (Field.FieldType == typeof(TItem))
        {
          var Property = (TItem)Activator.CreateInstance(typeof(TItem), new object[] { Index++, Field.Name });
          Field.SetValue(null, Property);
          List.Add(Property);
        }
      }

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(List.Count <= 64, "More than 64 items is not supported by the BitSet.");
    }
  }

  public abstract class BitmixSet<TSet, TItem> : IEnumerable<TItem>
    where TSet : BitmixSet<TSet, TItem>
    where TItem : BitmixItem<TItem>
  {
    public BitmixSet()
    {
    }
    public BitmixSet(TItem[] ItemArray)
    {
      AddArray(ItemArray);
    }

    public static readonly TSet Empty;
    public static readonly TSet Universal;
    public static TSet FromRaw(long Bits)
    {
      var Result = NewSet();
      Result.Bits = Bits;
      return Result;
    }
    public long GetRaw()
    {
      return Bits;
    }
    public bool All()
    {
      return Bits == Universal.Bits;
    }
    public bool Any()
    {
      return Bits != 0;
    }
    public int Count()
    {
      var Result = 0;
      foreach (var P in ItemList)
      {
        if ((Bits & P.Flag) == P.Flag)
          Result++;
      }
      return Result;
    }
    public int Count(params TItem[] ItemArray)
    {
      if (ItemArray == null || ItemArray.Length == 0)
        return 0;

      var Result = 0;

      foreach (var Item in ItemArray)
      {
        if ((Bits & Item.Flag) == Item.Flag)
          Result++;
      }

      return Result;
    }
    public bool ContainsAny(params TItem[] ItemArray)
    {
      if (ItemArray == null || ItemArray.Length == 0)
        return false;

      foreach (var Item in ItemArray)
      {
        if ((Bits & Item.Flag) == Item.Flag)
          return true;
      }

      return false;
    }
    public bool ContainsAll(params TItem[] ItemArray)
    {
      if (ItemArray == null || ItemArray.Length == 0)
        return false;

      foreach (var Item in ItemArray)
      {
        if ((Bits & Item.Flag) != Item.Flag)
          return false;
      }

      return true;
    }
    public bool Contains(TItem Item)
    {
      return (Bits & Item.Flag) == Item.Flag;
    }
    public void Add(TItem Item)
    {
      Bits |= Item.Flag;
    }
    public void AddSet(TSet BitSet)
    {
      this.Bits |= BitSet.Bits;
    }
    public void Assign(TSet BitSet)
    {
      this.Bits = BitSet.Bits;
    }
    public void AddArray(TItem[] ItemArray)
    {
      foreach (var Item in ItemArray)
        Bits |= Item.Flag;
    }
    public void RemoveRange(IEnumerable<TItem> Items)
    {
      foreach (var Item in Items)
        Bits &= ~Item.Flag;
    }
    public void Remove(TItem Item)
    {
      Bits &= ~Item.Flag;
    }
    public TSet Exclude(TItem Input)
    {
      var Result = NewSet();
      Result.Bits = Bits & ~Input.Flag;
      return Result;
    }
    public TSet Include(TItem Input)
    {
      var Result = NewSet();
      Result.Bits = Bits | Input.Flag;
      return Result;
    }
    public TSet Except(TSet Input)
    {
      var Result = NewSet();
      Result.Bits = Bits & ~Input.Bits;
      return Result;
    }
    public TSet Union(TSet Input)
    {
      var Result = NewSet();
      Result.Bits = Bits | Input.Bits;
      return Result;
    }
    public TSet Intersect(TSet Input)
    {
      var Result = NewSet();
      Result.Bits = Bits & Input.Bits;
      return Result;
    }
    public IEnumerator<TItem> GetEnumerator()
    {
      foreach (var P in ItemList)
      {
        if ((Bits & P.Flag) == P.Flag)
          yield return P;
      }
    }
    public override string ToString()
    {
      var Result = new StringBuilder();

      foreach (var Item in this)
        Result.Append(Item.ToString() + ", ");

      // trailing comma.
      if (Result.Length > 0)
        Result.Remove(Result.Length - 2, 2);

      return Result.ToString();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    static BitmixSet()
    {
      var ItemType = typeof(BitmixItem<TItem>);
      var ItemField = ItemType.GetTypeInfo().GetDeclaredField("List");
      ItemList = (Inv.DistinctList<TItem>)ItemField.GetValue(null);

      Empty = NewSet();

      Universal = NewSet();
      Universal.AddArray(ItemList.ToArray());
    }

    private static TSet NewSet()
    {
      return Activator.CreateInstance<TSet>();
    }

    private static Inv.DistinctList<TItem> ItemList;
    
    private long Bits;
  } 
}