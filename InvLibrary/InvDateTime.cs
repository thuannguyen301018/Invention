﻿/*! 25 !*/
using System;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;
using Inv.Support;

namespace Inv
{
  [XmlSchemaProvider("MySchema")]
  public struct Date : IComparable, IComparable<Inv.Date>, IEquatable<Inv.Date>, IFormattable, IDiscrete, IXmlSerializable
  {
    public Date(DateTime DateTime)
    {
      this.DateTime = System.DateTime.SpecifyKind(DateTime.Date, DateTimeKind.Unspecified);
    }
    public Date(int Year, int Month, int Day)
    {
      this.DateTime = new System.DateTime(Year, Month, Day, 0, 0, 0, DateTimeKind.Unspecified);
    }

    public static Inv.Date Now => new Inv.Date(DateTime.Now); // obsolete?
    public static Inv.Date Today => new Inv.Date(DateTime.Now);
    public static Inv.Date Yesterday => Now.AddDays(-1);
    public static Inv.Date Tomorrow => Now.AddDays(+1);
    public static readonly Inv.Date MinValue = new Inv.Date(DateTime.MinValue);
    public static readonly Inv.Date MaxValue = new Inv.Date(DateTime.MaxValue);

    public int Day
    {
      get { return DateTime.Day; }
    }
    public int Month
    {
      get { return DateTime.Month; }
    }
    public int Year
    {
      get { return DateTime.Year; }
    }
    public int DayOfYear
    {
      get { return DateTime.DayOfYear; }
    }
    public DayOfWeek DayOfWeek
    {
      get { return DateTime.DayOfWeek; }
    }
    public int YearAndMonth
    {
      get { return DateTime.Year * 100 + DateTime.Month; }
    }

    public DateTime ToDateTime()
    {
      return DateTime;
    }
    public int CompareTo(Inv.Date Date)
    {
      return DateTime.CompareTo(Date.DateTime);
    }
    public bool EqualTo(Inv.Date Date)
    {
      return DateTime == Date.DateTime;
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Date?;

      if (Source == null)
        return false;
      else
        return EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return DateTime.GetHashCode();
    }
    public override string ToString()
    {
      return ToString(null);
    }
    public string ToString(IFormatProvider FormatProvider)
    {
      return DateTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, FormatProvider);
    }
    public string ToString(string Format, IFormatProvider FormatProvider = null)
    {
      if (Format == null)
        return ToString(FormatProvider);
      else
        return DateTime.ToString(Format, FormatProvider);
    }
    public string ToRelativeString()
    {
      string Result;
      var CurrentDate = Date.Now;

      if (DateTime.Year != CurrentDate.Year)
        Result = DateTime.ToString("ddd, d MMM yyyy");
      else if (DateTime.Month != CurrentDate.Month)
        Result = DateTime.ToString("ddd, d MMM");
      else
        Result = DateTime.ToString("ddd, d");

      return Result;
    }
    public string ToAbbreviatedString()
    {
      return DateTime.ToString("ddd ") + DateTime.Day.ToOrdinal() + DateTime.ToString(" MMM yyyy");
    }
    public string ToVerboseString()
    {
      return DateTime.ToString("dddd ") + DateTime.Day.ToOrdinal() + DateTime.ToString(" MMMM yyyy");
    }
    public string ToBriefPeriodString(Inv.Date LaterDate)
    {
      string Result = null;

      var PeriodDifference = LaterDate.ToTimePeriodDifference(this);

      if (PeriodDifference.Years > 0)
      {
        Result = PeriodDifference.Years.ToString() + "Y";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Years))
          Result += "+";
      }
      else if (PeriodDifference.Months > 0)
      {
        Result = PeriodDifference.Months.ToString() + "M";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Months))
          Result += "+";
      }
      else if (PeriodDifference.Weeks > 0)
      {
        Result = PeriodDifference.Weeks.ToString() + "W";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Weeks))
          Result += "+";
      }
      else if (PeriodDifference.Days > 0)
      {
        Result = PeriodDifference.Days.ToString() + "D";

        if (PeriodDifference.HasOtherUnit(Inv.TimePeriodUnit.Days))
          Result += "+";
      }

      return Result;
    }
    public Date AddHours(long Value)
    {
      return new Date(DateTime.AddHours(Value));
    }
    public Date AddDays(long Value)
    {
      return new Date(DateTime.AddDays(Value));
    }
    public Date AddDays(double Value)
    {
      return new Date(this.DateTime.AddDays(Value));
    }
    public Date AddWeeks(long Value)
    {
      return AddDays(Value * 7);
    }
    public Date AddMonths(int Value)
    {
      return new Date(this.DateTime.AddMonths(Value));
    }
    public Date AddYears(int Value)
    {
      return new Date(this.DateTime.AddYears(Value));
    }
    public Date AddDatePeriod(TimePeriod Period)
    {
      return AddYears(Period.Years).AddMonths(Period.Months).AddWeeks(Period.Weeks).AddDays(Period.Days).AddHours(Period.Hours);
    }
    public DateTimeOffset AddDateTimePeriod(TimePeriod Period)
    {
      return AddDatePeriod(Period) + new TimeSpan(0, Period.Hours, Period.Minutes, Period.Seconds, Period.Milliseconds);
    }
    public bool IsBetween(Date From, Date Until)
    {
      return this >= From && this <= Until;
    }
    public bool IsWeekend()
    {
      var DayOfWeek = DateTime.DayOfWeek;

      return DayOfWeek == DayOfWeek.Saturday || DayOfWeek == DayOfWeek.Sunday;
    }
    public bool IsWeekday()
    {
      return !IsWeekend();
    }
    public Date StartOfFinancialYear()
    {
      var Target = new Date(Year, 7, 1);

      if (this < Target)
        Target = new Date(Year - 1, 7, 1);

      return Target;
    }
    public Date EndOfFinancialYear()
    {
      var Target = new Date(Year, 06, 30);

      if (this > Target)
        Target = new Date(Year + 1, 06, 30);

      return Target;
    }
    public Date StartOfWeek(DayOfWeek StartOfWeek)
    {
      var Difference = ToDateTime().DayOfWeek - StartOfWeek;

      if (Difference < 0)
        Difference += 7;

      return AddDays(-1 * Difference);
    }
    public Date StartOfWeek()
    {
      return StartOfWeek(DayOfWeekHelper.FirstDayOfWeek);
    }
    public Date EndOfWeek(DayOfWeek EndOfWeek)
    {
      var Target = (int)EndOfWeek;

      if (Target < (int)ToDateTime().DayOfWeek)
        Target += 7;

      return AddDays(Target - (int)ToDateTime().DayOfWeek);
    }
    public Date EndOfWeek()
    {
      return EndOfWeek(EnumHelper.Previous(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
    }
    public Date StartOfMonth()
    {
      return new Date(Year, Month, 1);
    }
    public Date EndOfMonth()
    {
      return new Date(Year, Month, DateTime.DaysInMonth(Year, Month));
    }
    public Date StartOfQuarter()
    {
      return new Date(DateTime.StartOfQuarter());
    }
    public Date EndOfQuarter()
    {
      return new Date(DateTime.EndOfQuarter());
    }
    public Date StartOfYear()
    {
      return new Date(Year, 1, 1);
    }
    public Date EndOfYear()
    {
      return new Date(Year, 12, 31);
    }
    public DateTime StartOfDay()
    {
      return ToDateTime().StartOfDay();
    }
    public DateTime EndOfDay()
    {
      return ToDateTime().EndOfDay();
    }
    public Date NextDayOfWeek(DayOfWeek DayOfWeek)
    {
      var DayOfWeekSeries = DayOfWeekHelper.DayOfWeekSeries().ToDistinctList();

      var CurrentIndex = DayOfWeekSeries.IndexOf(DateTime.DayOfWeek);
      var FindIndex = DayOfWeekSeries.IndexOf(DayOfWeek);

      return AddDays(CurrentIndex < FindIndex ? FindIndex - CurrentIndex : 7 - (CurrentIndex - FindIndex));
    }
    public Date NextDayOfWeek(IEnumerable<DayOfWeek> DayOfWeeks)
    {
      var Attempt = AddDays(1);
      while (!DayOfWeeks.Contains(Attempt.DayOfWeek))
        Attempt = Attempt.AddDays(1);

      return Attempt;
    }
    public Date PreviousDayOfWeek(DayOfWeek DayOfWeek)
    {
      var DayOfWeekSeries = DayOfWeekHelper.DayOfWeekSeries().ToDistinctList();

      var CurrentIndex = DayOfWeekSeries.IndexOf(DateTime.DayOfWeek);
      var FindIndex = DayOfWeekSeries.IndexOf(DayOfWeek);

      return AddDays(CurrentIndex > FindIndex ? FindIndex - CurrentIndex : (FindIndex - CurrentIndex) - 7);
    }
    public Date PreviousDayOfWeek(IEnumerable<DayOfWeek> DayOfWeeks)
    {
      var Attempt = AddDays(-1);
      while (!DayOfWeeks.Contains(Attempt.DayOfWeek))
        Attempt = Attempt.AddDays(-1);

      return Attempt;
    }
    public int CountDaysInRange(Date StartDate)
    {
      return (int)Math.Floor(ToTimeSpanDifference(StartDate).TotalDays) + 1;
    }
    public bool InClosedIntervalAllowingNulls(Date? Left, Date? Right)
    {
      if (Left == null && Right == null)
        return true;
      else if (Left == null)
        return this < Right;
      else if (Right == null)
        return this > Left;
      else
        return Left < this && this < Right;
    }
    public bool InLeftOpenIntervalAllowingNulls(Date? Left, Date? Right)
    {
      if (Left == null && Right == null)
        return true;
      else if (Left == null)
        return this < Right;
      else if (Right == null)
        return this >= Left;
      else
        return Left <= this && this < Right;
    }
    public bool InRightOpenIntervalAllowingNulls(Date? Left, Date? Right)
    {
      if (Left == null && Right == null)
        return true;
      else if (Left == null)
        return this <= Right;
      else if (Right == null)
        return this > Left;
      else
        return Left < this && this <= Right;
    }
    public bool BetweenInclusiveAllowingNulls(Date? Left, Date? Right)
    {
      if (Left == null && Right == null)
        return true;
      else if (Left == null)
        return this <= Right;
      else if (Right == null)
        return this >= Left;
      else
        return Left <= this && this <= Right;
    }
    public IEnumerable<Date> ToDateSeries(Date LastDate)
    {
      if (this > LastDate)
        yield break;

      var CurrentDate = this;
      do
      {
        yield return CurrentDate;

        CurrentDate = CurrentDate.AddDays(1);
      } while (CurrentDate <= LastDate);
    }
    public TimePeriod ToTimePeriodDifference(Date Date)
    {
      return TimePeriod.FromDateTimeDifference(this.DateTime, Date.DateTime);
    }
    public TimeSpan ToTimeSpanDifference(Date StartDate)
    {
      return this.DateTime - StartDate.DateTime;
    }
    public Date NextAnniversaryDate(Date RelativeDate, TimePeriod TimePeriod)
    {
      var NextDate = this;
      var Count = 1;
      do
      {
        NextDate = this.AddDatePeriod(TimePeriod * Count);
        Count++;
      }
      while (NextDate < RelativeDate);

      return NextDate;
    }
    public bool IsAnniversaryOf(Date AnniversaryDate, TimePeriod TimePeriod)
    {
      if (this <= AnniversaryDate)
        return false;

      return AnniversaryDate.NextAnniversaryDate(this, TimePeriod) == this;
    }
    public Date NextEndOfPeriodDate(Date RelativeDate, TimePeriod TimePeriod)
    {
      var NextDate = this;
      var Count = 1;
      do
      {
        NextDate = this.AddDatePeriod(TimePeriod * Count).AddDays(-1);
        Count++;
      }
      while (NextDate < RelativeDate);

      return NextDate;
    }
    public bool IsEndOfPeriodAnniversaryOf(Date PeriodFromDate, TimePeriod TimePeriod)
    {
      if (this <= PeriodFromDate)
        return false;

      return PeriodFromDate.NextEndOfPeriodDate(this, TimePeriod) == this;
    }
    public long ToDateTimeBinary()
    {
      return DateTime.ToBinary();
    }

    public static Date Min(Date d1, Date d2)
    {
      if (d1 <= d2)
        return d1;
      else
        return d2;
    }
    public static Date Max(Date d1, Date d2)
    {
      if (d1 >= d2)
        return d1;
      else
        return d2;
    }
    public static bool TryParse(string Input, out Date Date)
    {
      var Result = DateTime.TryParse(Input, out DateTime Value);

      if (Result)
        Date = new Date(Value);
      else
        Date = Date.MinValue;

      return Result;
    }
    public static Date Parse(string Text)
    {
      if (!TryParse(Text, out Date Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static bool TryParseExact(string Input, string Format, out Date Date)
    {
      var Result = DateTime.TryParseExact(Input, Format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime Value);

      if (Result)
        Date = new Date(Value);
      else
        Date = Date.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string[] FormatArray, out Date Date)
    {
      var Result = DateTime.TryParseExact(Input, FormatArray, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime Value);

      if (Result)
        Date = new Date(Value);
      else
        Date = Date.MinValue;

      return Result;
    }
    public static Date ParseExact(string Text, string Format)
    {
      if (!TryParseExact(Text, Format, out Date Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static Date ParseExact(string Text, string[] FormatArray)
    {
      if (!TryParseExact(Text, FormatArray, out Date Result))
        throw new Exception("Invalid date format: " + Text);

      return Result;
    }
    public static bool operator >(Date d1, Date d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator >(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator >(DateTimeOffset d1, Date d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator <(Date d1, Date d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator <(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator <(DateTimeOffset d1, Date d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator ==(Date d1, Date d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator ==(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator !=(Date d1, Date d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator !=(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator >=(Date d1, Date d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator >=(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator >=(DateTimeOffset d1, Date d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator <=(Date d1, Date d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static bool operator <=(Date d1, DateTimeOffset d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static bool operator <=(DateTimeOffset d1, Date d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static DateTime operator +(Date d1, TimeSpan t2)
    {
      return d1.DateTime + t2;
    }
    public static DateTime operator +(Date d, TimePeriod p)
    {
      var Result = d;

      if (p.Years > 0)
        Result = Result.AddYears(p.Years);

      if (p.Months > 0)
        Result = Result.AddMonths(p.Months);

      if (p.Weeks > 0)
        Result = Result.AddWeeks(p.Weeks);

      if (p.Days > 0)
        Result = Result.AddDays(p.Days);

      return Result.DateTime + new TimeSpan(p.Hours, p.Minutes, p.Seconds, p.Milliseconds);
    }
    public static DateTime operator +(Date d, Time p)
    {
      return d.DateTime + p.ToTimeSpan();
    }
    public static string FormatInclusiveDateDifference(Date From, Date Until)
    {
      return (Until.AddDays(1).ToTimePeriodDifference(From)).ToStringComprehensive();
    }

    int IComparable<Date>.CompareTo(Date other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Date>.Equals(Date other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Date)obj);
    }
    string IFormattable.ToString(string format, IFormatProvider formatProvider)
    {
      if (format == null && formatProvider == null)
        return ToString();
      else
        return DateTime.ToString(format, formatProvider);
    }
    object IDiscrete.PreviousValue()
    {
      return AddDays(-1);
    }
    object IDiscrete.NextValue()
    {
      return AddDays(+1);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("date", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = reader.ReadInnerXml();

      DateTime = DateTime.ParseExact(Result, "yyyy-MM-dd", null, System.Globalization.DateTimeStyles.None);
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(DateTime.ToString("yyyy-MM-dd"));
    }

    private /*readonly*/ DateTime DateTime;
  }

  public sealed class DateTimeRange
  {
    public DateTimeRange(Date Date, Time From, Time Until)
    {
      this.From = Date + From;
      this.Until = Date + Until;
    }
    public DateTimeRange(DateTime? From, DateTime? Until)
    {
      this.From = From;
      this.Until = Until;
    }

    public DateTime? From { get; private set; }
    public DateTime? Until { get; private set; }

    public int CompareTo(DateTimeRange Value)
    {
      var Result = From.CompareTo(Value.From);

      if (Result == 0)
        Result = Until.CompareTo(Value.Until);

      return Result;
    }
    public bool EqualTo(DateTimeRange Value)
    {
      return From == Value.From && Until == Value.Until;
    }

    public override string ToString()
    {
      return ToString(false, true, null);
    }
    public string ToString(bool FormatLocalTime, bool IncludeTime, TimeSpanRange TimeSpanRange)
    {
      var StringBuilder = new StringBuilder();

      if (From == null && Until == null)
      {
        StringBuilder.Append("-");
      }
      else if (From == null)
      {
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        StringBuilder.Append("Until ");

        if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
          StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

        StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
      }
      else if (Until == null)
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;

        StringBuilder.Append("From ");

        if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.End))
          StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

        StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
      }
      else
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        if (FromDateTime.Date == UntilDateTime.Date)
        {
          if (IncludeTime && ((TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin || UntilDateTime.TimeOfDay != TimeSpanRange.End)))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " - " + UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
        }
        else
        {
          if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d "));

          if (FromDateTime.Month != UntilDateTime.Month)
            StringBuilder.Append(FromDateTime.ToString("MMM "));

          if (FromDateTime.Year != UntilDateTime.Year)
            StringBuilder.Append(FromDateTime.ToString("yyy "));

          StringBuilder.Append("- ");

          if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
            StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
        }
      }

      return StringBuilder.ToString();
    }

    public static bool TryParse(string Value, out DateTimeRange Range)
    {
      // TODO: not yet implemented.

      Range = new DateTimeRange(null, null);

      return false;
    }
  }

  public sealed class TimestampRange
  {
    public TimestampRange(Date Date, Time From, Time Until)
    {
      this.From = new DateTimeOffset(Date.ToDateTime() + From.ToTimeSpan());
      this.Until = new DateTimeOffset(Date.ToDateTime() + Until.ToTimeSpan());
    }
    public TimestampRange(DateTimeOffset? From, DateTimeOffset? Until)
    {
      this.From = From;
      this.Until = Until;
    }

    public DateTimeOffset? From { get; private set; }
    public DateTimeOffset? Until { get; private set; }

    public int CompareTo(TimestampRange Value)
    {
      var Result = From.CompareTo(Value.From);

      if (Result == 0)
        Result = Until.CompareTo(Value.Until);

      return Result;
    }
    public bool EqualTo(TimestampRange Value)
    {
      return From == Value.From && Until == Value.Until;
    }

    public override string ToString()
    {
      return ToString(false, true, false, null);
    }
    public string ToString(bool FormatLocalTime, bool IncludeTime, bool IncludeOffset, TimeSpanRange TimeSpanRange)
    {
      var StringBuilder = new StringBuilder();

      if (From == null && Until == null)
      {
        StringBuilder.Append("-");
      }
      else if (From == null)
      {
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        StringBuilder.Append("Until ");

        if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
        {
          StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

          if (IncludeOffset && !FormatLocalTime)
            StringBuilder.Append(string.Format(" ({0:%z}) ", UntilDateTime));
        }

        StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
      }
      else if (Until == null)
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;

        StringBuilder.Append("From ");

        if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.End))
        {
          StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          if (IncludeOffset && !FormatLocalTime)
            StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
        }

        StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
      }
      else
      {
        var FromDateTime = FormatLocalTime ? From.Value.ToLocalTime() : From.Value;
        var UntilDateTime = FormatLocalTime ? Until.Value.ToLocalTime() : Until.Value;

        if (FromDateTime.Date == UntilDateTime.Date)
        {
          if (IncludeTime && ((TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin || UntilDateTime.TimeOfDay != TimeSpanRange.End)))
          {
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " - " + UntilDateTime.ToString("h:mmtt").ToLower() + " ");

            if (IncludeOffset && !FormatLocalTime)
              StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
          }

          StringBuilder.Append(FromDateTime.ToString("ddd d MMM yyy"));
        }
        else
        {
          if (IncludeTime && (TimeSpanRange == null || FromDateTime.TimeOfDay != TimeSpanRange.Begin))
            StringBuilder.Append(FromDateTime.ToString("h:mmtt").ToLower() + " ");

          StringBuilder.Append(FromDateTime.ToString("ddd d "));

          if (FromDateTime.Month != UntilDateTime.Month)
            StringBuilder.Append(FromDateTime.ToString("MMM "));

          if (FromDateTime.Year != UntilDateTime.Year)
            StringBuilder.Append(FromDateTime.ToString("yyy "));

          StringBuilder.Append("- ");

          if (IncludeTime && (TimeSpanRange == null || UntilDateTime.TimeOfDay != TimeSpanRange.End))
          {
            StringBuilder.Append(UntilDateTime.ToString("h:mmtt").ToLower() + " ");

            if (IncludeOffset && !FormatLocalTime)
              StringBuilder.Append(string.Format(" ({0:%z}) ", FromDateTime));
          }

          StringBuilder.Append(UntilDateTime.ToString("ddd d MMM yyy"));
        }
      }

      return StringBuilder.ToString();
    }

    public static bool TryParse(string Value, out TimestampRange Range)
    {
      // TODO: not yet implemented.

      Range = new TimestampRange(null, null);

      return false;
    }
  }

  [XmlSchemaProvider("MySchema")]
  public struct Time : IComparable, IComparable<Time>, IEquatable<Time>, IFormattable, IDiscrete, IXmlSerializable
  {
    public Time(DateTime DateTime)
    {
      // The year has been changed from 1 to 2000 to prevent overflow exceptions when adding (reasonable quantities of) negative units of time.
      this.DateTime = new System.DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified) + DateTime.TimeOfDay;
    }
    public Time(TimeSpan TimeSpan)
    {
      this.DateTime = new System.DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Unspecified) + TimeSpan;
    }
    public Time(int Hour, int Minute, int Second = 0, int Millisecond = 0)
    {
      this.DateTime = new System.DateTime(2000, 1, 1, Hour, Minute, Second, Millisecond, DateTimeKind.Unspecified);
    }

    public static Time Now
    {
      get { return new Time(DateTime.Now); }
    }
    public static readonly Time MinValue = new Time(DateTime.MinValue);
    public static readonly Time MaxValue = new Time(DateTime.MaxValue);
    public static readonly Time Midday = new Time(12, 00);
    public static readonly Time Midnight = new Time(00, 00);

    public int Hour
    {
      get { return DateTime.Hour; }
    }
    public int Minute
    {
      get { return DateTime.Minute; }
    }
    public int Second
    {
      get { return DateTime.Second; }
    }
    public int Millisecond
    {
      get { return DateTime.Millisecond; }
    }

    public DateTime ToDateTime()
    {
      return DateTime;
    }
    public TimeSpan ToTimeSpan()
    {
      return DateTime.TimeOfDay;
    }
    public int CompareTo(Time Value)
    {
      return DateTime.CompareTo(Value.DateTime);
    }
    public bool EqualTo(Time Value)
    {
      return DateTime == Value.DateTime;
    }
    public int CompareTo(object obj)
    {
      return CompareTo((Time)obj);
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Time?;

      if (Source == null)
        return false;
      else
        return EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      return DateTime.GetHashCode();
    }
    public override string ToString()
    {
      return DateTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
    }
    public string ToLongString()
    {
      return DateTime.ToString(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.LongTimePattern);
    }
    public string ToString(string Format)
    {
      if (Format == null)
        return ToString();
      else
        return DateTime.ToString(Format);
    }
    public Time AddHours(double value)
    {
      return new Time(this.DateTime.AddHours(value));
    }
    public Time AddMinutes(double value)
    {
      return new Time(this.DateTime.AddMinutes(value));
    }
    public Time AddSeconds(double value)
    {
      return new Time(this.DateTime.AddSeconds(value));
    }
    public Time AddMilliseconds(double value)
    {
      return new Time(this.DateTime.AddMilliseconds(value));
    }
    public Time AddPeriod(TimePeriod Period)
    {
      return AddHours(Period.Hours).AddMinutes(Period.Minutes).AddSeconds(Period.Seconds).AddMilliseconds(Period.Milliseconds);
    }
    public bool IsBetween(Time From, Time Until)
    {
      if (From <= Until)
        return this >= From && this <= Until;
      else
        return this >= From || this <= Until;
    }
    public Time RoundUp(TimeSpan ts)
    {
      return new Time(DateTime.RoundUp(ts));
    }

    public static Time Min(Time t1, Time t2)
    {
      if (t1 <= t2)
        return t1;
      else
        return t2;
    }
    public static Time Max(Time t1, Time t2)
    {
      if (t1 >= t2)
        return t1;
      else
        return t2;
    }
    public static Time Parse(string Text)
    {
      if (!TryParse(Text, out Time Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static bool TryParse(string Input, out Time Time)
    {
      var Result = DateTime.TryParse(Input, out DateTime Value);

      if (Result)
        Time = new Time(Value);
      else
        Time = Time.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string Format, out Time Time)
    {
      var Result = DateTime.TryParseExact(Input, Format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime Value);

      if (Result)
        Time = new Time(Value);
      else
        Time = Time.MinValue;

      return Result;
    }
    public static bool TryParseExact(string Input, string[] FormatArray, out Time Time)
    {
      var Result = DateTime.TryParseExact(Input, FormatArray, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out DateTime Value);

      if (Result)
        Time = new Time(Value);
      else
        Time = Time.MinValue;

      return Result;
    }
    public static Time ParseExact(string Text, string Format)
    {
      if (!TryParseExact(Text, Format, out Time Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static Time ParseExact(string Text, string[] FormatArray)
    {
      if (!TryParseExact(Text, FormatArray, out Time Result))
        throw new Exception("Invalid time format: " + Text);

      return Result;
    }
    public static Time? ParseOrDefault(string Value, Time? Default = null)
    {
      if (Time.TryParse(Value, out Time Result))
        return Result;
      else
        return Default;
    }
    public static bool operator >(Time d1, Time d2)
    {
      return d1.DateTime > d2.DateTime;
    }
    public static bool operator <(Time d1, Time d2)
    {
      return d1.DateTime < d2.DateTime;
    }
    public static bool operator ==(Time d1, Time d2)
    {
      return d1.DateTime == d2.DateTime;
    }
    public static bool operator !=(Time d1, Time d2)
    {
      return d1.DateTime != d2.DateTime;
    }
    public static bool operator >=(Time d1, Time d2)
    {
      return d1.DateTime >= d2.DateTime;
    }
    public static bool operator <=(Time d1, Time d2)
    {
      return d1.DateTime <= d2.DateTime;
    }
    public static Time operator -(Time d, TimeSpan t)
    {
      return new Time(d.DateTime - t);
    }
    public static TimeSpan operator -(Time d1, Time d2)
    {
      return d1.DateTime - d2.DateTime;
    }
    public static Time operator +(Time d, TimeSpan t)
    {
      return new Time(d.DateTime + t);
    }

    int IComparable<Time>.CompareTo(Time other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Time>.Equals(Time other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Time)obj);
    }
    string IFormattable.ToString(string format, IFormatProvider formatProvider)
    {
      if (format == null && formatProvider == null)
        return ToString();
      else
        return DateTime.ToString(format, formatProvider);
    }
    object IDiscrete.PreviousValue()
    {
      if (Hour == 0 && Minute == 0)
        return this;

      return AddMinutes(-1);
    }
    object IDiscrete.NextValue()
    {
      if (Hour == 23 && Minute == 59)
        return this;

      return AddMinutes(+1);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("time", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = reader.ReadInnerXml();
      DateTime = new DateTime(2000, 1, 1) + DateTime.ParseExact(Result, new [] { "HH:mm:ss.fffffff", "HH:mm:ss.ffffff" }, null, System.Globalization.DateTimeStyles.None).TimeOfDay;
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(DateTime.ToString("HH:mm:ss.fffffff"));
    }

    private /*readonly*/ DateTime DateTime;
  }

  // Note: TimePeriod cannot implement the IComparable or IComparable<T> interfaces due to it being a structure that operates on relative dates.
  //  e.g. "1 month" is greater than "4 weeks" when starting from any date not inside in February, but from within February the two are the same.
  //  To compare two time periods, add them both to a specific consistent date and then compare the resultant dates.
  [XmlSchemaProvider("MySchema")]
  public struct TimePeriod : IEquatable<TimePeriod>, IXmlSerializable
  {
    public static readonly TimePeriod Zero = new TimePeriod();

    public TimePeriod(int years = 0, int months = 0, int weeks = 0, int days = 0, int hours = 0, int minutes = 0, int seconds = 0, int milliseconds = 0)
    {
      this.YearsField = years;
      this.MonthsField = months;
      this.WeeksField = weeks;
      this.DaysField = days;
      this.HoursField = hours;
      this.MinutesField = minutes;
      this.SecondsField = seconds;
      this.MillisecondsField = milliseconds;
    }
    public TimePeriod(TimePeriodUnit periodUnit, int value)
      : this()
    {
      switch (periodUnit)
      {
        case TimePeriodUnit.Years: YearsField = value; break;
        case TimePeriodUnit.Months: MonthsField = value; break;
        case TimePeriodUnit.Weeks: WeeksField = value; break;
        case TimePeriodUnit.Days: DaysField = value; break;
        case TimePeriodUnit.Hours: HoursField = value; break;
        case TimePeriodUnit.Minutes: MinutesField = value; break;
        case TimePeriodUnit.Seconds: SecondsField = value; break;
        case TimePeriodUnit.Milliseconds: MillisecondsField = value; break;
        default:
          throw new Exception("PeriodUnits not handled.");
      }
    }

    public int Years
    {
      get { return YearsField; }
    }
    public int Months
    {
      get { return MonthsField; }
    }
    public int Weeks
    {
      get { return WeeksField; }
    }
    public int Days
    {
      get { return DaysField; }
    }
    public int Hours
    {
      get { return HoursField; }
    }
    public int Minutes
    {
      get { return MinutesField; }
    }
    public int Seconds
    {
      get { return SecondsField; }
    }
    public int Milliseconds
    {
      get { return MillisecondsField; }
    }

    public string ToStringRelative(bool IsInPast)
    {
      var FormatTimePeriod = IsInPast ? this : -this;

      if (FormatTimePeriod.Years > 1)
      {
        var Result = "OVER " + FormatTimePeriod.Years.ToString() + " YEAR".Plural(FormatTimePeriod.Years);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Years == 1)
      {
        var Result = "1 YEAR";

        if (FormatTimePeriod.Months > 0)
          Result += " AND " + FormatTimePeriod.Months.ToString() + " MONTH".Plural(FormatTimePeriod.Months);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Months > 0)
      {
        var Result = FormatTimePeriod.Months.ToString() + " MONTH".Plural(FormatTimePeriod.Months);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Weeks > 0)
      {
        var Result = FormatTimePeriod.Weeks.ToString() + " WEEK".Plural(FormatTimePeriod.Weeks);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Days == 1)
      {
        return IsInPast ? "YESTERDAY" : "TOMORROW";
      }
      else if (FormatTimePeriod.Days > 0)
      {
        var Result = FormatTimePeriod.Days.ToString() + " DAY".Plural(FormatTimePeriod.Days);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
      else if (FormatTimePeriod.Hours == 0 && FormatTimePeriod.Minutes == 0)
      {
        return "JUST NOW";
      }
      else
      {
        var Result = "";

        if (FormatTimePeriod.Hours > 0)
        {
          Result += FormatTimePeriod.Hours.ToString() + " HOUR".Plural(FormatTimePeriod.Hours);

          if (FormatTimePeriod.Minutes > 0)
            Result += " AND ";
        }

        if (FormatTimePeriod.Minutes > 0)
          Result += FormatTimePeriod.Minutes.ToString() + " MINUTE".Plural(FormatTimePeriod.Minutes);

        if (IsInPast)
          Result += " AGO";

        return Result;
      }
    }
    public string ToStringComprehensive()
    {
      var TimePeriod = this;

      return EnumHelper.GetEnumerable<TimePeriodUnit>().Where(U => TimePeriod.HasUnit(U)).Select(U =>
      {
        var Value = TimePeriod.ValueByUnit(U);

        return Value.ToString() + " " + U.ToString().ToLower().Singular(Value);
      }).AsSeparatedText(", ", " and ");
    }
    public string ToStringAsAnniversary()
    {
      var TimePeriod = this;

      return EnumHelper.GetEnumerable<TimePeriodUnit>().Where(U => TimePeriod.HasUnit(U)).Select(U =>
      {
        var Value = TimePeriod.ValueByUnit(U);

        if (Value > 1)
          return string.Format("{0} {1}", Value, U.FormatAdverb());
        else if (Value == 1)
          return U.FormatAdverb();
        else
          return string.Empty;
      }).AsSeparatedText(", ", " and ");
    }
    public override string ToString()
    {
      // eg. "P5Y2M10DT15H" is a period of five years, two months, 10 days, and 15 hours.

      var Result = new StringBuilder("P");

      if (YearsField != 0L)
        Result.Append(YearsField + "Y");

      if (MonthsField != 0L)
        Result.Append(MonthsField + "M");

      if (WeeksField != 0L)
        Result.Append(WeeksField + "W");

      if (DaysField != 0)
        Result.Append(DaysField + "D");

      if (HoursField != 0 || MinutesField != 0 || SecondsField != 0 || MillisecondsField != 0)
      {
        Result.Append("T");

        if (HoursField != 0)
          Result.Append(HoursField + "H");

        if (MinutesField != 0)
          Result.Append(MinutesField + "M");

        if (SecondsField != 0)
          Result.Append(SecondsField + "S");

        if (MillisecondsField != 0)
          Result.Append(MillisecondsField + "s");
      }

      return Result.ToString();
    }
    public override bool Equals(object other)
    {
      return EqualTo((TimePeriod)other);
    }
    public override int GetHashCode()
    {
      var Result = 17;

      unchecked
      {
        Result = (Result * 37) + YearsField.GetHashCode();
        Result = (Result * 37) + MonthsField.GetHashCode();
        Result = (Result * 37) + WeeksField.GetHashCode();
        Result = (Result * 37) + DaysField.GetHashCode();
        Result = (Result * 37) + HoursField.GetHashCode();
        Result = (Result * 37) + MinutesField.GetHashCode();
        Result = (Result * 37) + SecondsField.GetHashCode();
        Result = (Result * 37) + MillisecondsField.GetHashCode();
      }

      return Result;
    }
    public bool EqualTo(TimePeriod Value)
    {
      return YearsField == Value.YearsField &&
          MonthsField == Value.MonthsField &&
          WeeksField == Value.WeeksField &&
          DaysField == Value.DaysField &&
          HoursField == Value.HoursField &&
          MinutesField == Value.MinutesField &&
          SecondsField == Value.SecondsField &&
          MillisecondsField == Value.MillisecondsField;
    }
    public bool HasUnit(TimePeriodUnit Unit)
    {
      switch (Unit)
      {
        case TimePeriodUnit.Days:
          return DaysField > 0;

        case TimePeriodUnit.Hours:
          return HoursField > 0;

        case TimePeriodUnit.Milliseconds:
          return MillisecondsField > 0;

        case TimePeriodUnit.Minutes:
          return MinutesField > 0;

        case TimePeriodUnit.Months:
          return MonthsField > 0;

        case TimePeriodUnit.Seconds:
          return SecondsField > 0;

        case TimePeriodUnit.Weeks:
          return WeeksField > 0;

        case TimePeriodUnit.Years:
          return YearsField > 0;

        default:
          throw new Exception("Unexpected TimePeriodUnit");
      }
    }
    public bool HasOtherUnit(TimePeriodUnit Unit)
    {
      var ThisPeriod = this;

      return EnumHelper.GetEnumerable<TimePeriodUnit>().Except(Unit).Any(U => ThisPeriod.HasUnit(U));
    }
    public long ValueByUnit(TimePeriodUnit Unit)
    {
      switch (Unit)
      {
        case TimePeriodUnit.Days:
          return DaysField;

        case TimePeriodUnit.Hours:
          return HoursField;

        case TimePeriodUnit.Milliseconds:
          return MillisecondsField;

        case TimePeriodUnit.Minutes:
          return MinutesField;

        case TimePeriodUnit.Months:
          return MonthsField;

        case TimePeriodUnit.Seconds:
          return SecondsField;

        case TimePeriodUnit.Weeks:
          return WeeksField;

        case TimePeriodUnit.Years:
          return YearsField;

        default:
          throw new Exception("Unexpected TimePeriodUnit");
      }
    }
    public Date NextAnniversaryDateInclusive(Date AnniversaryDate, Date RelativeDate)
    {
      var NextDate = AnniversaryDate;

      while (NextDate < RelativeDate)
        NextDate = NextDate.AddDatePeriod(this);

      return NextDate;
    }

    public static bool TryParse(string Text, out TimePeriod Period)
    {
      // TODO: this could be stricter in terms of duplicate letters and enforcing the letter order.

      var Result = true;
      var Years = 0;
      var Months = 0;
      var Weeks = 0;
      var Days = 0;
      var Hours = 0;
      var Minutes = 0;
      var Seconds = 0;
      var Milliseconds = 0;

      var CharEnumerator = Text.GetEnumerable().GetEnumerator();
      var Continue = CharEnumerator.MoveNext();

      if (Continue && CharEnumerator.Current == 'P')
      {
        Continue = CharEnumerator.MoveNext();

        while (Continue && CharEnumerator.Current != 'T')
        {
          if (!TryParseField(CharEnumerator, out int Number, out char Letter))
          {
            Result = false;
            break;
          }

          switch (Letter)
          {
            case 'Y':
              Years = Number;
              break;

            case 'M':
              Months = Number;
              break;

            case 'W':
              Weeks = Number;
              break;

            case 'D':
              Days = Number;
              break;

            default:
              Result = false;
              break;
          }

          Continue = CharEnumerator.MoveNext();
        }

        if (Result && Continue && CharEnumerator.Current == 'T')
        {
          while (CharEnumerator.MoveNext())
          {
            if (!TryParseField(CharEnumerator, out int Number, out char Letter))
            {
              Result = false;
              break;
            }

            switch (Letter)
            {
              case 'H':
                Hours = Number;
                break;

              case 'M':
                Minutes = Number;
                break;

              case 'S':
                Seconds = Number;
                break;

              case 's':
                Milliseconds = Number;
                break;

              default:
                Result = false;
                break;
            }
          }
        }
      }
      else
      {
        Result = false;
      }

      if (Result)
        Period = new TimePeriod(Years, Months, Weeks, Days, Hours, Minutes, Seconds, Milliseconds);
      else
        Period = Zero;

      return Result;
    }
    public static TimePeriod Parse(string Text)
    {
      if (!TryParse(Text, out TimePeriod Result))
        throw new Exception("Invalid time period format: " + Text);

      return Result;
    }
    public static TimePeriod FromYears(int years)
    {
      return new TimePeriod(TimePeriodUnit.Years, years);
    }
    public static TimePeriod FromWeeks(int weeks)
    {
      return new TimePeriod(TimePeriodUnit.Weeks, weeks);
    }
    public static TimePeriod FromMonths(int months)
    {
      return new TimePeriod(TimePeriodUnit.Months, months);
    }
    public static TimePeriod FromDays(int days)
    {
      return new TimePeriod(TimePeriodUnit.Days, days);
    }
    public static TimePeriod FromHours(int hours)
    {
      return new TimePeriod(TimePeriodUnit.Hours, hours);
    }
    public static TimePeriod FromMinutes(int minutes)
    {
      return new TimePeriod(TimePeriodUnit.Minutes, minutes);
    }
    public static TimePeriod FromSeconds(int seconds)
    {
      return new TimePeriod(TimePeriodUnit.Seconds, seconds);
    }
    public static TimePeriod FromMilliseconds(int milliseconds)
    {
      return new TimePeriod(TimePeriodUnit.Milliseconds, milliseconds);
    }
    public static TimePeriod FromDateDifference(Date d1, Date d2)
    {
      return TimePeriod.FromDateTimeDifference(d1.ToDateTime(), d2.ToDateTime());
    }
    public static TimePeriod FromDateTimeDifference(DateTime d1, DateTime d2)
    {
      var Negate = 1;

      if (d1 < d2)
      {
        var d3 = d2;
        d2 = d1;
        d1 = d3;
        Negate = -1;
      }

      var years = 0;
      var months = 0;
      var weeks = 0;
      var days = 0;
      var hours = 0;
      var minutes = 0;
      var seconds = 0;
      var milliseconds = 0;

      while (d1 > d2)
      {
        d2 = d2.AddYears(1);
        years++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddYears(-1);
        years--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddMonths(1);
        months++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddMonths(-1);
        months--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddDays(7);
        weeks++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddDays(-7);
        weeks--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddDays(1);
        days++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddDays(-1);
        days--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddHours(1);
        hours++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddHours(-1);
        hours--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddMinutes(1);
        minutes++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddMinutes(-1);
        minutes--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddSeconds(1);
        seconds++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddSeconds(-1);
        seconds--;
      }

      while (d1 > d2)
      {
        d2 = d2.AddMilliseconds(1);
        milliseconds++;
      }

      if (d2 > d1)
      {
        d2 = d2.AddMilliseconds(-1);
        milliseconds--;
      }

      return new TimePeriod(years * Negate, months * Negate, weeks * Negate, days * Negate, hours * Negate, minutes * Negate, seconds * Negate, milliseconds * Negate);
    }
    public static TimePeriod operator -(TimePeriod value)
    {
      return new TimePeriod(-value.Years, -value.Months, -value.Weeks, -value.Days, -value.Hours, -value.Minutes, -value.Seconds, -value.Milliseconds);
    }
    public static TimePeriod operator +(TimePeriod value)
    {
      return value;
    }
    public static TimePeriod operator +(TimePeriod left, TimePeriod right)
    {
      return new TimePeriod(left.Years + right.Years, left.Months + right.Months, left.Weeks + right.Weeks, left.Days + right.Days, left.Hours + right.Hours, left.Minutes + right.Minutes, left.Seconds + right.Seconds, left.Milliseconds + right.Milliseconds);
    }
    public static DateTimeOffset operator +(DateTimeOffset left, TimePeriod right)
    {
      return left.AddYears(right.Years).AddMonths(right.Months).AddDays((right.Weeks * 7) + right.Days) + new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTimeOffset operator -(DateTimeOffset left, TimePeriod right)
    {
      return left.AddYears(-right.Years).AddMonths(-right.Months).AddDays((-right.Weeks * 7) - right.Days) - new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTime operator +(DateTime left, TimePeriod right)
    {
      return left.AddYears(right.Years).AddMonths(right.Months).AddDays((right.Weeks * 7) + right.Days) + new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static DateTime operator -(DateTime left, TimePeriod right)
    {
      return left.AddYears(-right.Years).AddMonths(-right.Months).AddDays((-right.Weeks * 7) - right.Days) - new TimeSpan(0, right.Hours, right.Minutes, right.Seconds, right.Milliseconds);
    }
    public static TimePeriod operator -(TimePeriod left, TimePeriod right)
    {
      return new TimePeriod(left.Years - right.Years, left.Months - right.Months, left.Weeks - right.Weeks, left.Days - right.Days, left.Hours - right.Hours, left.Minutes - right.Minutes, left.Seconds - right.Seconds, left.Milliseconds - right.Milliseconds);
    }
    public static TimePeriod operator /(TimePeriod left, int right)
    {
      return new TimePeriod(left.Years / right, left.Months / right, left.Weeks / right, left.Days / right, left.Hours / right, left.Minutes / right, left.Seconds / right, left.Milliseconds / right);
    }
    public static TimePeriod operator *(TimePeriod left, int right)
    {
      return new TimePeriod(left.Years * right, left.Months * right, left.Weeks * right, left.Days * right, left.Hours * right, left.Minutes * right, left.Seconds * right, left.Milliseconds * right);
    }
    public static bool operator ==(TimePeriod left, TimePeriod right)
    {
      return left.EqualTo(right);
    }
    public static bool operator !=(TimePeriod left, TimePeriod right)
    {
      return !left.EqualTo(right);
    }

    private static bool TryParseField(IEnumerator<char> CharEnumerator, out string NumberText, out char Letter)
    {
      var Result = true;

      var NumberValue = new StringBuilder();
      while (CharEnumerator.Current >= '0' && CharEnumerator.Current <= '9')
      {
        NumberValue.Append(CharEnumerator.Current);

        if (!CharEnumerator.MoveNext())
        {
          Result = false;
          break;
        }
      }

      // number not found.
      if (Result && NumberValue.Length == 0)
        Result = false;

      if (Result)
      {
        NumberText = NumberValue.ToString();
        Letter = CharEnumerator.Current; // character after the number.
      }
      else
      {
        NumberText = "";
        Letter = '\0';
      }

      return Result;
    }
    private static bool TryParseField(IEnumerator<char> CharEnumerator, out int Number, out char Letter)
    {
      var Result = true;

      Result = TryParseField(CharEnumerator, out string NumberText, out Letter);

      if (Result)
        Number = int.Parse(NumberText);
      else
        Number = 0;

      return Result;
    }
    private static bool TryParseField(IEnumerator<char> CharEnumerator, out long Number, out char Letter)
    {
      var Result = true;

      Result = TryParseField(CharEnumerator, out string NumberText, out Letter);

      if (Result)
        Number = long.Parse(NumberText);
      else
        Number = 0;

      return Result;
    }

    bool IEquatable<TimePeriod>.Equals(TimePeriod other)
    {
      return EqualTo(other);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("duration", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      var Result = Parse(reader.ReadInnerXml());

      YearsField = Result.YearsField;
      MonthsField = Result.MonthsField;
      WeeksField = Result.WeeksField;
      DaysField = Result.DaysField;
      HoursField = Result.HoursField;
      MinutesField = Result.MinutesField;
      SecondsField = Result.SecondsField;
      MillisecondsField = Result.MillisecondsField;
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      // NOTE: not a nice workaround but the standard duration format does not have a 'W'.
      var TempWeeks = WeeksField;
      WeeksField = 0;
      DaysField += TempWeeks * 7;
      try
      {
        writer.WriteRaw(ToString());
      }
      finally
      {
        DaysField -= WeeksField * 7;
        WeeksField = TempWeeks;
      }
    }

    private /*readonly*/ int YearsField;
    private /*readonly*/ int MonthsField;
    private /*readonly*/ int WeeksField;
    private /*readonly*/ int DaysField;
    private /*readonly*/ int HoursField;
    private /*readonly*/ int MinutesField;
    private /*readonly*/ int SecondsField;
    private /*readonly*/ int MillisecondsField;
  }

  public enum TimePeriodUnit
  {
    Years,
    Months,
    Weeks,
    Days,
    Hours,
    Minutes,
    Seconds,
    Milliseconds
  }

  public static class TimePeriodUnitHelper
  {
    public static string FormatAdverb(this TimePeriodUnit Unit)
    {
      var Result = string.Empty;
      switch (Unit)
      {
        case TimePeriodUnit.Years:
          Result = "Yearly";
          break;

        case TimePeriodUnit.Months:
          Result = "Monthly";
          break;

        case TimePeriodUnit.Weeks:
          Result = "Weekly";
          break;

        case TimePeriodUnit.Days:
          Result = "Daily";
          break;

        case TimePeriodUnit.Hours:
          Result = "Hourly";
          break;

        case TimePeriodUnit.Minutes:
          Result = "Every minute";
          break;

        case TimePeriodUnit.Seconds:
          Result = "Every second";
          break;

        case TimePeriodUnit.Milliseconds:
          Result = "Every millisecond";
          break;

        default:
          throw new Exception("Unhandled TimePeriodUnit.");
      }

      return Result;
    }
  }

  public sealed class TimeSpanRange
  {
    public TimeSpanRange(TimeSpan? Begin, TimeSpan? End)
    {
      this.Begin = Begin;
      this.End = End;
    }

    public TimeSpan? Begin { get; private set; }
    public TimeSpan? End { get; private set; }

    public int CompareTo(Inv.TimeSpanRange Value)
    {
      var Result = Begin.HasValue.CompareTo(Value.Begin.HasValue);

      if (Result == 0 && Begin.HasValue)
        Result = Begin.Value.CompareTo(Value.Begin.Value);

      if (Result == 0)
        Result = End.HasValue.CompareTo(Value.End.HasValue);

      if (Result == 0 && End.HasValue)
        Result = End.Value.CompareTo(Value.End.Value);

      return Result;
    }
    public bool EqualTo(Inv.TimeSpanRange Value)
    {
      return Begin.Equals(Value.Begin) && End.Equals(Value.End);
    }

    public override string ToString()
    {
      return Begin + " - " + End;
    }
  }
}