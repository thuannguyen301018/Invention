﻿/*! 10 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Globalization;
using Inv.Support;

namespace Inv
{
  public interface Extent : IComparable
  {
    byte[] GetBuffer();
    DataSize GetSize();
    int GetLength();
    string GetFormat();
    bool IsEmpty();
    bool IsHollow();
  }

  [DataContract]
  public sealed class Binary : Extent
  {
    public static Binary Empty(string Format)
    {
      return new Binary(new byte[] { }, 0, Format);
    }
    public static Inv.Binary Hollow(int Length, string Format)
    {
      return new Inv.Binary(null, Length, Format);
    }

    public Binary(byte[] Buffer, string Format)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Buffer, "Buffer");

      this.Buffer = Buffer;
      this.Length = Buffer.Length;
      this.Format = Format;
    }
    internal Binary(byte[] Buffer, int Length, string Format)
    {
      this.Buffer = Buffer;
      this.Length = Length;
      this.Format = Format;
    }
    internal Binary(Binary Binary)
    {
      this.Buffer = Binary.Buffer;
      this.Format = Binary.Format;
      this.Length = Binary.Length;
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public int GetLength()
    {
      return this.Length;
    }
    public string GetFormat()
    {
      return this.Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool IsHollow()
    {
      return Buffer == null;
    }
    public bool EqualTo(Binary Binary)
    {
      if (Binary == null)
        return false;
      else if (object.ReferenceEquals(Binary, this))
        return true;
      else if (Binary.Format != this.Format)
        return false;
      else if (Binary.Buffer == this.Buffer)
        return true;
      else if (this.IsHollow() || Binary.IsHollow())
        return this.GetSize() == Binary.GetSize();
      else
        return this.GetBuffer().EqualTo(Binary.GetBuffer());
    }
    public int CompareTo(Binary Binary)
    {
      if (Binary == null)
        return -1;
      else if (object.ReferenceEquals(Binary, this))
        return 0;
      else if (Binary.Format != this.Format)
        return string.Compare(Binary.Format, this.Format);
      else if (Binary.Buffer == this.Buffer)
        return 0;
      else if (this.IsHollow() || Binary.IsHollow())
        return this.GetSize().CompareTo(Binary.GetSize());
      else
        return this.GetBuffer().CompareTo(Binary.GetBuffer());
    }
    public Binary ChangeFormat(string NewFormat)
    {
      return new Binary(Buffer, NewFormat);
    }

    public override string ToString()
    {
      return "Binary[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Binary)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly int Length;
    [DataMember]
    private readonly string Format;
  }

  [DataContract]
  public sealed class Image : Extent
  {
    public static Inv.Image Empty(string Format)
    {
      return new Inv.Image(new byte[] { }, 0, Format);
    }
    public static Inv.Image Hollow(int Length, string Format)
    {
      return new Inv.Image(null, Length, Format);
    }

    public Image(byte[] Buffer, string Format)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Buffer, "Buffer");

      this.Buffer = Buffer;
      this.Length = Buffer.Length;
      this.Format = Format;
    }
    internal Image(byte[] Buffer, int Length, string Format)
    {
      this.Buffer = Buffer;
      this.Length = Length;
      this.Format = Format;
    }
    internal Image(Inv.Binary Binary)
    {
      this.Buffer = Binary.GetBuffer();
      this.Length = Binary.GetLength();
      this.Format = Binary.GetFormat();
    }
    internal Image(Inv.Image Image)
    {
      this.Buffer = Image.Buffer;
      this.Length = Image.Length;
      this.Format = Image.Format;
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public int GetLength()
    {
      return this.Length;
    }
    public string GetFormat()
    {
      return this.Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool IsHollow()
    {
      return Buffer == null;
    }
    public bool EqualTo(Image Image)
    {
      if (Image == null)
        return false;
      else if (object.ReferenceEquals(Image, this))
        return true;
      else if (Image.Format != this.Format)
        return false;
      else if (Image.Buffer == this.Buffer)
        return true;
      else if (this.IsHollow() || Image.IsHollow())
        return this.GetSize() == Image.GetSize();
      else
        return this.GetBuffer().EqualTo(Image.GetBuffer());
    }
    public int CompareTo(Image Image)
    {
      if (Image == null)
        return -1;
      else if (object.ReferenceEquals(Image, this))
        return 0;
      else if (Image.Format != this.Format)
        return string.Compare(Image.Format, this.Format);
      else if (Image.Buffer == this.Buffer)
        return 0;
      else if (this.IsHollow() || Image.IsHollow())
        return this.GetSize().CompareTo(Image.GetSize());
      else
        return this.GetBuffer().CompareTo(Image.GetBuffer());
    }

    public override string ToString()
    {
      return "Image[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }

    // not data member.
    internal object Node { get; set; }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Image)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly int Length;
    [DataMember]
    private readonly string Format;
  }

  [DataContract]
  public sealed class Sound : Extent
  {
    public static Inv.Sound Empty(string Format)
    {
      return new Inv.Sound(new byte[] { }, 0, Format);
    }
    public static Inv.Sound Hollow(int Length, string Format)
    {
      return new Inv.Sound(null, Length, Format);
    }

    public Sound(byte[] Buffer, string Format)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Buffer, "Buffer");

      this.Buffer = Buffer;
      this.Length = Buffer.Length;
      this.Format = Format;
    }
    internal Sound(byte[] Buffer, int Length, string Format)
    {
      this.Buffer = Buffer;
      this.Length = Length;
      this.Format = Format;
    }
    internal Sound(Inv.Binary Binary)
    {
      this.Buffer = Binary.GetBuffer();
      this.Length = Binary.GetLength();
      this.Format = Binary.GetFormat();
    }
    internal Sound(Inv.Sound Sound)
    {
      this.Buffer = Sound.Buffer;
      this.Length = Sound.Length;
      this.Format = Sound.Format;
    }

    public byte[] GetBuffer()
    {
      return this.Buffer;
    }
    public DataSize GetSize()
    {
      return DataSize.FromBytes(GetLength());
    }
    public int GetLength()
    {
      return this.Length;
    }
    public string GetFormat()
    {
      return this.Format;
    }
    public bool IsEmpty()
    {
      return GetLength() == 0;
    }
    public bool IsHollow()
    {
      return Buffer == null;
    }
    public bool EqualTo(Sound Sound)
    {
      if (Sound == null)
        return false;
      else if (object.ReferenceEquals(Sound, this))
        return true;
      else if (Sound.Format != this.Format)
        return false;
      else if (Sound.Buffer == this.Buffer)
        return true;
      else if (this.IsHollow() || Sound.IsHollow())
        return this.GetSize() == Sound.GetSize();
      else
        return this.GetBuffer().EqualTo(Sound.GetBuffer());
    }
    public int CompareTo(Sound Sound)
    {
      if (Sound == null)
        return -1;
      else if (object.ReferenceEquals(Sound, this))
        return 0;
      else if (Sound.Format != this.Format)
        return string.Compare(Sound.Format, this.Format);
      else if (Sound.Buffer == this.Buffer)
        return 0;
      else if (this.IsHollow() || Sound.IsHollow())
        return this.GetSize().CompareTo(Sound.GetSize());
      else
        return this.GetBuffer().CompareTo(Sound.GetBuffer());
    }

    public override string ToString()
    {
      return "Sound[" + GetLength().ToString(CultureInfo.InvariantCulture) + "]";
    }

    // not data member.
    internal object Node { get; set; }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Sound)obj);
    }

    [DataMember]
    private readonly byte[] Buffer;
    [DataMember]
    private readonly int Length;
    [DataMember]
    private readonly string Format;
  }
}
