﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  internal static class WeakHelpers
  {
    #region Plain Actions
    public static Action WeakWrapper<T1>(T1 o1, Action<T1> delegateToRun)
      where T1 : class
    {
      var w1 = new WeakReference<T1>(o1);
      return () =>
      {
        if (!w1.TryGetTarget(out T1 s1))
          return;
        delegateToRun(s1);
      };
    }
    public static Action WeakWrapper<T1, T2>(T1 o1, T2 o2, Action<T1, T2> delegateToRun)
      where T1 : class
      where T2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return () =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return;
        delegateToRun(s1, s2);
      };
    }
    public static Func<R> WeakWrapper<T1, T2, R>(T1 o1, T2 o2, Func<T1, T2, R> delegateToRun)
      where T1 : class
      where T2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return () =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return default(R);
        return delegateToRun(s1, s2);
      };
    }
    public static Action WeakWrapper<T1, T2, T3>(T1 o1, T2 o2, T3 o3, Action<T1, T2, T3> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return () =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return;
        delegateToRun(s1, s2, s3);
      };
    }
    public static Func<R> WeakWrapper<T1, T2, T3, R>(T1 o1, T2 o2, T3 o3, Func<T1, T2, T3, R> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return () =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return default(R);

        return delegateToRun(s1, s2, s3);
      };
    }
    #endregion

    #region Parameterised Action<...>s
    public static Action<P1> WeakWrapper<T1, T2, P1>(T1 o1, T2 o2, Action<T1, T2, P1> delegateToRun)
      where T1 : class
      where T2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return (p1) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return;
        delegateToRun(s1, s2, p1);
      };
    }
    public static Action<P1> WeakWrapper<T1, T2, T3, P1>(T1 o1, T2 o2, T3 o3, Action<T1, T2, T3, P1> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where P1 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return (p) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return;
        delegateToRun(s1, s2, s3, p);
      };
    }
    public static Action<P1, P2> WeakWrapper<T1, T2, P1, P2>(T1 o1, T2 o2, Action<T1, T2, P1, P2> delegateToRun)
      where T1 : class
      where T2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return (p1, p2) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return;
        delegateToRun(s1, s2, p1, p2);
      };
    }
    public static Action<P1, P2> WeakWrapper<T1, T2, T3, P1, P2>(T1 o1, T2 o2, T3 o3, Action<T1, T2, T3, P1, P2> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where P1 : class
      where P2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return (p1, p2) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return;
        delegateToRun(s1, s2, s3, p1, p2);
      };
    }
    #endregion

    #region Non-generic EventHandler
    public static EventHandler WeakEventHandlerWrapper<T1>(T1 o1, Action<T1, object, EventArgs> delegateToRun)
      where T1 : class
    {
      var w1 = new WeakReference<T1>(o1);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1))
          return;
        delegateToRun(s1, sender, args);
      };
    }
    public static EventHandler WeakEventHandlerWrapper<T1, T2>(T1 o1, T2 o2, Action<T1, T2, object, EventArgs> delegateToRun)
      where T1 : class
      where T2 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return;
        delegateToRun(s1, s2, sender, args);
      };
    }
    public static EventHandler WeakEventHandlerWrapper<T1, T2, T3>(T1 o1, T2 o2, T3 o3, Action<T1, T2, T3, object, EventArgs> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return;
        delegateToRun(s1, s2, s3, sender, args);
      };
    }
    public static EventHandler WeakEventHandlerWrapper<T1, T2, T3, T4>(T1 o1, T2 o2, T3 o3, T4 o4, Action<T1, T2, T3, T4, object, EventArgs> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where T4 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);
      var w4 = new WeakReference<T4>(o4);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3) || !w4.TryGetTarget(out T4 s4))
          return;
        delegateToRun(s1, s2, s3, s4, sender, args);
      };
    }
    public static EventHandler WeakEventHandlerWrapper<T1, T2, T3, T4, T5>(T1 o1, T2 o2, T3 o3, T4 o4, T5 o5, Action<T1, T2, T3, T4, T5, object, EventArgs> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where T4 : class
      where T5 : class
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);
      var w4 = new WeakReference<T4>(o4);
      var w5 = new WeakReference<T5>(o5);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3) || !w4.TryGetTarget(out T4 s4) || !w5.TryGetTarget(out T5 s5))
          return;
        delegateToRun(s1, s2, s3, s4, s5, sender, args);
      };
    }
    #endregion

    #region Generic EventHandler<TEventArgs>
    public static EventHandler<EventArgsType> WeakEventHandlerWrapper<T1, EventArgsType>(T1 o1, Action<T1, object, EventArgsType> delegateToRun)
      where T1 : class
      where EventArgsType : EventArgs
    {
      var w1 = new WeakReference<T1>(o1);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1))
          return;
        delegateToRun(s1, sender, args);
      };
    }
    public static EventHandler<EventArgsType> WeakEventHandlerWrapper<T1, T2, EventArgsType>(T1 o1, T2 o2, Action<T1, T2, object, EventArgsType> delegateToRun)
      where T1 : class
      where T2 : class
      where EventArgsType : EventArgs
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2))
          return;
        delegateToRun(s1, s2, sender, args);
      };
    }
    public static EventHandler<EventArgsType> WeakEventHandlerWrapper<T1, T2, T3, EventArgsType>(T1 o1, T2 o2, T3 o3, Action<T1, T2, T3, object, EventArgsType> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where EventArgsType : EventArgs
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3))
          return;
        delegateToRun(s1, s2, s3, sender, args);
      };
    }
    public static EventHandler<EventArgsType> WeakEventHandlerWrapper<T1, T2, T3, T4, EventArgsType>(T1 o1, T2 o2, T3 o3, T4 o4, Action<T1, T2, T3, T4, object, EventArgsType> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where T4 : class
      where EventArgsType : EventArgs
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);
      var w4 = new WeakReference<T4>(o4);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3) || !w4.TryGetTarget(out T4 s4))
          return;
        delegateToRun(s1, s2, s3, s4, sender, args);
      };
    }
    public static EventHandler<EventArgsType> WeakEventHandlerWrapper<T1, T2, T3, T4, T5, EventArgsType>(T1 o1, T2 o2, T3 o3, T4 o4, T5 o5, Action<T1, T2, T3, T4, T5, object, EventArgsType> delegateToRun)
      where T1 : class
      where T2 : class
      where T3 : class
      where T4 : class
      where T5 : class
      where EventArgsType : EventArgs
    {
      var w1 = new WeakReference<T1>(o1);
      var w2 = new WeakReference<T2>(o2);
      var w3 = new WeakReference<T3>(o3);
      var w4 = new WeakReference<T4>(o4);
      var w5 = new WeakReference<T5>(o5);

      return (sender, args) =>
      {
        if (!w1.TryGetTarget(out T1 s1) || !w2.TryGetTarget(out T2 s2) || !w3.TryGetTarget(out T3 s3) || !w4.TryGetTarget(out T4 s4) || !w5.TryGetTarget(out T5 s5))
          return;
        delegateToRun(s1, s2, s3, s4, s5, sender, args);
      };
    }
    #endregion
  }
}
