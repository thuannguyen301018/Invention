﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Phone"/>
  /// </summary>
  public sealed class Phone
  {
    internal Phone(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Ask if the device have phone services.
    /// </summary>
    public bool IsSupported
    {
      get { return Application.Platform.PhoneIsSupported; }
    }

    /// <summary>
    /// Prompt to dial the phone number.
    /// </summary>
    /// <param name="Number"></param>
    public void Dial(string Number)
    {
      Application.Platform.PhoneDial(Number);
    }
    /// <summary>
    /// Start an SMS session with the phone number.
    /// </summary>
    /// <param name="Number"></param>
    public void SMS(string Number)
    {
      Application.Platform.PhoneSMS(Number);
    }

    private Application Application;
  }
}
