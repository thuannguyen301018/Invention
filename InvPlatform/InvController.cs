﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal interface ControllerContract
  {
    object Node { get; }
    ControllerVibration GetVibration();
    void SetVibration(ControllerVibration Vibration);
  }

  public sealed class Controller
  {
    internal Controller(Device Device)
    {
      this.Device = Device;
      this.LeftTrigger = new ControllerTrigger(this, ControllerTriggerType.Left);
      this.RightTrigger = new ControllerTrigger(this, ControllerTriggerType.Right);
      this.LeftThumbstick = new ControllerThumbstick(this, ControllerButtonType.LeftThumbstick);
      this.RightThumbstick = new ControllerThumbstick(this, ControllerButtonType.RightThumbstick);

      this.ButtonArray = new Inv.EnumArray<ControllerButtonType, ControllerButton>()
      {
        { ControllerButtonType.LeftThumbstick, LeftThumbstick.Button },
        { ControllerButtonType.RightThumbstick, RightThumbstick.Button }
      };

      ControllerButton AddButton(ControllerButtonType Type)
      {
        var Result = new ControllerButton(this, Type);

        ButtonArray[Type] = Result;

        return Result;
      }

      this.MenuButton = AddButton(ControllerButtonType.Menu);
      this.ViewButton = AddButton(ControllerButtonType.View);
      this.AButton = AddButton(ControllerButtonType.A);
      this.BButton = AddButton(ControllerButtonType.B);
      this.XButton = AddButton(ControllerButtonType.X);
      this.YButton = AddButton(ControllerButtonType.Y);
      this.DPadUpButton = AddButton(ControllerButtonType.DPadUp);
      this.DPadDownButton = AddButton(ControllerButtonType.DPadDown);
      this.DPadLeftButton = AddButton(ControllerButtonType.DPadLeft);
      this.DPadRightButton = AddButton(ControllerButtonType.DPadRight);
      this.LeftShoulderButton = AddButton(ControllerButtonType.LeftShoulder);
      this.RightShoulderButton = AddButton(ControllerButtonType.RightShoulder);
    }

    public string UserId { get; internal set; }
    public ControllerTrigger LeftTrigger { get; }
    public ControllerTrigger RightTrigger { get; }
    public ControllerThumbstick LeftThumbstick { get; }
    public ControllerThumbstick RightThumbstick { get; }
    public ControllerButton MenuButton { get; }
    public ControllerButton ViewButton { get; }
    public ControllerButton AButton { get; }
    public ControllerButton BButton { get; }
    public ControllerButton XButton { get; }
    public ControllerButton YButton { get; }
    public ControllerButton DPadUpButton { get; }
    public ControllerButton DPadDownButton { get; }
    public ControllerButton DPadLeftButton { get; }
    public ControllerButton DPadRightButton { get; }
    public ControllerButton LeftShoulderButton { get; }
    public ControllerButton RightShoulderButton { get; }
    public event Action<ControllerButton> PressButtonEvent;
    public event Action<ControllerButton> ReleaseButtonEvent;

    public ControllerVibration GetVibration()
    {
      Device.Application.RequireThreadAffinity();

      return Contract.GetVibration();
    }
    public void StartVibration(ControllerVibration Vibration)
    {
      Device.Application.RequireThreadAffinity();

      Contract.SetVibration(Vibration);
    }
    public void StopVibration()
    {
      Device.Application.RequireThreadAffinity();

      Contract.SetVibration(new Inv.ControllerVibration()
      {
        LeftMotor = 0.0,
        RightMotor = 0.0,
        LeftTrigger = 0.0,
        RightTrigger = 0.0
      });
    }

    public IEnumerable<ControllerButton> GetButtons()
    {
      Device.Application.RequireThreadAffinity();

      return ButtonArray;
    }
    public ControllerButton GetButton(ControllerButtonType ButtonType)
    {
      return ButtonArray[ButtonType];
    }

    internal ControllerContract Contract { get; set; }

    internal void PressButtonInvoke(ControllerButton Button)
    {
      PressButtonEvent?.Invoke(Button);
    }
    internal void ReleaseButtonInvoke(ControllerButton Button)
    {
      ReleaseButtonEvent?.Invoke(Button);
    }

    private readonly Device Device;
    private readonly Inv.EnumArray<ControllerButtonType, ControllerButton> ButtonArray;
  }

  public sealed class ControllerButton
  {
    internal ControllerButton(Controller Controller, ControllerButtonType Type)
    {
      this.Controller = Controller;
      this.Type = Type;
    }

    public ControllerButtonType Type { get; }
    public bool IsPressed { get; private set; }
    public event Action PressEvent;
    public event Action ReleaseEvent;

    internal void PressInvoke()
    {
      this.IsPressed = true;

      Controller.PressButtonInvoke(this);

      PressEvent?.Invoke();
    }
    internal void ReleaseInvoke()
    {
      this.IsPressed = false;

      ReleaseEvent?.Invoke();

      Controller.ReleaseButtonInvoke(this);
    }

    private readonly Controller Controller;
  }

  public enum ControllerButtonType
  {
    Menu,
    View,
    A,
    B,
    X,
    Y,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
    LeftShoulder,
    RightShoulder,
    LeftThumbstick,
    RightThumbstick
  }

  public struct ControllerVibration
  {
    public double LeftMotor;
    public double RightMotor;
    public double LeftTrigger;
    public double RightTrigger;
  }

  public enum ControllerTriggerType
  {
    Left,
    Right
  }

  public sealed class ControllerTrigger
  {
    internal ControllerTrigger(Controller Controller, ControllerTriggerType Type)
    {
      this.Controller = Controller;
      this.Type = Type;
    }

    /// <summary>
    /// Left or Right trigger.
    /// </summary>
    public ControllerTriggerType Type { get; }
    /// <summary>
    /// The position of the trigger.
    /// The value is between 0.0 (not depressed) and 1.0 (fully depressed).
    /// </summary>
    public double Depression { get; internal set; }

    private readonly Controller Controller;
  }

  public sealed class ControllerThumbstick
  {
    internal ControllerThumbstick(Controller Controller, ControllerButtonType Type)
    {
      this.Controller = Controller;
      this.Button = new ControllerButton(Controller, Type);
    }

    /// <summary>
    /// The position of the thumbstick on the X-axis.
    /// The value is between -1.0 and 1.0.
    /// </summary>
    public double X { get; internal set; }
    /// <summary>
    /// The position of the thumbstick on the Y-axis.
    /// The value is between -1.0 and 1.0.
    /// </summary>
    public double Y { get; internal set; }
    /// <summary>
    /// The thumbstick is a button itself.
    /// </summary>
    public ControllerButton Button { get; }

    private readonly Controller Controller;
  }
}
