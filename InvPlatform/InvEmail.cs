﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Email"/>
  /// </summary>
  public sealed class Email
  {
    internal Email(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Start a new email message.
    /// </summary>
    /// <returns></returns>
    public EmailMessage NewMessage()
    {
      return new EmailMessage(this);
    }

    internal Application Application { get; private set; }
  }

  /// <summary>
  /// Populate an email message before prompting to send in the default mail app.
  /// </summary>
  public sealed class EmailMessage
  {
    internal EmailMessage(Email Email)
    {
      this.Email = Email;
      this.ToList = new Inv.DistinctList<EmailRecipient>();
      this.AttachmentList = new Inv.DistinctList<EmailAttachment>();
    }

    /// <summary>
    /// Subject of the email.
    /// </summary>
    public string Subject { get; set; }
    /// <summary>
    /// Plain text body of the email.
    /// </summary>
    public string Body { get; set; }

    /// <summary>
    /// The email To field will be filled in with the recipients.
    /// </summary>
    /// <param name="RecipientName"></param>
    /// <param name="RecipientAddress"></param>
    public void To(string RecipientName, string RecipientAddress)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(RecipientAddress, nameof(RecipientAddress));
        Inv.Assert.CheckNotNull(RecipientName, nameof(RecipientName));

        var Invalid = Inv.Support.EmailHelper.CheckAddress(RecipientAddress);
        Inv.Assert.Check(Invalid == null, $"RecipientAddress { Invalid.FormatTitle() }");
      }

      ToList.Add(new EmailRecipient(RecipientName, RecipientAddress));
    }
    /// <summary>
    /// Attach a file to the email.
    /// </summary>
    /// <param name="DisplayName"></param>
    /// <param name="File"></param>
    public void Attach(string DisplayName, File File)
    {
      AttachmentList.Add(new EmailAttachment(DisplayName, File));
    }

    /// <summary>
    /// Open the populated email in the default mail app so the user can send.
    /// </summary>
    /// <returns></returns>
    public bool Send()
    {
      AttachmentList.RemoveAll(A => !A.File.Exists());

      return Email.Application.Platform.EmailSendMessage(this);
    }

    internal IEnumerable<EmailRecipient> GetTos()
    {
      return ToList;
    }
    internal IEnumerable<EmailAttachment> GetAttachments()
    {
      return AttachmentList;
    }
    internal bool HasAttachments()
    {
      return AttachmentList.Count > 0;
    }

    private Email Email;
    private Inv.DistinctList<EmailRecipient> ToList;
    private Inv.DistinctList<EmailAttachment> AttachmentList;
  }

  internal sealed class EmailRecipient
  {
    internal EmailRecipient(string Name, string Address)
    {
      this.Name = Name;
      this.Address = Address;
    }

    public string Name { get; private set; }
    public string Address { get; private set; }
  }

  internal sealed class EmailAttachment
  {
    internal EmailAttachment(string Name, File File)
    {
      this.Name = Name;
      this.File = File;
    }

    public string Name { get; private set; }
    public File File { get; private set; }
  }
}
