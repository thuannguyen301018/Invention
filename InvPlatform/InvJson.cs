﻿/*! 5 !*/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// Json Convertors.
  /// </summary>
  public static class JsonHelper
  {
    static JsonHelper()
    {
      BaseConverter = new JsonConverter();
    }

    /// <summary>
    /// Invention Json Convertor.
    /// </summary>
    public static readonly JsonConverter BaseConverter;

    /// <summary>
    /// Get default Inv settings.
    /// </summary>
    /// <returns></returns>
    public static Newtonsoft.Json.JsonSerializerSettings NewSerializerSettings()
    {
      var Settings = new Newtonsoft.Json.JsonSerializerSettings()
      {
        Converters = new List<Newtonsoft.Json.JsonConverter>() { BaseConverter },
        Formatting = Newtonsoft.Json.Formatting.Indented,
      };

      return Settings;
    }
  }

  /// <summary>
  /// Json serialization helper.
  /// </summary>
  public sealed class JsonSerializer
  {
    public JsonSerializer()
    {
      this.IgnoreErrors = true;
    }

    /// <summary>
    /// Ignores any Json serialization errors (true by default).
    /// </summary>
    public bool IgnoreErrors { get; set; }
    public Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get; set; }

    /// <summary>
    /// Deserialize a Json object from the input text.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Text"></param>
    /// <returns></returns>
    public T Load<T>(string Text)
    {
      try
      {
        var Result = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(Text, JsonSerializerSettings ?? JsonHelper.NewSerializerSettings());

        return Result;
      }
      catch (Newtonsoft.Json.JsonReaderException)
      {
        if (!IgnoreErrors)
          throw;

        return default(T);
      }
    }
    public T LoadFromStream<T>(Stream Stream)
    {
      try
      {
        using (var Reader = new StreamReader(Stream))
        using (var JsonReader = new JsonTextReader(Reader))
        {
          var JsonSerializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings ?? JsonHelper.NewSerializerSettings());

          return JsonSerializer.Deserialize<T>(JsonReader);
        }
      }
      catch (Newtonsoft.Json.JsonWriterException)
      {
        if (!IgnoreErrors)
          throw;

        return default(T);
      }
    }
    /// <summary>
    /// Serialize the object to Json text.
    /// </summary>
    /// <param name="ObjectValue"></param>
    /// <returns></returns>
    public string Save(object ObjectValue)
    {
      try
      {
        var Result = Newtonsoft.Json.JsonConvert.SerializeObject(ObjectValue, JsonSerializerSettings ?? JsonHelper.NewSerializerSettings());

        return Result;
      }
      catch (Newtonsoft.Json.JsonWriterException)
      {
        if (!IgnoreErrors)
          throw;

        return string.Empty;
      }
    }
    public void SaveToStream(object ObjectValue, Stream Stream, bool LeaveStreamOpen = false)
    {
      try
      {
        using (var Writer = new StreamWriter(Stream, new UTF8Encoding(false, true), 1024, LeaveStreamOpen))
        using (var JsonWriter = new JsonTextWriter(Writer))
        {
          var JsonSerializer = Newtonsoft.Json.JsonSerializer.Create(JsonSerializerSettings ?? JsonHelper.NewSerializerSettings());

          JsonSerializer.Serialize(JsonWriter, ObjectValue);
        }
      }
      catch (Newtonsoft.Json.JsonWriterException)
      {
        if (!IgnoreErrors)
          throw;
      }
    }
  }

  /// <summary>
  /// Invention Json Convertor.
  /// </summary>
  public sealed class JsonConverter : Newtonsoft.Json.JsonConverter
  {
    /// <summary>
    /// Writes the JSON representation of the object.
    /// </summary>
    /// <param name="Writer"></param>
    /// <param name="Value"></param>
    /// <param name="Serializer"></param>
    public override void WriteJson(Newtonsoft.Json.JsonWriter Writer, object Value, Newtonsoft.Json.JsonSerializer Serializer)
    {
      if (Value == null)
        Writer.WriteNull();
      else if (Value is Inv.Colour)
        Writer.WriteValue(((Inv.Colour)Value).RawValue);
      else if (Value is Inv.Date)
        Writer.WriteValue(((Inv.Date)Value).ToDateTime());
      else if (Value is Inv.Time)
        Writer.WriteValue(((Inv.Time)Value).ToDateTime());
      else if (Value is Inv.TimePeriod)
        Writer.WriteValue(((Inv.TimePeriod)Value).ToString());
      else if (Value is Inv.Money)
        Writer.WriteValue(((Inv.Money)Value).ToString());
      else
        Writer.WriteValue(Value);
    }
    /// <summary>
    /// Reads the JSON representation of the object.
    /// </summary>
    /// <param name="Reader"></param>
    /// <param name="ObjectType"></param>
    /// <param name="ExistingValue"></param>
    /// <param name="Serializer"></param>
    /// <returns></returns>
    public override object ReadJson(Newtonsoft.Json.JsonReader Reader, Type ObjectType, object ExistingValue, Newtonsoft.Json.JsonSerializer Serializer)
    {
      switch (Reader.TokenType)
      {
        case Newtonsoft.Json.JsonToken.Null:
          return null;

        case Newtonsoft.Json.JsonToken.Integer:
          if (ObjectType == typeof(Inv.Colour))
            return Inv.Colour.FromArgb(Convert.ToInt32(Reader.Value));
          break;

        case Newtonsoft.Json.JsonToken.Date:
          var DateValue = (DateTime)Reader.Value;

          if (ObjectType == typeof(Inv.Time))
            return new Inv.Time(DateValue);
          else if (ObjectType == typeof(Inv.Time?))
            return new Inv.Time(DateValue) as Inv.Time?;
          else if (ObjectType == typeof(Inv.Date))
            return new Inv.Date(DateValue);
          else if (ObjectType == typeof(Inv.Date?))
            return new Inv.Date(DateValue) as Inv.Date?;
          else
            throw new Exception("Unexpected ObjectType.");

        case Newtonsoft.Json.JsonToken.String:
          var StringValue = (string)Reader.Value;

          if (ObjectType == typeof(Inv.TimePeriod))
          {
            Inv.TimePeriod.TryParse(StringValue, out var TimePeriod);
            return TimePeriod;
          }
          else if (ObjectType == typeof(Inv.TimePeriod?))
          {
            Inv.TimePeriod.TryParse(StringValue, out var TimePeriod);
            return TimePeriod as Inv.TimePeriod?;
          }
          else if (ObjectType == typeof(Inv.Money))
          {
            Inv.Money.TryParse(StringValue, out var Money);
            return Money;
          }
          else if (ObjectType == typeof(Inv.Money?))
          {
            Inv.Money.TryParse(StringValue, out var Money);
            return Money as Inv.Money?;
          }
          else
          {
            throw new Exception("Unexpected ObjectType.");
          }

        default:
          throw new Exception("Unhandled JSON type.");
      }

      throw new Exception("Unable to read JSON.");
    }
    /// <summary>
    /// Determines whether this instance can convert the specified object type.
    /// </summary>
    /// <param name="ObjectType"></param>
    /// <returns></returns>
    public override bool CanConvert(Type ObjectType)
    {
      return
        ObjectType == typeof(Inv.Date) || ObjectType == typeof(Inv.Date?) ||
        ObjectType == typeof(Inv.Time) || ObjectType == typeof(Inv.Time?) ||
        ObjectType == typeof(Inv.Colour) ||
        ObjectType == typeof(Inv.TimePeriod) || ObjectType == typeof(Inv.TimePeriod?) ||
        ObjectType == typeof(Inv.Money) || ObjectType == typeof(Inv.Money?);
    }
  }
}