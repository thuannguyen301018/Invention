﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Cross-platform key enumeration.
  /// </summary>
  public enum Key
  {
    #region Keys.
    /// <summary>
    /// Letter A.
    /// </summary>
    A,
    /// <summary>
    /// Letter B.
    /// </summary>
    B,
    /// <summary>
    /// Letter C.
    /// </summary>
    C,
    /// <summary>
    /// Letter D.
    /// </summary>
    D,
    /// <summary>
    /// Letter E.
    /// </summary>
    E,
    /// <summary>
    /// Letter F.
    /// </summary>
    F,
    /// <summary>
    /// Letter G.
    /// </summary>
    G,
    /// <summary>
    /// Letter H.
    /// </summary>
    H,
    /// <summary>
    /// Letter I.
    /// </summary>
    I,
    /// <summary>
    /// Letter J.
    /// </summary>
    J,
    /// <summary>
    /// Letter K.
    /// </summary>
    K,
    /// <summary>
    /// Letter L.
    /// </summary>
    L,
    /// <summary>
    /// Letter M.
    /// </summary>
    M,
    /// <summary>
    /// Letter N.
    /// </summary>
    N,
    /// <summary>
    /// Letter O.
    /// </summary>
    O,
    /// <summary>
    /// Letter P.
    /// </summary>
    P,
    /// <summary>
    /// Letter Q.
    /// </summary>
    Q,
    /// <summary>
    /// Letter R.
    /// </summary>
    R,
    /// <summary>
    /// Letter S.
    /// </summary>
    S,
    /// <summary>
    /// Letter T.
    /// </summary>
    T,
    /// <summary>
    /// Letter U.
    /// </summary>
    U,
    /// <summary>
    /// Letter V.
    /// </summary>
    V,
    /// <summary>
    /// Letter W.
    /// </summary>
    W,
    /// <summary>
    /// Letter X.
    /// </summary>
    X,
    /// <summary>
    /// Letter Y.
    /// </summary>
    Y,
    /// <summary>
    /// Letter Z.
    /// </summary>
    Z,
    /// <summary>
    /// Number 0.
    /// </summary>
    n0,
    /// <summary>
    /// Number 1.
    /// </summary>
    n1,
    /// <summary>
    /// Number 2.
    /// </summary>
    n2,
    /// <summary>
    /// Number 3.
    /// </summary>
    n3,
    /// <summary>
    /// Number 4.
    /// </summary>
    n4,
    /// <summary>
    /// Number 5.
    /// </summary>
    n5,
    /// <summary>
    /// Number 6.
    /// </summary>
    n6,
    /// <summary>
    /// Number 7.
    /// </summary>
    n7,
    /// <summary>
    /// Number 8.
    /// </summary>
    n8,
    /// <summary>
    /// Number 9.
    /// </summary>
    n9,
    /// <summary>
    /// Function Key 1.
    /// </summary>
    F1,
    /// <summary>
    /// Function Key 2.
    /// </summary>
    F2,
    /// <summary>
    /// Function Key 3.
    /// </summary>
    F3,
    /// <summary>
    /// Function Key 4.
    /// </summary>
    F4,
    /// <summary>
    /// Function Key 5.
    /// </summary>
    F5,
    /// <summary>
    /// Function Key 6.
    /// </summary>
    F6,
    /// <summary>
    /// Function Key 7.
    /// </summary>
    F7,
    /// <summary>
    /// Function Key 8.
    /// </summary>
    F8,
    /// <summary>
    /// Function Key 9.
    /// </summary>
    F9,
    /// <summary>
    /// Function Key 10.
    /// </summary>
    F10,
    /// <summary>
    /// Function Key 11.
    /// </summary>
    F11,
    /// <summary>
    /// Function Key 12.
    /// </summary>
    F12,
    /// <summary>
    /// Period (.)
    /// </summary>
    Period,
    /// <summary>
    /// Tilde (~)
    /// </summary>
    Tilde,
    /// <summary>
    /// Single Quote (`)
    /// </summary>
    SingleQuote,
    /// <summary>
    /// Double Quote (`)
    /// </summary>
    DoubleQuote,
    /// <summary>
    /// Back Quote (`)
    /// </summary>
    BackQuote,
    /// <summary>
    /// Asterisk (*)
    /// </summary>
    Asterisk,
    /// <summary>
    /// Comma (,)
    /// </summary>
    Comma,
    /// <summary>
    /// Escape (Esc)
    /// </summary>
    Escape,
    /// <summary>
    /// Enter
    /// </summary>
    Enter,
    /// <summary>
    /// Tab
    /// </summary>
    Tab,
    /// <summary>
    /// Space
    /// </summary>
    Space,
    /// <summary>
    /// Insert (Ins)
    /// </summary>
    Insert,
    /// <summary>
    /// Delete (Del)
    /// </summary>
    Delete,
    /// <summary>
    /// Arrow Up
    /// </summary>
    Up,
    /// <summary>
    /// Arrow Down
    /// </summary>
    Down,
    /// <summary>
    /// Arrow Left
    /// </summary>
    Left,
    /// <summary>
    /// Arrow Right
    /// </summary>
    Right,
    /// <summary>
    /// Home
    /// </summary>
    Home,
    /// <summary>
    /// Page Up (PgUp)
    /// </summary>
    PageUp,
    /// <summary>
    /// End
    /// </summary>
    End,
    /// <summary>
    /// Page Down (PgDn)
    /// </summary>
    PageDown,
    /// <summary>
    /// Clear
    /// </summary>
    Clear,
    /// <summary>
    /// Slash (/)
    /// </summary>
    Slash,
    /// <summary>
    /// Backslash (\)
    /// </summary>
    Backslash,
    /// <summary>
    /// Plus (+)
    /// </summary>
    Plus,
    /// <summary>
    /// Minus (-)
    /// </summary>
    Minus,
    /// <summary>
    /// Backspace
    /// </summary>
    Backspace,
    /// <summary>
    /// Left shift
    /// </summary>
    LeftShift,
    /// <summary>
    /// Right shift
    /// </summary>
    RightShift,
    /// <summary>
    /// Left alt
    /// </summary>
    LeftAlt,
    /// <summary>
    /// Right alt
    /// </summary>
    RightAlt,
    /// <summary>
    /// Left ctrl
    /// </summary>
    LeftCtrl,
    /// <summary>
    /// Right ctrl
    /// </summary>
    RightCtrl
    #endregion
  }

  internal enum KeyModifierFlags : byte
  {
    None = 0x00,
    LeftShift = 0x01,
    RightShift = 0x02,
    LeftAlt = 0x04,
    RightAlt = 0x08,
    LeftCtrl = 0x10,
    RightCtrl = 0x20
  }

  /// <summary>
  /// Cross-platform key modifier.
  /// </summary>
  public sealed class KeyModifier
  {
    internal KeyModifier(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }
    internal KeyModifier()
    {
    }

    /// <summary>
    /// Is there no modifier keys pressed.
    /// </summary>
    public bool IsNone
    {
      get { return Flags == KeyModifierFlags.None; }
    }
    /// <summary>
    /// Is the left shift pressed.
    /// </summary>
    public bool IsLeftShift
    {
      get { return (Flags & KeyModifierFlags.LeftShift) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftShift;
        else
          Flags &= ~KeyModifierFlags.LeftShift;
      }
    }
    /// <summary>
    /// Is the right shift pressed.
    /// </summary>
    public bool IsRightShift
    {
      get { return (Flags & KeyModifierFlags.RightShift) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightShift;
        else
          Flags &= ~KeyModifierFlags.RightShift;
      }
    }
    /// <summary>
    /// Is the shift pressed.
    /// </summary>
    public bool IsShift
    {
      get { return (Flags & (KeyModifierFlags.LeftShift | KeyModifierFlags.RightShift)) > 0; }
    }
    /// <summary>
    /// Is the left alt pressed.
    /// </summary>
    public bool IsLeftAlt
    {
      get { return (Flags & KeyModifierFlags.LeftAlt) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftAlt;
        else
          Flags &= ~KeyModifierFlags.LeftAlt;
      }
    }
    /// <summary>
    /// Is the right alt pressed.
    /// </summary>
    public bool IsRightAlt
    {
      get { return (Flags & KeyModifierFlags.RightAlt) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightAlt;
        else
          Flags &= ~KeyModifierFlags.RightAlt;
      }
    }
    /// <summary>
    /// Is the alt pressed.
    /// </summary>
    public bool IsAlt
    {
      get { return (Flags & (KeyModifierFlags.LeftAlt | KeyModifierFlags.RightAlt)) > 0; }
    }
    /// <summary>
    /// Is the left ctrl pressed.
    /// </summary>
    public bool IsLeftCtrl
    {
      get { return (Flags & KeyModifierFlags.LeftCtrl) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.LeftCtrl;
        else
          Flags &= ~KeyModifierFlags.LeftCtrl;
      }
    }
    /// <summary>
    /// Is the right ctrl pressed.
    /// </summary>
    public bool IsRightCtrl
    {
      get { return (Flags & KeyModifierFlags.RightCtrl) > 0; }
      set
      {
        if (value)
          Flags |= KeyModifierFlags.RightCtrl;
        else
          Flags &= ~KeyModifierFlags.RightCtrl;
      }
    }
    /// <summary>
    /// Is the ctrl pressed.
    /// </summary>
    public bool IsCtrl
    {
      get { return (Flags & (KeyModifierFlags.LeftCtrl | KeyModifierFlags.RightCtrl)) > 0; }
    }

    /// <summary>
    /// Render the key modifier to a string.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var Builder = new StringBuilder();

      if (IsLeftShift && IsRightShift)
        Builder.Append("shift + ");
      else if (IsLeftShift)
        Builder.Append("left shift + ");
      else if (IsRightShift)
        Builder.Append("right shift + ");

      if (IsLeftCtrl && IsRightCtrl)
        Builder.Append("ctrl + ");
      else if (IsLeftCtrl)
        Builder.Append("left ctrl + ");
      else if (IsRightCtrl)
        Builder.Append("right ctrl + ");

      if (IsLeftAlt && IsRightAlt)
        Builder.Append("alt + ");
      else if (IsLeftAlt)
        Builder.Append("left alt + ");
      else if (IsRightAlt)
        Builder.Append("right alt + ");

      if (Builder.Length == 0)
        return "";
      else
        return Builder.ToString().Substring(0, Builder.Length - 3);
    }

    /// <summary>
    /// Equality comparison for two key modifiers.
    /// </summary>
    /// <param name="KeyModifier"></param>
    /// <returns></returns>
    public bool IsEqual(KeyModifier KeyModifier)
    {
      return Flags == KeyModifier.Flags;
    }

    internal KeyModifier Clone() => new KeyModifier(GetFlags());
    internal KeyModifierFlags GetFlags()
    {
      return Flags;
    }
    internal void SetFlags(KeyModifierFlags Flags)
    {
      this.Flags = Flags;
    }

    private KeyModifierFlags Flags;
  }

  /// <summary>
  /// The keystroke is the <see cref="Key"/> that was pressed and the <see cref="Modifier"/>.
  /// </summary>
  public sealed class Keystroke
  {
    public Keystroke(Inv.Key Key)
      : this(Key, new KeyModifier())
    {
    }
    public Keystroke(Inv.Key Key, Inv.KeyModifier Modifier)
    {
      this.Key = Key;
      this.Modifier = Modifier;
    }

    /// <summary>
    /// The key pressed.
    /// </summary>
    public Key Key { get; }
    /// <summary>
    /// Any active modifiers such as shift, ctrl or alt.
    /// </summary>
    public KeyModifier Modifier { get; }

    /// <summary>
    /// Render the keystroke to a string.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      var Qualifier = Modifier.ToString();

      if (Qualifier.Length > 0)
        return Qualifier + " + " + Key.ToString();
      else
        return Key.ToString();
    }
  }
}
