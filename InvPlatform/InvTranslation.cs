﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Inv.Support;

namespace Inv.Translation
{
  /// <summary>
  /// Language translation map supports concepts and terms.
  /// </summary>
  public sealed class Map
  {
    /// <summary>
    /// Create a new translation map.
    /// </summary>
    public Map()
    {
      this.ConceptList = new Inv.DistinctList<Concept>();
    }

    /// <summary>
    /// Add a new concept.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public Concept AddConcept(string Name)
    {
      var Result = new Concept(this, Name);

      ConceptList.Add(Result);

      return Result;
    }
    /// <summary>
    /// Enumerate all concepts.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Concept> GetConcepts()
    {
      return ConceptList;
    }
    /// <summary>
    /// Get a concept by name.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public Inv.Translation.Concept GetConcept(string Name)
    {
      var Result = ConceptList.Find(C => C.Name == Name);

      if (Result == null)
        throw new Exception("Concept not found: " + Name);

      return Result;
    }

    /// <summary>
    /// Compile the concepts using reflection.
    /// </summary>
    /// <param name="This"></param>
    public void Compile(object This)
    {
      var Type = This.GetType();

      foreach (var Field in Type.GetReflectionFields())
      {
        if (Field.FieldType == typeof(Inv.Translation.Concept))
          Field.SetValue(This, AddConcept(Field.Name));
      }
    }
    /// <summary>
    /// Remove all terms from the concepts.
    /// </summary>
    public void Clear()
    {
      foreach (var Concept in ConceptList)
        Concept.Dictionary = null;
    }
    /// <summary>
    /// Remove all terms that does not have a translation.
    /// </summary>
    public void Strip()
    {
      foreach (var Concept in ConceptList)
      {
        if (Concept.Dictionary != null)
        {
          foreach (var Entry in Concept.Dictionary.ToArray())
          {
            if (string.IsNullOrWhiteSpace(Entry.Value))
              Concept.Dictionary.Remove(Entry.Key);
          }
        }
      }
    }
    /// <summary>
    /// Import term translations from a csv stream.
    /// </summary>
    /// <param name="Stream"></param>
    public void Import(System.IO.Stream Stream)
    {
      var ConceptDictionary = ConceptList.ToDictionary(C => C.Name);

      using (var CsvReader = new CsvReader(Stream))
      {
        var CurrentConcept = (Concept)null;
        var CurrentLine = 1;

        while (CsvReader.More())
        {
          CsvReader.ReadRecordBegin();

          var ConceptName = CsvReader.ReadRecordField();
          if (CurrentConcept == null || CurrentConcept.Name != ConceptName)
          {
            CurrentConcept = ConceptDictionary.GetValueOrDefault(ConceptName);
            if (CurrentConcept != null && CurrentConcept.Dictionary == null)
              CurrentConcept.Dictionary = new Dictionary<string, string>();
          }

          var SourceText = CsvReader.ReadRecordField().Trim();
          var TargetText = CsvReader.ReadRecordField().Trim();

          if (CurrentConcept != null)
          {
            var CurrentText = CurrentConcept.Dictionary.GetValueOrDefault(SourceText);

            if (string.IsNullOrWhiteSpace(CurrentText))
              CurrentConcept.Dictionary[SourceText] = TargetText;
          }

          if (CsvReader.MoreRecordFields())
            throw new Exception($"Line {CurrentLine}: more fields than expected at translation: {ConceptName},{SourceText.EncodeCsvField()},{TargetText.EncodeCsvField()}");

          CsvReader.ReadRecordEnd();

          CurrentLine++;
        }
      }
    }
    /// <summary>
    /// Export term translations to a csv stream.
    /// </summary>
    /// <param name="Stream"></param>
    public void Export(System.IO.Stream Stream)
    {
      using (var CsvWriter = new CsvWriter(Stream))
      {
        foreach (var Concept in ConceptList.OrderBy(C => C.Name))
        {
          if (Concept.Dictionary != null)
          {
            foreach (var Term in Concept.Dictionary.OrderBy(T => T.Key))
              CsvWriter.WriteRecord(Concept.Name, Term.Key, Term.Value);
          }
        }
      }
    }
    /// <summary>
    /// Merge this map with <paramref name="MergeMap"/>.
    /// </summary>
    /// <param name="MergeMap"></param>
    public void Merge(Map MergeMap)
    {
      var ConceptDictionary = ConceptList.ToDictionary(C => C.Name);

      foreach (var MergeConcept in MergeMap.GetConcepts())
      {
        var CurrentConcept = ConceptDictionary.GetValueOrDefault(MergeConcept.Name);

        if (CurrentConcept == null)
        {
          // lost concept.
          throw new Exception("Concept missing: " + MergeConcept.Name);
        }
        else if (MergeConcept.Dictionary != null)
        {
          if (CurrentConcept.Dictionary == null)
            CurrentConcept.Dictionary = new Dictionary<string, string>();

          var ObsoleteSet = CurrentConcept.Dictionary.Keys.ToHashSetX();

          foreach (var MergeEntry in MergeConcept.Dictionary)
          {
            ObsoleteSet.Remove(MergeEntry.Key);
            
            var CurrentText = CurrentConcept.Dictionary.GetValueOrDefault(MergeEntry.Key);

            if (string.IsNullOrWhiteSpace(CurrentText))
            {
              //Debug.WriteLine("MISSING " + CurrentConcept.Name + "," + MergeEntry.Key + "," + CurrentText);

              CurrentConcept.Dictionary[MergeEntry.Key] = MergeEntry.Value;
            }
          }

          foreach (var Obsolete in ObsoleteSet)
            Debug.WriteLine("OBSOLETE " + CurrentConcept.Name + ": " + Obsolete);
        }
      }
    }

    private Inv.DistinctList<Concept> ConceptList;
  }

  /// <summary>
  /// Translation concept.
  /// </summary>
  public sealed class Concept
  {
    internal Concept(Map Map, string Name)
    {
      this.Map = Map;
      this.Name = Name;
      this.Dictionary = null;
    }

    /// <summary>
    /// Name of the translation concept.
    /// </summary>
    public string Name { get; private set; }

    /// <summary>
    /// Get the translated text for a term.
    /// </summary>
    /// <param name="Text">The term to lookup and will be returned if there is no translation.</param>
    /// <returns></returns>
    public string Get(string Text)
    {
      return Get(Text, Text);
    }
    /// <summary>
    /// Get the translated text for a term.
    /// </summary>
    /// <param name="ReferenceText">The term to lookup</param>
    /// <param name="OriginalText">The term to return if there is no translation</param>
    /// <returns></returns>
    public string Get(string ReferenceText, string OriginalText)
    {
      if (Dictionary == null || string.IsNullOrWhiteSpace(OriginalText))
        return OriginalText;
      else
        return Dictionary.GetValueOrDefault(ReferenceText, OriginalText).EmptyAsNull() ?? OriginalText;
    }
    /// <summary>
    /// Set the translation for a term.
    /// </summary>
    /// <param name="SourceText"></param>
    /// <param name="TargetText"></param>
    public void Set(string SourceText, string TargetText)
    {
      if (Dictionary == null)
        Dictionary = new Dictionary<string, string>();

      Dictionary[SourceText] = TargetText.NullAsEmpty().Trim();
    }
    /// <summary>
    /// Declare the terms based on an enumerable and delegate.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Source"></param>
    /// <param name="Function"></param>
    public void Declare<T>(IEnumerable<T> Source, Func<T, string> Function)
    {
      foreach (var Item in Source)
      {
        var SourceText = Function(Item);

        if (!string.IsNullOrWhiteSpace(SourceText))
        {
          if (Dictionary == null)
            Dictionary = new Dictionary<string, string>();

          Dictionary.GetOrAdd(SourceText.Trim(), S => "");
        }
      }
    }
    /// <summary>
    /// Redefine the terms using a delegate.
    /// </summary>
    /// <param name="ProcessFunction"></param>
    public void Define(Func<string, string> ProcessFunction)
    {
      if (Dictionary != null)
      {
        foreach (var Term in Dictionary.Keys.ToArray())
          Dictionary[Term] = ProcessFunction(Term).NullAsEmpty().Trim();
      }
    }
    /// <summary>
    /// Get the terms.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<KeyValuePair<string, string>> GetTerms()
    {
      if (Dictionary != null)
        return Dictionary;
      else
        return new Dictionary<string, string>();
    }

    internal Dictionary<string, string> Dictionary { get; set; }

    private readonly Map Map;
  }
}
