﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Location"/>
  /// </summary>
  public sealed class Location
  {
    internal Location(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Ask if this device supports location services.
    /// </summary>
    public bool IsSupported
    {
      get { return Application.Platform.LocationIsSupported; }
    }
    internal event Action<Coordinate> ChangeEvent; // TODO: needs more dev before it can be public released.

    /// <summary>
    /// Async reverse geocode lookup.
    /// </summary>
    /// <param name="Coordinate">The coordinate to lookup.</param>
    /// <param name="ResultAction">Fired when a result is found.</param>
    public void Lookup(Inv.Coordinate Coordinate, Action<LocationResult> ResultAction)
    {
      var Result = new LocationResult(Coordinate, ResultAction);

      Application.Platform.LocationLookup(Result);
    }
    /// <summary>
    /// Launch the specified location (such as a street address) in the default map viewer.
    /// </summary>
    /// <param name="Location"></param>
    public void ShowMap(string Location)
    {
      Application.Platform.LocationShowMap(Location);
    }

    internal bool IsRequired
    {
      get { return ChangeEvent != null; }
    }

    internal void ChangeInvoke(Coordinate Coordinate)
    {
      if (ChangeEvent != null)
        ChangeEvent(Coordinate);
    }

    private Application Application;
  }

  /// <summary>
  /// The result of a reverse geocode loookup.
  /// </summary>
  public sealed class LocationResult
  {
    internal LocationResult(Coordinate Coordinate, Action<LocationResult> ResultAction)
    {
      this.Coordinate = Coordinate;
      this.ResultAction = ResultAction;
    }

    /// <summary>
    /// Coordinate location.
    /// </summary>
    public Coordinate Coordinate { get; private set; }

    /// <summary>
    /// Enumerable the list of matching placemarks.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<LocationPlacemark> GetPlacemarks()
    {
      return PlacemarkArray;
    }

    internal void SetPlacemarks(IEnumerable<LocationPlacemark> Placemarks)
    {
      this.PlacemarkArray = Placemarks.ToArray();
      ResultAction(this);
    }

    private Action<LocationResult> ResultAction;
    private LocationPlacemark[] PlacemarkArray;
  }

  /// <summary>
  /// The placemark are points of interest by latitude and longitude.
  /// </summary>
  public sealed class LocationPlacemark
  {
    internal LocationPlacemark(string Name, string Locality, string SubLocality, string PostalCode, string AdministrativeArea, string SubAdministrativeArea, string CountryName, string CountryCode, double Latitude, double Longitude)
    {
      this.Name = Name;
      this.Locality = Locality;
      this.SubLocality = SubLocality;
      this.PostalCode = PostalCode;
      this.AdministrativeArea = AdministrativeArea;
      this.SubAdministrativeArea = SubAdministrativeArea;
      this.CountryName = CountryName;
      this.CountryCode = CountryCode;
      this.Latitude = Latitude;
      this.Longitude = Longitude;
    }

    /// <summary>
    /// Street.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// Suburb.
    /// </summary>
    public string Locality { get; private set; }
    /// <summary>
    /// City.
    /// </summary>
    public string SubLocality { get; private set; }
    /// <summary>
    /// Postal code
    /// </summary>
    public string PostalCode { get; private set; }
    /// <summary>
    /// State.
    /// </summary>
    public string AdministrativeArea { get; private set; }
    /// <summary>
    /// Area.
    /// </summary>
    public string SubAdministrativeArea { get; private set; }
    /// <summary>
    /// Country name.
    /// </summary>
    public string CountryName { get; private set; }
    /// <summary>
    /// Country code.
    /// </summary>
    public string CountryCode { get; private set; }
    /// <summary>
    /// Latitude of placemark.
    /// </summary>
    public double Latitude { get; private set; }
    /// <summary>
    /// Longitude of placemark.
    /// </summary>
    public double Longitude { get; private set; }

    /// <summary>
    /// Convert this placemark to a string.
    /// </summary>
    /// <returns></returns>
    public string ToCanonical()
    {
      using (var StringWriter = new System.IO.StringWriter())
      {
        using (var CsvWriter = new Inv.CsvWriter(StringWriter))
          CsvWriter.WriteRecord(Name, Locality, SubLocality, PostalCode, AdministrativeArea, SubAdministrativeArea, CountryName, CountryCode, Latitude.ToString(), Longitude.ToString());

        return StringWriter.ToString();
      }
    }
    /// <summary>
    /// Convert from a string to a new placemark.
    /// </summary>
    /// <param name="Text"></param>
    /// <returns></returns>
    public static LocationPlacemark FromCanonical(string Text)
    {
      if (Text == null)
        return null;

      using (var StringReader = new System.IO.StringReader(Text))
      {
        using (var CsvReader = new Inv.CsvReader(StringReader))
        {
          var CsvRecord = CsvReader.ReadRecord();

          return new LocationPlacemark
          (
            Name: CsvRecord.Length > 0 ? CsvRecord[0] : null,
            Locality: CsvRecord.Length > 1 ? CsvRecord[1] : null,
            SubLocality: CsvRecord.Length > 2 ? CsvRecord[2] : null,
            PostalCode: CsvRecord.Length > 3 ? CsvRecord[3] : null,
            AdministrativeArea: CsvRecord.Length > 4 ? CsvRecord[4] : null,
            SubAdministrativeArea: CsvRecord.Length > 5 ? CsvRecord[5] : null,
            CountryName: CsvRecord.Length > 6 ? CsvRecord[6] : null,
            CountryCode: CsvRecord.Length > 7 ? CsvRecord[7] : null,
            Latitude: CsvRecord.Length > 8 ? double.Parse(CsvRecord[8]) : 0.0,
            Longitude: CsvRecord.Length > 9 ? double.Parse(CsvRecord[9]) : 0.0
          );
        }
      }
    }
  }
}
