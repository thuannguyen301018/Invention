﻿/*! 57 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Portable application that can be run on multiple platforms.
  /// </summary>
  public sealed class Application
  {
    /// <summary>
    /// Create a new instances of a portable application.
    /// </summary>
    public Application()
    {
      Recruit();

      this.Device = new Device(this);
      this.Process = new Process(this);
      this.Window = new Window(this);
      this.Directory = new Directory(this);
      this.Audio = new Audio(this);
      this.Graphics = new Graphics(this);
      this.Location = new Location(this);
      this.Calendar = new Calendar(this);
      this.Market = new Market(this);
      this.Vault = new Vault(this);
      this.Web = new Web(this);
      this.Email = new Email(this);
      this.Phone = new Phone(this);
      this.Clipboard = new Clipboard(this);
    }

    /// <summary>
    /// The title is the name of the application as presented to the end user.
    /// </summary>
    public string Title { get; set; }
    /// <summary>
    /// If the application has been marked to exit.
    /// </summary>
    public bool IsExit { get; private set; }
    /// <summary>
    /// Publishes device capabilities including the actual platform target.
    /// </summary>
    public Inv.Device Device { get; }
    /// <summary>
    /// Query the memory usage of the running process.
    /// </summary>
    public Inv.Process Process { get; }
    /// <summary>
    /// Represents the screen of your device and can be used to create layout surfaces.
    /// </summary>
    public Inv.Window Window { get; }
    /// <summary>
    /// Working with files and folders in local storage.
    /// </summary>
    public Inv.Directory Directory { get; }
    /// <summary>
    /// Playback of mp3 files.
    /// </summary>
    public Inv.Audio Audio { get; }
    /// <summary>
    /// Graphics services is for analysing images.
    /// </summary>
    public Inv.Graphics Graphics { get; }
    /// <summary>
    /// Reverse geocoding is the lookup of a location by a global position expressed in latitude and longitude.
    /// </summary>
    public Inv.Location Location { get; }
    /// <summary>
    /// Access the time zone for this device and invoke platform-specific pickers for date and time.
    /// </summary>
    public Inv.Calendar Calendar { get; }
    /// <summary>
    /// Open the app listing in the current platform's app store.
    /// </summary>
    public Inv.Market Market { get; }
    /// <summary>
    /// Storage and recovery of sensitive information from the device's key-chain.
    /// </summary>
    public Inv.Vault Vault { get; }
    /// <summary>
    /// Interop with the web using RESTful APIs and Json.
    /// </summary>
    public Inv.Web Web { get; }
    /// <summary>
    /// Start an email message in the default mail app.
    /// </summary>
    public Inv.Email Email { get; }
    /// <summary>
    /// Dial a phone number or start sending a text message.
    /// </summary>
    public Inv.Phone Phone { get; }
    /// <summary>
    /// Copy and paste from the shared application clipboard.
    /// </summary>
    public Inv.Clipboard Clipboard { get; }
    /// <summary>
    /// StartEvent is guaranteed to be run the first time the app is started.
    /// </summary>
    public event Action StartEvent;
    /// <summary>
    /// StopEvent is intended to run when the app is terminated but this is not guaranteed on all platforms.
    /// </summary>
    public event Action StopEvent;
    /// <summary>
    /// SuspendEvent is run when the app switches from foreground to background.
    /// </summary>
    public event Action SuspendEvent;
    /// <summary>
    /// ResumeEvent is run when the app switches from background to foreground.
    /// </summary>
    public event Action ResumeEvent;
    /// <summary>
    /// ExitQuery is only supported on Windows Desktop and allows you to ask for confirmation when the window is closed.
    /// </summary>
    public event Func<bool> ExitQuery;
    /// <summary>
    /// The global exception handler for the application.
    /// </summary>
    public event Action<Exception> HandleExceptionEvent;

    /// <summary>
    /// This method terminates the app on Windows but on Android/iOS it will only put the app into the background.
    /// </summary>
    public void Exit()
    {
      RequireThreadAffinity();

      this.IsExit = true;
    }
    /// <summary>
    /// Check that the current thread is the main thread off the application.
    /// </summary>
    public void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ThreadAffinity == Platform.ThreadAffinity(), "Thread affinity mismatch. Make sure your code is executing in the application thread.");
    }
    /// <summary>
    /// Check that the current thread is not the main thread of the application.
    /// </summary>
    public void PreventThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
      {
        if (ThreadAffinity == Platform.ThreadAffinity())
          throw new Exception("Thread affinity prevention. Make sure your code is NOT executing in the main thread");
      }
    }

    /// <summary>
    /// Return the application instance for the current thread.
    /// </summary>
    public static Inv.Application Access()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(SelfLocal.IsValueCreated && SelfLocal.Value != null, "Thread affinity mismatch. Make sure your code is executing in the application thread.");
      return SelfLocal.Value;
    }

    internal Platform Platform { get; private set; }
    internal int ThreadAffinity { get; private set; }

    internal void Recruit()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!SelfLocal.IsValueCreated || SelfLocal.Value == null, "Application cannot be recruited more than once on a given thread.");
      SelfLocal.Value = this;
    }
    internal void Dismiss()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(SelfLocal.IsValueCreated, "Application can only be dismissed from the original application thread.");
        Inv.Assert.Check(SelfLocal.Value != null, "Application has already been dismissed on this thread.");
        Inv.Assert.Check(SelfLocal.Value == this, "This application cannot be dismissed from a different application thread.");
      }
      SelfLocal.Value = null;
    }
    internal void StartInvoke()
    {
      RequireThreadAffinity();

      if (StartEvent != null)
        StartEvent();
    }
    internal void StopInvoke()
    {
      RequireThreadAffinity();

      if (StopEvent != null)
        StopEvent();
    }
    internal void SuspendInvoke()
    {
      RequireThreadAffinity();

      if (SuspendEvent != null)
        SuspendEvent();
    }
    internal void ResumeInvoke()
    {
      RequireThreadAffinity();

      if (ResumeEvent != null)
        ResumeEvent();
    }
    internal bool ExitQueryInvoke()
    {
      RequireThreadAffinity();

      if (ExitQuery != null)
        return ExitQuery();
      else
        return true;
    }
    internal void HandleExceptionInvoke(Exception Exception)
    {
      // can't handle this exception, we want to crash the process.
      if (Exception is ProcessIntentionalCrashException)
        throw Exception;

      // NOTE: Simon/Callan removed afinity check.
      // CheckThreadAffinity();

      try
      {
        if (HandleExceptionEvent != null)
          HandleExceptionEvent(Exception);
      }
      catch
      {
        // NOTE: prevent further exceptions.
      }
    }

    internal void SetPlatform(Platform Platform)
    {
      this.Platform = Platform;
      this.IsExit = false;
      this.ThreadAffinity = Platform.ThreadAffinity();
    }

    private static readonly System.Threading.ThreadLocal<Application> SelfLocal = new System.Threading.ThreadLocal<Application>();
  }
}