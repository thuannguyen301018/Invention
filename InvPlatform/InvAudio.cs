﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Audio"/>
  /// </summary>
  public sealed class Audio
  {
    internal Audio(Application Application)
    {
      this.Application = Application;
      this.IsRateControlled = true;
      this.IsVolumeControlled = true;
      this.IsPanControlled = true;
    }

    /// <summary>
    /// Global mute for sounds played through this API.
    /// </summary>
    /// <param name="IsMuted"></param>
    public void SetMuted(bool IsMuted)
    {
      this.IsMuted = IsMuted;
    }
    /// <summary>
    /// Global control to prevent rate variations for sounds played through this API.
    /// </summary>
    /// <param name="IsRateControlled"></param>
    public void SetRateControl(bool IsRateControlled)
    {
      this.IsRateControlled = IsRateControlled;
    }
    /// <summary>
    /// Global control to prevent volume variations for sounds played through this API.
    /// </summary>
    /// <param name="IsVolumeControlled"></param>
    public void SetVolumeControl(bool IsVolumeControlled)
    {
      this.IsVolumeControlled = IsVolumeControlled;
    }
    /// <summary>
    /// Global control to prevent pan variations for sounds played through this API.
    /// </summary>
    /// <param name="IsPanControlled"></param>
    public void SetPanControl(bool IsPanControlled)
    {
      this.IsPanControlled = IsPanControlled;
    }
    /// <summary>
    /// Play a mp3 track using the platform's audio subsystem.
    /// The sound will be played once and to the end of the track.
    /// </summary>
    /// <param name="Sound">The mp3 track held in memory</param>
    /// <param name="Volume">The volume adjustment between 0.0 and 1.0</param>
    /// <param name="Rate">The rate variation between 0.5 and 2.0</param>
    public void Play(Inv.Sound Sound, float Volume = 1.0F, float Rate = 1.0F, float Pan = 0.0F)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Volume >= 0.0F && Volume <= 1.0F, "Play volume must be between 0.0F and 1.0F.");
        Inv.Assert.Check(Rate >= 0.5F && Rate <= 2.0F, "Play rate must be between 0.5F and 2.0F.");
        Inv.Assert.Check(Pan >= -1.0F && Pan <= +1.0F, "Play pan must be between -1.0F and +1.0F.");
      }

      if (!IsMuted)
        Application.Platform.AudioPlaySound(Sound, IsVolumeControlled ? Volume : 1.0F, IsRateControlled ? Rate : 1.0F, IsPanControlled ? Pan : 0.0F);
    }
    /// <summary>
    /// Create a clip which can be played and optionally stopped before it is finished.
    /// </summary>
    /// <param name="Sound">The mp3 track held in memory</param>
    /// <param name="Volume">The volume adjustment between 0.0 and 1.0</param>
    /// <param name="Rate">The rate variation between 0.5 and 2.0</param>
    /// <param name="Loop">Loop the playing of this sound until it is explicitly stopped</param>
    /// <returns></returns>
    public AudioClip NewClip(Inv.Sound Sound, float Volume = 1.0F, float Rate = 1.0F, float Pan = 0.0F, bool Loop = false)
    {
      return new AudioClip(this, Sound, Volume, Rate, Pan, Loop);
    }
    /// <summary>
    /// Get the play duration of the sound as a TimeSpan (to the millisecond accuracy).
    /// </summary>
    /// <param name="Sound"></param>
    /// <returns></returns>
    public TimeSpan GetLength(Inv.Sound Sound)
    {
      return Application.Platform.AudioGetSoundLength(Sound);
    }
     
    internal Application Application { get; private set; }

    private bool IsMuted;
    private bool IsRateControlled;
    private bool IsVolumeControlled;
    private bool IsPanControlled;
  }

  /// <summary>
  /// Control the playback of a sound track.
  /// </summary>
  public sealed class AudioClip
  {
    internal AudioClip(Audio Audio, Inv.Sound Sound, float Volume, float Rate, float Pan, bool Loop)
    {
      this.Audio = Audio;
      this.Sound = Sound;
      this.Volume = Volume;
      this.Rate = Rate;
      this.Pan = Pan;
      this.Loop = Loop;
    }

    /// <summary>
    /// The sound to be played.
    /// </summary>
    public Sound Sound { get; private set; }
    /// <summary>
    /// The volume to play the sound between 0.0F and 1.0F.
    /// </summary>
    public float Volume { get; private set; }
    /// <summary>
    /// The rate to play the sound between 0.5F and 2.0F.
    /// </summary>
    public float Pan { get; private set; }
    /// <summary>
    /// The pan to play the sound between -1.0F and +1.0F.
    /// -1 is entirely the left speaker.
    /// +1 is entirely the right speaker.
    /// </summary>
    public float Rate { get; private set; }
    /// <summary>
    /// Whether to continuously loop the track until it is manually stopped.
    /// </summary>
    public bool Loop { get; private set; }

    /// <summary>
    /// Play the sound.
    /// </summary>
    public void Play()
    {
      Audio.Application.Platform.AudioPlayClip(this);
    }
    /// <summary>
    /// Stop playing the sound.
    /// </summary>
    public void Stop()
    {
      Audio.Application.Platform.AudioStopClip(this);
    }
    /*
    public void Pause()
    {
      Audio.Application.Platform.AudioPauseClip(this);
    }
    public void Resume()
    {
      Audio.Application.Platform.AudioResumeClip(this);
    }*/

    internal object Node { get; set; }

    private Audio Audio;
  }
}
