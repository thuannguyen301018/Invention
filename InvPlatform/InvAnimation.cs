﻿/*! 14 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

/*
Specification
•	The animation is a set of transforms on panels.
•	Supported transforms include fade, rotate, scale and translate.
•	Transforms run for a duration and may have a delayed start time.
•	Transforms can be absolute from ‘state A’ to ‘state B’.
•	Transforms can be relative from ‘current state’ to ‘specific state’.
•	Panels hold the final state at the end of the animation.
•	Animations are manually started but do not need to be stopped.
•	When an animation is manually stopped it will hold the panel at the current transformed state.
•	Concurrent transforms are allowed on a single panel. Eg. fade opacity + translate Y.
•	Contradictory transforms have undefined behaviour. Eg. Two fade transforms on a single panel that overlap in time.
•	The animation fires a CommenceEvent when the animation starts.
•	The animation fires a CompleteEvent when the animation ends.
•	There is no platform overhead to an animation until it is running.

Transforms
•	Fade Opacity [% → %]
•	Translate Position [+/- X,Y → +/- X,Y]
•	Scale Size [Width%,Height% → Width%,Height%]
•	Rotate Angle (+/-° → +/-°)
*/

namespace Inv
{
  /// <summary>
  /// Animations are used to manipulate the elements of a panel to achieve visual effects.
  /// </summary>
  public sealed class Animation
  {
    internal Animation(Inv.Window Window)
    {
      this.Window = Window;
      this.TargetList = new DistinctList<AnimationTarget>();
    }

    /// <summary>
    /// Create new animation.
    /// </summary>
    /// <returns></returns>
    public static Animation New() => new Inv.Animation(Inv.Application.Access().Window);

    /// <summary>
    /// Ask if the animation has been started but not yet stopped.
    /// </summary>
    public bool IsActive { get; private set; }
    /// <summary>
    /// Ask if the animation has run to completion.
    /// </summary>
    public bool IsCompleted { get; private set; }
    /// <summary>
    /// Fired when the animation begins.
    /// </summary>
    public event Action CommenceEvent;
    /// <summary>
    /// Fired when the animation ends.
    /// </summary>
    public event Action CompleteEvent;

    /// <summary>
    /// Remove all targets.
    /// </summary>
    public void RemoveTargets()
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsActive, "Animation must not be active.");

      TargetList.Clear();
    }
    /// <summary>
    /// Add a new animation target.
    /// </summary>
    /// <param name="Panel"></param>
    /// <returns></returns>
    public AnimationTarget AddTarget(Panel Panel)
    {
      RequireThreadAffinity();

      var Result = new AnimationTarget(Panel);

      TargetList.Add(Result);

      return Result;
    }
    /// <summary>
    /// Restart the animation.
    /// </summary>
    public void Restart()
    {
      Stop();
      Start();
    }
    /// <summary>
    /// Start the animation.
    /// </summary>
    public void Start()
    {
      RequireThreadAffinity();

      if (!IsActive)
      {
        this.IsActive = true;
        this.IsCompleted = false;

        if (CommenceEvent != null)
          CommenceEvent();

        Window.StartAnimation(this);
      }
    }
    /// <summary>
    /// Stop the animation.
    /// </summary>
    public void Stop()
    {
      RequireThreadAffinity();

      if (IsActive)
      {
        this.IsActive = false;
        this.IsCompleted = true;
        Window.StopAnimation(this);
      }
    }

    internal object Node { get; set; }
    internal Inv.Window Window { get; }
    internal int TargetCount
    {
      get { return TargetList.Count; }
    }

    internal IEnumerable<Inv.AnimationTarget> GetTargets()
    {
      RequireThreadAffinity();

      return TargetList;
    }
    internal void Complete()
    {
      RequireThreadAffinity();

      this.IsCompleted = true;

      if (CompleteEvent != null)
        CompleteEvent();
    }

    private void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.RequireThreadAffinity();
    }

    private Inv.DistinctList<Inv.AnimationTarget> TargetList;
  }

  /// <summary>
  /// Selects a panel for adding animation transforms.
  /// </summary>
  public sealed class AnimationTarget
  {
    internal AnimationTarget(Inv.Panel Panel)
    {
      this.Panel = Panel;
      this.TransformList = new Inv.DistinctList<AnimationTransform>();
    }

    internal Inv.Panel Panel { get; private set; }

    /// <summary>
    /// Fade the panel to opacity 0.0F.
    /// </summary>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void FadeOpacityOut(TimeSpan Duration, TimeSpan? Offset = null)
    {
      FadeOpacity(1.0F, 0.0F, Duration, Offset);
    }
    /// <summary>
    /// Fade the panel to opacity 1.0F.
    /// </summary>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void FadeOpacityIn(TimeSpan Duration, TimeSpan? Offset = null)
    {
      FadeOpacity(0.0F, 1.0F, Duration, Offset);
    }
    /// <summary>
    /// Fade the panel from the current opacity to the new opacity.
    /// </summary>
    /// <param name="ToOpacity"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void FadeOpacity(float ToOpacity, TimeSpan Duration, TimeSpan? Offset = null)
    {
      FadeOpacity(float.NaN, ToOpacity, Duration, Offset);
    }
    /// <summary>
    /// Fade the panel to from opacity <paramref name="FromOpacity"/> to opacity <paramref name="ToOpacity"/>.
    /// </summary>
    /// <param name="FromOpacity">Inital opacity</param>
    /// <param name="ToOpacity">Final opacity</param>
    /// <param name="Duration">The time to animate from the initial opacity to the final opacity.</param>
    /// <param name="Offset">Optionally delay the start time of the animation.</param>
    public void FadeOpacity(float FromOpacity, float ToOpacity, TimeSpan Duration, TimeSpan? Offset = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(float.IsNaN(FromOpacity) || (FromOpacity >= 0.0F && FromOpacity <= 1.0F), "From Opacity must be between 0.0F and 1.0F");
        Inv.Assert.Check(ToOpacity >= 0.0F && ToOpacity <= 1.0F, "To Opacity must be between 0.0F and 1.0F");
      }

      TransformList.Add(new AnimationFadeTransform(this)
      {
        Duration = Duration,
        Offset = Offset,
        FromOpacity = FromOpacity,
        ToOpacity = ToOpacity
      });
    }
    /// <summary>
    /// Scale the width of the panel to a percentage of the full width.
    /// </summary>
    /// <param name="ToWidth"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleWidth(float ToWidth, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleWidth(float.NaN, ToWidth, Duration, Offset);
    }
    /// <summary>
    /// Scale the width of the panel from and to a percentage of the full width.
    /// </summary>
    /// <param name="FromWidth"></param>
    /// <param name="ToWidth"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleWidth(float FromWidth, float ToWidth, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleSize(FromWidth, ToWidth, float.NaN, float.NaN, Duration, Offset);
    }
    /// <summary>
    /// Scale the height of the panel from and to a percentage of the full height.
    /// </summary>
    /// <param name="ToHeight"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleHeight(float ToHeight, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleHeight(float.NaN, ToHeight, Duration, Offset);
    }
    /// <summary>
    /// Scale the height of the panel from and to a percentage of the full height.
    /// </summary>
    /// <param name="FromHeight"></param>
    /// <param name="ToHeight"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleHeight(float FromHeight, float ToHeight, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleSize(float.NaN, float.NaN, FromHeight, ToHeight, Duration, Offset);
    }
    /// <summary>
    /// Scale the size of the panel to a percentage of the full size (both width and height).
    /// </summary>
    /// <param name="ToSize"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleSize(float ToSize, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleSize(float.NaN, ToSize, Duration, Offset);
    }
    /// <summary>
    /// Scale the size of the panel from and to a percentage of the full size (both width and height).
    /// </summary>
    /// <param name="FromSize"></param>
    /// <param name="ToSize"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleSize(float FromSize, float ToSize, TimeSpan Duration, TimeSpan? Offset = null)
    {
      ScaleSize(FromSize, ToSize, FromSize, ToSize, Duration, Offset);
    }
    /// <summary>
    /// Scale the size of the panel from and to a percentage of the full size (with different scaling values for width and height).
    /// </summary>
    /// <param name="FromWidth"></param>
    /// <param name="ToWidth"></param>
    /// <param name="FromHeight"></param>
    /// <param name="ToHeight"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void ScaleSize(float FromWidth, float ToWidth, float FromHeight, float ToHeight, TimeSpan Duration, TimeSpan? Offset = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(float.IsNaN(FromWidth) || !float.IsNaN(ToWidth), "ToWidth must be specified if FromWidth is specified.");
        Inv.Assert.Check(float.IsNaN(FromHeight) || !float.IsNaN(ToHeight), "ToHeight must be specified if FromHeight is specified.");
      }

      TransformList.Add(new AnimationScaleTransform(this)
      {
        Duration = Duration,
        Offset = Offset,
        FromWidth = FromWidth,
        ToWidth = ToWidth,
        FromHeight = FromHeight,
        ToHeight = ToHeight,
      });
    }
    /// <summary>
    /// Rotate the panel from the current angle to a new angle.
    /// </summary>
    /// <param name="ToAngle">To angle in degrees</param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void RotateAngle(float ToAngle, TimeSpan Duration, TimeSpan? Offset = null)
    {
      RotateAngle(float.NaN, ToAngle, Duration, Offset);
    }
    /// <summary>
    /// Rotate the panel from a specified angle to a new angle.
    /// </summary>
    /// <param name="FromAngle">From angle in degrees</param>
    /// <param name="ToAngle">To angle in degrees</param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void RotateAngle(float FromAngle, float ToAngle, TimeSpan Duration, TimeSpan? Offset = null)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!float.IsNaN(ToAngle), "To Angle must be specified.");

      TransformList.Add(new AnimationRotateTransform(this)
      {
        Duration = Duration,
        Offset = Offset,
        FromAngle = FromAngle,
        ToAngle = ToAngle
      });
    }
    /// <summary>
    /// Translate the panel to a new X offset.
    /// </summary>
    /// <param name="ToXOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslateX(int ToXOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(null, ToXOffset, null, null, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from a specified X offset to a new X offset.
    /// </summary>
    /// <param name="FromXOffset"></param>
    /// <param name="ToXOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslateX(int FromXOffset, int ToXOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(FromXOffset, ToXOffset, null, null, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from the current Y offset to a new Y offset.
    /// </summary>
    /// <param name="ToYOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslateY(int ToYOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(null, null, null, ToYOffset, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from a specified Y offset to a new Y offset.
    /// </summary>
    /// <param name="FromYOffset"></param>
    /// <param name="ToYOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslateY(int FromYOffset, int ToYOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(null, null, FromYOffset, ToYOffset, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from the current XY offset to a new XY offset.
    /// </summary>
    /// <param name="ToOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslatePosition(int ToOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(null, ToOffset, null, ToOffset, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from a specified XY offset to a new XY offset.
    /// </summary>
    /// <param name="FromOffset"></param>
    /// <param name="ToOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslatePosition(int FromOffset, int ToOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      TranslatePosition(FromOffset, ToOffset, FromOffset, ToOffset, Duration, Offset);
    }
    /// <summary>
    /// Translate the panel from a specified X and Y offset to a new X and Y offset.
    /// </summary>
    /// <param name="FromXOffset"></param>
    /// <param name="ToXOffset"></param>
    /// <param name="FromYOffset"></param>
    /// <param name="ToYOffset"></param>
    /// <param name="Duration"></param>
    /// <param name="Offset"></param>
    public void TranslatePosition(int? FromXOffset, int? ToXOffset, int? FromYOffset, int? ToYOffset, TimeSpan Duration, TimeSpan? Offset = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(FromXOffset == null || ToXOffset != null, "ToXOffset must be specified if FromXOffset is specified.");
        Inv.Assert.Check(FromYOffset == null || ToYOffset != null, "ToYOffset must be specified if FromYOffset is specified.");
      }

      TransformList.Add(new AnimationTranslateTransform(this)
      {
        Duration = Duration,
        Offset = Offset,
        FromX = FromXOffset,
        ToX = ToXOffset,
        FromY = FromYOffset,
        ToY = ToYOffset,
      });
    }

    internal int TransformCount
    {
      get { return TransformList.Count; }
    }

    internal IEnumerable<AnimationTransform> GetTransforms()
    {
      return TransformList;
    }

    private Inv.DistinctList<AnimationTransform> TransformList;
  }

  internal enum AnimationType
  {
    Fade,
    Rotate,
    Scale,
    Translate
  }

  internal abstract class AnimationTransform
  {
    internal AnimationTransform(AnimationTarget Target)
    {
      this.Target = Target;
    }

    internal AnimationTarget Target { get; private set; }
    internal abstract AnimationType Type { get; }
  }

  internal sealed class AnimationFadeTransform : AnimationTransform
  {
    public AnimationFadeTransform(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public float FromOpacity { get; internal set; }
    public float ToOpacity { get; internal set; }

    internal override AnimationType Type
    {
      get { return AnimationType.Fade; }
    }
  }

  internal sealed class AnimationScaleTransform : AnimationTransform
  {
    public AnimationScaleTransform(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public float FromWidth { get; internal set; }
    public float ToWidth { get; internal set; }
    public float FromHeight { get; internal set; }
    public float ToHeight { get; internal set; }

    internal override AnimationType Type
    {
      get { return AnimationType.Scale; }
    }
  }

  internal sealed class AnimationTranslateTransform : AnimationTransform
  {
    public AnimationTranslateTransform(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public int? FromX { get; internal set; }
    public int? ToX { get; internal set; }
    public int? FromY { get; internal set; }
    public int? ToY { get; internal set; }

    internal override AnimationType Type
    {
      get { return AnimationType.Translate; }
    }
  }

  internal sealed class AnimationRotateTransform : AnimationTransform
  {
    public AnimationRotateTransform(AnimationTarget Target)
      : base(Target)
    {
    }

    public TimeSpan? Offset { get; internal set; }
    public TimeSpan Duration { get; internal set; }
    public float FromAngle { get; internal set; }
    public float ToAngle { get; internal set; }

    internal override AnimationType Type
    {
      get { return AnimationType.Rotate; }
    }
  }
}