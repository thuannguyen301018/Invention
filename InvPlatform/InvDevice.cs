﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Device"/>.
  /// </summary>
  public sealed class Device
  {
    internal Device(Application Application)
    {
      this.Application = Application;
      this.ControllerList = new Inv.DistinctList<Controller>();
    }

    /// <summary>
    /// Target platform: Android, iOS, Windows Desktop and Universal Windows.
    /// </summary>
    public DeviceTarget Target { get; internal set; }
    /// <summary>
    /// Is this a mobile device platform.
    /// </summary>
    public bool IsMobile
    {
      get { return Target == DeviceTarget.IOS || Target == DeviceTarget.Android; }
    }
    /// <summary>
    /// Is this a Windows platform.
    /// </summary>
    public bool IsWindows
    {
      get { return Target == DeviceTarget.UniversalWindows || Target == DeviceTarget.WindowsDesktop; }
    }
    /// <summary>
    /// Name of the device.
    /// </summary>
    public string Name { get; internal set; }
    /// <summary>
    /// Manufacturer of the device.
    /// </summary>
    public string Manufacturer { get; internal set; }
    /// <summary>
    /// Model of the device.
    /// </summary>
    public string Model { get; internal set; }
    /// <summary>
    /// Operating system running the device.
    /// </summary>
    public string System { get; internal set; }
    /// <summary>
    /// Does the device have a physical keyboard.
    /// </summary>
    public bool Keyboard { get; internal set; }
    /// <summary>
    /// Does the device have a mouse.
    /// </summary>
    public bool Mouse { get; internal set; }
    /// <summary>
    /// Does the device have a touch screen.
    /// </summary>
    public bool Touch { get; internal set; }
    /// <summary>
    /// The platform-specific default proportional font name.
    /// </summary>
    public string ProportionalFontName { get; internal set; }
    /// <summary>
    /// The platform-specific default monospaced font name.
    /// </summary>
    public string MonospacedFontName { get; internal set; }
    /// <summary>
    /// Pixel density of the device screen.
    /// eg. 2.0F means 2px to a 1pt
    /// </summary>
    public float PixelDensity { get; internal set; }
    public event Action<Controller> AddControllerEvent;
    public event Action<Controller> RemoveControllerEvent;

    public IEnumerable<Controller> GetControllers()
    {
      Application.RequireThreadAffinity();

      return ControllerList;
    }
    public Controller GetController(int Index)
    {
      return Index >= 0 && Index < ControllerList.Count ? ControllerList[Index] : null;
    }

    internal Application Application { get; private set; }

    internal Controller FindController(object Node)
    {
      Application.RequireThreadAffinity();

      return ControllerList.Find(C => C.Contract.Node == Node);
    }
    internal void AddController(Controller Controller)
    {
      Application.RequireThreadAffinity();

      ControllerList.Add(Controller);

      AddControllerEvent?.Invoke(Controller);
    }
    internal void RemoveController(Controller Controller)
    {
      Application.RequireThreadAffinity();

      if (!ControllerList.Remove(Controller))
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Fail("Controller does not exist in the managed list");
      }

      RemoveControllerEvent?.Invoke(Controller);
    }

    private Inv.DistinctList<Controller> ControllerList;
  }

  /// <summary>
  /// Enumeration of device platforms.
  /// </summary>
  public enum DeviceTarget
  {
    /// <summary>
    /// Android
    /// </summary>
    Android,
    /// <summary>
    /// iOS
    /// </summary>
    IOS,
    /// <summary>
    /// Universal Windows
    /// </summary>
    UniversalWindows,
    /// <summary>
    /// Windows Desktop
    /// </summary>
    WindowsDesktop
  }
}
