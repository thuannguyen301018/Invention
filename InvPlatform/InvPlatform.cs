﻿/*! 15 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  internal interface Platform
  {
    int ThreadAffinity();
    string CalendarTimeZoneName();
    void CalendarShowPicker(Inv.CalendarPicker CalendarPicker);
    bool EmailSendMessage(Inv.EmailMessage EmailMessage);
    bool PhoneIsSupported { get; }
    void PhoneDial(string PhoneNumber);
    void PhoneSMS(string PhoneNumber);
    long DirectoryGetLengthFile(Inv.File File);
    DateTime DirectoryGetLastWriteTimeUtcFile(Inv.File File);
    void DirectorySetLastWriteTimeUtcFile(Inv.File File, DateTime Timestamp);
    System.IO.Stream DirectoryCreateFile(Inv.File File);
    System.IO.Stream DirectoryAppendFile(Inv.File File);
    System.IO.Stream DirectoryOpenFile(Inv.File File);
    bool DirectoryExistsFile(Inv.File File);
    void DirectoryDeleteFile(Inv.File File);
    void DirectoryCopyFile(Inv.File SourceFile, Inv.File TargetFile);
    void DirectoryMoveFile(Inv.File SourceFile, Inv.File TargetFile);
    IEnumerable<Inv.File> DirectoryGetFolderFiles(Inv.Folder Folder, string FileMask);
    System.IO.Stream DirectoryOpenAsset(Inv.Asset Asset);
    bool DirectoryExistsAsset(Inv.Asset Asset);
    void DirectoryShowFilePicker(Inv.DirectoryFilePicker FilePicker);
    string DirectoryGetFolderPath(Inv.Folder Folder);
    string DirectoryGetFilePath(Inv.File File);
    bool LocationIsSupported { get; }
    void LocationLookup(Inv.LocationResult LocationLookup);
    void LocationShowMap(string Location);
    void AudioPlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan);
    TimeSpan AudioGetSoundLength(Inv.Sound Sound);
    void AudioPlayClip(Inv.AudioClip Clip);
    void AudioStopClip(Inv.AudioClip Clip);
    void WindowStartAnimation(Inv.Animation Animation);
    void WindowStopAnimation(Inv.Animation Animation);
    void WindowBrowse(Inv.File File);
    void WindowShare(Inv.File File);
    void WindowPost(Action Action);
    void WindowCall(Action Action);
    Inv.Dimension WindowGetDimension(Inv.Panel Panel);
    long ProcessMemoryUsedBytes();
    void ProcessMemoryReclamation();
    string ProcessAncillaryInformation();
    void WebClientConnect(Inv.WebClient Client);
    void WebClientDisconnect(Inv.WebClient Client);
    void WebServerConnect(Inv.WebServer Server);
    void WebServerDisconnect(Inv.WebServer Server);
    void WebBroadcastConnect(Inv.WebBroadcast Broadcast);
    void WebBroadcastDisconnect(Inv.WebBroadcast Broadcast);
    void WebInstallUri(Uri Uri);
    void WebLaunchUri(Uri Uri);
    void MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID);
    void VaultLoadSecret(Inv.Secret Secret);
    void VaultSaveSecret(Inv.Secret Secret);
    void VaultDeleteSecret(Inv.Secret Secret);
    string ClipboardGet();
    void ClipboardSet(string Text);
    Inv.Dimension GraphicsGetDimension(Inv.Image Image);
    Inv.Image GraphicsGrayscale(Inv.Image Image);
    Inv.Image GraphicsTint(Inv.Image Image, Inv.Colour Colour);
    Inv.Image GraphicsResize(Inv.Image Image, Inv.Dimension Dimension);
  }
}
