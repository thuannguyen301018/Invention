﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Market"/>
  /// </summary>
  public sealed class Market
  {
    internal Market(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Browse to the app listing in the current platform's app store.
    /// The parameter example are the values for the Invention Manual app.
    /// </summary>
    /// <param name="AppleiTunesID">eg. "1195335633"</param>
    /// <param name="GooglePlayID">eg. "com.x10host.invention.manual"</param>
    /// <param name="WindowsStoreID">eg. "808CallanHodgskin.InventionManual_ax6pb6c2q0eqa"</param>
    public void Browse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
      Application.Platform.MarketBrowse(AppleiTunesID, GooglePlayID, WindowsStoreID);
    }

    internal Application Application { get; private set; }
  }
}
