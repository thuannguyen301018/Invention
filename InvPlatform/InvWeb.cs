﻿/*! 42 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Collections;
using Inv.Support;
using System.Net.Http;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Web"/>.
  /// </summary>
  public sealed class Web
  {
    internal Web(Application Application)
    {
      this.Application = Application;
      this.HttpClient = new HttpClient(new HttpClientHandler() { CookieContainer = new CookieContainer() });
    }

    /// <summary>
    /// Install the app package located at the <paramref name="Uri"/>.
    /// </summary>
    /// <param name="Uri"></param>
    public void Install(Uri Uri)
    {
      Application.RequireThreadAffinity();

      Application.Platform.WebInstallUri(Uri);
    }
    /// <summary>
    /// Launch the <paramref name="Uri"/> in the default browser app.
    /// </summary>
    /// <param name="Uri"></param>
    public void Launch(Uri Uri)
    {
      Application.RequireThreadAffinity();

      Application.Platform.WebLaunchUri(Uri);
    }
    /// <summary>
    /// Create a new socket client.
    /// </summary>
    /// <param name="Host">Host name of the remote server.</param>
    /// <param name="Port">The listening port of the remote server.</param>
    /// <param name="CertHash">The thumbprint of the certificate. This can be null if you are not using SSL.</param>
    /// <returns></returns>
    public WebClient NewClient(string Host, int Port, byte[] CertHash)
    {
      return new WebClient(this, Host, Port, CertHash);
    }
    /// <summary>
    /// Create a new socket server.
    /// </summary>
    /// <param name="Host">Host name of this server.</param>
    /// <param name="Port">Listening port of this server.</param>
    /// <param name="CertHash">The thumbprint of the certificate. This can be null if you are not using SSL.</param>
    /// <returns></returns>
    public WebServer NewServer(string Host, int Port, byte[] CertHash)
    {
      return new WebServer(this, Host, Port, CertHash);
    }
    public WebBroadcast NewBroadcast()
    {
      return new WebBroadcast(this);
    }
    /// <summary>
    /// Create a web broker for interop with web sites and web services.
    /// </summary>
    /// <returns></returns>
    public WebBroker NewBroker(Uri BaseUri = null)
    {
      return new WebBroker(this)
      {
        BaseUri = BaseUri
      };
    }

    internal Application Application { get; }

    internal readonly HttpClient HttpClient;
  }

  public sealed class WebRequest : IDisposable
  {
    internal WebRequest(HttpClient Client, WebMethod Method, Uri Uri)
    {
      this.Client = Client;
      this.Method = Method;
      this.Uri = Uri;
      this.Headers = new WebHeaders();
      this.RequestMessage = new HttpRequestMessage();
    }
    public void Dispose()
    {
      RequestMessage?.Dispose();
    }

    public Uri Uri { get; }
    public string AuthorizationScheme { get; set;  }
    public string AuthorizationValue { get; set; }
    public WebMethod Method { get; }
    public WebHeaders Headers { get; }
    public WebContentType ContentType { get; private set; }
    public Encoding ContentEncoding { get; set; }
    
    public void AsPlainTextLine(string Line)
    {
      this.ContentEncoding = Encoding.UTF8;

      AsStream(WebContentTypes.TextPlain, S =>
      {
        using (var RequestWriter = CreateOpenStreamWriter(S))
          RequestWriter.WriteLine(Line);
      });
    }
    public void AsCsvTextLine(string[] Text)
    {
      this.ContentEncoding = Encoding.UTF8;

      AsStream(WebContentTypes.TextCsv, S =>
      {
        using (var RequestWriter = CreateOpenStreamWriter(S))
        using (var CsvWriter = new Inv.CsvWriter(RequestWriter))
          CsvWriter.WriteRecord(Text);
      });
    }
    public void AsText(string Value, Encoding Encoding, WebContentType ContentType)
    {
      this.ContentType = ContentType;
      this.ContentEncoding = Encoding;
      this.RequestMessage.Content = new StringContent(Value, ContentEncoding, ContentType.Name);
    }
    public void AsJson(object Content, JsonSerializer Serializer = null)
    {
      this.ContentEncoding = Encoding.UTF8;

      var Serialiser = Serializer ?? new JsonSerializer();

      AsStream(WebContentTypes.ApplicationJson, S =>
      {
        Serialiser.SaveToStream(Content, S, true);
      });
    }
    public void AsStream(WebContentType ContentType, Stream Stream, Encoding Encoding = null)
    {
      this.ContentType = ContentType;
      this.RequestMessage.Content = new StreamContent(Stream);
    }
    public void AsStream(WebContentType ContentType, Action<Stream> StreamAction)
    {
      this.ContentType = ContentType;
      this.RequestMessage.Content = new StreamActionContent(StreamAction);
    }
    public WebResponse Send()
    {
      RequestMessage.Method = Method.ToHttpMethod();
      RequestMessage.RequestUri = Uri;

      if (!string.IsNullOrWhiteSpace(AuthorizationScheme) && !string.IsNullOrWhiteSpace(AuthorizationValue))
        RequestMessage.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(AuthorizationScheme, AuthorizationValue);

      if (RequestMessage.Content != null)
        RequestMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentType.Name);

      if (RequestMessage.Content != null)
        RequestMessage.Content.Headers.ContentEncoding.Add(ContentEncoding.WebName);

      Exception RequestException = null;
      HttpResponseMessage ResponseMessage = null;
      try
      {
        ResponseMessage = Client.SendAsync(RequestMessage, HttpCompletionOption.ResponseHeadersRead).Result;
      }
      catch (Exception Ex)
      {
        RequestException = Ex;
      }
      
      var WebResponse = new Inv.WebResponse(ResponseMessage, Uri)
      {
        ContentLength = ResponseMessage?.Content.Headers.ContentLength,
        ContentType = ResponseMessage?.Content.Headers.ContentType?.MediaType,
        StatusCode = ResponseMessage?.StatusCode,
        Exception = RequestException
      };

      return WebResponse;
    }

    private StreamWriter CreateOpenStreamWriter(Stream InputStream)
    {
      return new System.IO.StreamWriter(InputStream, ContentEncoding ?? Encoding.UTF8, 1024, true);
    }

    private readonly HttpClient Client;
    private HttpRequestMessage RequestMessage;

    public class StreamActionContent : HttpContent
    {
      public StreamActionContent(Action<Stream> StreamAction)
      {
        this.StreamAction = StreamAction;
      }

      protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
      {
        StreamAction?.Invoke(stream);

        return Task.FromResult(true);
      }
      protected override bool TryComputeLength(out long length)
      {
        length = -1;
        return false;
      }

      private readonly Action<Stream> StreamAction;
    }
  }

  public sealed class WebResponse : IDisposable
  {
    internal WebResponse(HttpResponseMessage ResponseMessage, Uri Uri)
    {
      this.ResponseMessage = ResponseMessage;
      this.Uri = Uri;
      this.Headers = new WebHeaders();
    }
    public void Dispose()
    {
      ResponseMessage?.Dispose();
      ResponseMessage = null;
    }

    public Uri Uri { get; }
    public WebHeaders Headers { get; }
    public System.Net.HttpStatusCode? StatusCode { get; internal set; }
    public string ContentType { get; internal set; }
    public long? ContentLength { get; internal set; }
    public Exception Exception { get; internal set; }

    public System.IO.Stream AsStream()
    {
      ResponseAssert();

      return ResponseMessage?.Content?.ReadAsStreamAsync().Result;
    }
    public string AsPlainText()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return null;

      try
      {
        using (var ResponseStream = AsStream())
        using (var StreamReader = new System.IO.StreamReader(ResponseStream))
          return StreamReader.ReadToEnd().Trim();
      }
      finally
      {
        Dispose();
      }
    }
    public IEnumerable<string> AsPlainTextLines()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        yield return null;

      try
      {
        using (var ResponseStream = AsStream())
        using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
        {
          while (!ResponseReader.EndOfStream)
            yield return ResponseReader.ReadLine();
        }
      }
      finally
      {
        Dispose();
      }
    }
    public IEnumerable<string[]> AsCsvTextLines()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        yield return null;

      try
      {
        using (var ResponseStream = AsStream())
        using (var ResponseReader = new System.IO.StreamReader(ResponseStream))
        using (var CsvReader = new Inv.CsvReader(ResponseReader))
        {
          while (!ResponseReader.EndOfStream)
            yield return CsvReader.ReadRecord();
        }
      }
      finally
      {
        Dispose();
      }
    }
    public T AsJson<T>(JsonSerializer Serializer = null)
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return default(T);

      return Access(() =>
      {
        var Serialiser = Serializer ?? new JsonSerializer();
        try
        {
          return Serialiser.LoadFromStream<T>(AsStream());
        }
        catch (Exception Exception)
        {
          throw new AggregateException("Failed to deserialize Json", Exception);
        }
      });
    }
    public Inv.Image AsImage()
    {
      ResponseAssert();

      if (ResponseMessage == null)
        return null;

      var ServerContentLength = ContentLength ?? 0;

      using (var ResponseStream = AsStream())
      using (var MemoryStream = new MemoryStream(ServerContentLength > 0 ? (int)ServerContentLength : 0))
      {
        ResponseStream.CopyTo(MemoryStream);

        MemoryStream.Flush();

        return new Inv.Image(MemoryStream.ToArray(), System.IO.Path.GetExtension(Uri.AbsolutePath));
      }
    }

    private T Access<T>(Func<T> Query)
    {
      try
      {
        return Query();
      }
      finally
      {
        Dispose();
      }
    }
    private void ResponseAssert()
    {
      Debug.Assert(ResponseMessage != null, "Attempted to access the response without checking if it was sucessful.");
    }

    private HttpResponseMessage ResponseMessage;
  }

  public enum WebMethod
  {
    DELETE,
    GET,
    HEAD,
    OPTIONS,
    PATCH,
    POST,
    PUT,
    TRACE
  }

  public static class WebMethodHelper
  {
    public static HttpMethod ToHttpMethod(this WebMethod Method)
    {
      switch (Method)
      {
        case WebMethod.DELETE:
          return HttpMethod.Delete;
        case WebMethod.GET:
          return HttpMethod.Get;
        case WebMethod.HEAD:
          return HttpMethod.Head;
        case WebMethod.OPTIONS:
          return HttpMethod.Options;
        case WebMethod.PATCH:
          return new HttpMethod("PATCH");
        case WebMethod.POST:
          return HttpMethod.Post;
        case WebMethod.PUT:
          return HttpMethod.Put;
        case WebMethod.TRACE:
          return HttpMethod.Trace;
        default:
          throw new Exception("Unhandled WebMethod.");
      }
    }
  }

  public sealed class WebHeaders : IEnumerable<KeyValuePair<string, string>>
  {
    internal WebHeaders()
    {
      this.Dictionary = new Dictionary<string, string>();
    }

    public void Clear()
    {
      Dictionary.Clear();
    }
    public void Add(string Key, string Value)
    {
      Dictionary[Key] = Value;
    }
    public void Add(WebHeaders Headers)
    {
      foreach (var Entry in Headers)
        Dictionary[Entry.Key] = Entry.Value;
    }
    public void Remove(string Key)
    {
      Dictionary.Remove(Key);
    }

    public IEnumerator<KeyValuePair<string, string>> GetEnumerator() => Dictionary.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => Dictionary.GetEnumerator();

    private Dictionary<string, string> Dictionary;
  }

  /// <summary>
  /// Raw-socket client for implementing custom TCP protocols.
  /// </summary>
  public sealed class WebClient
  {
    internal WebClient(Web Web, string Host, int Port, byte[] CertHash)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
    }

    /// <summary>
    /// Connect to the server on <see cref="Host"/> and <see cref="Port"/>.
    /// </summary>
    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebClientConnect(this);
        this.IsActive = true;
      }
    }
    /// <summary>
    /// Disconnect from the server.
    /// </summary>
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebClientDisconnect(this);
      }
    }

    /// <summary>
    /// Host name.
    /// </summary>
    public string Host { get; }
    /// <summary>
    /// Listening port.
    /// </summary>
    public int Port { get; }
    /// <summary>
    /// Certificate thumbprint for SSL (otherwise should be null).
    /// </summary>
    public byte[] CertHash { get; }
    /// <summary>
    /// Stream for reading.
    /// </summary>
    public Stream InputStream { get; private set; }
    /// <summary>
    /// Stream for writing.
    /// </summary>
    public Stream OutputStream { get; private set; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    private Web Web;
    private bool IsActive;
  }

  /// <summary>
  /// Raw-socket server for implementing custom TCP protocols.
  /// </summary>
  public sealed class WebServer
  {
    internal WebServer(Web Web, string Host, int Port, byte[] CertHash)
    {
      this.Web = Web;
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
      this.ChannelDictionary = new Dictionary<object, WebChannel>();
    }

    /// <summary>
    /// This is fired when a new client connects to the server.
    /// </summary>
    public event Action<WebChannel> AcceptEvent;
    /// <summary>
    /// This is fired when a client disconnects from the server.
    /// </summary>
    public event Action<WebChannel> RejectEvent;

    /// <summary>
    /// Start listening for clients on <see cref="Host"/> and <see cref="Port"/>.
    /// </summary>
    public void Connect()
    {
      if (!IsActive)
      {
        Web.Application.Platform.WebServerConnect(this);
        this.IsActive = true;
      }
    }
    /// <summary>
    /// Stop listening for clients.
    /// </summary>
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;
        Web.Application.Platform.WebServerDisconnect(this);
      }
    }

    /// <summary>
    /// Host name.
    /// </summary>
    public string Host { get; }
    /// <summary>
    /// Listening port.
    /// </summary>
    public int Port { get; }
    /// <summary>
    /// Certificate thumbprint for SSL (otherwise should be null).
    /// </summary>
    public byte[] CertHash { get; }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }
    internal Action<object> DropDelegate { get; set; }

    internal void AcceptChannel(object Node, System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      var Result = new WebChannel(this);

      lock (ChannelDictionary)
      {
        Result.Node = Node;
        Result.SetStreams(InputStream, OutputStream);

        ChannelDictionary.Add(Node, Result);
      }

      AcceptInvoke(Result);
    }
    internal void RejectChannel(object Node)
    {
      WebChannel Result;

      lock (ChannelDictionary)
      {
        Result = ChannelDictionary.GetValueOrDefault(Node);

        if (Result != null)
        {
          ChannelDictionary.Remove(Node);

          Result.SetStreams(null, null);
        }
      }

      if (Result != null)
        RejectInvoke(Result);
    }
    internal void DropChannel(WebChannel WebChannel)
    {
      if (DropDelegate != null)
        DropDelegate(WebChannel.Node);
    }

    private void AcceptInvoke(WebChannel WebChannel)
    {
      if (AcceptEvent != null)
        AcceptEvent(WebChannel);
    }
    private void RejectInvoke(WebChannel WebChannel)
    {
      if (RejectEvent != null)
        RejectEvent(WebChannel);
    }

    private Web Web;
    private bool IsActive;
    private Dictionary<object, WebChannel> ChannelDictionary;
  }

  /// <summary>
  /// Represents a connected client on the server.
  /// </summary>
  public sealed class WebChannel
  {
    internal WebChannel(WebServer WebServer)
    {
      this.WebServer = WebServer;
    }

    /// <summary>
    /// Stream for reading.
    /// </summary>
    public Stream InputStream { get; private set; }
    /// <summary>
    /// Stream for writing.
    /// </summary>
    public Stream OutputStream { get; private set; }

    /// <summary>
    /// Disconnect this client.
    /// </summary>
    public void Drop()
    {
      WebServer.DropChannel(this);
    }

    // NOTE: set by each platform implementation.
    internal object Node { get; set; }

    internal void SetStreams(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    private WebServer WebServer;
  }

  public interface IWebBroadcast
  {
    event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    void Close();
    void Bind(WebEndpoint LocalEndpoint);
    void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint);
    void JoinMulticastGroup(string MulticastIpAddress);
    void LeaveMulticastGroup(string MulticastIpAddress);
    void StartReceive();
  }

  public sealed class WebBroadcast
  {
    internal WebBroadcast(Web Web)
    {
      this.Web = Web;
    }

    public event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    public void Close()
    {
      Broadcast.Close();
    }
    public void Bind(WebEndpoint LocalEndpoint)
    {
      Broadcast.Bind(LocalEndpoint);
    }
    public void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint)
    {
      Broadcast.SendTo(Datagram, RemoteEndpoint);
    }
    public void JoinMulticastGroup(string MulticastIpAddress)
    {
      Broadcast.JoinMulticastGroup(MulticastIpAddress);
    }
    public void LeaveMulticastGroup(string MulticastIpAddress)
    {
      Broadcast.LeaveMulticastGroup(MulticastIpAddress);
    }
    public void StartReceive()
    {
      Broadcast.StartReceive();
    }

    internal object Node { get; set; }

    internal void SetBroadcast(IWebBroadcast InputBroadcast)
    {
      this.Broadcast.ReceiveEvent -= HandleBroadcastReceive;
      this.Broadcast = InputBroadcast;

      if (this.Broadcast != null)
        this.Broadcast.ReceiveEvent += HandleBroadcastReceive;
    }

    private void HandleBroadcastReceive(WebDatagram D, WebEndpoint E)
    {
      ReceiveEvent?.Invoke(D, E);
    }

    private readonly Web Web;
    private IWebBroadcast Broadcast;
  }

  public sealed class WebDatagram
  {
    public WebDatagram(byte[] Buffer)
    {
      this.Buffer = Buffer;
      this.Length = Buffer.Length;
    }

    public byte[] Buffer { get; }
    public int Length { get; }
  }

  public sealed class WebEndpoint
  {
    public WebEndpoint(string Address, int Port)
    {
      this.Address = Address;
      this.Port = Port;
    }

    public string Address { get; }
    public int Port { get; }
  }

  public sealed class WebContentType
  {
    internal WebContentType(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }

    public static WebContentType New(string Name)
    {
      return new WebContentType(Name);
    }
  }

  public static class WebContentTypes
  {
    public static WebContentType MultiPartFormData = WebContentType.New("multipart/form-data");
    public static WebContentType TextPlain = WebContentType.New("text/plain");
    public static WebContentType TextHtml = WebContentType.New("text/html");
    public static WebContentType TextCsv = WebContentType.New("text/csv");
    public static WebContentType ApplicationJson = WebContentType.New("application/json");
    public static WebContentType ApplicationXml = WebContentType.New("application/xml");
    public static WebContentType ApplicationXWwwFormUrlEncoded = WebContentType.New("application/x-www-form-urlencoded");
    public static WebContentType ApplicationOctetStream = WebContentType.New("application/octet-stream");
  }

  /// <summary>
  /// Broker for interop with web sites and web services.
  /// </summary>
  public sealed class WebBroker
  {
    internal WebBroker(Web Web)
    {
      this.Web = Web;
      this.Headers = new WebHeaders();

      //System.Net.WebRequest.DefaultWebProxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    /// <summary>
    /// Base Uri for all requests.
    /// </summary>
    public Uri BaseUri { get; set; }
    /// <summary>
    /// Additional headers to apply to the request.
    /// </summary>
    public WebHeaders Headers { get; }
    public Newtonsoft.Json.JsonSerializerSettings JsonSerializerSettings { get; set; }

    public WebRequest GET(string Path)
    {
      return NewRequest(WebMethod.GET, Path);
    }
    public WebRequest POST(string Path)
    {
      return NewRequest(WebMethod.POST, Path);
    }
    public WebRequest PUT(string Path)
    {
      return NewRequest(WebMethod.PUT, Path);
    }
    public WebRequest DELETE(string Path)
    {
      return NewRequest(WebMethod.DELETE, Path);
    }
    public WebRequest HEAD(string Path)
    {
      return NewRequest(WebMethod.HEAD, Path);
    }
    public WebRequest OPTIONS(string Path)
    {
      return NewRequest(WebMethod.OPTIONS, Path);
    }
    public WebRequest PATCH(string Path)
    {
      return NewRequest(WebMethod.PATCH, Path);
    }
    public WebRequest TRACE(string Path)
    {
      return NewRequest(WebMethod.TRACE, Path);
    }

    private WebRequest NewRequest(WebMethod WebMethod, string RelativePath)
    {
      var Uri = BaseUri != null ? new Uri(BaseUri, RelativePath) : new Uri(RelativePath);

      var Request = new WebRequest(Web.HttpClient, WebMethod, Uri);
      Request.Headers.Add(Headers);

      return Request;
    }

    private readonly Web Web;
  }
}