﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Clipboard"/>
  /// </summary>
  public sealed class Clipboard
  {
    internal Clipboard(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Read and write unicode text from the shared clipboard.
    /// </summary>
    public string Text
    {
      get { return Application.Platform.ClipboardGet(); }
      set { Application.Platform.ClipboardSet(value); }
    }

    private Application Application;
  }
}
