﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Sockets;
using System.Net;

namespace Inv.Udp
{
#if WINDOWS_UWP
  public sealed class Broadcast : IWebBroadcast, IDisposable
  {
    public Broadcast()
    {
      this.UdpClient = new System.Net.Sockets.UdpClient();
    }

    public event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    public void Close()
    {
      if (!IsClosed)
      {
        IsClosed = true;
        try
        {
          UdpClient.Dispose();
        }
        catch (Exception)
        {
        }
      }
    }
    public void Bind(WebEndpoint LocalEndpoint)
    {
      UdpClient.MulticastLoopback = false;
      UdpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
      UdpClient.Client.Bind(ToNetEndpoint(LocalEndpoint));
    }
    public void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint)
    {
      UdpClient.SendAsync(Datagram.Buffer, Datagram.Length, ToNetEndpoint(RemoteEndpoint)).Wait();
    }
    public void JoinMulticastGroup(string MulticastIpAddress)
    {
      UdpClient.JoinMulticastGroup(IPAddress.Parse(MulticastIpAddress));
    }
    public void LeaveMulticastGroup(string MulticastIpAddress)
    {
      UdpClient.DropMulticastGroup(IPAddress.Parse(MulticastIpAddress));
    }
    public void StartReceive()
    {
      //UdpClient.ReceiveAsync().ContinueWith();
    }

    private void ReceiveCallback(IAsyncResult Result)
    {
      var RemoteEndpoint = new IPEndPoint(IPAddress.None, 0);

      byte[] ReceivedByteArray = null;
      try
      {
        //ReceivedByteArray = UdpClient.EndReceive(Result, ref RemoteEndpoint);
      }
      catch (ObjectDisposedException)
      {
        if (IsClosed)
          return;

        throw;
      }

      if (ReceivedByteArray != null && ReceivedByteArray.Length > 0)
        ReceiveEvent?.Invoke(new WebDatagram(ReceivedByteArray), ToWebEndpoint(RemoteEndpoint));

      //UdpClient.BeginReceive(ReceiveCallback, null);
    }

    private static IPEndPoint ToNetEndpoint(WebEndpoint Endpoint)
    {
      return new IPEndPoint(IPAddress.Parse(Endpoint.Address), Endpoint.Port);
    }
    private static WebEndpoint ToWebEndpoint(IPEndPoint Endpoint)
    {
      return new WebEndpoint(Endpoint.Address.ToString(), Endpoint.Port);
    }

    void IDisposable.Dispose()
    {
      Close();
    }

    private System.Net.Sockets.UdpClient UdpClient;
    private bool IsClosed;
  }
#else

  public sealed class Broadcast : IWebBroadcast, IDisposable
  {
    public Broadcast()
    {
      this.UdpClient = new System.Net.Sockets.UdpClient();
    }

    public event Action<WebDatagram, WebEndpoint> ReceiveEvent;

    public void Close()
    {
      if (!IsClosed)
      {
        IsClosed = true;
        try
        {
          UdpClient.Close();
        }
        catch (Exception)
        {
        }
      }
    }
    public void Bind(WebEndpoint LocalEndpoint)
    {
      UdpClient.MulticastLoopback = false;
      UdpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
      UdpClient.Client.Bind(ToNetEndpoint(LocalEndpoint));
    }
    public void SendTo(WebDatagram Datagram, WebEndpoint RemoteEndpoint)
    {
      UdpClient.Send(Datagram.Buffer, Datagram.Length, ToNetEndpoint(RemoteEndpoint));
    }
    public void JoinMulticastGroup(string MulticastIpAddress)
    {
      UdpClient.JoinMulticastGroup(IPAddress.Parse(MulticastIpAddress));
    }
    public void LeaveMulticastGroup(string MulticastIpAddress)
    {
      UdpClient.DropMulticastGroup(IPAddress.Parse(MulticastIpAddress));
    }
    public void StartReceive()
    {
      UdpClient.BeginReceive(ReceiveCallback, null);
    }

    private void ReceiveCallback(IAsyncResult Result)
    {
      var RemoteEndpoint = new IPEndPoint(IPAddress.None, 0);

      byte[] ReceivedByteArray = null;
      try
      {
        ReceivedByteArray = UdpClient.EndReceive(Result, ref RemoteEndpoint);
      }
      catch (ObjectDisposedException)
      {
        if (IsClosed)
          return;

        throw;
      }

      if (ReceivedByteArray != null && ReceivedByteArray.Length > 0)
        ReceiveEvent?.Invoke(new WebDatagram(ReceivedByteArray), ToWebEndpoint(RemoteEndpoint));

      UdpClient.BeginReceive(ReceiveCallback, null);
    }

    private static IPEndPoint ToNetEndpoint(WebEndpoint Endpoint)
    {
      return new IPEndPoint(IPAddress.Parse(Endpoint.Address), Endpoint.Port);
    }
    private static WebEndpoint ToWebEndpoint(IPEndPoint Endpoint)
    {
      return new WebEndpoint(Endpoint.Address.ToString(), Endpoint.Port);
    }

    void IDisposable.Dispose()
    {
      Close();
    }

    private System.Net.Sockets.UdpClient UdpClient;
    private bool IsClosed;
  }
#endif
}
