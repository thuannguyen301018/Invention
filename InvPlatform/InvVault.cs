﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Vault"/>.
  /// </summary>
  public sealed class Vault
  {
    internal Vault(Inv.Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Create a new secret with a name.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public Secret NewSecret(string Name)
    {
      return new Secret(this, Name);
    }

    internal Application Application { get; private set; }
  }

  /// <summary>
  /// Secrets can be stored in a platform-specific secure storage such as a device key-chain.
  /// </summary>
  public sealed class Secret
  {
    internal Secret(Inv.Vault Vault, string Name)
    {
      this.Vault = Vault;
      this.Name = Name;
      this.Properties = new Dictionary<string, string>();
    }

    /// <summary>
    /// Name of the secret.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// Properties attached to the secret.
    /// </summary>
    public Dictionary<string, string> Properties { get; private set; }

    /// <summary>
    /// Save the secret properties to the platform-specific secure storage.
    /// </summary>
    public void Save()
    {
      Vault.Application.Platform.VaultSaveSecret(this);
    }
    /// <summary>
    /// Load the secret properties to the platform-specific secure storage.
    /// </summary>
    public void Load()
    {
      Vault.Application.Platform.VaultLoadSecret(this);
    }
    /// <summary>
    /// Delete the secret properties to the platform-specific secure storage.
    /// </summary>
    public void Delete()
    {
      Vault.Application.Platform.VaultDeleteSecret(this);
    }

    internal string Serialize()
    {
      var sb = new StringBuilder();

      foreach (var Property in Properties)
      {
        sb.Append(Uri.EscapeDataString(Property.Key));
        sb.Append("=");
        sb.Append(Uri.EscapeDataString(Property.Value));
        sb.Append("&");
      }

      return sb.ToString();
    }
    internal void Deserialize(string serializedString)
    {
      Properties.Clear();

      foreach (var p in serializedString.Split(new [] { '&' }, StringSplitOptions.RemoveEmptyEntries))
      {
        var kv = p.Split(new [] { '=' }, 2);

        if (kv.Length == 2)
        {
          var PropertyKey = Uri.UnescapeDataString(kv[0]);
          var PropertyValue = kv.Length > 1 ? Uri.UnescapeDataString(kv[1]) : "";

          Properties.Add(PropertyKey, PropertyValue);
        }
      }
    }

    private Vault Vault;
  }
}