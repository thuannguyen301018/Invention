﻿/*! 8 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public static class GenShell
  {
    public static void Execute(string Input)
    {
      var SW = System.Diagnostics.Stopwatch.StartNew();

      var Argument = Input;

      if (Path.GetExtension(Argument).ToLower() == ".csproj")
      {
        var Project = new Inv.CSharpProject();
        Project.LoadFromFile(Argument);

        foreach (var Reference in Project.IncludeReference)
        {
          if (Path.GetExtension(Reference.File.Path).ToLower() == ".invresourcepackage")
          {
            Argument = Path.Combine(Path.GetDirectoryName(Argument), Reference.File.Path);
            break;
          }
        }

        if (Argument == Input)
        {
          //WriteLine("Install the Invention VSIX if you want to add a resource package to project " + Argument);
          return;
        }
      }
      else if (Path.GetExtension(Argument).ToLower() != ".invresourcepackage")
      {
        WriteLine("Argument must be a .InvResourcePackage or a .csproj that contains a .InvResourcePackage: " + Argument);
        return;
      }

      if (!System.IO.File.Exists(Argument))
      {
        WriteLine("Argument file path does not exist: " + Argument);
        Environment.ExitCode = 1;
        return;
      }

      var ArgumentFilePath = GetExactPathName(Argument); // NOTE: VisualStudio right click 'Shell (Open)' makes the argument lower case for some reason.

      WriteLine(ArgumentFilePath + " started generation ...");

      var SourceFolderPath = Path.GetDirectoryName(ArgumentFilePath);
      var RootFolderName = Path.GetFileName(SourceFolderPath);
      var DataNamespace = Path.GetFileNameWithoutExtension(ArgumentFilePath);
      var CodeNamespace = System.IO.File.ReadAllText(ArgumentFilePath).Trim().EmptyAsNull() ?? DataNamespace;
      var RsFilePath = Argument + ".rs";
      var CsFilePath = Argument + ".cs";

      var ResourcePackage = new Inv.Resource.Header()
      {
        DirectoryList = new Inv.DistinctList<Inv.Resource.DirectoryHeader>()
      };

      var ContentList = new Inv.DistinctList<byte[]>();
      var InsightDictionary = new Dictionary<Inv.Resource.FileHeader, string>();

      foreach (var DirectoryInfo in new DirectoryInfo(SourceFolderPath).GetDirectories())
      {
        var ResourceDirectory = new Inv.Resource.DirectoryHeader();
        ResourceDirectory.Name = DirectoryInfo.Name.ConvertToCSharpIdentifier();
        ResourceDirectory.FileList = new Inv.DistinctList<Inv.Resource.FileHeader>();

        //WriteLine(ResourceDirectory.Name + " importing ...");

        foreach (var FileInfo in DirectoryInfo.GetFiles())
        {
          var Name = Path.GetFileNameWithoutExtension(FileInfo.Name).Replace('.', ' ').Replace('-', ' ').PascalCaseToTitleCase().ToTitleCase().ConvertToCSharpIdentifier();
          var Content = System.IO.File.ReadAllBytes(FileInfo.FullName);
          var Format = Inv.Resource.FileFormat.Binary;

          //WriteLine(ResourceDirectory.Name + "\\" + Name);

          switch (FileInfo.Extension.ToLower())
          {
            case ".mp3":
            case ".wav":
              Format = Inv.Resource.FileFormat.Sound;
              break;

            case ".png":
            case ".jpg":
            case ".jpeg":
            case ".bmp":
            case ".tif":
            case ".tiff":
            case ".gif":
              Format = Inv.Resource.FileFormat.Image;
              break;

            case ".sql":
            case ".txt":
            case ".csv":
              Format = Inv.Resource.FileFormat.Text;
              break;

            default:
              System.Diagnostics.Debug.WriteLine("File not handled " + FileInfo.Extension);
              break;
          }

          var ResourceFile = new Inv.Resource.FileHeader()
          {
            Name = Name,
            Format = Format,
            Extension = FileInfo.Extension.ToLower(),
            Length = (int)Content.Length
          };
          ResourceDirectory.FileList.Add(ResourceFile);

          ContentList.Add(Content);

          InsightDictionary.Add(ResourceFile, Format.GetInsightText(FileInfo.FullName));
        }

        if (ResourceDirectory.FileList.Count > 0)
          ResourcePackage.DirectoryList.Add(ResourceDirectory);
      }

      var FormatTypeArray = new Inv.EnumArray<Inv.Resource.FileFormat, string>
      {
        { Inv.Resource.FileFormat.Text, "global::Inv.Resource.TextReference" },
        { Inv.Resource.FileFormat.Sound, "global::Inv.Resource.SoundReference" },
        { Inv.Resource.FileFormat.Image, "global::Inv.Resource.ImageReference" },
        { Inv.Resource.FileFormat.Binary, "global::Inv.Resource.BinaryReference" }
      };

      var CsFileInfo = new FileInfo(CsFilePath);

      var OldText = CsFileInfo.Exists ? System.IO.File.ReadAllText(CsFileInfo.FullName) : null;

      string NewText;

      using (var StringWriter = new StringWriter())
      {
        StringWriter.WriteLine("namespace " + CodeNamespace);
        StringWriter.WriteLine("{");
        StringWriter.WriteLine("  public static class {0}", RootFolderName);
        StringWriter.WriteLine("  {");
        StringWriter.WriteLine("    static {0}()", RootFolderName);
        StringWriter.WriteLine("    {");
        StringWriter.WriteLine("      global::Inv.Resource.Foundation.Import(typeof({0}), \"{0}.{1}.InvResourcePackage.rs\");", RootFolderName, DataNamespace);
        StringWriter.WriteLine("    }");
        StringWriter.WriteLine("");
        foreach (var Directory in ResourcePackage.DirectoryList)
          StringWriter.WriteLine("    public static readonly {0}{1} {1};", RootFolderName, Directory.Name);
        StringWriter.WriteLine("  }");

        foreach (var ResourceDirectory in ResourcePackage.DirectoryList)
        {
          StringWriter.WriteLine("");
          StringWriter.WriteLine("  public sealed class {0}{1}", RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("  {");
          StringWriter.WriteLine("    public {0}{1}() {{ }}", RootFolderName, ResourceDirectory.Name);
          StringWriter.WriteLine("");
          foreach (var ResourceFile in ResourceDirectory.FileList)
          {
            StringWriter.WriteLine("    ///<Summary>({0}) {1} ({2:F1}KB)</Summary>", ResourceFile.Extension, InsightDictionary[ResourceFile], ResourceFile.Length / 1024.0);
            StringWriter.WriteLine("    public readonly {0} {1};", FormatTypeArray[ResourceFile.Format], ResourceFile.Name);
          }
          StringWriter.WriteLine("  }");
        }
        StringWriter.Write("}");

        NewText = StringWriter.ToString();
      }

      WriteLine("Next .cs length = " + NewText.Length.ToString("N0"));

      if (OldText == null || OldText != NewText)
      {
        if (CsFileInfo.Exists && CsFileInfo.IsReadOnly)
          CsFileInfo.IsReadOnly = false;

        System.IO.File.WriteAllText(CsFileInfo.FullName, NewText);
      }

      byte[] NewData;
      using (var MemoryStream = new MemoryStream())
      {
        var ResourceGovernor = new Inv.Resource.Governor();
        ResourceGovernor.Save(ResourcePackage, MemoryStream);

        // allocate the remainder stream in one go.
        var FullCapacity = (int)(MemoryStream.Position + ContentList.Sum(C => C.Length));
        MemoryStream.Capacity = FullCapacity;

        // TODO: header marker?

        foreach (var Content in ContentList)
        {
          // TODO: content marker?

          MemoryStream.Write(Content, 0, Content.Length);
        }

        MemoryStream.Flush();

        System.Diagnostics.Debug.Assert(MemoryStream.Capacity == FullCapacity, "Estimated full capacity was not correct.");

        NewData = MemoryStream.ToArray();
      }

      WriteLine("Next .rs length = " + Inv.DataSize.FromBytes(NewData.Length).ToString());

      var RsFileInfo = new FileInfo(RsFilePath);
      var OldData = RsFileInfo.Exists ? System.IO.File.ReadAllBytes(RsFilePath) : null;

      if (OldData == null || !OldData.ShallowEqualTo(NewData))
      {
        if (RsFileInfo.Exists && RsFileInfo.IsReadOnly)
          RsFileInfo.IsReadOnly = false;

        System.IO.File.WriteAllBytes(RsFilePath, NewData);
      }

      SW.Stop();

      WriteLine(ArgumentFilePath + " generated in " + SW.ElapsedMilliseconds.ToString("N0") + " ms.");
    }

    private static void WriteLine(string Text)
    {
      Console.WriteLine(Text);
      System.Diagnostics.Debug.WriteLine(Text);
    }

    private static string GetExactPathName(string pathName)
    {
      if (!(System.IO.File.Exists(pathName) || System.IO.Directory.Exists(pathName)))
        return pathName;

      var di = new DirectoryInfo(pathName);

      if (di.Parent != null)
        return Path.Combine(GetExactPathName(di.Parent.FullName), di.Parent.GetFileSystemInfos(di.Name)[0].Name);
      else
        return di.Name.ToUpper();
    }
  }
}
