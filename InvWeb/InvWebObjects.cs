﻿/*! 15 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal abstract class WebObject
  {
    internal WebObject(object Handle)
    {
      this.Handle = Handle;
    }

    internal readonly dynamic Handle;

    internal void AddNativeHandler(string eventName, Action<object> handler)
    {
      AddEventListener(eventName, handler);
    }
    internal void RemoveNativeHandler(string eventName, Action<object> handler)
    {
      RemoveEventListener(eventName, handler);
    }

    [JSIL.Meta.JSReplacement("$this.Handle.addEventListener($name, $handler)")]
    private void AddEventListener(string name, Action<object> handler)
    {
    }

    [JSIL.Meta.JSReplacement("$this.Handle.removeEventListener($name, $handler)")]
    private void RemoveEventListener(string name, Action<object> handler)
    {
    }

    internal static readonly dynamic undefined = JSIL.Builtins.Global["$$NotExist"];
  }

  internal sealed class WebWindow : WebObject
  {
    public WebWindow()
      : this((object)JSIL.Builtins.Global["window"])
    {
    }
    internal WebWindow(object Handle)
      : base(Handle)
    {
      this.AudioSourceDictionary = new Dictionary<object, object>();

      AddNativeHandler("load", e =>
      {
        if (LoadEvent != null)
          LoadEvent();
      });
      AddNativeHandler("resize", e =>
      {
        if (ResizeEvent != null)
          ResizeEvent();
      });
      AddNativeHandler("beforeunload", e =>
      {
        if (CloseEvent != null)
          CloseEvent();
      });
      AddNativeHandler("keydown", e =>
      {
        dynamic Event = e;

        this.IsShiftPressed = Event.shiftKey;
        this.IsCtrlPressed = Event.ctrlKey;
        this.IsAltPressed = Event.altKey;
      });
    }

    public int Width
    {
      get { return Handle.innerWidth; }
    }
    public int Height
    {
      get { return Handle.innerHeight; }
    }

    public event Action LoadEvent;
    public event Action ResizeEvent;
    public event Action CloseEvent;
    public bool IsShiftPressed { get; private set; }
    public bool IsCtrlPressed { get; private set; }
    public bool IsAltPressed { get; private set; }

    public void Close()
    {
      Handle.close();
    }
    public WebTimer SetInterval(int Delay, Action Action)
    {
      var Result = new WebTimer(this);
      Result.IntervalEvent += Action;
      Result.IntervalTime = TimeSpan.FromMilliseconds(Delay);
      Result.Start();

      return Result;
    }
    public long PerformanceMemoryBytes()
    {
      var Memory = Handle.performance.memory;
      
      if (Memory == undefined)
        return 0L;
      else
        return Memory.totalJSHeapSize;
    }
    public void RequestFileSystem(int SizeInMegabytes)
    {
      // TODO: 
      //Handle.RequestFileSystem(Handle.PERSISTENT, SizeInMegabytes * 1024 * 1024);
    }
    public void PlayAudio(object Key, byte[] Buffer, float Volume)
    {
      // NOTE: have only confirmed support in Chrome.
      // http://www.html5rocks.com/en/tutorials/webaudio/intro/

      if (AudioContext == null)
      {
        var AudioClass = Handle.AudioContext;

        if (AudioClass != WebObject.undefined)
          this.AudioContext = JSIL.Verbatim.Expression("new $0()", AudioClass);
      }

      if (AudioContext != null)
      {
        var AudioBuffer = AudioSourceDictionary.GetValueOrDefault(Key);

        if (AudioBuffer != null)
        {
          AudioDecodePlay(AudioBuffer, Volume);
        }
        else
        {
          dynamic AudioBufferDynamic = Buffer;
          object AudioBufferObject = AudioBufferDynamic.buffer;
          Action<object, float, object> AudioDecodeCompleteAction = AudioDecodeComplete;

          AudioContext.decodeAudioData(AudioBufferObject, JSIL.Verbatim.Expression("function (buffer) { $0(buffer, $1, $2); }", AudioDecodeCompleteAction, Volume, Key));
        }
      }
    }
    public WebTimer NewTimer()
    {
      return new WebTimer(this);
    }

    private void AudioDecodeComplete(object AudioBuffer, float Volume, object Key)
    {
      AudioSourceDictionary[Key] = AudioBuffer;
      AudioDecodePlay(AudioBuffer, Volume);
    }
    private void AudioDecodePlay(object AudioBuffer, float Volume)
    {
      var AudioSource = AudioContext.createBufferSource();
      AudioSource.buffer = AudioBuffer;
      AudioSource.connect(AudioContext.destination);

      if (Volume != 1.0F)
      {
        var AudioVolume = AudioContext.createGain();
        AudioSource.connect(AudioVolume);
        AudioVolume.connect(AudioContext.destination);
        AudioVolume.gain.value = Volume;
      }

      AudioSource.start(0);
    }

    private dynamic AudioContext;
    private Dictionary<object, object> AudioSourceDictionary;
  }

  internal sealed class WebTimer
  {
    internal WebTimer(WebWindow Window)
    {
      this.Window = Window;
      this.IntervalTimeField = TimeSpan.FromSeconds(1);
    }

    public event Action IntervalEvent;
    public TimeSpan IntervalTime
    {
      get { return IntervalTimeField; }
      set
      {
        if (IntervalTimeField != value)
        {
          this.IntervalTimeField = value;

          if (Handle != null)
          {
            Stop();
            Start();
          }
        }
      }
    }
    public bool Enabled
    {
      get { return Handle != null; }
    }

    public void Start()
    {
      if (this.Handle == null)
        this.Handle = Window.Handle.setInterval((Action)IntervalInvoke, IntervalTime.TotalMilliseconds);
    }
    public void Stop()
    {
      if (this.Handle != null)
      {
        Window.Handle.clearTimeout(Handle);
        this.Handle = null;
      }
    }

    private void IntervalInvoke()
    {
      if (IntervalEvent != null)
        IntervalEvent();
    }

    private WebWindow Window;
    private dynamic Handle;
    private TimeSpan IntervalTimeField;
  }

  internal abstract class WebElement : WebObject
  {
    internal WebElement(object Handle)
      : base(Handle)
    {
    }

    public string Class
    {
      get { return Handle.@className; }
      set { Handle.@className = value; }
    }

    public event Action<WebKeystroke> KeyDownEvent
    {
      add
      {
        if (value != null)
        {
          if (KeyDownAction == null)
            AddNativeHandler("keydown", KeyDownHandler);

          this.KeyDownAction += value;
        }
      }
      remove
      {
        if (value != null && KeyDownAction != null)
        {
          this.KeyDownAction -= value;

          if (KeyDownAction == null)
            RemoveNativeHandler("keydown", KeyDownHandler);
        }
      }
    }

    public void AddElement(WebElement Element)
    {
      Handle.appendChild(Element.Handle);
    }
    public void RemoveElement(WebElement Element)
    {
      Handle.removeChild(Element.Handle);
    }
    public void RemoveElements()
    {
      while (Handle.hasChildNodes())
        Handle.removeChild(Handle.lastChild);
    }
    public bool HasElement(WebElement Element)
    {
      return Element.Handle.parentNode == Handle;
    }

    private void KeyDownHandler(object e)
    {
      if (KeyDownAction != null)
      {
        dynamic Event = e;

        KeyDownAction(new WebKeystroke()
        {
          Code = Event.keyCode,
          IsAlt = Event.altKey,
          IsCtrl = Event.ctrlKey,
          IsShift = Event.shiftKey
        });
      }
    }

    private Action<WebKeystroke> KeyDownAction;
  }

  internal sealed class WebHtml : WebElement
  {
    internal WebHtml(object Handle)
      : base(Handle)
    {
    }
  }

  internal sealed class WebKeystroke
  {
    public int Code { get; internal set; }
    public bool IsCtrl { get; internal set; }
    public bool IsAlt { get; internal set; }
    public bool IsShift { get; internal set; }
  }

  internal sealed class WebHead : WebElement
  {
    internal WebHead(object Handle)
      : base(Handle)
    {
    }

    public string Title
    {
      get { return Handle.title; }
      set { Handle.title = value; }
    }

    public WebStyle GetStyle()
    {
      return new WebStyle((object)Handle.style);
    }
  }

  internal sealed class WebStyle : WebElement
  {
    internal WebStyle(object Handle)
      : base(Handle)
    {
    }

    public void AddRule(string Target, string Rule)
    {
      Handle.styleSheet.insertRule(Target + "{ " + Rule + "}", -1);
    }
  }

  internal sealed class WebBody : WebElement
  {
    internal WebBody(object Handle)
      : base(Handle)
    {
    }
  }

  internal sealed class WebDocument : WebObject
  {
    public WebDocument()
      : this((object)JSIL.Builtins.Global["document"])
    {
    }
    internal WebDocument(object Handle)
      : base(Handle)
    {
      AddNativeHandler("contextmenu", e =>
      {
        // if it bubbles up to the window, stop it from showing the context menu.
        dynamic Event = e;
        Event.preventDefault();
      });
    }

    public bool IsHtml5
    {
      get { return string.Equals(GetDoctype(), "<!DOCTYPE html>", StringComparison.CurrentCultureIgnoreCase); }
    }

    public WebHtml GetHtml()
    {
      return new WebHtml((object)Handle.getElementsByTagName("html")[0]);
    }
    public WebHead GetHead()
    {
      return new WebHead((object)Handle.head);
    }
    public WebBody GetBody()
    {
      return new WebBody((object)Handle.getElementsByTagName("body")[0]);
    }
    public WebDiv NewDiv()
    {
      return new WebDiv(this);
    }
    public WebCanvas NewCanvas()
    {
      return new WebCanvas(this);
    }
    public WebButton NewButton()
    {
      return new WebButton(this);
    }
    public WebLabel NewLabel()
    {
      return new WebLabel(this);
    }
    public WebTable NewTable()
    {
      return new WebTable(this);
    }
    public WebImage NewImage()
    {
      return new WebImage(this);
    }
    public WebInput NewTextInput()
    {
      return new WebInput(this)
      {
        Type = "text"
      };
    }
    public WebTextArea NewTextArea()
    {
      return new WebTextArea(this);
    }
    public WebAudio NewAudio()
    {
      return new WebAudio(this);
    }

    private string GetDoctype()
    {
      var node = Handle.doctype;

      // TODO: The unary operator 'IsFalse' is not implemented
      /*
      return 
        "<!DOCTYPE "
        + node.name
        + (node.publicId ? " PUBLIC \"" + node.publicId + "\"" : "")
        + (!node.publicId && node.systemId ? " SYSTEM" : "") 
        + (node.systemId ? " \"" + node.systemId + "\"" : "")
        + ">";*/

      var Result = "<!DOCTYPE";

      if (node != null)
      {
        Result += " " + node.name;

        if (node.publicId)
          Result += " PUBLIC \"" + node.publicId + "\"";
        else if (node.systemId)
          Result += " SYSTEM";

        if (node.systemId)
          Result += " \"" + node.systemId + "\"";
      }

      return Result + ">";
    }

    internal object CreateElement(string Type)
    {
      return (object)Handle.createElement(Type);
    }
  }

  internal sealed class WebCanvas : WebElement
  {
    internal WebCanvas(WebDocument WebDocument)
      : base(WebDocument.CreateElement("canvas"))
    {
      AddNativeHandler("mousemove", e =>
      {
        MouseEventInvoke(MouseMoveEvent, e);
      });
      AddNativeHandler("mousedown", e =>
      {
        MouseEventInvoke(MouseDownEvent, e);
      });
      AddNativeHandler("mouseup", e =>
      {
        MouseEventInvoke(MouseUpEvent, e);
      });
      AddNativeHandler("click", e =>
      {
        MouseEventInvoke(SingleClickEvent, e);
      });
      AddNativeHandler("dblclick", e =>
      {
        MouseEventInvoke(DoubleClickEvent, e);
      });
      AddNativeHandler("contextmenu", e =>
      {
        MouseEventInvoke(ContextMenuEvent, e);
      });
    }

    public event Action<int, int> MouseMoveEvent;
    public event Action<int, int> MouseDownEvent;
    public event Action<int, int> MouseUpEvent;
    public event Action<int, int> SingleClickEvent;
    public event Action<int, int> DoubleClickEvent;
    public event Action<int, int> ContextMenuEvent;
    public int Width
    {
      get { return Handle.width; }
      set { Handle.width = value; }
    }
    public int Height
    {
      get { return Handle.height; }
      set { Handle.height = value; }
    }
    public int ClientWidth
    {
      get { return Handle.clientWidth; }
      set { Handle.clientWidth = value; }
    }
    public int ClientHeight
    {
      get { return Handle.clientHeight; }
      set { Handle.clientHeight = value; }
    }

    public Web2DContext Get2DContext()
    {
      return new Web2DContext((object)Handle.getContext("2d"));
    }

    private void MouseEventInvoke(Action<int, int> Action, dynamic Event)
    {
      if (Action != null)
      {
        var ClientRect = Handle.getBoundingClientRect();

        var X = (int)(((Event.clientX - ClientRect.left) / (ClientRect.right - ClientRect.left) * Width) + 0.5F);
        var Y = (int)(((Event.clientY - ClientRect.top) / (ClientRect.bottom - ClientRect.top) * Height) + 0.5F);

        Action(X, Y);
      }
    }
  }

  internal sealed class Web2DContext : WebObject
  {
    internal Web2DContext(object Handle)
      : base(Handle)
    {
    }

    public string FillStyle
    {
      set { Handle.fillStyle = value; }
    }
    public string StrokeStyle
    {
      set { Handle.strokeStyle = value; }
    }
    public int LineWidth
    {
      set { Handle.lineWidth = value; }
    }
    public float GlobalOpacity
    {
      set { Handle.globalAlpha = value; }
    }
    public string GlobalCompositeOperation
    {
      set { Handle.globalCompositeOperation = value; }
    }

    public void ClearRect(int X, int Y, int Width, int Height)
    {
      Handle.clearRect(X, Y, Width, Height);
    }
    public void FillRect(int X, int Y, int Width, int Height)
    {
      Handle.fillRect(X, Y, Width, Height);
    }
    public void StrokeRect(int X, int Y, int Width, int Height)
    {
      Handle.strokeRect(X, Y, Width, Height);
    }
    public void FillEllipse(int CenterX, int CenterY, int RadiusX, int RadiusY)
    {
      if (RadiusX == RadiusY)
      {
        Handle.beginPath();
        Handle.arc(CenterX, CenterY, RadiusX, 0, 2 * Math.PI);
        Handle.fill();
        Handle.closePath();
      }
      else
      {
        Handle.beginPath();

        Handle.moveTo(CenterX, CenterY - RadiusY); // A1

        Handle.bezierCurveTo(
          CenterX + RadiusX, CenterY - RadiusY, // C1
          CenterX + RadiusX, CenterY + RadiusY, // C2
          CenterX, CenterY + RadiusY); // A2

        Handle.bezierCurveTo(
          CenterX - RadiusX, CenterY + RadiusY, // C3
          CenterX - RadiusX, CenterY - RadiusY, // C4
          CenterX, CenterY - RadiusY); // A1

        Handle.fill();
        Handle.closePath();
      }

      // NOTE: only supported in Chrome.
      //Handle.ellipse(CenterX, CenterY, RadiusX, RadiusY, 0, 0, Math.PI * 2);
      //Handle.fill();
    }
    public void StrokeEllipse(int CenterX, int CenterY, int RadiusX, int RadiusY)
    {
      if (RadiusX == RadiusY)
      {
        Handle.beginPath();
        Handle.arc(CenterX, CenterY, RadiusX, 0, 2 * Math.PI);
        Handle.stroke();
        Handle.closePath();
      }
      else
      {
        Handle.beginPath();

        Handle.moveTo(CenterX, CenterY - RadiusY); // A1

        Handle.bezierCurveTo(
          CenterX + RadiusX, CenterY - RadiusY, // C1
          CenterX + RadiusX, CenterY + RadiusY, // C2
          CenterX, CenterY + RadiusY); // A2

        Handle.bezierCurveTo(
          CenterX - RadiusX, CenterY + RadiusY, // C3
          CenterX - RadiusX, CenterY - RadiusY, // C4
          CenterX, CenterY - RadiusY); // A1

        Handle.stroke();
        Handle.closePath();
      }

      // NOTE: only supported in Chrome.
      //Handle.ellipse(CenterX, CenterY, RadiusX, RadiusY, 0, 0, Math.PI * 2);
      //Handle.stroke();
    }
    public void FillText(int X, int Y, string Text)
    {
      Handle.fillText(Text, X, Y);
    }
    public void StrokeText(int X, int Y, string Text)
    {
      Handle.strokeText(Text, X, Y);
    }
    public void DrawImage(int X, int Y, int Width, int Height, WebImage Image)
    {
      Handle.drawImage(Image.Handle, X, Y, Width, Height);
    }
    public void DrawImage(int X, int Y, WebImage Image)
    {
      Handle.drawImage(Image.Handle, X, Y);
    }
    public void DrawCanvas(int X, int Y, int Width, int Height, WebCanvas Canvas)
    {
      Handle.drawImage(Canvas.Handle, X, Y, Width, Height);
    }
    public void Translate(int X, int Y)
    {
      Handle.translate(X, Y);
    }
    public void Scale(int X, int Y)
    {
      Handle.scale(X, Y);
    }
    public void Save()
    {
      Handle.save();
    }
    public void Restore()
    {
      Handle.restore();
    }
  }

  internal sealed class WebButton : WebElement
  {
    internal WebButton(WebDocument WebDocument)
      : base(WebDocument.CreateElement("button"))
    {
      AddNativeHandler("click", e =>
      {
        if (ClickEvent != null)
          ClickEvent();
      });
      AddNativeHandler("contextmenu", e =>
      {
        if (ContextMenuEvent != null)
          ContextMenuEvent();
      });

      AddNativeHandler("mouseenter", e =>
      {
        if (MouseEnterEvent != null)
          MouseEnterEvent();
      });
      AddNativeHandler("mouseout", e =>
      {
        if (MouseLeaveEvent != null)
          MouseLeaveEvent();
      });
      AddNativeHandler("mousedown", e =>
      {
        if (MouseDownEvent != null)
          MouseDownEvent();
      });
      AddNativeHandler("mouseup", e =>
      {
        if (MouseUpEvent != null)
          MouseUpEvent();
      });
    }

    public string Text
    {
      get { return Handle.innerHTML.WebDecodeText(); }
      set { Handle.innerHTML = value.WebEncodeText(); }
    }
    public bool IsEnabled
    {
      get { return !Handle.disabled; }
      set { Handle.disabled = !value; }
    }
    public event Action ClickEvent;
    public event Action ContextMenuEvent;
    public event Action MouseEnterEvent;
    public event Action MouseLeaveEvent;
    public event Action MouseDownEvent;
    public event Action MouseUpEvent;
  }

  internal sealed class WebImage : WebElement
  {
    internal WebImage(WebDocument WebDocument)
      : base(WebDocument.CreateElement("img"))
    {
    }

    public int Width
    {
      get { return Handle.width; }
      set { Handle.width = value; }
    }
    public int Height
    {
      get { return Handle.height; }
      set { Handle.height = value; }
    }
    public string Source
    {
      set { Handle.src = value; }
    }
  }

  internal sealed class WebAudio : WebElement
  {
    internal WebAudio(WebDocument WebDocument)
      : base(WebDocument.CreateElement("Audio"))
    {
    }

    public void Play(string Uri)
    {
      Handle.play(Uri);
    }
  }

  internal sealed class WebTextArea : WebElement
  {
    internal WebTextArea(WebDocument WebDocument)
      : base(WebDocument.CreateElement("textarea"))
    {
      AddNativeHandler("keyup", e =>
      {
        ChangeInvoke();
      });
      AddNativeHandler("change", e =>
      {
        ChangeInvoke();
      });
    }

    public string Text
    {
      get { return Handle.value; }
      set { Handle.value = value; }
    }
    public bool IsReadOnly
    {
      get { return Handle.readOnly; }
      set { Handle.readOnly = value; }
    }
    public bool Wrap
    {
      set { Handle.Wrap = value ? "on" : "off"; }
    }
    public event Action ChangeEvent;

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
  }

  internal sealed class WebInput : WebElement
  {
    internal WebInput(WebDocument WebDocument)
      : base(WebDocument.CreateElement("input"))
    {
      AddNativeHandler("input", e =>
      {
        ChangeInvoke();
      });
    }

    public string Type
    {
      get { return Handle.type; }
      set { Handle.type = value; }
    }
    public string Text
    {
      get { return Handle.value; }
      set { Handle.value = value; }
    }
    public bool IsReadOnly
    {
      get { return Handle.readOnly; }
      set { Handle.readOnly = value; }
    }
    public event Action ChangeEvent;

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
  }

  internal sealed class WebLabel : WebElement
  {
    internal WebLabel(WebDocument WebDocument)
      : base(WebDocument.CreateElement("div"))
    {
      this.Label = WebDocument.CreateElement("label");
      Handle.appendChild(Label);
    }

    public string Text
    {
      get { return Label.innerHTML.WebDecodeText(); }
      set { Label.innerHTML = value.WebEncodeText(); }
    }

    private dynamic Label;
  }

  internal sealed class WebDiv : WebElement
  {
    internal WebDiv(WebDocument WebDocument)
      : base(WebDocument.CreateElement("div"))
    {
    }
  }

  internal sealed class WebTable : WebElement
  {
    internal WebTable(WebDocument WebDocument)
      : base(WebDocument.CreateElement("table"))
    {
    }

    public WebTableRow AddRow()
    {
      return new WebTableRow((object)Handle.insertRow());
    }
  }

  internal sealed class WebTableRow : WebElement
  {
    internal WebTableRow(object Handle)
      : base(Handle)
    {
    }

    public WebTableCell AddCell()
    {
      return new WebTableCell((object)Handle.insertCell());
    }
  }

  internal sealed class WebTableCell : WebElement
  {
    internal WebTableCell(object Handle)
      : base(Handle)
    {
    }
  }
}
