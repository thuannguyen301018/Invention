﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Inv
{
  public sealed class FolderWatcher : IDisposable
  {
    public FolderWatcher()
    {
      this.Handle = new System.IO.FileSystemWatcher();
      Handle.NotifyFilter = System.IO.NotifyFilters.DirectoryName | System.IO.NotifyFilters.FileName | System.IO.NotifyFilters.Attributes | System.IO.NotifyFilters.LastWrite | System.IO.NotifyFilters.Size;
      Handle.Changed += UpdateMethod;
      Handle.Created += UpdateMethod;
      Handle.Deleted += UpdateMethod;
      Handle.Renamed += RenameMethod;
      Handle.Error += ErrorMethod;
    }
    public void Dispose()
    {
      Handle.Dispose();
    }

    public string Path
    {
      get { return Handle.Path; }
      set { Handle.Path = value; }
    }
    public string Filter
    {
      get { return Handle.Filter; }
      set { Handle.Filter = value; }
    }
    public bool IncludeSubdirectories
    {
      get { return Handle.IncludeSubdirectories; }
      set { Handle.IncludeSubdirectories = value; }
    }
    public event Action ChangeEvent;

    public void Start()
    {
      if (!IsActive)
      {
        IsActive = true;

        Handle.EnableRaisingEvents = true;
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        IsActive = false;

        Handle.EnableRaisingEvents = false;
      }
    }

    private void UpdateMethod(object sender, System.IO.FileSystemEventArgs e)
    {
      ChangeInvoke();
    }
    private void RenameMethod(object sender, System.IO.RenamedEventArgs e)
    {
      ChangeInvoke();
    }
    private void ErrorMethod(object sender, System.IO.ErrorEventArgs e)
    {
      ChangeInvoke();
    }

    private void ChangeInvoke()
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private bool IsActive;
    private System.IO.FileSystemWatcher Handle;
  }
}
