﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.IO;
using System.Diagnostics;

namespace Inv
{
  public static class FileCompareHelper
  {
    public static void OpenWinMergeDownloadPage()
    {
      Process.Start("http://www.kestral.com.au/winmerge");
    }
  }

  public class FileCompare
  {
    public FileCompare()
    {
      ParameterFormat = "\"$S\" \"$T\"";
    }

    public string ExecutablePath { get; set; }
    public string ParameterFormat { get; set; }
    public event Action ComparisonClosedEvent;
    public event Action ComparisonExecutableMissingEvent;

    public void UseWinMerge()
    {
      ExecutablePath = @"C:\Program Files (x86)\WinMerge\WinMergeU.exe";
      ParameterFormat = "\"$S\" \"$T\"";
    }
  
    public void Compare(string ExecutablePath, string SourceFilePath, string TargetFilePath)
    {
      this.ExecutablePath = ExecutablePath;
      Compare(SourceFilePath, TargetFilePath);
    }
    public void Compare(string SourceSourceFilePath, string TargetFilePath)
    {
      CompareFiles(SourceSourceFilePath, TargetFilePath);
    }

    private void CompareFiles(string SourceFilePath, string TargetFilePath)
    {
      if (ExecutablePath.IsWhitespace())
        throw new Exception("ExecutablePath is empty");

      if (!File.Exists(ExecutablePath))
      {
        var ComparisonClosed = ComparisonClosedEvent;
        if (ComparisonClosed != null)
          ComparisonClosed();

        var ExecutableMissingEvent = ComparisonExecutableMissingEvent;

        if (ExecutableMissingEvent != null)
          ExecutableMissingEvent();
        else
          throw new Exception("Comparison executable does not exist: " + ExecutablePath);
      }
      else
      {
        var Comparer = Process.Start(ExecutablePath, ProduceParameters(SourceFilePath, TargetFilePath));
        Comparer.EnableRaisingEvents = true;
        Comparer.Exited += (Sender, E) =>
        {
          var ComparisonClosed = ComparisonClosedEvent;
          if (ComparisonClosed != null)
            ComparisonClosed();
        };
      }
    }
    private string ProduceParameters(string SourceFilePath, string TargetFilePath)
    {
      if (ParameterFormat.NullAsEmpty().IsWhitespace())
        throw new Exception("ParameterFormat is empty");

      return ParameterFormat.Replace("$S", SourceFilePath).Replace("$T", TargetFilePath);
    }
  }

  public class StringCompare
  {
    public StringCompare()
    {
      this.FileCompare = new FileCompare();
    }

    public event Action ComparisonExecutableMissingEvent
    {
      add { FileCompare.ComparisonExecutableMissingEvent += value; }
      remove { FileCompare.ComparisonExecutableMissingEvent -= value; }
    }
    
    public void UseWinMerge()
    {
      FileCompare.UseWinMerge();
    }

    public void Compare(string SourceString, string TargetString, string SourceDescription = null, string TargetDescription = null)
    {
      var SourceTempPath = Path.GetTempFileName();
      var TargetTempPath = Path.GetTempFileName();

      var Directory = Path.GetDirectoryName(SourceTempPath);
      var Filename = Path.GetFileNameWithoutExtension(SourceTempPath);
      var Extra = SourceDescription == null ? "" : "-" + SourceDescription;

      var SourceFilePath = Path.Combine(Directory, Filename + Extra + Path.GetExtension(SourceTempPath));

      Directory = Path.GetDirectoryName(TargetTempPath);
      Filename = Path.GetFileNameWithoutExtension(TargetTempPath);
      Extra = TargetDescription == null ? "" : "-" + TargetDescription;

      var TargetFilePath = Path.Combine(Directory, Filename + Extra + Path.GetExtension(TargetTempPath));
      
      File.Delete(SourceTempPath);
      File.Delete(TargetTempPath);

      System.IO.File.WriteAllText(SourceFilePath, SourceString);
      System.IO.File.WriteAllText(TargetFilePath, TargetString);

      FileCompare.ComparisonClosedEvent += () =>
      {
        File.Delete(SourceFilePath);
        File.Delete(TargetFilePath);
      };

      FileCompare.Compare(SourceFilePath, TargetFilePath);
    }

    private FileCompare FileCompare;
  }
}
