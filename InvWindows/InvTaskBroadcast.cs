﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class TaskBroadcastState<TResult>
  {
    internal TaskBroadcastState(bool Success, TResult Result)
    {
      this.Success = Success;
      this.Result = Result;
    }

    public bool Success { get; private set; }
    public TResult Result { get; private set; }
  }

  public sealed class TaskBroadcastCommand<TResult>
  {
    public TaskBroadcastCommand() : this(default(TResult)) { }
    public TaskBroadcastCommand(TResult Default)
    {
      this.Default = Default;
      this.ActionList = new DistinctList<Func<TaskBroadcastContext, TResult>>();
    }

    public event Action<Exception> ExceptionEvent;

    public Func<TResult, bool> RestrictQuery;
    public void AddAction(Func<TaskBroadcastContext, TResult> Action)
    {
      ActionList.Add(Action);
    }
    public TaskBroadcastState<TResult> ExecuteAny()
    {
      return Execute(TaskBroadcastPolicy.Any);
    }
    public TaskBroadcastState<TResult> ExecuteAll()
    {
      return Execute(TaskBroadcastPolicy.All);
    }

    private void HandleException(Exception Exception)
    {
      var AggregateException = Exception as AggregateException;
      if (AggregateException != null)
      {
        try
        {
          AggregateException.Handle((InnerException) => InnerException.GetType() == typeof(System.Threading.Tasks.TaskCanceledException));
        }
        catch (Exception ThrownException)
        {
          if (ExceptionEvent != null)
            ExceptionEvent(ThrownException);
        }
      }
      else if (ExceptionEvent != null)
        ExceptionEvent(Exception);
    }
    private TaskBroadcastState<TResult> Execute(TaskBroadcastPolicy Policy)
    {
      if (ActionList.Count == 0)
      {
        switch (Policy)
        {
          case TaskBroadcastPolicy.All:
            return new TaskBroadcastState<TResult>(true, Default);
          case TaskBroadcastPolicy.Any:
            return new TaskBroadcastState<TResult>(false, Default);
          default:
            throw new Exception(string.Format("Unknown type of Inv.TaskBroadcastPolicy.{0}", Policy));
        }
      }

      if (ActionList.Count == 1)
      {
        // NOTE: only one action, we can execute inline.
        var BroadcastContext = new TaskBroadcastContext(null);
        
        var Action = ActionList[0];

        try
        {
          var Result = Action(BroadcastContext);

          return new TaskBroadcastState<TResult>(!IsRestricted(Result), Result);
        }
        catch (Exception Exception)
        {
          HandleException(Exception);

          return new TaskBroadcastState<TResult>(false, Default);
        }
      }
      else
      {
        var TokenSource = new CancellationTokenSource();

        var BroadcastContext = new TaskBroadcastContext(TokenSource);

        // More than one thread, use a list of tasks.
        var ExecuteList = ActionList.Select(Action => new Task<TResult>(() => Action(BroadcastContext), TokenSource.Token)).ToDistinctList();

        var CleanupTask = Inv.ThreadGovernor.NewTask("InvTaskBroadcast", () =>
        {
          try
          {
            System.Threading.Tasks.Task.WaitAll(ExecuteList.ToArray());
          }
          catch (Exception Exception)
          {
            try
            {
              HandleException(Exception);
            }
            catch
            {
              // NOTE: suppress logging failures - can't allow an exception out of the cleanup task or it will crash the process.
            }
          }
        });

        foreach (var ExecuteTask in ExecuteList)
          ExecuteTask.Start();

        var TaskArray = ExecuteList.ToArray();
        do
        {
          int CompletedIndex;
          while (TaskArray.Length > 0 && (CompletedIndex = System.Threading.Tasks.Task.WaitAny(TaskArray)) >= 0) // Is not required until we implement a global timeout. This will run until one of the WaitHandles returns
          {
            var ExecuteTask = TaskArray[CompletedIndex];
            ExecuteList.Remove(ExecuteTask);

            // If a task has failed and we get it before one that succeeded, the return will be ambiguous.
            // Instead, slice the failed task from the task array and go back to waiting.
            if (ExecuteTask.IsCompleted && !ExecuteTask.IsCanceled)
            {
              if (ExecuteTask.Exception != null)
                HandleException(ExecuteTask.Exception);

              var Restrict = IsRestricted(ExecuteTask.Result);

              switch (Policy)
              {
                case TaskBroadcastPolicy.All:
                  if (ExecuteTask.IsFaulted || Restrict)
                  {
                    TokenSource.Cancel();

                    CleanupTask.Start();

                    return new TaskBroadcastState<TResult>(false, ExecuteTask.Result);
                  }
                  break;

                case TaskBroadcastPolicy.Any:
                  if (!ExecuteTask.IsFaulted && !Restrict)
                  {
                    TokenSource.Cancel();

                    CleanupTask.Start();

                    return new TaskBroadcastState<TResult>(true, ExecuteTask.Result);
                  }
                  break;

                default:
                  throw new Exception(string.Format("Unknown type of TaskBroadcastPolicy.{0}", Policy));
              }
            }

            TaskArray = ExecuteList.ToArray();
          }
        } while (TaskArray.Length > 0);

        switch (Policy)
        {
          case TaskBroadcastPolicy.All:
            return new TaskBroadcastState<TResult>(true, Default);
          case TaskBroadcastPolicy.Any:
            return new TaskBroadcastState<TResult>(false, Default);
          default:
            throw new Exception(string.Format("Unknown type of TaskBroadcastPolicy.{0}", Policy));
        }
      }
    }
    private bool IsRestricted(TResult Result)
    {
      bool Restrict;

      if (RestrictQuery != null)
        Restrict = RestrictQuery(Result);
      else if (Default == null && Result == null)
        Restrict = true;
      else if (Default != null)
        Restrict = Default.Equals(Result);
      else
        Restrict = Result.Equals(Default);

      return Restrict;
    }

    private Inv.DistinctList<Func<TaskBroadcastContext, TResult>> ActionList;
    private TResult Default;

    private enum TaskBroadcastPolicy
    {
      Any,
      All
    }
  }

  public sealed class TaskBroadcastContext
  {
    internal TaskBroadcastContext(CancellationTokenSource TokenSource)
    {
      this.TokenSource = TokenSource;
    }

    public bool IsCancellationRequested
    {
      get { return TokenSource != null && TokenSource.Token.IsCancellationRequested; }
    }

    public void ThrowIfCancellationRequested()
    {
      if (TokenSource != null)
        TokenSource.Token.ThrowIfCancellationRequested();
    }

    private CancellationTokenSource TokenSource;
  }
}