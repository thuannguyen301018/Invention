﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;
using System.Diagnostics;

//
// NOTE:
// * FallbackValue: the value of the switch when not used in the command line.
// * ImplicitValue: the value of the switch when is used in the command line, but no value is provided (eg. 'console' instead of 'console:true').
//

namespace Inv
{
  public sealed class CommandMap
  {
    public CommandMap(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }
    public bool IsLoaded { get; private set; }

    public CommandSwitch<T> AddSwitch<T>(string Name, string Hint, T ImplicitValue, T FallbackValue)
    {
      var Result = new CommandSwitch<T>(this, Name, Hint);

      Result.ImplicitValue = ImplicitValue;
      Result.FallbackValue = FallbackValue;

      KnownSwitchSet.Add(Result);

      return Result;
    }
    public void Load(string[] ArgumentArray)
    {
      this.IsLoaded = true;

      var RegisterSwitchDictionary = new Dictionary<string, ICommandSwitch>(StringComparer.InvariantCultureIgnoreCase);

      foreach (var CommandSwitch in KnownSwitchSet)
      {
        CommandSwitch.Reset();

        RegisterSwitchDictionary.Add(CommandSwitch.Name, CommandSwitch);

        foreach (var Alias in CommandSwitch.GetAliases())
          RegisterSwitchDictionary.Add(Alias, CommandSwitch);
      }

      UnknownSwitchNameSet.Clear();

      foreach (var ArgumentEntry in ArgumentArray)
      {
        var ArgumentSwitch = ArgumentEntry.TrimStart(new char[] { '/', '-' });

        if (!string.IsNullOrEmpty(ArgumentSwitch))
        {
          var SwitchArray = ArgumentSwitch.Split(':');

          string SwitchName;
          string SwitchValue;

          if (SwitchArray.Length >= 1)
          {
            SwitchName = SwitchArray[0];
            SwitchValue = SwitchArray.Skip(1).AsSeparatedText(":");
          }
          else
          {
            SwitchName = ArgumentSwitch;
            SwitchValue = null;
          }

          if (RegisterSwitchDictionary.TryGetValue(SwitchName, out var CommandSwitch))
          {
            if (string.IsNullOrEmpty(SwitchValue))
              CommandSwitch.Load(CommandSwitch.Implicit);
            else
              CommandSwitch.Extract(SwitchValue);
          }
          else
          {
            UnknownSwitchNameSet.Add(ArgumentSwitch);
          }
        }
      }
    }
    public void ResolveSwitches(Inv.CommandMap CommandMap)
    {
      foreach (var CommandSwitch in CommandMap.RetrieveKnownSwitches())
        UnknownSwitchNameSet.Remove(CommandSwitch.Name);
    }
    public string SaveToString()
    {
      return Save(true).AsSeparatedText(" ");
    }
    public string[] Save()
    {
      return Save(false);
    }
    public IEnumerable<string> RetrieveUnknownSwitchNames()
    {
      return UnknownSwitchNameSet.ToDistinctList();
    }
    public IEnumerable<ICommandSwitch> RetrieveKnownSwitches()
    {
      return KnownSwitchSet.ToDistinctList();
    }

    private string[] Save(bool WithEscaping)
    {
      var StringList = new List<string>();

      foreach (var KnownSwitch in KnownSwitchSet)
      {
        if ((KnownSwitch.Value != null && !KnownSwitch.Value.Equals(KnownSwitch.Fallback)) || (KnownSwitch.Fallback != null && !KnownSwitch.Fallback.Equals(KnownSwitch.Value)))
        {
          var Argument = KnownSwitch.Name;

          if ((KnownSwitch.Value != null && !KnownSwitch.Value.Equals(KnownSwitch.Implicit)) || (KnownSwitch.Implicit != null && !KnownSwitch.Implicit.Equals(KnownSwitch.Value)))
          {
            var Format = KnownSwitch.Format();

            if (WithEscaping && (string.IsNullOrEmpty(Format) || Format.Contains(' ') || Format.Contains(':') || Format.Contains('-') || Format.Contains('/')))
              Format = "\"" + Format + "\"";

            Argument += ":" + Format;
          }

          StringList.Add(Argument);
        }
      }

      return StringList.ToArray();
    }

    private HashSet<ICommandSwitch> KnownSwitchSet = new HashSet<ICommandSwitch>();
    private HashSet<string> UnknownSwitchNameSet = new HashSet<string>();
  }

  public interface ICommandSwitch
  {
    string Name { get; }
    string Hint { get; }
    object Value { get; }
    object Fallback { get; }
    object Implicit { get; }
    Type Type { get; }

    IEnumerable<string> GetAliases();
    void Reset();
    void Load(object Value);
    void Extract(string TextValue);
    string Format();
  }

  internal delegate void ExtractAction(ICommandSwitch Self, string Value);
  internal delegate string FormatAction(ICommandSwitch Self);

  public sealed class CommandSwitch<T> : ICommandSwitch 
  {
    internal CommandSwitch(CommandMap Map, string Name, string Hint)
    {
      this.Map = Map;
      this.Name = Name;
      this.Hint = Hint;
      this.AliasList = new Inv.DistinctList<string>();
      this.ExtractAction = ExtractActionDictionary[typeof(T)];
      this.FormatAction = FormatActionDictionary[typeof(T)];
    }

    public string Name { get; private set; }
    public string Hint { get; private set; }
    public T ImplicitValue { get; set; }
    public T FallbackValue { get; set; }
    public T ActiveValue
    {
      get
      {
#if DEBUG
        if (!Map.IsLoaded)
          throw new Exception("Switch is not accessible until the Map is loaded.");
#endif

        return ActiveValueField;
      }
    }
    public bool IsLoaded { get; private set; }

    public void Reset()
    {
#if DEBUG
      if (!Map.IsLoaded)
        throw new Exception("Switch cannot be reset until the Map is loaded.");
#endif

      this.ActiveValueField = FallbackValue;
      this.IsLoaded = false;
    }
    public void Load(T LoadValue)
    {
#if DEBUG
      if (!Map.IsLoaded)
        throw new Exception("Switch cannot be loaded until the Map is loaded.");
#endif

      this.ActiveValueField = LoadValue;
      this.IsLoaded = true;
    }
    public void AddAlias(string ObsoleteSwitchName)
    {
      AliasList.Add(ObsoleteSwitchName);
    }

    public override string ToString()
    {
      if (ActiveValue.Equals(ImplicitValue))
        return Name;
      else
        return Name + ":" + FormatAction(this);
    }

    internal CommandMap Map { get; private set; }

    private ExtractAction ExtractAction;
    private FormatAction FormatAction;
    private T ActiveValueField;
    private Inv.DistinctList<string> AliasList;

    string ICommandSwitch.Name
    {
      get { return Name; }
    }
    string ICommandSwitch.Hint
    {
      get { return Hint; }
    }
    IEnumerable<string> ICommandSwitch.GetAliases()
    {
      return AliasList;
    }
    void ICommandSwitch.Reset()
    {
      Reset();
    }
    void ICommandSwitch.Load(object Value)
    {
      Load((T)Value);
    }
    Type ICommandSwitch.Type
    {
      get { return typeof(T); }
    }
    object ICommandSwitch.Implicit
    {
      get { return ImplicitValue; }
    }
    object ICommandSwitch.Fallback
    {
      get { return FallbackValue; }
    }
    object ICommandSwitch.Value
    {
      get { return ActiveValue; }
    }
    void ICommandSwitch.Extract(string TextValue)
    {
      ExtractAction(this, TextValue);
    }
    string ICommandSwitch.Format()
    {
      return FormatAction(this);
    }

    static CommandSwitch()
    {
      ExtractActionDictionary.Add(typeof(int), (Self, Value) =>
      {
        Self.Load(int.Parse(Value));
      });
      ExtractActionDictionary.Add(typeof(long), (Self, Value) =>
      {
        Self.Load(long.Parse(Value));
      });
      ExtractActionDictionary.Add(typeof(string), (Self, Value) =>
      {
        Self.Load(Value);
      });
      ExtractActionDictionary.Add(typeof(bool), (Self, Value) =>
      {
        Self.Load(bool.Parse(Value));
      });

      FormatActionDictionary.Add(typeof(int), (Self) =>
      {
        return Self.Value.ToString();
      });
      FormatActionDictionary.Add(typeof(long), (Self) =>
      {
        return Self.Value.ToString();
      });
      FormatActionDictionary.Add(typeof(string), (Self) =>
      {
        return (string)Self.Value;
      });
      FormatActionDictionary.Add(typeof(bool), (Self) =>
      {
        return Self.Value.ToString().ToLower();
      });
    }

    private static Dictionary<Type, ExtractAction> ExtractActionDictionary = new Dictionary<Type, ExtractAction>();
    private static Dictionary<Type, FormatAction> FormatActionDictionary = new Dictionary<Type, FormatAction>();
  }
}
