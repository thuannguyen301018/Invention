﻿/*! 4 !*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  public sealed class MailTo
  {
    public MailTo()
    {
    }

    public string Subject { get; set; }
    public string Body { get; set; }

    public void To(IEnumerable<string> Addresses)
    {
      this.ToAddressArray = Addresses.ToArray();
    }
    public void Cc(IEnumerable<string> Addresses)
    {
      this.CcAddressArray = Addresses.ToArray();
    }
    public void To(params string[] AddressArray)
    {
      this.ToAddressArray = AddressArray;
    }
    public void Cc(params string[] AddressArray)
    {
      this.CcAddressArray = AddressArray;
    }
    public void Send()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(ToAddressArray != null, "To must be specified.");
        Inv.Assert.Check(Subject != null, "Subject must be specified.");
        Inv.Assert.Check(Body != null, "Body must be specified.");
      }

      // TODO: what is the string maximum length for the mailto command?

      var MailToAddress = ToAddressArray != null ? ToAddressArray.AsSeparatedText(",") : "";
      var MailCcAddress = CcAddressArray != null ? CcAddressArray.AsSeparatedText(",") : "";
      if (MailCcAddress != "")
        MailCcAddress = "&CC=" + MailCcAddress;

      var MailSubject = Uri.EscapeDataString(Subject ?? "");
      var MailBody = Uri.EscapeDataString(Body ?? "");

      Process.Start(string.Format("mailto:{0}?{1}Subject={2}&Body={3}", MailToAddress, MailCcAddress, MailSubject, MailBody));
    }

    private string[] ToAddressArray;
    private string[] CcAddressArray;
  }

  public sealed class Mapi
  {
    public Mapi()
    {
      this.lastMsgID = new StringBuilder(600);
      this.session = IntPtr.Zero;
      this.origin = new Win32.Mapi32.MapiRecipDesc();
      this.recpts = new ArrayList();
      this.attachs = new Dictionary<string, string>();
    }

    public bool Send(string Subject, string Body)
    {
      lastMsg = new Win32.Mapi32.MapiMessage();
      lastMsg.subject = Subject;
      lastMsg.noteText = Body;

      // set pointers
      lastMsg.originator = AllocOrigin();
      lastMsg.recips = AllocRecips(out lastMsg.recipCount);
      lastMsg.files = AllocAttachs(out lastMsg.fileCount);

      // *** WARNING ***
      // MAPISendMail() failing with multiple attachments.
      //
      // Using flag "MapiDialogModeless" crashes Outlook(causing restart) and freezes PHX if we have more than one attachment !!!!
      // However, a modal dialog for multiple attachments works.
      //
      // A reference to identical effect: https://social.msdn.microsoft.com/Forums/expression/en-US/5d8fece6-9d93-490c-9331-625c17e3291d/mapisendmailhelper-and-mapidialogmodeless?forum=windowsgeneraldevelopmentissues
      //
      // Also noticed that if IsOutlookInstalled == true within PublishDesignControl, no problem with multiple attachments.

      var MapiDialogFlag = lastMsg.fileCount < 2 ? Win32.Mapi32.MapiDialogModeless : Win32.Mapi32.MapiDialog;

      error = Win32.Mapi32.MAPISendMail(session, IntPtr.Zero, lastMsg, MapiDialogFlag | Win32.Mapi32.MapiLogonUI | Win32.Mapi32.MapiNewSession, 0);
      Dealloc();

      origin = new Win32.Mapi32.MapiRecipDesc();
      recpts.Clear();
      attachs.Clear();
      lastMsg = null;

      return error == 0;
    }
    public void AddRecipient(string Name, string Address, bool CC)
    {
      var dest = new Win32.Mapi32.MapiRecipDesc();
      if (CC)
        dest.recipClass = Win32.Mapi32.MapiCC;
      else
        dest.recipClass = Win32.Mapi32.MapiTO;
      dest.name = Name;
      dest.address = Address;
      recpts.Add(dest);
    }
    public void SetSender(string Name, string Address)
    {
      origin.name = Name;
      origin.address = Address;
    }
    public void Attach(string filepath, string displayName = null)
    {
      attachs.Add(filepath, displayName ?? System.IO.Path.GetFileName(filepath));
    }
    public bool Delete(string id)
    {
      error = Win32.Mapi32.MAPIDeleteMail(session, IntPtr.Zero, id, 0, 0);
      return error == 0;
    }
    public bool SaveAttachment(string id, string name, string savepath)
    {
      var ptrmsg = IntPtr.Zero;
      error = Win32.Mapi32.MAPIReadMail(session, IntPtr.Zero, id, Win32.Mapi32.MapiPeek, 0, ref ptrmsg);
      if ((error != 0) || (ptrmsg == IntPtr.Zero))
        return false;

      lastMsg = new Win32.Mapi32.MapiMessage();
      Marshal.PtrToStructure(ptrmsg, lastMsg);
      var f = false;
      if ((lastMsg.fileCount > 0) && (lastMsg.fileCount < 100) && (lastMsg.files != IntPtr.Zero))
        f = SaveAttachmentByName(name, savepath);
      Win32.Mapi32.MAPIFreeBuffer(ptrmsg);
      return f;
    }
    public bool SingleAddress(string label, out string name, out string addr)
    {
      name = null;
      addr = null;
      var newrec = 0;
      var ptrnew = IntPtr.Zero;
      error = Win32.Mapi32.MAPIAddress(session, IntPtr.Zero, null, 1, label, 0, IntPtr.Zero, 0, 0, ref newrec, ref ptrnew);

      if ((error != 0) || (newrec < 1) || (ptrnew == IntPtr.Zero))
        return false;

      var recip = new Win32.Mapi32.MapiRecipDesc();
      Marshal.PtrToStructure(ptrnew, recip);
      name = recip.name;
      addr = recip.address;

      Win32.Mapi32.MAPIFreeBuffer(ptrnew);
      return true;
    }
    public string Error()
    {
      if (error <= 26)
        return errors[error];
      return "?unknown? [" + error.ToString() + "]";
    }

    private bool SaveAttachmentByName(string name, string savepath)
    {
      var f = true;
      var fdtype = typeof(Win32.Mapi32.MapiFileDesc);
      var fdsize = Marshal.SizeOf(fdtype);
      var fdtmp = new Win32.Mapi32.MapiFileDesc();
      var runptr = (int)lastMsg.files;
      for (var i = 0; i < lastMsg.fileCount; i++)
      {
        Marshal.PtrToStructure((IntPtr)runptr, fdtmp);
        runptr += fdsize;
        if (fdtmp.flags != 0)
          continue;
        if (fdtmp.name == null)
          continue;

        try
        {
          if (name == fdtmp.name)
          {
            if (System.IO.File.Exists(savepath))
              System.IO.File.Delete(savepath);
            System.IO.File.Move(fdtmp.path, savepath);
          }
        }
        catch (Exception)
        {
          f = false;
          error = 13;
        }

        try
        {
          System.IO.File.Delete(fdtmp.path);
        }
        catch (Exception)
        {
        }
      }
      return f;
    }
    private IntPtr AllocOrigin()
    {
      origin.recipClass = Win32.Mapi32.MapiORIG;
      var rtype = typeof(Win32.Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);
      var ptro = Marshal.AllocHGlobal(rsize);
      Marshal.StructureToPtr(origin, ptro, false);
      return ptro;
    }
    private IntPtr AllocRecips(out int recipCount)
    {
      recipCount = 0;
      if (recpts.Count == 0)
        return IntPtr.Zero;

      var rtype = typeof(Win32.Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);
      var ptrr = Marshal.AllocHGlobal(recpts.Count * rsize);

      var runptr = (int)ptrr;
      foreach (var recpt in recpts)
      {
        Marshal.StructureToPtr(recpt as Win32.Mapi32.MapiRecipDesc, (IntPtr)runptr, false);
        runptr += rsize;
      }

      recipCount = recpts.Count;
      return ptrr;
    }
    private IntPtr AllocAttachs(out int fileCount)
    {
      fileCount = 0;
      if (attachs == null)
        return IntPtr.Zero;
      if ((attachs.Count <= 0) || (attachs.Count > 100))
        return IntPtr.Zero;

      var atype = typeof(Win32.Mapi32.MapiFileDesc);
      var asize = Marshal.SizeOf(atype);
      var ptra = Marshal.AllocHGlobal(attachs.Count * asize);

      var mfd = new Win32.Mapi32.MapiFileDesc();
      mfd.position = -1;
      var runptr = (int)ptra;
      foreach (var attach in attachs)
      {
        mfd.path = attach.Key;
        mfd.name = attach.Value;
        Marshal.StructureToPtr(mfd, (IntPtr)runptr, false);
        runptr += asize;
      }

      fileCount = attachs.Count;
      return ptra;
    }
    private void Dealloc()
    {
      var rtype = typeof(Win32.Mapi32.MapiRecipDesc);
      var rsize = Marshal.SizeOf(rtype);

      if (lastMsg.originator != IntPtr.Zero)
      {
        Marshal.DestroyStructure(lastMsg.originator, rtype);
        Marshal.FreeHGlobal(lastMsg.originator);
      }

      if (lastMsg.recips != IntPtr.Zero)
      {
        var runptr = (int)lastMsg.recips;
        for (var i = 0; i < lastMsg.recipCount; i++)
        {
          Marshal.DestroyStructure((IntPtr)runptr, rtype);
          runptr += rsize;
        }
        Marshal.FreeHGlobal(lastMsg.recips);
      }

      if (lastMsg.files != IntPtr.Zero)
      {
        var ftype = typeof(Win32.Mapi32.MapiFileDesc);
        var fsize = Marshal.SizeOf(ftype);

        var runptr = (int)lastMsg.files;
        for (var i = 0; i < lastMsg.fileCount; i++)
        {
          Marshal.DestroyStructure((IntPtr)runptr, ftype);
          runptr += fsize;
        }
        Marshal.FreeHGlobal(lastMsg.files);
      }
    }

    private StringBuilder lastMsgID;
    private Win32.Mapi32.MapiMessage lastMsg;
    private IntPtr session;
    private Win32.Mapi32.MapiRecipDesc origin;
    private ArrayList recpts;
    private Dictionary<string, string> attachs;
    private int error;

    private static readonly string[] errors = new string[]
    {
      "OK [0]", "User abort [1]", "General MAPI failure [2]", "MAPI login failure [3]",
      "Disk full [4]", "Insufficient memory [5]", "Access denied [6]", "-unknown- [7]",
      "Too many sessions [8]", "Too many files were specified [9]", "Too many recipients were specified [10]", "A specified attachment was not found [11]",
      "Attachment open failure [12]", "Attachment write failure [13]", "Unknown recipient [14]", "Bad recipient type [15]",
      "No messages [16]", "Invalid message [17]", "Text too large [18]", "Invalid session [19]",
      "Type not supported [20]", "A recipient was specified ambiguously [21]", "Message in use [22]", "Network failure [23]",
      "Invalid edit fields [24]", "Invalid recipients [25]", "Not supported [26]"
    };
  }

  public sealed class MailEnvelop
  {
    public string id;
    public DateTime date;
    public string from;
    public string subject;
    public bool unread;
    public int atts;
  }

  public sealed class MailAttach
  {
    public MailAttach(string path, string name = null)
    {
      this.path = path;
      this.name = name;
    }

    public int position;
    public string path;
    public string name;
  }
}
