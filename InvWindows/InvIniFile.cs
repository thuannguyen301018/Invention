﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using Inv.Support;

namespace Inv
{
  public sealed class IniFile
  {
    public IniFile()
    {
      this.SettingDictionary = new Dictionary<IniKey, string>();
    }

    public void Load(string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
      using (var StreamReader = new StreamReader(FileStream))
      {
        var IniLine = StreamReader.ReadLine();

        string CurrentRoot = null;

        while (IniLine != null)
        {
          IniLine = IniLine.Trim();

          if (IniLine != "")
          {
            if (IniLine.StartsWith("[") && IniLine.EndsWith("]"))
            {
              CurrentRoot = IniLine.Substring(1, IniLine.Length - 2);
            }
            else if (!IniLine.StartsWith(";")) // semicolon is a single line INI comment.
            {
              var IniLineArray = IniLine.Split(new char[] { '=' }, 2);

              if (CurrentRoot == null)
                CurrentRoot = "ROOT";

              var IniKey = new IniKey(CurrentRoot, IniLineArray[0]);

              string IniValue;
              if (IniLineArray.Length > 1)
                IniValue = IniLineArray[1];
              else
                IniValue = null;

              SettingDictionary.Add(IniKey, IniValue);
            }
          }

          IniLine = StreamReader.ReadLine();
        }
      }
    }
    public void Save(string FilePath)
    {
      var SectionList = SettingDictionary.Keys.Select(Index => Index.Section).Distinct().ToDistinctList();

      using (var StreamWriter = new StreamWriter(FilePath))
      {
        foreach (var Section in SectionList)
        {
          StreamWriter.WriteLine("[" + Section + "]");

          foreach (var SettingEntry in SettingDictionary.Where(Index => Index.Key.Section.Equals(Section, StringComparison.InvariantCultureIgnoreCase)))
          {
            var IniKey = SettingEntry.Key;
            var IniValue = SettingEntry.Value;

            if (IniValue == null)
              StreamWriter.WriteLine(IniKey.Setting);
            else
              StreamWriter.WriteLine(IniKey.Setting + "=" + IniValue);
          }

          if (Section != SectionList.Last())
            StreamWriter.WriteLine();
        }
      }
    }
    public IEnumerable<string> EnumerateSettings(string Section)
    {
      return SettingDictionary.Keys.Where(Index => Index.Section.Equals(Section, StringComparison.InvariantCultureIgnoreCase)).Select(Index => Index.Setting);
    }
    public void WriteSetting(string Section, string Setting, string SettingValue)
    {
      SettingDictionary[new IniKey(Section, Setting)] = SettingValue;
    }
    public string ReadSetting(string Section, string Setting)
    {
      return SettingDictionary.GetValueOrDefault(new IniKey(Section, Setting));
    }
    public void DeleteSetting(string Section, string Setting)
    {
      SettingDictionary.Remove(new IniKey(Section, Setting));
    }

    private Dictionary<IniKey, string> SettingDictionary;

    private sealed class IniKey
    {
      internal IniKey(string Section, string Setting)
      {
        this.Section = Section;
        this.Setting = Setting;
      }

      public string Section { get; private set; }
      public string Setting { get; private set; }

      public override int GetHashCode()
      {
        return Section.GetHashCode() ^ Setting.GetHashCode();
      }
      public override bool Equals(object obj)
      {
        var IniKey = obj as IniKey;

        if (IniKey == null)
          return base.Equals(obj);
        else
          return Section.Equals(IniKey.Section, StringComparison.InvariantCultureIgnoreCase) && Setting.Equals(IniKey.Setting, StringComparison.InvariantCultureIgnoreCase);
      }
    }
  }
}