﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Security.Permissions;
using System.Transactions;
using System.ComponentModel;
using Inv.Support;

namespace Inv
{
  public sealed class FileSystemTransaction : IDisposable
  {
    public static FileSystemTransaction BeginKTM(string Description)
    {
      Debug.Assert(Description == null || Description.Length <= 64, "Description has a max length of 64 characters.");

      IntPtr TransactionPointer;

      if (IsSupported)
      {
        TransactionPointer = Win32.Ktmw32.CreateTransaction(IntPtr.Zero, IntPtr.Zero, 0, 0, 0, 0, Description.NullAsEmpty().Substring(0, Math.Min(64, Description.NullAsEmpty().Length)));

        if (TransactionPointer == IntPtr.Zero || TransactionPointer == InvalidHandle)
          throw new Win32Exception();
      }
      else
      {
        TransactionPointer = IntPtr.Zero;
      }

      return new FileSystemTransaction(TransactionPointer);
    }
    [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode)]
    public static FileSystemTransaction JoinDTC()
    {
      IntPtr TransactionPointer;

      if (IsSupported)
      {
        var CurrentTransaction = Transaction.Current;

        if (CurrentTransaction == null)
        {
          TransactionPointer = IntPtr.Zero;

          if (TransactionPointer == IntPtr.Zero || TransactionPointer == InvalidHandle)
            throw new Exception("Unable to join as there is no active DTC transaction");
        }
        else
        {
          var KernelTransaction = (Win32.Ktmw32.IKernelTransaction)TransactionInterop.GetDtcTransaction(CurrentTransaction);

          KernelTransaction.GetHandle(out TransactionPointer);

          if (TransactionPointer == IntPtr.Zero || TransactionPointer == InvalidHandle)
            throw new Exception("Unable to join DTC transaction");
        }
      }
      else
      {
        TransactionPointer = IntPtr.Zero;
      }

      return new FileSystemTransaction(TransactionPointer);
    }

    private static readonly IntPtr InvalidHandle = new IntPtr(-1);
    private static readonly bool IsSupported = System.Environment.OSVersion.Platform == PlatformID.Win32NT && System.Environment.OSVersion.Version.Major >= 6;

    private FileSystemTransaction(IntPtr TransactionPointer)
    {
      this.TransactionPointer = TransactionPointer;
    }
    ~FileSystemTransaction()
    {
      Dispose();
    }
    public void Dispose()
    {
      if (TransactionPointer != IntPtr.Zero)
      {
        // TODO: ignore rollback failures?
        if (!IsCommit && !IsRollback)
          Win32.Ktmw32.RollbackTransaction(TransactionPointer);

        // NOTE: ignore handle close failures.
        Win32.Kernel32.CloseHandle(TransactionPointer);

        TransactionPointer = IntPtr.Zero;
      }

      GC.SuppressFinalize(this);
    }

    public void WriteAllStream(string FilePath, Stream Stream, int BufferSize = 65536)
    {
      using (var FileStream = OpenFile(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
        Stream.CopyTo(FileStream, BufferSize);
    }
    public void WriteAllBytes(string FilePath, byte[] Buffer)
    {
      if (IsSupportedPath(FilePath))
      {
        using (var FileStream = OpenFile(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
          FileStream.Write(Buffer, 0, Buffer.Length);
      }
      else
      {
        File.WriteAllBytes(FilePath, Buffer);
      }
    }
    public void WriteAllText(string FilePath, string Text)
    {
      using (var StreamWriter = OpenWriter(FilePath))
        StreamWriter.Write(Text);
    }
    public StreamWriter OpenWriter(string FilePath)
    {
      if (IsSupportedPath(FilePath))
        return new StreamWriter(OpenFile(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read));
      else
        return new StreamWriter(FilePath);
    }
    public StreamReader OpenReader(string FilePath)
    {
      if (IsSupportedPath(FilePath))
        return new StreamReader(OpenFile(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read));
      else
        return new StreamReader(new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
    }
    public FileStream OpenFile(string FilePath, FileMode FileMode, FileAccess FileAccess, FileShare FileShare, int FileBuffer = 65536)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(FilePath))
      {
        var IsAppend = FileMode == FileMode.Append;

        // NOTE: CreateFileTransacted doesn't directly support Append - in fact Append is synthesized by the .NET framework in normal FileStream's.
        FileMode TranslateFileMode;
        if (IsAppend)
          TranslateFileMode = FileMode.OpenOrCreate;
        else
          TranslateFileMode = FileMode;

        var SafeFileHandle = Win32.Kernel32.CreateFileTransacted(FilePath, FileAccess, FileShare, IntPtr.Zero, TranslateFileMode, new Win32.Kernel32.EFileAttributes(), IntPtr.Zero, TransactionPointer, IntPtr.Zero, IntPtr.Zero);

        if (SafeFileHandle == null || SafeFileHandle.IsInvalid)
          throw new Win32Exception();

        var FileStream = new FileStream(SafeFileHandle, FileAccess, FileBuffer);
        try
        {
          if (IsAppend)
            FileStream.Seek(0L, SeekOrigin.End);

          return FileStream;
        }
        catch (Exception Exception)
        {
          FileStream.Dispose();

          throw Exception.Preserve();
        }
      }
      else
      {
        return new FileStream(FilePath, FileMode, FileAccess, FileShare, FileBuffer);
      }
    }
    public bool ExistsDirectory(string FilePath)
    {
      if (IsSupported)
        return Directory.Exists(FilePath); // TODO: TxF version?
      else
        return Directory.Exists(FilePath);
    }
    public bool ExistsFile(string FilePath)
    {
      if (IsSupported)
        return File.Exists(FilePath); // TODO: TxF version?
      else
        return File.Exists(FilePath);
    }
    public void DeleteFile(string FilePath)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(FilePath))
      {
        if (!Win32.Kernel32.DeleteFileTransacted(FilePath, TransactionPointer))
          throw new Win32Exception();
      }
      else
      {
        File.Delete(FilePath);
      }
    }
    public void CopyFile(string SourceFilePath, string TargetFilePath, bool OverwriteExisting)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(SourceFilePath) && IsSupportedPath(TargetFilePath))
      {
        var Cancel = 0;

        if (!Win32.Kernel32.CopyFileTransacted(SourceFilePath, TargetFilePath, null, IntPtr.Zero, ref Cancel, OverwriteExisting ? 0 : Win32.Kernel32.CopyFileFlags.COPY_FILE_FAIL_IF_EXISTS, TransactionPointer))
          throw new Win32Exception();
      }
      else
      {
        File.Copy(SourceFilePath, TargetFilePath, OverwriteExisting);
      }
    }
    public void MoveFile(string SourceFilePath, string TargetFilePath, bool ReplaceExisting)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(SourceFilePath) && IsSupportedPath(TargetFilePath))
      {
        if (!Win32.Kernel32.MoveFileTransacted(SourceFilePath, TargetFilePath, ReplaceExisting ? Win32.Kernel32.MoveFileFlags.MOVEFILE_REPLACE_EXISTING : 0, TransactionPointer))
          throw new Win32Exception();
      }
      else
      {
        if (ReplaceExisting && File.Exists(TargetFilePath))
          File.Delete(TargetFilePath);

        File.Move(SourceFilePath, TargetFilePath);
      }
    }
    public void CreateDirectory(string DirectoryPath)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(DirectoryPath))
      {
        Directory.CreateDirectory(DirectoryPath);

        // TODO: this is not yet as complete as the standard Directory.CreateDirectory implementation.
        /*
        if (!Win32.Kernel32.CreateDirectoryTransacted(null, DirectoryPath, IntPtr.Zero, TransactionPointer))
        {
          var LastError = Marshal.GetLastWin32Error();

          if (LastError != Win32.Kernel32.ERROR_ALREADY_EXISTS)
            throw new Win32Exception(LastError);
        }*/
      }
      else
      {
        Directory.CreateDirectory(DirectoryPath);
      }
    }
    public void DeleteDirectory(string DirectoryPath)
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupportedPath(DirectoryPath))
      {
        if (!Win32.Kernel32.RemoveDirectoryTransacted(DirectoryPath, TransactionPointer))
          throw new Win32Exception();
      }
      else
      {
        Directory.Delete(DirectoryPath);
      }
    }
    public bool FileExists(string FilePath)
    {
      if (string.IsNullOrEmpty(FilePath))
      {
        return false;
      }
      else 
      {
        return File.Exists(FilePath);

        /* // TODO: seems to have side effects: An unhandled exception of type 'System.Runtime.InteropServices.SEHException' occurred in mscorlib.dll. Additional information: External component has thrown an exception.
        Win32.Kernel32.WIN32_FIND_DATA FindData;
        var Handle = Win32.Kernel32.FindFirstFileTransacted(FilePath, Win32.Kernel32.FINDEX_INFO_LEVELS.FindExInfoStandard, out FindData, Win32.Kernel32.FINDEX_SEARCH_OPS.FindExSearchNameMatch, IntPtr.Zero, 0, TransactionPointer);
        try
        {
          return !Handle.IsInvalid && (FindData.dwFileAttributes & FileAttributes.Directory) != FileAttributes.Directory;
        }
        finally
        {
          Win32.Kernel32.FindClose(Handle);
        }
         */
      }
    }
    public FileAttributes GetFileAttributes(string FilePath)
    {
      if (IsSupportedPath(FilePath))
      {
        if (!Win32.Kernel32.GetFileAttributesTransacted(FilePath, Win32.Kernel32.GET_FILEEX_INFO_LEVELS.GetFileExInfoStandard, out var FileData, TransactionPointer))
          throw new Win32Exception();

        return FileData.dwFileAttributes;
      }
      else
      {
        return File.GetAttributes(FilePath);
      }
    }
    public void SetFileAttributes(string FilePath, FileAttributes Attributes)
    {
      File.SetAttributes(FilePath, Attributes);

      // TODO: not sure if the TxF function works as expected.
      //if (!Win32.Kernel32.SetFileAttributesTransacted(FilePath, (uint)Attributes, TransactionPointer))
      //  throw new Win32Exception();
    }
    public void SetReadOnly(string FilePath, bool Value)
    {
      // TODO: not sure if the TxF function works as expected.
      Inv.Support.FileHelper.SetReadOnly(FilePath, Value);
    }
    public void Commit()
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      if (IsSupported)
      {
        if (!Win32.Ktmw32.CommitTransaction(TransactionPointer))
          throw new Win32Exception();
      }

      IsCommit = true;
    }
    public void Rollback()
    {
      Debug.Assert(IsActive(), "Transaction must be active.");

      IsRollback = true;

      if (IsSupported)
      {
        if (!Win32.Ktmw32.RollbackTransaction(TransactionPointer))
          throw new Win32Exception();
      }
    }
    public bool IsActive()
    {
      return !IsCommit && !IsRollback;
    }

    private bool IsSupportedPath(string FilePath)
    {
      //
      // TxF does not support the following transaction scenarios:
      //
      // * Transactions on network volumes, for example on file shares. TxF is not supported by the CIFS/SMB protocols.
      // * Transactions on any file system other than NTFS.
      // * Transacted operations against files cached by client-side caching.
      // * File access using object IDs.
      // * Any shared writer scenario.
      // * Any situation where a file is opened for an extended period of time (days or weeks).
      //

      var Result = IsSupported && !FilePath.StartsWith(@"\\");

      if (Result)
      {
        try
        {
          var FileInfo = new FileInfo(FilePath);
          var DriveInfo = new DriveInfo(FileInfo.Directory.Root.FullName);

          Result = DriveInfo.DriveFormat == "NTFS" && DriveInfo.DriveType != DriveType.Network;
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Describe());
          if (Debugger.IsAttached)
            Debugger.Break();

          Result = false;
        }
      }

      return Result;
    }

    private IntPtr TransactionPointer;
    private bool IsCommit;
    private bool IsRollback;
  }
}
