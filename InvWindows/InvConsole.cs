﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  [SuppressUnmanagedCodeSecurity]
  public static class Console
  {
    public static bool IsVisible => Win32.Kernel32.GetConsoleWindow() != IntPtr.Zero;
    public static int WindowHeight => System.Console.WindowHeight;
    public static int CursorLeft => System.Console.CursorLeft;
    public static int CursorTop => System.Console.CursorTop;
    public static bool NoWindowRequested => System.Diagnostics.Process.GetCurrentProcess().StartInfo.CreateNoWindow;

    public static void Show()
    {
      if (!IsVisible)
      {
        if (!Win32.Kernel32.AttachConsole(Win32.Kernel32.ATTACH_PARENT_PROCESS))
        {
          Win32.Kernel32.AllocConsole();

          // NOTE: VS2017 doesn't write to the console window, this is a gross workaround.
          var stdHandle = Win32.Kernel32.CreateFile("CONOUT$", Win32.Kernel32.GENERIC_WRITE, Win32.Kernel32.FILE_SHARE_WRITE, 0, Win32.Kernel32.OPEN_EXISTING, 0, 0);
          var safeFileHandle = new Microsoft.Win32.SafeHandles.SafeFileHandle(stdHandle, true);
          var fileStream = new FileStream(safeFileHandle, FileAccess.Write);
          var encoding = System.Text.Encoding.GetEncoding(Win32.Kernel32.MY_CODE_PAGE);
          var standardOutput = new StreamWriter(fileStream, encoding);
          standardOutput.AutoFlush = true;
          System.Console.SetOut(standardOutput);
        }
      }
    }
    public static void Hide()
    {
      if (IsVisible)
      {
        System.Console.SetOut(TextWriter.Null);
        System.Console.SetError(TextWriter.Null);

        Win32.Kernel32.FreeConsole();
      }
    }
    public static void WriteLine()
    {
      System.Console.WriteLine();
    }
    public static void Write(string Field)
    {
      System.Console.Write(Field);
    }
    public static void WriteLine(string Line)
    {
      System.Console.WriteLine(Line);
    }
    public static string ReadLine()
    {
      return System.Console.ReadLine();
    }
    public static int Read()
    {
      return System.Console.Read();
    }
    public static ConsoleKeyInfo ReadKey(bool Intercept)
    {
      return System.Console.ReadKey(Intercept);
    }
    public static void SetCursorPosition(int Left, int Top)
    {
      System.Console.SetCursorPosition(Left, Top);
    }
  }
}