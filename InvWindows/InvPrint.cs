﻿/*! 5 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Drawing.Printing;
using System.Printing;
using System.Text;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Diagnostics;
using Inv.Support;
using Win32;

namespace Inv
{
  /// <summary>
  /// Represents a printer attached to the system.
  /// </summary>
  public sealed class Printer
  {     
    /*
     * WPF printing is incomplete. There's bare bones support for printing, but we need more. So 
     * we use winforms printing services - they're a much more bare metal mapping to the API services
     * we need. But we also maintain the WPF printqueue handle because it gives us more information 
     * about the state of the queue. But the UI support is expressed in terms of WPF
     * 
     * ToDo: should we separate out the printing engine stuff from the UI stuff?
     */
    /// <summary>
    /// Initialize a new <see cref="Printer"/> object with the given print settings.
    /// </summary>
    /// <param name="PrintSettings">The printer settings to use.</param>
    public Printer(PrinterSettings PrintSettings)
    {
      this.PrintSettings = PrintSettings;
      this.PageSettings = PrintSettings.DefaultPageSettings;
      LoadPageSettings();
    }

    /// <summary>
    /// Gets the full name of this <see cref="Printer"/>.
    /// </summary>
    public string FullName => PrintSettings.PrinterName;
    /// <summary>
    /// Gets a value indicating whether this is the system default printer.
    /// </summary>
    public bool IsDefault
    {
      get { return PrintSettings.IsDefaultPrinter; }
    }
    /// <summary>
    /// Gets a value indicating the current status of this <see cref="Printer"/>'s queue.
    /// </summary>
    public string QueueStatus 
    {
      get 
      {
        try
        {
          return Describe(AccessQueue().QueueStatus);
        }
        catch (Exception Exception)
        {
          return Exception.Describe();
        }
      } 
    }
    /// <summary>
    /// Gets or sets a value indicating whether or not this printer prints in landscape format.
    /// </summary>
    public bool IsLandscape
    {
      get => PageSettings.Landscape;
      set => PageSettings.Landscape = value;
    }
    /// <summary>
    /// Gets a collection of paper sizes supported by this printer.
    /// </summary>
    public PrinterSettings.PaperSizeCollection PaperSizes => PrintSettings.PaperSizes;
    /// <summary>
    /// Gets a collection of paper sources supported by this printer.
    /// </summary>
    public PrinterSettings.PaperSourceCollection PaperSources => PrintSettings.PaperSources;
    /// <summary>
    /// Gets the default Page Settings for this printer.
    /// </summary>
    public PageSettings DefaultPageSettings => PrintSettings.DefaultPageSettings;
    /// <summary>
    /// Gets or sets the current paper size in use by this printer.
    /// </summary>
    public PaperSize PaperSize
    {
      get => PaperSizeField;
      set
      {
        PaperSizeField = value;
        PageSettings.PaperSize = value;
      }
    }
    /// <summary>
    /// Gets or sets the kind of paper in use by this printer.
    /// </summary>
    public PaperKind PaperKind
    {
      get => PaperSizeField.Kind;
      set
      {
        foreach (PaperSize p in PaperSizes)
        {
          if (p.Kind == value)
            PaperSize = p;
        }
      }
    }
    /// <summary>
    /// Gets or sets the paper source in use by this printer.
    /// </summary>
    public PaperSource PaperSource
    {
      get => PaperSourceField;
      set
      {
        PaperSourceField = value;
        PageSettings.PaperSource = value;
      }
    }
    /// <summary>
    /// Gets the maximum number of copies that the printer enables the user to print at a time.
    /// </summary>
    public int MaximumCopies => PrintSettings.MaximumCopies;
    public List<int> PagesPerSheetCapability => AccessQueue().GetPrintCapabilities().PagesPerSheetCapability.ToList();

    public System.Printing.PrintQueue AccessQueue()
    {
      var Result = FindQueue();

      Result.Refresh();

      return Result;
    }
    /// <summary>
    /// Get an icon for this printer.
    /// </summary>
    /// <returns>A bitmap image representation of this <see cref="Printer"/>.</returns>
    public Inv.Image GetIcon()
    {
      var printerName = PrintSettings.PrinterName;
      if (printerName.StartsWith("\\", StringComparison.Ordinal))
      {
        var parts = printerName.Split('\\');
        if (parts.Length == 4)
          printerName = parts[3] + " on " + parts[2];
      }

      Shell32.SHGetDesktopFolder(out var pDesktop);
      var desktop = (IShellFolder)Marshal.GetTypedObjectForIUnknown(pDesktop, typeof(IShellFolder));

      Shell32.SHGetSpecialFolderLocation(IntPtr.Zero, Shell32.CSIDL.PRINTERS, out var pPrinters);
      desktop.BindToObject(pPrinters, IntPtr.Zero, ref Shell32.IID_IShellFolder, out var pPrinterInterfaces);
      var printers = (IShellFolder)Marshal.GetTypedObjectForIUnknown(pPrinterInterfaces, typeof(IShellFolder));

      printers.EnumObjects(IntPtr.Zero, Shell32.SHCONTF.NONFOLDERS, out var pEnumId);
      var enumId = (IEnumIDList)Marshal.GetTypedObjectForIUnknown(pEnumId, typeof(IEnumIDList));

      var flag = Shell32.SHGFI.USEFILEATTRIBUTES | Shell32.SHGFI.SYSICONINDEX;
      var shfi = new Shell32.SHFILEINFO();
      var imageList = Shell32.SHGetFileInfo(".txt", Shell32.FILE_ATTRIBUTE.NORMAL, ref shfi, Shell32.cbFileInfo, flag);

      while (enumId.Next(1, out var pidlPrinter, out var fetched) == Shell32.S_OK && fetched == 1)
      {
        var pidlFull = new PIDL(pidlPrinter, true);
        pidlFull.Insert(pPrinters);

        Shell32.FILE_ATTRIBUTE dwAttr = 0;
        shfi = new Shell32.SHFILEINFO();
        var dwflag = /* Shell32.SHGFI.SYSICONINDEX | */ Shell32.SHGFI.PIDL | Shell32.SHGFI.ICON | Shell32.SHGFI.TYPENAME | Shell32.SHGFI.DISPLAYNAME;
        Shell32.SHGetFileInfo(pidlFull.Ptr, dwAttr, ref shfi, Shell32.cbFileInfo, dwflag);
        Marshal.FreeCoTaskMem(pidlFull.Ptr);

        // right, now we've done all that com stuff, we have an icon
        if (shfi.szDisplayName.Equals(printerName))
        {
          var i = Shell32.ImageList_ReplaceIcon(imageList, -1, shfi.hIcon);
          var iconPtr = Shell32.ImageList_GetIcon(imageList, i, Shell32.ILD.NORMAL);
          if (iconPtr != IntPtr.Zero)
          {
            var icon = System.Drawing.Icon.FromHandle(iconPtr);
            var retVal = (System.Drawing.Icon)icon.Clone();
            Shell32.DestroyIcon(iconPtr);

            if (retVal == null)
            {
              return null;
            }
            else
            {
              var hBitmap = retVal.ToBitmap().GetHbitmap();
              try
              {
                return System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, System.IntPtr.Zero, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions()).ConvertToInvImagePng();
              }
              finally
              {
                Win32.Gdi32.DeleteObject(hBitmap);
              }
            }
          }
        }
      }
      return null;
    }
    /// <summary>
    /// Display the printer properties dialog for this printer.
    /// (The window handle can be accessed using new WindowInteropHelper(_targetWindow).Handle)
    /// </summary>
    /// <param name="WindowHandle">A window handle corresponding to the parent window of the printer properties dialog.</param>
    /// <returns>True if the dialog was successful.</returns>
    public bool DoPropertiesDialog(IntPtr WindowHandle)
    {
      var printerName = PrintSettings.PrinterName;

      WinSpool.OpenPrinter(printerName, out var handle, IntPtr.Zero);
      try
      {
        var hDevMode = PrintSettings.GetHdevmode(PageSettings);
        try
        {
          var pDevMode = Kernel32.GlobalLock(hDevMode);
          try
          {
            var res = WinSpool.DocumentProperties(WindowHandle, handle, printerName, pDevMode, pDevMode, WinSpool.DM_IN_BUFFER | WinSpool.DM_PROMPT | WinSpool.DM_OUT_BUFFER) == 1; //1 = IDOK;
            if (res)
            {
              PrintSettings.SetHdevmode(hDevMode);
              PageSettings.SetHdevmode(hDevMode);

              LoadPageSettings();
            }
            return res;
          }
          finally
          {
            Kernel32.GlobalUnlock(pDevMode);
          }
        }
        finally
        {
          Kernel32.GlobalFree(hDevMode);
        }
      }
      finally
      {
        WinSpool.ClosePrinter(handle);
      }
    }

    private string Describe(PrintQueueStatus QueueStatus)
    {
      if (QueueStatus == PrintQueueStatus.None)
        return "Idle";
      else
        return QueueStatus.ToString();
    }
    private void LoadPageSettings()
    {
      try
      {
        PaperSizeField = PageSettings.PaperSize;
        PaperSource = PageSettings.PaperSource;
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Describe());

        if (Debugger.IsAttached)
          Debugger.Break();

        PaperSizeField = null;
        PaperSource = null;
      }
    }
    private System.Printing.PrintQueue FindQueue()
    {
      PrintQueue Result = null;

      var enumerationFlags = new EnumeratedPrintQueueTypes[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections };

      using (var ps = new LocalPrintServer())
      {
        foreach (var p in ps.GetPrintQueues(enumerationFlags))
        {
          if (p.FullName.Equals(PrintSettings.PrinterName))
            Result = p;
        }
      }

      if (Result == null)
        throw new Exception("unable to find queue for " + PrintSettings.PrinterName);

      return Result;
    }

    private PrinterSettings PrintSettings; 
    private PageSettings PageSettings;
    private PaperSize PaperSizeField;
    private PaperSource PaperSourceField;
  }

  /// <summary>
  /// Manages the list of system printers.
  /// </summary>
  public static class PrinterManager
  {
    static PrinterManager()
    {
      Printers = new Inv.DistinctList<Printer>();
      DefaultPrinter = null;

      try
      {
        foreach (string PrintName in PrinterSettings.InstalledPrinters)
        {
          var Printer = new Printer(new PrinterSettings()
          {
            PrinterName = PrintName
          });

          Printers.Add(Printer);

          if (Printer.IsDefault)
            DefaultPrinter = Printer;
        }
      }
      catch (Exception Exception)
      {
        Debug.WriteLine(Exception.Describe());

        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }

    /// <summary>
    /// The default printer for the system.
    /// </summary>
    public static Inv.Printer DefaultPrinter { get; private set; }
    /// <summary>
    /// The list of printers installed on the system.
    /// </summary>
    public static Inv.DistinctList<Printer> Printers { get; private set; }

    public static Inv.Printer GetPrinter(string PrinterName)
    {
      return PrinterName != null ? Printers.FirstOrDefault(P => string.Equals(P.FullName, PrinterName, StringComparison.InvariantCultureIgnoreCase)) : null;
    }
    public static IEnumerable<Inv.Printer> GetPrinters(params string[] PrinterNameArray)
    {
      var Result = new Inv.DistinctList<Inv.Printer>();

      foreach (var PrinterName in PrinterNameArray)
      {
        var Printer = GetPrinter(PrinterName);

        if (Printer != null)
          Result.Add(Printer);
      }

      return Result;
    }
  }
}
