﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public sealed class TimeUsage
  {
    internal TimeUsage(long Count, Inv.Time From, Inv.Time Until)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(From <= Until, "From must precede Until.");

      this.Count = Count;
      this.From = From;
      this.Until = Until;
    }

    public long Count { get; internal set; }
    public Inv.Time From { get; internal set; }
    public Inv.Time Until { get; internal set; }
  }

  public sealed class TimeUsageSet
  {
    public TimeUsageSet()
    {
      this.UsageList = new Inv.DistinctList<TimeUsage>();
    }

    public void Clear()
    {
      UsageList.Clear();
    }
    public void Merge(long Count, Inv.Time From, Inv.Time Until)
    {
      var IsComplete = false;
      var CurrentFrom = From;
      var CurrentUntil = Until;

      var UsageIndex = 0;
      while (!IsComplete && UsageList.ExistsAt(UsageIndex))
      {
        var CurrentUsage = UsageList[UsageIndex];

        if (CurrentUsage.From >= CurrentUntil)
        {
          IsComplete = true;
          UsageList.Insert(UsageIndex, new TimeUsage(Count, CurrentFrom, CurrentUntil));
          break;
        }

        if (CurrentUsage.From == CurrentFrom)
        {
          if (CurrentUsage.Until == CurrentUntil)
          {
            IsComplete = true;
            CurrentUsage.Count += Count;
            break;
          }
          else if (CurrentUsage.Until > CurrentUntil)
          {
            IsComplete = true;
            UsageList.Insert(UsageIndex, new TimeUsage(Count + CurrentUsage.Count, CurrentFrom, CurrentUntil));
            CurrentUsage.From = CurrentUntil;
            break;
          }
          else
          {
            CurrentUsage.Count += Count;
            CurrentFrom = CurrentUsage.Until;
            UsageIndex++;
            continue;
          }
        }

        if (CurrentUsage.From.IsBetween(CurrentFrom, CurrentUntil))
        {
          UsageList.Insert(UsageIndex, new TimeUsage(Count, CurrentFrom, CurrentUsage.From));
          CurrentFrom = CurrentUsage.From;
          UsageIndex++;
          continue;
        }

        if (CurrentFrom.IsBetween(CurrentUsage.From, CurrentUsage.Until))
        {
          UsageList.Insert(UsageIndex + 1, new TimeUsage(CurrentUsage.Count, CurrentFrom, CurrentUsage.Until));
          CurrentUsage.Until = CurrentFrom;
          UsageIndex++;
          continue;
        }

        if (CurrentFrom > CurrentUsage.Until)
        {
          UsageIndex++;
          continue;
        }

        Debug.Assert(false, "Unexpected merge condition.");
      }

      if (!IsComplete)
        UsageList.Add(new TimeUsage(Count, CurrentFrom, CurrentUntil));
    }
    public IEnumerable<TimeUsage> EnumerateUsages()
    {
      return UsageList;
    }

    private Inv.DistinctList<TimeUsage> UsageList;
  }
}