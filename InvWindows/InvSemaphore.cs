﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inv
{
  public sealed class GlobalSemaphore : IDisposable
  {
    public GlobalSemaphore(string Name)
    {
      var SemaphoreId = $@"Global\{Name}";

      this.Handle = new Semaphore(1, 1, SemaphoreId);

      var AllowEveryoneRule = new SemaphoreAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), SemaphoreRights.FullControl, AccessControlType.Allow);
      var SecuritySettings = new SemaphoreSecurity();
      SecuritySettings.AddAccessRule(AllowEveryoneRule);
      Handle.SetAccessControl(SecuritySettings);
    }
    public void Dispose()
    {
      if (Handle != null)
      {
        Unlock();
        Handle.Dispose();
        Handle = null;
      }
    }

    public bool Lock(TimeSpan Timeout)
    {
      if (!IsAcquired)
      {
        try
        {
          IsAcquired = Handle.WaitOne(Timeout, false);
        }
        catch (AbandonedMutexException)
        {
          IsAcquired = true;
        }
      }

      return IsAcquired;
    }
    public void Unlock()
    {
      if (IsAcquired)
      {
        Handle.Release();
        IsAcquired = false;
      }
    }

    private Semaphore Handle;
    private bool IsAcquired;
  }
}
