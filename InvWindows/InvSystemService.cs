﻿/*! 8 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceProcess;
using System.Configuration.Install;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;
using Inv.Support;
using Microsoft.Win32;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Management;
using System.Windows;

namespace Inv
{
  /// <summary>
  /// Indicates the current state of a Windows service.
  /// </summary>
  public enum SystemServiceStatus
  {
    /// <summary>
    /// The service is stopped.
    /// </summary>
    Stopped, 
    /// <summary>
    /// The service is starting.
    /// </summary>
    StartPending, 
    /// <summary>
    /// The service is stopping.
    /// </summary>
    StopPending, 
    /// <summary>
    /// The service is running.
    /// </summary>
    Running, 
    /// <summary>
    /// The service is continuing after a pause.
    /// </summary>
    ContinuePending, 
    /// <summary>
    /// The service is pausing.
    /// </summary>
    PausePending, 
    /// <summary>
    /// The service is paused.
    /// </summary>
    Paused
  }

  /// <summary>
  /// An interface to the Windows service system.
  /// </summary>
  /// <remarks>
  /// <para>Provides methods to manipulate the Windows service system by adding/removing services, starting and stopping existing services.</para>
  /// <para>Also provides a wrapper around Windows service functionality, allowing you to host your code as a Windows service.</para>
  /// <example>
  /// <para>Using <see cref="SystemServiceManager"/> to host code as a Windows service:</para>
  /// <code>
  /// using (var Service = new Inv.SystemService())
  /// {
  ///   Service.StartEvent += () =>
  ///   {
  ///     // Delegate will be called when the service starts.
  ///   };
  ///   
  ///   Service.StopEvent += () =>
  ///   {
  ///     // Delegate will be called when the service shuts down.
  ///   };
  ///   
  ///   // Run the service.
  ///   Service.Run();
  /// }
  /// </code>
  /// <para>Using <see cref="SystemServiceManager"/> to register the currently executing assembly as a new Windows service:</para>
  /// <code>
  /// using (var Service = new Inv.SystemService())
  ///   Service.Install("ServiceName", "Service Display Name", "Description of the service", Assembly.GetEntryAssembly().Location);
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class SystemServiceManager
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SystemServiceManager"/> class.
    /// </summary>
    public SystemServiceManager(string MachineName, string ServiceName)
    {
      this.MachineName = MachineName;
      this.ServiceName = ServiceName;
    }

    public string MachineName { get; private set; }
    public string ServiceName { get; private set; }

    public bool IsLocalMachine
    {
      get { return MachineName == null || MachineName == "."; }
    }
    
    /// <summary>
    /// Register a new Windows service.
    /// </summary>
    /// <param name="Name">The service name.</param>
    /// <param name="Display">The long display name for a service</param>
    /// <param name="Description">A description of the service.</param>
    /// <param name="Executable">The path to the service executable.</param>
    /// <param name="Username">The account username to run the service as.</param>
    /// <param name="Password">The account password to run the service as.</param>
    public void Install(string Display, string Description, string Executable, string Username, string Password, params string[] DependencyArray)
    {
      if (IsLocalMachine)
      {
        using (var ProcessServiceInstaller = new ServiceProcessInstaller())
        using (var ServiceInstaller = new ServiceInstaller())
        {
          var NTService = false;

          if (string.IsNullOrWhiteSpace(Username))
          {
            ProcessServiceInstaller.Account = ServiceAccount.LocalSystem;
            ProcessServiceInstaller.Username = null;
            ProcessServiceInstaller.Password = null;
          }
          else if (Username.ToLower().StartsWith("nt service"))
          {
            ProcessServiceInstaller.Account = ServiceAccount.LocalSystem;
            ProcessServiceInstaller.Username = null;
            ProcessServiceInstaller.Password = null;

            NTService = true;
          }
          else if (Username.ToLower().EndsWith("local service"))
          {
            ProcessServiceInstaller.Account = ServiceAccount.LocalService;
            ProcessServiceInstaller.Username = "NT AUTHORITY\\Local Service";
            ProcessServiceInstaller.Password = null;
          }
          else if (Username.ToLower().EndsWith("network service"))
          {
            ProcessServiceInstaller.Account = ServiceAccount.NetworkService;
            ProcessServiceInstaller.Username = "NT AUTHORITY\\Network Service";
            ProcessServiceInstaller.Password = null;
          }
          else
          {
            var FullUsername = Username;
            if (!FullUsername.Contains('\\'))
              FullUsername = Environment.MachineName + "\\" + FullUsername;

            ProcessServiceInstaller.Account = ServiceAccount.User;
            ProcessServiceInstaller.Username = FullUsername;
            ProcessServiceInstaller.Password = Password;

            using (var LsaWrapper = new Win32.LsaController())
              LsaWrapper.AddPrivileges(FullUsername, "SeServiceLogonRight");
          }

          ServiceInstaller.ServiceName = ServiceName;
          ServiceInstaller.DisplayName = Display;
          ServiceInstaller.Description = Description;
          ServiceInstaller.DelayedAutoStart = DependencyArray == null || DependencyArray.Length == 0;
          ServiceInstaller.StartType = ServiceStartMode.Automatic;
          ServiceInstaller.Context = new InstallContext("", new string[] { string.Format(CultureInfo.InvariantCulture, "/assemblypath={0}", Executable) });
          ServiceInstaller.Parent = ProcessServiceInstaller;

          if (DependencyArray != null)
            ServiceInstaller.ServicesDependedOn = DependencyArray;

          var ServiceInstallState = new System.Collections.Specialized.ListDictionary();
          ServiceInstaller.Install(ServiceInstallState);

          if (NTService)
          {
            using (var Service = new ManagementObject(new ManagementPath(string.Format("Win32_Service.Name='{0}'", ServiceName))))
            {
              var WMIParams = new object[11];
              WMIParams[6] = @"NT Service\" + ServiceName;
              Service.InvokeMethod("Change", WMIParams);
            }
          }
        }
      }
      else
      {
        // Command line utility required for remote machine installation.
        var UsernameOption = Username != null ? " obj= " + Username : "";
        var PasswordOption = Username != null ? " password= " + Password : "";
        var DependencyOption = DependencyArray != null && DependencyArray.Length > 0 ? " depend= " + DependencyArray.AsSeparatedText("/") : "";

        var ProcessStart = new ProcessStartInfo();
        ProcessStart.FileName = "sc";
        ProcessStart.Arguments = string.Format(@"\\{0}" + " create {1} binpath= \"{2}\" DisplayName= \"{3}\" start= auto{4}{5}{6}", MachineName, ServiceName, Executable, Display, UsernameOption, PasswordOption, DependencyOption);
        ProcessStart.WindowStyle = ProcessWindowStyle.Hidden;

        var ProcessResult = Process.Start(ProcessStart);
        ProcessResult.WaitForExit();
        if (ProcessResult.ExitCode != 0)
          throw new Exception("Failed to install service on remote machine with exit code " + ProcessResult.ExitCode);
      }
    }
    /// <summary>
    /// Unregister a Windows service from the list of known services.
    /// </summary>
    /// <param name="Name">The name of the service to remove.</param>
    public void Remove()
    {
      if (IsLocalMachine)
      {
        using (var ProcesServiceInstaller = new ServiceProcessInstaller())
        using (var ServiceInstaller = new ServiceInstaller())
        {
          ProcesServiceInstaller.Account = ServiceAccount.LocalSystem;
          ProcesServiceInstaller.Username = null;
          ProcesServiceInstaller.Password = null;

          ServiceInstaller.ServiceName = ServiceName;
          ServiceInstaller.Context = new InstallContext("", null);
          ServiceInstaller.Parent = ProcesServiceInstaller;

          ServiceInstaller.Uninstall(null);
        }
      }
      else
      {
        var ProcessStart = new ProcessStartInfo();
        ProcessStart.FileName = "sc";
        ProcessStart.Arguments = string.Format(@"\\{0}" + " delete {1}", MachineName, ServiceName);
        ProcessStart.WindowStyle = ProcessWindowStyle.Hidden;

        var ProcessResult = Process.Start(ProcessStart);
        if (!ProcessResult.WaitForExit(30000))
          throw new Exception("Remove service command on remote machine failed to return within the 30 second timeout.");

        if (ProcessResult.ExitCode != 0)
          throw new Exception("Failed to remove service on remote machine with exit code " + ProcessResult.ExitCode + ".");
      }
    }
    /// <summary>
    /// Check to see whether or not a given Windows service exists.
    /// </summary>
    /// <param name="Name">The name of the service to look for.</param>
    /// <returns>True if the service exists, False otherwise.</returns>
    public bool Exists()
    {
      return ServiceController.GetServices(MachineName ?? ".").Any(Index => Index.ServiceName == ServiceName);
    }
    public SystemServiceControl NewControl()
    {
      return new SystemServiceControl(this);
    }
  }

  public sealed class SystemServiceControl : IDisposable
  {
    internal SystemServiceControl(SystemServiceManager Manager)
    {
      this.ServiceController = new ServiceController(Manager.ServiceName, Manager.MachineName ?? ".");
    }
    public void Dispose()
    {
      ServiceController.Dispose();
    }

    /// <summary>
    /// Start a Windows service.
    /// </summary>
    /// <param name="Name">The name of the service to start.</param>
    public void Start()
    {
      ProtectCall(() => ServiceController.Start());
    }
    /// <summary>
    /// Stop a Windows service.
    /// </summary>
    /// <param name="Name">The name of the service to stop.</param>
    public void Stop()
    {
      ProtectCall(() => ServiceController.Stop());
    }
    public void WaitForStatus(Inv.SystemServiceStatus Status, TimeSpan? Timeout = null)
    {
      ProtectCall(() => ServiceController.WaitForStatus(StatusDictionary.GetByRight(Status), Timeout ?? TimeSpan.FromMinutes(1)));
    }
    public void Refresh()
    {
      this.FilePathField = null;
      this.DependencyArrayField = null;
      this.StatusField = null;
      this.ServiceStatusField = null;

      ProtectCall(() => ServiceController.Refresh());
    }
    public bool IsLocalMachine()
    {
      return ServiceController.MachineName == "" || ServiceController.MachineName == "." || string.Equals(ServiceController.MachineName, Environment.MachineName, StringComparison.CurrentCultureIgnoreCase);
    }
    /// <summary>
    /// Get the status of a Windows service.
    /// </summary>
    /// <param name="Name">The name of the service to retrieve status information for.</param>
    /// <returns>The status of the service.</returns>
    public Inv.SystemServiceStatus Status
    {
      get 
      { 
        if (StatusField == null)
          StatusField = StatusDictionary.GetByLeft(ServiceController.Status);

        return StatusField.Value;
      }
    }
    public string[] DependencyArray
    {
      get 
      {
        if (DependencyArrayField == null)
          DependencyArrayField = ServiceController.ServicesDependedOn.Select(D => D.ServiceName).ToArray();

        return DependencyArrayField;
      }
    }
    public string FilePath
    {
      get
      {
        if (FilePathField == null)
        {
          var RegistryPath = @"SYSTEM\CurrentControlSet\Services\" + ServiceController.ServiceName;
          var LocalMachineKey = Registry.LocalMachine;

          using (var RegKey = IsLocalMachine() ? LocalMachineKey.OpenSubKey(RegistryPath) : RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, ServiceController.MachineName).OpenSubKey(RegistryPath))
            FilePathField = ExpandEnvironmentVariables(RegKey.GetValue("ImagePath").ToString().Trim('"'));
        }

        return FilePathField;
      }
    }
    public int ProcessID
    {
      get { return AccessServiceStatus().processID; }
    }

    private void ProtectCall(Action Action)
    {
      try
      {
        Action();
      }
      catch (InvalidOperationException Exception)
      {
        var Win32Exception = Exception.InnerException as System.ComponentModel.Win32Exception;

        if (Win32Exception != null)
          throw new InvalidOperationException("[" + Win32Exception.Message + "] " + Exception.Describe(), Win32Exception);
        else
          throw Exception.Preserve();
      }
    }
    private Win32.advapi32.SERVICE_STATUS_PROCESS AccessServiceStatus()
    {
      if (ServiceStatusField == null)
      {
        var Handle = ServiceController.ServiceHandle.DangerousGetHandle();
        var Buffer = IntPtr.Zero;
        try
        {
          var Size = 0;

          Win32.advapi32.QueryServiceStatusEx(Handle, 0, Buffer, Size, out Size);

          Buffer = Marshal.AllocHGlobal(Size);

          if (!Win32.advapi32.QueryServiceStatusEx(Handle, 0, Buffer, Size, out Size))
            throw new Win32Exception(Marshal.GetLastWin32Error());

          ServiceStatusField = (Win32.advapi32.SERVICE_STATUS_PROCESS)Marshal.PtrToStructure(Buffer, typeof(Win32.advapi32.SERVICE_STATUS_PROCESS));
        }
        finally
        {
          if (!Buffer.Equals(IntPtr.Zero))
            Marshal.FreeHGlobal(Buffer);
        }
      }

      return ServiceStatusField.Value;
    }
    private string ExpandEnvironmentVariables(string Path)
    {
      if (IsLocalMachine())
      {
        return Environment.ExpandEnvironmentVariables(Path);
      }
      else
      {
        var SystemRootKey = @"Software\Microsoft\Windows NT\CurrentVersion\";

        using (var RegKey = RegistryKey.OpenRemoteBaseKey(RegistryHive.LocalMachine, ServiceController.MachineName).OpenSubKey(SystemRootKey))
          return Path.Replace("%SystemRoot%", RegKey.GetValue("SystemRoot").ToString());
      }
    }

    private static Inv.Bidictionary<ServiceControllerStatus, Inv.SystemServiceStatus> StatusDictionary = new Inv.Bidictionary<ServiceControllerStatus, SystemServiceStatus>()
    {
      { ServiceControllerStatus.Stopped, Inv.SystemServiceStatus.Stopped },
      { ServiceControllerStatus.StartPending, Inv.SystemServiceStatus.StartPending },
      { ServiceControllerStatus.StopPending, Inv.SystemServiceStatus.StopPending },
      { ServiceControllerStatus.Running, Inv.SystemServiceStatus.Running },
      { ServiceControllerStatus.ContinuePending, Inv.SystemServiceStatus.ContinuePending },
      { ServiceControllerStatus.PausePending, Inv.SystemServiceStatus.PausePending },
      { ServiceControllerStatus.Paused, Inv.SystemServiceStatus.Paused }
    };

    private ServiceController ServiceController;
    private Inv.SystemServiceStatus? StatusField;
    private string[] DependencyArrayField;
    private string FilePathField;
    private Win32.advapi32.SERVICE_STATUS_PROCESS? ServiceStatusField;
  }

  public sealed class SystemServiceApplication
  {
    public SystemServiceApplication(params string[] NameArray)
    {
      this.ServiceName = NameArray.AsSeparatedText(".");
      this.ServiceDisplay = NameArray.AsSeparatedText(" ");

      this.ServiceCommandMap = new Inv.CommandMap("Service");
      this.ServiceNameSwitch = ServiceCommandMap.AddSwitch<string>("service-name", "", null, null);
      this.ServiceDisplaySwitch = ServiceCommandMap.AddSwitch<string>("service-display", "", null, null);
      this.ServiceDescriptionSwitch = ServiceCommandMap.AddSwitch<string>("service-description", "", null, null);
      this.ServiceUsernameSwitch = ServiceCommandMap.AddSwitch<string>("service-username", "", null, null);
      this.ServicePasswordSwitch = ServiceCommandMap.AddSwitch<string>("service-password", "", null, null);
      this.ServiceDependencySwitch = ServiceCommandMap.AddSwitch<string>("service-dependency", "", null, null);
    }

    public string ServiceName { get; private set; }
    public string ServiceDisplay { get; private set; }
    public bool IsActive { get; private set; }
    public event Action<string[]> StartEvent;
    public event Action StopEvent;
    public event Action RunEvent;
    public event Action<Exception> HandleExceptionEvent;
    public bool IsManaged { get; private set; }
    public bool HasConsole { get; private set; }

    public void Run(string[] CommandArgumentArray)
    {
      // TODO: this is incorrect, you need to install properly (or at least need to acknowledge the deploymentarea.txt file for the service name).
      var ArgumentPrefixes = new char[] { '/', '-' };
      var Command = CommandArgumentArray.Length >= 1 ? CommandArgumentArray[0].TrimStart(ArgumentPrefixes).ToLower() : "";

      ServiceCommandMap.Load(CommandArgumentArray);

      var SystemService = new Inv.SystemServiceManager(null, ServiceNameSwitch.ActiveValue ?? ServiceName);

      switch (Command)
      {
        case "install":
          var InstallDisplay = ServiceDisplaySwitch.ActiveValue ?? ServiceNameSwitch.ActiveValue ?? ServiceDisplay;
          var InstallDescription = ServiceDescriptionSwitch.ActiveValue ?? ServiceDisplaySwitch.ActiveValue ?? ServiceNameSwitch.ActiveValue ?? ServiceDisplay;
          var InstallExecutable = Assembly.GetEntryAssembly().Location;
          var InstallUsername = ServiceUsernameSwitch.ActiveValue.EmptyAsNull();
          var InstallPassword = ServicePasswordSwitch.ActiveValue.EmptyAsNull();
          var InstallDependencyArray = ServiceDependencySwitch.ActiveValue?.Split(',').Select(V => V.Trim().EmptyAsNull()).ExceptNull().ToArray();

          void Install()
          {
            SystemService.Install
            (
              Display: InstallDisplay,
              Description: InstallDescription,
              Executable: InstallExecutable,
              Username: InstallUsername,
              Password: InstallPassword,
              DependencyArray: InstallDependencyArray
            );
          }

          if (Environment.UserInteractive)
          {
            Console.Show();

            Console.WriteLine("Install " + SystemService.ServiceName + " (use blank for LocalSystem)");
            Console.WriteLine();

            do
            {
              if (InstallUsername == null)
              {
                Console.Write("Username: ");
                InstallUsername = Console.ReadLine();
              }

              if (InstallUsername != "" && InstallPassword == null)
              {
                Console.Write("Password: ");
                InstallPassword = ReadPassword();
              }

              var InstallDomain = Environment.UserDomainName;

              var NameArray = InstallUsername.Split(new char[] { '\\' }, 2);
              if (NameArray.Length == 2)
              {
                InstallDomain = NameArray[0];
                InstallUsername = NameArray[1];
              }

              if (InstallUsername == "")
              {
                // install as LocalSystem.
                InstallPassword = "";
              }
              else
              {
                var Identity = Inv.Support.WindowsIdentityHelper.GetWindowsIdentity(InstallDomain, InstallUsername, InstallPassword, false);
                if (Identity == null)
                {
                  Console.WriteLine($"Unable to authenticate user {InstallDomain}\\{InstallUsername} with password {new string('*', (InstallPassword ?? "").Length)}.");
                  Console.WriteLine();

                  InstallUsername = null;
                  InstallPassword = null;
                }
                else
                {
                  InstallUsername = InstallDomain + "\\" + InstallUsername;
                }
              }
            }
            while (InstallUsername == null || InstallPassword == null);
          }

          Console.WriteLine();
          if (InstallUsername == "")
            Console.WriteLine($"Installing {SystemService.ServiceName} as LocalSystem.");
          else
            Console.WriteLine($"Installing {SystemService.ServiceName} as {InstallUsername ?? ""} with password {new string('*', (InstallPassword ?? "").Length)}.");
          Console.WriteLine();

          Install();
          break;

        case "start":
          using (var SystemServiceControl = SystemService.NewControl())
          {
            SystemServiceControl.Start();
            SystemServiceControl.WaitForStatus(Inv.SystemServiceStatus.Running);
          }
          break;

        case "stop":
          using (var SystemServiceControl = SystemService.NewControl())
          {
            SystemServiceControl.Stop();
            SystemServiceControl.WaitForStatus(Inv.SystemServiceStatus.Stopped);
          }
          break;

        case "uninstall":
        case "remove":
          SystemService.Remove();
          break;

        case "debug":
        case "console":
          HasConsole = true;

          try
          {
            Console.Show();

            Console.WriteLine("Starting " + ServiceName + "...");
            StartInvoke(CommandArgumentArray);

            if (IsActive)
              Console.WriteLine(ServiceName + " is active.");
            else
              Console.WriteLine(ServiceName + " is NOT ACTIVE.");

            if (IsActive && RunEvent != null)
            {
              RunEvent();
            }
            else
            {
              Console.WriteLine("Press ENTER to shutdown this process");
              try
              {
                Console.ReadLine();
              }
              catch (System.IO.IOException)
              {
                // NOTE: in some scenarios it throws an exception: "The handle is invalid."
              }
            }

            Console.WriteLine("Stopping " + ServiceName + "...");
            StopInvoke();
          }
          catch (Exception E)
          {
            try
            {
              Console.WriteLine();
              Console.WriteLine(E.Message);
              Console.WriteLine();
              Console.WriteLine(E.StackTrace);
              Console.ReadLine();
            }
            catch (System.IO.IOException)
            {
              // NOTE: in some scenarios it throws an exception: "The handle is invalid."
            }
          }

          //Win32.Kernel32.FreeConsole();
          break;

        default:
          if (RunEvent != null)
          {
            StartInvoke(CommandArgumentArray);

            if (IsActive)
              RunEvent();

            StopInvoke();
          }
          else
          {
            IsManaged = true;

            using (var Wrapper = new ServiceWrapper(this))
              Wrapper.Run();
          }
          break;
      }
    }
    public void Terminate(string Message)
    {
      IsActive = false;

      if (IsManaged)
      {
        try
        {
          EventLog.WriteEntry(".NET Runtime", Message, EventLogEntryType.Warning);
        }
        catch
        {
        }

        // NOTE: doesn't use Inv.SystemService because we can't wait for the status to become stopped (we are stopping ourself).
        using (var Service = new System.ServiceProcess.ServiceController(ServiceName))
          Service.Stop();
      }
      else
      {
        //// NOTE: This is done so that the Console.ReadLine() after "Press ENTER to stop the service" is tripped and the StopInvoke is executed.
        Win32.User32.PostMessage(System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle, (uint)Win32.User32.WM_KEYDOWN, (int)Win32.User32.VirtualKeyCode.RETURN, 0);
      }
    }
    public void Restart(string Message)
    {
      if (IsManaged)
      {
        // assumes that the running user has sufficient permissions to restart this service.
        var PROC = new Process();
        var PSI = new ProcessStartInfo();

        PSI.CreateNoWindow = true;
        PSI.FileName = "cmd.exe";
        PSI.Arguments = string.Format("/C net stop {0} && net start {0}", ServiceName);
        PSI.LoadUserProfile = false;
        PSI.UseShellExecute = false;
        PSI.WindowStyle = ProcessWindowStyle.Hidden;

        PROC.StartInfo = PSI;
        PROC.Start();
      }
      else
      {
        Process.Start(Assembly.GetEntryAssembly().Location);
        Terminate(Message);
      }  
    }

    private string ReadPassword()
    {
      var Result = "";
      var KeyInfo = Console.ReadKey(true);
      while (KeyInfo.Key != ConsoleKey.Enter)
      {
        if (KeyInfo.Key != ConsoleKey.Backspace)
        {
          Console.Write("*");
          Result += KeyInfo.KeyChar;
        }
        else if (KeyInfo.Key == ConsoleKey.Backspace)
        {
          if (!string.IsNullOrEmpty(Result))
          {
            // remove one character from the list of password characters
            Result = Result.Substring(0, Result.Length - 1);
            // get the location of the cursor
            var pos = Console.CursorLeft;
            // move the cursor to the left by one character
            Console.SetCursorPosition(pos - 1, Console.CursorTop);
            // replace it with space
            Console.Write(" ");
            // move the cursor to the left by one character again
            Console.SetCursorPosition(pos - 1, Console.CursorTop);
          }
        }
        KeyInfo = Console.ReadKey(true);
      }
      // add a new line because user pressed enter at the end of their password
      Console.WriteLine();
      return Result;
    }  
    private void StartInvoke(string[] args)
    {
      Inv.SystemExceptionHandler.Recruit(UnhandledException);

      this.IsActive = true;
      try
      {
        if (StartEvent != null)
          StartEvent(args);
      }
      catch (Exception Exception)
      {
        this.IsActive = false;

        PrintException(Exception);
      }
    }
    private void StopInvoke()
    {
      this.IsActive = false;

      try
      {
        if (StopEvent != null)
          StopEvent();
      }
      catch (Exception Exception)
      {
        PrintException(Exception);
      }

      Inv.SystemExceptionHandler.Dismiss(UnhandledException);
    }
    private void UnhandledException(object Object)
    {
      if (Object == null)
        return;

      var Exception = Object as Exception;

      if (Exception == null)
      {
        if (HasConsole)
        {
          Console.WriteLine("Type = " + Object.GetType().AssemblyQualifiedName);
          Console.WriteLine("Text = " + Object.ToString());
        }
      }
      else
      {
        PrintException(Exception);

        if (HandleExceptionEvent != null)
          HandleExceptionEvent(Exception);
      }
    }
    private void PrintException(Exception Exception)
    {
      if (HasConsole)
      {
        Console.WriteLine("Type = " + Exception.GetType().FullName);
        Console.WriteLine("Message = " + Exception.Describe());
        Console.WriteLine("StackTrace =\r\n" + Exception.StackTrace);
      }
    }
    
    private Inv.CommandMap ServiceCommandMap;
    private Inv.CommandSwitch<string> ServiceNameSwitch;
    private Inv.CommandSwitch<string> ServiceDisplaySwitch;
    private Inv.CommandSwitch<string> ServiceDescriptionSwitch;
    private Inv.CommandSwitch<string> ServiceUsernameSwitch;
    private Inv.CommandSwitch<string> ServicePasswordSwitch;
    private CommandSwitch<string> ServiceDependencySwitch;

    private sealed class ServiceWrapper : ServiceBase
    {
      internal ServiceWrapper(SystemServiceApplication Application)
      {
        this.Application = Application;
      }

      internal void Run()
      {
        ServiceBase.Run(this);
      }

      protected override void OnContinue()
      {
      }
      protected override void OnCustomCommand(int command)
      {
      }
      protected override void OnPause()
      {
      }
      protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus)
      {
        return base.OnPowerEvent(powerStatus);
      }
      protected override void OnSessionChange(SessionChangeDescription changeDescription)
      {
      }
      protected override void OnShutdown()
      {
      }
      protected override void OnStart(string[] args)
      {
        var ProcessId = System.Diagnostics.Process.GetCurrentProcess().Id;
        var query = "SELECT * FROM Win32_Service where ProcessId = " + ProcessId;
        var ManagementObjectSearcher = new System.Management.ManagementObjectSearcher(query);

        foreach (System.Management.ManagementObject queryObj in ManagementObjectSearcher.Get())
          Application.ServiceName = queryObj["Name"].ToString();

        Application.StartInvoke(args);

        if (!Application.IsActive)
          Stop();
      }
      protected override void OnStop()
      {
        Application.StopInvoke();
      }

      private SystemServiceApplication Application;
    }
  }

  public static class SystemExceptionHandler
  {
    static SystemExceptionHandler()
    {
      if (System.ServiceModel.Dispatcher.ExceptionHandler.AsynchronousThreadExceptionHandler != null)
        throw new Exception("AsynchronousThreadExceptionHandler has already been set.");

      AsyncHandler = new AsyncExceptionHandler();
      System.ServiceModel.Dispatcher.ExceptionHandler.AsynchronousThreadExceptionHandler = AsyncHandler;
    }

    /// <summary>
    /// Register a global exception handler for otherwise unhandled exceptions.
    /// </summary>
    /// <param name="ExceptionAction"></param>
    public static void Recruit(Action<object> ExceptionAction)
    {
      if (ExceptionAction == null)
        return;

      lock (CriticalSection)
      {
        // can't recruit if already added.
        if (ExceptionDelegate != null && ExceptionDelegate.GetInvocationList().Contains(ExceptionAction))
          return;

        if (ExceptionDelegate == null)
        {
          AsyncHandler.FaultDelegate += AsynchronousExceptionHandler;
          System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionEventHandler;
          System.Threading.Tasks.TaskScheduler.UnobservedTaskException += UnhandledTaskExceptionEventHandler;
          
        }

        ExceptionDelegate += ExceptionAction;
      }
    }
    /// <summary>
    /// Remove a global exception handler for otherwise unhandled exceptions.
    /// </summary>
    /// <param name="ExceptionAction"></param>
    public static void Dismiss(Action<object> ExceptionAction)
    {
      if (ExceptionAction == null)
        return;

      lock (CriticalSection)
      {
        // can't dismiss if not added.
        if (ExceptionDelegate == null || !ExceptionDelegate.GetInvocationList().Contains(ExceptionAction))
          return;

        ExceptionDelegate -= ExceptionAction;

        if (ExceptionDelegate == null)
        {
          AsyncHandler.FaultDelegate -= AsynchronousExceptionHandler;
          System.AppDomain.CurrentDomain.UnhandledException -= UnhandledExceptionEventHandler;
          System.Threading.Tasks.TaskScheduler.UnobservedTaskException -= UnhandledTaskExceptionEventHandler;
        }
      }
    }

    private static void AsynchronousExceptionHandler(Exception Exception)
    {
      ExceptionInvoke(Exception);
    }
    private static void UnhandledExceptionEventHandler(object sender, UnhandledExceptionEventArgs e)
    {
      ExceptionInvoke(e.ExceptionObject);
    }
    private static void UnhandledTaskExceptionEventHandler(object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e)
    {
      if (!e.Observed)
      {
        ExceptionInvoke(e.Exception);

        e.SetObserved();
      }
    }
    private static void ExceptionInvoke(object Object)
    {
      if (Object == null)
        return;

      Action<object> LocalDelegate;
      lock (CriticalSection)
        LocalDelegate = ExceptionDelegate;

      if (LocalDelegate != null)
        LocalDelegate(Object);
    }

    private static readonly AsyncExceptionHandler AsyncHandler;
    private static Action<object> ExceptionDelegate;
    private static object CriticalSection = new object();

    private sealed class AsyncExceptionHandler : System.ServiceModel.Dispatcher.ExceptionHandler
    {
      public AsyncExceptionHandler()
        : base()
      {
      }

      public Action<Exception> FaultDelegate;

      public override bool HandleException(Exception exception)
      {
        try
        {
          var LocalDelegate = FaultDelegate;

          if (LocalDelegate != null)
            LocalDelegate(exception);
          else
            return false;
        }
        catch
        {
          return false;
        }

        return true; // TRUE means this exception was handled and don't terminate the process.
      }
    }
  }
}
