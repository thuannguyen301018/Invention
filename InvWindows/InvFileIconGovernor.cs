﻿/*! 8 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.ComponentModel;
using Inv.Support;

namespace Inv
{
  public static class FileIconGovernor
  {
    static FileIconGovernor()
    {
      Cache = new Inv.EnumArray<FileIconSize, Dictionary<string, System.Windows.Media.Imaging.BitmapSource>>();
      Cache.Fill(ImageSize => new Dictionary<string, BitmapSource>());
    }

    public static Inv.Image Load(string Extension, FileIconSize Size)
    {
      return ExtractSource(Extension, Size).ConvertToInvImagePng();
    }

    internal static BitmapSource ExtractSource(string Extension, FileIconSize IconSize)
    {
      // TODO: why was this assertion here? It seems to work on background threads.
      //Debug.Assert(System.Threading.Thread.CurrentThread.GetApartmentState() == System.Threading.ApartmentState.STA, "Must be called from a STA thread.");

      if (Extension == null)
      {
        return null;
      }
      else
      {
        var Dictionary = Cache[IconSize];

        lock (Dictionary)
        {
          var Result = Dictionary.GetValueOrDefault(Extension);

          if (Result == null)
          {
            var ShInfo = new Win32.Shell32.SHFILEINFO();

            var FileInfo = Win32.Shell32.SHGetFileInfo(Extension, Win32.Shell32.FILE_ATTRIBUTE.NORMAL, ref ShInfo, Marshal.SizeOf(ShInfo), Win32.Shell32.SHGFI.SYSICONINDEX | Win32.Shell32.SHGFI.USEFILEATTRIBUTES);
            if (FileInfo == IntPtr.Zero)
              return null;

            var IconIndex = ShInfo.iIcon;

            // Get the System IImageList object from the Shell:
            var IidImageList = new Guid("46EB5926-582E-4017-9FDF-E8998DAA0950");

            if (Win32.Shell32.SHGetImageList((int)IconSize, ref IidImageList, out var ImageList) != Win32.Shell32.S_OK)
              throw new Win32Exception();

            var HIcon = IntPtr.Zero;
            var ILD_TRANSPARENT = 1;
            if (ImageList.GetIcon(IconIndex, ILD_TRANSPARENT, ref HIcon) != Win32.Shell32.S_OK)
              throw new Win32Exception();

            var Icon = System.Drawing.Icon.FromHandle(HIcon);

            if (IconSize == FileIconSize.Jumbo256x256)
            {
              Win32.User32.GetIconInfo(HIcon, out var IconInfo);

              using (var MaskBitmap = System.Drawing.Bitmap.FromHbitmap(IconInfo.hbmMask))
              {
                var MaskHeight = MaskBitmap.Height;
                var MaskWidth = MaskBitmap.Width;

                var MaxHeight = 0;
                var MaxWidth = 0;

                for (var HIndex = 0; HIndex < MaskBitmap.Height; HIndex++)
                {
                  for (var WIndex = 0; WIndex < MaskBitmap.Width; WIndex++)
                  {
                    var Pixel = MaskBitmap.GetPixel(WIndex, HIndex);

                    if (Pixel.ToArgb() != System.Drawing.Color.White.ToArgb())
                    {
                      if (HIndex > MaxHeight)
                        MaxHeight = HIndex;
                      if (WIndex > MaxWidth)
                        MaxWidth = WIndex;
                    }
                  }
                }

                if ((MaskHeight - MaxHeight) < 10 || (MaskWidth - MaxWidth) < 10)
                {
                  Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(Icon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                }
                else
                {
                  var MaxValue = Math.Max(MaxHeight, MaxWidth);
                  Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(Icon.Handle, new System.Windows.Int32Rect(0, 0, MaxValue + 1, MaxValue + 1), System.Windows.Media.Imaging.BitmapSizeOptions.FromWidthAndHeight(256, 256));
                }
                Result.Freeze();
              }
            }
            else
            {
              Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(Icon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
              Result.Freeze();
            }

            Icon.Dispose();

            Win32.Shell32.DestroyIcon(HIcon);

            Dictionary.Add(Extension, Result);
          }

          return Result;
        }
      }
    }

    private static readonly Inv.EnumArray<FileIconSize, Dictionary<string, System.Windows.Media.Imaging.BitmapSource>> Cache;
  }

  public enum FileIconSize
  {
    Small16x16 = 0x1,
    Large32x32 = 0x0,
    ExtraLarge48x48 = 0x2,
    Jumbo256x256 = 0x4
  }
}