﻿/*! 40 !*/
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Reflection.Emit;
using System.Resources;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Security.Principal;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using Microsoft.Win32;

/// <summary>
/// Namespace containing extension methods for framework classes. 
/// </summary>
namespace Inv.Support
{
  public static class Extension
  {
    /// <summary>
    /// Extract a given named resource image from this <see cref="Assembly"/>.
    /// </summary>
    /// <param name="Assembly">This <see cref="Assembly"/>.</param>
    /// <param name="ResourceName">The name of the image to extract.</param>
    /// <param name="DefaultNamespace">The default namespace used to find the resource.</param>
    /// <returns>A <see cref="BitmapImage"/> containing the named image resource.</returns>
    public static global::System.Windows.Media.Imaging.BitmapImage ExtractResourceImage(this Assembly Assembly, string ResourceName, string DefaultNamespace = "")
    {
      var ResourceLocation = Assembly.CanonicaliseResourceName(ResourceName, DefaultNamespace);

      using (var ResourceStream = Assembly.GetManifestResourceStream(ResourceLocation))
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ResourceStream != null, "Image '" + ResourceLocation + "' is not an embedded resource.");

        var Result = new global::System.Windows.Media.Imaging.BitmapImage();

        global::Inv.Support.ImageHelper.LoadFromStream(Result, ResourceStream);

        Result.Freeze();

        return Result;
      }
    }

    public static DateTime RetrieveLinkerTimestamp()
    {
      string filePath = System.Reflection.Assembly.GetCallingAssembly().Location;
      const int c_PeHeaderOffset = 60;
      const int c_LinkerTimestampOffset = 8;
      var b = new byte[2048];
      System.IO.Stream s = null;

      try
      {
        s = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        s.Read(b, 0, 2048);
      }
      finally
      {
        if (s != null)
        {
          s.Close();
        }
      }

      int i = System.BitConverter.ToInt32(b, c_PeHeaderOffset);
      int secondsSince1970 = System.BitConverter.ToInt32(b, i + c_LinkerTimestampOffset);
      DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0);
      dt = dt.AddSeconds(secondsSince1970);
      dt = dt.AddHours(TimeZone.CurrentTimeZone.GetUtcOffset(dt).Hours);
      return dt;
    }

    /// <summary>
    /// Convert every element of this <see cref="IEnumerable{T}"/> using the specified <see cref="Converter{T,TOutput}"/>.
    /// </summary>
    /// <typeparam name="T">The type of elements in this <see cref="IEnumerable{T}"/>.</typeparam>
    /// <typeparam name="TOutput">The desired output type of the conversion.</typeparam>
    /// <param name="Source">This <see cref="IEnumerable{T}"/>.</param>
    /// <param name="Converter">A <see cref="Converter{T,TOutput}"/> to convert elements.</param>
    /// <returns>An <see cref="IEnumerable{TOutput}"/> of converted elements.</returns>
    public static IEnumerable<TOutput> ConvertAll<T, TOutput>(this IEnumerable<T> Source, Converter<T, TOutput> Converter)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Converter != null, "Converter must be specified.");

      return Source.Select(Item => Converter(Item));
    }

    public static string RemoveDiacritics(this string Source)
    {
      if (Source != null && Source.Length > 0)
      {
        var CharArray = new char[Source.Length];
        var CharIndex = 0;

        Source = Source.Normalize(NormalizationForm.FormD);
        for (int SourceIndex = 0; SourceIndex < Source.Length; SourceIndex++)
        {
          var Character = Source[SourceIndex];
          if (CharUnicodeInfo.GetUnicodeCategory(Character) != UnicodeCategory.NonSpacingMark)
            CharArray[CharIndex++] = Character;
        }

        return new string(CharArray, 0, CharIndex).Normalize(NormalizationForm.FormC);
      }

      return Source;
    }
  }

  public static class StreamHelper
  {
    public static bool EqualTo(this Stream LeftStream, Stream RightStream, int BufferSize)
    {
      Debug.Assert(LeftStream.Position == 0);
      Debug.Assert(RightStream.Position == 0);

      var LeftLength = LeftStream.Length;
      var RightLength = RightStream.Length;

      if (LeftLength != RightLength)
        return false;

      var LeftBuffer = new byte[BufferSize];
      var RightBuffer = new byte[BufferSize];

      var RemainderLength = LeftLength;

      while (RemainderLength > 0)
      {
        var ReadLength = LeftBuffer.Length;

        if (ReadLength > RemainderLength)
          ReadLength = (int)RemainderLength;

        RemainderLength -= ReadLength;

        LeftStream.Read(LeftBuffer, 0, ReadLength);
        RightStream.Read(RightBuffer, 0, ReadLength);

        for (var BufferIndex = 0; BufferIndex < ReadLength; BufferIndex++)
        {
          if (LeftBuffer[BufferIndex] != RightBuffer[BufferIndex])
            return false;
        }
      }

      return true;
    }

    public static bool FileEqualTo(string LeftFilePath, string RightFilePath)
    {
      using (var LeftStream = new FileStream(LeftFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      using (var RightStream = new FileStream(RightFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      {
        return LeftStream.EqualTo(RightStream, 65536);
      }
    }
  }

  public static class DebugHelper
  {
    public static bool IsDebugging()
    {
#if DEBUG
      return true;
#else
      return false;
#endif
    }
  }

  /// <summary>
  /// Static class with helper functions for converting values.
  /// </summary>
  public static class ConverterHelper
  {
    /// <summary>
    /// Convert a long value to a base 26 string.
    /// </summary>
    /// <param name="Value">The value to convert.</param>
    /// <returns>A string representing the base 26 conversion of <paramref name="Value"/>.</returns>
    public static string ToBase26(long Value)
    {
      const long Base26 = 26L;
      var Dividend = Value;
      var Result = "";

      while (Dividend > 0)
      {
        var Modulo = (Dividend - 1) % Base26;
        Result = Convert.ToChar(65 + Modulo).ToString() + Result;
        Dividend = (Dividend - Modulo) / Base26;
      }

      return Result;
    }
    /// <summary>
    /// Convert a base 26 string to a long value.
    /// </summary>
    /// <param name="Value">The base 26 string to convert.</param>
    /// <returns><paramref name="Value"/> converted from base 26.</returns>
    public static long FromBase26(string Value)
    {
      var Result = 0;
      const int Base26 = 26;

      foreach (var Char in Value.ToUpper().ToCharArray())
      {
        if (Char < 'A' || Char > 'Z')
          throw new Exception("Not a valid Base26 string: " + Value);

        Result = (Result * Base26) + (Char - '@');
      }

      return Result;
    }
  }

  public static class GuidHelper
  {
    public static bool IsCOMRegistered(this Guid ClassGUID)
    {
      var Result = Win32.Ole32.ProgIDFromCLSID(ref ClassGUID, out var ProgID);
      return Result == 0;
    }
  }

  public struct HSLColour
  {
    internal HSLColour(byte AA, float HH, float SS, float LL)
      : this()
    {
      A = AA;
      H = HH;
      S = SS;
      L = LL;
    }

    public byte A { get; private set; }
    public float H { get; private set; }
    public float S { get; private set; }
    public float L { get; private set; }
  }

  public static class ImageHelper
  {
    public static void LoadFromStream(this BitmapImage BitmapImage, Stream Stream, bool IgnoreColorProfile = false, int? Width = null, int? Height = null)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      BitmapImage.BeginInit();

      if (IgnoreColorProfile)
        BitmapImage.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;

      BitmapImage.CacheOption = BitmapCacheOption.OnLoad;

      if (Width != null)
        BitmapImage.DecodePixelWidth = Width.Value;

      if (Height != null)
        BitmapImage.DecodePixelHeight = Height.Value;

      BitmapImage.StreamSource = Stream;
      BitmapImage.EndInit();
    }
    public static void LoadFromStream(this BitmapImage BitmapImage, Stream Stream, int Width, int Height)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var SourceWidth = 0;
      var SourceHeight = 0;

      if (Stream.CanSeek)
      {
        // Create a temporary bitmap to determine the original aspect ratio of the image.
        var SourceBitmap = new BitmapImage();
        SourceBitmap.BeginInit();
        SourceBitmap.StreamSource = Stream;
        SourceBitmap.EndInit();

        SourceWidth = SourceBitmap.PixelWidth;
        SourceHeight = SourceBitmap.PixelHeight;

        Stream.Seek(0, SeekOrigin.Begin);
      }

      BitmapImage.BeginInit();
      BitmapImage.CacheOption = BitmapCacheOption.OnLoad;
      BitmapImage.StreamSource = Stream;

      if (Stream.CanSeek)
      {
        if (SourceWidth / (double)SourceHeight > Width / (double)Height)
          BitmapImage.DecodePixelHeight = Height;
        else
          BitmapImage.DecodePixelWidth = Width;
      }
      else
      {
        BitmapImage.DecodePixelWidth = Width;
        BitmapImage.DecodePixelHeight = Height;
      }

      BitmapImage.EndInit();
    }
    public static void SaveToStreamAsBmp(this BitmapSource BitmapSource, Stream Stream)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new BmpBitmapEncoder();
      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsBmp(this ImageSource ImageSource, Stream Stream)
    {
      Debug.Assert(ImageSource is BitmapSource ||
                   ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsBmp((BitmapSource)ImageSource, Stream);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsBmp(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream);
    }
    public static BitmapSource AsBitmapSource(this ImageSource ImageSource)
    {
      Debug.Assert(ImageSource is BitmapSource ||
                  ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        return (BitmapSource)ImageSource;
      else if (ImageSource is DrawingImage)
        return ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource);

      return null;
    }
    public static void SaveToStreamAsPng(this BitmapSource BitmapSource, Stream Stream)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new PngBitmapEncoder();
      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsPng(this ImageSource ImageSource, Stream Stream)
    {
      Debug.Assert(ImageSource is BitmapSource || ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsPng((BitmapSource)ImageSource, Stream);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsPng(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream);

    }
    public static void SaveToStreamAsJpg(this BitmapSource BitmapSource, Stream Stream, int QualityLevel)
    {
      Debug.Assert(Stream != null, "Stream must not be null.");

      var Encoder = new JpegBitmapEncoder();
      if (QualityLevel > 0)
        Encoder.QualityLevel = QualityLevel;

      Encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(BitmapSource));
      Encoder.Save(Stream);
    }
    public static void SaveToStreamAsJpg(this ImageSource ImageSource, Stream Stream, int QualityLevel)
    {
      Debug.Assert(ImageSource is BitmapSource ||
             ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        SaveToStreamAsJpg((BitmapSource)ImageSource, Stream, QualityLevel);
      else if (ImageSource is DrawingImage)
        SaveToStreamAsJpg(ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource), Stream, QualityLevel);
    }
    public static BitmapImage ConvertToMediaBitmap(this Inv.Image InvImage, bool IgnoreColorProfile = false, int? Width = null, int? Height = null)
    {
      // Note: After initialization (BeginInit and EndInit), property changes are ignored.

      using (var MemoryStream = new MemoryStream(InvImage.GetBuffer()))
      {
        var BitmapImage = new BitmapImage();

        BitmapImage.LoadFromStream(MemoryStream, IgnoreColorProfile, Width, Height);

        return BitmapImage;
      }
    }
    public static BitmapImage ConvertToMediaBitmapAndFreeze(this Inv.Image InvImage)
    {
      // Note: After initialization (BeginInit and EndInit), property changes are ignored.

      var Result = InvImage.Node as BitmapImage;

      if (Result == null)
      {
        using (var MemoryStream = new MemoryStream(InvImage.GetBuffer()))
        {
          Result = new BitmapImage();
          Result.LoadFromStream(MemoryStream);
          Result.Freeze();
        }

        InvImage.Node = Result;
      }

      return Result;
    }
    public static BitmapImage ConvertToMediaBitmapAndFreeze(this Inv.Image InvImage, int Width, int Height)
    {
      // Note: After initialization (BeginInit and EndInit), property changes are ignored.

      using (var MemoryStream = new MemoryStream(InvImage.GetBuffer()))
      {
        var BitmapImage = new BitmapImage();

        BitmapImage.LoadFromStream(MemoryStream, Width, Height);
        BitmapImage.Freeze();

        return BitmapImage;
      }

    }
    public static BitmapSource Rotate(this BitmapSource Source, int Angle)
    {
      var Result = new TransformedBitmap(Source, new RotateTransform(Angle)).AsBitmapSource();
      Result.Freeze();
      return Result;
    }
    public static BitmapImage Rotate(this Inv.Image InvImage, int Angle)
    {
      var TransformedBitmapSource = new TransformedBitmap(InvImage.ConvertToMediaBitmap(), new RotateTransform(Angle));
      var Result = new BitmapImage();
      using (var Stream = new MemoryStream())
      {
        TransformedBitmapSource.SaveToStreamAsBmp(Stream);
        Stream.Position = 0;

        Result.BeginInit();
        Result.CacheOption = BitmapCacheOption.OnLoad;
        Result.StreamSource = Stream;
        Result.EndInit();
        Result.Freeze();
      }

      return Result;
    }
    public static Inv.Image ConvertToInvImagePng(this ImageSource ImageSource)
    {
      if (ImageSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new MemoryStream())
        {
          ImageSource.SaveToStreamAsPng(MemoryStream);

          var Result = new Inv.Image(MemoryStream.ToArray(), ".png");

          if (ImageSource is BitmapImage)
            Result.Node = ImageSource;

          return Result;
        }
      }
    }
    public static Inv.Image ConvertToInvImageJpeg(this ImageSource ImageSource, int Quality = 100)
    {
      if (ImageSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new MemoryStream())
        {
          ImageSource.SaveToStreamAsJpg(MemoryStream, Quality);

          return new Inv.Image(MemoryStream.ToArray(), ".jpg");
        }
      }
    }
    public static bool StreamEquals(this BitmapSource LeftBitmapSource, BitmapSource RightBitmapSource)
    {
      using (var LeftMemoryStream = new MemoryStream())
      {
        using (var RightMemoryStream = new MemoryStream())
        {
          var LeftEncoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
          LeftEncoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(LeftBitmapSource));
          LeftEncoder.Save(LeftMemoryStream);

          var RightEncoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
          RightEncoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(RightBitmapSource));
          RightEncoder.Save(RightMemoryStream);

          return LeftMemoryStream.GetBuffer().CompareTo(RightMemoryStream.GetBuffer()) == 0;
        }
      }
    }
    public static BitmapSource AsTint(this BitmapSource BitmapSource, Brush TintBrush)
    {
      var DrawingVisual = new System.Windows.Media.DrawingVisual();

      using (var WpfDrawingContext = DrawingVisual.RenderOpen())
      {
        var WpfImageRect = new System.Windows.Rect(0, 0, BitmapSource.Width, BitmapSource.Height);
        WpfDrawingContext.DrawImage(BitmapSource, WpfImageRect);

        var WpfImageBrush = new System.Windows.Media.ImageBrush(BitmapSource);
        WpfImageBrush.Freeze();

        WpfDrawingContext.PushOpacityMask(WpfImageBrush);
        WpfDrawingContext.DrawRectangle(TintBrush, null, WpfImageRect);
        WpfDrawingContext.Pop();
      }

      var WpfBitmap = new RenderTargetBitmap(BitmapSource.PixelWidth, BitmapSource.PixelHeight, BitmapSource.DpiX, BitmapSource.DpiY, System.Windows.Media.PixelFormats.Default);
      WpfBitmap.Render(DrawingVisual);
      WpfBitmap.Freeze();

      return WpfBitmap;
    }
    public static Inv.Image AsGrayscale(this Inv.Image Image)
    {
      return Image.ConvertToMediaBitmapAndFreeze().AsGrayscale().ConvertToInvImagePng();
    }
    public static BitmapSource AsGrayscale(this BitmapSource BitmapSource)
    {
      var Result = UnsafeGrayscaleConversion(BitmapSource);
      Result.Freeze();
      return Result;
    }
    public static BitmapSource Scale(this BitmapSource BitmapSource, double Scale)
    {
      return BitmapSource.Scale(Scale, Scale);
    }
    public static BitmapSource Scale(this BitmapSource BitmapSource, double Width, double Height)
    {
      var TransformedBitmapSource = new TransformedBitmap(BitmapSource, new ScaleTransform(Width, Height));

      using (var Stream = new MemoryStream())
      {
        TransformedBitmapSource.SaveToStreamAsBmp(Stream);
        Stream.Position = 0;

        var Result = new BitmapImage();
        Result.BeginInit();
        Result.CacheOption = BitmapCacheOption.OnLoad;
        Result.StreamSource = Stream;
        Result.EndInit();
        Result.Freeze();
        return Result;
      }
    }
    public static BitmapSource Resize(this ImageSource Image, int ResizeWidth, int ResizeHeight)
    {
      var Rect = new System.Windows.Rect(0, 0, ResizeWidth, ResizeHeight);

      var Group = new DrawingGroup();
      RenderOptions.SetBitmapScalingMode(Group, BitmapScalingMode.Fant);
      Group.Children.Add(new ImageDrawing(Image, Rect));

      var DrawingVisual = new DrawingVisual();
      using (var drawingContext = DrawingVisual.RenderOpen())
        drawingContext.DrawDrawing(Group);

      var ResizedImage = new RenderTargetBitmap(
          ResizeWidth, ResizeHeight,         // Resized dimensions
          96, 96,                // Default DPI values
          PixelFormats.Default); // Default pixel format
      ResizedImage.Render(DrawingVisual);
      ResizedImage.Freeze();

      return ResizedImage;
    }

    internal static BitmapSource ConvertDrawingImageToBitmapSource(DrawingImage image)
    {
      var wrapperImage = new System.Windows.Controls.Image();
      wrapperImage.Source = image;
      wrapperImage.Measure(new System.Windows.Size(image.Width, image.Height));
      wrapperImage.Arrange(new System.Windows.Rect(0, 0, image.Width, image.Height));

      var render = new RenderTargetBitmap((int)(image.Width * 96), (int)(image.Height * 96), 96, 96, PixelFormats.Default);
      render.Render(wrapperImage);
      return render;
    }

    private static unsafe BitmapSource UnsafeGrayscaleConversion(BitmapSource Source)
    {
      try
      {
        var Result = new WriteableBitmap(Source);
        Result.Lock();
        try
        {
          var buff = Result.BackBuffer;
          var stride = Result.BackBufferStride;

          if (Result.Format == PixelFormats.Bgra32)
          {
            var nWidth = Result.PixelWidth;
            var nHeight = Result.PixelHeight;

            for (var y = 0; y < nHeight; y++)
            {
              var p = (byte*)buff;

              for (var x = 0; x < nWidth; x++)
              {
                var i = y * stride + x * 4;
                var Avg = (byte)((p[i] + p[i + 1] + p[i + 2]) / 3);

                p[i] = Avg;
                p[i + 1] = Avg;
                p[i + 2] = Avg;
                //p[i + 3] = unchanged alpha.
              }
            }

            return Result;
          }
          else
          {
            Debug.WriteLine("PixelFormat not supported for correct grayscale conversion: " + Result.Format);
          }
        }
        finally
        {
          Result.Unlock();
        }
      }
      catch (Exception Exception)
      {
        // failed to convert to grayscale.
        Debug.WriteLine(Exception.Describe());
      }

      // couldn't use the pixel manipulation (stride was not 4 or there was an exception).
      var ConvertedBitmap = new FormatConvertedBitmap();
      ConvertedBitmap.BeginInit();
      ConvertedBitmap.Source = Source;
      ConvertedBitmap.DestinationFormat = PixelFormats.Gray32Float;
      ConvertedBitmap.AlphaThreshold = 100;
      ConvertedBitmap.EndInit();

      // Create Opacity Mask for greyscale image as FormatConvertedBitmap does not keep transparency info
      return ConvertedBitmap;
    }
  }

  public static class DrawingBitmapHelper
  {
    /// <summary>
    /// Convert this <see cref="BitmapSource"/> to a <see cref="System.Drawing.Bitmap"/>.
    /// </summary>
    /// <param name="BitmapSource">This <see cref="BitmapSource"/>.</param>
    /// <returns>A <see cref="System.Drawing.Bitmap"/> with the image contents of this <see cref="BitmapSource"/>.</returns>
    public static System.Drawing.Bitmap ConvertToDrawingBitmap(this BitmapSource BitmapSource)
    {
      if (BitmapSource == null)
      {
        return null;
      }
      else
      {
        using (var MemoryStream = new MemoryStream())
        {
          var ImageEncoder = new PngBitmapEncoder();
          ImageEncoder.Frames.Add(BitmapFrame.Create(BitmapSource));
          ImageEncoder.Save(MemoryStream);

          return new System.Drawing.Bitmap(MemoryStream);
        }
      }
    }
    public static System.Drawing.Bitmap ConvertToDrawingBitmap(this ImageSource ImageSource)
    {
      Debug.Assert(ImageSource is BitmapSource || ImageSource is DrawingImage, "ImageSource is of unknown type (possibly D3DImage?)");

      if (ImageSource is BitmapSource)
        return ConvertToDrawingBitmap((BitmapSource)ImageSource);
      else if (ImageSource is DrawingImage)
        return ConvertToDrawingBitmap(ImageHelper.ConvertDrawingImageToBitmapSource((DrawingImage)ImageSource));

      return null;
    }
    public static System.Drawing.Bitmap DrawingBitmapAsGrayscale(this System.Drawing.Bitmap original)
    {
      //create a blank bitmap the same size as original
      var newBitmap = new System.Drawing.Bitmap(original.Width, original.Height);

      //get a graphics object from the new image
      var g = System.Drawing.Graphics.FromImage(newBitmap);

      //create the grayscale ColorMatrix
      var colorMatrix = new System.Drawing.Imaging.ColorMatrix(new float[][] 
      {
        new float[] { .3f, .3f, .3f, 0, 0 },
        new float[] { .59f, .59f, .59f, 0, 0 },
        new float[] { .11f, .11f, .11f, 0, 0 },
        new float[] { 0, 0, 0, 1, 0 },
        new float[] { 0, 0, 0, 0, 1 }
      });

      //create some image attributes
      var attributes = new System.Drawing.Imaging.ImageAttributes();

      //set the color matrix attribute
      attributes.SetColorMatrix(colorMatrix);

      //draw the original image on the new image
      //using the grayscale color matrix
      g.DrawImage(original, new System.Drawing.Rectangle(0, 0, original.Width, original.Height),
         0, 0, original.Width, original.Height, System.Drawing.GraphicsUnit.Pixel, attributes);

      //dispose the Graphics object
      g.Dispose();
      return newBitmap;
    }
  }

  public static class ResourceManagerHelper
  {
    public static byte[] ExtractResourceBinary(this System.Resources.ResourceManager ResourceManager, string ResourceName)
    {
      using (var ResourceStream = ResourceManager.GetStream(ResourceName))
      {
        Debug.Assert(ResourceStream != null, "Buffer '" + ResourceName + "' is not a compiled resource.");

        var Result = new byte[ResourceStream.Length];

        ResourceStream.Read(Result, 0, Result.Length);

        return Result;
      }
    }
    public static string ExtractResourceString(this System.Resources.ResourceManager ResourceManager, string ResourceName)
    {
      using (var ResourceStream = ResourceManager.GetStream(ResourceName))
      {
        Debug.Assert(ResourceStream != null, "String '" + ResourceName + "' is not a compiled resource.");

        using (var StreamReader = new global::System.IO.StreamReader(ResourceStream))
        {
          return StreamReader.ReadToEnd();
        }
      }
    }
    public static global::System.Windows.Media.Imaging.BitmapImage ExtractResourceImage(this ResourceManager ResourceManager, string ResourceName)
    {
      using (var ResourceStream = ResourceManager.GetStream(ResourceName))
      {
        Debug.Assert(ResourceStream != null, "Image '" + ResourceName + "' is not a compiled resource.");

        var Result = new global::System.Windows.Media.Imaging.BitmapImage();

        if (ResourceStream != null)
          global::Inv.Support.ImageHelper.LoadFromStream(Result, ResourceStream);

        Result.Freeze();

        return Result;
      }
    }
  }

  public static class TypeHelper
  {
    private class SimpleTypeEqualityComparer : IEqualityComparer<Type>
    {
      public bool Equals(Type x, Type y)
      {
        return x.Assembly == y.Assembly &&
            x.Namespace == y.Namespace &&
            x.Name == y.Name;
      }
      public int GetHashCode(Type obj)
      {
        throw new NotImplementedException();
      }
    }

    /// <summary>
    /// Searches for the specified public generic method whose parameters match the specified argument types.
    /// </summary>
    /// <param name="type">This Type.</param>
    /// <param name="name">The string containing the name of the public generic method to get.</param>
    /// <param name="parameterTypes">An array of Type objects representing the number, order, and type of the parameters for the method to get.
    /// -or- 
    /// An empty array of Type objects (as provided by the EmptyTypes field) to get a method that takes no parameters. </param>
    /// <returns>An object representing the public method whose parameters match the specified argument types, if found; otherwise, null.</returns>
    public static MethodInfo GetGenericMethod(this Type type, string name, Type[] parameterTypes)
    {
      var methods = type.GetMethods();
      foreach (var method in methods.Where(m => m.Name == name))
      {
        var methodParameterTypes = method.GetParameters().Select(p => p.ParameterType).ToArray();

        if (methodParameterTypes.SequenceEqual(parameterTypes, new SimpleTypeEqualityComparer()))
        {
          return method;
        }
      }

      return null;
    }
  }

  public static class DoubleHelper
  {
    public static double RoundToSignificantDigits(this double Value, int Digits)
    {
      if (Value == 0.0)
        return Value;

      var Scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(Value))) + 1);
      return Scale * Math.Round(Value / Scale, Digits);
    }
    public static double TruncateToSignificantDigits(this double Value, int Digits)
    {
      if (Value == 0)
        return 0;

      var Scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(Value))) + 1 - Digits);
      return Scale * (Value / Scale).Truncate();
    }
  }

  public static class WebHelper
  {
    public static string ShortenUrl(string Source)
    {
      if (Source == null)
        return "";

      if (!Source.StartsWith("http://") && !Source.StartsWith("ftp://"))
        Source = "http://" + Source;

      var client = new System.Net.WebClient();
      var TinyUrl = client.DownloadString("http://tinyurl.com/api-create.php?url=" + Source);

      return TinyUrl;
    }
    public static string UrlEncode(string Source)
    {
      return Source.Replace('/', '_').Replace('+', '-');
    }
    public static string UrlDecode(string Source)
    {
      return Source.Replace('_', '/').Replace('-', '+');
    }
  }

  public static class JsonHelper
  {
    /// <summary>
    /// Parse a stream containing JSON into an object of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Stream"></param>
    /// <returns></returns>
    public static T LoadFromStream<T>(Stream Stream)
    {
      var Serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
      var Result = (T)Serializer.ReadObject(Stream);

      return Result;
    }
    /// <summary>
    /// Parse a file containing JSON into an object of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="FilePath"></param>
    /// <returns></returns>
    public static T LoadFromFile<T>(string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open))
      {
        var Serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
        var Result = (T)Serializer.ReadObject(FileStream);

        return Result;
      }
    }
    /// <summary>
    /// Save the JSON representation of an object of type <typeparamref name="T"/> to a file.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Source"></param>
    /// <param name="FilePath"></param>
    public static void SaveToFile<T>(T Source, string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.OpenOrCreate))
      {
        new DataContractJsonSerializer(typeof(T)).WriteObject(FileStream, Source);
        FileStream.Flush();
      }
    }
    /// <summary>
    /// Produce the JSON representation of an object of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Source"></param>
    /// <returns></returns>
    public static string ToJson<T>(T Source)
    {
      using (var MemoryStream = new MemoryStream())
      {
        new DataContractJsonSerializer(typeof(T)).WriteObject(MemoryStream, Source);
        return Encoding.UTF8.GetString(MemoryStream.GetBuffer());
      }
    }
    /// <summary>
    /// Parse a JSON string into an object of type <typeparamref name="T"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Json"></param>
    /// <returns></returns>
    public static T FromJson<T>(string Json)
    {
      using (var MemoryStream = new MemoryStream(Encoding.UTF8.GetBytes(Json)))
      {
        return LoadFromStream<T>(MemoryStream);
      }
    }
  }

  public static class ShellHelper
  {
    public static string GetFileTypeDescription(string Extension)
    {
      var ShInfo = new Win32.Shell32.SHFILEINFO();

      var FileInfo = Win32.Shell32.SHGetFileInfo(Extension, Win32.Shell32.FILE_ATTRIBUTE.NORMAL, ref ShInfo, Marshal.SizeOf(ShInfo), Win32.Shell32.SHGFI.TYPENAME | Win32.Shell32.SHGFI.USEFILEATTRIBUTES);
      if (FileInfo == IntPtr.Zero)
        return null;
      else
        return ShInfo.szTypeName.Trim();
    }
  }

  public static class RegistryHelper
  {
    public static ContentType AccessFileContentType(string Extension)
    {
      Debug.Assert(!string.IsNullOrWhiteSpace(Extension), "Extension must not be null or blank.");

      using (var ContentRegistryKey = Registry.ClassesRoot.OpenSubKey(Extension))
      {
        if (ContentRegistryKey != null)
        {
          var DefaultValue = ContentRegistryKey.GetValue("");
          if (DefaultValue != null)
          {
            var DefaultString = DefaultValue.ToString();
            if (DefaultString.Equals("txtfile", StringComparison.CurrentCultureIgnoreCase) || DefaultString.Equals("VBSFile", StringComparison.CurrentCultureIgnoreCase) || DefaultString.StartsWith("Microsoft.PowerShellScript", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Text;
            else if (DefaultString.StartsWith("Word", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Word;
            else if (DefaultString.StartsWith("Excel", StringComparison.CurrentCultureIgnoreCase))
              return ContentType.Excel;
          }

          var ContentTypeValue = (ContentRegistryKey.GetValue("Content Type") ?? "").ToString();

          if (!string.IsNullOrWhiteSpace(ContentTypeValue))
          {
            if (ContentTypeValue.ToLower().StartsWith("text/") || ContentTypeValue.ToLower().EndsWith("/xml") || ContentTypeValue.ToLower().EndsWith("/javascript"))
              return ContentType.Text;
            else if (ContentTypeValue.ToLower().StartsWith("video/") || ContentTypeValue.ToLower().StartsWith("audio/"))
              return ContentType.Media;
            else if (ContentTypeValue.ToLower().StartsWith("image/"))
              return ContentType.Image;
          }

          var PerceivedType = (ContentRegistryKey.GetValue("PerceivedType") ?? "").ToString();

          if (PerceivedType.Equals("text", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Text;
          else if (PerceivedType.Equals("video", StringComparison.CurrentCultureIgnoreCase) || PerceivedType.Equals("audio", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Media;
          else if (PerceivedType.Equals("image", StringComparison.CurrentCultureIgnoreCase))
            return ContentType.Image;
        }

        return ContentType.Other;
      }
    }

    public enum ContentType { Text, Media, Image, Word, Excel, Other }
  }

  public static class FileHelper
  {
    public static void SetReadOnly(string FilePath, bool Value)
    {
      var Attributes = File.GetAttributes(FilePath);

      if (!Value && (Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
        File.SetAttributes(FilePath, Attributes & ~FileAttributes.ReadOnly);
      else if (Value && (Attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly)
        File.SetAttributes(FilePath, Attributes | FileAttributes.ReadOnly);
    }

    [System.Diagnostics.DebuggerNonUserCode]
    public static bool FileInUse(string FilePath)
    {
      Debug.Assert(System.IO.File.Exists(FilePath), "File does not exist.");

      var Result = false;
      try
      {
        using (var FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
          FileStream.Close();
      }
      catch (IOException)
      {
        Result = true;
      }

      return Result;
    }
  }

  public static class NameValueCollectionHelper
  {
    public static T GetValue<T>(this System.Collections.Specialized.NameValueCollection NameValueCollection, string Key, T Default = default(T)) where T : IConvertible
    {
      T Result;
      var Value = NameValueCollection[Key];

      if (Value == null)
        Result = Default;
      else
      {
        try
        {
          Result = (T)Convert.ChangeType(Value, typeof(T));
        }
        catch
        {
          Result = Default;
        }
      }

      return Result;
    }
    public static bool TryGetValue<T>(this System.Collections.Specialized.NameValueCollection NameValueCollection, string Key, out T Result) where T : IConvertible
    {
      Result = default(T);
      var Value = NameValueCollection[Key];

      if (Value == null)
        return false;
      else
      {
        try
        {
          Result = (T)Convert.ChangeType(Value, typeof(T));
          return true;
        }
        catch
        {
          return false;
        }
      }
    }
  }

  public static class DirectoryInfoHelper
  {
    public static Inv.DataSize Size(this DirectoryInfo Source, System.IO.SearchOption SearchOption = SearchOption.AllDirectories)
    {
      long FolderSize = 0;
      try
      {
        if (Source.Exists)
        {
          try
          {
            foreach (var File in Source.EnumerateFiles("*", SearchOption))
            {
              if (File.Exists)
                FolderSize += File.Length;
            }
          }
          catch (NotSupportedException e)
          {
            Debug.WriteLine("Unable to calculate folder size: {0}", e.Message);
          }
        }
      }
      catch (UnauthorizedAccessException e)
      {
        Debug.WriteLine("Unable to calculate folder size: {0}", e.Message);
      }

      return Inv.DataSize.FromBytes(FolderSize);
    }
  }

  public static class FileInfoHelper
  {
    public static byte[] ReadAllBytes(this FileInfo Source)
    {
      byte[] Result;

      using (var FileStream = new FileStream(Source.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 65536))
      {
        var FileLength = FileStream.Length;

        if (FileLength > Int32.MaxValue)
          throw new IOException("File too long");

        var Count = (int)FileLength;
        Result = new byte[Count];

        var Index = 0;
        while (Count > 0)
        {
          var Size = FileStream.Read(Result, Index, Count);

          if (Size == 0)
            throw new InvalidOperationException("End of file reached before expected");

          Index += Size;
          Count -= Size;
        }
      }

      return Result;
    }
    public static Inv.Dimension ReadImageDimension(this FileInfo Source)
    {
      using (var FileStream = new FileStream(Source.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, 65536))
      {
        var Result = BitmapFrame.Create(FileStream, BitmapCreateOptions.DelayCreation, BitmapCacheOption.None);

        return new Inv.Dimension(Result.PixelWidth, Result.PixelHeight);
      }
    }
    public static TimeSpan ReadSoundLength(this FileInfo Source)
    {
      var lengthBuf = new StringBuilder(32);

      var Res = Win32.WinMM.mciSendString(string.Format("status \"{0}\" length", Source.FullName), lengthBuf, lengthBuf.Capacity, IntPtr.Zero);
      //Debug.Assert(Res == 0, Source.FullName + " sound length not found [" + Res + "].");

      if (!int.TryParse(lengthBuf.ToString(), out int length))
        length = 0;

      return TimeSpan.FromMilliseconds(length);
    }
    public static Inv.DataSize Size(this FileInfo Source)
    {
      return Inv.DataSize.FromBytes(Source.Length);
    }
  }

  /// <summary>
  /// A static class with extension methods related to paths.
  /// </summary>
  public static class PathHelper
  {
    /// <summary>
    /// Get the path to the x86 Program Files directory.
    /// </summary>
    /// <returns>The path to the x86 Program Files directory.</returns>
    public static string ProgramFilesx86()
    {
      var ProgramFilePath = new System.Text.StringBuilder(255);

      if (Win32.Shell32.SHGetSpecialFolderPath(IntPtr.Zero, ProgramFilePath, Win32.Shell32.CSIDL_PROGRAM_FILESX86, 0))
        return ProgramFilePath.ToString();
      else
        return null;
    }

    /// <summary>
    /// Get the relative path between two absolute paths. The two paths must share a common prefix and refer to files or directories which
    /// actually exist at the time of calling GetRelativePath.
    /// </summary>
    /// <param name="fromPath">The path to use as the source of the relative path.</param>
    /// <param name="toPath">The destination path.</param>
    /// <returns>A string expressing the relative path required to get to toPath from fromPath.</returns>
    public static string GetRelativePath(string fromPath, string toPath)
    {
      var fromAttr = GetPathAttribute(fromPath);
      var toAttr = GetPathAttribute(toPath);

      var path = new StringBuilder(260); // MAX_PATH
      if (PathRelativePathTo(
          path,
          fromPath,
          fromAttr,
          toPath,
          toAttr) == 0)
      {
        throw new ArgumentException("Paths must have a common prefix");
      }
      return path.ToString();
    }

    public static string GetApplicationPath()
    {
      return System.AppDomain.CurrentDomain.BaseDirectory;
    }

    [DebuggerNonUserCode]
    private static int GetPathAttribute(string path)
    {
      var di = new DirectoryInfo(path);
      if (di.Exists)
      {
        return FILE_ATTRIBUTE_DIRECTORY;
      }

      var fi = new FileInfo(path);
      if (fi.Exists)
      {
        return FILE_ATTRIBUTE_NORMAL;
      }

      throw new FileNotFoundException();
    }

    private const int FILE_ATTRIBUTE_DIRECTORY = 0x10;
    private const int FILE_ATTRIBUTE_NORMAL = 0x80;

    [DllImport("shlwapi.dll", SetLastError = true)]
    private static extern int PathRelativePathTo(StringBuilder pszPath,
        string pszFrom, int dwAttrFrom, string pszTo, int dwAttrTo);
  }

  public static class GraphicsHelper
  {
    /// <summary>
    /// Calculate the intersection between an ellipse at the origin and a line segment starting at the origin at the specified angle from the X axis.
    /// </summary>
    /// <param name="EllipseRadiusX">The X radius of the ellipse.</param>
    /// <param name="EllipseRadiusY">The Y radius of the ellipse.</param>
    /// <param name="LineAngleFromXAxis">The angle of the line segment from the X axis, expressed in degrees.</param>
    /// <param name="SignMultiplier">The sign multiplier to apply to the intersection point, thus mirroring the intersection around the origin.</param>
    /// <returns>The intersection point.</returns>
    public static System.Windows.Point CalculateOriginedLineAndEllipseIntersectionPoint(double EllipseRadiusX, double EllipseRadiusY, double LineAngleFromXAxis, int SignMultiplier)
    {
      // Assumes ellipse's center is at origin (0, 0), and that line passes through both origin and point (x0, y0).

      System.Diagnostics.Debug.Assert(((SignMultiplier == -1) || (SignMultiplier == 1)), "SignMultiplier must be 1 or -1");

      var AngleRadians = (LineAngleFromXAxis * (Math.PI / 180));

      var X0 = 1;
      var Y0 = Math.Tan(AngleRadians);

      var IntersectionPoint = new System.Windows.Point()
      {
        X = (SignMultiplier * (((EllipseRadiusX * EllipseRadiusY) / Math.Sqrt(((Math.Pow(EllipseRadiusX, 2) * Math.Pow(Y0, 2)) + (Math.Pow(EllipseRadiusY, 2) * Math.Pow(X0, 2))))) * X0)),
        Y = (-(SignMultiplier * (((EllipseRadiusX * EllipseRadiusY) / Math.Sqrt(((Math.Pow(EllipseRadiusX, 2) * Math.Pow(Y0, 2)) + (Math.Pow(EllipseRadiusY, 2) * Math.Pow(X0, 2))))) * Y0)))
      };

      return (IntersectionPoint);
    }
    /// <summary>
    /// Calculate a transform matrix to reflect coordinates along the given line.
    /// </summary>
    /// <param name="LineGradient">The gradient of the reflection line.</param>
    /// <param name="LineYIntersect">The Y intersect of the reflection line.</param>
    /// <returns>A matrix that will transform coordinates by reflecting them along the given line.</returns>
    public static Matrix CalculateMatrixForReflectionAboutLine(double LineGradient, double LineYIntersect)
    {
      double Theta;

      if (LineGradient < 0)
        Theta = Math.Atan((Math.Abs(LineGradient) + (Math.PI / 2)));
      else
        Theta = Math.Atan(LineGradient);

      var MatrixForReflection = new Matrix();

      MatrixForReflection.M11 = Math.Cos(2 * Theta);
      MatrixForReflection.M12 = Math.Sin(2 * Theta);
      MatrixForReflection.M21 = Math.Sin(2 * Theta);
      MatrixForReflection.M22 = (-Math.Cos(2 * Theta));
      MatrixForReflection.OffsetX = 0;
      MatrixForReflection.OffsetY = LineYIntersect;

      return (MatrixForReflection);
    }
  }

  public static class SocketHelper
  {
    [System.Diagnostics.DebuggerNonUserCode]
    public static void NonBlockingConnect(this System.Net.Sockets.Socket Socket, string Address, int Port)
    {
      Debug.Assert(!Socket.Blocking, "Socket must be in non blocking mode.");
      try
      {
        Socket.Connect(Address, Port);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        if (Exception.ErrorCode != 10035 /* WSAEWOULDBLOCK */ )
          throw Exception.Preserve();
      }
    }
    [System.Diagnostics.DebuggerNonUserCode]
    public static void NonBlockingSend(this System.Net.Sockets.Socket Socket, byte[] Buffer, int Size)
    {
      Debug.Assert(!Socket.Blocking, "Socket must be in non blocking mode.");
      try
      {
        Socket.Send(Buffer, Size, System.Net.Sockets.SocketFlags.None);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        if (Exception.ErrorCode != 10035 /* WSAEWOULDBLOCK */ )
          throw Exception.Preserve();
      }
    }
  }

  public static class ConcurrentDictionaryHelper
  {
    public static bool Remove<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key)
    {
      return Dictionary.TryRemove(Key, out TValue Value);
    }
    public static void Add<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key, TValue Value)
    {
      if (!Dictionary.TryAdd(Key, Value))
        throw new Exception("Concurrent dictionary already contains an item for this key.");
    }
    public static TValue GetValueOrDefault<TKey, TValue>(this System.Collections.Concurrent.ConcurrentDictionary<TKey, TValue> Dictionary, TKey Key, TValue DefaultValue = default(TValue))
    {
      if (Dictionary.TryGetValue(Key, out TValue Result))
        return Result;
      else
        return DefaultValue;
    }
  }

  public static class MediaColorHelper
  {
    public static Inv.Colour ConvertToInvColour(this System.Windows.Media.Color? MediaColor)
    {
      return MediaColor != null ? MediaColor.Value.ConvertToInvColour() : (Inv.Colour)null;
    }
    public static Inv.Colour ConvertToInvColour(this System.Windows.Media.Color MediaColor)
    {
      var ArgbArray = new byte[4] { MediaColor.B, MediaColor.G, MediaColor.R, MediaColor.A };

      return Inv.Colour.FromArgb(BitConverter.ToInt32(ArgbArray, 0));
    }
    public static System.Windows.Media.Color? ConvertToMediaColor(this Inv.Colour InvColour)
    {
      if (InvColour == null)
      {
        return null;
      }
      else
      {
        var ArgbArray = BitConverter.GetBytes(InvColour.RawValue);
        return System.Windows.Media.Color.FromArgb(ArgbArray[3], ArgbArray[2], ArgbArray[1], ArgbArray[0]);
      }
    }
  }

  public static class StringConverterHelper
  {
    public static TimeSpan? TimeSpanFromString(string Text)
    {
      var TimeSpan = new TimeSpan?();
      var ParseError = false;

      if (Text.ToLower().Contains("from") || Text.ToLower().Contains("to") || Text.ToLower().Contains("till"))
      {
        var TimeSplit = Text.ToLower().Split(new string[] { "from", "to", "till" }, StringSplitOptions.RemoveEmptyEntries);

        if (TimeSplit.Length != 2)
        {
          TimeSpan = new TimeSpan(0);
        }
        else
        {
          var StartTime = TimeSplit[0];
          StartTime = StartTime.Trim();

          var StartTimeSplit = StartTime.Split(':');

          var EndTime = TimeSplit[1];
          EndTime = EndTime.Trim();

          var EndTimeSplit = EndTime.Split(':');

          var StartTimeHours = 0;
          var StartTimeMinutes = 0;

          if (StartTimeSplit.Length == 1)
          {
            if (!int.TryParse(StartTimeSplit[0], out StartTimeHours))
              ParseError = true;
          }
          else if (StartTimeSplit.Length == 2)
          {
            if (!int.TryParse(StartTimeSplit[0], out StartTimeHours))
              ParseError = true;

            if (!int.TryParse(StartTimeSplit[1], out StartTimeMinutes))
              ParseError = true;
          }
          else
          {
            ParseError = true;
          }

          var EndTimeHours = 0;
          var EndTimeMinutes = 0;

          if (EndTimeSplit.Length == 1)
          {
            if (!int.TryParse(EndTimeSplit[0], out EndTimeHours))
              ParseError = true;
          }
          else if (EndTimeSplit.Length == 2)
          {
            if (!int.TryParse(EndTimeSplit[0], out EndTimeHours))
              ParseError = true;

            if (!int.TryParse(EndTimeSplit[1], out EndTimeMinutes))
              ParseError = true;
          }
          else
          {
            ParseError = true;
          }

          if (ParseError)
          {
            TimeSpan = new TimeSpan(0);
          }
          else
          {
            if (EndTimeHours < StartTimeHours)
              EndTimeHours += 12;

            if (EndTimeMinutes < StartTimeMinutes)
            {
              EndTimeMinutes += 60;
              EndTimeHours -= 1;
            }

            var TimeSpanHours = EndTimeHours - StartTimeHours;
            var TimeSpanMinutes = EndTimeMinutes - StartTimeMinutes;

            TimeSpan = new TimeSpan(TimeSpanHours, TimeSpanMinutes, 0);
          }

        }
      }
      else if (Text.ToLower().Contains("hour") || Text.ToLower().Contains("hours") || Text.ToLower().Contains("hrs") || Text.ToLower().Contains("hr") ||
        Text.ToLower().Contains("minute") || Text.ToLower().Contains("minutes") || Text.ToLower().Contains("mins") || Text.ToLower().Contains("min"))
      {
        var HourSplit = Text.ToLower().Split(new string[] { "hours", "hour", "hrs", "hr" }, StringSplitOptions.None);

        var TimeSpanHours = 0;
        var TimeSpanMinutes = 0;

        if (HourSplit.Length == 2)
        {
          if (!int.TryParse(HourSplit[0], out TimeSpanHours))
            ParseError = true;

          if (HourSplit[1] != "")
          {
            var MinuteString = HourSplit[1].Trim();
            MinuteString = MinuteString.Replace("minutes", "");
            MinuteString = MinuteString.Replace("minute", "");
            MinuteString = MinuteString.Replace("mins", "");
            MinuteString = MinuteString.Replace("min", "");

            if (!int.TryParse(MinuteString, out TimeSpanMinutes))
              ParseError = true;
          }
        }
        else if (HourSplit.Length == 1)
        {
          var MinuteString = HourSplit[0].Trim();
          MinuteString = MinuteString.Replace("minutes", "");
          MinuteString = MinuteString.Replace("minute", "");
          MinuteString = MinuteString.Replace("mins", "");
          MinuteString = MinuteString.Replace("min", "");

          if (!int.TryParse(MinuteString, out TimeSpanMinutes))
            ParseError = true;
        }
        else
        {
          ParseError = true;
        }

        if (ParseError)
          TimeSpan = new TimeSpan(0);
        else
          TimeSpan = new TimeSpan(TimeSpanHours, TimeSpanMinutes, 0);

      }
      else if (System.Text.RegularExpressions.Regex.IsMatch(Text, "[0-9]"))
      {
        if (int.TryParse(Text.Trim(), out int TimeSpanMinutes))
          TimeSpan = new TimeSpan(0, TimeSpanMinutes, 0);
        else
          ParseError = true;
      }
      else
      {
        ParseError = true;
      }

      return TimeSpan;
    }
    public static double? MathematicalEvaluateFromString(string Text)
    {
      var Queue = Organise(Tokenise(Text));

      var DynamicMethod = new DynamicMethod("Execution", typeof(double), null, typeof(StringConverterHelper));
      var ILGenerator = DynamicMethod.GetILGenerator();

      while (Queue.Count > 0)
      {
        var Token = Queue.Dequeue();

        switch (Token)
        {
          case "+":
            ILGenerator.Emit(OpCodes.Add);
            break;

          case "-":
            ILGenerator.Emit(OpCodes.Sub);
            break;

          case "*":
            ILGenerator.Emit(OpCodes.Mul);
            break;

          case "/":
            ILGenerator.Emit(OpCodes.Div);
            break;

          default:
            ILGenerator.Emit(OpCodes.Ldc_R8, double.Parse(Token));
            break;
        }
      }

      ILGenerator.Emit(OpCodes.Ret);

      return NonUserExecutionInvoke(DynamicMethod);
    }

    [System.Diagnostics.DebuggerNonUserCode]
    private static double? NonUserExecutionInvoke(DynamicMethod Method)
    {
      double? Result = null;
      try
      {
        Result = (double)Method.Invoke(null, null);
      }
      catch (Exception)
      {
      }

      return Result;
    }
    private static Queue<string> Tokenise(string Text)
    {
      var Position = 0;
      var TokenQueue = new Queue<string>();
      var StringBuilder = new StringBuilder();

      while (Position < Text.Length)
      {
        var Symbol = Text[Position];

        if (SymbolMap.Contains(Symbol))
        {
          if (StringBuilder.Length > 0)
          {
            TokenQueue.Enqueue(StringBuilder.ToString());
            StringBuilder.Clear();
          }

          TokenQueue.Enqueue(Symbol.ToString());
        }
        else if (char.IsDigit(Symbol) || Symbol == '.')
        {
          StringBuilder.Append(Symbol);
        }
        else if (!char.IsWhiteSpace(Symbol))
        {
          throw new InvalidDataException(string.Format("Invalid expression symbol '{0}'", Symbol));
        }

        Position++;
      }

      if (StringBuilder.Length > 0)
        TokenQueue.Enqueue(StringBuilder.ToString());

      return TokenQueue;
    }
    private static Queue<string> Organise(Queue<string> Text)
    {
      var PrecedenceStack = new Stack<string>();

      var Result = new Queue<String>();

      while (Text.Count > 0)
      {
        var Token = Text.Dequeue();

        if (OperatorMap.Contains(Token))
        {
          while (PrecedenceStack.Count > 0 && PrecedenceStack.Peek() != "(")
          {
            if (PrecedenceLevel(PrecedenceStack.Peek()) >= PrecedenceLevel(Token))
              Result.Enqueue(PrecedenceStack.Pop());
            else
              break;
          }

          PrecedenceStack.Push(Token);
        }
        else if (Token == "(")
        {
          PrecedenceStack.Push("(");
        }
        else if (Token == ")")
        {
          while (PrecedenceStack.Count > 0 && PrecedenceStack.Peek() != "(")
            Result.Enqueue(PrecedenceStack.Pop());

          if (PrecedenceStack.Count > 0)
            PrecedenceStack.Pop();
        }
        else
        {
          Result.Enqueue(Token);
        }
      }
      while (PrecedenceStack.Count > 0)
      {
        Result.Enqueue(PrecedenceStack.Pop());
      }

      return Result;
    }
    private static int PrecedenceLevel(string Value)
    {
      int Result;

      switch (Value)
      {
        case "*":
        case "/":
          Result = 10;
          break;

        case "+":
        case "-":
          Result = 9;
          break;

        default:
          Result = 0;
          break;
      }

      return Result;
    }

    private const string OperatorMap = "+-/*";
    private const string SymbolMap = OperatorMap + "()";
  }

  public static class ResourceDictionaryHelper
  {
    [DebuggerNonUserCode]
    public static bool TrySetSource(this ResourceDictionary ResourceDictionary, Uri Uri)
    {
      try
      {
        ResourceDictionary.Source = Uri;

        return true;
      }
      catch
      {
        return false;
      }
    }
  }

  public static class TcpClientHelper
  {
    [DebuggerNonUserCode]
    public static bool TryConnect(this System.Net.Sockets.TcpClient TcpClient, string Host, int Port)
    {
      try
      {
        TcpClient.Connect(Host, Port);

        return true;
      }
      catch (System.Net.Sockets.SocketException)
      {
        return false;
      }
    }
  }

  public static class EffectHelper
  {
    public static Effect FreezeIt(this Effect Effect)
    {
      Effect.Freeze();

      return Effect;
    }
  }

  public static class PenHelper
  {
    public static Pen FreezeIt(this Pen Pen)
    {
      Pen.Freeze();

      return Pen;
    }
  }

  public static class StyleHelper
  {
    public static Style SealIt(this Style Style)
    {
      Style.Seal();

      return Style;
    }
  }

  public static class FrameworkElementHelper
  {
    public static BitmapSource RenderToBitmapSource(this FrameworkElement Element)
    {
      Element.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
      Element.Arrange(new System.Windows.Rect(Element.DesiredSize));

      var Result = new RenderTargetBitmap((int)Element.ActualWidth, (int)Element.ActualHeight, 96d, 96d, PixelFormats.Pbgra32);
      Result.Render(Element);
      Result.Freeze();

      return Result;
    }
    public static BitmapSource ScaledRenderToBitmapSource(this FrameworkElement Element, int Width, int Height)
    {
      var ScaleTransform = new ScaleTransform()
      {
        ScaleX = double.IsNaN(Element.Width) ? 1 : Width / Element.Width,
        ScaleY = double.IsNaN(Element.Height) ? 1 : Height / Element.Height
      };
      var ContentControl = new ContentControl()
      {
        Content = Element,
        RenderTransform = ScaleTransform
      };
      ContentControl.Measure(new System.Windows.Size(double.PositiveInfinity, double.PositiveInfinity));
      ContentControl.Arrange(new System.Windows.Rect(ContentControl.DesiredSize));

      var Result = new RenderTargetBitmap(Width, Height, 96, 96, PixelFormats.Pbgra32);
      Result.Render(ContentControl);
      Result.Freeze();

      return Result;
    }
    public static DrawingVisual WrapWithDrawingVisual(this FrameworkElement Element, System.Windows.Size ElementSize, System.Windows.Media.Brush BackgroundBrush = null)
    {
      var DrawingVisual = new DrawingVisual();
      using (var DrawingContext = DrawingVisual.RenderOpen())
      {
        var VisualBrush = new VisualBrush(Element);

        if (BackgroundBrush != null)
          DrawingContext.DrawRectangle(BackgroundBrush, null, new System.Windows.Rect(0, 0, ElementSize.Width, ElementSize.Height));

        DrawingContext.DrawRectangle(VisualBrush, null, new System.Windows.Rect(0, 0, ElementSize.Width, ElementSize.Height));
      }

      return DrawingVisual;
    }
  }

  public static class DrawingVisualHelper
  {
    public static BitmapSource RenderToBitmapSource(this DrawingVisual Visual, System.Windows.Size Size)
    {
      var Result = new RenderTargetBitmap((int)Size.Width, (int)Size.Height, 96d, 96d, PixelFormats.Pbgra32);
      Result.Render(Visual);
      Result.Freeze();

      return Result;
    }
  }

  public static class ConcurrentBagHelper
  {
    public static IEnumerable<T> TakeAll<T>(this ConcurrentBag<T> Bag)
    {
      while (Bag.TryTake(out T Result))
        yield return Result;
    }
  }

  public static class WindowsIdentityHelper
  {
    public static WindowsIdentity GetWindowsIdentity(string Domain, string Username, string Password, bool IsInteractive = true)
    {
      if (Win32.advapi32.LogonUser(Username, Domain, Password, IsInteractive ? Win32.advapi32.LOGON32_LOGON_INTERACTIVE : Win32.advapi32.LOGON32_LOGON_NETWORK, Win32.advapi32.LOGON32_PROVIDER_DEFAULT, out Win32.Kernel32.SafeTokenHandle LogonHandle))
        return new WindowsIdentity(LogonHandle.DangerousGetHandle());
      else
        return null;
    }
    public static bool IsElevated()
    {
      return WindowsIdentity.GetCurrent().IsElevated();
    }
    public static bool IsElevated(this WindowsIdentity WindowsIdentity)
    {
      return new WindowsPrincipal(WindowsIdentity).IsInRole(WindowsBuiltInRole.Administrator);
    }
  }

  public static class ProcessHelper
  {
    [DebuggerNonUserCode]
    public static string MainFilePath(this Process Process)
    {
      try
      {
        return Process.MainModule.FileName;
      }
      catch
      {
        return Process.ProcessName;
      }
    }
  }

  public static class NetworkInterfaceHelper
  {
    public static NetworkInterface GetInterfaceByAddress(string Address)
    {
      NetworkInterface ActiveInterface = null;

      var UdpClient = new UdpClient(Address, 1);
      IPAddress LocalAddress = ((IPEndPoint)UdpClient.Client.LocalEndPoint).Address;

      foreach (var Interface in NetworkInterface.GetAllNetworkInterfaces())
      {
        if (Interface.GetIPProperties().UnicastAddresses.Exists(UnicastAddress => UnicastAddress.Address.Equals(LocalAddress)))
          ActiveInterface = Interface;

        if (ActiveInterface != null)
          break;
      }

      return ActiveInterface;
    }
    public static bool IsWireless(this NetworkInterface Interface)
    {
      return Interface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211;
    }
    public static bool IsEthernet(this NetworkInterface Interface)
    {
      return Interface.NetworkInterfaceType.ToString().Contains("Ethernet", StringComparison.CurrentCultureIgnoreCase);
    }
    public static bool IsLocalHost(this IPAddress Address)
    {
      var HostEntry = Dns.GetHostEntry(Address);
      var LocalHostEntry = Dns.GetHostEntry(Dns.GetHostName());

      foreach (IPAddress HostAddress in HostEntry.AddressList)
      {
        if (IPAddress.IsLoopback(HostAddress) || Array.IndexOf(LocalHostEntry.AddressList, HostAddress) != -1)
          return true;
      }

      return false;
    }
    public static IPAddress AsIPAddress(this string Source)
    {
      if (IPAddress.TryParse(Source, out IPAddress Address))
        return Address;

      var AddressList = Dns.GetHostEntry(Source).AddressList;
      var V4Addresses = AddressList.Where(A => !A.ToString().Contains(":") && !A.Equals(IPAddress.Loopback) && !A.Equals(IPAddress.IPv6Loopback)).ToList();
      var V4Address = V4Addresses.Where(A => !A.ToString().StartsWith("169")).FirstOrDefault();

      return V4Address;
    }
  }

  public static class ScreenHelper
  {
    [DllImport("gdi32.dll")]
    static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
    public enum DeviceCap
    {
      HORZRES = 8,
      VERTRES = 10,
      DESKTOPVERTRES = 117,
      DESKTOPHORZRES = 118,

      // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
    }

    public static float GetScalingFactor(out int PhysicalScreenWidth, out int PhysicalScreenHeight)
    {
      var G = System.Drawing.Graphics.FromHwnd(IntPtr.Zero);
      var Desktop = G.GetHdc();

      return GetScalingFactorForDeviceContext(Desktop, out PhysicalScreenWidth, out PhysicalScreenHeight);
    }
    public static float GetScalingFactorForDeviceContext(IntPtr DeviceContext, out int PhysicalScreenWidth, out int PhysicalScreenHeight)
    {
      var LogicalScreenHeight = GetDeviceCaps(DeviceContext, (int)DeviceCap.VERTRES);
      PhysicalScreenHeight = GetDeviceCaps(DeviceContext, (int)DeviceCap.DESKTOPVERTRES);
      PhysicalScreenWidth = GetDeviceCaps(DeviceContext, (int)DeviceCap.DESKTOPHORZRES);

      var ScreenScalingFactor = (float)PhysicalScreenHeight / (float)LogicalScreenHeight;

      return ScreenScalingFactor;
    }
  }

  public class Monitor
  {
    public IntPtr Handle;
    public string Name;
    public Rect Location;
    public bool IsPrimary;
    public float ScalingFactor;
    public int PhysicalScreenWidth;
    public int PhysicalScreenHeight;
  }

  public static class MonitorHelper
  {
    public static IntPtr GetWindowHandle(Window Window)
    {
      return new WindowInteropHelper(Window).Handle;
    }

    public static Monitor[] GetMonitorInformation()
    {
      var InformationList = new List<Monitor>();
      Win32.User32.EnumDisplayMonitors(IntPtr.Zero, IntPtr.Zero, MonitorDelegate, IntPtr.Zero);
      return InformationList.ToArray();

      bool MonitorDelegate(IntPtr MonitorHandle, IntPtr DeviceContext, ref Rect Rect, IntPtr Data)
      {
        var MonitorInfo = new Win32.User32.MonitorInfoEx();

        if (Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfo))
        {
          InformationList.Add(MonitorInfo.AsMonitor(MonitorHandle));
        }

        return true;
      };
    }
    public static bool WindowIsOnPrimaryMonitor(IntPtr WindowHandle)
    {
      var MonitorHandle = Win32.User32.MonitorFromWindow(WindowHandle, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST);
      return IsPrimaryMonitor(MonitorHandle);
    }
    public static bool WindowIsOnPrimaryMonitor(Window Window)
    {
      var MonitorHandle = GetCurrentMonitorHandle(Window);
      return IsPrimaryMonitor(MonitorHandle);
    }

    public static Monitor GetCurrentMonitorInformation(IntPtr WindowHandle)
    {
      return GetMonitorInformation(Win32.User32.MonitorFromWindow(WindowHandle, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST));
    }
    public static Monitor GetCurrentMonitorInformation(Window Window)
    {
      return GetMonitorInformation(GetCurrentMonitorHandle(Window));
    }
    public static Monitor GetPrimaryMonitorInformation()
    {
      return GetMonitorInformation(GetPrimaryMonitorHandle());
    }

    private static Monitor GetMonitorInformation(IntPtr MonitorHandle)
    {
      var MonitorInfoEx = new Win32.User32.MonitorInfoEx();
      Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfoEx);

      return MonitorInfoEx.AsMonitor(MonitorHandle);
    }

    public static Rect GetWindowLocation(IntPtr WindowHandle)
    {
      var Rect = new Win32.User32.RectStruct();
      Win32.User32.GetWindowRect(WindowHandle, ref Rect);

      return new Rect(Rect.Left, Rect.Top, Rect.Right - Rect.Left, Rect.Bottom - Rect.Top);
    }

    private static IntPtr GetCurrentMonitorHandle(Window Window)
    {
      //var Point = new System.Drawing.Point((int)Window.Left, (int)Window.Top);
      // Use middle of window as the point for determining which screen the window is primarily on
      var Point = new System.Drawing.Point((int)Window.Left + ((int)Window.Width / 2), (int)Window.Top + ((int)Window.Height / 2));

      return Win32.User32.MonitorFromPoint(Point, Win32.User32.MonitorConstants.MONITOR_DEFAULTTONEAREST);
    }
    private static IntPtr GetPrimaryMonitorHandle()
    {
      var Point = new System.Drawing.Point(0, 0);

      return Win32.User32.MonitorFromPoint(Point, Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY);
    }
    private static bool IsPrimaryMonitor(IntPtr MonitorHandle)
    {
      var MonitorInfo = new Win32.User32.MonitorInfoEx();

      Win32.User32.GetMonitorInfo(MonitorHandle, MonitorInfo);

      return MonitorInfo.IsPrimaryMonitor();
    }
  }

  static class MonitorInfoExHelper
  {
    public static bool IsPrimaryMonitor(this Win32.User32.MonitorInfoEx MonitorInfoEx)
    {
      return (MonitorInfoEx.dwFlags & Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY) == Win32.User32.MonitorConstants.MONITOR_DEFAULTTOPRIMARY;
    }
    public static float ScalingFactor(this Win32.User32.MonitorInfoEx MonitorInfoEx, out int PhysicalScreenWidth, out int PhysicalScreenHeight)
    {
      PhysicalScreenWidth = 0;
      PhysicalScreenHeight = 0;

      IntPtr DC = IntPtr.Zero;
      try
      {
        var DeviceName = new string(MonitorInfoEx.szDevice);
        DC = Win32.Gdi32.CreateDC(null, DeviceName, null, IntPtr.Zero);

        if (DC != IntPtr.Zero)
          return ScreenHelper.GetScalingFactorForDeviceContext(DC, out PhysicalScreenWidth, out PhysicalScreenHeight);
        else
          return 1;
      }
      finally
      {
        Win32.Gdi32.DeleteDC(DC);
      }
    }

    public static Monitor AsMonitor(this Win32.User32.MonitorInfoEx MonitorInfoEx, IntPtr MonitorHandle)
    {
      return new Monitor
      {
        Handle = MonitorHandle,
        Name = new string(MonitorInfoEx.szDevice).Trim('\0'),
        Location = new Rect(
          Left: MonitorInfoEx.rcMonitor.Left,
          Top: MonitorInfoEx.rcMonitor.Top,
          Height: MonitorInfoEx.rcMonitor.Bottom - MonitorInfoEx.rcMonitor.Top,
          Width: MonitorInfoEx.rcMonitor.Right - MonitorInfoEx.rcMonitor.Left
        ),
        IsPrimary = MonitorInfoEx.IsPrimaryMonitor(),
        ScalingFactor = MonitorInfoEx.ScalingFactor(out var PhysicalScreenWidth, out var PhysicalScreenHeight),
        PhysicalScreenWidth = PhysicalScreenWidth,
        PhysicalScreenHeight = PhysicalScreenHeight
      };
    }
  }

  public static class CsvTableHelper
  {
    public static CsvReadFile ReadResource(this CsvTable CsvTable, Inv.Resource.BinaryReference Resource)
    {
      return CsvTable.ReadFile(new StreamReader(new MemoryStream(Resource.GetBuffer())));
    }
    public static CsvReadFile ReadFile(this CsvTable CsvTable, string Path)
    {
      return CsvTable.ReadFile(new StreamReader(Path));
    }
    public static CsvWriteFile WriteFile(this CsvTable CsvTable, string Path)
    {
      return CsvTable.WriteFile(new StreamWriter(Path));
    }
  }

  public static class XmlDocumentHelper
  {
    public static void WriteToFile(this XmlDocument XmlDocument, string FilePath, System.Text.Encoding Encoding = null)
    {
      using (var FileSystemTransaction = Inv.FileSystemTransaction.BeginKTM("Inv.XmlDocumentWriteToFile"))
      {
        using (var FileStream = FileSystemTransaction.OpenFile(FilePath, FileMode.Create, FileAccess.Write, FileShare.Read))
          XmlDocument.WriteToStream(FileStream, Encoding);

        FileSystemTransaction.Commit();
      }
    }
    public static void ReadFromFile(this XmlDocument XmlDocument, string FilePath)
    {
      using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 65536))
        XmlDocument.ReadFromStream(FileStream);
    }
  }

  public static class EnvironmentHelper
  {
    public static EnvironmentAuthority GetMachineAuthority()
    {
      return new EnvironmentAuthority(Environment.MachineName, string.Empty, string.Empty);
    }
    public static EnvironmentAuthority GetDomainAuthority()
    {
      var UserDNSDomain = Environment.GetEnvironmentVariable("USERDNSDOMAIN").NullAsEmpty().ToLower();

      return new EnvironmentAuthority
      (
        Name: !string.IsNullOrEmpty(UserDNSDomain) ? Environment.UserDomainName : Environment.MachineName,
        Host: !string.IsNullOrEmpty(UserDNSDomain) ? UserDNSDomain : string.Empty,
        Root: !string.IsNullOrEmpty(UserDNSDomain) ? UserDNSDomain.Split('.').Select(Part => "DC=" + Part).AsSeparatedText(",") : string.Empty
      );
    }
    public static string DotNetFramework()
    {
      return GetDotNetFrameworkFromRegistry() + " (" + Environment.Version.ToString() + ")";
    }
    
    private static string GetDotNetFrameworkFromRegistry()
    {
      try
      {
        using (var BaseKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
        {
          var ReleaseKey = Convert.ToInt32(BaseKey.GetValue("Release"));

          // Checking the version using >= will enable forward compatibility,  
          // however you should always compile your code on newer versions of 
          // the framework to ensure your app works the same. 

          if (ReleaseKey == 461808/* Windows 10 */ || ReleaseKey == 461814/* Other Windows */)
            return "4.7.2";
          else if (ReleaseKey > 461808)
            return "4.7.2 + patches";

          if (ReleaseKey == 461308/* Windows 10 */ || ReleaseKey == 461310/* Other Windows */)
            return "4.7.1";
          else if (ReleaseKey > 461308)
            return "4.7.1 + patches";

          if (ReleaseKey == 460798/* Windows 10 */ || ReleaseKey == 460805/* Other Windows */)
            return "4.7.0";
          else if (ReleaseKey > 460798)
            return "4.7.0 + patches";

          if (ReleaseKey == 394802/* Windows 10 */ || ReleaseKey == 394806/* Other Windows */)
            return "4.6.2";
          else if (ReleaseKey > 394802)
            return "4.6.2 + patches";

          if (ReleaseKey == 394254/* Windows 10 */ || ReleaseKey == 394271/* Other Windows */)
            return "4.6.1";
          else if (ReleaseKey > 394254)
            return "4.6.1 + patches";

          if (ReleaseKey == 393295/* Windows 10 */ || ReleaseKey == 393297/* Other Windows */)
            return "4.6.0";
          else if (ReleaseKey > 393295)
            return "4.6.0 + patches";

          if (ReleaseKey == 379893)
            return "4.5.2";
          else if (ReleaseKey > 379893)
            return "4.5.2 + patches";

          if (ReleaseKey == 378675/*Windows 8.1*/ || ReleaseKey == 378758/*Windows 8, Windows 7, etc*/)
            return "4.5.1";
          else if (ReleaseKey > 378675)
            return "4.5.1 + patches";

          if (ReleaseKey == 378389)
            return "4.5.0";
          else if (ReleaseKey > 378389)
            return "4.5.0 + patches";

          // This line should never execute. A non-null release key should mean that 4.5 or later is installed. 
          return "?.?.?";
        }
      }
      catch
      {
        return "4.5.?";
      }
    }
  }

  public sealed class EnvironmentAuthority
  {
    internal EnvironmentAuthority(string Name, string Host, string Root)
    {
      this.Name = Name;
      this.Host = Host;
      this.Root = Root;
    }

    public string Name { get; private set; }
    public string Host { get; private set; }
    public string Root { get; private set; }
  }

  public static class DataObjectHelper
  {
    public static bool TryGetData<T>(this IDataObject DataObject, out T Result)
    {
      if (DataObject.GetDataPresent(typeof(T)))
      {
        Result = (T)DataObject.GetData(typeof(T));
        return true;
      }

      Result = default(T);
      return false;
    }
  }

  public static class ResourceFileFormatHelper
  {
    public static string GetInsightText(this Inv.Resource.FileFormat FileFormat, string FilePath)
    {
      var FileInfo = new FileInfo(FilePath);

      switch (FileFormat)
      {
        case Inv.Resource.FileFormat.Image:
          var ImageDimension = FileInfo.ReadImageDimension();
          return ImageDimension.Width + " x " + ImageDimension.Height;

        case Inv.Resource.FileFormat.Sound:
          return "~"; // TODO: this is too slow: FileInfo.ReadSoundLength().TotalSeconds.ToString("0.000");

        case Inv.Resource.FileFormat.Text:
          using (var FileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
          using (var StreamReader = new StreamReader(FileStream))
            return StreamReader.ReadLine().Truncate(128);

        case Inv.Resource.FileFormat.Binary:
          return "~";

        default:
          throw new Exception("FileFormat not handled: " + FileFormat);
      }
    }
  }
}