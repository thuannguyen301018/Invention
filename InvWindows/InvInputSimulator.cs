﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Inv.Support;

namespace Inv
{
  public static class MagneticStripeHelper
  {
    public static char? GetCharFromKey(Key key)
    {
      var Result = (char?)null;

      var VirtualKey = KeyInterop.VirtualKeyFromKey(key);

      var KeyboardState = new byte[256];
      Win32.User32.GetKeyboardState(KeyboardState);

      var ScanCode = Win32.User32.MapVirtualKey((uint)VirtualKey, Win32.User32.MapType.MAPVK_VK_TO_VSC);
      var StringBuilder = new StringBuilder(2);

      var ToUnicodeResult = Win32.User32.ToUnicode((uint)VirtualKey, ScanCode, KeyboardState, StringBuilder, StringBuilder.Capacity, 0);

      switch (ToUnicodeResult)
      {
        case -1:
          break;

        case 0:
          break;

        //case 1:
        default:
          if (StringBuilder.Length > 0)
            Result = StringBuilder[0];
          break;
      }

      return Result;
    }

    public static readonly char[] SpecialCharacters = new[] { ';', '?', '=', '%', '^', '+' };
  }

  public static class InputSimulator
  {
    public static void SendKeyDown(Key InputKey)
    {
      var Code = KeyInterop.VirtualKeyFromKey(InputKey);

      if (!SendInput(KeyToKeyDownInput(Code)))
        throw new Exception(string.Format("Failed to send input for {0} (Code: {1}).", InputKey.ToString(), Code));
    }
    public static void SendKeyUp(Key InputKey)
    {
      var Code = KeyInterop.VirtualKeyFromKey(InputKey);

      if (!SendInput(KeyToKeyUpInput(Code)))
        throw new Exception(string.Format("Failed to send input for {0} (Code: {1}).", InputKey.ToString(), Code));
    }
    public static void SendKeyStroke(Key InputKey)
    {
      var Code = KeyInterop.VirtualKeyFromKey(InputKey);

      if (!SendInput(KeyToKeyDownInput(Code), KeyToKeyUpInput(Code)))
        throw new Exception(string.Format("Failed to send input for {0} (Code: {1}).", InputKey.ToString(), Code));
    }
    public static void SendModifiedKeyStroke(Key ModifierKey, Key InputKey)
    {
      SendModifiedKeyStroke(ModifierKey.SingleToDistinctList(), InputKey.SingleToDistinctList());
    }
    public static void SendModifiedKeyStroke(IEnumerable<Key> ModifierKeys, Key InputKey)
    {
      SendModifiedKeyStroke(ModifierKeys, InputKey.SingleToDistinctList());
    }
    public static void SendModifiedKeyStroke(Key ModifierKey, IEnumerable<Key> InputKeys)
    {
      SendModifiedKeyStroke(ModifierKey.SingleToDistinctList(), InputKeys);
    }
    public static void SendModifiedKeyStroke(IEnumerable<Key> ModifierKeys, IEnumerable<Key> InputKeys)
    {
      var ModifierKeyList = ModifierKeys.ToList();

      ModifierKeyList.ForEach(Key => SendKeyDown(Key));

      foreach (var Key in InputKeys)
        SendKeyStroke(Key);

      ModifierKeyList.ForEach(Key => SendKeyUp(Key));
    }
    public static void SendModifiedKeyStroke(ModifierKeys ModifierKeys, Key InputKey)
    {
      SendModifiedKeyStroke(ModifierKeys, InputKey.SingleToDistinctList());
    }
    public static void SendModifiedKeyStroke(ModifierKeys ModifierKeys, IEnumerable<Key> InputKeys)
    {
      Debug.Assert(ModifierKeys != System.Windows.Input.ModifierKeys.None, "Modifier has not been specified.");

      var TranslatedModifierKeys = new Inv.DistinctList<System.Windows.Input.Key>();

      if (ModifierKeys.HasFlag(System.Windows.Input.ModifierKeys.Alt))
        TranslatedModifierKeys.Add(Key.LeftAlt);

      if (ModifierKeys.HasFlag(System.Windows.Input.ModifierKeys.Shift))
        TranslatedModifierKeys.Add(Key.LeftShift);

      if (ModifierKeys.HasFlag(System.Windows.Input.ModifierKeys.Control))
        TranslatedModifierKeys.Add(Key.LeftCtrl);

      if (ModifierKeys.HasFlag(System.Windows.Input.ModifierKeys.Windows))
        TranslatedModifierKeys.Add(Key.LWin);

      SendModifiedKeyStroke(TranslatedModifierKeys, InputKeys);
    }
    public static void SendText(string Text)
    {
      var CharacterByteArray = Encoding.ASCII.GetBytes(Text);
      var ArrayLength = CharacterByteArray.Length;
      var InputList = new Win32.User32.INPUT[ArrayLength * 2];

      for (var Character = 0; Character < ArrayLength; Character++)
      {
        var ScanCode = (ushort)CharacterByteArray[Character];

        var Down = ScanCodeToKeyDownInput(ScanCode);
        var Up = ScanCodeToKeyUpInput(ScanCode);

        if ((ScanCode & 65280) == 57344)
        {
          Down.data.ki.dwFlags = Down.data.ki.dwFlags | Win32.User32.KEYEVENTF.EXTENDEDKEY;
          Up.data.ki.dwFlags = Up.data.ki.dwFlags | Win32.User32.KEYEVENTF.EXTENDEDKEY;
        }

        InputList[2 * Character] = Down;
        InputList[2 * Character + 1] = Up;
      }

      if (!SendInput(InputList.ToArray()))
        throw new Exception(string.Format("Failed to send input for '{0}'.", Text));
    }

    private static Win32.User32.INPUT KeyToKeyDownInput(int VirtualKeyCode)
    {
      var Result = default(Win32.User32.INPUT);
      Result.type = 1;
      Result.data.ki = default(Win32.User32.KEYBDINPUT);
      Result.data.ki.wVk = (ushort)VirtualKeyCode;
      Result.data.ki.wScan = 0;
      Result.data.ki.dwFlags = IsExtendedKey(VirtualKeyCode) ? Win32.User32.KEYEVENTF.EXTENDEDKEY : 0;
      Result.data.ki.time = 0;
      Result.data.ki.dwExtraInfo = UIntPtr.Zero;

      var VirtualScanCode = Win32.User32.MapVirtualKey((uint)VirtualKeyCode, (uint)Win32.User32.MAPVK.MAPVK_VK_TO_VSC);
      Result.data.ki.wScan = (ushort)VirtualScanCode;

      return Result;
    }
    private static Win32.User32.INPUT KeyToKeyUpInput(int VirtualKeyCode)
    {
      var Result = KeyToKeyDownInput(VirtualKeyCode);
      Result.data.ki.dwFlags = (IsExtendedKey(VirtualKeyCode) ? Win32.User32.KEYEVENTF.EXTENDEDKEY : 0) | Win32.User32.KEYEVENTF.KEYUP;

      return Result;
    }
    private static Win32.User32.INPUT ScanCodeToKeyDownInput(int ScanCode)
    {
      var Result = default(Win32.User32.INPUT);
      Result.type = 1;
      Result.data.ki = default(Win32.User32.KEYBDINPUT);
      Result.data.ki.wVk = 0;
      Result.data.ki.wScan = (ushort)ScanCode;
      Result.data.ki.dwFlags = Win32.User32.KEYEVENTF.UNICODE;
      Result.data.ki.time = 0;
      Result.data.ki.dwExtraInfo = UIntPtr.Zero;

      return Result;
    }
    private static Win32.User32.INPUT ScanCodeToKeyUpInput(int ScanCode)
    {
      var Result = ScanCodeToKeyDownInput(ScanCode);
      Result.data.ki.dwFlags = Win32.User32.KEYEVENTF.UNICODE | Win32.User32.KEYEVENTF.KEYUP;

      return Result;
    }
    private static bool SendInput(params Win32.User32.INPUT[] InputArray)
    {
      var Count = InputArray.Count();

      return Win32.User32.SendInput((uint)Count, InputArray, Win32.User32.INPUT.Size) == Count; 
    }
    private static bool IsExtendedKey(int VirtualKeyCode)
    {
      var Enum = (Win32.User32.VirtualKeyCode)VirtualKeyCode;

      if (Enum == Win32.User32.VirtualKeyCode.MENU ||
          Enum == Win32.User32.VirtualKeyCode.LMENU ||
          Enum == Win32.User32.VirtualKeyCode.RMENU ||
          Enum == Win32.User32.VirtualKeyCode.CONTROL ||
          Enum == Win32.User32.VirtualKeyCode.RCONTROL ||
          Enum == Win32.User32.VirtualKeyCode.INSERT ||
          Enum == Win32.User32.VirtualKeyCode.DELETE ||
          Enum == Win32.User32.VirtualKeyCode.HOME ||
          Enum == Win32.User32.VirtualKeyCode.END ||
          Enum == Win32.User32.VirtualKeyCode.PRIOR ||
          Enum == Win32.User32.VirtualKeyCode.NEXT ||
          Enum == Win32.User32.VirtualKeyCode.RIGHT ||
          Enum == Win32.User32.VirtualKeyCode.UP ||
          Enum == Win32.User32.VirtualKeyCode.LEFT ||
          Enum == Win32.User32.VirtualKeyCode.DOWN ||
          Enum == Win32.User32.VirtualKeyCode.NUMLOCK ||
          Enum == Win32.User32.VirtualKeyCode.CANCEL ||
          Enum == Win32.User32.VirtualKeyCode.SNAPSHOT ||
          Enum == Win32.User32.VirtualKeyCode.DIVIDE)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }  
}
