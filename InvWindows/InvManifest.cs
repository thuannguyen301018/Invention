﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class AndroidManifestFile
  {
    public AndroidManifestFile(string FilePath)
    {
      this.FilePath = FilePath;

      this.XmlDocument = new Inv.XmlDocument();
      XmlDocument.ReadFromFile(FilePath);
      this.XmlRoot = XmlDocument.AccessRoot("manifest", "");
    }

    public string PackageIdentifier
    {
      get { return XmlRoot.AccessAttribute("package"); }
      set { XmlRoot.UpdateAttribute("package", value); }
    }
    public string VersionCode
    {
      get { return XmlRoot.AccessAttribute("android", "versionCode"); }
      set { XmlRoot.UpdateAttribute("android", "versionCode", value); }
    }
    public string VersionName
    {
      get { return XmlRoot.AccessAttribute("android", "versionName"); }
      set { XmlRoot.UpdateAttribute("android", "versionName", value); }
    }

    public void UpdateVersion(Inv.Version Version)
    {
      var VersionArray = Version.GetNumberArray();

      this.VersionName = VersionArray.Take(3).Select(V => V.ToString()).AsSeparatedText(".");
      this.VersionCode = VersionArray[VersionArray.Length - 1].ToString();
    }
    public bool IncrementVersionCode()
    {
      if (int.TryParse(this.VersionCode, out int VersionNumber))
      {
        VersionNumber++;

        this.VersionCode = VersionNumber.ToString();

        return true;
      }

      return false;
    }
    public void Save()
    {
      if (System.IO.File.Exists(FilePath))
        Inv.Support.FileHelper.SetReadOnly(FilePath, false);

      XmlDocument.WriteToFile(FilePath);
    }

    private string FilePath;
    private Inv.XmlDocument XmlDocument;
    private XmlElement XmlRoot;
  }

  public sealed class iOSManifestFile
  {
    public iOSManifestFile(string FilePath)
    {
      this.FilePath = FilePath;

      this.XmlDocument = new Inv.XmlDocument();
      XmlDocument.ReadFromFile(FilePath);

      var XmlRoot = XmlDocument.AccessRoot("plist", "");

      var XmlDict = XmlRoot.AccessChildElement("dict").Single();

      Inv.XmlElement FindValueElement(string KeyName)
      {
        var FoundVersion = false;

        foreach (var ChildElement in XmlDict.GetChildElements())
        {
          if (FoundVersion)
          {
            if (ChildElement.LocalName != "string")
              throw new Exception("Version string not found after key element.");

            return ChildElement;
          }

          if (ChildElement.LocalName == "key" && ChildElement.Content == KeyName)
            FoundVersion = true;
        }

        throw new Exception(KeyName + " value was not found.");
      }

      CFBundleVersionElement = FindValueElement("CFBundleVersion");
      CFBundleShortVersionStringElement = FindValueElement("CFBundleShortVersionString");
    }

    public string BundleShortVersionString
    {
      get { return CFBundleShortVersionStringElement.Content; }
      set { CFBundleShortVersionStringElement.Content = value; }
    }
    public string BundleVersion
    {
      get { return CFBundleVersionElement.Content; }
      set { CFBundleVersionElement.Content = value; }
    }

    public void UpdateVersion(Inv.Version Version)
    {
      var VersionArray = Version.GetNumberArray();

      this.BundleShortVersionString = VersionArray.Take(3).Select(V => V.ToString()).AsSeparatedText(".");
      this.BundleVersion = VersionArray[VersionArray.Length - 1].ToString();
    }
    public bool IncrementBundleVersion()
    {
      if (int.TryParse(this.BundleVersion, out int VersionCode))
      {
        VersionCode++;

        this.BundleVersion = VersionCode.ToString();

        return true;
      }

      return false;
    }
    public void Save()
    {
      if (System.IO.File.Exists(FilePath))
        Inv.Support.FileHelper.SetReadOnly(FilePath, false);

      XmlDocument.WriteToFile(FilePath);
    }

    private string FilePath;
    private Inv.XmlDocument XmlDocument;
    private XmlElement CFBundleVersionElement;
    private XmlElement CFBundleShortVersionStringElement;
  }

  public sealed class UwpManifestFile
  {
    public UwpManifestFile(string FilePath)
    {
      this.FilePath = FilePath;

      this.XmlDocument = new Inv.XmlDocument();
      XmlDocument.ReadFromFile(FilePath);
      var XmlRoot = XmlDocument.AccessRoot("Package", "http://schemas.microsoft.com/appx/manifest/foundation/windows10");
      this.IdentityXmlElement = XmlRoot.AccessChildElement("Identity").Single();
    }

    public string Version
    {
      get { return IdentityXmlElement.AccessAttribute("Version"); }
      set { IdentityXmlElement.UpdateAttribute("Version", value); }
    }

    public bool IncrementVersionBuild()
    {
      var VersionArray = this.Version.Split('.');
      var VersionCode = VersionArray[VersionArray.Length - 2]; // major.minor.build.revision
      if (int.TryParse(VersionCode, out int VersionNumber))
      {
        VersionNumber++;
        VersionArray[VersionArray.Length - 2] = VersionNumber.ToString();

        this.Version = VersionArray.AsSeparatedText(".");

        return true;
      }

      return false;
    }
    public void Save()
    {
      if (System.IO.File.Exists(FilePath))
        Inv.Support.FileHelper.SetReadOnly(FilePath, false);

      XmlDocument.WriteToFile(FilePath);
    }

    private string FilePath;
    private Inv.XmlDocument XmlDocument;
    private XmlElement IdentityXmlElement;
  }

  public sealed class VsixManifestFile
  {
    public VsixManifestFile(string FilePath)
    {
      this.FilePath = FilePath;

      this.XmlDocument = new Inv.XmlDocument();
      XmlDocument.ReadFromFile(FilePath);
      var XmlRoot = XmlDocument.AccessRoot("PackageManifest", "http://schemas.microsoft.com/developer/vsx-schema/2011");
      var MetadataXmlElement = XmlRoot.AccessChildElement("Metadata").Single();
      this.IdentityXmlElement = MetadataXmlElement.AccessChildElement("Identity").Single();
    }

    public string Version
    {
      get { return IdentityXmlElement.AccessAttribute("Version"); }
      set { IdentityXmlElement.UpdateAttribute("Version", value); }
    }

    public bool IncrementVersionBuild()
    {
      var VersionArray = this.Version.Split('.');
      var VersionCode = VersionArray[VersionArray.Length - 1]; // major.minor.build
      if (int.TryParse(VersionCode, out int VersionNumber))
      {
        VersionNumber++;
        VersionArray[VersionArray.Length - 1] = VersionNumber.ToString();

        this.Version = VersionArray.AsSeparatedText(".");

        return true;
      }

      return false;
    }
    public void Save()
    {
      if (System.IO.File.Exists(FilePath))
        Inv.Support.FileHelper.SetReadOnly(FilePath, false);

      XmlDocument.WriteToFile(FilePath);
    }

    private string FilePath;
    private Inv.XmlDocument XmlDocument;
    private XmlElement IdentityXmlElement;
  }
}