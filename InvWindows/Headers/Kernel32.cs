﻿/*! 3 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;
using System.Security;

namespace Win32
{
  public sealed class Kernel32
  {
    public enum CopyProgressResult : uint
    {
      PROGRESS_CONTINUE = 0,
      PROGRESS_CANCEL = 1,
      PROGRESS_STOP = 2,
      PROGRESS_QUIET = 3
    }

    public enum CopyProgressCallbackReason : uint
    {
      CALLBACK_CHUNK_FINISHED = 0x00000000,
      CALLBACK_STREAM_SWITCH = 0x00000001
    }

    [Flags]
    public enum CopyFileFlags : uint
    {
      COPY_FILE_FAIL_IF_EXISTS = 0x00000001,
      COPY_FILE_RESTARTABLE = 0x00000002,
      COPY_FILE_OPEN_SOURCE_FOR_WRITE = 0x00000004,
      COPY_FILE_ALLOW_DECRYPTED_DESTINATION = 0x00000008
    }

    [Flags]
    public enum EFileAccess : uint
    {
      GenericRead = 0x80000000,
      GenericWrite = 0x40000000,
      GenericExecute = 0x20000000,
      GenericAll = 0x10000000
    }

    [Flags]
    public enum EFileShare : uint
    {
      None = 0x00000000,
      Read = 0x00000001,
      Write = 0x00000002,
      Delete = 0x00000004
    }

    public enum ECreationDisposition : uint
    {
      New = 1,
      CreateAlways = 2,
      OpenExisting = 3,
      OpenAlways = 4,
      TruncateExisting = 5
    }

    [Flags]
    public enum EFileAttributes : uint
    {
      Readonly = 0x00000001,
      Hidden = 0x00000002,
      System = 0x00000004,
      Directory = 0x00000010,
      Archive = 0x00000020,
      Device = 0x00000040,
      Normal = 0x00000080,
      Temporary = 0x00000100,
      SparseFile = 0x00000200,
      ReparsePoint = 0x00000400,
      Compressed = 0x00000800,
      Offline = 0x00001000,
      NotContentIndexed = 0x00002000,
      Encrypted = 0x00004000,
      Write_Through = 0x80000000,
      Overlapped = 0x40000000,
      NoBuffering = 0x20000000,
      RandomAccess = 0x10000000,
      SequentialScan = 0x08000000,
      DeleteOnClose = 0x04000000,
      BackupSemantics = 0x02000000,
      PosixSemantics = 0x01000000,
      OpenReparsePoint = 0x00200000,
      OpenNoRecall = 0x00100000,
      FirstPipeInstance = 0x00080000
    }

    public enum GET_FILEEX_INFO_LEVELS
    {
      GetFileExInfoStandard,
      GetFileExMaxInfoLevel
    }

    [Flags]
    public enum MoveFileFlags
    {
      MOVEFILE_REPLACE_EXISTING = 0x00000001,
      MOVEFILE_COPY_ALLOWED = 0x00000002,
      MOVEFILE_DELAY_UNTIL_REBOOT = 0x00000004,
      MOVEFILE_WRITE_THROUGH = 0x00000008,
      MOVEFILE_CREATE_HARDLINK = 0x00000010,
      MOVEFILE_FAIL_IF_NOT_TRACKABLE = 0x00000020
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct WIN32_FILE_ATTRIBUTE_DATA
    {
      public FileAttributes dwFileAttributes;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftCreationTime;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftLastAccessTime;
      public System.Runtime.InteropServices.ComTypes.FILETIME ftLastWriteTime;
      public uint nFileSizeHigh;
      public uint nFileSizeLow;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_ATTRIBUTES
    {
      public int nLength;
      public IntPtr lpSecurityDescriptor;
      public int bInheritHandle;
    }

    [Flags]
    public enum LoadLibraryFlags : uint
    {
      DONT_RESOLVE_DLL_REFERENCES = 0x00000001,
      LOAD_IGNORE_CODE_AUTHZ_LEVEL = 0x00000010,
      LOAD_LIBRARY_AS_DATAFILE = 0x00000002,
      LOAD_LIBRARY_AS_DATAFILE_EXCLUSIVE = 0x00000040,
      LOAD_LIBRARY_AS_IMAGE_RESOURCE = 0x00000020,
      LOAD_WITH_ALTERED_SEARCH_PATH = 0x00000008
    }

    public enum FINDEX_INFO_LEVELS
    {
      FindExInfoStandard,
      FindExInfoMaxInfoLevel
    }

    public enum FINDEX_SEARCH_OPS
    {
      FindExSearchNameMatch,
      FindExSearchLimitToDirectories,
      FindExSearchLimitToDevices,
      FindExSearchMaxSearchOp
    }

    public struct FILETIME
    {
      public uint DateTimeLow;
      public uint DateTimeHigh;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public struct WIN32_FIND_DATA
    {
      public FileAttributes dwFileAttributes;
      public FILETIME ftCreationTime;
      public FILETIME ftLastAccessTime;
      public FILETIME ftLastWriteTime;
      public uint nFileSizeHigh;
      public uint nFileSizeLow;
      public uint dwReserved0;
      public uint dwReserved1;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
      public string cFileName;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
      public string cAlternateFileName;
    }

    public const int ERROR_FILE_NOT_FOUND = 2;
    public const int ERROR_INVALID_HANDLE = 6;
    public const int ERROR_HANDLE_EOF = 38;
    public const int ERROR_ALREADY_EXISTS = 183;
    public const int ERROR_MORE_DATA = 234;
    public const int ERROR_ARITHMETIC_OVERFLOW = 534;
    public const int ERROR_NOT_ENOUGH_MEMORY = 8;

    public delegate CopyProgressResult CopyProgressRoutine(long TotalFileSize, long TotalBytesTransferred, long StreamSize, long StreamBytesTransferred, uint dwStreamNumber, CopyProgressCallbackReason dwCallbackReason, IntPtr hSourceFile, IntPtr hDestinationFile, IntPtr lpData);

    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern IntPtr GlobalAlloc(int flags, IntPtr size);

    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern IntPtr GlobalFree(IntPtr handle);

    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern IntPtr GlobalLock(IntPtr handle);

    [DllImport("kernel32.dll", ExactSpelling = true)]
    public static extern bool GlobalUnlock(IntPtr handle);

    [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern IntPtr LoadLibrary(string lpFileName);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern IntPtr LoadLibraryEx(string lpFileName, IntPtr hFile, LoadLibraryFlags dwFlags);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool FreeLibrary([In] IntPtr hModule);

    [DllImport("kernel32", CharSet = CharSet.Ansi, ExactSpelling = true, SetLastError = true)]
    public static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern bool CloseHandle(IntPtr handle);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetFileAttributesTransacted(string lpFileName, GET_FILEEX_INFO_LEVELS fInfoLevelId, out WIN32_FILE_ATTRIBUTE_DATA fileData, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool SetFileAttributesTransacted(string lpFileName, uint dwFileAttributes, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern uint GetLongPathNameTransacted(string lpszShortPath, [Out] System.Text.StringBuilder lpszLongPath, uint cchBuffer, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool MoveFileTransacted(string lpExistingFileName, [MarshalAs(UnmanagedType.LPWStr)]string lpNewFileName, MoveFileFlags dwFlags, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CopyFileTransacted(string lpExistingFileName, [MarshalAs(UnmanagedType.LPWStr)]string lpNewFileName, CopyProgressRoutine lpProgressRoutine, IntPtr lpData, ref Int32 pbCancel, CopyFileFlags dwCopyFlags, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CreateDirectoryTransacted(string lpTemplateDirectory, [MarshalAs(UnmanagedType.LPWStr)]string lpNewDirectory, IntPtr SecurityAttributes, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool RemoveDirectoryTransacted(string lpPathName, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern SafeFileHandle CreateFileTransacted(string fileName, [MarshalAs(UnmanagedType.U4)] FileAccess fileAccess, [MarshalAs(UnmanagedType.U4)] FileShare fileShare, IntPtr securityAttributes, [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition, [MarshalAs(UnmanagedType.U4)] EFileAttributes flags, IntPtr template, IntPtr transaction, IntPtr miniVersion, IntPtr extendedOpenInformation);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool DeleteFileTransacted(string file, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool CreateHardLinkTransacted(string lpFileName, [MarshalAs(UnmanagedType.LPWStr)]string lpExistingFileName, IntPtr lpSecurityAttributes, IntPtr transaction);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern uint GetCompressedFileSizeTransacted(string lpFileName, IntPtr lpFileSizeHigh, IntPtr transaction);

    [DllImport("kernel32.dll", EntryPoint = "FindFirstFileTransacted", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern SafeFileHandle FindFirstFileTransacted([In] string lpDirSpec, [In] FINDEX_INFO_LEVELS fInfoLevelId, [Out] out WIN32_FIND_DATA lpFindFileData, [In] FINDEX_SEARCH_OPS fSearchOp, [In] IntPtr lpSearchFilter, [In] int dwAdditionalFlags, [In] IntPtr hTransaction);

    [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool FindClose([In] SafeFileHandle handle);

    [DllImportAttribute("kernel32.dll", EntryPoint = "MoveFileEx")]
    public static extern bool MoveFileEx(string lpExistingFileName, string lpNewFileName, MoveFileFlags dwFlags);

    [DllImport("Kernel32.dll")]
    public static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

    [DllImport("Kernel32.dll")]
    public static extern bool QueryPerformanceFrequency(out long lpFrequency);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public sealed class MEMORYSTATUSEX
    {
      public uint dwLength;
      public uint dwMemoryLoad;
      public ulong ullTotalPhys;
      public ulong ullAvailPhys;
      public ulong ullTotalPageFile;
      public ulong ullAvailPageFile;
      public ulong ullTotalVirtual;
      public ulong ullAvailVirtual;
      public ulong ullAvailExtendedVirtual;
      public MEMORYSTATUSEX()
      {
        this.dwLength = (uint)Marshal.SizeOf(this);
      }
    }

    [return: MarshalAs(UnmanagedType.Bool)]
    [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool GlobalMemoryStatusEx([In, Out] MEMORYSTATUSEX lpBuffer);

    public enum StdHandle 
    {
      STD_INPUT_HANDLE = -10,
      STD_OUTPUT_HANDLE = -11,
      STD_ERROR_HANDLE = -12 
    };

    [DllImport("kernel32.dll", EntryPoint = "GetStdHandle", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern IntPtr GetStdHandle(StdHandle std);
    [DllImport("kernel32.dll")]
    public static extern void SetStdHandle(StdHandle nStdHandle, IntPtr handle);

    public const int MY_CODE_PAGE = 437;

    [DllImport("kernel32.dll")]
    public static extern int AllocConsole();

    [DllImport("kernel32.dll", EntryPoint = "FreeConsole", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern Boolean FreeConsole();

    [DllImport("kernel32.dll", EntryPoint = "AttachConsole", SetLastError = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
    public static extern bool AttachConsole(int dwProcessId);

    [DllImport("kernel32.dll")]
    public static extern int GetConsoleOutputCP();

    [DllImport("kernel32.dll")]
    public static extern IntPtr GetConsoleWindow();

    public const int ATTACH_PARENT_PROCESS = -1;
    
    [DllImport("kernel32.dll")]
    public static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
    [DllImport("kernel32.dll")]
    public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

    [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
    public static extern bool CreateHardLink(string lpFileName, string lpExistingFileName, IntPtr lpSecurityAttributes);

    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern SafeHandle FindFirstFileNameW([In] string lpFileName, Int32 dwFlags, [In, Out] ref UInt32 StringLength, StringBuilder LinkName);
    [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool FindNextFileNameW(SafeHandle hFindStream, [In, Out] ref UInt32 StringLength, [In, Out] StringBuilder LinkName);

    /// <summary>
    /// Represents the type of data in a stream.
    /// </summary>
    public enum FileStreamType
    {
      /// <summary>
      /// Unknown stream type.
      /// </summary>
      Unknown = 0,
      /// <summary>
      /// Standard data.
      /// </summary>
      Data = 1,
      /// <summary>
      /// Extended attribute data.
      /// </summary>
      ExtendedAttributes = 2,
      /// <summary>
      /// Security data.
      /// </summary>
      SecurityData = 3,
      /// <summary>
      /// Alternate data stream.
      /// </summary>
      AlternateDataStream = 4,
      /// <summary>
      /// Hard link information.
      /// </summary>
      Link = 5,
      /// <summary>
      /// Property data.
      /// </summary>
      PropertyData = 6,
      /// <summary>
      /// Object identifiers.
      /// </summary>
      ObjectId = 7,
      /// <summary>
      /// Reparse points.
      /// </summary>
      ReparseData = 8,
      /// <summary>
      /// Sparse file.
      /// </summary>
      SparseBlock = 9,
      /// <summary>
      /// Transactional data.
      /// (Undocumented - BACKUP_TXFS_DATA)
      /// </summary>
      TransactionData = 10,
    }

    /// <summary>
    /// Represents the attributes of a file stream.
    /// </summary>
    [Flags]
    public enum FileStreamAttributes
    {
      /// <summary>
      /// No attributes.
      /// </summary>
      None = 0,
      /// <summary>
      /// Set if the stream contains data that is modified when read.
      /// </summary>
      ModifiedWhenRead = 1,
      /// <summary>
      /// Set if the stream contains security data.
      /// </summary>
      ContainsSecurity = 2,
      /// <summary>
      /// Set if the stream contains properties.
      /// </summary>
      ContainsProperties = 4,
      /// <summary>
      /// Set if the stream is sparse.
      /// </summary>
      Sparse = 8,
    }

    [Flags]
    public enum NativeFileFlags : uint
    {
      WriteThrough = 0x80000000,
      Overlapped = 0x40000000,
      NoBuffering = 0x20000000,
      RandomAccess = 0x10000000,
      SequentialScan = 0x8000000,
      DeleteOnClose = 0x4000000,
      BackupSemantics = 0x2000000,
      PosixSemantics = 0x1000000,
      OpenReparsePoint = 0x200000,
      OpenNoRecall = 0x100000
    }

    [Flags]
    public enum NativeFileAccess : uint
    {
      GenericRead = 0x80000000,
      GenericWrite = 0x40000000
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Win32StreamId
    {
      public readonly int StreamId;
      public readonly int StreamAttributes;
      public LargeInteger Size;
      public readonly int StreamNameSize;
    }

    public struct Win32StreamInfo
    {
      public FileStreamType StreamType;
      public FileStreamAttributes StreamAttributes;
      public long StreamSize;
      public string StreamName;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct LargeInteger
    {
      public readonly int Low;
      public readonly int High;

      public long ToInt64()
      {
        return (this.High * 0x100000000) + this.Low;
      }

      /*
      public static LargeInteger FromInt64(long value)
      {
        return new LargeInteger
        {
          Low = (int)(value & 0x11111111),
          High = (int)((value / 0x100000000) & 0x11111111)
        };
      }
      */
    }

    // "Characters whose integer representations are in the range from 1 through 31, 
    // except for alternate streams where these characters are allowed"
    // http://msdn.microsoft.com/en-us/library/aa365247(v=VS.85).aspx
    public static readonly char[] InvalidStreamNameChars = Path.GetInvalidFileNameChars().Where(c => c < 1 || c > 31).ToArray();
    public const int MaxPath = 256;
    public const string LongPathPrefix = @"\\?\";
    public const char StreamSeparator = ':';
    public const int DefaultBufferSize = 0x1000;

    [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern int GetFileAttributes(string fileName);

    [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool GetFileSizeEx(SafeFileHandle handle, out LargeInteger size);

    [DllImport("kernel32.dll")]
    public static extern int GetFileType(SafeFileHandle handle);

    [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
    public static extern SafeFileHandle CreateFile(
      string name,
      NativeFileAccess access,
      FileShare share,
      IntPtr security,
      FileMode mode,
      NativeFileFlags flags,
      IntPtr template);

    [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool DeleteFile(string name);

    [DllImport("kernel32", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool BackupRead(
      SafeFileHandle hFile,
      ref Win32StreamId pBuffer,
      int numberOfBytesToRead,
      out int numberOfBytesRead,
      [MarshalAs(UnmanagedType.Bool)] bool abort,
      [MarshalAs(UnmanagedType.Bool)] bool processSecurity,
      ref IntPtr context);

    [DllImport("kernel32", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool BackupRead(
      SafeFileHandle hFile,
      SafeHGlobalHandle pBuffer,
      int numberOfBytesToRead,
      out int numberOfBytesRead,
      [MarshalAs(UnmanagedType.Bool)] bool abort,
      [MarshalAs(UnmanagedType.Bool)] bool processSecurity,
      ref IntPtr context);

    [DllImport("kernel32", SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool BackupSeek(
      SafeFileHandle hFile,
      int bytesToSeekLow,
      int bytesToSeekHigh,
      out int bytesSeekedLow,
      out int bytesSeekedHigh,
      ref IntPtr context);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, BestFitMapping = false, ThrowOnUnmappableChar = true)]
    public static extern int FormatMessage(
      int dwFlags,
      IntPtr lpSource,
      int dwMessageId,
      int dwLanguageId,
      StringBuilder lpBuffer,
      int nSize,
      IntPtr vaListArguments);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr CreateFileMapping(IntPtr hFile, IntPtr lpFileMappingAttributes, uint flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, uint dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern IntPtr CreateFile(string lpFileName, uint dwDesiredAccess, uint dwShareMode, uint lpSecurityAttributes, uint dwCreationDisposition, uint dwFlagsAndAttributes, uint hTemplateFile);

    public const uint GENERIC_WRITE = 0x40000000;
    public const uint FILE_SHARE_WRITE = 0x2;
    public const uint OPEN_EXISTING = 0x3;

    [StructLayout(LayoutKind.Sequential)]
    public struct PROCESS_INFORMATION
    {
      public IntPtr hProcess;
      public IntPtr hThread;
      public int dwProcessId;
      public int dwThreadId;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct STARTUPINFO
    {
      public Int32 cb;
      public string lpReserved;
      public string lpDesktop;
      public string lpTitle;
      public Int32 dwX;
      public Int32 dwY;
      public Int32 dwXSize;
      public Int32 dwYSize;
      public Int32 dwXCountChars;
      public Int32 dwYCountChars;
      public Int32 dwFillAttribute;
      public Int32 dwFlags;
      public Int16 wShowWindow;
      public Int16 cbReserved2;
      public IntPtr lpReserved2;
      public IntPtr hStdInput;
      public IntPtr hStdOutput;
      public IntPtr hStdError;
    }

    public const int NORMAL_PRIORITY_CLASS = 0x00000020;
    public const int BELOW_NORMAL_PRIORITY_CLASS = 0x00004000;
    public const int ABOVE_NORMAL_PRIORITY_CLASS = 0x00008000;

    [DllImport("kernel32.dll")]
    public static extern bool CreateProcess(string lpApplicationName,
       string lpCommandLine, ref SECURITY_ATTRIBUTES lpProcessAttributes,
       ref SECURITY_ATTRIBUTES lpThreadAttributes, bool bInheritHandles,
       uint dwCreationFlags, IntPtr lpEnvironment, string lpCurrentDirectory,
       [In] ref STARTUPINFO lpStartupInfo,
       out PROCESS_INFORMATION lpProcessInformation);

    /// <summary>
    /// A <see cref="SafeHandle"/> for a global memory allocation.
    /// </summary>
    public sealed class SafeHGlobalHandle : SafeHandle
    {
      /// <summary>
      /// Initializes a new instance of the <see cref="SafeHGlobalHandle"/> class.
      /// </summary>
      /// <param name="toManage">
      /// The initial handle value.
      /// </param>
      /// <param name="size">
      /// The size of this memory block, in bytes.
      /// </param>
      [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
      private SafeHGlobalHandle(IntPtr toManage, int size)
        : base(IntPtr.Zero, true)
      {
        _size = size;
        base.SetHandle(toManage);
      }
      /// <summary>
      /// Initializes a new instance of the <see cref="SafeHGlobalHandle"/> class.
      /// </summary>
      private SafeHGlobalHandle()
        : base(IntPtr.Zero, true)
      {
      }

      /// <summary>
      /// Gets a value indicating whether the handle value is invalid.
      /// </summary>
      /// <value>
      /// <see langword="true"/> if the handle value is invalid;
      /// otherwise, <see langword="false"/>.
      /// </value>
      public override bool IsInvalid
      {
        get { return IntPtr.Zero == base.handle; }
      }
      /// <summary>
      /// Returns the size of this memory block.
      /// </summary>
      /// <value>
      /// The size of this memory block, in bytes.
      /// </value>
      public int Size
      {
        get { return _size; }
      }

      /// <summary>
      /// Executes the code required to free the handle.
      /// </summary>
      /// <returns>
      /// <see langword="true"/> if the handle is released successfully;
      /// otherwise, in the event of a catastrophic failure, <see langword="false"/>.
      /// In this case, it generates a releaseHandleFailed MDA Managed Debugging Assistant.
      /// </returns>
      protected override bool ReleaseHandle()
      {
        Marshal.FreeHGlobal(base.handle);
        return true;
      }

      /// <summary>
      /// Allocates memory from the unmanaged memory of the process using GlobalAlloc.
      /// </summary>
      /// <param name="bytes">
      /// The number of bytes in memory required.
      /// </param>
      /// <returns>
      /// A <see cref="SafeHGlobalHandle"/> representing the memory.
      /// </returns>
      /// <exception cref="OutOfMemoryException">
      /// There is insufficient memory to satisfy the request.
      /// </exception>
      public static SafeHGlobalHandle Allocate(int bytes)
      {
        return new SafeHGlobalHandle(Marshal.AllocHGlobal(bytes), bytes);
      }
      /// <summary>
      /// Returns an invalid handle.
      /// </summary>
      /// <returns>
      /// An invalid <see cref="SafeHGlobalHandle"/>.
      /// </returns>
      public static SafeHGlobalHandle Invalid()
      {
        return new SafeHGlobalHandle();
      }

      private readonly int _size;
    }
    public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
      private SafeTokenHandle()
        : base(true)
      {
      }

      [DllImport("kernel32.dll")]
      [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
      [SuppressUnmanagedCodeSecurity]
      [return: MarshalAs(UnmanagedType.Bool)]
      private static extern bool CloseHandle(IntPtr handle);

      protected override bool ReleaseHandle()
      {
        return CloseHandle(handle);
      }
    }
  }
}
