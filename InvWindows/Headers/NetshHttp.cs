﻿/*! 2 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Win32
{
  public static class NetshHttp
  {
    public static int HasUrlAcl(string UrlPattern, out bool Result, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(UrlPattern != null, "UrlPattern is not assigned.");

      Command = string.Format("netsh http show urlacl url={0}", UrlPattern);
      OutputLines = Execute(Command, out int ExitCode);

      Result = OutputLines.Any(L => L.IndexOf(UrlPattern, StringComparison.InvariantCultureIgnoreCase) > 0);

      return ExitCode;
    }
    public static bool DeleteUrlAcl(string UrlPattern, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(UrlPattern != null, "UrlPattern is not assigned.");

      Command = string.Format("netsh http delete urlacl url={0}", UrlPattern);
      OutputLines = Execute(Command, out int ExitCode);

      return ExitCode == 0;
    }
    public static bool AddUrlAcl(string UrlPattern, string Username, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(UrlPattern != null, "UrlPattern is not assigned.");

      Command = string.Format("netsh http add urlacl url={0} user={1}", UrlPattern, Username ?? "Everyone");
      OutputLines = Execute(Command, out int ExitCode);

      return ExitCode == 0;
    }
    public static int HasSslCert(string IpPortPattern, out bool Result, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(IpPortPattern != null, "IpPortPattern is not assigned.");

      Command = string.Format("netsh http show sslcert");
      OutputLines = Execute(Command, out int ExitCode);

      Result = OutputLines.Any(L => L.IndexOf(IpPortPattern, StringComparison.InvariantCultureIgnoreCase) > 0);

      return ExitCode;
    }
    public static bool DeleteSslCert(string IpPortPattern, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(IpPortPattern != null, "IpPortPattern is not assigned.");

      Command = string.Format("netsh http delete sslcert ipport={0}", IpPortPattern);
      OutputLines = Execute(Command, out int ExitCode);

      return ExitCode == 0;
    }
    public static bool AddSslCert(string IpPortPattern, string CertHash, string AppId, out List<string> OutputLines, out string Command)
    {
      Debug.Assert(IpPortPattern != null, "IpPortPattern is not assigned.");
      Debug.Assert(CertHash != null, "CertHash is not assigned.");
      Debug.Assert(AppId != null, "AppId is not assigned.");

      Command = string.Format("netsh http add sslcert ipport={0} certhash={1} appid={2}", IpPortPattern, CertHash, AppId);
      OutputLines = Execute(Command, out int ExitCode);

      return ExitCode == 0;
    }

    private static List<string> Execute(string Command, out int ExitCode)
    {
      var ActionProcess = new Process
      {
        StartInfo = new ProcessStartInfo
        {
          WindowStyle = ProcessWindowStyle.Hidden,
          CreateNoWindow = true,
          FileName = "cmd.exe",
          UseShellExecute = false,
          RedirectStandardOutput = true,
          Arguments = "/c " + Command
        }
      };

      ActionProcess.Start();

      var OutputLineList = new List<string>();

      while (!ActionProcess.StandardOutput.EndOfStream)
        OutputLineList.Add(ActionProcess.StandardOutput.ReadLine());

      ExitCode = ActionProcess.ExitCode;

      return OutputLineList.Where(V => !string.IsNullOrWhiteSpace(V)).ToList();
    }
  }
}
