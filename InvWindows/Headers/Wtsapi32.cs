﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Win32
{
  public enum WtsInfoClass : int
  {
    InitialProgram = 0,
    ApplicationName = 1,
    WorkingDirectory = 2,
    OEMId = 3,
    SessionId = 4,
    UserName = 5,
    WinStationName = 6,
    DomainName = 7,
    ConnectState = 8,
    ClientBuildNumber = 9,
    ClientName = 10,
    ClientDirectory = 11,
    ClientProductId = 12,
    ClientHardwareId = 13,
    ClientAddress = 14,
    ClientDisplay = 15,
    ClientProtocolType = 16,
    IdleTime = 17,
    LogonTime = 18,
    IncomingBytes = 19,
    OutgoingBytes = 20,
    IncomingFrames = 21,
    OutgoingFrames = 22,
    ClientInfo = 23,
    SessionInfo = 24,
    SessionInfoEx = 25,
    ConfigInfo = 26,
    ValidationInfo = 27,
    SessionAddressV4 = 28,
    IsRemoteSession = 29
  }

  public static class Wtsapi32
  {
    [DllImport("wtsapi32.dll")]
    public static extern bool WTSQuerySessionInformation(IntPtr hServer, int SessionId, WtsInfoClass WTSInfoClass, out IntPtr ppBuffer, out int pBytesReturned);
    [DllImport("wtsapi32.dll")]
    public static extern void WTSFreeMemory(IntPtr pMemory);
  }
}
