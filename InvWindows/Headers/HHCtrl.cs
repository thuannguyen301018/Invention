﻿/*! 1 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public static class HHCtrl
  {
    [DllImport("hhctrl.ocx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr HtmlHelpW(IntPtr hwnd, string HelpFile, int Command, int TopicID);

    [DllImport("hhctrl.ocx", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern IntPtr HtmlHelpA(IntPtr hwnd, string HelpFile, int Command, int TopicID);

    public const int HH_DISPLAY_TOC = 0x0001;
    public const int HH_DISPLAY_INDEX = 0x0002;
    public const int HH_DISPLAY_SEARCH = 0x0003;
    public const int HH_DISPLAY_TOPIC = 0x000;
    public const int HH_DISPLAY_CONTEXT = 0x000F;
    public const int HH_UNINITIALIZE = 0x001D;
    public const int HH_CLOSE_ALL = 0x0012;  // close all windows opened directly or indirectly by the caller
    public const int HH_INITIALIZE = 0x001C;  // Initializes the help system.
  }

  public sealed class HtmlHelp
  {
    public void Open()
    {
      HHCtrl.HtmlHelpW(IntPtr.Zero, "", HHCtrl.HH_INITIALIZE, 0);  // force HTML Help to run single-threaded
    }
    public void Close()
    {
      HHCtrl.HtmlHelpW(IntPtr.Zero, null, HHCtrl.HH_CLOSE_ALL, 0);
      HHCtrl.HtmlHelpW(IntPtr.Zero, null, HHCtrl.HH_UNINITIALIZE, 0);
    }
    public bool OpenByContextID(IntPtr Parent, string HelpFilePath, int TopicID)
    {
      var Result = HHCtrl.HtmlHelpW(Parent, HelpFilePath, HHCtrl.HH_DISPLAY_CONTEXT, TopicID);

      return Result != IntPtr.Zero;
    }
  }
}