﻿/*! 7 !*/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Set of emulated devices.
  /// </summary>
  public sealed class WpfDeviceEmulation
  {
    private WpfDeviceEmulation(string Name, int Width, int Height, WpfDeviceInset? PortraitInset = null, WpfDeviceInset? LandscapeInset = null)
    {
      this.Name = Name;
      this.Width = Width;
      this.Height = Height;
      this.PortraitInset = PortraitInset ?? WpfDeviceInset.Zero;
      this.LandscapeInset = LandscapeInset ?? WpfDeviceInset.Zero;
    }

    /// <summary>
    /// Name of the device.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// Width of the device in points.
    /// </summary>
    public int Width { get; private set; }
    /// <summary>
    /// Height of the device in points.
    /// </summary>
    public int Height { get; private set; }
    /// <summary>
    /// Insets to avoid the notch or home screen in portrait orientation.
    /// </summary>
    public Inv.WpfDeviceInset PortraitInset { get; private set; }
    /// <summary>
    /// Insets to avoid the notch or home screen in landscape orientation.
    /// </summary>
    public Inv.WpfDeviceInset LandscapeInset { get; private set; }

    /// <summary>
    /// iPhone 5
    /// </summary>
    public static readonly WpfDeviceEmulation iPhone5 = new WpfDeviceEmulation("Apple iPhone 5", 320, 568);
    /// <summary>
    /// iPhone 6 and iPhone 7
    /// </summary>
    public static readonly WpfDeviceEmulation iPhone6_7 = new WpfDeviceEmulation("Apple iPhone 6/7", 375, 667);
    /// <summary>
    /// iPhone 6 Plus and iPhone 7 Plus
    /// </summary>
    public static readonly WpfDeviceEmulation iPhone6_7Plus = new WpfDeviceEmulation("Apple iPhone 6/7 Plus", 414, 736);
    /// <summary>
    /// iPhone X
    /// </summary>
    public static readonly WpfDeviceEmulation iPhoneX = new WpfDeviceEmulation("Apple iPhone X", 375, 812, PortraitInset: new WpfDeviceInset(0, 44, 0, 34), LandscapeInset: new WpfDeviceInset(44, 0, 44, 21));
    /// <summary>
    /// iPad and iPad Mini
    /// </summary>
    public static readonly WpfDeviceEmulation iPad_Mini = new WpfDeviceEmulation("Apple iPad/Mini", 768, 1024);
    /// <summary>
    /// iPad Pro
    /// </summary>
    public static readonly WpfDeviceEmulation iPadPro = new WpfDeviceEmulation("Apple iPad Pro", 1024, 1366);
    /// <summary>
    /// Android Phone ZteZ752C
    /// </summary>
    public static readonly WpfDeviceEmulation ZteZ752C = new WpfDeviceEmulation("Zte Z752C", 320, 533);
    /// <summary>
    /// Google Nexus 5 Phone
    /// </summary>
    public static readonly WpfDeviceEmulation Nexus5 = new WpfDeviceEmulation("Google Nexus 5", 360, 592);
    /// <summary>
    /// Google Nexus 7 Mini Tablet
    /// </summary>
    public static readonly WpfDeviceEmulation Nexus7 = new WpfDeviceEmulation("Google Nexus 7", 600, 888);
    /// <summary>
    /// Google Nexus 9 Tablet
    /// </summary>
    public static readonly WpfDeviceEmulation Nexus9 = new WpfDeviceEmulation("Google Nexus 9", 768, 1024);
    /// <summary>
    /// Galaxy Avant SM-G386T
    /// </summary>
    public static readonly WpfDeviceEmulation GalaxyAvantSMG386T = new WpfDeviceEmulation("Samsung Galaxy Avant SM-G386T", 360, 640); // 5.23"
    /// <summary>
    /// Galaxy Note 4
    /// </summary>
    public static readonly WpfDeviceEmulation GalaxyNote4 = new WpfDeviceEmulation("Samsung Galaxy Note 4", 450, 800); // 5.7"
    /// <summary>
    /// Galaxy S10
    /// </summary>
    public static readonly WpfDeviceEmulation GalaxyS10 = new WpfDeviceEmulation("Samsung Galaxy Tab S 10\"", 800, 1280);
    /// <summary>
    /// Microsoft Surface 3
    /// </summary>
    public static readonly WpfDeviceEmulation Surface3 = new WpfDeviceEmulation("Microsoft Surface 3", 1064, 1600); // TODO: is this right?
    /// <summary>
    /// Vaio Duo 11
    /// </summary>
    public static readonly WpfDeviceEmulation VaioDuo11 = new WpfDeviceEmulation("Sony Vaio Duo 11", 1080, 1920); // TODO: is this right?

    /// <summary>
    /// The full array of emulated devices.
    /// </summary>
    public static readonly WpfDeviceEmulation[] Array;

    static WpfDeviceEmulation()
    {
      var List = new DistinctList<WpfDeviceEmulation>();

      var TypeInfo = typeof(WpfDeviceEmulation).GetReflectionInfo();

      foreach (var ReflectionField in TypeInfo.GetReflectionFields().Where(F => F.IsStatic && F.FieldType == typeof(WpfDeviceEmulation)))
        List.Add((WpfDeviceEmulation)ReflectionField.GetValue(null));

      Array = List.ToArray();
    }
  }

  /// <summary>
  /// Used to represent the inset to avoid the notch and home button on some phone models.
  /// </summary>
  public struct WpfDeviceInset
  {
    internal WpfDeviceInset(int Left, int Top, int Right, int Bottom)
    {
      this.Left = Left;
      this.Top = Top;
      this.Right = Right;
      this.Bottom = Bottom;
    }

    /// <summary>
    /// The left inset in logical points.
    /// </summary>
    public int Left { get; private set; }
    /// <summary>
    /// The top inset in logical points.
    /// </summary>
    public int Top { get; private set; }
    /// <summary>
    /// The right inset in logical points.
    /// </summary>
    public int Right { get; private set; }
    /// <summary>
    /// The bottom inset in logical points.
    /// </summary>
    public int Bottom { get; private set; }

    public static readonly WpfDeviceInset Zero = new WpfDeviceInset(0, 0, 0, 0);
  }  
}