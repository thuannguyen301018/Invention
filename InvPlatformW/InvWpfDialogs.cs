﻿/*! 4 !*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  internal sealed class WpfFileDialog
  {
    public WpfFileDialog(string Title)
    {
      this.Title = Title;
      this.Filter = new WpfFileFilter();
    }

    public string Title { get; private set; }
    public string InitialDirectoryPath { get; private set; }
    public string InitialFilePath { get; private set; }
    public string DefaultFileExtension { get; private set; }
    public WpfFileFilter Filter { get; private set; }

    public void SetInitialDirectoryPath(string DirectoryPath)
    {
      this.InitialDirectoryPath = DirectoryPath;
    }
    public void SetInitialFilePath(string FilePath)
    {
      this.InitialFilePath = FilePath;
    }
    public void SetDefaultFileExtension(string Extension)
    {
      this.DefaultFileExtension = Extension;
    }
    public string OpenSingleFile()
    {
      var Handle = new Microsoft.Win32.OpenFileDialog()
      {
        CheckFileExists = true,
        CheckPathExists = true,
        Multiselect = false
      };
      Prepare(Handle);

      if (Handle.ShowDialog().GetValueOrDefault(false))
        return Handle.FileName;
      else
        return null;
    }
    public string[] OpenMultipleFile()
    {
      var Handle = new Microsoft.Win32.OpenFileDialog()
      {
        CheckFileExists = true,
        CheckPathExists = true,

        Multiselect = true
      };
      Prepare(Handle);

      if (Handle.ShowDialog().GetValueOrDefault(false))
        return Handle.FileNames;
      else
        return null;
    }
    public string SaveSingleFile()
    {
      var Handle = new Microsoft.Win32.SaveFileDialog()
      {
        CheckFileExists = false,
        CheckPathExists = true,
        OverwritePrompt = true,
        AddExtension = true
      };
      Prepare(Handle);

      if (Handle.ShowDialog().GetValueOrDefault(false))
        return Handle.FileName;
      else
        return null;
    }

    private void Prepare(Microsoft.Win32.FileDialog Handle)
    {
      Handle.Title = Title;

      if (DefaultFileExtension == null && Filter.ItemList.Count > 0 && Filter.ItemList[0].ExtensionList.Count > 0)
        Handle.DefaultExt = Filter.ItemList[0].ExtensionList[0];
      else
        Handle.DefaultExt = DefaultFileExtension;

      Handle.InitialDirectory = InitialDirectoryPath;
      Handle.FileName = InitialFilePath;

      var FilterBuilder = new StringBuilder();

      foreach (var FilterItem in Filter.ItemList)
      {
        if (FilterBuilder.Length != 0)
          FilterBuilder.Append("|");

        FilterBuilder.Append(FilterItem.Title + "|");

        foreach (var FilterExtension in FilterItem.ExtensionList)
        {
          FilterBuilder.Append("*" + FilterExtension);

          if (FilterExtension != FilterItem.ExtensionList.Last())
            FilterBuilder.Append(";");
        }
      }

      Handle.Filter = FilterBuilder.ToString();
    }
  }

  internal sealed class WpfFileFilter
  {
    internal WpfFileFilter()
    {
    }

    public void Clear()
    {
      ItemList.Clear();
    }
    public void AddFile(string Title, params string[] ExtensionArray)
    {
      ItemList.Add(new Item()
      {
        Title = Title,
        ExtensionList = ExtensionArray.ToDistinctList()
      });
    }
    public void AddAllFile()
    {
      AddFile("All files", ".*");
    }
    public void AddCsvFile()
    {
      AddFile("Csv files", ".csv");
    }
    public void AddImageFile()
    {
      AddFile("Image files", ".jpg", ".jpeg", ".png", ".tif", ".tiff", ".bmp");
    }
    public void AddJpegFile()
    {
      AddFile("Jpeg files", ".jpg", ".jpeg");
    }
    public void AddPngFile()
    {
      AddFile("Png files", ".png");
    }
    public void AddLogFile()
    {
      AddFile("Log files", ".Log");
    }
    public void AddSoundFile()
    {
      AddFile("Sound files", ".mp3", ".wav");
    }
    public void AddMp3File()
    {
      AddFile("Mp3 files", ".mp3");
    }
    public void AddTextFile()
    {
      AddFile("Text files", ".txt");
    }
    public void AddSqlFile()
    {
      AddFile("Sql files", ".sql");
    }
    public void AddWavFile()
    {
      AddFile("Wave files", ".wav");
    }
    public void AddXmlFile()
    {
      AddFile("Xml files", ".xml");
    }
    public void AddZipFile()
    {
      AddFile("Zip files", ".zip");
    }

    internal Inv.DistinctList<Item> ItemList = new Inv.DistinctList<Item>();

    internal sealed class Item
    {
      public string Title { get; set; }
      public Inv.DistinctList<string> ExtensionList { get; set; }
    }
  }
}
